Conventions for names
=====================

-   ABM: Abschnittsmauer
-   AMB: Ambarlıkaya
-   BK: Büyükkale
-   BKA: Büyükkaya
-   HaH: Haus am Hang
-   KNW: Kesikkaya Nordwest
-   KSO: Kesikkaya Sudost
-   KIZ: Kızlarkaya
-   NT: Nişantepe
-   NWH: Nordwesthang
-   NOV: Nordost Viertel
-   NWHMP: Nordwesthang - Mittleres Plateau
-   MK: Mihraplıkaya
-   MP: Mittleres Plateau
-   OS: Oberstadt
-   PM: Poternenmauer
-   QG: Quellgrotte
-   SA: Südareal
-   SB: Südburg
-   SK: Sarıkale
-   US: Unterstadt
-   WT: Westtore
-   YK: Yazilikaya
-   YKP: Yerkapı
-   YCK: Yenicekale

Tempel I
========

-   KI: Komplex I
-   KII: Komplex II
-   KIII: Komplex III
-   NOM: Nordost-Magazine
-   NWM: Nordwest-Magazine
-   WM: West-Magazine
-   T1: Tempel

Unterstadt
==========

-   STER: Süd-Terrasse
-   NTER: Nord-Terrasse
-   WTER: West-Terrasse
-   NWTER: Nordwest-Terrasse

#### Change log

V. 0.0.1. Néhémie Strupler | 2015.06.30

#### License

[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) |
Néhémie Strupler
