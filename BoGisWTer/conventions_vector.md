Conventions for names
---------------------

See the corresponding file

Variable
--------

-   ID: serial number
-   Kennung: name of the feature (Excavation number if known or generic)
-   Art: art of the feature (Mauer, Grube, Teich, Zerstorung,
    Stadtmauer, Silo, Stutzenbasis, Tempel, Gebaude,
    Wasserleitung-Stein, Wasserleitugng-Kera)
-   Type: Specification of the Art (Material, size, ...)
-   Name: generic name for the building (TempelI, HausAmHang, etc.)
-   Stand: conservation i.e. either 'nachgewiesen' or 'vermutet'
-   Periode: general period: EBA, MBA, Hittte, Iron-Age,...
-   Phase: phase of the period
-   Lage: geographic area (OS, NWH, BK, BKA, etc.)

Change log
----------

V. 0.0.2. Néhémie Strupler | 2016.02.18

License
-------

[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) |
Néhémie Strupler
