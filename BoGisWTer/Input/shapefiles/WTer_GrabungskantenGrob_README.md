 # Description of the data from the shapefile  
 
  This is a shapefile of polygons, documenting the general excavation limits 
_U_s_a_g_e:

     data("objet")
     
_F_o_r_m_a_t:

     A data frame with 277 observations on the following 11 variables.

     ‘id’ a numeric vector

     ‘Jahr’ a numeric vector

     ‘RomischeZa’ a factor with levels ‘G14’ ‘H13’ ‘H18’ ‘H19’ ‘H42’
          ‘I’ ‘II’ ‘III’ ‘IV’ ‘IX’ ‘J’ ‘K’ ‘Nordwestlich’ ‘Schnitt.B’
          ‘Schnitt.C’ ‘Stadtmauer’ ‘Stadttor I/19’ ‘Stadttor J/21’ ‘V’
          ‘VI’ ‘VII’ ‘VIII’ ‘X’ ‘XI’ ‘XII’ ‘XIII’ ‘XIV’

     ‘ArabischeZ’ a factor with levels ‘0’ ‘0-10m’ ‘0-5m’ ‘1’ ‘10’
          ‘10-15m’ ‘10-20m’ ‘100-110m’ ‘15-20m’ ‘19’ ‘2’ ‘20’ ‘20-25m’
          ‘21’ ‘25-30m’ ‘3’ ‘30-35m’ ‘30-40m’ ‘35-40m’ ‘4’ ‘40-50m’ ‘5’
          ‘5-10m’ ‘50-60m’ ‘6’ ‘60-70m’ ‘7’ ‘70-80m’ ‘8’ ‘80-90m’ ‘9’
          ‘90-100m’ ‘H13’ ‘Hause’ ‘R1’ ‘R11’ ‘R14’ ‘R3’ ‘R4’ ‘R5’ ‘R7’
          ‘R9’

     ‘Zusammense’ a factor with levels ‘1956 / Schnitt C’ ‘37’ ‘38’ ‘56
          - Schnitt B’ ‘70/I-0-Nord’ ‘70/I-0-Sud’ ‘70/I-1’ ‘70/I-2’
          ‘70/I-3’ ‘70/I-4’ ‘70/I-5’ ‘70/I-6’ ‘70/II-1’ ‘70/II-2’
          ‘70/II-3’ ‘70/III-1’ ‘70/III-2’ ‘70/IV-1’ ‘70/IV-2’ ‘70/V-1’
          ‘70/V-2’ ‘70/V-3’ ‘70/V-4’ ‘70/V-5’ ‘70/V-6’ ‘70/VI-2’
          ‘71/II-1-Erweiterung’ ‘71/II-2-Erweiterung’ ‘71/IX’ ‘71/VII’
          ‘71/VIII’ ‘71/X’ ‘71/XII-1’ ‘71/XII-2’ ‘71/XII-3’ ‘71/XII-4’
          ‘71/XII-5’ ‘71/XII-6’ ‘71/XII-7’ ‘71/XII-8’ ‘71/XII-9’
          ‘71/XIII’ ‘73-II-2’ ‘73/I-0’ ‘73/I-1’ ‘73/I-10’
          ‘73/I-10-NO-Erweiterung’ ‘73/I-2’ ‘73/I-3’ ‘73/I-4’ ‘73/I-5’
          ‘73/I-6’ ‘73/I-7’ ‘73/I-7-NO-Erweiterung’ ‘73/I-8’
          ‘73/I-8-NO-Erweiterung’ ‘73/I-9’ ‘73/I-9-NO-Erweiterung’
          ‘73/II-1’ ‘73/II-2-NW-Erweiterung ’ ‘73/II-3’ ‘73/II-5’
          ‘73/II-6’ ‘73/II-6-NW-Erweiterung’ ‘73/II-7’ ‘73/II-8’
          ‘73/II-8-Norderweiterung’ ‘73/III-1’ ‘73/III-2’ ‘73/III-3’
          ‘75/I’ ‘75/I-1’ ‘75/I-Westerweiterung2’
          ‘75/I-Westerweiterung3’ ‘75/II’ ‘75/II-2’ ‘75/II-5’ ‘75/III’
          ‘75/IV’ ‘75/IX’ ‘75/V’ ‘75/VI’ ‘75/VII’ ‘75/VIII’ ‘75/X’
          ‘75/XI’ ‘75/XII’ ‘75/XIII’ ‘75/XIV’ ‘76/H13-R3’ ‘76/H19-R1’
          ‘76/I-1’ ‘76/I-2’ ‘76/I-3’ ‘76/I-4’ ‘76/I-5’ ‘76/I-6’
          ‘76/II-3’ ‘76/II-4’ ‘76/II-5’ ‘76/II-6’ ‘76/III-1’ ‘76/III-2’
          ‘76/III-3’ ‘76/IV-1’ ‘76/IV-2’ ‘77/I-1’ ‘77/I-2’ ‘77/I-3’
          ‘77/II-1’ ‘77/II-2’ ‘77/II-3’ ‘77/III-1’ ‘77/III-2’
          ‘77/III-3’ ‘77/III-4’ ‘77/IV-1’ ‘77/IV-2’ ‘77/IV-3’ ‘77/IV-4’
          ‘77/V-2’ ‘77/V-3’ ‘J21g10’

     ‘Car’ a factor with levels ‘37.K.20.Schnitt1’ ‘38.J.21.Grab am
          Tor’ ‘38.J.21.Tor’ ‘38.K.20’ ‘38.K.20.Keramikzimmer’
          ‘38.K.20.R13’ ‘38.K.20.R15’ ‘38.K.20.R16’ ‘38.K.20.R17’
          ‘38.K.20.R18’ ‘38.K.20.R19’ ‘38.K.20.R20’ ‘38.K.20.R3’
          ‘38.K.20.R33’ ‘38.K.20.R34’ ‘38.K.20.R35’ ‘38.K.20.R37’
          ‘38.K.20.R39’ ‘38.K.20.R40’ ‘38.K.20.R7’ ‘38.K.20.R8’
          ‘38.K.20.R9’ ‘53.J.20.k’ ‘53.J.20.k.1’ ‘53.J.20.k.2’
          ‘53.K.20.a.1’ ‘53.K.20.a.2’ ‘53.K.20.b.1’ ‘53.K.20.b.2’
          ‘55.J.20’ ‘56.J.19’ ‘56.J.20.f.1.b’ ‘56.J.20.g.1.a’
          ‘56.J.20.g.1.b’ ‘56.J.20.h.1.a’ ‘56.J.20.h.1.b’
          ‘56.J.20.I.1.a’ ‘56.J.20.I.1.b’ ‘56.J.20.k.1.a’ ‘56.J.20.k.2’
          ‘56.J.21.g.10.a’ ‘56.J.21.g.10.b’ ‘56.J.21.g.10.c’
          ‘56.J.21.g.10.d’ ‘56.J.21.h.10.a’ ‘56.J.21.h.10.b’
          ‘56.J.21.h.10.c’ ‘56.J.21.h.10.d’ ‘56.Schnitt.B.0-10m’
          ‘56.Schnitt.B.10-20m’ ‘56.Schnitt.B.100-110m’
          ‘56.Schnitt.B.20-25m’ ‘56.Schnitt.B.25-30m’
          ‘56.Schnitt.B.30-40m’ ‘56.Schnitt.B.40-50m’
          ‘56.Schnitt.B.50-60m’ ‘56.Schnitt.B.60-70m’
          ‘56.Schnitt.B.70-80m’ ‘56.Schnitt.B.80-90m’
          ‘56.Schnitt.B.90-100m’ ‘56.Schnitt.C.0-5m’
          ‘56.Schnitt.C.10-15m’ ‘56.Schnitt.C.15-20m’
          ‘56.Schnitt.C.20-25m’ ‘56.Schnitt.C.25-30m’
          ‘56.Schnitt.C.30-35m’ ‘56.Schnitt.C.35-40m’
          ‘56.Schnitt.C.5-10m’ ‘56.Stadttor I/19’ ‘57.J.20.g.1.d’
          ‘57.J.20.g.2.b’ ‘57.J.20.h.1.b’ ‘57.J.20.h.1.c’
          ‘57.J.20.h.1.d’ ‘57.J.20.h.2.b’ ‘57.J.20.h.7.a’
          ‘57.J.20.h.7.b’ ‘57.J.20.h.7.d’ ‘57.J.20.I.1.a’
          ‘57.J.20.I.1.c’ ‘57.J.20.I.2’ ‘57.J.20.I.2.a’ ‘57.J.20.I.5’
          ‘57.J.20.I.6.a’ ‘57.J.20.I.6.b’ ‘57.J.20.I.6.c’
          ‘57.J.20.I.6.d’ ‘57.J.20.I.7’ ‘57.J.20.I.7.a’ ‘57.J.20.I.7.b’
          ‘57.J.20.I.7.c’ ‘57.J.20.I.7.d’ ‘57.J.20.I.8.d’
          ‘57.J.20.k.1.c’ ‘57.J.20.k.1.d’ ‘57.J.20.k.2.a’
          ‘57.J.20.k.2.b’ ‘57.J.20.k.5’ ‘57.J.20.k.5.b’ ‘57.J.20.k.6.a’
          ‘57.J.20.k.6.b’ ‘57.J.20.k.6.c’ ‘57.J.20.k.6.d’
          ‘57.J.20.k.7.a’ ‘57.J.20.k.7.b’ ‘57.J.20.k.7.c’
          ‘57.J.20.k.7.d’ ‘57.K.20.a.6.a’ ‘57.K.20.a.6.c’
          ‘57.K.20.a.6.d’ ‘57.K.20.a.7.a’ ‘57.K.20.a.7.b’
          ‘57.K.20.a.7.c’ ‘57.K.20.a.7.d’ ‘57.K.20.a.8.b’
          ‘57.K.20.a.8.d’ ‘57.K.20.b.6.c’ ‘57.K.20.b.7.a’
          ‘57.K.20.b.7.b’ ‘57.K.20.b.7.c’ ‘57.K.20.b.7.d’ ‘57.K.20.b.9’
          ‘57.K.20.c.7.c’ ‘58.J.20.h.2’ ‘58.J.20.h.4.c’ ‘58.J.20.h.5’
          ‘58.J.20.h.5.b’ ‘58.J.20.h.6’ ‘58.J.20.I.6.b’ ‘58.J.20.I.6.d’
          ‘58.J.20.k.1.c’ ‘70.I.0.Nord’ ‘70.I.0.Sud’ ‘70.I.1’ ‘70.I.2’
          ‘70.I.3’ ‘70.I.4’ ‘70.I.5’ ‘70.I.6’ ‘70.II.1’ ‘70.II.2’
          ‘70.II.3’ ‘70.II.3.Erweiterung’ ‘70.III.1’ ‘70.III.2’
          ‘70.IV.1’ ‘70.IV.2’ ‘70.Stadttor I/19’ ‘70.V.1’ ‘70.V.2’
          ‘70.V.3’ ‘70.V.4’ ‘70.V.5’ ‘70.V.6’ ‘70.VI.2’
          ‘71.II.1.NO-Erweiterung’ ‘71.II.2.Erweiterung’ ‘71.IX’
          ‘71.VII’ ‘71.VIII’ ‘71.X’ ‘71.XI’ ‘71.XII’ ‘71.XII.1’
          ‘71.XII.2’ ‘71.XII.3’ ‘71.XII.4’ ‘71.XII.5’ ‘71.XII.6’
          ‘71.XII.7’ ‘71.XII.8’ ‘71.XII.9’ ‘71.XIII’ ‘72.Stadttor I/19’
          ‘73.I.0’ ‘73.I.1’ ‘73.I.10’ ‘73.I.10.NO-Erweiterung’ ‘73.I.2’
          ‘73.I.3’ ‘73.I.4’ ‘73.I.5’ ‘73.I.6’ ‘73.I.7’
          ‘73.I.7.NO-Erweiterung’ ‘73.I.8’ ‘73.I.8.NO-Erweiterung’
          ‘73.I.9’ ‘73.I.9.NO-Erweiterung’ ‘73.II.1’
          ‘73.II.1.SO-Erweiterung’ ‘73.II.2’ ‘73.II.2.NW-Erweiterung’
          ‘73.II.3’ ‘73.II.5’ ‘73.II.6’ ‘73.II.6.NW-Erweiterung’
          ‘73.II.7’ ‘73.II.8’ ‘73.II.8.Nord-Erweiterung’ ‘73.III.1’
          ‘73.III.2’ ‘73.III.3’ ‘73.J.19.VorTempelterasse’
          ‘73.Nordwestlich.Hause.5’ ‘75.G14’ ‘75.H13.R11’ ‘75.H13.R5’
          ‘75.H13.R7’ ‘75.H13.R9’ ‘75.H18.R5’ ‘75.I’ ‘75.I.1’ ‘75.I.2’
          ‘75.I.Westerweiterung 2’ ‘75.I.Westerweiterung 3’ ‘75.II’
          ‘75.II.2’ ‘75.II.5’ ‘75.III’ ‘75.IV’ ‘75.IX’
          ‘75.Nordwestlich.H13’ ‘75.Stadtmauer’ ‘75.Stadttor I/19’
          ‘75.Stadttor J/21’ ‘75.V’ ‘75.VI’ ‘75.VII’ ‘75.VIII’ ‘75.X’
          ‘75.XI’ ‘75.XII’ ‘75.XIII’ ‘75.XIV’ ‘76.H13.R14’ ‘76.H13.R3’
          ‘76.H13.R4’ ‘76.H19.R1’ ‘76.I.0’ ‘76.I.1’ ‘76.I.2’ ‘76.I.3’
          ‘76.I.4’ ‘76.I.5’ ‘76.I.6’ ‘76.II.1’ ‘76.II.3’ ‘76.II.4’
          ‘76.II.5’ ‘76.II.6’ ‘76.III.1’ ‘76.III.2’ ‘76.III.3’
          ‘76.IV.1’ ‘76.IV.2’ ‘76.Stadtmauer’ ‘77.H42’ ‘77.I.1’
          ‘77.I.2’ ‘77.I.3’ ‘77.II.1’ ‘77.II.2’ ‘77.II.3’ ‘77.II.4’
          ‘77.III.1’ ‘77.III.2’ ‘77.III.3’ ‘77.III.4’ ‘77.IV.1’
          ‘77.IV.2’ ‘77.IV.3’ ‘77.IV.4’ ‘77.V.2’ ‘77.V.3’ ‘77.V.4’
          ‘78.Stadttor J/21’

     ‘xmin’ a numeric vector

     ‘ymin’ a numeric vector

     ‘Zusatz1’ a factor with levels ‘5’ ‘a’ ‘b’ ‘c’ ‘Erweiterung’ ‘f’
          ‘g’ ‘Grab am Tor’ ‘h’ ‘I’ ‘k’ ‘Keramikzimmer’
          ‘NO-Erweiterung’ ‘Nord’ ‘Nord-Erweiterung’ ‘NW-Erweiterung’
          ‘R13’ ‘R15’ ‘R16’ ‘R17’ ‘R18’ ‘R19’ ‘R20’ ‘R3’ ‘R33’ ‘R34’
          ‘R35’ ‘R37’ ‘R39’ ‘R40’ ‘R7’ ‘R8’ ‘R9’ ‘Schnitt1’
          ‘SO-Erweiterung’ ‘Sud’ ‘Tor’ ‘VorTempelterasse’
          ‘Westerweiterung 2’ ‘Westerweiterung 3’

     ‘Zusatz2’ a numeric vector

     ‘Zusatz3’ a factor with levels ‘a’ ‘b’ ‘c’ ‘d’

_E_x_a_m_p_l_e_s:

     data(objet)
     ## maybe str(objet) ; plot(objet) ...
     
