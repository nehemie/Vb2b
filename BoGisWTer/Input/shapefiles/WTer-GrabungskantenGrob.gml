<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ WTer-GrabungskantenGrob.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>2838.271409512064</gml:X><gml:Y>3876.804839242144</gml:Y></gml:coord>
      <gml:coord><gml:X>3032.719604360563</gml:X><gml:Y>4099.038630445798</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                                
  <gml:featureMember>
    <ogr:WTer_GrabungskantenGrob fid="WTer_GrabungskantenGrob.0">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:4326"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3012.49456437064,3996.85704580661 3032.71960436056,3995.59822589023 3032.13215506625,3975.87671386686 3012.15887905961,3977.17749444712 3012.49456437064,3996.85704580661</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:Jahr>53</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Car>53.K.20</ogr:Car>
      <ogr:xmin>3012.158870000000206</ogr:xmin>
      <ogr:ymin>3975.876710000000003</ogr:ymin>
      <ogr:id xsi:nil="true"/>
      <ogr:Zusammense xsi:nil="true"/>
    </ogr:WTer_GrabungskantenGrob>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_GrabungskantenGrob fid="WTer_GrabungskantenGrob.1">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:4326"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3007.64000047573,3997.18437275448 3007.85890327088,3992.20406705046 3002.98038249776,3992.49165279582 3002.97932322848,3992.30098432479 2998.29464242295,3992.60058600421 2998.29240404305,3992.38570153383 2993.13351123769,3992.84728667957 2993.13877162433,3992.71434198902 2982.76212418854,3993.21287341103 2982.76220892899,3993.75518920103 2982.74624152393,3993.53802476794 2978.17838830086,3993.78794994576 2978.17233208747,3993.70409468344 2973.73850335412,3993.92859234083 2973.73852798073,3993.92910211502 2969.13630137773,3994.01277896235 2969.38886124229,3999.23234949654 2973.9388869654,3999.01713535302 2973.9834103666,3998.99820011342 2978.53308043591,3998.75550856423 2978.53714078072,3998.81141638895 2983.11128054993,3998.50273211004 2983.09589153158,3998.29343401604 2992.93305306051,3997.91342575941 2992.93634917792,3997.83012364886 2998.23504637566,3997.7748295437 2998.23681021623,3997.88536355223 3002.99778187486,3997.35325495509 3002.99854450992,3997.4317145418 3007.64000047573,3997.18437275448</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Car>56.J.20</ogr:Car>
      <ogr:xmin>2969.136300000000119</ogr:xmin>
      <ogr:ymin>3992.204060000000027</ogr:ymin>
      <ogr:id xsi:nil="true"/>
      <ogr:Zusammense xsi:nil="true"/>
    </ogr:WTer_GrabungskantenGrob>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_GrabungskantenGrob fid="WTer_GrabungskantenGrob.2">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:4326"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2983.08085357264,3998.48356004529 2978.76181092431,3998.78115696718 2974.10270865735,3999.11642735364 2974.34838972246,4004.20779956778 2974.57297987991,4008.86209430039 2979.25949934053,4008.63538760849 2983.5744053773,4008.4164209844 2983.57379087938,4008.40299847262 2988.33239471885,4008.19649343245 2993.41826459003,4007.95046847862 2993.1875205992,4003.25143836032 2992.92649652701,3997.93576373578 2987.91402574137,3998.20793409518 2983.0802074875,3998.47040386462 2983.08085357264,3998.48356004529</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>21</ogr:ArabischeZ>
      <ogr:Car>56.J.21</ogr:Car>
      <ogr:xmin>2974.102699999999913</ogr:xmin>
      <ogr:ymin>3997.935759999999846</ogr:ymin>
      <ogr:id>137</ogr:id>
      <ogr:Zusammense xsi:nil="true"/>
    </ogr:WTer_GrabungskantenGrob>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_GrabungskantenGrob fid="WTer_GrabungskantenGrob.3">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:4326"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3012.42530540852,4003.96045448236 3012.48340915604,4003.99877823073 3006.96455345213,4013.00726886206 3006.90544688508,4012.97058202734 3004.16966039261,4017.43623728433 3001.4692406886,4021.84416293221 2996.02425542172,4030.73207401306 2990.50333718479,4039.74393134037 2984.98456918878,4048.75227880509 2979.33742715502,4057.97017266466 2973.80926973073,4066.99384659944 2968.34207876624,4075.91800429183 2962.60469281909,4085.28320431266 2954.63222077046,4098.29676067502 2958.26157246881,4099.0386304458 2965.54666471953,4087.15830728217 2971.38849575147,4077.63161384602 2976.84494468606,4068.73339067497 2982.2818404311,4059.86705429444 2987.92914349312,4050.65759159025 2993.47624831262,4041.61152935656 2998.9746164859,4032.64494547225 3004.45139336032,4023.7135720697 3007.09729178684,4019.39871547168 3009.89897146397,4014.82981519977 3009.95807839019,4014.86650225742 3020.85041976464,3996.33697181943 3017.12854087626,3996.5686240324 3012.42530540852,4003.96045448236</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>Schnitt.B</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Car>56.Schnitt.B</ogr:Car>
      <ogr:xmin>2954.632219999999961</ogr:xmin>
      <ogr:ymin>3995.758190000000013</ogr:ymin>
      <ogr:id>131</ogr:id>
      <ogr:Zusammense xsi:nil="true"/>
    </ogr:WTer_GrabungskantenGrob>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_GrabungskantenGrob fid="WTer_GrabungskantenGrob.4">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:4326"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2991.70244596214,3952.63916395737 2993.39329768423,3956.41051241233 2996.72299083585,3954.76093965832 2995.05772593453,3951.06020867624 2992.86715180533,3946.19207896185 2990.84110614505,3941.68958212777 2988.66434809237,3936.85215598634 2986.48740895152,3932.0143274113 2984.33810531734,3927.23791343502 2982.22762103318,3922.54776799845 2980.12352160876,3917.87181168476 2976.76185094298,3919.31501778562 2978.89524590123,3924.07343372151 2981.0736079698,3928.93214623564 2983.20088697323,3933.67692088267 2985.40767444063,3938.59903462976 2987.48390365244,3943.22994562438 2989.46391648684,3947.64625146968 2991.70244596214,3952.63916395737</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>Schnitt.C</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Car>56.Schnitt.C</ogr:Car>
      <ogr:xmin>2976.761849999999868</ogr:xmin>
      <ogr:ymin>3917.871810000000096</ogr:ymin>
      <ogr:id>127</ogr:id>
      <ogr:Zusammense>1956 / Schnitt C</ogr:Zusammense>
    </ogr:WTer_GrabungskantenGrob>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_GrabungskantenGrob fid="WTer_GrabungskantenGrob.5">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:4326"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2980.84770054564,3963.10067516852 2985.93950618324,3962.92733710426 2988.38241333147,3960.98533262426 2990.70183433302,3959.023433113 2990.69705408948,3956.32259551636 2990.66649589347,3939.05721476551 2980.13436925969,3939.8964280431 2980.84770054564,3963.10067516852</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:Jahr>58</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Car>58.J.20</ogr:Car>
      <ogr:xmin>2980.134360000000015</ogr:xmin>
      <ogr:ymin>3939.057209999999941</ogr:ymin>
      <ogr:id xsi:nil="true"/>
      <ogr:Zusammense>1958</ogr:Zusammense>
    </ogr:WTer_GrabungskantenGrob>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_GrabungskantenGrob fid="WTer_GrabungskantenGrob.6">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:4326"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3011.36359335208,3947.78503285247 3020.93320252492,3942.09480134767 3030.60033616593,3941.66375738326 3030.15019055031,3906.58652626255 3019.25219816144,3907.33495287253 3019.01900958057,3917.51949147466 3015.05613177348,3917.90904017019 3015.55965974004,3927.81175684578 3010.60830140224,3928.73489145113 3011.36359335208,3947.78503285247</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2977.96290490392,3993.73195980063 3012.41307713445,3992.07985658479 3012.28672795743,3977.61906487086 2997.60049559957,3978.54219947621 2997.18088896077,3983.15787250297 2977.62721959288,3983.82924312504 2977.96290490392,3993.73195980063</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2990.46718274004,3957.89755284745 3002.39326635744,3957.70204327995 3002.17058877406,3953.47177810568 3010.84706520509,3953.09064509964 3010.29450169886,3927.59956068534 2990.63502539556,3929.78390804812 2990.46718274004,3957.89755284745</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J-K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Car>57.J-K.20</ogr:Car>
      <ogr:xmin>2977.627219999999852</ogr:xmin>
      <ogr:ymin>3906.586519999999837</ogr:ymin>
      <ogr:id xsi:nil="true"/>
      <ogr:Zusammense>1957</ogr:Zusammense>
    </ogr:WTer_GrabungskantenGrob>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_GrabungskantenGrob fid="WTer_GrabungskantenGrob.7">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:4326"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2977.96290490392,3993.56411714511 3012.41307713445,3992.07985658479 3012.28672795743,3977.45122221534 2997.60049559957,3978.37435682069 2997.18088896077,3982.99002984745 2977.62721959288,3983.66140046952 2977.96290490392,3993.56411714511</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2990.46718274004,3957.72971019193 3002.14285303001,3957.53830576095 3002.01447849045,3953.46004573377 3010.84395169685,3952.77916881539 3010.29450169886,3927.43171802982 2990.63502539555,3929.6160653926 2990.46718274004,3957.72971019193</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Car>57.J.20</ogr:Car>
      <ogr:xmin>2977.627219999999852</ogr:xmin>
      <ogr:ymin>3927.431709999999839</ogr:ymin>
      <ogr:id xsi:nil="true"/>
      <ogr:Zusammense xsi:nil="true"/>
    </ogr:WTer_GrabungskantenGrob>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_GrabungskantenGrob fid="WTer_GrabungskantenGrob.8">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:4326"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3011.36359335208,3947.61719019695 3021.02762805164,3942.02824923845 3030.62298158843,3941.72697020399 3030.15019055031,3906.41868360703 3019.25219816144,3907.16711021702 3019.01900958057,3917.35164881915 3015.05613177348,3917.74119751468 3015.55965974004,3927.64391419026 3010.60830140224,3928.56704879561 3011.36359335208,3947.61719019695</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Car>57.K.20</ogr:Car>
      <ogr:xmin>3010.608299999999872</ogr:xmin>
      <ogr:ymin>3906.418680000000222</ogr:ymin>
      <ogr:id xsi:nil="true"/>
      <ogr:Zusammense xsi:nil="true"/>
    </ogr:WTer_GrabungskantenGrob>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_GrabungskantenGrob fid="WTer_GrabungskantenGrob.9">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:4326"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2838.27140951206,3898.49391554514 2904.05968564597,3944.72190438926 2945.51434308633,3898.69109788806 2936.49564754169,3884.59011064349 2932.92862239513,3890.17304134577 2923.09268124968,3884.91129062098 2913.45557567638,3884.37883907648 2902.73457444544,3876.80483924214 2838.27140951206,3898.49391554514</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:Jahr>75</ogr:Jahr>
      <ogr:RomischeZa xsi:nil="true"/>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Car>75</ogr:Car>
      <ogr:xmin>2838.271409999999833</ogr:xmin>
      <ogr:ymin>3876.804830000000038</ogr:ymin>
      <ogr:id xsi:nil="true"/>
      <ogr:Zusammense xsi:nil="true"/>
    </ogr:WTer_GrabungskantenGrob>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_GrabungskantenGrob fid="WTer_GrabungskantenGrob.10">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:4326"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2862.60254973293,3889.66951337922 2932.42509442858,3951.60345326551 2959.9512899336,3923.07020182738 2964.3320561466,3926.45550291361 2984.45631763928,3912.83179984076 2970.75990960385,3896.38022037142 2930.91451052891,3942.03642190096 2925.87923086336,3935.49055833574 2949.61186153897,3912.15267406023 2943.40807717466,3907.07191033915 2921.34747916436,3929.6160653926 2868.98057064262,3882.95580715848 2862.60254973293,3889.66951337922</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Car>73</ogr:Car>
      <ogr:xmin>2862.602550000000065</ogr:xmin>
      <ogr:ymin>3882.955800000000181</ogr:ymin>
      <ogr:id xsi:nil="true"/>
      <ogr:Zusammense xsi:nil="true"/>
    </ogr:WTer_GrabungskantenGrob>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_GrabungskantenGrob fid="WTer_GrabungskantenGrob.11">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:4326"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2977.30202444781,3943.81975011584 2983.00867473544,3941.21818895531 2979.48397896955,3932.23860688507 2973.69340735417,3937.5256505339 2977.30202444781,3943.81975011584</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:Jahr>78</ogr:Jahr>
      <ogr:RomischeZa xsi:nil="true"/>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Car>78</ogr:Car>
      <ogr:xmin>2973.693400000000111</ogr:xmin>
      <ogr:ymin>3932.238600000000133</ogr:ymin>
      <ogr:id xsi:nil="true"/>
      <ogr:Zusammense xsi:nil="true"/>
    </ogr:WTer_GrabungskantenGrob>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_GrabungskantenGrob fid="WTer_GrabungskantenGrob.12">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:4326"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2976.12712585918,3898.16654781484 2982.50514676888,3922.83941817604 2993.24707672206,3941.46995293859 3018.08778973878,3930.22449501885 2998.28235638761,3884.57129271785 2976.12712585918,3898.16654781484</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:Jahr>77</ogr:Jahr>
      <ogr:RomischeZa xsi:nil="true"/>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Car>77</ogr:Car>
      <ogr:xmin>2976.127120000000104</ogr:xmin>
      <ogr:ymin>3884.571289999999863</ogr:ymin>
      <ogr:id xsi:nil="true"/>
      <ogr:Zusammense xsi:nil="true"/>
    </ogr:WTer_GrabungskantenGrob>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_GrabungskantenGrob fid="WTer_GrabungskantenGrob.13">
      <ogr:geometryProperty><gml:MultiPolygon srsName="EPSG:4326"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2939.69323316246,3945.40481457818 2971.34432960383,3968.53645739815 2980.71698935157,3961.2291445832 2982.10652046202,3943.88269111166 2972.54537871944,3917.01783916491 2939.69323316246,3945.40481457818</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ogr:geometryProperty>
      <ogr:Jahr>76</ogr:Jahr>
      <ogr:RomischeZa xsi:nil="true"/>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Car>76</ogr:Car>
      <ogr:xmin>2940.817219999999907</ogr:xmin>
      <ogr:ymin>3917.017829999999776</ogr:ymin>
      <ogr:id xsi:nil="true"/>
      <ogr:Zusammense xsi:nil="true"/>
    </ogr:WTer_GrabungskantenGrob>
  </gml:featureMember>
</ogr:FeatureCollection>
