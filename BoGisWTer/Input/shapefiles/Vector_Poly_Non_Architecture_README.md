 # Description of the data from the shapefile  
 
  This is a shapefile of polygons, mainly for ‘Schnittkante’, ‘Strasse’
  and ‘Weg’ 
 See ‘Art’ in this file 
 
 
_U_s_a_g_e:

     data("objet")
     
_F_o_r_m_a_t:

     A data frame with 68 observations on the following 9 variables.

     ‘Id’ a factor with levels ‘1’ ‘10’ ‘105399’ ‘11’ ‘12’ ‘13’ ‘14’
          ‘15’ ‘16’ ‘17’ ‘18’ ‘19’ ‘2’ ‘20’ ‘21’ ‘22’ ‘23’ ‘24’ ‘25’
          ‘26’ ‘27’ ‘28’ ‘29’ ‘3’ ‘4’ ‘43’ ‘44’ ‘45’ ‘46’ ‘5’ ‘6’ ‘7’
          ‘8’ ‘9’

     ‘Kennung’ a factor with levels ‘KNW’ ‘KNW-WTer’ ‘Komplex-1’
          ‘Komplex-2’ ‘ModerneStrasse’ ‘NWH-HaH-8a’
          ‘Schnittkante--1938-1957’ ‘Schnittkante--1955’
          ‘Schnittkante--1956’ ‘Schnittkante--1970-1978’
          ‘Schnittkante--1978’ ‘Schnittkante--KSW’ ‘Siedlung-BK-EBA’
          ‘Siedlungsnac-BK-EBA’ ‘Siedlungsnac-BKA-EBA’
          ‘Siedlungsnac-NWH-EBA’ ‘Siedlungsnac-WTER-EBA’ ‘Strasse’
          ‘Strasse-10’ ‘Strasse-11’ ‘Strasse-2’ ‘Strasse-3’ ‘Strasse-4’
          ‘Strasse-5’ ‘Strasse-6’ ‘Strasse-7’ ‘Strasse-8’ ‘Strasse-9’
          ‘Tempel-1’ ‘Tempel-1-ost’ ‘Tempelstrasse’ ‘WterStrasse’

     ‘Art’ a factor with levels ‘Schnittkante’ ‘Siedlungsnac’ ‘Strasse’
          ‘Weg’

     ‘Type’ a factor with levels ‘1938-1957’ ‘1955’ ‘1956’ ‘1970-1978’
          ‘1978’ ‘2010-2014’ ‘gepflastert’ ‘moderne’ ‘ungepflastert’

     ‘Name’ a factor with levels ‘BÃ¼yÃ¼kkale’ ‘BÃ¼yÃ¼kkaya’
          ‘BÃ¼ykkale’ ‘Komplex-1’ ‘Komplex-2’ ‘ModerneStrasse’
          ‘Nordwesthang’ ‘Schnittkante-BKA’ ‘Schnittkante-NOV’
          ‘Schnittkante-NWter’ ‘Schnittkante-WTER’ ‘Strasse’
          ‘Strasse-1’ ‘Strasse-2’ ‘Strasse-3’ ‘Tempel-1’ ‘UST’

     ‘Stand’ a factor with levels ‘nachgewiesen’ ‘vermutet’

     ‘Periode’ a factor with levels ‘EBA’ ‘LBA’ ‘MBA’ ‘Moderne’

     ‘Phase’ a factor with levels ‘1’ ‘2’ ‘3’

     ‘Lage’ a factor with levels ‘BK’ ‘BKA’ ‘BO’ ‘KNW’ ‘KSW’ ‘MTER’
          ‘NOV’ ‘NWH’ ‘NWTER’ ‘WTER’

_E_x_a_m_p_l_e_s:

     data(objet)
     ## maybe str(objet) ; plot(objet) ...
     
