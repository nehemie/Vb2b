<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation=""
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>2860.43927297827</gml:X><gml:Y>3338.82093106877</gml:Y></gml:coord>
      <gml:coord><gml:X>3359.67857782167</gml:X><gml:Y>4008.45948255092</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                                    
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.0">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2916.37773877324,3847.29965820909 2927.07227683121,3857.33872457963 2944.18353772397,3843.12533851549 2932.07456076155,3831.32684814186 2924.82987369003,3839.29600392054 2922.89795713762,3841.469410042 2916.37773877324,3847.29965820909</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>1</ogr:id>
      <ogr:Hausnummer>1</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>345.06</ogr:qm2>
      <ogr:perim>76.04</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.1">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2916.37773877324,3847.29965820909 2927.07227683121,3857.33872457963 2944.18353772397,3843.12533851549 2932.07456076155,3831.32684814186 2924.82987369003,3839.29600392054 2922.89795713762,3841.469410042 2916.37773877324,3847.29965820909</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>2</ogr:id>
      <ogr:Hausnummer>1</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>345.06</ogr:qm2>
      <ogr:perim>76.04</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.2">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2931.04002741405,3851.60252399118 2926.21144435296,3846.98759504784 2921.91699658625,3850.43810904946 2926.66363278466,3855.10999498524 2931.04002741405,3851.60252399118</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>3</ogr:id>
      <ogr:Hausnummer>1</ogr:Hausnummer>
      <ogr:Raumnummer>10-10a-11-12</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>36.79</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.3">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2931.98010553214,3850.98292704972 2936.43268937073,3847.49167480123 2929.40557341329,3840.5138752801 2925.18590458778,3844.03916822293 2927.39109485217,3845.96382164939 2931.98010553214,3850.98292704972</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>4</ogr:id>
      <ogr:Hausnummer>1</ogr:Hausnummer>
      <ogr:Raumnummer>7-7a-8-8a</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>53.31</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.4">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2941.9898009486,3843.20591716372 2938.76362377061,3839.57379716202 2934.31961812147,3843.35547504614 2937.53511259357,3846.67779657711 2941.9898009486,3843.20591716372</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>5</ogr:id>
      <ogr:Hausnummer>1</ogr:Hausnummer>
      <ogr:Raumnummer>1-2-3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>27.15</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.5">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2925.43144033016,3845.96002390575 2923.10066722834,3843.49606376955 2918.76559834947,3847.59650928342 2921.05537916153,3849.70011440938 2925.43144033016,3845.96002390575</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>6</ogr:id>
      <ogr:Hausnummer>1</ogr:Hausnummer>
      <ogr:Raumnummer>13</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>19</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.6">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2927.41726192986,3838.64053223312 2923.65674822267,3842.25855277046 2924.62288263084,3843.26333255495 2928.90069785403,3839.88247858824 2927.41726192986,3838.64053223312</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>7</ogr:id>
      <ogr:Hausnummer>1</ogr:Hausnummer>
      <ogr:Raumnummer>9</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>8.8</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.7">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2932.0121536498,3833.14280821782 2928.1236487068,3837.53340033753 2930.1863580791,3839.47891016431 2934.55463765099,3835.74938845434 2932.0121536498,3833.14280821782</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>8</ogr:id>
      <ogr:Hausnummer>1</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>18.75</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.8">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2933.68933847412,3842.56495481048 2938.03719977027,3838.80464233812 2935.62290823973,3836.33693727814 2930.9972965905,3840.3429519859 2933.68933847412,3842.56495481048</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>9</ogr:id>
      <ogr:Hausnummer>1</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>20.44</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.9">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2925.43144033016,3845.96002390575 2923.10066722834,3843.49606376955 2918.76559834947,3847.59650928342 2921.05537916153,3849.70011440938 2925.43144033016,3845.96002390575</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>10</ogr:id>
      <ogr:Hausnummer>1</ogr:Hausnummer>
      <ogr:Raumnummer>13</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>19</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.10">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2925.60458347486,3851.67374768106 2923.34040389024,3849.2231062483 2921.91699658625,3850.43810904946 2924.25647968724,3852.78152349974 2925.60458347486,3851.67374768106</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>11</ogr:id>
      <ogr:Hausnummer>1</ogr:Hausnummer>
      <ogr:Raumnummer>12</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>6</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.11">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2928.00986642714,3854.08433939251 2926.52357401215,3852.5394634046 2925.17441633994,3853.67059716274 2926.66363278466,3855.10999498524 2928.00986642714,3854.08433939251</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>12</ogr:id>
      <ogr:Hausnummer>1</ogr:Hausnummer>
      <ogr:Raumnummer>11</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>3.62</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.12">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2928.68120396926,3849.28969976549 2926.21144435296,3846.98759504784 2924.4192188688,3848.38402793164 2926.81658548781,3850.75475714378 2928.68120396926,3849.28969976549</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>13</ogr:id>
      <ogr:Hausnummer>1</ogr:Hausnummer>
      <ogr:Raumnummer>10a</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>7.75</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.13">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2931.04002741405,3851.60252399118 2929.47205496566,3850.11295016522 2927.54678258888,3851.60252399118 2929.04236141312,3853.25834340372 2931.04002741405,3851.60252399118</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>14</ogr:id>
      <ogr:Hausnummer>1</ogr:Hausnummer>
      <ogr:Raumnummer>10</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>5.49</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.14">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2933.66683419668,3849.65465568335 2928.90348623658,3844.79764034093 2927.25936638859,3846.06539540445 2931.98010553214,3850.98292704972 2933.66683419668,3849.65465568335</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>15</ogr:id>
      <ogr:Hausnummer>1</ogr:Hausnummer>
      <ogr:Raumnummer>7</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>14.29</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.15">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2936.43268937073,3847.49167480123 2931.83054764972,3842.56495481047 2929.82219894289,3844.12462987003 2934.54395494511,3849.11345351943 2936.43268937073,3847.49167480123</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>16</ogr:id>
      <ogr:Hausnummer>1</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>17.08</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.16">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2927.81796722619,3841.87182774202 2928.79926645087,3843.08650860692 2930.75892170574,3841.59148917778 2929.40557341329,3840.5138752801 2927.81796722619,3841.87182774202</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>17</ogr:id>
      <ogr:Hausnummer>1</ogr:Hausnummer>
      <ogr:Raumnummer>8</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>3.7</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.17">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2925.18590458778,3844.03916822293 2926.31827141184,3845.20358316465 2927.80076394653,3843.96990056586 2926.83104129443,3842.72519539878 2925.18590458778,3844.03916822293</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>18</ogr:id>
      <ogr:Hausnummer>1</ogr:Hausnummer>
      <ogr:Raumnummer>8a</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>3.22</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.18">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2927.41726192986,3838.64053223312 2923.65674822267,3842.25855277046 2924.62288263084,3843.26333255495 2928.90069785403,3839.88247858824 2927.41726192986,3838.64053223312</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>19</ogr:id>
      <ogr:Hausnummer>1</ogr:Hausnummer>
      <ogr:Raumnummer>9</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>8.8</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.19">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2932.0121536498,3833.14280821782 2928.1236487068,3837.53340033753 2930.1863580791,3839.47891016431 2934.55463765099,3835.74938845434 2932.0121536498,3833.14280821782</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>20</ogr:id>
      <ogr:Hausnummer>1</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>18.75</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.20">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2933.68933847412,3842.56495481048 2938.03719977027,3838.80464233812 2935.62290823973,3836.33693727814 2930.9972965905,3840.3429519859 2933.68933847412,3842.56495481048</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>21</ogr:id>
      <ogr:Hausnummer>1</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>20.44</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.21">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2937.16121788751,3841.03732786858 2934.31961812147,3843.35547504614 2935.62470635499,3844.3335511348 2938.14402682915,3842.24447363385 2937.16121788751,3841.03732786858</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>22</ogr:id>
      <ogr:Hausnummer>1</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>5.45</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.22">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2939.09478765312,3843.1311382225 2936.37140285394,3845.15608399692 2937.53511259357,3846.67779657711 2940.36602965372,3844.5733035173 2939.09478765312,3843.1311382225</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>23</ogr:id>
      <ogr:Hausnummer>1</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>6.63</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.23">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2941.14362388715,3844.00724002651 2941.9898009486,3843.20591716372 2938.76362377061,3839.57379716202 2937.86627647608,3840.46046175067 2941.14362388715,3844.00724002651</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>24</ogr:id>
      <ogr:Hausnummer>1</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>5.87</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.24">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2941.38724705002,3852.11326489455 2944.74356031664,3857.70712033891 2947.24747656316,3856.53507443628 2943.27850293835,3850.91458158504 2941.38724705002,3852.11326489455</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>25</ogr:id>
      <ogr:Hausnummer>2</ogr:Hausnummer>
      <ogr:Raumnummer>8</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>16.66</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.25">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2937.00370683083,3855.55679632883 2939.65581560296,3859.8913877029 2943.54227047918,3858.18320097386 2940.77458669183,3853.15212376278 2937.00370683083,3855.55679632883</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>26</ogr:id>
      <ogr:Hausnummer>2</ogr:Hausnummer>
      <ogr:Raumnummer>7</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>23.5</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.26">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2935.59404936781,3864.24445071024 2936.91216269453,3866.20445313296 2942.8024765812,3862.04498635024 2941.44052186378,3860.07784955104 2935.59404936781,3864.24445071024</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>27</ogr:id>
      <ogr:Hausnummer>2</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>17.1</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.27">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2931.65441289133,3858.51450770556 2934.94099458557,3863.32761319014 2938.75014376911,3860.58396028172 2935.82002901254,3856.2687003675 2931.65441289133,3858.51450770556</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>28</ogr:id>
      <ogr:Hausnummer>2</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>25.95</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.28">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2933.39602498665,3867.05685015305 2934.11523497235,3867.93588458002 2936.13967789508,3866.79047608427 2935.36719309562,3865.75161721603 2933.39602498665,3867.05685015305</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>29</ogr:id>
      <ogr:Hausnummer>2</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>2.83</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.29">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2930.8121965195,3863.40752541078 2932.86327684909,3866.28436535359 2934.69434128262,3865.08879214404 2932.67681500095,3862.12892988064 2930.8121965195,3863.40752541078</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>30</ogr:id>
      <ogr:Hausnummer>2</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>7.91</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.30">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2928.65456656238,3860.47741065421 2930.25281097506,3862.6616780182 2932.09079204963,3861.3298076743 2930.79324883947,3859.31920198352 2928.65456656238,3860.47741065421</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>31</ogr:id>
      <ogr:Hausnummer>2</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>5.97</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.31">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2939.08881004244,3865.81773213843 2940.64139965744,3868.17562124192 2944.77019772351,3865.59179277477 2943.50481503217,3862.88067481763 2939.08881004244,3865.81773213843</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>32</ogr:id>
      <ogr:Hausnummer>2</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>14.72</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.32">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2939.08881004244,3865.81773213843 2940.64139965744,3868.17562124192 2944.77019772351,3865.59179277477 2943.50481503217,3862.88067481763 2939.08881004244,3865.81773213843</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>33</ogr:id>
      <ogr:Hausnummer>2</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>14.72</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.33">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2941.38724705002,3852.11326489455 2944.74356031664,3857.70712033891 2947.24747656316,3856.53507443628 2943.27850293835,3850.91458158504 2941.38724705002,3852.11326489455</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>34</ogr:id>
      <ogr:Hausnummer>2</ogr:Hausnummer>
      <ogr:Raumnummer>8</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>16.66</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.34">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2937.00370683083,3855.55679632883 2939.65581560296,3859.8913877029 2943.54227047918,3858.18320097386 2940.77458669183,3853.15212376278 2937.00370683083,3855.55679632883</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>35</ogr:id>
      <ogr:Hausnummer>2</ogr:Hausnummer>
      <ogr:Raumnummer>7</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>23.5</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.35">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2935.59404936781,3864.24445071024 2936.91216269453,3866.20445313296 2942.8024765812,3862.04498635024 2941.44052186378,3860.07784955104 2935.59404936781,3864.24445071024</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>36</ogr:id>
      <ogr:Hausnummer>2</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>17.1</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.36">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2931.65441289133,3858.51450770556 2934.94099458557,3863.32761319014 2938.75014376911,3860.58396028172 2935.82002901254,3856.2687003675 2931.65441289133,3858.51450770556</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>37</ogr:id>
      <ogr:Hausnummer>2</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>25.95</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.37">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2933.39602498665,3867.05685015305 2934.11523497235,3867.93588458002 2936.13967789508,3866.79047608427 2935.36719309562,3865.75161721603 2933.39602498665,3867.05685015305</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>38</ogr:id>
      <ogr:Hausnummer>2</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>2.83</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.38">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2930.8121965195,3863.40752541078 2932.86327684909,3866.28436535359 2934.69434128262,3865.08879214404 2932.67681500095,3862.12892988064 2930.8121965195,3863.40752541078</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>39</ogr:id>
      <ogr:Hausnummer>2</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>7.91</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.39">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2928.65456656238,3860.47741065421 2930.25281097506,3862.6616780182 2932.09079204963,3861.3298076743 2930.79324883947,3859.31920198352 2928.65456656238,3860.47741065421</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>40</ogr:id>
      <ogr:Hausnummer>2</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>5.97</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.40">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2927.40260843912,3860.23767399231 2933.28362105039,3868.78738140197 2938.6435941416,3866.20445313296 2940.33546363242,3869.15326200048 2946.34159678149,3866.25606426 2942.45119293315,3859.63851522122 2948.62020689234,3857.11735699065 2943.54284889223,3849.70927098905 2927.40260843912,3860.23767399231</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>41</ogr:id>
      <ogr:Hausnummer>2</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>219.08</ogr:qm2>
      <ogr:perim>68.8</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.41">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2927.40260843912,3860.23767399231 2933.74231127606,3869.45421677206 2935.18073124747,3868.97474344826 2936.61915121888,3870.94591155722 2946.34159678149,3866.25606426 2942.19096434362,3859.75525380158 2948.2863354314,3856.90799813257 2943.68750953024,3849.61490774211 2927.40260843912,3860.23767399231</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>42</ogr:id>
      <ogr:Hausnummer>2</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>232.9</ogr:qm2>
      <ogr:perim>68.26</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.42">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2957.34241599936,3845.91304742138 2954.80338448545,3847.90865632507 2957.4496033974,3850.26196511653 2959.52732113387,3848.18424738006 2957.34241599936,3845.91304742138</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>43</ogr:id>
      <ogr:Hausnummer>3</ogr:Hausnummer>
      <ogr:Raumnummer>7</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>10.26</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.43">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2954.31374678285,3842.78351849924 2952.00625405292,3845.14727893045 2954.0666527239,3847.49167480123 2956.27755749477,3845.38731965788 2954.31374678285,3842.78351849924</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>44</ogr:id>
      <ogr:Hausnummer>3</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>10.08</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.44">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2951.53365626709,3842.66695837301 2952.24137080288,3843.70594354257 2953.66709162074,3842.21746823941 2952.73478238001,3841.15197196429 2951.53365626709,3842.66695837301</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>45</ogr:id>
      <ogr:Hausnummer>3</ogr:Hausnummer>
      <ogr:Raumnummer>3b</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>2.61</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.45">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2954.24359352032,3848.680837059 2952.78523557522,3849.99953855533 2954.59940086146,3852.55278210803 2956.62384378418,3850.82135066097 2954.24359352032,3848.680837059</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>46</ogr:id>
      <ogr:Hausnummer>3</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>7.29</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.46">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2951.2963624086,3845.97334260919 2950.49724020226,3846.61264037426 2952.41513349747,3849.14319402766 2953.40071755196,3848.18424738006 2951.2963624086,3845.97334260919</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>47</ogr:id>
      <ogr:Hausnummer>3</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>3.72</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.47">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2950.0980845198,3844.38340298615 2949.29794876171,3845.09430818222 2950.04702539,3846.14796210441 2950.87016389856,3845.28077003037 2950.0980845198,3844.38340298615</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>48</ogr:id>
      <ogr:Hausnummer>3</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>1.39</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.48">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2944.29343609768,3849.43593573379 2948.97693882612,3855.39850959167 2953.6616016551,3852.49793492629 2948.59792888371,3845.95253116706 2944.29343609768,3849.43593573379</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>49</ogr:id>
      <ogr:Hausnummer>3</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>43.67</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.49">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2936.27286492946,3868.33544568319 2935.79339160566,3868.54854493821 2937.01320929185,3870.75582849874 2937.63530746848,3870.45574500293 2936.27286492946,3868.33544568319</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>50</ogr:id>
      <ogr:Hausnummer>2</ogr:Hausnummer>
      <ogr:Raumnummer>3b</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>1.52</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.50">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2937.71128490087,3867.4830486631 2938.7945903265,3869.89653800677 2939.25652344423,3869.67371384491 2938.27067044531,3867.26994940807 2937.71128490087,3867.4830486631</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>51</ogr:id>
      <ogr:Hausnummer>2</ogr:Hausnummer>
      <ogr:Raumnummer>3a</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>1.46</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.51">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2964.45524140629,3853.7381467141 2959.47404632012,3858.02676922144 2961.09892813967,3860.29094880606 2967.17225690783,3857.6005707114 2964.45524140629,3853.7381467141</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>63</ogr:id>
      <ogr:Hausnummer>4</ogr:Hausnummer>
      <ogr:Raumnummer>8</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>24.51</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.52">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2960.32644334021,3848.9167760692 2955.58498491594,3853.04557413528 2958.59501189315,3857.04118516696 2963.65611919995,3852.72592525274 2960.32644334021,3848.9167760692</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>64</ogr:id>
      <ogr:Hausnummer>4</ogr:Hausnummer>
      <ogr:Raumnummer>7</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>32.51</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.53">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2958.2639072354,3861.81923163497 2960.40635556084,3860.71714731611 2958.94129818256,3858.74597920715 2956.78294264162,3860.11425559003 2958.2639072354,3861.81923163497</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>65</ogr:id>
      <ogr:Hausnummer>4</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>5.77</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.54">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2956.30419490165,3859.0389906828 2958.00898894183,3857.73375774579 2954.97232455775,3853.63159708659 2953.37408014508,3855.20320409238 2956.30419490165,3859.0389906828</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>66</ogr:id>
      <ogr:Hausnummer>4</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>10.85</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.55">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2952.09548461494,3859.94466251665 2954.27975197893,3858.34641810398 2952.54832053187,3855.97568889184 2950.79025167792,3857.09445998072 2952.09548461494,3859.94466251665</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>67</ogr:id>
      <ogr:Hausnummer>4</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>7.21</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.56">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2954.94568715087,3859.22545253095 2952.65487015938,3860.82369694362 2953.90682828264,3863.8603613277 2957.40498947845,3862.24849871174 2954.94568715087,3859.22545253095</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>68</ogr:id>
      <ogr:Hausnummer>4</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>11.75</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.57">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2947.09217020296,3865.95122646596 2952.68150756625,3863.24770096951 2951.26972500172,3860.77042212987 2945.88896881239,3863.56734985205 2947.09217020296,3865.95122646596</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>69</ogr:id>
      <ogr:Hausnummer>4</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>16.93</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.58">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2945.19639623356,3862.28875432191 2950.87016389856,3859.5184640066 2949.83130503032,3857.38747145637 2943.81125107591,3860.1311243648 2945.19639623356,3862.28875432191</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>70</ogr:id>
      <ogr:Hausnummer>4</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>15.88</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.59">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2946.86087014742,3866.93950852146 2953.14410998949,3864.19519136378 2953.59653701886,3864.7046470103 2968.90368835489,3858.10668144208 2960.19780071129,3848.22596598366 2950.41732798163,3856.18878814687 2942.78653899779,3860.14059819748 2946.86087014742,3866.93950852146</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>71</ogr:id>
      <ogr:Hausnummer>4</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>222.65</ogr:qm2>
      <ogr:perim>66.33</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.60">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2951.89302633023,3864.74162448397 2946.86087014742,3866.93950852146 2946.34159678149,3866.25606426 2937.66643929741,3870.4407278426 2947.10097082533,3887.14145493899 2948.11319228669,3886.60870680143 2948.11319228669,3886.60870680143 2948.6463407376,3886.45866227305 2953.97342179983,3897.53004362137 2954.1332462411,3897.63659324888 2954.45289512363,3898.00951694517 2954.45289512363,3898.00951694517 2954.66599437866,3898.00951694517 2954.66599437866,3898.00951694517 2956.4240632326,3900.30033393667 2960.63277351931,3899.39466210282 2959.6738268717,3897.15711992508 2961.80481942193,3896.78419622879 2956.69043730138,3875.793919609 2955.57956643288,3875.75661779314 2952.52090628704,3864.46738670509 2951.89302633023,3864.74162448397</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>73</ogr:id>
      <ogr:Hausnummer>5</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>402.85</ogr:qm2>
      <ogr:perim>96.67</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.61">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2958.79479244473,3897.10384511132 2956.1732253027,3898.00390134385 2956.79698692889,3899.23483766156 2959.40745280292,3898.702089524 2958.79479244473,3897.10384511132</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>74</ogr:id>
      <ogr:Hausnummer>5</ogr:Hausnummer>
      <ogr:Raumnummer>21</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>4.14</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.62">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2958.44850615532,3887.4877412284 2957.1432732183,3887.94057714533 2959.51400243043,3896.14489846372 2960.20657500926,3896.09162364997 2958.44850615532,3887.4877412284</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>75</ogr:id>
      <ogr:Hausnummer>5</ogr:Hausnummer>
      <ogr:Raumnummer>20</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>8.92</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.63">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2957.67602135586,3892.97504704525 2954.14202232879,3894.25138601069 2955.51839139875,3896.86410844942 2958.58169318971,3896.30472290499 2957.67602135586,3892.97504704525</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>76</ogr:id>
      <ogr:Hausnummer>5</ogr:Hausnummer>
      <ogr:Raumnummer>19</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>10.85</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.64">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2956.85026174264,3890.41785598497 2952.88128811784,3892.17592483891 2953.62713551042,3893.6409822172 2957.30309765957,3892.20256224579 2956.85026174264,3890.41785598497</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>77</ogr:id>
      <ogr:Hausnummer>5</ogr:Hausnummer>
      <ogr:Raumnummer>18</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>7.17</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.65">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2955.49175399187,3885.96940903636 2950.85684519512,3888.23358862098 2952.3219025734,3891.27025300506 2956.53061286011,3889.538821558 2955.49175399187,3885.96940903636</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>78</ogr:id>
      <ogr:Hausnummer>5</ogr:Hausnummer>
      <ogr:Raumnummer>17</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>17.11</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.66">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2957.91575801776,3884.93055016812 2956.21096397757,3885.51657311944 2956.69043730138,3887.11481753211 2958.26204430717,3886.5287945808 2957.91575801776,3884.93055016812</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>79</ogr:id>
      <ogr:Hausnummer>5</ogr:Hausnummer>
      <ogr:Raumnummer>16</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>2.86</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.67">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2950.45728409195,3885.06373720251 2949.39178781683,3885.75630978134 2950.32409705756,3887.46110382152 2951.41623073955,3887.0082679046 2950.45728409195,3885.06373720251</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>80</ogr:id>
      <ogr:Hausnummer>5</ogr:Hausnummer>
      <ogr:Raumnummer>15</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>2.51</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.68">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2952.02889109774,3883.91832870676 2951.04330704326,3884.4777142512 2952.18871553901,3886.60870680143 2953.14766218662,3886.20914569826 2952.02889109774,3883.91832870676</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>81</ogr:id>
      <ogr:Hausnummer>5</ogr:Hausnummer>
      <ogr:Raumnummer>14</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>2.69</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.69">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2954.10660883422,3882.55982095599 2952.5616392353,3883.49213019672 2953.68041032417,3885.83622200197 2955.25201732997,3885.1969242369 2954.10660883422,3882.55982095599</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>82</ogr:id>
      <ogr:Hausnummer>5</ogr:Hausnummer>
      <ogr:Raumnummer>13</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>4.77</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.70">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2956.85026174264,3881.04148876395 2954.95900585431,3882.10698503907 2955.9445899088,3884.66417609934 2957.75593357649,3884.07815314803 2956.85026174264,3881.04148876395</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>83</ogr:id>
      <ogr:Hausnummer>5</ogr:Hausnummer>
      <ogr:Raumnummer>12</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>5.96</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.71">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2948.03328006606,3882.1868972597 2946.49741155179,3882.9006105332 2947.84580275635,3885.13318557713 2949.3118755962,3884.34452721681 2948.03328006606,3882.1868972597</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>84</ogr:id>
      <ogr:Hausnummer>5</ogr:Hausnummer>
      <ogr:Raumnummer>11</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>4.28</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.72">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2952.77473849032,3879.44324435128 2948.56602820362,3881.7074239359 2950.11099780253,3883.67859204486 2953.8935095792,3881.92052319092 2952.77473849032,3879.44324435128</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>85</ogr:id>
      <ogr:Hausnummer>5</ogr:Hausnummer>
      <ogr:Raumnummer>10</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>11.57</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.73">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2955.86467768816,3877.8449999386 2953.46731106915,3878.91049621372 2954.55944475114,3881.14803839146 2956.61052508074,3880.08254211635 2955.86467768816,3877.8449999386</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>86</ogr:id>
      <ogr:Hausnummer>5</ogr:Hausnummer>
      <ogr:Raumnummer>9</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>5.96</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.74">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2948.2197419142,3877.8449999386 2947.04769601158,3878.3244732624 2948.08655487981,3880.48210321951 2949.15205115493,3880.05590470947 2948.2197419142,3877.8449999386</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>87</ogr:id>
      <ogr:Hausnummer>5</ogr:Hausnummer>
      <ogr:Raumnummer>8</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>2.89</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.75">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2951.17649407765,3876.6463166291 2949.15205115493,3877.52535105607 2949.95117336127,3879.44324435128 2952.0821659115,3878.40438548304 2951.17649407765,3876.6463166291</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>88</ogr:id>
      <ogr:Hausnummer>5</ogr:Hausnummer>
      <ogr:Raumnummer>7</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>4.64</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.76">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2947.2607952666,3874.91488518203 2945.76910048144,3875.47427072647 2946.65358246845,3877.11288979209 2948.05991747294,3876.69959144285 2947.2607952666,3874.91488518203</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>89</ogr:id>
      <ogr:Hausnummer>5</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>2.89</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.77">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2949.5516122581,3873.23672854872 2948.00664265918,3874.14240038257 2949.09877634117,3876.32666774656 2950.69702075385,3875.63409516774 2949.5516122581,3873.23672854872</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>90</ogr:id>
      <ogr:Hausnummer>5</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>4.49</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.78">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2943.49722149349,3868.92970909856 2940.12197022332,3871.07909859162 2946.00883714334,3881.44104986712 2947.39398230099,3881.01485135707 2944.11758125501,3874.91488518203 2946.57654159222,3873.62196059116 2943.49722149349,3868.92970909856</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>91</ogr:id>
      <ogr:Hausnummer>5</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>32.86</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.79">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2945.4494515989,3867.80269754563 2944.44501325942,3868.03880747467 2947.68699377665,3873.10354151434 2948.32629154171,3872.57079337678 2945.4494515989,3867.80269754563</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>92</ogr:id>
      <ogr:Hausnummer>5</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>5.22</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.80">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2952.69482626969,3871.05246118474 2949.87126114063,3871.95813301859 2953.06774996598,3877.95154956611 2954.26643327549,3877.33888920792 2952.69482626969,3871.05246118474</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>93</ogr:id>
      <ogr:Hausnummer>5</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>14.16</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.81">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2946.99442119782,3867.9625219869 2948.96558930678,3871.18564821913 2952.77473849032,3869.72059084084 2951.62932999457,3865.83152943667 2946.99442119782,3867.9625219869</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>94</ogr:id>
      <ogr:Hausnummer>5</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>17.82</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.82">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2917.39337539103,3877.69431944436 2913.73869700883,3872.22461319854 2912.3869018134,3873.13346054582 2915.87366409307,3878.72079111053 2917.39337539103,3877.69431944436</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>95</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>15</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>11.39</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.83">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2921.08600722907,3878.06758186842 2920.29948997837,3877.12109500741 2916.56686573775,3879.82724758185 2917.22007497986,3880.77373444287 2921.08600722907,3878.06758186842</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>96</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>14</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>5.54</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.84">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2919.766257944,3876.13461574381 2916.18027251283,3870.72231059492 2914.81258433296,3871.5832190474 2918.36652385377,3877.04111020225 2919.766257944,3876.13461574381</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>97</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>13</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>10.68</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.85">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2920.33796667105,3869.7055104256 2919.55437619278,3868.30922634464 2917.2217999845,3869.93213404021 2918.04496427587,3871.25381762466 2920.33796667105,3869.7055104256</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>98</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>12</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>4.42</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.86">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2923.93879861298,3875.32143689139 2921.17932283509,3870.86894940437 2918.93974829072,3872.38866070234 2921.95250928493,3876.85447899022 2923.93879861298,3875.32143689139</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>99</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>11</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>13.83</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.87">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2925.54685979779,3868.6689486731 2923.52125659812,3865.83011597427 2920.71274480501,3867.52291838867 2922.5523953236,3870.64232578976 2925.54685979779,3868.6689486731</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>100</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>10</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>12.19</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.88">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2928.3246320957,3866.86970914656 2926.35167356852,3863.73697094461 2924.53868465165,3865.08338183141 2926.64495118742,3868.05615042304 2928.3246320957,3866.86970914656</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>101</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>9</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>7.91</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.89">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2927.95316671102,3875.26529818776 2924.57867705422,3871.02891901468 2923.69884419751,3871.89542107054 2926.4794529164,3876.20875320164 2927.95316671102,3875.26529818776</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>102</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>8</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>7.82</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.90">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2933.5352647961,3872.20388782293 2930.95059057026,3869.96966890658 2929.27660791272,3871.16191308117 2928.61006786976,3870.26874942359 2930.00431300398,3869.02929888578 2929.37776536359,3868.09614282562 2925.64514112297,3870.40237137429 2928.97393848996,3874.74692317075 2933.5352647961,3872.20388782293</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>103</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>7</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>23.4</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.91">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2930.26999076542,3880.2605036782 2928.1713860013,3877.24009930853 2926.53663843044,3878.12757047229 2928.57037972143,3881.12816058256 2930.26999076542,3880.2605036782</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>104</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>6.83</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.92">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2933.24297469398,3876.326968925 2932.00722173061,3874.6603235119 2929.28278340747,3876.39456636057 2930.37590907793,3877.78096964994 2933.24297469398,3876.326968925</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>105</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>6.12</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.93">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2935.399143528,3876.94826631187 2937.89943571044,3875.66508852418 2935.90819143457,3872.50197250964 2933.34867766957,3873.78172939214 2935.399143528,3876.94826631187</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>106</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>10.6</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.94">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2932.52396391759,3884.26330724871 2930.55279568067,3881.42642323207 2929.24745611808,3882.35534890871 2931.325413123,3885.21197243762 2932.52396391759,3884.26330724871</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>107</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>5.46</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.95">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2937.22224745597,3881.18904411619 2934.24603472274,3877.29691417848 2930.85745844963,3879.09758005435 2933.85855490214,3883.32271590895 2937.22224745597,3881.18904411619</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>108</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>19.58</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.96">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2940.18873173431,3879.1903994049 2938.41979002142,3876.61786726754 2936.13376164153,3877.83578965033 2938.0313946771,3880.33945321674 2940.18873173431,3879.1903994049</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>109</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>7.81</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.97">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2924.63759386104,3876.30587720825 2922.80835316337,3877.87419502097 2923.28119753528,3878.4653623001 2925.23865277673,3877.0115108312 2924.63759386104,3876.30587720825</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>110</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>21</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>2.04</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.98">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2921.88908890398,3878.64167721958 2920.22777552475,3879.80097865806 2926.80325444763,3883.60652962548 2930.96246431575,3887.7657394936 2931.19973146323,3887.50431704267 2925.03660184134,3878.91966970749 2923.28392302076,3880.26049860978 2921.88908890398,3878.64167721958</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>111</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>20</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>19.65</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.99">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2914.05853158209,3878.73046260738 2913.31415032807,3879.37400035264 2915.38042446127,3881.93351411763 2916.04696450424,3881.4536052867 2914.05853158209,3878.73046260738</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>113</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>18</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>3</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.100">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2913.26082712463,3877.69431944436 2912.5963260284,3876.86577084434 2911.60780781807,3877.58767303748 2912.19436305588,3878.37419028818 2913.26082712463,3877.69431944436</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>114</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>17</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>1.27</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.101">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2912.08771664901,3875.53472970514 2911.06877288559,3874.09317356033 2909.28824846855,3875.28144448881 2910.44018629846,3876.64150011857 2912.08771664901,3875.53472970514</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>115</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>16</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>3.65</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.102">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2940.18873173431,3879.1903994049 2938.41979002142,3876.61786726754 2936.13376164153,3877.83578965033 2938.0313946771,3880.33945321674 2940.18873173431,3879.1903994049</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>116</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>7.81</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.103">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2937.22224745597,3881.18904411619 2934.24603472274,3877.29691417848 2930.85745844963,3879.09758005435 2933.85855490214,3883.32271590895 2937.22224745597,3881.18904411619</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>117</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>19.58</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.104">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2932.52396391759,3884.26330724871 2930.55279568067,3881.42642323207 2929.24745611808,3882.35534890871 2931.325413123,3885.21197243762 2932.52396391759,3884.26330724871</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>118</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>5.46</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.105">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2935.399143528,3876.94826631187 2937.89943571044,3875.66508852418 2935.90819143457,3872.50197250964 2933.34867766957,3873.78172939214 2935.399143528,3876.94826631187</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>119</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>10.6</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.106">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2933.24297469398,3876.326968925 2932.00722173061,3874.6603235119 2929.28278340747,3876.39456636057 2930.37590907793,3877.78096964994 2933.24297469398,3876.326968925</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>120</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>6.12</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.107">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2930.26999076542,3880.2605036782 2928.1713860013,3877.24009930853 2926.53663843044,3878.12757047229 2928.57037972143,3881.12816058256 2930.26999076542,3880.2605036782</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>121</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>6.83</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.108">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2933.5352647961,3872.20388782293 2930.95059057026,3869.96966890658 2929.27660791272,3871.16191308117 2928.61006786976,3870.26874942359 2930.00431300398,3869.02929888578 2929.37776536359,3868.09614282562 2925.64514112297,3870.40237137429 2928.97393848996,3874.74692317075 2933.5352647961,3872.20388782293</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>122</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>7</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>23.4</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.109">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2927.95316671102,3875.26529818776 2924.57867705422,3871.02891901468 2923.69884419751,3871.89542107054 2926.4794529164,3876.20875320164 2927.95316671102,3875.26529818776</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>123</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>8</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>7.82</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.110">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2928.3246320957,3866.86970914656 2926.35167356852,3863.73697094461 2924.53868465165,3865.08338183141 2926.64495118742,3868.05615042304 2928.3246320957,3866.86970914656</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>124</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>9</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>7.91</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.111">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2925.54685979779,3868.6689486731 2923.52125659812,3865.83011597427 2920.71274480501,3867.52291838867 2922.5523953236,3870.64232578976 2925.54685979779,3868.6689486731</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>125</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>10</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>12.19</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.112">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2923.93879861298,3875.32143689139 2921.17932283509,3870.86894940437 2918.93974829072,3872.38866070234 2921.95250928493,3876.85447899022 2923.93879861298,3875.32143689139</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>126</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>11</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>13.83</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.113">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2920.33796667105,3869.7055104256 2919.55437619278,3868.30922634464 2917.2217999845,3869.93213404021 2918.04496427587,3871.25381762466 2920.33796667105,3869.7055104256</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>127</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>12</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>4.42</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.114">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2919.766257944,3876.13461574381 2916.18027251283,3870.72231059492 2914.81258433296,3871.5832190474 2918.36652385377,3877.04111020225 2919.766257944,3876.13461574381</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>128</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>13</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>10.68</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.115">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2921.08600722907,3878.06758186842 2920.29948997837,3877.12109500741 2916.56686573775,3879.82724758185 2917.22007497986,3880.77373444287 2921.08600722907,3878.06758186842</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>129</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>14</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>5.54</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.116">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2917.39337539103,3877.69431944436 2913.73869700883,3872.22461319854 2912.3869018134,3873.13346054582 2915.87366409307,3878.72079111053 2917.39337539103,3877.69431944436</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>130</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>15</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>11.39</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.117">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2930.26999076542,3880.2605036782 2928.1713860013,3877.24009930853 2926.53663843044,3878.12757047229 2928.57037972143,3881.12816058256 2930.26999076542,3880.2605036782</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>131</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>6.83</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.118">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2933.24297469398,3876.326968925 2932.00722173061,3874.6603235119 2929.28278340747,3876.39456636057 2930.37590907793,3877.78096964994 2933.24297469398,3876.326968925</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>132</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>6.12</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.119">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2935.399143528,3876.94826631187 2937.89943571044,3875.66508852418 2935.90819143457,3872.50197250964 2933.34867766957,3873.78172939214 2935.399143528,3876.94826631187</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>133</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>10.6</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.120">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2932.52396391759,3884.26330724871 2930.55279568067,3881.42642323207 2929.24745611808,3882.35534890871 2931.325413123,3885.21197243762 2932.52396391759,3884.26330724871</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>134</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>5.46</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.121">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2937.22224745597,3881.18904411619 2934.24603472274,3877.29691417848 2930.85745844963,3879.09758005435 2933.85855490214,3883.32271590895 2937.22224745597,3881.18904411619</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>135</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>19.58</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.122">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2940.18873173431,3879.1903994049 2938.41979002142,3876.61786726754 2936.13376164153,3877.83578965033 2938.0313946771,3880.33945321674 2940.18873173431,3879.1903994049</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>136</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>7.81</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.123">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2912.08771664901,3875.53472970514 2911.06877288559,3874.09317356033 2909.28824846855,3875.28144448881 2910.44018629846,3876.64150011857 2912.08771664901,3875.53472970514</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>137</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>16</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>3.65</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.124">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2913.26082712463,3877.69431944436 2912.5963260284,3876.86577084434 2911.60780781807,3877.58767303748 2912.19436305588,3878.37419028818 2913.26082712463,3877.69431944436</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>138</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>17</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>1.27</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.125">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2914.05853158209,3878.73046260738 2913.31415032807,3879.37400035264 2915.38042446127,3881.93351411763 2916.04696450424,3881.4536052867 2914.05853158209,3878.73046260738</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>139</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>18</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>3</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.126">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2919.33322460593,3880.54864151176 2917.91918857856,3881.66645211636 2918.96474354233,3882.93998958251 2924.1237634749,3883.53987562118 2919.33322460593,3880.54864151176</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>140</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>19</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>7.76</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.127">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2942.58985161533,3880.66186035801 2936.32311261131,3870.66898739148 2934.58959337259,3871.69005504345 2932.07058713718,3869.37589970812 2926.56969666644,3861.39295876242 2906.6112594643,3874.69858356385 2915.71369448275,3884.77297470067 2918.41392507387,3884.35552635231 2926.8427416767,3885.35674011918 2931.20408508132,3889.46541660317 2934.53678529616,3885.85276957028 2942.58985161533,3880.66186035801</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>141</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>520.22</ogr:qm2>
      <ogr:perim>95.94</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.128">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2941.82608823406,3879.51098649747 2936.24334884422,3870.43092311788 2934.53378530144,3871.72292679641 2932.07058713718,3869.37589970812 2926.56969666644,3861.39295876242 2906.6112594643,3874.69858356385 2914.92717723205,3884.06644225513 2924.63759386104,3876.30587720825 2925.23865277673,3877.0115108312 2924.17911006978,3877.79845997223 2931.19973146323,3887.50431704267 2933.70666879286,3884.96001832743 2933.60497733382,3884.76299112554 2941.82608823406,3879.51098649747</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>142</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>434.32</ogr:qm2>
      <ogr:perim>102.35</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.129">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2936.32311261131,3870.66898739148 2924.02610345012,3877.91210185483 2930.96246431575,3887.7657394936 2933.70666879286,3884.96001832743 2933.60497733382,3884.76299112554 2941.82608823406,3879.51098649747 2936.32311261131,3870.66898739148</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>143</ogr:id>
      <ogr:Hausnummer>7</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>151.46</ogr:qm2>
      <ogr:perim>50.5</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.130">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2952.16905276524,3911.32691439341 2952.30126344016,3909.3640470874 2950.35770897996,3909.54220801674 2950.17124711971,3911.46010143644 2952.16905276524,3911.32691439341</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>144</ogr:id>
      <ogr:Hausnummer>9</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>3.8</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.131">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2955.82916394825,3908.83917729013 2953.13927783047,3909.0524406095 2953.28782392673,3911.35355180202 2953.9271217333,3911.38018921062 2955.99928245221,3911.11381512455 2955.82916394825,3908.83917729013</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>145</ogr:id>
      <ogr:Hausnummer>9</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>6.33</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.132">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2955.2748433684,3905.09392482989 2950.38434638857,3905.1687720548 2950.35873670575,3908.65148059553 2955.65822519173,3908.12519630916 2955.2748433684,3905.09392482989</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>146</ogr:id>
      <ogr:Hausnummer>9</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>16.65</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.133">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2954.71006468573,3902.0574242992 2951.93886585164,3902.42005069228 2952.59525130295,3904.45446297281 2955.09916771201,3904.34791333838 2954.71006468573,3902.0574242992</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>147</ogr:id>
      <ogr:Hausnummer>9</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>5.83</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.134">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2951.10355642096,3902.43001991868 2947.6730519136,3902.61141677802 2947.36210618143,3905.90663535432 2949.48219755097,3905.95041330097 2949.45203708733,3904.50773779002 2951.60966718449,3904.34791333838 2951.10355642096,3902.43001991868</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>148</ogr:id>
      <ogr:Hausnummer>9</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>10.28</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.135">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2953.25040031427,3899.81988197621 2947.78197256105,3900.90236690214 2947.65752880987,3902.08879860769 2951.07691901235,3901.7640847035 2953.47428578698,3901.41779839161 2953.25040031427,3899.81988197621</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>149</ogr:id>
      <ogr:Hausnummer>9</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>8.1</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.136">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2971.5115709863,3889.68857992278 2968.47087892804,3890.69373842925 2970.65309206475,3896.74340000437 2973.73635250678,3895.68695433825 2971.5115709863,3889.68857992278</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>150</ogr:id>
      <ogr:Hausnummer>8</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>20.72</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.137">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2956.75068704564,3911.43346402784 2955.39217920668,3901.01823726251 2951.07691901235,3901.7640847035 2951.60966718449,3904.34791333838 2949.45203708733,3904.50773779002 2949.61186153897,3912.15267406023 2956.75068704564,3911.43346402784</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>151</ogr:id>
      <ogr:Hausnummer>9</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>63.13</ogr:qm2>
      <ogr:perim>34.41</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.138">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2953.68738505584,3898.7007827137 2947.21449476434,3900.13920277848 2946.65510918359,3906.50554343555 2955.38120079047,3906.54866909684 2953.68738505584,3898.7007827137</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>152</ogr:id>
      <ogr:Hausnummer>9</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>54.41</ogr:qm2>
      <ogr:perim>29.7</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.139">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2968.55892350373,3918.21481619706 2970.22731117969,3921.1572577357 2974.16649920455,3919.65972095895 2973.39692947908,3918.53795904258 2971.01056423763,3919.35199952214 2970.7875394487,3918.73868135261 2973.00663609847,3917.95809459139 2972.54537871944,3917.01783916491 2968.55892350373,3918.21481619706</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>153</ogr:id>
      <ogr:Hausnummer>11</ogr:Hausnummer>
      <ogr:Raumnummer>12</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>11.51</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.140">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2965.94181237047,3923.1569850094 2969.55915874654,3921.58974997985 2967.72875680091,3918.42159926954 2964.24036568876,3919.54027612587 2965.94181237047,3923.1569850094</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>154</ogr:id>
      <ogr:Hausnummer>11</ogr:Hausnummer>
      <ogr:Raumnummer>11</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>14.42</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.141">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2969.4316940582,3914.92998349654 2962.95002041345,3916.10947260467 2963.85339351257,3918.5244663986 2970.14349145847,3916.87867214585 2969.4316940582,3914.92998349654</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>155</ogr:id>
      <ogr:Hausnummer>11</ogr:Hausnummer>
      <ogr:Raumnummer>9</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>15.07</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.142">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2974.78771698358,3913.49163317097 2970.26581451431,3914.69121324396 2971.21995053517,3916.60224882289 2975.50988707056,3915.15152575743 2974.78771698358,3913.49163317097</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>156</ogr:id>
      <ogr:Hausnummer>11</ogr:Hausnummer>
      <ogr:Raumnummer>10</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>8.98</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.143">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2974.36583924638,3909.42000571654 2976.2180404463,3914.91115094757 2978.79279090261,3913.85315993829 2976.54020033193,3908.88053606358 2974.36583924638,3909.42000571654</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>157</ogr:id>
      <ogr:Hausnummer>11</ogr:Hausnummer>
      <ogr:Raumnummer>8</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>14.06</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.144">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2970.77122559096,3910.21715232988 2972.07269452714,3913.4875101695 2974.53107842735,3912.91914256194 2973.5077084878,3909.6855798142 2970.77122559096,3910.21715232988</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>158</ogr:id>
      <ogr:Hausnummer>11</ogr:Hausnummer>
      <ogr:Raumnummer>7</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>9.09</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.145">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2968.71499778634,3906.5837043342 2960.08571663523,3907.99118379566 2962.5696805975,3915.1123764784 2971.27413130268,3913.66632989054 2968.71499778634,3906.5837043342</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>159</ogr:id>
      <ogr:Hausnummer>11</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>65.15</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.146">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2972.04926374362,3903.29567172972 2973.91267222514,3908.9276382789 2976.708441509,3908.1122184775 2974.6033369596,3902.94014656945 2972.04926374362,3903.29567172972</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>160</ogr:id>
      <ogr:Hausnummer>11</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>15.61</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.147">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2968.7718092931,3903.9408418109 2970.59305874829,3909.65114517531 2973.24567394213,3909.11537615481 2971.34856200541,3903.60256386894 2968.7718092931,3903.9408418109</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>161</ogr:id>
      <ogr:Hausnummer>11</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>15.48</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.148">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2965.65177173357,3904.41064540431 2966.13450549714,3906.31483144771 2968.4958383988,3905.86854322659 2968.01915859918,3903.94535838436 2965.65177173357,3904.41064540431</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>162</ogr:id>
      <ogr:Hausnummer>11</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>4.74</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.149">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2961.59998225782,3904.38727917926 2962.24255344677,3906.91083148494 2965.41929890998,3906.42435570367 2964.82452131508,3903.91995467821 2961.59998225782,3904.38727917926</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>163</ogr:id>
      <ogr:Hausnummer>11</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>8.34</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.150">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2959.78909981624,3907.35478976094 2961.28453821961,3907.09776128536 2960.71206570582,3904.57420897968 2959.06261934729,3904.94487734204 2959.78909981624,3907.35478976094</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>164</ogr:id>
      <ogr:Hausnummer>11</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>4.08</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.151">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2957.96676143867,3904.50013992721 2963.15383622382,3918.92107116197 2965.4989342617,3924.00830612122 2981.36446260123,3917.3390606246 2976.79483963743,3907.89880658969 2977.40960114484,3907.55740467026 2974.80596939273,3901.50568801266 2966.20477265397,3903.26629607635 2965.25376295735,3902.9681623748 2957.96676143867,3904.50013992721</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>165</ogr:id>
      <ogr:Hausnummer>11</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>323.67</ogr:qm2>
      <ogr:perim>72.95</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.152">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2973.49465404696,3916.70638999116 2974.82976855099,3919.38943019237 2980.61478881507,3917.01729188374 2979.18095721282,3914.41779029902 2973.49465404696,3916.70638999116</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>166</ogr:id>
      <ogr:Hausnummer>11</ogr:Hausnummer>
      <ogr:Raumnummer>13</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>18.38</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.153">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2958.87745820593,3918.74724236885 2958.44684897596,3915.51085281731 2955.87540480003,3915.85307733866 2955.02897052884,3916.24142452764 2955.60707725705,3919.23570359863 2958.87745820593,3918.74724236885</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>167</ogr:id>
      <ogr:Hausnummer>12</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>11.08</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.154">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2954.80984069167,3919.56518719509 2954.12453556698,3916.2160347309 2952.70971208376,3916.3928876663 2953.28821339918,3919.83307932404 2954.80984069167,3919.56518719509</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>168</ogr:id>
      <ogr:Hausnummer>12</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>5.12</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.155">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2952.42232606373,3920.23943901131 2951.8106688656,3916.7157207669 2948.76368096259,3916.90133985558 2949.21448024193,3920.69947895651 2952.42232606373,3920.23943901131</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>169</ogr:id>
      <ogr:Hausnummer>12</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>11.62</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.156">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2947.9171698374,3920.87563944718 2947.68045673324,3915.86232886009 2943.96660902574,3915.69562347382 2944.55618331652,3921.42179975682 2947.9171698374,3920.87563944718</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>170</ogr:id>
      <ogr:Hausnummer>12</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>19.07</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.157">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2943.41439019593,3921.49197013377 2943.20438186711,3919.18490691899 2940.26574190787,3919.63061363362 2939.89679029519,3922.11551527905 2943.41439019593,3921.49197013377</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>171</ogr:id>
      <ogr:Hausnummer>12</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>7.69</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.158">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2943.07122710431,3918.55933612498 2942.85016093506,3915.37598328774 2940.46264630712,3915.33177005388 2940.22426311553,3918.86593224685 2943.07122710431,3918.55933612498</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>172</ogr:id>
      <ogr:Hausnummer>12</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>8.79</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.159">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2939.32415553546,3914.44750537687 2939.10308936621,3923.42279184856 2953.16359616312,3921.26703904039 2954.40086827855,3920.50471841442 2960.02372384364,3919.62426597822 2959.08747106672,3914.58014507843 2952.67655215837,3915.64126269084 2950.28903753044,3915.72968915854 2950.06797136118,3914.75699801382 2939.32415553546,3914.44750537687</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>173</ogr:id>
      <ogr:Hausnummer>12</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>138.07</ogr:qm2>
      <ogr:perim>55.96</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.160">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2970.83430358429,3952.17134841719 2972.38918298626,3951.40859156876 2971.73135822636,3949.70458095464 2970.07058330641,3950.56753583364 2970.83430358429,3952.17134841719</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>174</ogr:id>
      <ogr:Hausnummer>13</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>3.24</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.161">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2971.06988433909,3948.41753676092 2968.89238257194,3944.09569315202 2967.43334585487,3944.92469128672 2969.75454063203,3949.14705511946 2971.06988433909,3948.41753676092</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>175</ogr:id>
      <ogr:Hausnummer>13</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>7.68</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.162">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2969.66611416433,3952.703476789 2967.8091583426,3948.21583355315 2965.84166943625,3949.06693830478 2967.8975848103,3953.41088853061 2969.66611416433,3952.703476789</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>176</ogr:id>
      <ogr:Hausnummer>13</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>9.77</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.163">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2967.22678223506,3946.96610411134 2966.44199733421,3945.4075876181 2964.48556173632,3946.43554530513 2965.44719957257,3947.96090187298 2967.22678223506,3946.96610411134</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>177</ogr:id>
      <ogr:Hausnummer>13</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>3.76</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.164">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2965.79400696142,3954.62918266753 2966.88828449923,3954.15389040363 2965.95980658836,3952.17534818882 2964.93184890133,3952.83854669658 2965.79400696142,3954.62918266753</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>178</ogr:id>
      <ogr:Hausnummer>13</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>2.51</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.165">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2965.46413232803,3951.1653422922 2963.58506988937,3947.31879094719 2962.5128989685,3947.88250967879 2964.5246011087,3951.6406345561 2965.46413232803,3951.1653422922</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>179</ogr:id>
      <ogr:Hausnummer>13</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>4.84</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.166">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2968.62263489291,3943.05009712399 2967.71626359897,3941.40315416305 2965.56086844875,3942.45321846701 2966.14669379728,3944.12226804487 2968.62263489291,3943.05009712399</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>180</ogr:id>
      <ogr:Hausnummer>13</ogr:Hausnummer>
      <ogr:Raumnummer>7</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>4.63</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.167">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2965.28453573719,3944.73435483683 2964.84240339868,3943.02109202512 2961.34955792448,3944.76751476222 2962.26698252688,3946.39235110623 2965.28453573719,3944.73435483683</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>181</ogr:id>
      <ogr:Hausnummer>13</ogr:Hausnummer>
      <ogr:Raumnummer>8</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>6.59</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.168">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2955.81569176177,3923.6770179432 2958.64533872822,3928.98260600529 2962.00554450087,3927.30250311896 2959.92752250989,3922.83696650004 2955.81569176177,3923.6770179432</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>183</ogr:id>
      <ogr:Hausnummer>13</ogr:Hausnummer>
      <ogr:Raumnummer>10</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>21.34</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.169">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2965.85413391578,3956.06195794117 2973.97184442198,3952.10382141713 2961.45287907774,3921.40003639989 2948.0994059654,3923.71010286857 2958.19662135717,3945.85105244098 2960.5996575695,3945.08218126064 2965.85413391578,3956.06195794117</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>551</ogr:id>
      <ogr:Hausnummer>13</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>403.92</ogr:qm2>
      <ogr:perim>92.55</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.170">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2933.02376971174,3898.91760698682 2934.04067409031,3896.66273206044 2930.50361538225,3895.46897474646 2929.53092423754,3898.16598201136 2933.02376971174,3898.91760698682</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>185</ogr:id>
      <ogr:Hausnummer>14</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>9.67</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.171">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2931.03417418846,3894.3636439002 2934.61544613037,3895.38054827877 2936.03026961359,3892.28562190922 2932.53742413938,3891.3129307645 2931.03417418846,3894.3636439002</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>186</ogr:id>
      <ogr:Hausnummer>14</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>12.32</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.172">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2933.37747558255,3890.07496021668 2936.69346812135,3891.0476513614 2937.44509309681,3888.88120290271 2934.96915200117,3887.8200852903 2933.37747558255,3890.07496021668</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>187</ogr:id>
      <ogr:Hausnummer>14</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>7.59</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.173">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2934.48280642882,3899.27131285763 2937.62194603221,3900.06715106694 2938.37357100768,3898.12176877751 2935.36707110583,3897.06065116509 2934.48280642882,3899.27131285763</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>188</ogr:id>
      <ogr:Hausnummer>14</ogr:Hausnummer>
      <ogr:Raumnummer>5a</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>7.14</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.174">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2938.81570334618,3900.42085693774 2941.55692384492,3901.26090838091 2942.22012235269,3899.75765842998 2939.52311508779,3898.82918051912 2938.81570334618,3900.42085693774</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>189</ogr:id>
      <ogr:Hausnummer>14</ogr:Hausnummer>
      <ogr:Raumnummer>5b</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>4.81</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.175">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2937.59983941529,3892.77196748157 2943.76758553746,3895.13737549259 2944.29814434367,3893.70044539244 2938.17461145535,3891.18029106295 2937.59983941529,3892.77196748157</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>190</ogr:id>
      <ogr:Hausnummer>14</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>10.66</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.176">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2936.09658946437,3895.95532031882 2942.57382822349,3898.40915479754 2942.94964071122,3898.23230186213 2943.39177304973,3897.74595628978 2943.43598628358,3897.32593056819 2943.2812399651,3896.64062544351 2936.95874752445,3893.96572479554 2936.09658946437,3895.95532031882</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>191</ogr:id>
      <ogr:Hausnummer>14</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>14.63</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.177">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2943.65705245283,3903.18418405341 2946.39827295158,3893.14777996931 2938.83780996311,3889.5665080274 2939.41258200317,3887.908511758 2934.36037131677,3886.04400232394 2931.87422563162,3889.21280215659 2929.53092423754,3893.63412554166 2927.78450150044,3899.40395255918 2943.65705245283,3903.18418405341</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>192</ogr:id>
      <ogr:Hausnummer>14</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>197.12</ogr:qm2>
      <ogr:perim>57.13</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.178">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2918.12390990406,3897.06065116509 2923.78320383695,3898.23230186214 2925.44120010635,3893.65623215859 2919.75979955654,3892.15298220767 2919.64926647191,3894.1867909648 2918.12390990406,3897.06065116509</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>193</ogr:id>
      <ogr:Hausnummer>15</ogr:Hausnummer>
      <ogr:Raumnummer>1a</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>27.7</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.179">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2920.02507895964,3891.51189031683 2925.61805304175,3892.9709270339 2926.17071846489,3891.44557046605 2920.64406423355,3890.00864036591 2920.02507895964,3891.51189031683</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>194</ogr:id>
      <ogr:Hausnummer>15</ogr:Hausnummer>
      <ogr:Raumnummer>1b</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>9.27</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.180">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2927.38658239578,3898.56390111602 2928.02767428662,3896.198493105 2925.52962657405,3895.31422842799 2924.64536189704,3898.34283494676 2927.38658239578,3898.56390111602</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>195</ogr:id>
      <ogr:Hausnummer>15</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>7.49</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.181">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2925.88333244486,3894.67313653716 2928.31506030664,3895.31422842799 2929.61935070524,3892.13087559074 2926.98866329112,3891.62242340146 2925.88333244486,3894.67313653716</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>196</ogr:id>
      <ogr:Hausnummer>15</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>8.58</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.182">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2927.16551622653,3890.64973225674 2930.17201612837,3891.55610355068 2930.68046831766,3890.45077270442 2927.67396841581,3889.25701539045 2927.16551622653,3890.64973225674</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>197</ogr:id>
      <ogr:Hausnummer>15</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>4.29</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.183">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2926.30335816644,3890.56130578904 2927.93325611981,3886.38407748571 2925.1317074694,3886.27262210552 2924.11480309083,3890.00864036591 2926.30335816644,3890.56130578904</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>198</ogr:id>
      <ogr:Hausnummer>15</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>10.31</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.184">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2923.45160458307,3890.00864036591 2924.38008249393,3886.1620890209 2922.30206050295,3885.89680961779 2920.84302378588,3889.12437568889 2923.45160458307,3890.00864036591</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>199</ogr:id>
      <ogr:Hausnummer>15</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>8.97</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.185">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2917.41649816245,3896.86169161276 2921.61675537827,3885.67574344854 2917.26175184397,3885.38835742851 2915.84692836075,3890.47287932134 2918.23444298869,3890.80447857522 2915.86903497768,3896.66273206044 2917.41649816245,3896.86169161276</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>200</ogr:id>
      <ogr:Hausnummer>15</ogr:Hausnummer>
      <ogr:Raumnummer>7</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>30.87</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.186">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2914.10050562365,3896.37534604041 2915.4490092561,3896.59641220966 2917.01857905779,3891.18029106295 2915.78060850998,3891.20239767988 2914.10050562365,3896.37534604041</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>201</ogr:id>
      <ogr:Hausnummer>15</ogr:Hausnummer>
      <ogr:Raumnummer>8</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>7.01</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.187">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2912.72060169907,3897.07412438171 2927.65186179888,3900.17768415157 2931.67033700615,3889.59749767636 2928.27084707279,3888.24011101188 2928.40348677435,3885.98523608549 2916.59855333621,3884.12828026377 2912.72060169907,3897.07412438171</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>202</ogr:id>
      <ogr:Hausnummer>15</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>207.75</ogr:qm2>
      <ogr:perim>57.8</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.188">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2925.50751995713,3901.30512161476 2925.06538761862,3903.64842300884 2930.63625508381,3905.10745972592 2931.38788005927,3902.36623922717 2925.50751995713,3901.30512161476</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>203</ogr:id>
      <ogr:Hausnummer>16</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>15.31</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.189">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2927.09919637575,3906.34543027374 2925.64015965868,3911.42995216656 2929.48671100369,3912.35843007743 2930.54782861611,3906.83177584609 2927.09919637575,3906.34543027374</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>204</ogr:id>
      <ogr:Hausnummer>16</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>20.24</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.190">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2924.53482881241,3914.12695943146 2928.73508602823,3915.14386381002 2929.22143160058,3914.08274619761 2924.88853468322,3912.66792271438 2924.53482881241,3914.12695943146</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>205</ogr:id>
      <ogr:Hausnummer>16</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>5.89</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.191">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2923.38528473229,3903.20629067034 2924.13690970776,3901.0840554455 2920.06929219349,3900.06715106694 2919.27345398418,3902.32202599332 2923.38528473229,3903.20629067034</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>206</ogr:id>
      <ogr:Hausnummer>16</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>9.69</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.192">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2924.71168174781,3908.46766549857 2925.64015965868,3905.72644499983 2919.27345398418,3903.78106271039 2917.6375643317,3906.30121703988 2924.71168174781,3908.46766549857</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>207</ogr:id>
      <ogr:Hausnummer>16</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>20.32</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.193">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2923.25264503074,3913.4637609237 2924.26954940931,3909.83827574794 2918.56604224257,3908.15817286161 2917.549137864,3911.56259186812 2920.2461451289,3913.06584181904 2923.25264503074,3913.4637609237</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>208</ogr:id>
      <ogr:Hausnummer>16</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>23.59</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.194">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2917.72599079941,3901.79146718711 2918.61025547642,3899.18288638992 2916.57644671929,3898.47547464831 2915.55954234072,3901.26090838091 2917.72599079941,3901.79146718711</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>209</ogr:id>
      <ogr:Hausnummer>16</ogr:Hausnummer>
      <ogr:Raumnummer>7</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>6.25</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.195">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2914.67527766371,3900.8187760424 2915.85381004288,3897.72538216581 2913.74679975284,3897.9449158421 2912.72989537428,3900.02293783309 2914.67527766371,3900.8187760424</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>210</ogr:id>
      <ogr:Hausnummer>16</ogr:Hausnummer>
      <ogr:Raumnummer>8</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>5.56</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.196">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2916.31116731618,3905.15167295977 2917.06279229165,3902.94101126723 2912.64146890658,3900.641923107 2911.27085865721,3903.07365096878 2916.31116731618,3905.15167295977</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>211</ogr:id>
      <ogr:Hausnummer>16</ogr:Hausnummer>
      <ogr:Raumnummer>9</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>13.3</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.197">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2916.39959378389,3908.20238609546 2916.93015259009,3906.74334937839 2912.90674830968,3904.93060679051 2911.66877776186,3907.05284201535 2916.39959378389,3908.20238609546</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>212</ogr:id>
      <ogr:Hausnummer>16</ogr:Hausnummer>
      <ogr:Raumnummer>10</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>9.15</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.198">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2925.50751995713,3901.30512161476 2925.06538761862,3903.64842300884 2930.63625508381,3905.10745972592 2931.38788005927,3902.36623922717 2925.50751995713,3901.30512161476</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>213</ogr:id>
      <ogr:Hausnummer>16</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>15.31</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.199">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2927.09919637575,3906.34543027374 2925.64015965868,3911.42995216656 2929.48671100369,3912.35843007743 2930.54782861611,3906.83177584609 2927.09919637575,3906.34543027374</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>214</ogr:id>
      <ogr:Hausnummer>16</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>20.24</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.200">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2924.53482881241,3914.12695943146 2928.73508602823,3915.14386381002 2929.22143160058,3914.08274619761 2924.88853468322,3912.66792271438 2924.53482881241,3914.12695943146</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>215</ogr:id>
      <ogr:Hausnummer>16</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>5.89</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.201">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2923.38528473229,3903.20629067034 2924.13690970776,3901.0840554455 2920.06929219349,3900.06715106694 2919.27345398418,3902.32202599332 2923.38528473229,3903.20629067034</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>216</ogr:id>
      <ogr:Hausnummer>16</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>9.69</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.202">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2924.71168174781,3908.46766549857 2925.64015965868,3905.72644499983 2919.27345398418,3903.78106271039 2917.6375643317,3906.30121703988 2924.71168174781,3908.46766549857</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>217</ogr:id>
      <ogr:Hausnummer>16</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>20.32</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.203">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2923.25264503074,3913.4637609237 2924.26954940931,3909.83827574794 2918.56604224257,3908.15817286161 2917.549137864,3911.56259186812 2920.2461451289,3913.06584181904 2923.25264503074,3913.4637609237</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>218</ogr:id>
      <ogr:Hausnummer>16</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>23.59</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.204">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2929.88463010834,3916.55868729324 2933.20062264715,3901.26090838091 2918.99008852754,3898.3772780987 2918.99008852754,3898.3772780987 2916.31116731618,3905.15167295977 2918.06982799725,3906.43359778746 2915.25004970377,3912.40264331128 2920.7767039351,3914.74594470537 2929.88463010834,3916.55868729324</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>219</ogr:id>
      <ogr:Hausnummer>16</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>228.6</ogr:qm2>
      <ogr:perim>61.34</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.205">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2910.8287263187,3907.40654788615 2916.09010114693,3909.17507724018 2915.25004970377,3912.40264331128 2920.7767039351,3914.74594470537 2929.88463010834,3916.55868729324 2933.20062264715,3901.26090838091 2912.72060169907,3897.07412438171 2910.45190391138,3903.3103965728 2912.19933656807,3904.70954062126 2910.8287263187,3907.40654788615</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>220</ogr:id>
      <ogr:Hausnummer>16</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>297</ogr:qm2>
      <ogr:perim>72.44</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.206">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2911.66877776186,3919.07884162274 2915.38268940532,3921.15686361372 2919.14081428263,3915.23229027772 2915.3605827884,3913.61850724218 2914.16682547443,3916.13866157166 2911.66877776186,3919.07884162274</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>221</ogr:id>
      <ogr:Hausnummer>17</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>26.81</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.207">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2916.33327393311,3921.7537422707 2920.00297234272,3923.2348856047 2922.81051269224,3918.72513575193 2918.9418547303,3917.75244460721 2916.33327393311,3921.7537422707</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>222</ogr:id>
      <ogr:Hausnummer>17</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>19.36</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.208">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2920.75459731818,3923.61069809243 2924.22533617546,3924.73813555562 2926.9665566742,3920.0957460013 2923.6505641354,3919.03462838888 2920.75459731818,3923.61069809243</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>223</ogr:id>
      <ogr:Hausnummer>17</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>18.73</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.209">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2922.94315239379,3917.77455122414 2927.43079562963,3919.14516147351 2928.49191324205,3917.35452550256 2923.58424428462,3916.11655495474 2922.94315239379,3917.77455122414</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>224</ogr:id>
      <ogr:Hausnummer>17</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>9.21</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.210">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2919.53873338728,3916.91239316405 2922.01467448292,3917.70823137336 2922.94315239379,3916.09444833781 2920.3345715966,3915.54178291468 2919.53873338728,3916.91239316405</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>225</ogr:id>
      <ogr:Hausnummer>17</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>4.37</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.211">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2924.59333890824,3926.14468511102 2929.62935548148,3916.50788020732 2920.7767039351,3914.74594470537 2914.96266368374,3912.18157714203 2909.67918223858,3919.45465411046 2916.88270232887,3923.2325948814 2924.59333890824,3926.14468511102</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>226</ogr:id>
      <ogr:Hausnummer>17</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>151.32</ogr:qm2>
      <ogr:perim>51.48</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.212">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2906.78321542136,3920.55998495673 2910.09920796017,3916.93449978098 2906.73900218751,3914.10485281453 2903.46722288256,3918.03983062725 2906.78321542136,3920.55998495673</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>227</ogr:id>
      <ogr:Hausnummer>18</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>21.43</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.213">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2901.74290676238,3918.4377497319 2904.26306109187,3915.91759540241 2901.69869352853,3913.61850724218 2899.3111789006,3916.80186007942 2901.74290676238,3918.4377497319</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>228</ogr:id>
      <ogr:Hausnummer>18</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>11.95</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.214">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2898.47112745743,3916.62500714402 2900.94706855307,3913.13216166982 2898.25006128818,3911.54048525119 2895.50884078944,3914.81226455614 2898.47112745743,3916.62500714402</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>229</ogr:id>
      <ogr:Hausnummer>18</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>14.01</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.215">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2894.75721581398,3914.10485281453 2899.84173770681,3908.04763977699 2897.94056865123,3906.41175012451 2892.90025999225,3912.77845579901 2894.75721581398,3914.10485281453</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>230</ogr:id>
      <ogr:Hausnummer>18</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>19.17</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.216">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2896.94577088959,3905.85908470138 2892.23706148449,3902.16727967485 2889.716907155,3904.73164723819 2894.84564228168,3909.0203309217 2896.94577088959,3905.85908470138</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>231</ogr:id>
      <ogr:Hausnummer>18</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>23.3</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.217">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2894.40350994317,3909.50667649406 2889.14213511494,3905.28431266132 2886.22406168079,3907.82657360774 2892.06020854908,3912.42474992821 2894.40350994317,3909.50667649406</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>232</ogr:id>
      <ogr:Hausnummer>18</ogr:Hausnummer>
      <ogr:Raumnummer>7</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>26.75</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.218">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2896.2604657649,3904.0463421135 2897.94056865123,3902.58730539643 2895.06670845093,3900.17768415157 2893.2097526292,3901.68093410249 2896.2604657649,3904.0463421135</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>233</ogr:id>
      <ogr:Hausnummer>18</ogr:Hausnummer>
      <ogr:Raumnummer>8</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>8.61</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.219">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2900.81442885152,3907.29601480153 2901.65448029468,3906.41175012451 2898.25006128818,3903.67052962577 2896.79102457111,3904.82007370589 2900.81442885152,3907.29601480153</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>234</ogr:id>
      <ogr:Hausnummer>18</ogr:Hausnummer>
      <ogr:Raumnummer>9</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>6.78</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.220">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2904.30727432573,3915.0333307254 2908.19803890459,3911.14256614654 2902.3618920363,3906.45596335836 2898.95747302979,3910.92149997728 2904.30727432573,3915.0333307254</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>235</ogr:id>
      <ogr:Hausnummer>18</ogr:Hausnummer>
      <ogr:Raumnummer>10</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>39.41</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.221">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2907.40220069527,3913.44165430677 2910.54134029867,3916.31551450707 2911.20453880643,3915.2986101285 2908.06539920303,3912.60160286361 2907.40220069527,3913.44165430677</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>236</ogr:id>
      <ogr:Hausnummer>18</ogr:Hausnummer>
      <ogr:Raumnummer>11</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>4.76</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.222">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2906.65982044999,3922.00636547295 2912.81226545619,3915.1417487607 2908.9938771139,3911.40784554964 2909.1707300493,3910.96571321114 2898.69219362669,3902.69783848106 2898.82483332824,3902.34413261025 2895.24356138633,3898.76286066834 2884.80923819757,3907.69393390618 2891.78689740547,3913.72165044743 2898.35276341278,3918.11148412758 2899.04589949749,3917.28820565178 2901.83133323009,3919.49886734432 2902.93666407635,3918.21668356265 2906.65982044999,3922.00636547295</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>237</ogr:id>
      <ogr:Hausnummer>18</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>297.09</ogr:qm2>
      <ogr:perim>76.11</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.223">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2907.26956099372,3934.26608745044 2913.59205343437,3926.79405092968 2908.50753154154,3923.74333779398 2903.51143611641,3930.95009491165 2907.26956099372,3934.26608745044</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>238</ogr:id>
      <ogr:Hausnummer>19</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>50.47</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.224">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2907.88854626763,3934.88507272436 2912.75200199121,3939.43903581098 2917.83652388404,3933.73552864424 2912.6635755235,3929.53527142842 2907.88854626763,3934.88507272436</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>239</ogr:id>
      <ogr:Hausnummer>19</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>49.31</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.225">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2915.2721563207,3941.47284456811 2917.65967094863,3938.99690347247 2915.8027151269,3937.36101381999 2913.72469313592,3939.96959461718 2915.2721563207,3941.47284456811</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>240</ogr:id>
      <ogr:Hausnummer>19</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>7.83</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.226">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2915.40479602225,3942.84345481748 2919.16292089956,3939.04111670632 2916.28906069926,3937.05152118304 2919.51662677036,3933.64710217654 2912.79621522506,3928.60679351756 2915.18372985299,3926.44034505887 2908.14741004962,3922.35346971286 2902.22925233474,3930.99430814549 2915.40479602225,3942.84345481748</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>241</ogr:id>
      <ogr:Hausnummer>19</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>172.05</ogr:qm2>
      <ogr:perim>61.32</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.227">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2903.13562362868,3927.72252884054 2905.52313825662,3924.27401503672 2902.14363360786,3922.75696325479 2901.56656432146,3924.73063138558 2900.53788123242,3926.62628226444 2903.13562362868,3927.72252884054</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>242</ogr:id>
      <ogr:Hausnummer>20</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>13.076</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.228">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2901.64898324959,3930.09062615584 2902.73477834668,3928.58749464145 2900.03800795485,3927.49327632191 2899.33328551752,3929.13735232377 2901.64898324959,3930.09062615584</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>243</ogr:id>
      <ogr:Hausnummer>20</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>4.86</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.229">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2901.47762735928,3931.21537431475 2902.97353183242,3929.53463430855 2907.18876151881,3923.75314972605 2901.73013842158,3921.54491409473 2900.93595602634,3924.24617450163 2898.41849100114,3929.51889872966 2901.47762735928,3931.21537431475</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>244</ogr:id>
      <ogr:Hausnummer>20</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>41.154</ogr:qm2>
      <ogr:perim>27.46</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.230">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2898.25006128818,3922.9696062016 2898.82483332824,3921.37792978297 2897.94056865123,3921.11265037987 2897.45422307887,3922.83696650004 2898.25006128818,3922.9696062016</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>245</ogr:id>
      <ogr:Hausnummer>21</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>1.5</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.231">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2896.74681133726,3922.52747386309 2897.36579661117,3920.75894450906 2894.31508347547,3918.45985634883 2891.97178208138,3921.46635625067 2896.74681133726,3922.52747386309</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>246</ogr:id>
      <ogr:Hausnummer>21</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>11.83</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.232">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2891.13173063822,3920.80315774291 2893.2981790969,3918.19457694572 2890.02639979195,3915.18807704387 2887.41781899476,3918.72513575193 2891.13173063822,3920.80315774291</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>247</ogr:id>
      <ogr:Hausnummer>21</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>16.8</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.233">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2889.6284806873,3922.66011356464 2890.68959829971,3921.46635625067 2887.10832635781,3919.21148132429 2885.16294406838,3921.51056948452 2889.6284806873,3922.66011356464</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>248</ogr:id>
      <ogr:Hausnummer>21</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>9.59</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.234">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2885.91456904384,3919.25569455814 2890.02639979195,3915.18807704387 2886.00299551154,3912.44685654513 2882.64278973889,3916.73554022865 2885.91456904384,3919.25569455814</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>249</ogr:id>
      <ogr:Hausnummer>21</ogr:Hausnummer>
      <ogr:Raumnummer>7</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>25.07</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.235">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2884.23446615751,3921.20107684757 2885.11873083453,3920.0957460013 2881.84695152958,3917.53137843796 2880.69740744946,3918.94620192118 2884.23446615751,3921.20107684757</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>250</ogr:id>
      <ogr:Hausnummer>21</ogr:Hausnummer>
      <ogr:Raumnummer>8</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>6.74</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.236">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2879.81314277244,3918.23879017957 2885.11873083453,3911.42995216657 2882.99649560969,3910.0593419172 2877.33720167681,3917.44295197026 2879.81314277244,3918.23879017957</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>251</ogr:id>
      <ogr:Hausnummer>21</ogr:Hausnummer>
      <ogr:Raumnummer>9</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>22.25</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.237">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2899.09011273135,3923.2348856047 2899.88595094066,3921.42214301682 2882.85838685591,3908.69243855626 2875.78973849203,3918.06193724417 2881.27217948952,3920.0073195336 2890.38010566276,3922.88117973389 2891.97178208138,3921.46635625067 2899.09011273135,3923.2348856047</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>252</ogr:id>
      <ogr:Hausnummer>21</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>160.88</ogr:qm2>
      <ogr:perim>59.65</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.238">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2901.29375183959,3919.0722154471 2902.51663835477,3917.26609903486 2901.12392148848,3916.29340789014 2900.11438826043,3918.13621260649 2901.29375183959,3919.0722154471</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>253</ogr:id>
      <ogr:Hausnummer>23</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>3.41</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.239">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2902.84823760865,3916.62500714402 2904.00268709866,3914.79922649302 2902.81328086807,3913.88505476208 2901.67658691161,3915.87338216856 2902.84823760865,3916.62500714402</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>254</ogr:id>
      <ogr:Hausnummer>23</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>3.21</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.240">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2899.45947332655,3917.61643884944 2902.31091867682,3913.49894167293 2900.32317692009,3911.97117321528 2897.43211646194,3916.38183435784 2899.45947332655,3917.61643884944</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>255</ogr:id>
      <ogr:Hausnummer>23</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>12.53</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.241">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2896.79102457111,3916.02812848704 2899.6648847714,3911.96051097277 2898.006888502,3911.00992644499 2895.17724153556,3915.1217571931 2896.79102457111,3916.02812848704</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>256</ogr:id>
      <ogr:Hausnummer>23</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>9.34</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.242">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2901.60803411411,3920.1234500667 2905.58945810739,3914.23749251608 2897.71950248197,3909.63931619561 2893.98230283946,3915.18946363536 2898.03850139495,3917.9112894636 2901.60803411411,3920.1234500667</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>257</ogr:id>
      <ogr:Hausnummer>23</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>63.07</ogr:qm2>
      <ogr:perim>31.91</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.243">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2874.18923592411,3910.69317491889 2878.57203770432,3906.26632487959 2875.11424936516,3902.85258479954 2870.35703738263,3907.43360374568 2874.18923592411,3910.69317491889</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>258</ogr:id>
      <ogr:Hausnummer>25</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>31.67</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.244">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2872.33920904202,3903.93176714742 2874.73983916283,3901.42101637887 2872.25111252382,3898.71204844437 2869.67428936662,3901.42101637887 2872.33920904202,3903.93176714742</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>259</ogr:id>
      <ogr:Hausnummer>25</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>13.22</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.245">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2869.71833762572,3906.64073508192 2871.85467819195,3904.61451516344 2869.96060305075,3903.00675370638 2868.02247965046,3904.94487710667 2869.71833762572,3906.64073508192</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>260</ogr:id>
      <ogr:Hausnummer>25</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>6.92</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.246">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2867.49390054129,3904.48237038614 2869.58619284842,3902.43412633811 2869.0355896097,3902.08174026533 2867.05341795032,3903.93176714742 2867.49390054129,3904.48237038614</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>261</ogr:id>
      <ogr:Hausnummer>25</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>1.89</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.247">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2874.18923592411,3912.14676746911 2880.19647252601,3906.13700079956 2875.66485260388,3901.72935419255 2876.0947379742,3901.3027318151 2872.09815541858,3897.21320547912 2865.5772119677,3904.06360703436 2869.73576460965,3907.8336932731 2874.18923592411,3912.14676746911</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>262</ogr:id>
      <ogr:Hausnummer>25</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>105.78</ogr:qm2>
      <ogr:perim>42.31</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.248">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2868.86096154426,3899.32515669255 2871.27225322694,3896.98921787495 2869.22516705883,3895.10539624786 2866.70084607852,3896.00963062886 2868.86096154426,3899.32515669255</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>263</ogr:id>
      <ogr:Hausnummer>27</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>9.82</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.249">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2866.8013165653,3901.52294859082 2867.95672716325,3900.43033204711 2865.59567072396,3896.48686544106 2863.29740833891,3897.78042295833 2866.8013165653,3901.52294859082</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>264</ogr:id>
      <ogr:Hausnummer>27</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>10.13</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.250">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2865.6584647782,3902.61556513454 2866.22361126632,3902.1006538898 2862.43085039044,3898.28277539222 2861.71499817215,3898.77256901527 2865.6584647782,3902.61556513454</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>265</ogr:id>
      <ogr:Hausnummer>27</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>4.4</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.251">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2865.68195063454,3903.95357664817 2872.34608701852,3896.96527387918 2869.67914952748,3893.98599235766 2865.08793974226,3895.89453961538 2860.43927297827,3898.66168519858 2865.68195063454,3903.95357664817</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>266</ogr:id>
      <ogr:Hausnummer>27</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>57.87</ogr:qm2>
      <ogr:perim>31.4</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.252">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2876.88604167567,3896.87618857733 2882.7635651522,3892.30478142892 2880.77927303833,3890.09443071979 2874.87663194011,3894.31419116448 2876.88604167567,3896.87618857733</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>267</ogr:id>
      <ogr:Hausnummer>28</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>22.83</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.253">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2881.53280168917,3901.82436005116 2886.80750224503,3897.37854101122 2883.49197618135,3893.20901580992 2877.6646879482,3897.60459960647 2881.53280168917,3901.82436005116</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>268</ogr:id>
      <ogr:Hausnummer>28</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>39.16</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.254">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2883.64268191151,3904.11006362537 2886.20467932436,3902.07553626811 2884.79809250946,3900.44289085796 2882.28633034001,3902.52765345861 2883.64268191151,3904.11006362537</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>269</ogr:id>
      <ogr:Hausnummer>28</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>6.92</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.255">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2886.75726700164,3901.39736048235 2889.01785295415,3899.66424458543 2887.63638376095,3898.03159917528 2885.45115067352,3899.81495031559 2886.75726700164,3901.39736048235</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>270</ogr:id>
      <ogr:Hausnummer>28</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>5.94</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.256">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2885.02415110472,3905.8180619006 2887.46056040909,3904.08494600367 2886.5060907847,3903.13047637928 2884.39621056235,3904.91382751959 2885.02415110472,3905.8180619006</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>271</ogr:id>
      <ogr:Hausnummer>28</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>3.5</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.257">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2887.98803046467,3903.482123083 2890.55002787752,3901.47271334744 2889.62067587482,3900.36753799287 2887.23450181384,3902.50253583691 2887.98803046467,3903.482123083</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>272</ogr:id>
      <ogr:Hausnummer>28</ogr:Hausnummer>
      <ogr:Raumnummer>7</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>4.32</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.258">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2885.18587172435,3907.37156114174 2892.12583363792,3901.43142424962 2881.18115498544,3888.185491471 2873.24398652996,3894.18860305601 2878.01633465193,3899.66424458543 2885.18587172435,3907.37156114174</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>273</ogr:id>
      <ogr:Hausnummer>28</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>167.87</ogr:qm2>
      <ogr:perim>53.92</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.259">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2905.67480510576,3903.78910079953 2906.59875740216,3902.0788061232 2901.50719049218,3899.83773034043 2900.74050667176,3901.21382950529 2905.67480510576,3903.78910079953</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>274</ogr:id>
      <ogr:Hausnummer>29</ogr:Hausnummer>
      <ogr:Raumnummer>1a</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>9.77</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.260">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2905.43890239178,3900.50612136336 2906.40217180718,3898.48128973507 2903.33543652549,3896.92826353473 2901.90036168214,3899.05138796051 2905.43890239178,3900.50612136336</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>275</ogr:id>
      <ogr:Hausnummer>29</ogr:Hausnummer>
      <ogr:Raumnummer>1b</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>8.65</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.261">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2907.05090427062,3901.41041510027 2908.1517836025,3899.28729067449 2906.75602587815,3898.59924109206 2905.67480510576,3900.78134119633 2907.05090427062,3901.41041510027</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>276</ogr:id>
      <ogr:Hausnummer>29</ogr:Hausnummer>
      <ogr:Raumnummer>1c</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>3.7</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.262">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2904.2987059409,3906.24642073678 2905.2816339158,3904.63441885794 2902.56875270507,3903.0813926576 2901.58582473017,3904.39851614397 2904.2987059409,3906.24642073678</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>277</ogr:id>
      <ogr:Hausnummer>29</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>5.64</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.263">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2901.05504362373,3903.94636927551 2901.84138600365,3902.74719714614 2900.20972556531,3901.82324484973 2899.61996878037,3902.84548994362 2901.05504362373,3903.94636927551</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>278</ogr:id>
      <ogr:Hausnummer>29</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>2.4</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.264">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2899.20713903091,3901.62665925475 2900.70118955276,3899.1300221985 2898.30284529401,3897.49836176017 2896.27801366571,3899.07104652001 2899.20713903091,3901.62665925475</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>279</ogr:id>
      <ogr:Hausnummer>29</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>9.1</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.265">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2901.25162921871,3898.73685100854 2902.60806982407,3896.25987251179 2900.91743370724,3895.43421301288 2898.91226063844,3896.98723921322 2901.25162921871,3898.73685100854</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>280</ogr:id>
      <ogr:Hausnummer>29</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>6.22</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.266">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2905.12436543981,3896.79065361824 2906.2449033312,3894.58889495446 2903.41407076349,3892.93757595663 2901.62514184917,3894.80513910894 2905.12436543981,3896.79065361824</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>281</ogr:id>
      <ogr:Hausnummer>29</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>9.09</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.267">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2907.28680698459,3897.81289871214 2908.13212504301,3896.00431123832 2906.69705019965,3895.00172470392 2905.67480510576,3897.10519057021 2907.28680698459,3897.81289871214</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>282</ogr:id>
      <ogr:Hausnummer>29</ogr:Hausnummer>
      <ogr:Raumnummer>7</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>3.78</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.268">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2908.56461335196,3898.50094829457 2909.80310260034,3896.57440946376 2908.87915030393,3896.12226259531 2907.95519800752,3898.2060699021 2908.56461335196,3898.50094829457</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>283</ogr:id>
      <ogr:Hausnummer>29</ogr:Hausnummer>
      <ogr:Raumnummer>8</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>1.94</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.269">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2904.95614924708,3907.64028447689 2910.88432337273,3896.61372658276 2903.33543652549,3891.87601374374 2894.84293882236,3898.69753388955 2898.82483332824,3902.34413261025 2898.86071993117,3902.83081071286 2904.95614924708,3907.64028447689</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>284</ogr:id>
      <ogr:Hausnummer>29</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>130.16</ogr:qm2>
      <ogr:perim>45.85</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.270">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2910.2749769141,3895.12465583264 2911.12518749367,3894.45157245715 2911.72741998753,3891.90094071844 2905.43940424279,3888.26983303486 2903.04818698775,3890.41307220419 2905.31541519994,3892.21976968578 2910.2749769141,3895.12465583264</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>285</ogr:id>
      <ogr:Hausnummer>30</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>28.22</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.271">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2906.838709155,3888.19898215323 2909.70816986105,3884.03649285741 2908.92881016311,3883.29255860029 2906.34275298359,3887.88015318589 2906.838709155,3888.19898215323</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>286</ogr:id>
      <ogr:Hausnummer>30</ogr:Hausnummer>
      <ogr:Raumnummer>2c</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>4.24</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.272">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2908.663119357,3889.54514890421 2911.07204933244,3885.08154336147 2910.25726419369,3884.30218366353 2907.5295052509,3888.67722560423 2908.663119357,3889.54514890421</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>287</ogr:id>
      <ogr:Hausnummer>30</ogr:Hausnummer>
      <ogr:Raumnummer>2b</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>6.42</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.273">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2911.79827086916,3891.05073013887 2912.29422704058,3889.42115986136 2910.11556243043,3888.51781112056 2909.3716281733,3889.88169059196 2911.79827086916,3891.05073013887</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>288</ogr:id>
      <ogr:Hausnummer>30</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>4.09</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.274">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2912.75475777118,3888.73036376546 2913.42784114667,3887.26020797162 2911.6565691059,3885.64835041452 2910.66465676307,3887.80930230426 2912.75475777118,3888.73036376546</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>289</ogr:id>
      <ogr:Hausnummer>30</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>4.56</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.275">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2910.87720940796,3896.32912082037 2911.76257329756,3894.94428162755 2913.25071394259,3889.26174537769 2914.45754621948,3886.895804593 2908.69854479781,3882.1589444942 2905.26227703872,3887.56132421855 2901.93228560207,3890.39535948378 2904.38574455728,3892.53519144077 2910.87720940796,3896.32912082037</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>290</ogr:id>
      <ogr:Hausnummer>30</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>90.75</ogr:qm2>
      <ogr:perim>39.08</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.276">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2978.78797190858,3930.57717533325 2977.36153253178,3927.75282536719 2973.0453053557,3929.68959397184 2971.74136138719,3926.411972353 2968.8884826336,3928.12369960515 2971.28490078662,3933.14476621148 2978.78797190858,3930.57717533325</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>291</ogr:id>
      <ogr:Hausnummer>32</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>32.53</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.277">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2973.42455985182,3928.92250565616 2977.24741738164,3927.210778404 2975.62127649208,3924.10114056258 2972.11223562516,3926.24079962778 2973.42455985182,3928.92250565616</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>292</ogr:id>
      <ogr:Hausnummer>32</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>13.44</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.278">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2970.91402654865,3933.94357226249 2979.92912341002,3931.06216472136 2975.7924492173,3923.1596905739 2968.08967658259,3927.9239980924 2970.91402654865,3933.94357226249</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>293</ogr:id>
      <ogr:Hausnummer>32</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>71.49</ogr:qm2>
      <ogr:perim>34</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.279">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2975.0792295289,3942.6733812485 2980.12882492277,3939.30698431926 2978.04622343264,3935.01340179509 2973.32470909544,3937.79495857985 2975.0792295289,3942.6733812485</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>294</ogr:id>
      <ogr:Hausnummer>33</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>28.3</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.280">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2972.75413334472,3936.72512904725 2976.69110602468,3934.99913740132 2976.06347269889,3933.40152529931 2971.94106289994,3934.67105634466 2972.75413334472,3936.72512904725</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>295</ogr:id>
      <ogr:Hausnummer>33</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>8.44</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.281">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2977.2188885941,3934.54267680075 2979.42986962814,3933.41578969308 2978.80223630235,3932.18905182903 2976.64831284338,3933.11623742395 2977.2188885941,3934.54267680075</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>296</ogr:id>
      <ogr:Hausnummer>33</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>3.51</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.282">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2980.14308931654,3936.86777298493 2981.62658626841,3936.05470254015 2980.64234309842,3933.61549120583 2978.95914463379,3934.65679195089 2980.14308931654,3936.86777298493</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>297</ogr:id>
      <ogr:Hausnummer>33</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>4.69</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.283">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2980.8705733987,3938.82199493114 2982.83905973869,3937.89480933622 2982.45392110695,3936.69660025971 2980.37131961682,3937.75216539854 2980.8705733987,3938.82199493114</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>298</ogr:id>
      <ogr:Hausnummer>33</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>2.74</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.284">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2974.83673483485,3943.57203805588 2983.79477412114,3937.9518669113 2980.91336658001,3932.44581091685 2980.20014689161,3932.9593290925 2979.4047574673,3931.22976269672 2970.55741670445,3934.01489423133 2970.98534851749,3934.89928664495 2974.83673483485,3943.57203805588</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>299</ogr:id>
      <ogr:Hausnummer>33</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>92.11</ogr:qm2>
      <ogr:perim>39.22</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.285">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2982.59656504463,3959.80491816385 2984.59358017215,3959.03464090038 2982.99596807013,3955.69677275867 2981.08453930522,3956.69528032242 2982.59656504463,3959.80491816385</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>300</ogr:id>
      <ogr:Hausnummer>35</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>7.67</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.286">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2980.74219385479,3955.78235912127 2982.62509383217,3954.89796670766 2980.77072264233,3950.59011978973 2978.91635145249,3951.56009856595 2980.74219385479,3955.78235912127</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>301</ogr:id>
      <ogr:Hausnummer>35</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>9.68</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.287">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2983.96594684636,3955.38295609577 2990.84138464252,3952.78683643 2989.12965739037,3948.47898951207 2982.19716201913,3951.95950159146 2983.96594684636,3955.38295609577</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>302</ogr:id>
      <ogr:Hausnummer>35</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>31.98</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.288">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2985.27827107301,3958.5781802998 2992.09665129411,3955.66824397113 2990.92697100513,3953.64270005608 2984.36534987186,3956.38146365953 2985.27827107301,3958.5781802998</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>303</ogr:id>
      <ogr:Hausnummer>35</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>17.06</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.289">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2982.08306514606,3960.97324858301 2993.03810128279,3956.18176214678 2989.30371332619,3947.6916885643 2982.16863323159,3950.9609940277 2981.36982718058,3949.33485313814 2977.9749014638,3950.90393645262 2982.08306514606,3960.97324858301</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>304</ogr:id>
      <ogr:Hausnummer>35</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>115.41</ogr:qm2>
      <ogr:perim>45.39</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.290">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2998.80436986511,3951.70882604795 3007.3630061259,3947.8574397306 3006.87801673779,3946.80187459177 2998.17673653932,3950.25385788362 2998.80436986511,3951.70882604795</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>305</ogr:id>
      <ogr:Hausnummer>36</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>12.86</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.291">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2997.77733351381,3949.54063819522 3000.5446259048,3948.45654426885 2998.57613956482,3943.83488068803 2995.72326081123,3945.06161855207 2997.77733351381,3949.54063819522</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>306</ogr:id>
      <ogr:Hausnummer>36</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>15.11</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.292">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3001.11520165552,3948.11419881842 3004.45306979723,3946.74481701669 3002.54164103232,3942.00903828572 2999.20377289061,3943.5781216002 3001.11520165552,3948.11419881842</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>307</ogr:id>
      <ogr:Hausnummer>36</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>18.28</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.293">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3005.1377606981,3946.40247156626 3006.16479704939,3946.00306854076 3004.22483949694,3941.09611708457 3003.14074557058,3941.66669283529 3005.1377606981,3946.40247156626</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>308</ogr:id>
      <ogr:Hausnummer>36</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>6.04</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.294">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2995.35238657326,3944.2913412886 2999.85993500394,3942.32285494862 2998.29085168946,3939.46997619502 2994.0400623466,3941.43846253501 2995.35238657326,3944.2913412886</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>309</ogr:id>
      <ogr:Hausnummer>36</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>15.33</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.295">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3000.60168347988,3941.98050949819 3003.82543647144,3940.55407012139 3002.42752588218,3937.55854743011 2998.97554259033,3939.12763074459 3000.60168347988,3941.98050949819</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>310</ogr:id>
      <ogr:Hausnummer>36</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>12.03</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.296">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2999.51413605346,3952.10214552914 3009.32803896583,3947.50901073584 3004.02168448414,3935.41280482059 2994.29336793438,3940.1771123391 2999.51413605346,3952.10214552914</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>311</ogr:id>
      <ogr:Hausnummer>36</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>141.98</ogr:qm2>
      <ogr:perim>47.77</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.297">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3010.15537380438,3944.57054561964 3013.3505980084,3943.37233654313 3012.15238893189,3938.7506729623 3008.41511776468,3939.8632956762 3010.15537380438,3944.57054561964</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>312</ogr:id>
      <ogr:Hausnummer>37</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>17.87</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.298">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3014.57733587245,3943.00146230516 3019.0563555156,3941.6035517159 3017.9437328017,3936.95335934754 3013.32206922087,3938.37979872434 3014.57733587245,3943.00146230516</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>313</ogr:id>
      <ogr:Hausnummer>37</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>22.77</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.299">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3007.70189807628,3938.63655781216 3008.52923291483,3938.40832751187 3007.27396626324,3934.87075785741 3006.47516021224,3935.01340179509 3007.70189807628,3938.63655781216</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>314</ogr:id>
      <ogr:Hausnummer>37</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>3.14</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.300">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3009.67038441627,3938.0374532739 3012.66590710754,3937.2386472229 3011.23946773074,3933.10197303018 3008.1868874644,3934.35723968176 3009.67038441627,3938.0374532739</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>315</ogr:id>
      <ogr:Hausnummer>37</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>13.31</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.301">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3013.6644146713,3936.35425480928 3017.45874341358,3935.41280482059 3016.80258130026,3932.98785788004 3012.72296468261,3934.27165331916 3013.6644146713,3936.35425480928</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>316</ogr:id>
      <ogr:Hausnummer>37</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>9.76</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.302">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3005.76194052384,3934.15753816901 3006.7604480876,3933.95783665626 3006.4466314247,3932.73109879221 3005.59076779862,3933.07344424264 3005.76194052384,3934.15753816901</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>317</ogr:id>
      <ogr:Hausnummer>37</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>1.14</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.303">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3007.73042686382,3933.30167454293 3010.98270864292,3932.01787910382 3010.55477682988,3931.01937154006 3007.21690868817,3932.18905182903 3007.73042686382,3933.30167454293</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>318</ogr:id>
      <ogr:Hausnummer>37</ogr:Hausnummer>
      <ogr:Raumnummer>7</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>4.06</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.304">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3012.38061923218,3933.2731457554 3016.68846615011,3932.01787910382 3015.97524646171,3928.90824126239 3011.29652530582,3930.61996851455 3012.38061923218,3933.2731457554</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>319</ogr:id>
      <ogr:Hausnummer>37</ogr:Hausnummer>
      <ogr:Raumnummer>8</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>14.28</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.305">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3009.32803896583,3945.88286984629 3020.34015095472,3941.6035517159 3016.46023584983,3927.85267612356 3003.907569334,3932.36022455425 3009.32803896583,3945.88286984629</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>320</ogr:id>
      <ogr:Hausnummer>37</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>181.1</ogr:qm2>
      <ogr:perim>53.86</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.306">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2980.05750295393,3912.13331419125 2981.94040293131,3911.04922026488 2980.08603174147,3907.28342031013 2978.37430448931,3908.25339908635 2980.05750295393,3912.13331419125</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>321</ogr:id>
      <ogr:Hausnummer>40</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>8.69</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.307">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2982.9674392826,3910.56423087677 2986.16266348663,3908.82397483707 2984.59358017215,3905.08670366986 2981.08453930523,3906.88401728463 2982.9674392826,3910.56423087677</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>322</ogr:id>
      <ogr:Hausnummer>40</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>15.49</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.308">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2986.96146953764,3908.22487029882 2989.84287707877,3907.22636273506 2986.36236499938,3903.74585065567 2985.02151198519,3904.51612791914 2986.96146953764,3908.22487029882</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>323</ogr:id>
      <ogr:Hausnummer>40</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>9.99</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.309">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2979.51473625569,3913.51776360835 2991.29194029165,3907.12052871848 2987.01852711271,3902.69028551684 2976.79483963743,3907.89880658969 2979.51473625569,3913.51776360835</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>324</ogr:id>
      <ogr:Hausnummer>40</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>75.56</ogr:qm2>
      <ogr:perim>37.18</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.310">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2978.03195903888,3905.94256729594 2981.56952869334,3904.51612791914 2980.05750295393,3899.89446433832 2975.57848331079,3901.12120220236 2978.03195903888,3905.94256729594</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>325</ogr:id>
      <ogr:Hausnummer>41</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>21.56</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.311">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2981.96893171884,3904.25936883132 2985.56355894837,3902.60469915423 2984.10859078404,3898.92448556209 2980.71366506726,3899.86593555078 2981.96893171884,3904.25936883132</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>326</ogr:id>
      <ogr:Hausnummer>41</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>15.87</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.312">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2975.20760907282,3900.32239615135 2983.79477412115,3898.23979466123 2982.65362261971,3895.15868560734 2974.09498635892,3898.6106688992 2975.20760907282,3900.32239615135</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>327</ogr:id>
      <ogr:Hausnummer>41</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>23.66</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.313">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2977.62024851998,3907.37568414189 2986.23846332947,3902.98508108853 2982.99425331818,3893.9319477433 2973.18035040581,3898.46802496152 2977.62024851998,3907.37568414189</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>328</ogr:id>
      <ogr:Hausnummer>41</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>99.91</ogr:qm2>
      <ogr:perim>39.95</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.314">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3007.35955262585,3908.28192787389 3008.87157836526,3908.25339908635 3008.52923291483,3904.91553094465 3007.1598511131,3905.08670366986 3007.35955262585,3908.28192787389</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>329</ogr:id>
      <ogr:Hausnummer>42</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>4.73</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.315">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3009.89861471656,3908.56721574925 3011.15388136814,3908.51015817418 3009.78449956641,3904.94405973218 3009.89861471656,3908.56721574925</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>330</ogr:id>
      <ogr:Hausnummer>42</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>2.28</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.316">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2994.26483914684,3905.08670366986 2995.37746186075,3905.08670366986 2994.32189672192,3901.80589310323 2993.32338915816,3901.8629506783 2994.26483914684,3905.08670366986</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>331</ogr:id>
      <ogr:Hausnummer>42</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>3.46</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.317">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2996.2903830619,3904.8014157945 3000.68381634244,3904.63024306929 2999.99912544157,3901.63472037801 2995.71980731118,3902.94704460466 2996.2903830619,3904.8014157945</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>332</ogr:id>
      <ogr:Hausnummer>42</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>10.98</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.318">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3001.39703603084,3904.37348398146 3002.4811299572,3904.11672489364 3001.11174815548,3900.26533857628 3000.36999967954,3900.89297190207 3001.39703603084,3904.37348398146</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>333</ogr:id>
      <ogr:Hausnummer>42</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>3.88</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.319">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3003.5081663085,3904.34495519393 3005.67635416123,3904.05966731857 3003.25140722067,3899.18124464992 3001.56820875605,3899.95152191339 3003.5081663085,3904.34495519393</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>334</ogr:id>
      <ogr:Hausnummer>42</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>10.08</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.320">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3006.4466314247,3903.83143701828 3009.12833745308,3903.46056278031 3006.64633293746,3897.72627648558 3004.2213859969,3898.6962552618 3006.4466314247,3903.83143701828</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>335</ogr:id>
      <ogr:Hausnummer>42</ogr:Hausnummer>
      <ogr:Raumnummer>7</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>15.45</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.321">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2995.74833609871,3902.14823855366 2999.48560726593,3901.03561583975 2998.4300421271,3898.72478404934 2995.23481792307,3900.00857948846 2995.74833609871,3902.14823855366</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>336</ogr:id>
      <ogr:Hausnummer>42</ogr:Hausnummer>
      <ogr:Raumnummer>8</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>8.65</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.322">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2992.78134219497,3900.60768402671 2994.29336793438,3900.55062645164 2993.46603309584,3897.07011437226 2992.80987098251,3897.44098861022 2992.78134219497,3900.60768402671</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>337</ogr:id>
      <ogr:Hausnummer>42</ogr:Hausnummer>
      <ogr:Raumnummer>9</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>3.69</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.323">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2994.8639436851,3899.20977343745 2998.1162254642,3897.98303557341 2997.4030057758,3895.50103105778 2994.26483914684,3896.89894164704 2994.8639436851,3899.20977343745</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>338</ogr:id>
      <ogr:Hausnummer>42</ogr:Hausnummer>
      <ogr:Raumnummer>10</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>8.52</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.324">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2999.97059665404,3899.66623403803 3006.30398748702,3896.8704128595 3005.59076779862,3895.13015681981 2999.31443454071,3897.86892042326 2999.97059665404,3899.66623403803</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>339</ogr:id>
      <ogr:Hausnummer>42</ogr:Hausnummer>
      <ogr:Raumnummer>11</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>13.05</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.325">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2999.11473302796,3896.92747043458 3005.13430719805,3894.18870683112 3004.24991478443,3892.33433564128 2998.57268606478,3895.10162803227 2999.11473302796,3896.92747043458</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>340</ogr:id>
      <ogr:Hausnummer>42</ogr:Hausnummer>
      <ogr:Raumnummer>12</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>12.73</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.326">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2992.58164068222,3896.10013559603 2993.26633158309,3895.90043408328 2989.72876192863,3888.68265083668 2983.50948624578,3892.53403715404 2992.58164068222,3896.10013559603</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>341</ogr:id>
      <ogr:Hausnummer>42</ogr:Hausnummer>
      <ogr:Raumnummer>13</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>31.38</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.327">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2994.20778157177,3895.38691590763 2998.65827242738,3893.6181310804 2997.48859213841,3891.42141444013 2993.32338915816,3893.38990078012 2994.20778157177,3895.38691590763</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>342</ogr:id>
      <ogr:Hausnummer>42</ogr:Hausnummer>
      <ogr:Raumnummer>14</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>10.95</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.328">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2999.34296332825,3893.04755532968 3003.45110873343,3891.59258716535 3002.68083146996,3890.39437808884 2999.00061787782,3891.99199019085 2999.34296332825,3893.04755532968</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>343</ogr:id>
      <ogr:Hausnummer>42</ogr:Hausnummer>
      <ogr:Raumnummer>15</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>5.24</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.329">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2998.71533000246,3891.30729928999 3002.53818753228,3889.59557203783 3002.16731329431,3889.02499628711 2998.4300421271,3890.82230990188 2998.71533000246,3891.30729928999</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>344</ogr:id>
      <ogr:Hausnummer>42</ogr:Hausnummer>
      <ogr:Raumnummer>16</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>2.57</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.330">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2992.61016946976,3892.30580685375 2992.80987098251,3892.61962351664 2994.23631035931,3891.67817352796 2992.12518008165,3887.08503873467 2990.41345282949,3888.31177659871 2992.61016946976,3892.30580685375</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>345</ogr:id>
      <ogr:Hausnummer>42</ogr:Hausnummer>
      <ogr:Raumnummer>17</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>9.48</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.331">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2994.8639436851,3891.53552959028 2998.5156284897,3889.99497506334 2996.11921033668,3884.973908457 2992.66722704483,3886.7141644967 2994.8639436851,3891.53552959028</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>346</ogr:id>
      <ogr:Hausnummer>42</ogr:Hausnummer>
      <ogr:Raumnummer>18</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>21.25</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.332">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2999.28590575318,3889.6526296129 3001.79643905634,3888.19766144857 3000.76940270505,3886.37181904627 2998.08769667667,3887.54149933524 2999.28590575318,3889.6526296129</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>347</ogr:id>
      <ogr:Hausnummer>42</ogr:Hausnummer>
      <ogr:Raumnummer>19</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>6.57</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.333">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2997.8024088013,3886.62857813409 3000.17029816679,3885.48742663265 2999.02914666535,3882.77719181674 2996.6897860874,3884.63156300657 2997.8024088013,3886.62857813409</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>348</ogr:id>
      <ogr:Hausnummer>42</ogr:Hausnummer>
      <ogr:Raumnummer>20</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>7.23</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.334">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2993.64152753194,3906.43530372325 2998.57268606478,3905.89977411463 3006.26203097696,3905.35527818821 3006.46089581847,3908.99514756229 3012.63737832001,3909.7939536133 3013.46152121362,3909.79399582528 2999.91889603122,3882.14299742057 2980.77072264233,3893.84636138069 2990.95549979267,3897.46951739776 2991.78283463122,3898.18273708616 2991.72577705614,3899.08139389354 2991.85415660006,3901.20678856497 2992.35341038194,3903.17527490495 2993.64152753194,3906.43530372325</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>349</ogr:id>
      <ogr:Hausnummer>42</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>387.04</ogr:qm2>
      <ogr:perim>96.81</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.335">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2993.93675809018,3913.63107553688 2997.87373077015,3912.40433767284 2996.78963684378,3908.58148014302 2992.71002022614,3909.57998770678 2993.93675809018,3913.63107553688</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>350</ogr:id>
      <ogr:Hausnummer>43</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>17.07</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.336">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2993.43080157765,3914.93339804258 2998.8722383339,3913.03197099863 2997.13198229421,3907.58297257926 2991.60611319547,3908.72570177287 2993.43080157765,3914.93339804258</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>351</ogr:id>
      <ogr:Hausnummer>43</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>34.67</ogr:qm2>
      <ogr:perim>23.53</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.337">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2997.01654778948,3913.68041374352 3001.24012769939,3911.94787707226 3000.2986777107,3908.98088316852 2996.04788836784,3910.66408163314 2997.01654778948,3913.68041374352</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>352</ogr:id>
      <ogr:Hausnummer>44</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>14.31</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.338">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2996.36170503074,3914.37282401282 3002.38127920083,3912.0619922224 3000.66955194867,3908.26766348012 2995.04938080408,3910.35026497025 2996.36170503074,3914.37282401282</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>353</ogr:id>
      <ogr:Hausnummer>44</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>26.07</ogr:qm2>
      <ogr:perim>20.78</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.339">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2984.76475289737,3914.81502021963 2987.07558468778,3913.58828235558 2985.8203180362,3910.96363390227 2983.50948624578,3912.21890055385 2984.76475289737,3914.81502021963</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>354</ogr:id>
      <ogr:Hausnummer>45</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>7.59</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.340">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2987.67468922604,3913.33152326776 2990.01404980399,3912.333015704 2988.70172557733,3909.36602180026 2986.44795136199,3910.70687481445 2987.67468922604,3913.33152326776</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>355</ogr:id>
      <ogr:Hausnummer>45</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>7.91</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.341">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2987.13264226285,3919.46521258799 2992.12518008165,3917.0973232225 2990.1852225292,3912.90359145472 2985.22121349794,3915.5852974831 2987.13264226285,3919.46521258799</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>356</ogr:id>
      <ogr:Hausnummer>45</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>24.96</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.342">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2986.44795136199,3920.60636408943 2993.32338915816,3917.38261109786 2989.37748272678,3908.16043892906 2982.49088436783,3911.90115592436 2986.44795136199,3920.60636408943</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>357</ogr:id>
      <ogr:Hausnummer>45</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>75.44</ogr:qm2>
      <ogr:perim>34.93</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.343">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2988.47349527704,3923.17395496766 2993.69426339613,3920.83459438971 2992.17295422416,3917.92202664783 2987.28552233755,3920.21364408844 2988.47349527704,3923.17395496766</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>358</ogr:id>
      <ogr:Hausnummer>45</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>17.98</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.344">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2989.10112860284,3924.91421100736 2993.29486037062,3923.25954133027 2992.6386982573,3922.26103376651 2988.87289830255,3923.97276101867 2989.10112860284,3924.91421100736</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>359</ogr:id>
      <ogr:Hausnummer>45</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>4.6</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.345">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2988.58761042719,3925.77007463344 2996.11921033668,3922.8030807297 2989.37748272678,3908.16043892906 2982.49088436783,3911.90115592436 2988.58761042719,3925.77007463344</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>360</ogr:id>
      <ogr:Hausnummer>45</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>124.3</ogr:qm2>
      <ogr:perim>47.08</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.346">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2990.72726949239,3927.86694051733 2994.45027626583,3926.11242008387 2993.3519179457,3924.17246253142 2989.51479602211,3925.77007463344 2990.72726949239,3927.86694051733</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>361</ogr:id>
      <ogr:Hausnummer>45</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>9.57</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.347">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2990.42771722326,3928.993827625 2995.30613989191,3926.31212159662 2994.04210823525,3923.62133307268 2996.11921033668,3922.8030807297 2989.48343897009,3908.39056975115 2982.49088436783,3911.90115592436 2988.58761042719,3925.77007463344 2990.42771722326,3928.993827625</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>362</ogr:id>
      <ogr:Hausnummer>45</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>142.34</ogr:qm2>
      <ogr:perim>53.18</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.348">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2984.76475289737,3914.81502021963 2987.07558468778,3913.58828235558 2985.8203180362,3910.96363390227 2983.50948624578,3912.21890055385 2984.76475289737,3914.81502021963</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>363</ogr:id>
      <ogr:Hausnummer>45</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>7.59</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.349">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2987.67468922604,3913.33152326776 2990.01404980399,3912.333015704 2988.70172557733,3909.36602180026 2986.44795136199,3910.70687481445 2987.67468922604,3913.33152326776</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>364</ogr:id>
      <ogr:Hausnummer>45</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>7.91</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.350">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2987.13264226285,3919.46521258799 2992.12518008165,3917.0973232225 2990.1852225292,3912.90359145472 2985.22121349794,3915.5852974831 2987.13264226285,3919.46521258799</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>365</ogr:id>
      <ogr:Hausnummer>45</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>24.96</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.351">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2988.47349527704,3923.17395496766 2993.69426339613,3920.83459438971 2992.17295422416,3917.92202664783 2987.28552233755,3920.21364408844 2988.47349527704,3923.17395496766</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>366</ogr:id>
      <ogr:Hausnummer>45</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>17.98</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.352">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2989.10112860284,3924.91421100736 2993.29486037062,3923.25954133027 2992.6386982573,3922.26103376651 2988.87289830255,3923.97276101867 2989.10112860284,3924.91421100736</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>367</ogr:id>
      <ogr:Hausnummer>45</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>4.6</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.353">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2984.76475289737,3914.81502021963 2987.07558468778,3913.58828235558 2985.8203180362,3910.96363390227 2983.50948624578,3912.21890055385 2984.76475289737,3914.81502021963</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>368</ogr:id>
      <ogr:Hausnummer>45</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>7.59</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.354">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2987.67468922604,3913.33152326776 2990.01404980399,3912.333015704 2988.70172557733,3909.36602180026 2986.44795136199,3910.70687481445 2987.67468922604,3913.33152326776</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>369</ogr:id>
      <ogr:Hausnummer>45</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>7.91</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.355">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2987.13264226285,3919.46521258799 2992.12518008165,3917.0973232225 2990.1852225292,3912.90359145472 2985.22121349794,3915.5852974831 2987.13264226285,3919.46521258799</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>370</ogr:id>
      <ogr:Hausnummer>45</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>24.96</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.356">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3012.43679843256,3915.84047145862 3010.89624390562,3911.81791241605 3003.30758642105,3914.61373359457 3005.21901518596,3919.2353971754 3012.60797115778,3916.32546084673 3012.43679843256,3915.84047145862</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>371</ogr:id>
      <ogr:Hausnummer>46</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>39.38</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.357">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3007.5860715036,3924.59977914634 3014.86091232528,3922.37453371854 3013.14918507312,3917.43905347481 3005.73170031377,3920.2063458658 3007.5860715036,3924.59977914634</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>372</ogr:id>
      <ogr:Hausnummer>46</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>38.72</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.358">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3006.87617587383,3924.81084057504 3004.99327589646,3920.16064820668 3001.455706242,3921.5300300084 3003.05331834401,3926.18022237677 3006.87617587383,3924.81084057504</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>373</ogr:id>
      <ogr:Hausnummer>46</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>19.5</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.359">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3002.57165301452,3927.08317140205 3016.23694224425,3922.68973812151 3011.50116351328,3910.76470493147 3002.14372120148,3913.93140034797 3004.56866814204,3919.09511089198 3000.11817728643,3921.00653965689 3002.57165301452,3927.08317140205</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>374</ogr:id>
      <ogr:Hausnummer>46</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>155.12</ogr:qm2>
      <ogr:perim>54.02</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.360">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3004.74510124443,3919.22113453654 3002.71937967549,3914.90753919563 3000.836657352,3915.71209366961 3002.65935342245,3920.11693648572 3004.74510124443,3919.22113453654</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>375</ogr:id>
      <ogr:Hausnummer>46</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>10.29</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.361">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3002.04369462178,3920.15095697428 3000.18436177127,3915.86117002739 2997.54919866811,3916.9720721199 2999.17533955766,3921.39403418797 3002.04369462178,3920.15095697428</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>376</ogr:id>
      <ogr:Hausnummer>46</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>14.04</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.362">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3016.28735170207,3922.58891920586 3011.5515729711,3910.66388601582 2995.86073982632,3916.35537912925 2998.57097464224,3922.48906844948 3000.50893007016,3921.74866409495 3002.65059125988,3927.36749111813 3016.28735170207,3922.58891920586</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>377</ogr:id>
      <ogr:Hausnummer>46</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>198</ogr:qm2>
      <ogr:perim>58.61</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.363">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3002.31687476629,3926.32827009108 3000.60974898582,3921.8494830106 2998.12974659471,3922.70400251528 2999.92706020947,3927.12596458335 3002.31687476629,3926.32827009108</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>378</ogr:id>
      <ogr:Hausnummer>46</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>12.28</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.364">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3016.38817061772,3922.99219486846 3011.65239188675,3911.06716167842 2995.96155874197,3916.75865479185 2998.34999916165,3922.16407258374 2997.24535418109,3922.57852744918 2999.38501324629,3928.31281374391 3016.38817061772,3922.99219486846</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>379</ogr:id>
      <ogr:Hausnummer>46</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>213.43</ogr:qm2>
      <ogr:perim>60.39</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.365">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3001.96808043504,3920.27698061884 3000.10874758453,3915.98719367195 2997.47358448138,3917.09809576446 2999.09972537093,3921.52005783253 3001.96808043504,3920.27698061884</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>380</ogr:id>
      <ogr:Hausnummer>46</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>14.04</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.366">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3012.53678430037,3915.72871396273 3010.99622977343,3911.70615492016 3003.40757228886,3914.50197609868 3005.31900105377,3919.12363967951 3012.70795702558,3916.21370335084 3012.53678430037,3915.72871396273</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>381</ogr:id>
      <ogr:Hausnummer>46</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>39.38</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.367">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3007.68689041925,3924.37293658613 3014.96173124093,3922.14769115832 3013.25000398877,3917.2122109146 3005.83251922942,3919.97950330559 3007.68689041925,3924.37293658613</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>382</ogr:id>
      <ogr:Hausnummer>46</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>38.72</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.368">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3007.00219951839,3924.88645476178 3005.11929954102,3920.23626239342 3001.58172988656,3921.60564419514 3003.17934198857,3926.2558365635 3007.00219951839,3924.88645476178</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>383</ogr:id>
      <ogr:Hausnummer>46</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>19.5</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.369">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3004.71989651551,3919.09511089198 3002.69417494658,3914.78151555107 3000.81145262309,3915.58607002505 3002.63414869354,3919.99091284116 3004.71989651551,3919.09511089198</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>384</ogr:id>
      <ogr:Hausnummer>46</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>10.29</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.370">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3012.61239848711,3915.72871396273 3011.07184396016,3911.70615492016 3003.4831864756,3914.50197609868 3005.39461524051,3919.12363967951 3012.78357121232,3916.21370335084 3012.61239848711,3915.72871396273</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>385</ogr:id>
      <ogr:Hausnummer>46</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>39.38</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.371">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3007.40963840122,3924.67539333308 3014.68447922289,3922.45014790527 3012.97275197073,3917.51466766155 3005.55526721138,3920.28196005254 3007.40963840122,3924.67539333308</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>386</ogr:id>
      <ogr:Hausnummer>46</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>38.72</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.372">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3006.92658533165,3924.96206894851 3005.04368535428,3920.31187658015 3001.50611569982,3921.68125838188 3003.10372780183,3926.33145075024 3006.92658533165,3924.96206894851</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>387</ogr:id>
      <ogr:Hausnummer>46</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>19.5</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.373">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3013.40765558348,3914.38708840659 3012.63737832001,3912.33301570399 3009.41362532845,3913.64533993065 3010.26948895452,3915.61382627063 3013.40765558348,3914.38708840659</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>388</ogr:id>
      <ogr:Hausnummer>48</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>7.43</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.374">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3009.84155714148,3916.09881565874 3008.78599200265,3913.8450414434 3005.84752688645,3915.04325051991 3006.7604480876,3917.38261109786 3009.84155714148,3916.09881565874</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>389</ogr:id>
      <ogr:Hausnummer>48</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>8.13</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.375">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3006.0472283992,3917.52525503554 3005.3340087108,3915.12883688252 3002.28142844445,3916.46968989671 3003.108763283,3918.63787774944 3006.0472283992,3917.52525503554</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>390</ogr:id>
      <ogr:Hausnummer>48</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>7.78</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.376">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3013.12236770812,3916.52674747178 3012.75149347015,3915.2714808202 3010.44066167974,3916.24145959642 3010.92565106785,3917.29702473525 3013.12236770812,3916.52674747178</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>391</ogr:id>
      <ogr:Hausnummer>48</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>2.98</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.377">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3014.2920479971,3918.72346411205 3013.56456391493,3917.12585201004 3011.09682379307,3917.98171563612 3011.838572269,3919.57932773813 3014.2920479971,3918.72346411205</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>392</ogr:id>
      <ogr:Hausnummer>48</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>4.56</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.378">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3011.06829500553,3919.47947698175 3010.055523048,3916.94041489105 3007.84454201397,3917.69642776076 3008.99995790917,3920.37813378914 3011.06829500553,3919.47947698175</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>393</ogr:id>
      <ogr:Hausnummer>48</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>6.48</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.379">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3008.30100261454,3920.46372015175 3007.18837990064,3917.89612927351 3003.36552237082,3919.49374137552 3004.36402993458,3922.28956255405 3008.30100261454,3920.46372015175</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>394</ogr:id>
      <ogr:Hausnummer>48</ogr:Hausnummer>
      <ogr:Raumnummer>7</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>12.21</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.380">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3002.42407238213,3915.44265354542 3004.9346056853,3914.41561719412 3005.87605567398,3912.50418842921 3003.16582085807,3911.81949752835 3002.42407238213,3915.44265354542</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>395</ogr:id>
      <ogr:Hausnummer>48</ogr:Hausnummer>
      <ogr:Raumnummer>8</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>7.08</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.381">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3005.96164203659,3913.95915659355 3008.9856935154,3912.84653387964 3009.09980866555,3911.39156571531 3006.53221778731,3911.24892177763 3005.96164203659,3913.95915659355</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>396</ogr:id>
      <ogr:Hausnummer>48</ogr:Hausnummer>
      <ogr:Raumnummer>9</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>5.66</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.382">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3003.36552237082,3922.54632164187 3016.23200554954,3918.23847472394 3013.35059800841,3911.10627783995 3001.31144966823,3916.18440202135 3003.36552237082,3922.54632164187</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>397</ogr:id>
      <ogr:Hausnummer>48</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>95.6</ogr:qm2>
      <ogr:perim>40.9</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.383">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3003.36552237082,3922.54632164187 3016.23200554954,3918.23847472394 3013.35059800841,3911.10627783995 3009.67723672182,3912.65570511249 3009.72744199134,3910.87804753966 3006.33251627456,3910.47864451416 3005.7334117363,3911.79096874081 3002.36701480706,3911.22039299009 3001.31144966823,3916.18440202135 3003.36552237082,3922.54632164187</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>398</ogr:id>
      <ogr:Hausnummer>48</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>120.71</ogr:qm2>
      <ogr:perim>46.94</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.384">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3013.17942528319,3928.02384884878 3017.68697371388,3926.36917917169 3016.31759191215,3922.91719587984 3011.91625238969,3924.81651751196 3013.17942528319,3928.02384884878</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>399</ogr:id>
      <ogr:Hausnummer>52</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>17.17</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.385">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3010.98270864292,3924.2295201065 3014.23499042202,3923.14542618013 3012.6302753201,3919.44437553247 3009.37073818711,3920.53570614684 3010.98270864292,3924.2295201065</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>400</ogr:id>
      <ogr:Hausnummer>52</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>13.79</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.386">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3012.62311392624,3928.83691929355 3018.75680324647,3927.03960567879 3016.93096084417,3922.30382694782 3014.90541692912,3922.81734512346 3013.33633361464,3918.08156639249 3008.30100261454,3920.46372015175 3009.57053365989,3923.50203602433 3011.4819624248,3926.04109811503 3012.62311392624,3928.83691929355</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>401</ogr:id>
      <ogr:Hausnummer>52</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>58.06</ogr:qm2>
      <ogr:perim>33.52</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.387">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2991.067993725,4006.27758631284 2994.45727921452,4000.74989352779 2990.56290383731,3998.28359755947 2986.81672456259,4003.82858760813 2991.067993725,4006.27758631284</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>402</ogr:id>
      <ogr:Hausnummer>54</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>31.32</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.388">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2991.01902962115,3997.67119944746 2995.86672379958,4000.67019669344 2998.56112154466,3997.31254498015 2997.55844551442,3996.65994956706 2995.90999132673,3998.80079916145 2991.91791840373,3996.43522737141 2991.01902962115,3997.67119944746</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>403</ogr:id>
      <ogr:Hausnummer>54</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>12.18</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.389">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2986.12008575609,4003.31172655815 2988.72686322557,3999.58133811044 2985.94030799958,3997.71614388659 2983.2661138714,4001.62631009081 2986.12008575609,4003.31172655815</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>404</ogr:id>
      <ogr:Hausnummer>54</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>15.46</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.390">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2982.70430838229,4001.24428235821 2985.53580804741,3997.33411615399 2984.23241931267,3996.45769959098 2981.42339186711,4000.45775467346 2982.70430838229,4001.24428235821</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>405</ogr:id>
      <ogr:Hausnummer>54</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>7.46</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.391">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2987.68793176534,3997.7919744747 2988.21967230278,3996.85793786871 2991.19880737766,3992.18797787372 2988.5249552626,3990.51332568371 2984.66591453847,3995.92093709525 2987.68793176534,3997.7919744747</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>406</ogr:id>
      <ogr:Hausnummer>54</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>22.06</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.392">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2995.670779071,3997.5813105692 2996.9966400253,3995.94083854099 2991.8729739646,3992.4576445085 2988.73764964215,3997.17674200575 2989.80586941778,3997.9218500995 2991.57889597498,3995.04747214259 2995.670779071,3997.5813105692</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>407</ogr:id>
      <ogr:Hausnummer>54</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>17.74</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.393">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2990.99655740158,4007.44661495801 2995.61777769346,4001.01658381916 2999.7208747699,3996.99578283413 2988.34483549297,3989.01939491513 2980.25483644976,4000.66000464954 2990.99655740158,4007.44661495801</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>408</ogr:id>
      <ogr:Hausnummer>54</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>180.31</ogr:qm2>
      <ogr:perim>54.29</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.394">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2997.33372331877,3991.19920021288 2998.81688981003,3989.53625596511 2996.12022346229,3987.04183959345 2994.54716809277,3988.72725606079 2997.33372331877,3991.19920021288</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>409</ogr:id>
      <ogr:Hausnummer>55</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>8.38</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.395">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2999.31127864045,3988.74972828036 3002.61469491643,3984.45753434354 3000.00791744695,3981.53614580015 2996.3674178775,3986.27778412826 2999.31127864045,3988.74972828036</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>410</ogr:id>
      <ogr:Hausnummer>55</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>21.9</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.396">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2993.94041816453,3988.39017276732 2995.62583463187,3986.32272856739 2993.64827931019,3984.77214541744 2992.16511281894,3986.54745076303 2993.94041816453,3988.39017276732</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>411</ogr:id>
      <ogr:Hausnummer>55</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>6.29</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.397">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2995.82317001471,3985.78804087811 2999.42363973827,3981.13164584799 2997.80563992963,3979.55859047848 2994.32244589713,3984.32270102615 2995.82317001471,3985.78804087811</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>412</ogr:id>
      <ogr:Hausnummer>55</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>12.73</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.398">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2997.42361219703,3992.18797787372 3003.69336145552,3984.12045105007 2997.91800102745,3978.45745171982 2990.95161296246,3986.8845340565 2997.42361219703,3992.18797787372</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>413</ogr:id>
      <ogr:Hausnummer>55</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>86.79</ogr:qm2>
      <ogr:perim>37.51</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.399">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3014.14294355301,3985.40136756525 3018.43513748983,3981.53614580015 3016.45758216815,3979.08667386762 3011.82830493787,3982.95189563272 3014.14294355301,3985.40136756525</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>414</ogr:id>
      <ogr:Hausnummer>56</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>19.22</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.400">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3011.06424947268,3982.41256236317 3016.09802665512,3978.5922850372 3014.00811023562,3976.50236861771 3009.53613854229,3980.43500704149 3011.06424947268,3982.41256236317</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>415</ogr:id>
      <ogr:Hausnummer>56</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>16.68</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.401">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3013.80586025954,3986.39014522608 3019.22166517459,3981.69345133711 3013.82143570326,3975.29387364221 3008.14286092929,3980.16534040672 3013.80586025954,3986.39014522608</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>416</ogr:id>
      <ogr:Hausnummer>56</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>61.48</ogr:qm2>
      <ogr:perim>31.36</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.402">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3037.17930971792,3993.18677249083 3041.60399586082,3996.12061629751 3044.34560664769,3991.62617238461 3040.83994039563,3989.06433935426 3037.17930971792,3993.18677249083</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>417</ogr:id>
      <ogr:Hausnummer>58</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>25.88</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.403">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3025.42399777438,3982.36761792404 3037.01966306966,3992.21045009329 3040.00846827174,3988.5924227434 3038.77552084431,3987.67471755594 3038.12071266211,3988.38257309841 3028.14313634169,3979.76084045456 3025.42399777438,3982.36761792404</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>418</ogr:id>
      <ogr:Hausnummer>58</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>56.8</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.404">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3044.9074121368,3990.63739472377 3046.71209260964,3987.64817764426 3044.61527328246,3985.80586751741 3042.24774578112,3988.84955202493 3044.9074121368,3990.63739472377</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>419</ogr:id>
      <ogr:Hausnummer>58</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>10.96</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.405">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3041.71635695864,3988.43511720645 3043.8961622564,3985.51372866307 3041.78377361734,3983.89572885443 3039.33430168481,3986.83958961737 3041.71635695864,3988.43511720645</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>420</ogr:id>
      <ogr:Hausnummer>58</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>10.31</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.406">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3047.04227299543,3986.50250632391 3049.43923983309,3983.07328225087 3047.35688406933,3981.82828465449 3045.06471767375,3985.06428427178 3047.04227299543,3986.50250632391</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>421</ogr:id>
      <ogr:Hausnummer>58</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>9.91</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.407">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3044.43549552595,3984.70472875875 3046.90743967804,3981.42378470233 3044.97482879549,3979.58106269804 3042.36805132601,3983.04178451098 3044.43549552595,3984.70472875875</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>422</ogr:id>
      <ogr:Hausnummer>58</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>11.19</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.408">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3037.96349629137,3986.50250632391 3042.37596159334,3981.47841216819 3035.85365777779,3976.07343153166 3031.4914970568,3981.19906250669 3037.96349629137,3986.50250632391</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>423</ogr:id>
      <ogr:Hausnummer>58</ogr:Hausnummer>
      <ogr:Raumnummer>7</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>56.46</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.409">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3043.08545093842,3980.6210860601 3045.51608182499,3978.19245548876 3043.08598858377,3975.93172385687 3040.75005151737,3978.61475725677 3043.08545093842,3980.6210860601</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>424</ogr:id>
      <ogr:Hausnummer>58</ogr:Hausnummer>
      <ogr:Raumnummer>8</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>11.17</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.410">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3040.09835715,3977.9630628894 3042.23321800862,3975.53606317643 3038.54777400005,3972.8393968287 3036.6151631175,3975.22145210253 3040.09835715,3977.9630628894</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>425</ogr:id>
      <ogr:Hausnummer>58</ogr:Hausnummer>
      <ogr:Raumnummer>9</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>14.15</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.411">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3049.85462182709,3982.22862762098 3052.92999452132,3978.68217391546 3049.23465277551,3975.06362093089 3045.73888426068,3979.31139606327 3049.85462182709,3982.22862762098</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>426</ogr:id>
      <ogr:Hausnummer>58</ogr:Hausnummer>
      <ogr:Raumnummer>10</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>25.96</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.412">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3046.00855089546,3977.15406298508 3047.81132277697,3974.76434211889 3045.44128512254,3972.77261009386 3043.70537369851,3975.20998189516 3046.00855089546,3977.15406298508</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>427</ogr:id>
      <ogr:Hausnummer>58</ogr:Hausnummer>
      <ogr:Raumnummer>11</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>9.12</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.413">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3042.63771796079,3974.77200771124 3044.92988435636,3972.34500799827 3041.37927366517,3969.73823052879 3039.26688502611,3972.12028580263 3042.63771796079,3974.77200771124</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>428</ogr:id>
      <ogr:Hausnummer>58</ogr:Hausnummer>
      <ogr:Raumnummer>12</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>14.11</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.414">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3053.64910554739,3977.55856293724 3056.9300496038,3973.82817448953 3052.88505008219,3970.95173038528 3049.69399490404,3974.68211883298 3053.64910554739,3977.55856293724</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>429</ogr:id>
      <ogr:Hausnummer>58</ogr:Hausnummer>
      <ogr:Raumnummer>13</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>24.23</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.415">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3048.88499499972,3974.18773000257 3051.49579731352,3971.11665813895 3044.30066220856,3966.00784208109 3041.73882917821,3969.01911950273 3048.88499499972,3974.18773000257</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>430</ogr:id>
      <ogr:Hausnummer>58</ogr:Hausnummer>
      <ogr:Raumnummer>14</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>35.1</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.416">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3041.67141251951,3997.37906059312 3050.20918272135,3983.57527877496 3058.32494914865,3973.81006582459 3044.45796774551,3964.61456446809 3030.12069166336,3980.79456255452 3027.60380307214,3978.77206279372 3022.32005589642,3984.01305432933 3041.67141251951,3997.37906059312</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>431</ogr:id>
      <ogr:Hausnummer>58</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>3</ogr:Phase>
      <ogr:qm2>533.2</ogr:qm2>
      <ogr:perim>101.11</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.417">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3054.2109110365,3988.81714493905 3056.52554965164,3984.90697873482 3054.30079991476,3983.82831219573 3052.25582793439,3987.62611730213 3054.2109110365,3988.81714493905</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>432</ogr:id>
      <ogr:Hausnummer>59</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>10.53</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.418">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3051.64907800615,3987.33397844779 3053.78393886478,3983.55864556096 3051.8063835431,3982.4575068023 3049.55916158665,3986.07553415218 3051.64907800615,3987.33397844779</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>433</ogr:id>
      <ogr:Hausnummer>59</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>10.1</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.419">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3056.88510516468,3984.1653954892 3060.45818807543,3978.54734059807 3055.60819274362,3975.33110763193 3052.1884112757,3981.67097911754 3056.88510516468,3984.1653954892</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>434</ogr:id>
      <ogr:Hausnummer>59</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>38.52</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.420">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3054.61541098866,3989.87333925858 3061.91888234712,3978.34509062199 3055.24463313647,3974.18773000256 3048.45768530936,3986.40708234412 3054.61541098866,3989.87333925858</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>435</ogr:id>
      <ogr:Hausnummer>59</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>103.04</ogr:qm2>
      <ogr:perim>42.44</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.421">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3065.98635408829,3991.51381128679 3067.96390940997,3988.61489496297 3064.86274311007,3987.01936737389 3063.28968774056,3989.94075591727 3065.98635408829,3991.51381128679</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>436</ogr:id>
      <ogr:Hausnummer>60</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>11.25</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.422">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3062.61552115362,3989.51378374555 3064.36835427965,3986.72722851955 3061.46943795583,3985.15417315004 3060.07616034283,3988.09803391299 3062.61552115362,3989.51378374555</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>437</ogr:id>
      <ogr:Hausnummer>60</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>10.14</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.423">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3059.26716043851,3987.55870064344 3060.84021580803,3984.81708985657 3058.56501353314,3983.63904354991 3056.75515695345,3986.49583255108 3059.26716043851,3987.55870064344</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>438</ogr:id>
      <ogr:Hausnummer>60</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>8.6</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.424">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3061.94135456669,3984.4800065631 3063.98632654706,3980.88445143278 3061.91888234712,3979.44622938065 3059.69413261024,3983.31145114575 3061.94135456669,3984.4800065631</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>439</ogr:id>
      <ogr:Hausnummer>60</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>10.83</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.425">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3064.30093762096,3981.82828465449 3062.63799337318,3984.99686761308 3066.66052067523,3986.61486742173 3067.71671499476,3983.94067329355 3064.30093762096,3981.82828465449</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>440</ogr:id>
      <ogr:Hausnummer>60</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>13.4</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.426">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3066.05377074699,3992.57000560632 3069.04257594907,3988.16545057168 3067.42457614042,3987.06431181302 3068.99763150994,3983.94067329355 3061.91888234712,3978.34509062199 3056.37617429271,3987.09404210481 3066.05377074699,3992.57000560632</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>441</ogr:id>
      <ogr:Hausnummer>60</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>99.31</ogr:qm2>
      <ogr:perim>41.17</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.427">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3059.7166048298,3997.22175505617 3062.87437848699,3990.77098081179 3059.35704931677,3989.44636708685 3056.9300496038,3996.00825519969 3059.7166048298,3997.22175505617</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>442</ogr:id>
      <ogr:Hausnummer>61</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>24.05</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.428">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3055.9862163821,3995.51386636927 3057.24466067771,3992.50258894762 3055.08732759952,3991.3789779694 3053.60416110826,3994.65992202581 3055.9862163821,3995.51386636927</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>443</ogr:id>
      <ogr:Hausnummer>61</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>8.5</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.429">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3057.64916062987,3991.87336679982 3058.95254936461,3989.17670045208 3056.70532740816,3988.14297835211 3055.35699423429,3990.97447801724 3057.64916062987,3991.87336679982</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>444</ogr:id>
      <ogr:Hausnummer>61</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>7.55</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.430">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3052.25582793439,3995.42397749101 3060.21099366022,3998.88469930394 3063.69144239871,3991.23330760751 3056.37617429271,3987.09404210481 3052.25582793439,3995.42397749101</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>445</ogr:id>
      <ogr:Hausnummer>61</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>75.45</ogr:qm2>
      <ogr:perim>34.69</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.431">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3049.31452851355,3999.37322550687 3051.451210771,3994.4766046368 3046.25574531067,3991.6935890433 3043.91863447596,3996.95208842139 3049.31452851355,3999.37322550687</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>446</ogr:id>
      <ogr:Hausnummer>62</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>32.71</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.432">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3051.86915077705,3993.570406829 3053.97858642931,3989.51486315115 3052.21853211448,3988.5241076748 3050.21085595402,3992.90708889978 3051.86915077705,3993.570406829</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>447</ogr:id>
      <ogr:Hausnummer>62</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>8.91</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.433">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3049.67152268447,3992.59247782588 3051.70003187516,3988.23223773234 3048.33946093979,3986.5374761327 3046.54205235372,3991.20656953655 3049.67152268447,3992.59247782588</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>448</ogr:id>
      <ogr:Hausnummer>62</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>17.6</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.434">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3051.86915077705,3993.570406829 3053.97858642931,3989.51486315115 3052.21853211448,3988.5241076748 3050.21085595402,3992.90708889978 3051.86915077705,3993.570406829</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>449</ogr:id>
      <ogr:Hausnummer>62</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>8.91</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.435">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3051.63098852752,3994.61143795419 3045.30545439011,3991.50357789519 3042.16173379384,3996.58631404084 3048.61532836494,4000.55887966149 3051.63098852752,3994.61143795419</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>450</ogr:id>
      <ogr:Hausnummer>62</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>46.14</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.436">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3049.67152268447,3992.59247782588 3051.70003187516,3988.23223773234 3048.43799365866,3986.58716693568 3046.01583221589,3990.85226475375 3049.67152268447,3992.59247782588</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>451</ogr:id>
      <ogr:Hausnummer>62</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>18.68</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.437">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3042.99727347382,3997.48018558116 3050.00860597794,4000.62629632018 3054.94258308003,3989.99227471278 3047.76138402149,3985.92946472501 3042.99727347382,3997.48018558116</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>452</ogr:id>
      <ogr:Hausnummer>62</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>96.19</ogr:qm2>
      <ogr:perim>40.05</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.438">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3041.67141251951,3997.37906059312 3049.26702273231,4002.46901832447 3054.94258308003,3989.99227471278 3048.45768530936,3986.40708234412 3041.67141251951,3997.37906059312</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>453</ogr:id>
      <ogr:Hausnummer>62</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>109.57</ogr:qm2>
      <ogr:perim>43.05</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.439">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2926.91147031988,3937.34199903087 2929.88853671577,3936.72411732607 2926.65870053155,3929.95550410524 2924.10291711622,3930.51721474597 2926.91147031988,3937.34199903087</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>454</ogr:id>
      <ogr:Hausnummer>64</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>20.58</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.440">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2930.16939203613,3944.95317821281 2933.11837289998,3944.39146757207 2930.08513544003,3937.70711094735 2927.58552308876,3938.26882158808 2930.16939203613,3944.95317821281</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>455</ogr:id>
      <ogr:Hausnummer>64</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>19.79</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.441">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2930.92770140112,3936.38709094163 2934.01710992516,3935.76920923682 2930.78727374094,3929.30953686839 2927.95063500524,3929.95550410523 2930.92770140112,3936.38709094163</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>456</ogr:id>
      <ogr:Hausnummer>64</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>21.06</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.442">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2934.38222184163,3944.47572416818 2937.33120270548,3943.82975693134 2934.38222184163,3936.94880158236 2931.96686608648,3937.39817009494 2934.38222184163,3944.47572416818</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>457</ogr:id>
      <ogr:Hausnummer>64</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>20.19</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.443">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2935.14053120662,3935.6006960446 2937.38737376955,3935.01089987183 2934.94393248236,3928.12994452285 2931.96686608648,3929.02868154803 2935.14053120662,3935.6006960446</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>458</ogr:id>
      <ogr:Hausnummer>64</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>19.66</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.444">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2938.31419632676,3943.43655948283 2940.84189421006,3942.70633564987 2937.8086567501,3936.02197902515 2935.67415631532,3936.55560413385 2938.31419632676,3943.43655948283</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>459</ogr:id>
      <ogr:Hausnummer>64</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>17.6</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.445">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2938.6512227112,3934.7581300835 2941.7125457032,3934.2245049748 2939.12867675583,3927.51206281805 2936.32012355216,3928.18611558693 2938.6512227112,3934.7581300835</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>460</ogr:id>
      <ogr:Hausnummer>64</ogr:Hausnummer>
      <ogr:Raumnummer>7</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>20.98</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.446">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2942.330427408,3942.59399352173 2945.67260572036,3941.86376968877 2942.24617081189,3935.12324199998 2939.1848478199,3935.74112370478 2942.330427408,3942.59399352173</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>461</ogr:id>
      <ogr:Hausnummer>64</ogr:Hausnummer>
      <ogr:Raumnummer>8</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>23.98</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.447">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2942.94830911281,3933.77513646222 2946.2343163611,3933.15725475741 2943.3976776254,3926.75375345306 2940.28018356933,3927.31546409379 2942.94830911281,3933.77513646222</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>462</ogr:id>
      <ogr:Hausnummer>64</ogr:Hausnummer>
      <ogr:Raumnummer>9</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>22.22</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.448">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2946.90836912998,3941.69525649655 2949.82926446179,3941.04928925971 2946.99262572609,3934.33684710295 2943.53810528558,3934.73004455146 2946.90836912998,3941.69525649655</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>463</ogr:id>
      <ogr:Hausnummer>64</ogr:Hausnummer>
      <ogr:Raumnummer>10</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>23.41</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.449">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2947.694764027,3932.73597177686 2951.00885680733,3931.89340581576 2947.58242189886,3925.4056479153 2944.66152656705,3926.52906919676 2947.694764027,3932.73597177686</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>464</ogr:id>
      <ogr:Hausnummer>64</ogr:Hausnummer>
      <ogr:Raumnummer>11</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>22.96</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.450">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2951.0650278714,3940.45949308694 2954.18252192747,3939.70118372195 2951.28971212769,3932.90448496908 2948.05987594348,3933.60662327 2951.0650278714,3940.45949308694</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>465</ogr:id>
      <ogr:Hausnummer>64</ogr:Hausnummer>
      <ogr:Raumnummer>12</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>23.81</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.451">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2929.46725373522,3946.30128375056 2956.31702236225,3940.9650326636 2948.0994059654,3923.71010286857 2922.22118646976,3929.36570793246 2929.46725373522,3946.30128375056</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>466</ogr:id>
      <ogr:Hausnummer>64</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>493.19</ogr:qm2>
      <ogr:perim>91.15</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.452">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2950.41924495412,3915.20574332406 2952.68287320766,3915.10166846183 2952.74791999656,3912.82503085051 2950.38021688079,3913.15026479498 2950.41924495412,3915.20574332406</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>467</ogr:id>
      <ogr:Hausnummer>66</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>5.01</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.453">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2953.29431302328,3914.80245323291 2956.36452145912,3914.49022864622 2956.3775308169,3912.31766589713 2953.41139724329,3912.69493727272 2953.29431302328,3914.80245323291</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>468</ogr:id>
      <ogr:Hausnummer>66</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>6.44</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.454">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2957.04100806363,3914.39916314176 2958.51106549265,3914.16499470174 2958.15981283262,3912.04446938377 2956.92392384361,3912.20058167711 2957.04100806363,3914.39916314176</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>469</ogr:id>
      <ogr:Hausnummer>66</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>2.97</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.455">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2959.31764567495,3914.7504158018 2958.83774330976,3911.33280266431 2949.96391743186,3912.44775947492 2950.08100165187,3915.9212580019 2959.31764567495,3914.7504158018</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>470</ogr:id>
      <ogr:Hausnummer>66</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>31.54</ogr:qm2>
      <ogr:perim>25.11</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.456">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2978.75470050112,4006.38636645926 2980.47795559095,4002.71508387657 2976.28220406788,4000.9169046524 2975.88260868473,4002.29051378198 2975.50798801303,4004.71306079232 2978.75470050112,4006.38636645926</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>471</ogr:id>
      <ogr:Hausnummer>70</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>16.29</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.457">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2976.45702704801,4000.24258744334 2980.57785443674,4002.16564022474 2980.8276015512,4001.7160954187 2982.50090721814,4002.59021031934 2983.27512327299,4000.81700580661 2977.53093964022,3998.1447116818 2976.45702704801,4000.24258744334</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>472</ogr:id>
      <ogr:Hausnummer>70</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>14.22</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.458">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2982.95045202418,4003.28950223985 2985.12325192006,4003.88889531458 2985.97239210925,4002.0907160904 2983.74964279048,4001.24157590121 2982.95045202418,4003.28950223985</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>473</ogr:id>
      <ogr:Hausnummer>70</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>4.82</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.459">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2979.02942232703,4007.210531937 2981.02739924278,4002.54026089645 2982.10131183499,4003.3144769513 2982.02638770065,4003.56422406577 2985.37299903453,4004.76301021521 2986.796557587,4001.79101955304 2977.25621781431,3997.09577380104 2975.50798801303,4000.81700580661 2975.45803859014,4001.66614599581 2975.23326618711,4002.49031147355 2974.88362022686,4005.36240328994 2979.02942232703,4007.210531937</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>474</ogr:id>
      <ogr:Hausnummer>70</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>62.52</ogr:qm2>
      <ogr:perim>37.3</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.460">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2978.51610038701,3991.08622068722 2986.14137946208,3994.92169040163 2986.80345459136,3993.59754014308 2979.7260997612,3989.53376865996 2978.51610038701,3991.08622068722</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>475</ogr:id>
      <ogr:Hausnummer>72</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>14.27</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.461">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2980.15987312176,3988.73471246946 2983.17345646879,3990.56112661918 2984.86288955728,3988.3009391089 2982.16892868645,3986.31471372109 2980.15987312176,3988.73471246946</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>476</ogr:id>
      <ogr:Hausnummer>72</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>10.2</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.462">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2982.46572098578,3985.85811018366 2985.13685167974,3987.9813166327 2987.3285486594,3985.15037470064 2985.22817238723,3982.27377241484 2982.46572098578,3985.85811018366</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>477</ogr:id>
      <ogr:Hausnummer>72</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>13.85</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.463">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2983.83553159806,3990.94923962599 2987.67100131247,3993.52904961247 2986.689303707,3995.21848270096 2988.24175573426,3995.94904836085 2991.3238296119,3990.83508874164 2987.44269954376,3986.33754389796 2983.83553159806,3990.94923962599</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>478</ogr:id>
      <ogr:Hausnummer>72</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>33.8</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.464">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2991.20967872755,3997.50150038811 2994.06345083648,3994.87603004789 2991.55213138062,3991.56565440152 2988.78967997917,3996.24584066018 2991.20967872755,3997.50150038811</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>479</ogr:id>
      <ogr:Hausnummer>72</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>15.42</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.465">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2993.8123188909,3998.66583940855 2995.45609162564,3996.72527437448 2994.40590348955,3995.40112411593 2992.25986686364,3997.52433056498 2993.8123188909,3998.66583940855</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>480</ogr:id>
      <ogr:Hausnummer>72</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>4.98</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.466">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2993.85797924464,3999.41923524531 2996.25514781614,3996.56546313638 2991.75760297246,3991.13188104097 2991.91741421056,3990.60678697292 2987.74611817212,3985.43630150545 2988.17450360003,3985.1982637384 2985.2029257454,3981.17274672483 2977.38130359369,3991.47586656181 2991.3238296119,3998.20923587112 2991.78043314933,3997.95810392554 2993.85797924464,3999.41923524531</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>481</ogr:id>
      <ogr:Hausnummer>72</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>146.49</ogr:qm2>
      <ogr:perim>54.8</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.467">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2993.13581726628,3985.04865853552 2996.93680169804,3981.24767410376 2994.04081355956,3978.14052016351 2990.11061266119,3982.2342530215 2993.13581726628,3985.04865853552</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>482</ogr:id>
      <ogr:Hausnummer>73</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>23.13</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.468">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2997.26863367224,3980.64434324158 2999.86295637964,3978.08018707729 2997.60046564644,3976.20986140452 2994.22181281821,3977.68802201687 2997.26863367224,3980.64434324158</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>483</ogr:id>
      <ogr:Hausnummer>73</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>12.57</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.469">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2994.01064701645,3989.12114185527 2996.06197194787,3987.40164889804 2994.1916462751,3984.83749273376 2992.5324864041,3986.73798494964 2994.01064701645,3989.12114185527</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>484</ogr:id>
      <ogr:Hausnummer>73</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>7.62</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.470">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2996.7859689825,3992.8316266577 2998.83729391392,3990.8708013556 2996.39380392208,3987.85414704468 2994.31231244754,3989.72447271745 2996.7859689825,3992.8316266577</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>485</ogr:id>
      <ogr:Hausnummer>73</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>11.04</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.471">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2999.25962551745,3990.35797012275 3003.81477352694,3985.41065705283 3000.77838872298,3982.41849206942 3002.72877797501,3980.70467632779 3000.18129731586,3978.52653734595 2994.61397787863,3984.38499458712 2999.25962551745,3990.35797012275</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>486</ogr:id>
      <ogr:Hausnummer>73</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>54.19</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.472">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3001.67294896619,3982.695668173 3005.74543228594,3986.43631951855 3007.22359289829,3984.74699310443 3003.18127612165,3981.24767410376 3001.67294896619,3982.695668173</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>487</ogr:id>
      <ogr:Hausnummer>73</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>11.77</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.473">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2999.86295637964,3996.14994639972 3000.79811921602,3995.15445047711 2998.08313033619,3992.52996122661 2997.32896675846,3993.31429134745 2999.86295637964,3996.14994639972</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>488</ogr:id>
      <ogr:Hausnummer>73</ogr:Hausnummer>
      <ogr:Raumnummer>7</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>4.64</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.474">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3002.06511402661,3998.74426910711 3003.3321088372,3997.89960590005 3001.43161662132,3995.54661553753 3000.28528798316,3996.7532772619 3002.06511402661,3998.74426910711</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>489</ogr:id>
      <ogr:Hausnummer>73</ogr:Hausnummer>
      <ogr:Raumnummer>8</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>4.51</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.475">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3001.67294896619,3994.94328467535 3004.20693858737,3992.01712999375 3001.19028427644,3989.09097531216 2998.50546193972,3992.19812925241 3001.67294896619,3994.94328467535</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>490</ogr:id>
      <ogr:Hausnummer>73</ogr:Hausnummer>
      <ogr:Raumnummer>9</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>16.73</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.476">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3003.60360772518,3997.44710775341 3004.9611021651,3996.33094565837 3002.72877797501,3994.67178578737 3002.09528056972,3995.4561159082 3003.60360772518,3997.44710775341</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>491</ogr:id>
      <ogr:Hausnummer>73</ogr:Hausnummer>
      <ogr:Raumnummer>10</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>3.59</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.477">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3004.50860401846,3991.41379913157 3005.20243450997,3990.71996864006 3003.54327463896,3989.2418080277 3005.17226796686,3986.9189842083 3004.23710513047,3985.83298865636 3001.52211625064,3988.60831062241 3004.50860401846,3991.41379913157</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>492</ogr:id>
      <ogr:Hausnummer>73</ogr:Hausnummer>
      <ogr:Raumnummer>11</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>8.61</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.478">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3005.50409994106,3995.96894714106 3007.97775647602,3993.19362517501 3005.59459957039,3990.96130098493 3003.24160920787,3994.09862146829 3005.50409994106,3995.96894714106</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>493</ogr:id>
      <ogr:Hausnummer>73</ogr:Hausnummer>
      <ogr:Raumnummer>12</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>11.82</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.479">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3007.97775647602,3992.43946159728 3010.63241226963,3988.09547938955 3007.46492524316,3985.07882507863 3005.65493265661,3987.2508161825 3004.29743821669,3989.27197457082 3007.97775647602,3992.43946159728</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>494</ogr:id>
      <ogr:Hausnummer>73</ogr:Hausnummer>
      <ogr:Raumnummer>13</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>23.97</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.480">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3002.24611328527,3999.52859922795 3008.89509208024,3993.50762991706 3008.6283032474,3992.71898712824 3011.56757510602,3987.97481321712 2997.74800205578,3975.30460368574 2994.01064701645,3977.02435806847 2989.28305035486,3982.30221630726 2992.59281949031,3985.07882507863 2991.51504352867,3986.53647637399 2994.79978178918,3991.81374373179 2996.72563589628,3993.73662295098 2997.02730132737,3993.55562369232 2998.92779354325,3996.02928022728 3002.24611328527,3999.52859922795</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>495</ogr:id>
      <ogr:Hausnummer>73</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>281.93</ogr:qm2>
      <ogr:perim>68.51</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.481">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3011.95974016644,3985.25982433729 3013.43790077879,3983.63083100939 3014.82556176181,3984.83749273376 3018.29471421937,3981.79067187973 3014.91606139114,3977.62768893066 3008.9129193124,3982.42416928502 3011.95974016644,3985.25982433729</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>496</ogr:id>
      <ogr:Hausnummer>75</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>36.48</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.482">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3017.96288224517,3980.31251126738 3021.55270087517,3977.98968744796 3018.86787853845,3974.97303313704 3015.428892624,3977.1148576978 3017.96288224517,3980.31251126738</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>497</ogr:id>
      <ogr:Hausnummer>75</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>16.74</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.483">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3016.90705323635,3989.75463926056 3017.78188298652,3988.63847716552 3013.55856695122,3984.53582730267 3012.5027379424,3985.80282211325 3016.90705323635,3989.75463926056</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>498</ogr:id>
      <ogr:Hausnummer>75</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>9.03</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.484">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3017.99304878828,3988.03514630334 3020.46670532324,3985.22965779418 3018.77737890912,3982.3638361988 3015.21772682223,3985.56148976838 3017.99304878828,3988.03514630334</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>499</ogr:id>
      <ogr:Hausnummer>75</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>14.75</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.485">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3019.19971051265,3991.1724667867 3021.49236778895,3988.21614556199 3019.71254174551,3986.70781840653 3017.32938483988,3990.05630469165 3019.19971051265,3991.1724667867</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>500</ogr:id>
      <ogr:Hausnummer>75</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>8.82</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.486">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3021.19070235786,3992.13779616619 3022.94036185819,3991.05180061426 3024.38835592744,3989.875305433 3025.63283798082,3988.18376314482 3020.70803766811,3985.74248902704 3020.04437371971,3986.22515371678 3022.36719753912,3988.00497976023 3019.83320791794,3991.41379913157 3021.19070235786,3992.13779616619</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>501</ogr:id>
      <ogr:Hausnummer>75</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>16.32</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.487">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3025.41401839315,3987.52231507048 3026.74134628995,3982.57500200057 3022.27669790979,3978.38185250838 3018.47571347803,3980.82534250023 3020.91920346987,3985.16932470796 3025.41401839315,3987.52231507048</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>502</ogr:id>
      <ogr:Hausnummer>75</ogr:Hausnummer>
      <ogr:Raumnummer>7</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>41.92</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.488">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3021.13036927164,3992.89195974392 3024.59744256645,3991.15878386144 3026.6245255368,3988.23362666553 3025.8966830829,3987.9144801309 3027.37484369525,3982.27333656948 3021.40186815962,3977.02435806847 3019.0488777971,3974.24903610242 3015.39872608089,3976.69252609427 3011.68824127845,3979.25668225855 3008.21803229712,3982.66743108033 3016.14124562312,3989.83347004903 3021.13036927164,3992.89195974392</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>503</ogr:id>
      <ogr:Hausnummer>75</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>202.29</ogr:qm2>
      <ogr:perim>55.81</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.489">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3033.58915157575,3988.54797753619 3037.17897020575,3984.71682656132 3035.09747873121,3982.78616780233 3034.19248243793,3983.72133063872 3033.40815231709,3982.93700051788 3035.36897761919,3981.24767410376 3031.26632775634,3977.20535732713 3027.64634258323,3983.14816631964 3033.58915157575,3988.54797753619</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>504</ogr:id>
      <ogr:Hausnummer>77</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>51.83</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.490">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3037.11863711953,3992.1679627093 3041.76428475835,3988.45747790687 3038.26496575768,3984.83749273376 3033.95115009306,3989.06080876905 3037.11863711953,3992.1679627093</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>505</ogr:id>
      <ogr:Hausnummer>77</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>28.29</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.491">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3042.39778216364,3987.85414704468 3043.84577623289,3986.64748532031 3039.47162748205,3982.39400274191 3038.74763044743,3983.20849940586 3039.41129439583,3983.72133063872 3038.9889627923,3984.23416187158 3042.39778216364,3987.85414704468</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>506</ogr:id>
      <ogr:Hausnummer>77</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>9.92</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.492">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3037.96330032659,3982.33366965569 3043.45361117246,3978.1103536204 3040.25595760289,3974.55070153351 3034.94664601566,3979.16618262923 3037.96330032659,3982.33366965569</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>507</ogr:id>
      <ogr:Hausnummer>77</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>31.89</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.493">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3044.63010635372,3985.59165631149 3048.00875918196,3983.02750014721 3043.69494351734,3978.47235213771 3040.13529143045,3981.76050533662 3044.63010635372,3985.59165631149</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>508</ogr:id>
      <ogr:Hausnummer>77</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>27.43</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.494">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3037.1004599627,3993.07656970427 3049.21985513134,3983.15237345533 3040.22579105978,3973.88703758511 3034.19248243793,3979.10584954301 3031.17582812701,3976.11936177519 3027.37484369525,3982.27333656948 3027.02332072597,3983.61486300326 3037.1004599627,3993.07656970427</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>509</ogr:id>
      <ogr:Hausnummer>77</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>226.12</ogr:qm2>
      <ogr:perim>63.07</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.495">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3044.66027289683,4002.24358810778 3049.7885852254,3996.66277763258 3043.12177919826,3991.1724667867 3038.29513230079,3995.51644899442 3044.66027289683,4002.24358810778</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>510</ogr:id>
      <ogr:Hausnummer>78</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>62.74</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.496">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3045.535102647,3992.52996122661 3048.06909226818,3989.69430617435 3045.38426993146,3988.21614556199 3043.21227882759,3990.44846975207 3045.535102647,3992.52996122661</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>511</ogr:id>
      <ogr:Hausnummer>78</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>10.53</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.497">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3050.09025065649,3996.11977985661 3053.34823731229,3992.71096048527 3048.61209004414,3989.90547197611 3046.07810042297,3992.86179320081 3050.09025065649,3996.11977985661</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>512</ogr:id>
      <ogr:Hausnummer>78</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>22.7</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.498">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3044.66027289683,4003.38991674593 3054.16273397624,3992.59029431283 3045.28806948868,3987.25894174605 3037.17897020575,3995.63711516686 3044.66027289683,4003.38991674593</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>513</ogr:id>
      <ogr:Hausnummer>78</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>136.03</ogr:qm2>
      <ogr:perim>47.05</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.499">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3346.98870789708,3342.39357225722 3347.2991634304,3341.9666958989 3346.17376212211,3340.06515575729 3344.34983586383,3340.22038352395 3346.98870789708,3342.39357225722</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>514</ogr:id>
      <ogr:Hausnummer>80</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>2.72</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.500">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3344.2722219805,3344.52795404881 3346.25137600544,3343.05329026553 3343.2632414972,3340.45322517394 3340.54675558062,3340.76368070727 3339.96465145564,3341.65624036557 3344.2722219805,3344.52795404881</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>515</ogr:id>
      <ogr:Hausnummer>80</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>13.23</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.501">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3346.00948165515,3351.80760213585 3348.15291614704,3348.36984127369 3339.227319564,3342.62641390721 3336.9765169474,3346.19665254043 3346.00948165515,3351.80760213585</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>516</ogr:id>
      <ogr:Hausnummer>80</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>43.94</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.502">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3343.12611913112,3355.8081649541 3345.16336672645,3352.62085823238 3336.58844753075,3347.0115983154 3334.41525879749,3350.23257447363 3343.12611913112,3355.8081649541</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>517</ogr:id>
      <ogr:Hausnummer>80</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>39.47</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.503">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3348.11410920538,3346.97279137373 3349.78280769699,3345.57574147378 3347.92007449705,3342.78164167387 3345.3976232888,3345.11005817379 3348.11410920538,3346.97279137373</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>518</ogr:id>
      <ogr:Hausnummer>80</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>9.14</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.504">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3350.79178818029,3349.84450505697 3352.34406584691,3348.09819268203 3350.40371876364,3346.46830113208 3349.12308968868,3347.51608855705 3349.58877298866,3347.98177185703 3349.39473828034,3348.95194539867 3350.79178818029,3349.84450505697</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>519</ogr:id>
      <ogr:Hausnummer>80</ogr:Hausnummer>
      <ogr:Raumnummer>7</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>5.94</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.505">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3356.76805719676,3353.65243620789 3357.97107238839,3352.21657936627 3354.05157128018,3349.61166340698 3353.04259079688,3348.83552457367 3351.52912007193,3350.46541612362 3354.78890317183,3352.40576320689 3356.76805719676,3353.65243620789</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>520</ogr:id>
      <ogr:Hausnummer>80</ogr:Hausnummer>
      <ogr:Raumnummer>8</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>11.93</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.506">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3351.61525730364,3360.77633772574 3355.94510933976,3354.26403199605 3354.78890317183,3353.64758534019 3348.64640293782,3349.56498683938 3344.2722219805,3356.24765043177 3350.24849099697,3360.08953765664 3351.61525730364,3360.77633772574</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>521</ogr:id>
      <ogr:Hausnummer>80</ogr:Hausnummer>
      <ogr:Raumnummer>9</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>68.49</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.507">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3336.70486835575,3355.78196713178 3338.3040855083,3356.64606517476 3339.61538898065,3354.88940747348 3337.90788354737,3353.95804087351 3336.70486835575,3355.78196713178</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>522</ogr:id>
      <ogr:Hausnummer>80</ogr:Hausnummer>
      <ogr:Raumnummer>10</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>4.09</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.508">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3339.18851262233,3357.25663091507 3340.62436946395,3358.11038363171 3341.90499853891,3356.3252643151 3340.31391393063,3355.39389771513 3339.18851262233,3357.25663091507</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>523</ogr:id>
      <ogr:Hausnummer>80</ogr:Hausnummer>
      <ogr:Raumnummer>11</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>3.83</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.509">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3341.55573606392,3358.69248775669 3342.95278596388,3359.46862659 3349.53723737013,3363.56000364284 3350.97938002279,3361.78847241136 3342.79755819722,3356.94617538174 3341.55573606392,3358.69248775669</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>524</ogr:id>
      <ogr:Hausnummer>80</ogr:Hausnummer>
      <ogr:Raumnummer>12</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>20.43</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.510">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3332.33635085382,3353.12975873488 3334.23152580688,3354.24259580898 3335.67297771551,3355.19168723341 3337.10652545632,3353.23551920501 3333.65898502259,3351.18732734113 3332.33635085382,3353.12975873488</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>525</ogr:id>
      <ogr:Hausnummer>80</ogr:Hausnummer>
      <ogr:Raumnummer>13</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>9.35</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.511">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3349.82161463865,3365.13929094086 3359.67857782167,3352.10015854128 3354.75009623016,3348.68029680701 3351.41269924694,3345.96381089043 3348.3081439137,3341.22936400725 3346.42310850812,3338.82093106877 3339.77061674731,3339.8323141073 3338.61343857634,3341.51068037259 3337.60396904406,3342.86858824177 3331.86315220289,3351.36244967438 3330.41814380595,3353.57482232456 3333.53715505179,3355.28702473783 3342.33187489723,3360.63283483996 3349.82161463865,3365.13929094086</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>526</ogr:id>
      <ogr:Hausnummer>80</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>413.68</ogr:qm2>
      <ogr:perim>81.1</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.512">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2897.81325784104,3735.23602072053 2898.25918744064,3734.34416152133 2898.25334190148,3734.29761267586 2896.73081250618,3732.98267218111 2895.9474787613,3733.76586861524 2897.07788186782,3734.69750428969 2897.81325784104,3735.23602072053</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>527</ogr:id>
      <ogr:Hausnummer>82</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>2.36</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.513">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2895.6077684183,3733.53286976216 2896.25387726716,3732.75399882108 2895.29548779026,3731.93501144991 2894.57961547794,3732.7268486299 2895.6077684183,3733.53286976216</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>528</ogr:id>
      <ogr:Hausnummer>82</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>1.33</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.514">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2896.59777968872,3732.1928948701 2899.73234737418,3727.86935323498 2899.25328233873,3727.55723510582 2895.70134292157,3731.48608895754 2896.59777968872,3732.1928948701</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>529</ogr:id>
      <ogr:Hausnummer>82</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>4.54</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.515">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2898.65390592254,3733.7890534973 2899.918444507,3732.02087145891 2900.82524931441,3730.5571342812 2901.5287505396,3729.00882968031 2900.1031212698,3728.11091804577 2897.02313268382,3732.49154697304 2898.42372391114,3733.64235122988 2898.65390592254,3733.7890534973</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>530</ogr:id>
      <ogr:Hausnummer>82</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>10.97</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.516">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2898.02199084511,3735.87170759655 2898.72026009023,3734.8188347209 2901.81798447888,3730.13676840415 2901.82389065249,3728.50689352438 2899.69351342812,3727.00298929616 2899.53000033578,3727.08865892892 2893.7832098701,3732.75399882108 2896.82652000363,3735.01779985264 2898.02199084511,3735.87170759655</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>531</ogr:id>
      <ogr:Hausnummer>82</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>35.46</ogr:qm2>
      <ogr:perim>24.57</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.517">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2952.7279528598,3839.74808938341 2949.01829874871,3843.977261715 2943.17142277985,3849.25371076006 2948.62020689234,3857.11735699065 2950.92142203009,3855.7783773853 2960.19780071129,3848.22596598366 2952.7279528598,3839.74808938341</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>50</ogr:id>
      <ogr:Hausnummer>3</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>145.66</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.518">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2951.84024227683,3844.12472812995 2951.23095641309,3843.04875522165 2950.4882259264,3843.98556492096 2951.26020394999,3844.73029740662 2951.84024227683,3844.12472812995</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>45</ogr:id>
      <ogr:Hausnummer>3</ogr:Hausnummer>
      <ogr:Raumnummer>3a</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>1.13</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.519">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2941.7783613479,3909.85383184372 2945.10554549341,3910.29745639646 2945.30271196129,3908.57224980249 2941.95088200729,3908.10397944127 2941.7783613479,3909.85383184372</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>532</ogr:id>
      <ogr:Hausnummer>65</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>5.89</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.520">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2945.89421136494,3910.00170669464 2948.45737544741,3910.32210220494 2948.65454191529,3908.8926453128 2946.06673202434,3908.64618722795 2945.89421136494,3910.00170669464</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>533</ogr:id>
      <ogr:Hausnummer>65</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>3.64</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.521">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2945.4505868122,3914.06826509471 2948.1369799371,3914.31472317956 2948.35879221347,3911.11076807647 2945.795628131,3910.93824741708 2945.4505868122,3914.06826509471</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>534</ogr:id>
      <ogr:Hausnummer>65</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>8.37</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.522">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2944.46475447279,3914.09291090319 2944.98231645098,3911.08612226799 2942.51773560245,3910.7410809492 2941.95088200729,3913.9203902438 2944.46475447279,3914.09291090319</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>535</ogr:id>
      <ogr:Hausnummer>65</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>7.84</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.523">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2941.45796583759,3913.82180700986 2939.7574050521,3913.9203902438 2939.31425029691,3914.84965806219 2948.83522402861,3914.72148677379 2949.51848166128,3907.68600324408 2941.28544517819,3907.26602195277 2940.71859158303,3912.21982945831 2941.55654907153,3912.2444752668 2941.45796583759,3913.82180700986</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>536</ogr:id>
      <ogr:Hausnummer>65</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>60.36</ogr:qm2>
      <ogr:perim>34.8791872150234</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.524">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2971.20872230442,3888.86997150967 2967.42057642772,3890.21988385912 2970.1830948927,3897.72612136412 2974.02586976229,3896.48479946823 2971.20872230442,3888.86997150967</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>150</ogr:id>
      <ogr:Hausnummer>8</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>32.46</ogr:qm2>
      <ogr:perim>24.1133168357965</ogr:perim>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.525">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2978.59944725224,4008.45948255092 2983.35139981568,4008.34634082322 2985.37299903453,4004.76301021521 2980.74923619337,4003.19046702443 2978.59944725224,4008.45948255092</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>537</ogr:id>
      <ogr:Hausnummer>71</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>0</ogr:Phase>
      <ogr:qm2>22.27</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.526">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2999.94551987849,3973.80039996519 3001.83121534017,3976.13866233767 3008.53530139534,3982.35559821702 3010.43780700673,3980.48569101333 3003.94319425726,3972.02784623121 2999.94551987849,3973.80039996519</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>538</ogr:id>
      <ogr:Hausnummer>74</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>0</ogr:Phase>
      <ogr:qm2>40.81</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.527">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3007.71458518062,3996.95674023466 3010.80712573778,3997.63559060086 3014.26549778122,4000.67012881881 3010.61855619161,4006.1966479969 3011.25969264859,4006.72464272617 3017.74648503678,4001.8218345258 3017.48248767214,4001.59555107039 3023.62985487723,3996.69274287002 3011.97625692403,3989.90423920796 3007.71458518062,3996.95674023466</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>539</ogr:id>
      <ogr:Hausnummer>76</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>0</ogr:Phase>
      <ogr:qm2>108.43</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.528">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3054.22036792816,3991.4523474899 3060.92891110932,3978.28835516399 3057.45468050152,3975.56435883265 3056.24377736302,3976.31421449066 3058.47750700913,3978.40149689169 3055.19220667491,3984.26050941277 3051.8063835431,3982.4575068023 3048.44560715297,3987.71683247241 3054.22036792816,3991.4523474899</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>540</ogr:id>
      <ogr:Hausnummer>79</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>0</ogr:Phase>
      <ogr:qm2>64.87</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.529">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3025.89268943125,3994.12819704213 3031.8892009994,3998.05044360243 3029.40008298998,4000.61498943032 3037.16914829211,3999.86071124565 3037.01829265518,3997.10759587159 3035.96230319663,3996.20246204998 3037.38347627452,3994.41737765659 3036.25319560443,3993.63669138169 3035.43430846736,3994.12819704213 3029.93740232749,3989.27436505261 3025.89268943125,3994.12819704213</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>541</ogr:id>
      <ogr:Hausnummer>0</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>0</ogr:Phase>
      <ogr:qm2>68.1</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.530">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2987.57535764985,3979.38205853177 2989.67920056101,3981.85995678747 2991.1004933162,3980.27323477739 2989.95133393157,3978.32606907323 2987.57535764985,3979.38205853177</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>542</ogr:id>
      <ogr:Hausnummer>0</ogr:Hausnummer>
      <ogr:Raumnummer>total</ogr:Raumnummer>
      <ogr:Phase>0</ogr:Phase>
      <ogr:qm2>6.35</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.531">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3009.84155714148,3916.09881565874 3008.78599200265,3913.8450414434 3005.84752688645,3915.04325051991 3006.7604480876,3917.38261109786 3009.84155714148,3916.09881565874</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>389</ogr:id>
      <ogr:Hausnummer>48</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>8.13</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.532">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3006.0472283992,3917.52525503554 3005.3340087108,3915.12883688252 3002.28142844445,3916.46968989671 3003.108763283,3918.63787774944 3006.0472283992,3917.52525503554</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>390</ogr:id>
      <ogr:Hausnummer>48</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>7.78</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.533">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3013.12236770812,3916.52674747178 3012.75149347015,3915.2714808202 3010.44066167974,3916.24145959642 3010.92565106785,3917.29702473525 3013.12236770812,3916.52674747178</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>391</ogr:id>
      <ogr:Hausnummer>48</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>2.98</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.534">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3014.2920479971,3918.72346411205 3013.56456391493,3917.12585201004 3011.09682379307,3917.98171563612 3011.838572269,3919.57932773813 3014.2920479971,3918.72346411205</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>392</ogr:id>
      <ogr:Hausnummer>48</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>4.56</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.535">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3011.06829500553,3919.47947698175 3010.055523048,3916.94041489105 3007.84454201397,3917.69642776076 3008.99995790917,3920.37813378914 3011.06829500553,3919.47947698175</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>393</ogr:id>
      <ogr:Hausnummer>48</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>6.48</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.536">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3008.30100261454,3920.46372015175 3007.18837990064,3917.89612927351 3003.36552237082,3919.49374137552 3004.36402993458,3922.28956255405 3008.30100261454,3920.46372015175</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>394</ogr:id>
      <ogr:Hausnummer>48</ogr:Hausnummer>
      <ogr:Raumnummer>7</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>12.21</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.537">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3013.40765558348,3914.38708840659 3012.63737832001,3912.33301570399 3009.41362532845,3913.64533993065 3010.26948895452,3915.61382627063 3013.40765558348,3914.38708840659</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>388</ogr:id>
      <ogr:Hausnummer>48</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>7.43</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.538">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2942.23603672189,3901.5043165785 2943.32549888192,3901.80284686703 2943.75927140726,3900.19140763493 2942.9434484635,3899.91264015987 2942.23603672189,3901.5043165785</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>189</ogr:id>
      <ogr:Hausnummer>14</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>1</ogr:Phase>
      <ogr:qm2>1.69</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.539">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2950.22728284295,3925.03816870694 2954.22121249906,3933.28095969934 2958.64002828879,3931.53892655146 2954.43365556587,3924.31586227978 2950.22728284295,3925.03816870694</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>544</ogr:id>
      <ogr:Hausnummer>13</ogr:Hausnummer>
      <ogr:Raumnummer>9</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>38.4</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.540">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2954.47614417924,3934.42815226013 2955.6658253534,3936.89249183518 2960.42455005003,3934.38566364677 2959.53228916941,3932.21874436526 2954.47614417924,3934.42815226013</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>545</ogr:id>
      <ogr:Hausnummer>13</ogr:Hausnummer>
      <ogr:Raumnummer>12</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>13.82</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.541">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2960.97690202375,3934.0032661265 2964.58843415959,3932.38869881872 2962.93137823844,3928.47974638933 2959.23486887587,3930.39173399066 2960.97690202375,3934.0032661265</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>546</ogr:id>
      <ogr:Hausnummer>13</ogr:Hausnummer>
      <ogr:Raumnummer>13</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>16.74</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.542">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2963.05884407853,3934.72557255367 2964.71589999968,3939.2718541835 2966.88281928119,3938.4645705296 2965.22576336004,3933.49340276615 2963.05884407853,3934.72557255367</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>547</ogr:id>
      <ogr:Hausnummer>13</ogr:Hausnummer>
      <ogr:Raumnummer>14</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>12</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.543">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2959.23486887587,3942.41601157235 2964.07857079924,3939.69674031712 2961.52925399747,3934.72557255367 2957.11043820773,3937.48733242226 2959.23486887587,3942.41601157235</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>548</ogr:id>
      <ogr:Hausnummer>13</ogr:Hausnummer>
      <ogr:Raumnummer>15</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>29.33</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.544">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2958.64002828879,3944.54044224049 2967.52014848163,3940.33406951757 2967.05277373464,3939.39932002358 2958.42758522198,3944.03057888013 2958.64002828879,3944.54044224049</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>549</ogr:id>
      <ogr:Hausnummer>13</ogr:Hausnummer>
      <ogr:Raumnummer>11</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>7.82</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.545">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2970.83430358429,3952.17134841719 2972.38918298626,3951.40859156876 2971.73135822636,3949.70458095464 2970.07058330641,3950.56753583364 2970.83430358429,3952.17134841719</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>174</ogr:id>
      <ogr:Hausnummer>13</ogr:Hausnummer>
      <ogr:Raumnummer>1</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>3.24</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.546">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2971.06988433909,3948.41753676092 2968.89238257194,3944.09569315202 2967.43334585487,3944.92469128672 2969.75454063203,3949.14705511946 2971.06988433909,3948.41753676092</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>175</ogr:id>
      <ogr:Hausnummer>13</ogr:Hausnummer>
      <ogr:Raumnummer>2</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>7.68</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.547">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2969.66611416433,3952.703476789 2967.8091583426,3948.21583355315 2965.84166943625,3949.06693830478 2967.8975848103,3953.41088853061 2969.66611416433,3952.703476789</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>550</ogr:id>
      <ogr:Hausnummer>13</ogr:Hausnummer>
      <ogr:Raumnummer>3</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>9.77</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.548">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2967.22678223506,3946.96610411134 2966.44199733421,3945.4075876181 2964.48556173632,3946.43554530513 2965.44719957257,3947.96090187298 2967.22678223506,3946.96610411134</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>177</ogr:id>
      <ogr:Hausnummer>13</ogr:Hausnummer>
      <ogr:Raumnummer>4</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>3.76</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.549">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2965.79400696142,3954.62918266753 2966.88828449923,3954.15389040363 2965.95980658836,3952.17534818882 2964.93184890133,3952.83854669658 2965.79400696142,3954.62918266753</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>178</ogr:id>
      <ogr:Hausnummer>13</ogr:Hausnummer>
      <ogr:Raumnummer>5</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>2.51</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.550">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2965.46413232803,3951.1653422922 2963.58506988937,3947.31879094719 2962.5128989685,3947.88250967879 2964.5246011087,3951.6406345561 2965.46413232803,3951.1653422922</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>179</ogr:id>
      <ogr:Hausnummer>13</ogr:Hausnummer>
      <ogr:Raumnummer>6</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>4.84</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.551">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2968.62263489291,3943.05009712399 2967.71626359897,3941.40315416305 2965.56086844875,3942.45321846701 2966.14669379728,3944.12226804487 2968.62263489291,3943.05009712399</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>180</ogr:id>
      <ogr:Hausnummer>13</ogr:Hausnummer>
      <ogr:Raumnummer>7</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>4.63</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.552">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2965.28453573719,3944.73435483683 2964.84240339868,3943.02109202512 2961.34955792448,3944.76751476222 2962.26698252688,3946.39235110623 2965.28453573719,3944.73435483683</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>181</ogr:id>
      <ogr:Hausnummer>13</ogr:Hausnummer>
      <ogr:Raumnummer>8</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>6.59</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Qdm fid="Qdm.553">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2955.81569176177,3923.6770179432 2958.64533872822,3928.98260600529 2962.00554450087,3927.30250311896 2959.92752250989,3922.83696650004 2955.81569176177,3923.6770179432</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>183</ogr:id>
      <ogr:Hausnummer>13</ogr:Hausnummer>
      <ogr:Raumnummer>10</ogr:Raumnummer>
      <ogr:Phase>2</ogr:Phase>
      <ogr:qm2>21.34</ogr:qm2>
    </ogr:Qdm>
  </gml:featureMember>
</ogr:FeatureCollection>
