<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ Vector_Poly_Emprise.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>2860.424984912632</gml:X><gml:Y>2495.882927846942</gml:Y></gml:coord>
      <gml:coord><gml:X>3465.413434988697</gml:X><gml:Y>4007.335877118606</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                                
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.837">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2932.074560761553585,3831.326848141859955 2916.377738773240708,3847.299658209087283 2927.072276831212093,3857.338724579634345 2944.18353772396631,3843.125338515491876 2932.074560761553585,3831.326848141859955</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>843</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-1</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.87">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3338.930349567639041,3340.624502783651224 3329.100501270300356,3341.880298381108332 3328.927288084444172,3342.226724752819791 3328.450951823339437,3345.344562098232473 3328.23443534101898,3351.926663160767475 3328.450951823339437,3353.225762054688857 3330.440747128619932,3359.716465771537514 3342.318992358228115,3359.183881809370178 3346.362339625381082,3358.449555148454237 3345.036114369069765,3350.10792470927754 3343.135195476070749,3343.769936379193041 3340.121076558425102,3340.698994314841002 3338.930349567639041,3340.624502783651224</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>92</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-1-BK-Vc</ogr:NAME>
      <ogr:PERIODE>EBA</ogr:PERIODE>
      <ogr:PHASE>BK-Vc</ogr:PHASE>
      <ogr:LAGE>BK</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.842">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2970.702742864246829,3886.912638307081579 2969.90675352103608,3886.949235518264686 2969.687596111286439,3887.034045667207465 2969.463012335452277,3887.132221574175219 2960.032088145900161,3888.64707448749823 2961.641961275880931,3896.791138556815895 2960.563270152596488,3899.120303834009519 2956.780621928271557,3900.57321185532146 2957.961495349977213,3904.138986501255658 2972.273866764457125,3901.841036033564706 2973.408126260341305,3901.651634068408384 2973.650067492096696,3901.549288481684471 2973.823690112967597,3901.461734412715941 2974.009687209042113,3901.312863070360436 2974.141528327623746,3901.138180555888084 2974.23757817349815,3900.969650815155546 2974.324927174047843,3900.781660337689573 2974.403635845042118,3900.572547380817014 2974.435658404825062,3900.316366902543905 2974.458531661814959,3899.05833776815598 2974.408210496440461,3898.70608961052767 2974.0010665220384,3896.528555545191011 2973.959804889736915,3896.308695230164631 2973.749649535712706,3895.544915650988059 2972.414552106603878,3891.72343996767313 2970.702742864246829,3886.912638307081579</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>848</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-10</ogr:NAME>
      <ogr:PERIODE>?</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-1b</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.843">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2975.07579398550206,3902.140480723680867 2965.354588964736649,3903.162274040313605 2957.966761438666254,3904.500139927207783 2963.153836223818416,3918.921071161973941 2963.470052229832618,3919.731567103928683 2965.498934261696377,3924.008306121221267 2975.642509005180727,3919.79556489275501 2981.364462601228297,3917.339060624596186 2977.409601144839598,3907.557404670259075 2975.07579398550206,3902.140480723680867</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>849</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-11</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.851">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2939.439665842779505,3914.531939842720931 2938.022611452707679,3922.551293672207976 2938.367310132858165,3923.654329448688713 2953.034638341043774,3921.162380115975793 2960.037719637407918,3919.222076857716729 2959.048293472600108,3914.609037985433133 2939.439665842779505,3914.531939842720931</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>857</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-12</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.855">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2961.405972309450135,3921.202622988550047 2948.18172777349946,3923.961686144148189 2958.000940631269259,3945.952660214731623 2965.784304039902054,3956.200118038233995 2973.906861665182987,3952.037559460161901 2965.800654555668189,3931.568237769714415 2961.405972309450135,3921.202622988550047</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>861</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-13</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.860">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2934.356265025888661,3886.048453543255164 2931.940619059409073,3889.137128831187056 2929.760033011111773,3893.29877699226563 2928.890679608240134,3895.880572938815931 2927.804906285332436,3899.277590258961936 2941.712172488068973,3902.770498435651007 2943.790238293351649,3902.928177071231858 2944.447482376960579,3900.361454139808757 2946.286417161886675,3893.070440770055484 2939.226523896857088,3889.431211321759292 2939.467533851741337,3887.933941708249222 2934.356265025888661,3886.048453543255164</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>866</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-14</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.862">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2916.974234981987138,3884.578098547807258 2913.062371878409067,3896.980043197187115 2913.572859280594457,3897.102167212348377 2923.651887261383763,3898.943439303011928 2924.36792502413391,3899.062778930137029 2927.804906285332436,3899.277590258961936 2931.582467164218542,3889.83994984711353 2926.014811878458204,3885.481929363318613 2919.451132386586323,3884.813627451417688 2917.550695602586075,3884.623228468929483 2916.974234981987138,3884.578098547807258</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>868</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-15</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.864">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2912.720601699069448,3897.074124381710135 2910.321666179092063,3903.455338357856363 2907.614964744586359,3909.603298638655815 2912.758177458450518,3915.090092632005508 2915.00522179005884,3912.251485875961862 2920.101655399208994,3914.483433712456645 2929.780477663040074,3916.68876804413685 2933.086770630081446,3901.339317108780961 2926.587729773355477,3899.686676763636569 2917.479566070290275,3897.640174898616351 2915.068845320803121,3897.151391395353585 2912.720601699069448,3897.074124381710135</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>870</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-16</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.875">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2915.030119816742626,3912.276383902646103 2909.908462860218151,3919.436820048360914 2912.384805469923776,3920.864703418239969 2915.247535238018372,3922.307896370076833 2919.311802617245576,3924.02481015384592 2924.59603498809247,3925.83570519717432 2927.77354475715947,3920.175104063103845 2928.17649303890812,3919.424657242127978 2929.624549469765043,3916.651043481248962 2915.030119816742626,3912.276383902646103</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>881</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-17</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.884">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2895.199925635876752,3898.968542977876496 2890.941917429610839,3902.499574173317342 2884.721430947085537,3907.793605222272618 2888.002877583772715,3910.573096921516026 2891.957943167406029,3913.863971778242558 2898.177069584972742,3918.043035023833454 2906.5407974317136,3921.688341921244501 2906.984795720007241,3921.649562622571921 2912.758177458450518,3915.090092632005508 2908.573810878041058,3910.424648797627015 2895.199925635876752,3898.968542977876496</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>890</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-18</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.888">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2908.164209861455674,3922.664547836146539 2902.737972171719775,3930.236382575093558 2902.298006953633376,3930.852333880415244 2904.791143189458126,3933.165051041149127 2915.441998215038893,3942.822448191996955 2919.038654698500977,3938.948782073077837 2919.427319444474506,3933.634347273774893 2915.027667263608237,3926.418917697152665 2908.164209861455674,3922.664547836146539</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>894</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-19</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.890">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2943.39910133955982,3849.827527812602057 2926.900388774409294,3860.143255556511122 2933.839089982768201,3869.572375245551939 2936.529553395821495,3871.558967825753825 2940.306637474644504,3869.448631260869206 2941.517554380970068,3868.737989828485752 2945.972871274430418,3865.986527966852918 2942.11665728905291,3859.859040824801468 2948.590852576060115,3856.738531839321695 2943.39910133955982,3849.827527812602057</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>896</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-2</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.893">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2901.743461181616567,3921.626450712217775 2898.411980883942761,3929.548470277129127 2901.56369840319303,3931.220338105856626 2902.961281609475918,3929.545299356639134 2907.135005705379626,3923.702085622373488 2901.743461181616567,3921.626450712217775</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>899</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-20</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.894">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2883.006188126842972,3908.943816572068044 2878.358250634990782,3914.605204348528787 2875.828264377437335,3917.821848958725695 2881.327132146594977,3920.05731387679316 2884.348251272484049,3921.161219001351583 2885.227534023075805,3921.392609198876016 2889.779581629630684,3922.579038490925086 2891.502633649858126,3922.813656405375696 2892.037837757321086,3921.235568714937017 2899.118315450642058,3923.200107236185431 2899.805116467107837,3921.253775080360811 2890.873454842669616,3914.682293470671993 2883.006188126842972,3908.943816572068044</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>900</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-21</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.896">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2872.252174634812491,3916.386216660956507 2871.396406573590411,3918.055954520824343 2870.549279119063158,3919.835180159295305 2888.324131055456746,3931.758345637279945 2890.460547700352436,3932.950241239168008 2894.815098953512916,3935.311266263088328 2898.216838169025323,3925.878355436776928 2898.461807753556513,3925.199062845650133 2899.112407779225123,3923.274300737838985 2898.358354108181629,3923.012340581545686 2898.289841832540333,3922.98853924335981 2898.232121873451433,3922.96848718329602 2892.037837757321086,3921.235568714937017 2891.502633649858126,3922.813656405375696 2883.894997954970222,3920.995602010072162 2872.252174634812491,3916.386216660956507</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>902</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-22</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.908">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2872.122620952198304,3897.20250644169937 2865.738029678359453,3904.076472451924019 2866.642612305032344,3905.063289862839156 2869.507123956161649,3907.914095716594602 2874.056217242270122,3911.743394486190937 2872.252174634812491,3916.386216660956507 2875.828264377437335,3917.821848958725695 2883.006188126842972,3908.943816572068044 2876.086938988431939,3901.333577231951949 2872.122620952198304,3897.20250644169937</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>914</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-25</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.909">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2869.597394162155069,3893.98796110175681 2865.093705477096137,3895.961603621209633 2860.424984912631771,3898.595240862704031 2865.770481173088228,3904.046970551823961 2868.413309619033043,3901.316291086120145 2872.336207891200502,3896.95919348541338 2871.982532147067559,3896.569554320691623 2869.597394162155069,3893.98796110175681</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>915</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-27</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.914">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2881.111383042953094,3888.225656784957664 2875.4794358895806,3892.364289343800465 2873.178516743442742,3894.182728515507733 2877.978703395109733,3899.664177325567834 2879.688450550018388,3901.504761479527133 2885.192987936398367,3907.392280124988247 2892.187208336672938,3901.50008721035374 2881.111383042953094,3888.225656784957664</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>920</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-28</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.923">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2903.391657895240314,3891.994433161668894 2901.531051664299412,3893.323437612339603 2894.746034852887078,3898.668164506016637 2898.419395255249583,3902.914989608073938 2904.934580105041732,3907.740003197686292 2910.843617425712182,3896.548938130662464 2903.391657895240314,3891.994433161668894</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>929</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-29</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.925">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2952.62150239663606,3839.932720857272216 2943.002066132135951,3849.299014062181413 2948.590852576060115,3856.738531839321695 2950.652611933141998,3855.852606604953507 2956.643664343488581,3850.761618406489561 2960.32830221088534,3847.555139651656646 2952.62150239663606,3839.932720857272216</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>931</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-3</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.926">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2908.693818440851828,3882.212504386856835 2902.099046464995808,3890.388587277645001 2902.696308061212676,3891.187112464184793 2904.320047747700755,3892.630552134649861 2910.843617425712182,3896.399549970557018 2911.629475240149986,3894.986081344696686 2914.430119830650256,3886.78235143152915 2908.693818440851828,3882.212504386856835</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>932</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-30</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.929">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2905.081369556909522,3878.251907951113935 2904.475112708183588,3878.768348970399529 2894.50433184118765,3888.72811751387826 2897.060815987832484,3892.77063678024706 2897.761379457471776,3892.465262960145992 2902.094993227998657,3890.130051394681686 2904.244444012950225,3888.205170094724963 2906.011008829370894,3886.168645295535953 2907.753390482778286,3882.293620275958347 2906.15915951020088,3879.666507264808843 2905.081369556909522,3878.251907951113935</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>935</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-31</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.930">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2981.116591233399959,3917.242610569653152 2970.475205087718223,3921.941604607904992 2965.498934261696377,3924.008306121221267 2970.82559426448779,3934.100140965516857 2985.013652124148848,3928.714610286463085 2984.381046747849723,3926.546207762502945 2981.116591233399959,3917.242610569653152</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>936</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-32</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.931">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2979.255340589260413,3931.39780765905607 2970.598876622616899,3934.296177290744708 2974.639866259140945,3943.716487206871079 2983.771328742825062,3938.037305448614916 2987.647506564775995,3935.908118443857802 2985.013652124148848,3928.714610286463085 2979.255340589260413,3931.39780765905607</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>937</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-33</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.933">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2986.678796732325736,3936.601771988519886 2981.374768679682347,3939.230940181304959 2974.639866259140945,3943.716487206871079 2975.932374610350053,3947.081085498408811 2977.858282604275701,3951.02553608844164 2988.034862161842739,3944.609990809148258 2990.360763810287608,3943.411114303208706 2987.647506564775995,3935.908118443857802 2986.678796732325736,3936.601771988519886</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>939</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-34</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.939">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2987.900727737237048,3945.215945216231376 2981.271795879166802,3949.360360828457033 2977.858282604275701,3951.02553608844164 2982.076876072897448,3960.961807805505487 2992.893189504206021,3956.144186671427633 2987.900727737237048,3945.215945216231376</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>945</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-35</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.943">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3002.814051568573632,3936.513491535743015 2993.735468789712741,3940.734453461488556 2993.062331682379408,3941.064533672470134 2998.60353266323682,3953.060785817183842 3008.227545434649528,3948.442985967222739 3006.975992204286285,3945.42199541117543 3005.710490891508925,3942.466588191657138 3004.17078668795557,3939.121072251414716 3002.814051568573632,3936.513491535743015</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>949</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-36</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.947">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3016.521398768512427,3927.848518610981046 3007.010012817440838,3931.319882400543065 3003.868293968526359,3932.580607499599864 3010.045532893984728,3947.789695673923688 3010.209865488634478,3948.192280890821166 3011.385690275672914,3947.796776189727098 3019.968289524070769,3940.476656519446351 3019.253307212431992,3937.733008295839682 3017.175115723744966,3930.062834085535087 3016.521398768512427,3927.848518610981046</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>953</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-37</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.952">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3021.505258365351892,3929.182287283520509 3017.480592159673051,3930.062834085535087 3019.6790054773669,3938.230318916902888 3022.368820702628454,3937.261392705599974 3021.505258365351892,3929.182287283520509</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>958</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-38</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.953">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3032.810179013137713,3922.912320784485473 3025.172449896780108,3924.94648984562582 3027.76313690860934,3933.409400750937493 3028.626699245885447,3932.929643896895868 3033.078842851400623,3923.641551202631035 3032.810179013137713,3922.912320784485473</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>959</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-39</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.954">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2960.693953296963628,3847.245742578822956 2950.652611933141998,3855.852606604953507 2942.11665728905291,3859.859040824801468 2946.461687946561597,3867.103409253494192 2953.437185588656121,3864.937629743650632 2968.498447053143991,3858.091399972492127 2968.437682799261893,3857.524108216272907 2964.260822315991845,3851.715881348963649 2960.693953296963628,3847.245742578822956</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>960</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-4</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.960">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2986.883895379783553,3902.713099405676985 2985.118951034725796,3903.451954341468536 2977.113589453748318,3908.006496911828435 2979.450845179040698,3913.487745406300746 2988.755357240217108,3908.388885881636725 2989.487631365600919,3905.275644218470916 2986.883895379783553,3902.713099405676985</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>966</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-40</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.961">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2982.419064178823191,3894.285028326868087 2974.464669824262273,3898.188707900089412 2974.29743999829634,3898.780307194467696 2974.392563955424976,3900.636094037616203 2977.599921696844376,3907.382737202086901 2986.256978120593885,3902.857018856710965 2983.919162521545786,3896.720252909209194 2983.139890655196268,3894.733109650017468 2982.419064178823191,3894.285028326868087</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>967</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-41</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.967">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2999.918788053884782,3882.14299745494327 2974.892525431198919,3897.431091320399901 2974.892842199759798,3897.442059431836242 2982.419064178823191,3894.285028326868087 2991.624947193458411,3898.126804321823329 2992.400309421686416,3902.636848370400912 2993.641527531936845,3906.435303723251309 3006.020461177539346,3905.557828765708109 3006.680637391859818,3909.28837763211277 3012.459177959860881,3909.822167657964656 3014.677261708676269,3909.599308854967148 3014.858174248492105,3907.585783336389341 2999.918788053884782,3882.14299745494327</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>973</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-42</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2b</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.972">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3000.622197862870053,3908.252572957752363 2994.489030170779643,3909.338208148167269 2996.173035377828,3913.844769084336349 2996.446581070384582,3914.296228841612901 3002.266830000954997,3912.221449509279864 3000.622197862870053,3908.252572957752363</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>978</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-44</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.973">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2989.089872703559649,3908.535380367031394 2988.402057682212671,3908.771020698419306 2987.883077328376203,3908.970220460108976 2982.524796930752473,3911.909977409778548 2988.666758760067296,3926.015017993279344 2990.509878740711429,3928.976149394012737 2992.48878120454674,3927.895644367747082 2995.173344869524499,3926.328763175864879 2994.929939753674716,3921.095940050543959 2989.348487794972698,3909.085683322265595 2989.089872703559649,3908.535380367031394</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>979</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-45</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2b</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.987">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3011.498349421862713,3910.850912323136072 3005.271541939616782,3912.845725529119591 3002.245967652909712,3913.931536092369697 2999.733699884572161,3914.963717777295187 2996.037889819808697,3916.551350777052448 2997.153221632748227,3922.370157795900923 2999.264965995463626,3928.040788637193145 3002.228944970900557,3927.590490013893032 3008.728360214266559,3925.427677151041735 3016.177668446000553,3922.651554391248737 3013.331937184381331,3915.424095919064257 3013.300981999800115,3915.346890046932003 3011.498349421862713,3910.850912323136072</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>993</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-46</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.1022">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2952.019334096224611,3864.501212268912241 2945.838440690476091,3866.055106743676788 2938.022087789415309,3870.694672013926265 2947.13812443544839,3886.92198642103358 2954.75061829340757,3898.155809277833214 2956.371378355613615,3900.171732106090531 2960.563270152596488,3899.120303834009519 2961.641961275880931,3896.791138556815895 2959.788087394022114,3887.41271774270399 2956.46201954715616,3875.798743129873856 2952.019334096224611,3864.501212268912241</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1028</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-5</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2b</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.1066">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2988.351356098094129,3989.33669935313992 2983.848288380012036,3995.380638300884584 2980.261610119788202,4000.535139519984568 2991.087189050640518,4007.335877118606277 2999.708610949081049,3997.103240577244833 2997.35339506521359,3994.870475515475846 2988.351356098094129,3989.33669935313992</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1072</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-54</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>NOV-LBA-2a</ogr:PHASE>
      <ogr:LAGE>NOV</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.1068">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2997.665698985952986,3978.394364471091649 2990.92364232087084,3986.655057435736126 2994.247479950699926,3989.481964100342793 2997.503465647471785,3992.25116304780795 2998.532920771148838,3990.849340108140041 3003.549134489366679,3984.018694168598813 2997.665698985952986,3978.394364471091649</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1074</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-55</ogr:NAME>
      <ogr:PERIODE>?</ogr:PERIODE>
      <ogr:PHASE>NOV-LBA-1a</ogr:PHASE>
      <ogr:LAGE>NOV</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.1070">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3013.776758977676764,3975.201192467408873 3008.142854859284398,3980.025196818022778 3009.262702910275038,3981.353771921235875 3011.195718698527799,3983.472002485207213 3011.548561011010406,3983.858652907363648 3013.913335573016866,3986.316873171811494 3014.50345855774458,3985.972880059830459 3019.209915831305352,3981.524337310261672 3018.361664141295932,3980.536256220556879 3013.776758977676764,3975.201192467408873</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1076</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-56</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>NOV-LBA-2a</ogr:PHASE>
      <ogr:LAGE>NOV</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.1076">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3019.502219297646207,3987.89235944930806 3017.708695876860929,3989.957673605721538 3016.077919203870806,3991.885342897453029 3015.431035508771856,3992.745915613862962 3013.447455901336525,3995.416773220335926 3012.085736098032612,3997.282884210232169 3012.95992616089643,3997.952037845849645 3019.416212999360596,4000.088679141332705 3030.162883824282289,4001.404380897654846 3033.727669158477511,4001.113870085273447 3040.040199525342814,4000.442304693006463 3040.536004937409416,3999.786998109276738 3029.460123388381362,3992.431330390513722 3021.318730114645405,3986.301081945270198 3019.502219297646207,3987.89235944930806</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1082</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-57</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>NOV-LBA-2a</ogr:PHASE>
      <ogr:LAGE>NOV</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.1085">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3049.197277187525742,3967.561743746380216 3042.294602807723095,3962.157653658045547 3031.560628877596173,3974.725630216965328 3027.584052603107921,3978.707174978687362 3022.613674528282445,3983.765260591939295 3031.597252758105242,3990.322422732999712 3041.498220910512828,3997.291753140163109 3043.639105788583947,3994.380076380963146 3058.05108298794039,3973.72333242158038 3049.197277187525742,3967.561743746380216</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1091</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-58</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>NOV-LBA-2b</ogr:PHASE>
      <ogr:LAGE>NOV</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.1098">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3055.127228789466244,3974.080208361481255 3048.309359928675804,3986.114546699599941 3051.720933040849559,3988.147191409776042 3053.125577717785291,3988.972141458135866 3054.720279981362182,3989.85824342244814 3061.709517410170974,3978.24779082946543 3061.419670095882339,3978.047127304192145 3055.127228789466244,3974.080208361481255</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1104</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-59</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>NOV-LBA-2a</ogr:PHASE>
      <ogr:LAGE>NOV</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.1102">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2968.863256370361796,3859.553091741618118 2962.34859340228104,3862.158525718786677 2957.08286386217469,3864.056499686032566 2953.288034183906348,3865.607256977908946 2952.532272403813749,3865.805589187729311 2956.46201954715616,3875.798743129873856 2960.064410378698085,3874.880128931327818 2969.113947293541969,3872.519067391682711 2969.497181798211841,3870.669530938963817 2970.367445762982697,3866.608433345959384 2968.863256370361796,3859.553091741618118</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1108</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-6</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.1111">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3061.724549891840525,3978.254378843768791 3056.515187028082892,3986.273706512678928 3056.144391849170461,3986.938241132483654 3065.965484983638817,3992.454382809696654 3070.752785021443287,3984.862872590032566 3069.170089829709468,3983.738695885641846 3061.724549891840525,3978.254378843768791</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1117</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-60</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>NOV-LBA-2a</ogr:PHASE>
      <ogr:LAGE>NOV</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.1113">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3056.144391849170461,3986.938241132483654 3052.341400516588692,3995.241722724536885 3058.582782463055537,3997.842121380469962 3060.13396656986879,3998.480468836499767 3060.576102575559162,3997.591137104485369 3062.153815888790632,3994.251955924306003 3063.500605206192631,3991.280616480947629 3063.694077375466804,3990.846576922393524 3056.144391849170461,3986.938241132483654</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1119</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-61</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>NOV-LBA-2a</ogr:PHASE>
      <ogr:LAGE>NOV</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.1116">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3051.720933040849559,3988.147191409776042 3048.309359928675804,3986.114546699599941 3041.498220910512828,3997.291753140163109 3044.689391834413073,3999.429561951877076 3046.236301634146457,3999.443604306969064 3050.478668541218667,3999.030665423541905 3054.745881804549299,3989.805749384566752 3052.189147933161621,3988.370150882304642 3051.720933040849559,3988.147191409776042</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1122</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-62</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>NOV-LBA-2a</ogr:PHASE>
      <ogr:LAGE>NOV</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.1121">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3067.385900176891482,3955.888258614876577 3055.65072683462904,3969.315057393879215 3065.327058365352968,3976.811868697519003 3079.257807428161414,3986.472746975820883 3079.825009822620359,3986.821080261112456 3080.749028100095984,3985.243876649557933 3092.160498090629517,3973.751038842823618 3073.818891019027888,3960.056412913540953 3073.329118214751816,3959.732691144035471 3067.385900176891482,3955.888258614876577</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1127</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-63</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>NOV-LBA-2a</ogr:PHASE>
      <ogr:LAGE>NOV</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.1126">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2948.161550837267896,3924.002040016609499 2923.33386513161804,3929.205230939456669 2922.312545803915327,3929.426882378080336 2929.470126039890147,3946.075872016528592 2929.630169904434752,3946.419513560104861 2929.817087911234012,3946.497213771677707 2932.261445027904301,3946.089138936160907 2943.864628217016616,3943.619739522897817 2956.029064303846553,3940.840152812129418 2949.94948738081348,3927.587332777060055 2948.161550837267896,3924.002040016609499</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1132</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-64</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.1136">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2941.108503097068024,3907.62632295110825 2939.836719433765666,3913.843376063852247 2939.670851319922804,3914.686063494149039 2948.951940064468545,3914.983060105820641 2949.517438614757339,3907.750084129520019 2941.108503097068024,3907.62632295110825</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1142</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-65</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.1140">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2958.759446860695334,3911.375207040671285 2952.792754923830216,3912.047697506127861 2949.69072091849921,3912.447759474915983 2949.894126246658743,3915.780507578196193 2952.685311923610243,3915.686369979360734 2959.311915362592572,3914.807305232460749 2958.759446860695334,3911.375207040671285</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1146</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-66</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.1145">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2926.569696666444088,3861.392958762424769 2906.611259464304567,3874.698583563852935 2914.92717723205169,3884.066442255129004 2915.713694482753453,3884.772974700671966 2917.654552088378296,3884.282292442234393 2926.482186625449231,3885.26948629650451 2931.204085081322319,3889.465416603167341 2942.615250616928734,3880.680418836851914 2936.323112611313718,3870.668987391477913 2934.422006261419483,3871.694295277088713 2926.569696666444088,3861.392958762424769</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1151</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-7</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2c</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.834">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3352.082096738619384,3332.861550454506869 3337.855560996064924,3335.851694910128117 3337.755810302875943,3336.249493629473363 3337.122020726599658,3338.779609058482492 3338.854260751216771,3340.417228275796333 3347.217121970483731,3339.391861710217199 3350.588029484500566,3338.721398645984664 3353.459743167740726,3338.022873696007537 3352.082096738619384,3332.861550454506869</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>840</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-81</ogr:NAME>
      <ogr:PERIODE>MBA</ogr:PERIODE>
      <ogr:PHASE>BK-IVd</ogr:PHASE>
      <ogr:LAGE>BK</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.1209">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2899.693513428115239,3727.002989296163832 2899.530000335783825,3727.088658928921632 2893.78320987009738,3732.753998821081041 2896.803967522736457,3735.015307869063236 2898.02199084510994,3735.871707596550095 2898.720260090227839,3734.818834720896575 2901.817984478877861,3730.136768404151098 2902.280657395844173,3729.425358206247893 2902.563582152984509,3728.990330457511845 2899.693513428115239,3727.002989296163832</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1215</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-82</ogr:NAME>
      <ogr:PERIODE>MBA</ogr:PERIODE>
      <ogr:PHASE>KNW-MBA-1</ogr:PHASE>
      <ogr:LAGE>KNW</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.1208">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2904.04598213480449,3725.586348350492244 2900.869409767158231,3726.251528034311832 2900.5617076004919,3726.378228926469092 2900.371656262256693,3727.274185235292862 2900.507407218139178,3727.627137720588962 2900.697458556374386,3727.907689696076886 2900.878459830884367,3728.061540779412098 2901.24951244363001,3728.33304269117707 2901.756316012256775,3728.604544602941587 2902.563582152984509,3728.990330457511845 2904.471335129904219,3726.056951664217195 2904.04598213480449,3725.586348350492244</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1214</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-82bis</ogr:NAME>
      <ogr:PERIODE>MBA</ogr:PERIODE>
      <ogr:PHASE>KNW-MBA-1</ogr:PHASE>
      <ogr:LAGE>KNW</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.1288">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3114.500838762472995,3727.321843201724732 3111.361105993751153,3728.396268182049425 3105.88533562027078,3730.873402398623966 3104.788145555316532,3731.874830889564691 3105.040124974253104,3732.524731303532008 3111.016339701383004,3741.372952832502961 3111.453769195420136,3741.854279845488236 3115.62751459576566,3745.869954308031993 3123.426801931309456,3743.1592058852425 3127.72498615503946,3739.274696668963315 3122.959315540778789,3733.91037441790786 3121.05910107567297,3732.06060812444457 3119.6600051155242,3730.849852005084813 3116.142085946496081,3728.098745044984753 3114.500838762472995,3727.321843201724732</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1294</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-83</ogr:NAME>
      <ogr:PERIODE>MBA</ogr:PERIODE>
      <ogr:PHASE>NWH-8a</ogr:PHASE>
      <ogr:LAGE>NWH</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.1236">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2997.046073741168584,3926.204622372435551 2987.579630393214757,3930.186898253817617 2985.877104927190885,3931.072847965205256 2988.253643212412044,3936.41461415664071 2990.023587663562466,3942.478717923567274 2995.497068502812908,3939.915421956404316 3001.652518259196768,3937.053530437605787 2999.613274323186033,3932.275547473227562 2997.046073741168584,3926.204622372435551</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1242</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-87</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2a</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.1239">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2953.755702065938294,3898.661882669767238 2953.051398742896708,3898.818155615912929 2948.476484303751022,3899.898757110512179 2947.228203033415866,3900.214451189953706 2947.094139757970424,3901.489520839535999 2946.597198403030688,3906.560163183472014 2949.549020803207895,3906.672306788491369 2949.477845350591906,3912.208967371132985 2952.369503413262919,3912.017162018051295 2956.842416318642336,3911.53390281263637 2955.849140262803758,3904.312412008383944 2955.300977525156668,3900.976308671784864 2953.755702065938294,3898.661882669767238</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1245</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-9</ogr:NAME>
      <ogr:PERIODE>LBA-late</ogr:PERIODE>
      <ogr:PHASE>WTER-LBA-2b</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.730">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2910.432474027663829,3736.330398557624903 2909.794031187804649,3736.85061124195272 2903.163759548994676,3746.679791508812741 2901.878807488451457,3751.18511980404719 2901.689050212026814,3752.114930458528761 2922.917578451562804,3757.399012272952859 2925.114151621519795,3751.396739881201484 2926.926516939981411,3746.266413956487213 2923.366853042574348,3740.421161938940713 2910.432474027663829,3736.330398557624903</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>736</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-90</ogr:NAME>
      <ogr:PERIODE>LBA</ogr:PERIODE>
      <ogr:PHASE>KNW-LBA-2b</ogr:PHASE>
      <ogr:LAGE>KNW</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.703">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2897.449025741031619,3719.999686350166485 2896.719282398611995,3719.999862905087866 2895.06153109044817,3721.092991980687202 2894.481036122228943,3721.550723918512176 2894.788643321500331,3722.029895592015237 2897.242323275179388,3725.690016758072943 2897.27490112961641,3725.737086556006943 2897.717785197596641,3726.216324527399138 2899.994872270784526,3723.768455923721831 2900.002146356656795,3721.710665966176748 2900.000175012170075,3721.074660161385509 2899.416573569676075,3720.031302378444252 2899.398188539415059,3720.000405187000979 2898.836308369822746,3720.000189369980035 2897.449025741031619,3719.999686350166485</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>709</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-91</ogr:NAME>
      <ogr:PERIODE>LBA</ogr:PERIODE>
      <ogr:PHASE>KNW-LBA-2b</ogr:PHASE>
      <ogr:LAGE>KNW</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.1257">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2969.833998149948457,3910.103991168857647 2964.342230978792486,3911.007081770336299 2964.537493811545119,3911.568462414499209 2968.808868277999409,3913.399051471551957 2969.199393943503765,3913.350235763363344 2970.078076690888793,3910.518924688455627 2969.833998149948457,3910.103991168857647</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1263</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-92</ogr:NAME>
      <ogr:PERIODE>MBA</ogr:PERIODE>
      <ogr:PHASE>WTER-MBA-1</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.4">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3431.2946325584644,3335.234389190044112 3403.166932010813071,3347.888624389964662 3414.395645619952575,3365.272116326072137 3415.117260963512308,3366.37276123993297 3426.065446166723177,3360.173458350409874 3440.897925897551431,3350.508092715376279 3440.137463336962355,3349.234251732709708 3431.2946325584644,3335.234389190044112</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>5</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-A</ogr:NAME>
      <ogr:PERIODE>LBA</ogr:PERIODE>
      <ogr:PHASE>BK-III</ogr:PHASE>
      <ogr:LAGE>BK</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.67">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3282.734342724359522,3344.818203344373615 3282.249482463701952,3346.47611262275268 3280.529010571044637,3354.906424896772933 3280.325682074640099,3356.752022017987201 3280.341322728209434,3356.970991167961074 3283.26612494572646,3357.987633649986492 3283.938673049220142,3357.768664500012164 3285.580941674028963,3356.611256135859549 3286.722709384610425,3351.371637190040929 3287.19192899169866,3347.743005561892005 3286.409896313218724,3346.053814976373815 3285.83119213114287,3345.834845826399032 3282.734342724359522,3344.818203344373615</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>72</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-X</ogr:NAME>
      <ogr:PHASE>BK-IVa</ogr:PHASE>
      <ogr:LAGE>NWH</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.163">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3294.523461892224532,3327.847961529970689 3290.92595232738131,3328.994667703763753 3294.118742066179948,3339.584836485270898 3297.828673804924165,3338.573036920158756 3297.671282761461953,3337.988441615872034 3294.523461892224532,3327.847961529970689</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>169</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-Y</ogr:NAME>
      <ogr:PHASE>BK-IVb2</ogr:PHASE>
      <ogr:LAGE>NWH</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.68">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3295.734336027084737,3332.865167698216737 3291.728493710484599,3334.091712251181434 3292.602732913132058,3338.763020229498579 3293.816229119789114,3342.325218771622076 3298.435343712872509,3341.959865074994013 3299.153002759820083,3341.738043187755466 3300.066387001390012,3338.906552038887639 3295.734336027084737,3332.865167698216737</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>73</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gebaude-Z</ogr:NAME>
      <ogr:PHASE>BK-IVa</ogr:PHASE>
      <ogr:LAGE>NWH</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.16">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3238.329271233904819,3732.374230788431305 3237.637317594962497,3732.745350441874052 3235.176207223589245,3735.789241654988928 3233.472996125664849,3738.762128080787988 3234.613506569440233,3739.349778552791122 3243.558506623320227,3740.97967010273851 3243.594816636448741,3740.970255834969521 3244.276435044129357,3740.688618040248457 3241.742360466832451,3736.746655768245546 3240.956919050915985,3735.575903469048171 3238.329271233904819,3732.374230788431305</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>21</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Gefassraum</ogr:NAME>
      <ogr:PHASE>NWH-8a</ogr:PHASE>
      <ogr:LAGE>NWH</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.51">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3119.848516871328684,3678.177415145651594 3140.189710438039583,3704.482918531050927 3118.493141654725605,3723.638051530756002 3138.32899124509504,3746.676101054995797 3147.135041063240806,3743.740751115612511 3152.205190958536605,3739.382201205623005 3162.612340743618461,3723.638051530756002 3162.523390745456254,3722.926451545450618 3162.434440747292683,3722.748551549125295 3126.574096478658703,3679.59831224579284 3125.697876600238033,3679.006271787401602 3119.848516871328684,3678.177415145651594</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>56</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>HausAmHang</ogr:NAME>
      <ogr:PHASE>NWH-5</ogr:PHASE>
      <ogr:LAGE>NWH</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.53">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3115.93328701879318,3701.946782592448017 3110.643568745186258,3706.05746727495125 3101.498091970938731,3713.227266139779204 3101.402494652741098,3714.724957458208792 3102.167273198322619,3717.114890413152807 3106.277957880824488,3723.488044959666695 3115.742092382397004,3726.7383537783885 3119.342924701176798,3724.794541641701926 3130.719005566704709,3712.87674263972076 3132.662817703391283,3706.407990775008784 3132.694683476124283,3704.782836365649018 3120.904347565072385,3702.7752926834969 3115.93328701879318,3701.946782592448017</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>58</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>HausAmHang-Altbau</ogr:NAME>
      <ogr:PHASE>NWH-6</ogr:PHASE>
      <ogr:LAGE>NWH</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.679">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2969.3880922851572,3741.906397460937114 2957.564937988282509,3745.896634521484884 2952.900292480468579,3748.594299316405795 2949.694999755859953,3750.541963378905621 2941.130530273438126,3758.421048339844219 2936.674151123047068,3762.580267333984466 2934.736718994140119,3764.414666992188359 2929.927927246093532,3769.181755859375244 2924.618483886719787,3774.602665283200622 2919.017988525391502,3780.735283203123799 2916.585733886719481,3783.703598144531497 2914.719027893029761,3786.209548342982998 2899.175244530154941,3808.404584822612378 2922.482533815275929,3818.611029949402564 2919.994965452754968,3822.179096181529985 2933.229719713809573,3829.918295658912939 2934.233845627637038,3828.550891015034722 2944.437722228136863,3838.562414335857738 2973.808620447098292,3799.454262350579029 2989.646723319472585,3777.899275566203414 3015.359394775391138,3750.792025634764741 3016.272579345703889,3749.284582763671096 3016.696460449219558,3747.022457275391389 3015.762594970703958,3745.022114257812518 3014.452752441407483,3743.740766113280642 3013.286622070313115,3743.170878417968652 3011.569978291562165,3742.734447945600095 2980.916740840319108,3743.314221418345824 2969.3880922851572,3741.906397460937114</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>685</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Komplex-1</ogr:NAME>
      <ogr:PERIODE>LBA</ogr:PERIODE>
      <ogr:PHASE>MTER-LBA-2b</ogr:PHASE>
      <ogr:LAGE>MTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.218">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3014.29599018004501,3718.981331728064106 2984.83825,3719.945877441407902 2984.699756831269951,3740.047080533267945 3016.203081437506171,3739.194261022671981 3014.29599018004501,3718.981331728064106</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>224</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Komplex-2</ogr:NAME>
      <ogr:PERIODE>LBA</ogr:PERIODE>
      <ogr:PHASE>MTER-LBA-1</ogr:PHASE>
      <ogr:LAGE>MTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.464">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2937.436771240234975,3751.071016357422195 2917.977338867187882,3760.851349609375575 2913.528215087892022,3764.07075 2913.189723399243576,3765.474122157587772 2912.670251953125444,3768.879293701171719 2911.509051513671693,3784.26375 2911.954915771484139,3784.26375 2935.555598710448066,3759.479493147453013 2937.5389355468742,3752.067699707030897 2937.436771240234975,3751.071016357422195</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>470</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Komplex-3</ogr:NAME>
      <ogr:PERIODE>LBA</ogr:PERIODE>
      <ogr:PHASE>MTER-LBA-1a</ogr:PHASE>
      <ogr:LAGE>KNW</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.1283">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3283.41321063891337,3732.191220370444171 3281.69602854500863,3732.194748683228227 3279.289998161752919,3732.3499764498888 3276.224249770183633,3732.970887516537005 3269.433034978736487,3734.561972124819476 3266.056831053844689,3735.687373433116136 3265.009043628877407,3736.50231920808892 3259.100686760320059,3746.412641935897227 3259.993246418624494,3747.111166885873445 3268.967351678751129,3749.890714082659997 3279.619857165908343,3747.232438578577785 3288.341717305213479,3744.142435848469631 3286.284924322154438,3738.681681363983898 3283.41321063891337,3732.191220370444171</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1289</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Pithosgebaude</ogr:NAME>
      <ogr:PERIODE>MBA</ogr:PERIODE>
      <ogr:PHASE>NWH-8a</ogr:PHASE>
      <ogr:LAGE>NWH</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.52">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3109.47798914003215,3730.554562955976508 3104.215512659553497,3733.185801196216289 3099.73459209201792,3737.666721763750957 3099.526177181900493,3739.490352227282528 3101.688481874373338,3743.788909748463993 3114.089169026390664,3742.955250107992924 3115.313606623333271,3742.330005377638827 3117.319600133216682,3740.271908140224696 3117.501963179571248,3737.640669899985824 3113.958909707565454,3730.710874138565032 3109.47798914003215,3730.554562955976508</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>57</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Pithoshaus</ogr:NAME>
      <ogr:PHASE>NWH-6</ogr:PHASE>
      <ogr:LAGE>NWH</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.6">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2975.018707684592755,3735.107189121938518 2971.117191217770596,3735.121844415798478 2971.109175790000791,3738.447467196390789 2972.004041017411964,3738.444105807817778 2974.023771397535711,3738.436519080327344 2974.417550199440484,3738.435039926232093 2976.073018908457016,3738.129938382007367 2976.083223658936276,3737.438323065516215 2975.018707684592755,3735.107189121938518</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>7</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Quellgrotte</ogr:NAME>
      <ogr:PERIODE>LBA</ogr:PERIODE>
      <ogr:PHASE>MTER-LBA-1</ogr:PHASE>
      <ogr:LAGE>MTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.191">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3109.499437870484599,3411.308884664509151 3106.131061397396479,3413.231746736588775 3077.38539269558396,3429.653653764465616 3058.949198590847573,3441.565911181824504 3041.660253196564554,3454.075122111190922 3011.890772523046053,3478.561045246492085 3037.660240803903889,3509.077371525280796 3114.51927002407956,3451.461838351892311 3118.276689802455167,3445.328260686919293 3126.566085641507925,3431.519289713980925 3109.499437870484599,3411.308884664509151</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>197</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Silo-NWH</ogr:NAME>
      <ogr:PHASE>NWH-6</ogr:PHASE>
      <ogr:LAGE>NWH</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.466">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2982.471563272138155,2495.882927846942039 2981.70578001666172,2495.903087906523069 2981.170344146867137,2495.922903802263136 2980.902416713295679,2495.943487441588786 2980.245509470863908,2496.017996201630012 2979.902416713295679,2496.058586485100022 2979.742542733516075,2496.081409096327206 2979.246770311820455,2496.180466703173806 2978.790661338225618,2496.279524310016313 2977.75945304672905,2496.537109082777079 2976.886898547640158,2496.79467198368593 2976.391167485122423,2496.992809069229224 2975.915206107889844,2497.210762050511221 2975.459097134295007,2497.488140847157865 2975.042651608800952,2497.785335539548214 2974.705532979514828,2498.122162023411875 2922.712065740092385,2561.686084376264262 2922.203141100984794,2562.335831412382049 2921.807292444263567,2563.0985684235402 2921.665926786688487,2563.635325484616715 2921.524519769944163,2564.341567508255594 2921.467981778744161,2564.991292672524651 2921.411443787550525,2565.697534696164894 2921.467981778744161,2566.432035149488001 2921.637637111507956,2567.223008718486199 2921.779002769083036,2567.844519196768033 2922.123648768083967,2568.706423174020529 2922.24719426651609,2568.987477706024947 2946.669136820913991,2621.789617197468033 2948.068896714117727,2624.292259844442015 2948.695115971393534,2624.991524748060783 2949.210823515074026,2625.359562361308235 2949.984405510186207,2625.654001200643961 2950.573773743206857,2625.874819394222413 2952.047235684920906,2625.948418168132775 2952.636603917940647,2625.874819394222413 2953.410144553884038,2625.727599974554778 2954.294238262576528,2625.433183007065054 2955.178290612108412,2625.065145393818057 2955.804509869376488,2624.770706554481876 2956.578091864489124,2624.402668941230786 2975.887840633123687,2614.357671462174039 2976.493876613365956,2614.034755474656322 2977.019096767124665,2613.691651769951022 2977.524092284681046,2613.32836034806769 2978.170494818983116,2612.763257369908843 2978.938121093002792,2611.855050687046059 2979.362300784082436,2611.209218712008351 2979.584523627304861,2610.765198293244794 2979.773516349584042,2610.279653541786047 2979.801907447148096,2610.170896395867203 2986.916352439936418,2575.433991288574816 2987.044007577519096,2574.66886337502865 2987.146120666202933,2573.903722670764182 2990.974449975422885,2530.367389009817089 2991.104715459717681,2528.805610079247799 2991.102303062608826,2526.029960464576106 2991.025910681502864,2503.337572772536987 2990.996948711228015,2502.643574603954676 2990.967375420463213,2501.989203511756841 2990.899060381187155,2501.274779996191683 2990.808491372476055,2500.718559200766776 2990.678222617637857,2500.161704089343857 2990.508336150432115,2499.604259676345009 2990.31798865836754,2499.0861142471108 2990.106610175212609,2498.646895515996221 2989.816142342784588,2498.227065716434026 2989.484259370685322,2497.885079249926093 2989.072447619609193,2497.581496242227786 2988.620671479044177,2497.297114963942022 2988.14812865337808,2497.071846207609724 2987.655512834232923,2496.866086042026836 2986.787035053469481,2496.595094628652078 2985.759922693096996,2496.321634110878222 2985.305395947731085,2496.215552807089807 2984.811210473120809,2496.108859694511011 2984.257252258898006,2496.021040856680884 2983.900935016166841,2495.975907501209804 2983.386023873330487,2495.928349417800746 2982.850957365883005,2495.900276779964315 2982.471563272138155,2495.882927846942039</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>472</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Sud-Teiche</ogr:NAME>
      <ogr:PERIODE>LBA</ogr:PERIODE>
      <ogr:PHASE>OS-?</ogr:PHASE>
      <ogr:LAGE>OS</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.213">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3043.325457857971742,3752.709990367653063 3019.442373067107837,3759.351224587763681 3011.119419939973795,3767.002971817548769 2999.04431099240719,3778.225206138207341 2954.401372726373211,3836.942614961772506 2953.565993748222809,3838.171113459051867 2964.134122850479798,3850.268325083924992 2967.818218130833884,3855.250345242180174 2969.706038263173014,3859.916619979650477 2970.374684721524318,3862.798487035466678 2971.53109600003836,3866.042923657400024 2968.142375772937157,3879.383336762314684 2974.892525431198919,3897.431091320399901 2999.918788053884782,3882.14299745494327 3022.117386699887902,3923.440739438643504 3030.382595041432069,3920.370069601291561 3030.844408427249164,3922.103404139120812 3057.693526377664057,3918.465775241264055 3066.587855497968576,3915.566518787683435 3072.28808852534803,3913.011241913341109 3108.439691472967752,3887.822159291944899 3101.370841592677152,3857.233851005516954 3056.412198038108272,3775.583886338284174 3049.574612548931782,3763.174934894965645 3043.870406046780317,3752.869344292992537 3043.325457857971742,3752.709990367653063</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs><gml:innerBoundaryIs><gml:LinearRing><gml:coordinates>3005.781144814873642,3842.312838489494879 3039.43212659824394,3817.329533832144079 3074.782652916127972,3865.426644158880663 3070.02392821948979,3869.165642134810696 3071.383563847100049,3873.414503471094577 3049.629393805325435,3889.900085455877615 3037.504145987209085,3875.142146015673916 3032.463994006737721,3879.872772702246948 3005.781144814873642,3842.312838489494879</gml:coordinates></gml:LinearRing></gml:innerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>219</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Tempel-1-Magazine</ogr:NAME>
      <ogr:PERIODE>LBA</ogr:PERIODE>
      <ogr:PHASE>MTER-LBA-2b</ogr:PHASE>
      <ogr:LAGE>MTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.409">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3418.228035450451898,3364.382845247751902 3414.078800219082041,3365.540363522910866 3397.156485154938764,3376.213550974190184 3395.950715217848938,3376.996801537968167 3403.565339178503109,3386.245473869335001 3404.42841002658497,3387.218182595162034 3421.984840656268261,3375.45940037459286 3421.530829101563086,3369.665339843751099 3418.228035450451898,3364.382845247751902</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>415</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Torbau-1</ogr:NAME>
      <ogr:PERIODE>LBA</ogr:PERIODE>
      <ogr:PHASE>BK-III</ogr:PHASE>
      <ogr:LAGE>BK</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.0">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3464.088557408902943,3345.131389175747699 3458.686658352462018,3347.046283033283544 3457.440313695790792,3347.67789261541202 3454.011775050094002,3349.627478390616943 3446.831119882808707,3354.14978783420429 3448.491233669492885,3356.729011711500334 3449.230783500352118,3357.84502808309071 3465.351395592975223,3349.903499050757091 3465.41343498869719,3348.859952424064431 3464.088557408902943,3345.131389175747699</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Torbau-2</ogr:NAME>
      <ogr:PERIODE>LBA</ogr:PERIODE>
      <ogr:PHASE>BK-III</ogr:PHASE>
      <ogr:LAGE>BK</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.448">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3397.204949272640079,3325.968347819028622 3396.855034114736554,3328.682701568314314 3396.79012210148403,3329.252211982075096 3412.236818145646794,3335.911789892972138 3412.859380259100362,3336.06426757124882 3413.796878706176358,3335.915336677832784 3418.584673447810474,3335.022850779875171 3417.43588890637011,3328.327642458623814 3397.204949272640079,3325.968347819028622</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>454</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Wasserbecken</ogr:NAME>
      <ogr:PERIODE>LBA</ogr:PERIODE>
      <ogr:PHASE>BK-III</ogr:PHASE>
      <ogr:LAGE>BK</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.104">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3005.781144814873642,3842.312838489494879 3032.463994006737721,3879.872772702246948 3037.504145987209085,3875.142146015673916 3049.629393805325435,3889.900085455877615 3071.383563847100049,3873.414503471094577 3070.02392821948979,3869.165642134810696 3074.782652916127972,3865.426644158880663 3039.43212659824394,3817.329533832144079 3005.781144814873642,3842.312838489494879</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1500</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>Tempel-1</ogr:NAME>
      <ogr:PERIODE>LBA</ogr:PERIODE>
      <ogr:PHASE>MTER-LBA-2b</ogr:PHASE>
      <ogr:LAGE>MTER</ogr:LAGE>
      <ogr:STAND>plausible</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="NULLVector_Poly_Architecture.1502">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2881.111383042953094,3888.225656784957664 2892.187208336672938,3901.50008721035374 2895.199925635876752,3898.968542977876496 2894.746034852887078,3898.668164506016637 2903.391657895240314,3891.994433161668894 2902.099046464995808,3890.388587277645001 2897.060815987832484,3892.77063678024706 2894.50433184118765,3888.72811751387826 2905.081369556909522,3878.251907951113935 2901.726530689279116,3875.11404800561013 2881.111383042953094,3888.225656784957664</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1502</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>possible-occupation</ogr:NAME>
      <ogr:PERIODE>LBA</ogr:PERIODE>
      <ogr:PHASE>KNW-LBA-2b</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>vermutet</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.1501">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2933.564228270262447,3900.724084671313904 2929.780477663040074,3916.68876804413685 2927.053511168226578,3921.457813872451879 2924.59603498809247,3925.83570519717432 2938.367310132858165,3923.654329448688713 2938.022611452707679,3922.551293672207976 2939.439665842779505,3914.531939842720931 2941.108503097068024,3907.62632295110825 2949.517438614757339,3907.750084129520019 2949.549020803207895,3906.672306788491369 2946.597198403030688,3906.560163183472014 2947.228203033415866,3900.214451189953706 2953.755702065938294,3898.661882669767238 2943.147078871556005,3881.728789783299817 2937.570092296393341,3884.564483478479815 2937.198967072765754,3886.259179020561078 2940.024354449130442,3887.890748115838051 2939.933808752418372,3889.236654973601162 2946.286417161886675,3893.070440770055484 2943.790238293351649,3902.928177071231858 2941.712172488068973,3902.770498435651007 2933.564228270262447,3900.724084671313904</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1501</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>possible-occupation</ogr:NAME>
      <ogr:PERIODE>LBA</ogr:PERIODE>
      <ogr:PHASE>KNW-LBA-2b</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>vermutet</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.1503">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2956.46201954715616,3875.798743129873856 2960.032088145900161,3888.64707448749823 2970.702742864246829,3886.912638307081579 2968.142375772937157,3879.383336762314684 2969.113947293541969,3872.519067391682711 2956.46201954715616,3875.798743129873856</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1503</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>possible-occupation</ogr:NAME>
      <ogr:PERIODE>LBA</ogr:PERIODE>
      <ogr:PHASE>KNW-LBA-2b</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>vermutet</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.1504">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2993.349309524422551,3956.038473065666039 2998.090778410512939,3953.262218257363202 2992.94378915916468,3941.533321539139251 2990.457254215216381,3942.637477478623168 2990.85619825893491,3943.765310426867018 2988.327095770076539,3945.338974197712105 2993.349309524422551,3956.038473065666039</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1504</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>possible-occupation</ogr:NAME>
      <ogr:PERIODE>LBA</ogr:PERIODE>
      <ogr:PHASE>KNW-LBA-2b</ogr:PHASE>
      <ogr:LAGE>WTER</ogr:LAGE>
      <ogr:STAND>vermutet</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.1505">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3050.478668541218667,3999.030665423541905 3059.698680055440036,3998.274978800970075 3052.244440802553981,3995.213396435665345 3050.478668541218667,3999.030665423541905</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1505</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>possible-occupation</ogr:NAME>
      <ogr:PERIODE>LBA</ogr:PERIODE>
      <ogr:PHASE>KNW-LBA-2b</ogr:PHASE>
      <ogr:LAGE>NOV</ogr:LAGE>
      <ogr:STAND>vermutet</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.1506">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3060.884047276962519,3998.025427806965126 3084.529003958914018,3996.029019854926901 3070.752785021443287,3984.862872590032566 3065.965484983638817,3992.454382809696654 3063.433278999296817,3991.032137162061645 3060.884047276962519,3998.025427806965126</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1506</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>possible-occupation</ogr:NAME>
      <ogr:PERIODE>LBA</ogr:PERIODE>
      <ogr:PHASE>KNW-LBA-2b</ogr:PHASE>
      <ogr:LAGE>NOV</ogr:LAGE>
      <ogr:STAND>vermutet</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Vector_Poly_Emprise fid="Vector_Poly_Architecture.1507">
      <ogr:geometryProperty><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3069.431168821625306,3955.35220783215027 3087.274064892965725,3967.954533029390859 3085.277656940927955,3941.377352167883146 3069.431168821625306,3955.35220783215027</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:ID>1507</ogr:ID>
      <ogr:ART>surface</ogr:ART>
      <ogr:TYPE>surface</ogr:TYPE>
      <ogr:NAME>possible-occupation</ogr:NAME>
      <ogr:PERIODE>LBA</ogr:PERIODE>
      <ogr:PHASE>KNW-LBA-2b</ogr:PHASE>
      <ogr:LAGE>NOV</ogr:LAGE>
      <ogr:STAND>vermutet</ogr:STAND>
    </ogr:Vector_Poly_Emprise>
  </gml:featureMember>
</ogr:FeatureCollection>
