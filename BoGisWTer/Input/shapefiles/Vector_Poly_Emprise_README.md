 # Description of the data from the shapefile  
 
  This is a shapefile of polygons, documenting the perimeter and surface of building 
 See ‘Qm’ for the surface in square meter (Quadratmeter) 
 See ‘Perim’ for the perimeter in meter 
 
 
 
_U_s_a_g_e:

     data("objet")
     
_F_o_r_m_a_t:

     A data frame with 554 observations on the following 9 variables.

     ‘Kennung’ a factor with levels ‘1’ ‘1-2-3’ ‘10’ ‘10-10a-11-12’
          ‘10a’ ‘11’ ‘12’ ‘13’ ‘14’ ‘15’ ‘16’ ‘17’ ‘18’ ‘19’ ‘1a’ ‘1b’
          ‘1c’ ‘2’ ‘20’ ‘21’ ‘2b’ ‘2c’ ‘3’ ‘3a’ ‘3b’ ‘4’ ‘5’ ‘5a’ ‘5b’
          ‘6’ ‘7’ ‘7-7a-8-8a’ ‘8’ ‘8a’ ‘9’ ‘total’

     ‘Art’ a logical vector

     ‘Type’ a logical vector

     ‘Name’ a numeric vector

     ‘Stand’ a logical vector

     ‘Periode’ a logical vector

     ‘Phase’ a numeric vector

     ‘Qm’ a numeric vector

     ‘Perim’ a numeric vector

_E_x_a_m_p_l_e_s:

     data(objet)
     ## maybe str(objet) ; plot(objet) ...
     
