 # Description of the data from the shapefile  
 
  This is a shapefile of polygons, go to the category Art, Name, Kennung, Type, and Periode to see 
 the documented variable in this file 
 
 
_U_s_a_g_e:

     data("objet")
     
_F_o_r_m_a_t:

     A data frame with 1482 observations on the following 9 variables.

     ‘Id’ a numeric vector

     ‘Kennung’ a factor with levels ‘BKA-EBA-Gebaude-1’
          ‘BKA-EBA-Gebaude-2’ ‘BKA-EBA-Gebaude-3’ ‘Bo09-101’ ‘Bo09-105’
          ‘Bo10-48’ ‘Bo10-49’ ‘Bo10-86’ ‘Bo13-09’ ‘Bo13-10’ ‘Bo13-17’
          ‘Bo13-19;Bo13-22;Bo13-25’ ‘Bo13-23’ ‘Bo13-34’ ‘Bo13-39’
          ‘Bo13-47’ ‘Bo13-62’ ‘Bo13-63’ ‘Bo13-68’ ‘Brunnen’ ‘Gebaude-C’
          ‘Gebaude-J’ ‘Gebaude-K’ ‘Hutte1969’ ‘Komplex-1’ ‘Komplex-2’
          ‘Komplex-3’ ‘Mauer’ ‘Mauer 15’ ‘Mauer 6,8 ,13 , P’ ‘Mauer 7’
          ‘Mauer XA, XB, XC’ ‘Monumentalgebaude’ ‘Nord-Magazine’
          ‘Nordwest-Magazine’ ‘Quellgrotte’ ‘Sud-Teich’
          ‘Sudost-Magazine’ ‘Terrasse’ ‘UST-Teich’ ‘West-Magazine’

     ‘Art’ a factor with levels ‘Grube’ ‘Mauer’ ‘Pflaster’
          ‘Steinpflasterung’ ‘Stutzenbasis’ ‘Teich’ ‘Zerstorung’

     ‘Type’ a factor with levels ‘Gebaude’ ‘Grube’ ‘Magazin’
          ‘Mauerreste’ ‘Ofen’ ‘Silogrube’ ‘Stadtmauer’ ‘Teich’ ‘Tempel’

     ‘Name’ a factor with levels ‘Abschlussmauer’ ‘Abschnittsmauer’
          ‘althet-Bauwerk’ ‘BK-Mauer’ ‘BK-MittlererBurghof’
          ‘BK-UntererBurghof’ ‘BKA-Bau-I’ ‘BKA-Bau-II’
          ‘BKA-Befestigung’ ‘BKA-EBA-Gebaude-1’ ‘BKA-EBA-Gebaude-2’
          ‘BKA-EBA-Gebaude-3’ ‘Blockade’ ‘Gebaude’ ‘Gebaude-1’
          ‘Gebaude-1-BK-Vc’ ‘Gebaude-10’ ‘Gebaude-11’ ‘Gebaude-12’
          ‘Gebaude-13’ ‘Gebaude-14’ ‘Gebaude-15’ ‘Gebaude-16’
          ‘Gebaude-17’ ‘Gebaude-18’ ‘Gebaude-19’ ‘Gebaude-2’
          ‘Gebaude-20’ ‘Gebaude-21’ ‘Gebaude-22’ ‘Gebaude-23’
          ‘Gebaude-24’ ‘Gebaude-25’ ‘Gebaude-27’ ‘Gebaude-28’
          ‘Gebaude-29’ ‘Gebaude-3’ ‘Gebaude-3-BK-Vc’ ‘Gebaude-30’
          ‘Gebaude-31’ ‘Gebaude-32’ ‘Gebaude-33’ ‘Gebaude-34’
          ‘Gebaude-35’ ‘Gebaude-36’ ‘Gebaude-37’ ‘Gebaude-38’
          ‘Gebaude-39’ ‘Gebaude-4’ ‘Gebaude-40’ ‘Gebaude-41’
          ‘Gebaude-42’ ‘Gebaude-43’ ‘Gebaude-44’ ‘Gebaude-45’
          ‘Gebaude-46’ ‘Gebaude-47’ ‘Gebaude-48’ ‘Gebaude-49’
          ‘Gebaude-5’ ‘Gebaude-50’ ‘Gebaude-51’ ‘Gebaude-52’
          ‘Gebaude-53’ ‘Gebaude-54’ ‘Gebaude-55’ ‘Gebaude-56’
          ‘Gebaude-57’ ‘Gebaude-58’ ‘Gebaude-59’ ‘Gebaude-6’
          ‘Gebaude-60’ ‘Gebaude-61’ ‘Gebaude-62’ ‘Gebaude-63’
          ‘Gebaude-64’ ‘Gebaude-65’ ‘Gebaude-66’ ‘Gebaude-67’
          ‘Gebaude-68’ ‘Gebaude-69’ ‘Gebaude-7’ ‘Gebaude-70’
          ‘Gebaude-71’ ‘Gebaude-72’ ‘Gebaude-73’ ‘Gebaude-74’
          ‘Gebaude-75’ ‘Gebaude-76’ ‘Gebaude-77’ ‘Gebaude-78’
          ‘Gebaude-79’ ‘Gebaude-8’ ‘Gebaude-80’ ‘Gebaude-81’
          ‘Gebaude-82’ ‘Gebaude-82bis’ ‘Gebaude-83’ ‘Gebaude-84’
          ‘Gebaude-85’ ‘Gebaude-86’ ‘Gebaude-87’ ‘Gebaude-88’
          ‘Gebaude-89’ ‘Gebaude-9’ ‘Gebaude-90’ ‘Gebaude-91’
          ‘Gebaude-92’ ‘Gebaude-93’ ‘Gebaude-94’ ‘Gebaude-95’
          ‘Gebaude-96’ ‘Gebaude-97’ ‘Gebaude-A’ ‘Gebaude-B’
          ‘Gebaude-BKA-MBA’ ‘Gebaude-C’ ‘Gebaude-D’ ‘Gebaude-E’
          ‘Gebaude-F’ ‘Gebaude-G’ ‘Gebaude-H’ ‘Gebaude-J’ ‘Gebaude-K’
          ‘Gebaude-L’ ‘Gebaude-LBA-KSW’ ‘Gebaude-M’ ‘Gebaude-N’
          ‘Gebaude-X’ ‘Gebaude-Y’ ‘Gebaude-Z’ ‘Gefassraum’ ‘Grube’
          ‘HausAmHang’ ‘HausAmHang-Altbau’ ‘Hutte1969’ ‘Komplex-1’
          ‘Komplex-2’ ‘Komplex-3’ ‘Mauer’ ‘Mauerreste’
          ‘MBA-Stadtmauer-Vermutung’ ‘Monumentalgebaude’
          ‘NWH-EBA-GEbaude’ ‘NWH-EBA-Mauerreste’ ‘Ofen-1’ ‘Ofen-2’
          ‘Ofen-3’ ‘Ofen-4’ ‘Pithosgebaude’ ‘Pithoshaus’
          ‘Poternenmauer’ ‘Quellgrotte’ ‘Rundbecken’ ‘Silo-BKA-1’
          ‘Silo-BKA-10’ ‘Silo-BKA-11’ ‘Silo-BKA-2’ ‘Silo-BKA-3’
          ‘Silo-BKA-4’ ‘Silo-BKA-5’ ‘Silo-BKA-6’ ‘Silo-BKA-7’
          ‘Silo-BKA-8’ ‘Silo-BKA-9’ ‘Silo-NWH’ ‘Stadtmauer’
          ‘Stadtmauer-KSW’ ‘Steinpflasterung’ ‘Sud-Teiche’ ‘Sudhalle’
          ‘Sudwesthalle’ ‘Tempel-1’ ‘Terrassenmauer’ ‘Torbau-1’
          ‘Torbau-2’ ‘UST-Teich’ ‘Verbindungsbau’ ‘Viereckbau’
          ‘Wasserbecken’ ‘WTER-EBA-Oeen’ ‘Zerstorung’

     ‘Stand’ a factor with levels ‘nachgewiesen’ ‘vermutet’

     ‘Periode’ a factor with levels ‘?’ ‘EBA’ ‘LBA’ ‘LBA-IA’ ‘LBA-late’
          ‘LBA-MBA’ ‘LBA-old’ ‘LBA2’ ‘MBA’ ‘MBA?’

     ‘Phase’ a factor with levels ‘AM-1’ ‘BK-III’ ‘BK-IVa’ ‘BK-IVb1’
          ‘BK-IVb2’ ‘BK-IVb3’ ‘BK-IVc1’ ‘BK-IVc2-IVc5’ ‘BK-IVd’ ‘BK-Vc’
          ‘BK-Vg’ ‘BKA-10’ ‘BKA-11’ ‘BKA-12’ ‘BKA-13’ ‘BKA-14’ ‘BKA-9’
          ‘BKA-LBA-1’ ‘KNW-LBA-1’ ‘KNW-LBA-2a’ ‘KNW-LBA-2b’ ‘KNW-LBA-3’
          ‘KNW-LBA-4’ ‘KNW-MBA-1’ ‘KSW-LBA’ ‘MBA-1?’ ‘MTER-LBA-1’
          ‘MTER-LBA-1a’ ‘MTER-LBA-2a’ ‘MTER-LBA-2b’ ‘NOV-LBA-1a’
          ‘NOV-LBA-2a’ ‘NOV-LBA-2b’ ‘NOV-LBA-2c’ ‘NOV-MBA-1’ ‘NWH-5’
          ‘NWH-6’ ‘NWH-7’ ‘NWH-8a’ ‘NWH-9’ ‘OS-?’ ‘PM-1’ ‘PM-2’
          ‘WTER-5’ ‘WTER-LBA’ ‘WTER-LBA-1a’ ‘WTER-LBA-1b’ ‘WTER-LBA-2a’
          ‘WTER-LBA-2b’ ‘WTER-LBA-2c’ ‘WTER-LBA-3a’ ‘WTER-LBA-3b’
          ‘WTER-MBA’ ‘WTER-MBA-1’

     ‘Lage’ a factor with levels ‘BK’ ‘BKA’ ‘KNW’ ‘KSW’ ‘MTER’ ‘NOV’
          ‘NWH’ ‘NWTE’ ‘OS’ ‘WTER’

_E_x_a_m_p_l_e_s:

     data(objet)
     ## maybe str(objet) ; plot(objet) ...
     
