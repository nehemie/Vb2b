<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ WTer-Grabungskanten.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>2819.76751945699</gml:X><gml:Y>3829.211580432461</gml:Y></gml:coord>
      <gml:coord><gml:X>3088.226099493208</gml:X><gml:Y>4099.038630445798</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                                 
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.0">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2907.51729266599,3930.11607019706 2913.43420484684,3935.45063981094 2913.43420484684,3935.45063981094 2920.05866695086,3927.97758958616 2914.20067153657,3922.66707037881 2907.51729266599,3930.11607019706</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>1</ogr:id>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>II</ogr:RomischeZa>
      <ogr:ArabischeZ>2</ogr:ArabischeZ>
      <ogr:Zusammense>73-II-2</ogr:Zusammense>
      <ogr:Car>73.II.2</ogr:Car>
      <ogr:xmin>2907.517289999999775</ogr:xmin>
      <ogr:ymin>3922.667069999999967</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.1">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2906.39913558762,3931.26244682782 2904.40084743228,3933.58922070733 2910.51031249156,3938.69404485832 2912.31187862261,3936.60033984552 2912.31187862261,3936.60033984552 2906.39913558762,3931.26244682782</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>2</ogr:id>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>II</ogr:RomischeZa>
      <ogr:ArabischeZ>2</ogr:ArabischeZ>
      <ogr:Zusammense>73/II-2-NW-Erweiterung
</ogr:Zusammense>
      <ogr:Car>73.II.2.NW-Erweiterung</ogr:Car>
      <ogr:xmin>2904.400839999999789</ogr:xmin>
      <ogr:ymin>3931.262439999999970</ogr:ymin>
      <ogr:Zusatz1>NW-Erweiterung</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.2">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2906.01590224276,3928.82617770693 2912.74218287806,3921.17907767627 2906.80974274283,3915.96048684376 2900.10315920778,3923.46091087888 2906.01590224276,3928.82617770693</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>3</ogr:id>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>II</ogr:RomischeZa>
      <ogr:ArabischeZ>3</ogr:ArabischeZ>
      <ogr:Zusammense>73/II-3</ogr:Zusammense>
      <ogr:Car>73.II.3</ogr:Car>
      <ogr:xmin>2900.103149999999914</ogr:xmin>
      <ogr:ymin>3915.960480000000189</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.3">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2933.64063724006,3916.96446571381 2939.2338041586,3922.09402121638 2944.41330745712,3916.44535632196 2938.89639458131,3911.42295491881 2933.64063724006,3916.96446571381</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>4</ogr:id>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>III</ogr:RomischeZa>
      <ogr:ArabischeZ>2</ogr:ArabischeZ>
      <ogr:Zusammense>73/III-2</ogr:Zusammense>
      <ogr:Car>73.III.2</ogr:Car>
      <ogr:xmin>2933.640629999999874</ogr:xmin>
      <ogr:ymin>3911.422950000000128</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.4">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2938.82016941445,3911.35356223606 2944.41330745712,3916.44535632196 2948.56636716477,3911.96534601836 2942.91565884174,3906.61586186873 2938.82016941445,3911.35356223606</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>5</ogr:id>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>III</ogr:RomischeZa>
      <ogr:ArabischeZ>3</ogr:ArabischeZ>
      <ogr:Zusammense>73/III-3</ogr:Zusammense>
      <ogr:Car>73.III.3</ogr:Car>
      <ogr:xmin>2938.820160000000214</ogr:xmin>
      <ogr:ymin>3906.615859999999884</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.5">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2944.66056931056,3916.17862872133 2949.75912006765,3920.95879676047 2953.15303998474,3917.23603795912 2948.00879815464,3912.49708071922 2944.66056931056,3916.17862872133</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>6</ogr:id>
      <ogr:Jahr>71</ogr:Jahr>
      <ogr:RomischeZa>XII</ogr:RomischeZa>
      <ogr:ArabischeZ>3</ogr:ArabischeZ>
      <ogr:Zusammense>71/XII-3</ogr:Zusammense>
      <ogr:Car>71.XII.3</ogr:Car>
      <ogr:xmin>2944.660559999999805</ogr:xmin>
      <ogr:ymin>3912.497080000000096</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.6">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2940.71211981405,3920.48179910461 2945.88137956256,3925.17996174548 2949.30314470775,3921.42436585442 2944.04877151207,3916.84291208684 2940.71211981405,3920.48179910461</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>7</ogr:id>
      <ogr:Jahr>71</ogr:Jahr>
      <ogr:RomischeZa>XII</ogr:RomischeZa>
      <ogr:ArabischeZ>4</ogr:ArabischeZ>
      <ogr:Zusammense>71/XII-4</ogr:Zusammense>
      <ogr:Car>71.XII.4</ogr:Car>
      <ogr:xmin>2940.712120000000141</ogr:xmin>
      <ogr:ymin>3916.842909999999847</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.7">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2936.81787481212,3924.81274792503 2936.81787481212,3924.79605638773 2941.94217676126,3929.40292068077 2945.34725036916,3925.71409093888 2940.19239514056,3921.04860065849 2936.81787481212,3924.81274792503</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>8</ogr:id>
      <ogr:Jahr>71</ogr:Jahr>
      <ogr:RomischeZa>XII</ogr:RomischeZa>
      <ogr:ArabischeZ>5</ogr:ArabischeZ>
      <ogr:Zusammense>71/XII-5</ogr:Zusammense>
      <ogr:Car>71.XII.5</ogr:Car>
      <ogr:xmin>2936.817869999999857</ogr:xmin>
      <ogr:ymin>3921.048600000000079</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.8">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2932.83258871881,3929.10995663071 2937.93868182972,3933.77764780786 2941.37967038008,3930.01465036423 2936.27533331632,3925.40932854275 2936.27533331632,3925.40932854275 2932.83258871881,3929.10995663071</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>9</ogr:id>
      <ogr:Jahr>71</ogr:Jahr>
      <ogr:RomischeZa>XII</ogr:RomischeZa>
      <ogr:ArabischeZ>6</ogr:ArabischeZ>
      <ogr:Zusammense>71/XII-6</ogr:Zusammense>
      <ogr:Car>71.XII.6</ogr:Car>
      <ogr:xmin>2932.832579999999780</ogr:xmin>
      <ogr:ymin>3925.409320000000207</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.9">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2928.73224122065,3933.51976431741 2933.95438190227,3938.05851374933 2937.39712649978,3934.37077983589 2932.30254707736,3929.85771568254 2932.30254707736,3929.85771568254 2928.73224122065,3933.51976431741</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>10</ogr:id>
      <ogr:Jahr>71</ogr:Jahr>
      <ogr:RomischeZa>XII</ogr:RomischeZa>
      <ogr:ArabischeZ>7</ogr:ArabischeZ>
      <ogr:Zusammense>71/XII-7</ogr:Zusammense>
      <ogr:Car>71.XII.7</ogr:Car>
      <ogr:xmin>2928.732239999999820</ogr:xmin>
      <ogr:ymin>3929.857710000000225</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.10">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2928.22675087263,3934.06852406627 2924.84864306862,3937.87941979034 2929.93139945125,3942.5070039596 2933.34835569971,3938.74190499902 2928.22675087263,3934.06852406627</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>11</ogr:id>
      <ogr:Jahr>71</ogr:Jahr>
      <ogr:RomischeZa>XII</ogr:RomischeZa>
      <ogr:ArabischeZ>8</ogr:ArabischeZ>
      <ogr:Zusammense>71/XII-8</ogr:Zusammense>
      <ogr:Car>71.XII.8</ogr:Car>
      <ogr:xmin>2924.848640000000159</ogr:xmin>
      <ogr:ymin>3934.068519999999808</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.11">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2920.82811223836,3942.13307289844 2924.90267138747,3945.88527768449 2925.48290924098,3945.16320391123 2926.56255265397,3946.2197493633 2929.40273829582,3943.03566511502 2924.2321743123,3938.35507976335 2920.82811223836,3942.13307289844</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>12</ogr:id>
      <ogr:Jahr>71</ogr:Jahr>
      <ogr:RomischeZa>XII</ogr:RomischeZa>
      <ogr:ArabischeZ>9</ogr:ArabischeZ>
      <ogr:Zusammense>71/XII-9</ogr:Zusammense>
      <ogr:Car>71.XII.9</ogr:Car>
      <ogr:xmin>2920.828109999999924</ogr:xmin>
      <ogr:ymin>3938.355079999999816</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.12">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2953.65385572412,3916.58572002331 2961.4594098576,3908.1662459018 2950.65003292556,3897.86116038849 2942.91565884174,3906.61586186873 2942.84998027329,3906.55368444941 2953.65385572412,3916.58572002331</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>13</ogr:id>
      <ogr:Jahr>71</ogr:Jahr>
      <ogr:RomischeZa>XII</ogr:RomischeZa>
      <ogr:ArabischeZ>2</ogr:ArabischeZ>
      <ogr:Zusammense>71/XII-2</ogr:Zusammense>
      <ogr:Car>71.XII.2</ogr:Car>
      <ogr:xmin>2942.849979999999960</ogr:xmin>
      <ogr:ymin>3897.861159999999927</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.13">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2961.4594098576,3908.1662459018 2969.39651827423,3899.46173750013 2958.54328991447,3889.13472627297 2950.65003292556,3897.86116038849 2961.4594098576,3908.1662459018</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>14</ogr:id>
      <ogr:Jahr>71</ogr:Jahr>
      <ogr:RomischeZa>XII</ogr:RomischeZa>
      <ogr:ArabischeZ>1</ogr:ArabischeZ>
      <ogr:Zusammense>71/XII-1</ogr:Zusammense>
      <ogr:Car>71.XII.1</ogr:Car>
      <ogr:xmin>2950.650029999999788</ogr:xmin>
      <ogr:ymin>3889.134720000000016</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.14">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2891.02373404712,3914.83257267512 2895.93189152653,3909.17754894977 2890.67059589314,3904.11266733134 2885.5683530316,3909.32701006677 2891.02373404712,3914.83257267512</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>15</ogr:id>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>II</ogr:RomischeZa>
      <ogr:ArabischeZ>5</ogr:ArabischeZ>
      <ogr:Zusammense>73/II-5</ogr:Zusammense>
      <ogr:Car>73.II.5</ogr:Car>
      <ogr:xmin>2885.117510000000038</ogr:xmin>
      <ogr:ymin>3902.609869999999773</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.15">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2885.0350677486,3908.339647769 2891.3789488748,3900.68184163909 2885.32829334013,3895.50574932652 2878.70311749151,3903.22617751795 2885.0350677486,3908.339647769</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>16</ogr:id>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>II</ogr:RomischeZa>
      <ogr:ArabischeZ>6</ogr:ArabischeZ>
      <ogr:Zusammense>73/II-6</ogr:Zusammense>
      <ogr:Car>73.II.6</ogr:Car>
      <ogr:xmin>2877.506820000000062</ogr:xmin>
      <ogr:ymin>3895.232309999999870</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.16">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2877.12125722884,3901.88887260505 2883.11013570862,3894.51199250992 2877.25030450857,3889.26015447569 2870.93461190081,3896.64757114561 2877.12125722884,3901.88887260505</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>17</ogr:id>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>II</ogr:RomischeZa>
      <ogr:ArabischeZ>7</ogr:ArabischeZ>
      <ogr:Zusammense>73/II-7</ogr:Zusammense>
      <ogr:Car>73.II.7</ogr:Car>
      <ogr:xmin>2870.173729999999978</ogr:xmin>
      <ogr:ymin>3888.723160000000007</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.17">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2869.43453029304,3895.23467192701 2875.65701436066,3887.733882481 2869.57351000291,3882.83685441488 2863.40750337895,3890.36637728369 2869.43453029304,3895.23467192701</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>19</ogr:id>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>II</ogr:RomischeZa>
      <ogr:ArabischeZ>8</ogr:ArabischeZ>
      <ogr:Zusammense>73/II-8</ogr:Zusammense>
      <ogr:Car>73.II.8</ogr:Car>
      <ogr:xmin>2863.612579999999980</ogr:xmin>
      <ogr:ymin>3882.836850000000140</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.18">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2960.8019553133,3886.32665863149 2953.71576270259,3894.75859701991 2955.69232681359,3898.56453429747 2963.80885603536,3889.102259298 2960.8019553133,3886.32665863149</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>20</ogr:id>
      <ogr:Jahr>70</ogr:Jahr>
      <ogr:RomischeZa>II</ogr:RomischeZa>
      <ogr:ArabischeZ>2</ogr:ArabischeZ>
      <ogr:Zusammense>70/II-2</ogr:Zusammense>
      <ogr:Car>70.II.2</ogr:Car>
      <ogr:xmin>2953.715760000000046</ogr:xmin>
      <ogr:ymin>3886.326649999999972</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.19">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2971.08429414606,3882.1842849095 2969.82265747947,3875.91815613207 2960.82298259108,3886.34768590927 2963.78782875758,3889.06020474245 2967.93020247957,3884.2449581316 2971.08429414606,3882.1842849095</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>21</ogr:id>
      <ogr:Jahr>70</ogr:Jahr>
      <ogr:RomischeZa>II</ogr:RomischeZa>
      <ogr:ArabischeZ>1</ogr:ArabischeZ>
      <ogr:Zusammense>70/II-1</ogr:Zusammense>
      <ogr:Car>70.II.1</ogr:Car>
      <ogr:xmin>2960.822979999999916</ogr:xmin>
      <ogr:ymin>3875.918149999999969</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.20">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2958.13149103568,3878.69375679858 2961.79023736881,3882.92023963168 2964.79713809086,3879.40868424299 2961.49585547993,3875.58171968764 2958.13149103568,3878.69375679858</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>22</ogr:id>
      <ogr:Jahr>70</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>6</ogr:ArabischeZ>
      <ogr:Zusammense>70/I-6</ogr:Zusammense>
      <ogr:Car>70.I.6</ogr:Car>
      <ogr:xmin>2958.131490000000213</ogr:xmin>
      <ogr:ymin>3875.581720000000132</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.21">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2952.83261703597,3872.55379168781 2957.77402731347,3878.3152657986 2961.13839175773,3875.28733779877 2956.17595420245,3869.58894552131 2952.83261703597,3872.55379168781</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>23</ogr:id>
      <ogr:Jahr>70</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>5</ogr:ArabischeZ>
      <ogr:Zusammense>70/I-5</ogr:Zusammense>
      <ogr:Car>70.I.5</ogr:Car>
      <ogr:xmin>2952.832609999999931</ogr:xmin>
      <ogr:ymin>3869.588940000000093</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.22">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2947.44963392516,3866.37177202149 2952.32796236933,3872.0281097434 2955.65027225804,3869.0632635769 2950.75091653609,3863.42795313277 2947.44963392516,3866.37177202149</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>24</ogr:id>
      <ogr:Jahr>70</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>4</ogr:ArabischeZ>
      <ogr:Zusammense>70/I-4</ogr:Zusammense>
      <ogr:Car>70.I.4</ogr:Car>
      <ogr:xmin>2947.449630000000070</ogr:xmin>
      <ogr:ymin>3863.427949999999782</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.23">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2942.382059981,3860.50516152182 2947.09217020296,3865.95122646596 2950.33037098056,3862.98638029946 2945.57820620304,3857.62442446643 2942.382059981,3860.50516152182</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>25</ogr:id>
      <ogr:Jahr>70</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>3</ogr:ArabischeZ>
      <ogr:Zusammense>70/I-3</ogr:Zusammense>
      <ogr:Car>70.I.3</ogr:Car>
      <ogr:xmin>2942.382059999999910</ogr:xmin>
      <ogr:ymin>3857.624420000000100</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.24">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2936.87291320353,3854.15492363329 2941.62760318889,3859.61805046668 2945.07355153641,3857.03566068868 2940.15316853668,3851.29521385567 2936.87291320353,3854.15492363329</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>26</ogr:id>
      <ogr:Jahr>70</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>2</ogr:ArabischeZ>
      <ogr:Zusammense>70/I-2</ogr:Zusammense>
      <ogr:Car>70.I.2</ogr:Car>
      <ogr:xmin>2936.872910000000047</ogr:xmin>
      <ogr:ymin>3851.295210000000225</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.25">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2936.47339492578,3853.77643263331 2939.75365025893,3850.91672285569 2934.51785809255,3844.77675774492 2931.19554820385,3847.78365846698 2936.47339492578,3853.77643263331</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>27</ogr:id>
      <ogr:Jahr>70</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>1</ogr:ArabischeZ>
      <ogr:Zusammense>70/I-1</ogr:Zusammense>
      <ogr:Car>70.I.1</ogr:Car>
      <ogr:xmin>2931.195540000000165</ogr:xmin>
      <ogr:ymin>3844.776749999999993</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.26">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2925.85461964859,3841.79088430064 2930.77500264832,3847.30003107811 2934.16039437035,3844.39826674494 2929.20372223036,3838.95486344116 2925.85461964859,3841.79088430064</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>28</ogr:id>
      <ogr:Jahr>70</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>0</ogr:ArabischeZ>
      <ogr:Zusammense>70/I-0-Nord</ogr:Zusammense>
      <ogr:Car>70.I.0.Nord</ogr:Car>
      <ogr:xmin>2925.854620000000068</ogr:xmin>
      <ogr:ymin>3838.954859999999826</ogr:ymin>
      <ogr:Zusatz1>Nord</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.27">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2920.45060926,3835.69297374543 2925.56023775972,3841.34931146734 2928.86152037064,3838.44754713416 2923.80574767621,3832.88133872862 2920.45060926,3835.69297374543</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>29</ogr:id>
      <ogr:Jahr>70</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>0</ogr:ArabischeZ>
      <ogr:Zusammense>70/I-0-Sud</ogr:Zusammense>
      <ogr:Car>70.I.0.Sud</ogr:Car>
      <ogr:xmin>2920.450600000000122</ogr:xmin>
      <ogr:ymin>3832.881330000000162</ogr:ymin>
      <ogr:Zusatz1>Sud</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.28">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2964.92330175752,3860.92570707735 2969.40211192394,3856.95155157757 2967.57273875737,3853.20869613334 2965.15460181306,3850.39104091127 2959.8136732578,3855.16423296656 2964.92330175752,3860.92570707735</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>30</ogr:id>
      <ogr:Jahr>70</ogr:Jahr>
      <ogr:RomischeZa>V</ogr:RomischeZa>
      <ogr:ArabischeZ>1</ogr:ArabischeZ>
      <ogr:Zusammense>70/V-1</ogr:Zusammense>
      <ogr:Car>70.V.1</ogr:Car>
      <ogr:xmin>2959.813670000000002</ogr:xmin>
      <ogr:ymin>3850.391039999999975</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.29">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2960.73887347997,3864.66856252159 2964.25042886867,3861.5985799662 2959.11977309118,3855.83710585541 2955.73438136914,3858.9070884108 2960.73887347997,3864.66856252159</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>31</ogr:id>
      <ogr:Jahr>70</ogr:Jahr>
      <ogr:RomischeZa>V</ogr:RomischeZa>
      <ogr:ArabischeZ>2</ogr:ArabischeZ>
      <ogr:Zusammense>70/V-2</ogr:Zusammense>
      <ogr:Car>70.V.2</ogr:Car>
      <ogr:xmin>2955.734379999999874</ogr:xmin>
      <ogr:ymin>3855.837100000000191</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.30">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2956.70163614687,3868.20114518806 2960.04497331335,3865.27835357711 2955.06150848029,3859.55893402187 2951.69714403604,3862.56583474393 2956.70163614687,3868.20114518806</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>32</ogr:id>
      <ogr:Jahr>70</ogr:Jahr>
      <ogr:RomischeZa>V</ogr:RomischeZa>
      <ogr:ArabischeZ>3</ogr:ArabischeZ>
      <ogr:Zusammense>70/V-3</ogr:Zusammense>
      <ogr:Car>70.V.3</ogr:Car>
      <ogr:xmin>2951.697140000000218</ogr:xmin>
      <ogr:ymin>3859.558930000000146</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.31">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2944.56889686977,3846.52202180038 2948.60613420287,3843.11560280057 2943.13904198096,3837.14385591201 2939.1438592034,3840.63438402293 2944.56889686977,3846.52202180038</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>33</ogr:id>
      <ogr:Jahr>70</ogr:Jahr>
      <ogr:RomischeZa>IV</ogr:RomischeZa>
      <ogr:ArabischeZ>2</ogr:ArabischeZ>
      <ogr:Zusammense>70/IV-2</ogr:Zusammense>
      <ogr:Car>70.IV.2</ogr:Car>
      <ogr:xmin>2939.143849999999929</ogr:xmin>
      <ogr:ymin>3837.143849999999929</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.32">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2943.89602398092,3847.15284013368 2938.59714998121,3841.09698413402 2935.35894920361,3844.04080302274 2940.74193231442,3849.97049535574 2943.89602398092,3847.15284013368</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>34</ogr:id>
      <ogr:Jahr>70</ogr:Jahr>
      <ogr:RomischeZa>IV</ogr:RomischeZa>
      <ogr:ArabischeZ>1</ogr:ArabischeZ>
      <ogr:Zusammense>70/IV-1</ogr:Zusammense>
      <ogr:Car>70.IV.1</ogr:Car>
      <ogr:xmin>2935.358940000000075</ogr:xmin>
      <ogr:ymin>3841.096979999999803</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.33">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2936.95702231464,3862.96535302168 2932.24691209268,3857.96086091085 2929.68158420393,3860.35797057738 2934.22347620368,3865.53068091043 2936.95702231464,3862.96535302168</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>35</ogr:id>
      <ogr:Jahr>70</ogr:Jahr>
      <ogr:RomischeZa>III</ogr:RomischeZa>
      <ogr:ArabischeZ>2</ogr:ArabischeZ>
      <ogr:Zusammense>70/III-2</ogr:Zusammense>
      <ogr:Car>70.III.2</ogr:Car>
      <ogr:xmin>2929.681579999999940</ogr:xmin>
      <ogr:ymin>3857.960860000000139</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.34">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2936.24209487023,3854.6806055777 2933.1300577593,3857.37209713311 2937.71400431459,3862.71302568836 2940.99425964774,3860.14769779962 2936.24209487023,3854.6806055777</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>36</ogr:id>
      <ogr:Jahr>70</ogr:Jahr>
      <ogr:RomischeZa>III</ogr:RomischeZa>
      <ogr:ArabischeZ>1</ogr:ArabischeZ>
      <ogr:Zusammense>70/III-1</ogr:Zusammense>
      <ogr:Car>70.III.1</ogr:Car>
      <ogr:xmin>2933.130050000000210</ogr:xmin>
      <ogr:ymin>3854.680600000000140</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.35">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2942.63438731432,3869.88332741019 2937.90324981458,3864.45828974382 2935.48511287028,3866.49793568815 2939.6905684256,3872.53276441004 2942.63438731432,3869.88332741019</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>37</ogr:id>
      <ogr:Jahr>70</ogr:Jahr>
      <ogr:RomischeZa>VI</ogr:RomischeZa>
      <ogr:ArabischeZ>2</ogr:ArabischeZ>
      <ogr:Zusammense>70/VI-2</ogr:Zusammense>
      <ogr:Car>70.VI.2</ogr:Car>
      <ogr:xmin>2935.485110000000077</ogr:xmin>
      <ogr:ymin>3864.458290000000034</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.36">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2947.84915220292,3876.04431979873 2943.07596014763,3870.34592752127 2940.15316853668,3872.99536452112 2944.28202684532,3879.2325262327 2947.84915220292,3876.04431979873</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>38</ogr:id>
      <ogr:Jahr>70</ogr:Jahr>
      <ogr:RomischeZa>V</ogr:RomischeZa>
      <ogr:ArabischeZ>5</ogr:ArabischeZ>
      <ogr:Zusammense>70/V-5</ogr:Zusammense>
      <ogr:Car>70.V.5</ogr:Car>
      <ogr:xmin>2940.153159999999843</ogr:xmin>
      <ogr:ymin>3870.345920000000206</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.37">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2951.73919859159,3872.67995535447 2946.86087014742,3866.93950852146 2943.3703420365,3869.96743652129 2948.33277959178,3875.6237742432 2951.73919859159,3872.67995535447</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>39</ogr:id>
      <ogr:Jahr>70</ogr:Jahr>
      <ogr:RomischeZa>V</ogr:RomischeZa>
      <ogr:ArabischeZ>4</ogr:ArabischeZ>
      <ogr:Zusammense>70/V-4</ogr:Zusammense>
      <ogr:Car>70.V.4</ogr:Car>
      <ogr:xmin>2943.370339999999942</ogr:xmin>
      <ogr:ymin>3866.939499999999953</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.38">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2939.01769553674,3873.77337379886 2935.40100375917,3876.71719268758 2940.06905942557,3882.60483046503 2943.51690255917,3879.94731027376 2939.01769553674,3873.77337379886</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>40</ogr:id>
      <ogr:Jahr>70</ogr:Jahr>
      <ogr:RomischeZa>V</ogr:RomischeZa>
      <ogr:ArabischeZ>6</ogr:ArabischeZ>
      <ogr:Zusammense>70/V-6</ogr:Zusammense>
      <ogr:Car>70.V.6</ogr:Car>
      <ogr:xmin>2935.400999999999840</ogr:xmin>
      <ogr:ymin>3873.773369999999886</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.39">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2948.60613420287,3906.78619990812 2953.82089909147,3900.5200711307 2952.13871686934,3896.69310657535 2945.66231531415,3904.13676290827 2948.60613420287,3906.78619990812</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>41</ogr:id>
      <ogr:Jahr>70</ogr:Jahr>
      <ogr:RomischeZa>II</ogr:RomischeZa>
      <ogr:ArabischeZ>3</ogr:ArabischeZ>
      <ogr:Zusammense>70/II-3</ogr:Zusammense>
      <ogr:Car>70.II.3</ogr:Car>
      <ogr:xmin>2945.662310000000161</ogr:xmin>
      <ogr:ymin>3896.693099999999959</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.40">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2940.16668510972,3839.74075717846 2929.51310102947,3829.21158043246 2924.85272475618,3834.03401962928 2928.86152037064,3838.44754713416 2929.20372223036,3838.95486344116 2934.16039437035,3844.39826674494 2934.51785809255,3844.77675774492 2935.35894920361,3844.04080302274 2938.59714998121,3841.09698413402 2939.1438592034,3840.63438402293 2940.16668510972,3839.74075717846</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>42</ogr:id>
      <ogr:Jahr>71</ogr:Jahr>
      <ogr:RomischeZa>VII</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense>71/VII</ogr:Zusammense>
      <ogr:Car>71.VII</ogr:Car>
      <ogr:xmin>2924.852719999999863</ogr:xmin>
      <ogr:ymin>3829.211580000000140</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.41">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2922.74962064809,3838.23796989109 2914.36767293479,3846.74839191052 2915.33086901976,3847.9108699441 2913.37126319172,3849.90368943024 2923.26893330623,3860.4988463649 2934.19440731913,3851.1887375421 2922.74962064809,3838.23796989109</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>43</ogr:id>
      <ogr:Jahr>71</ogr:Jahr>
      <ogr:RomischeZa>IX</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense>71/IX</ogr:Zusammense>
      <ogr:Car>71.IX</ogr:Car>
      <ogr:xmin>2913.371259999999893</ogr:xmin>
      <ogr:ymin>3838.237970000000132</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.42">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2952.09838853909,3839.20889152128 2948.60613420287,3843.11560280057 2944.56889686977,3846.52202180038 2943.89602398092,3847.15284013368 2940.74193231442,3849.97049535574 2940.15316853668,3851.29521385567 2945.07355153641,3857.03566068868 2945.57820620304,3857.62442446643 2950.33037098056,3862.98638029946 2950.75091653609,3863.42795313277 2951.69714403604,3862.56583474393 2955.06150848029,3859.55893402187 2955.73438136914,3858.9070884108 2959.11977309118,3855.83710585541 2959.8136732578,3855.16423296656 2965.15460181306,3850.39104091127 2952.09838853909,3839.20889152128</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>44</ogr:id>
      <ogr:Jahr>71</ogr:Jahr>
      <ogr:RomischeZa>XIII</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense>71/XIII</ogr:Zusammense>
      <ogr:Car>71.XIII</ogr:Car>
      <ogr:xmin>2940.153159999999843</ogr:xmin>
      <ogr:ymin>3839.208889999999883</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.43">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2941.85197501451,3860.91401709118 2937.83312238412,3863.73717802988 2943.01445304809,3869.38349990728 2946.34159678149,3866.25606426 2941.85197501451,3860.91401709118</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>45</ogr:id>
      <ogr:Jahr>71</ogr:Jahr>
      <ogr:RomischeZa>X</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense>71/X</ogr:Zusammense>
      <ogr:Car>71.X</ogr:Car>
      <ogr:xmin>2937.833119999999781</ogr:xmin>
      <ogr:ymin>3860.914009999999962</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.44">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2960.15902194587,3887.09169210431 2958.13149103568,3878.69375679858 2957.77402731347,3878.3152657986 2952.83261703597,3872.55379168781 2952.32796236933,3872.0281097434 2951.73919859159,3872.67995535447 2948.33277959178,3875.6237742432 2947.84915220292,3876.04431979873 2944.28202684532,3879.2325262327 2953.57798539879,3894.62414626531 2960.15902194587,3887.09169210431</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>46</ogr:id>
      <ogr:Jahr>71</ogr:Jahr>
      <ogr:RomischeZa>VIII</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense>71/VIII</ogr:Zusammense>
      <ogr:Car>71.VIII</ogr:Car>
      <ogr:xmin>2944.282020000000102</ogr:xmin>
      <ogr:ymin>3872.028110000000197</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.45">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2963.78782875758,3889.06020474245 2961.44989424111,3891.90040432922 2969.39651827423,3899.46173750013 2971.36117077676,3897.01148688709 2963.78782875758,3889.06020474245</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>47</ogr:id>
      <ogr:Jahr>71</ogr:Jahr>
      <ogr:RomischeZa>II</ogr:RomischeZa>
      <ogr:ArabischeZ>2</ogr:ArabischeZ>
      <ogr:Zusammense>71/II-2-Erweiterung</ogr:Zusammense>
      <ogr:Car>71.II.2.Erweiterung</ogr:Car>
      <ogr:xmin>2961.449889999999868</ogr:xmin>
      <ogr:ymin>3889.060199999999895</ogr:ymin>
      <ogr:Zusatz1>Erweiterung</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.46">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2971.08429414606,3882.1842849095 2967.93020247957,3884.2449581316 2963.80885603536,3889.102259298 2971.81069462285,3897.4822546619 2974.53421458725,3894.7587346975 2971.08429414606,3882.1842849095</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>48</ogr:id>
      <ogr:Jahr>71</ogr:Jahr>
      <ogr:RomischeZa>II</ogr:RomischeZa>
      <ogr:ArabischeZ>1</ogr:ArabischeZ>
      <ogr:Zusammense>71/II-1-Erweiterung</ogr:Zusammense>
      <ogr:Car>71.II.1.NO-Erweiterung</ogr:Car>
      <ogr:xmin>2963.808849999999893</ogr:xmin>
      <ogr:ymin>3882.184279999999944</ogr:ymin>
      <ogr:Zusatz1>NO-Erweiterung</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.47">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2959.90264759228,3921.04772735816 2963.23970743888,3917.8046343919 2958.10619076029,3911.7832013326 2954.59880074949,3915.56645347909 2959.90264759228,3921.04772735816</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>49</ogr:id>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>7</ogr:ArabischeZ>
      <ogr:Zusammense>73/I-7</ogr:Zusammense>
      <ogr:Car>73.I.7</ogr:Car>
      <ogr:xmin>2954.598800000000210</ogr:xmin>
      <ogr:ymin>3911.783199999999852</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.48">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2945.88137956256,3925.17996174548 2951.43411537704,3930.19770789274 2954.62262655487,3926.61063281768 2949.30314470775,3921.42436585442 2945.88137956256,3925.17996174548</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>50</ogr:id>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>5</ogr:ArabischeZ>
      <ogr:Zusammense>73/I-5</ogr:Zusammense>
      <ogr:Car>73.I.5</ogr:Car>
      <ogr:xmin>2945.881379999999808</ogr:xmin>
      <ogr:ymin>3921.424359999999979</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.49">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2941.94217676126,3929.40292068077 2947.71418566958,3934.18334686502 2950.96912416361,3930.46341715756 2945.34725036916,3925.71409093888 2941.94217676126,3929.40292068077</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>51</ogr:id>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>4</ogr:ArabischeZ>
      <ogr:Zusammense>73/I-4</ogr:Zusammense>
      <ogr:Car>73.I.4</ogr:Car>
      <ogr:xmin>2941.942169999999805</ogr:xmin>
      <ogr:ymin>3925.714089999999942</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.50">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2937.93868182972,3933.77764780786 2943.92782864591,3938.63397705074 2947.11633982373,3934.78119271087 2941.37967038008,3930.01465036423 2937.93868182972,3933.77764780786</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>52</ogr:id>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>3</ogr:ArabischeZ>
      <ogr:Zusammense>73/I-3</ogr:Zusammense>
      <ogr:Car>73.I.3</ogr:Car>
      <ogr:xmin>2937.938680000000204</ogr:xmin>
      <ogr:ymin>3930.014650000000074</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.51">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2971.95168766844,3909.79377930236 2972.00843242197,3909.73943066769 2976.88041780766,3904.29221198068 2971.81069462285,3897.4822546619 2971.21037130915,3896.85356082727 2969.39651827423,3899.46173750013 2966.06850499584,3903.11152001816 2971.95168766844,3909.79377930236</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>53</ogr:id>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>10</ogr:ArabischeZ>
      <ogr:Zusammense>73/I-10</ogr:Zusammense>
      <ogr:Car>73.I.10</ogr:Car>
      <ogr:xmin>2966.068499999999858</ogr:xmin>
      <ogr:ymin>3896.853560000000016</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.52">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2967.28624511925,3914.26221764921 2972.00843242197,3909.73943066769 2966.17355456801,3902.99631371939 2961.4594098576,3908.1662459018 2967.28624511925,3914.26221764921</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>54</ogr:id>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>9</ogr:ArabischeZ>
      <ogr:Zusammense>73/I-9</ogr:Zusammense>
      <ogr:Car>73.I.9</ogr:Car>
      <ogr:xmin>2961.459409999999934</ogr:xmin>
      <ogr:ymin>3902.996309999999994</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.53">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2933.95438190227,3938.05851374933 2940.37396722895,3943.40013698844 2943.56247840678,3939.34807069994 2937.39712649978,3934.37077983589 2933.95438190227,3938.05851374933</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>55</ogr:id>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>2</ogr:ArabischeZ>
      <ogr:Zusammense>73/I-2</ogr:Zusammense>
      <ogr:Car>73.I.2</ogr:Car>
      <ogr:xmin>2933.954380000000128</ogr:xmin>
      <ogr:ymin>3934.370780000000195</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.54">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2929.93139945125,3942.5070039596 2936.38832825667,3947.51863059313 2939.7096940669,3944.2636920991 2933.34835569971,3938.74190499902 2929.93139945125,3942.5070039596</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>56</ogr:id>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>1</ogr:ArabischeZ>
      <ogr:Zusammense>73/I-1</ogr:Zusammense>
      <ogr:Car>73.I.1</ogr:Car>
      <ogr:xmin>2929.931390000000192</ogr:xmin>
      <ogr:ymin>3938.741899999999987</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.55">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2929.40273829582,3943.03566511502 2926.50326256423,3946.15039856132 2930.5295380268,3950.85987755827 2935.72405509462,3947.78433985795 2929.40273829582,3943.03566511502</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>57</ogr:id>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>0</ogr:ArabischeZ>
      <ogr:Zusammense>73/I-0</ogr:Zusammense>
      <ogr:Car>73.I.0</ogr:Car>
      <ogr:xmin>2926.503259999999955</ogr:xmin>
      <ogr:ymin>3943.035660000000007</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.56">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2949.75912006765,3920.95879676047 2955.44291903792,3926.1314771397 2959.61423506703,3922.04952472081 2953.78222220495,3916.47612480255 2949.75912006765,3920.95879676047</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>58</ogr:id>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>6</ogr:ArabischeZ>
      <ogr:Zusammense>73/I-6</ogr:Zusammense>
      <ogr:Car>73.I.6</ogr:Car>
      <ogr:xmin>2949.759120000000166</ogr:xmin>
      <ogr:ymin>3916.476119999999810</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.57">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2963.2318916142,3917.81223013402 2963.23970743888,3917.8046343919 2967.28624511925,3914.26221764921 2961.4594098576,3908.1662459018 2958.09101136073,3911.79957461752 2963.2318916142,3917.81223013402</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>59</ogr:id>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>8</ogr:ArabischeZ>
      <ogr:Zusammense>73/I-8</ogr:Zusammense>
      <ogr:Car>73.I.8</ogr:Car>
      <ogr:xmin>2958.091010000000097</ogr:xmin>
      <ogr:ymin>3908.166240000000016</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.58">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2963.23970743888,3917.8046343919 2959.90264759228,3921.04772735816 2964.67197583746,3925.97660164617 2968.18049825777,3923.60000685073 2963.23970743888,3917.8046343919</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>61</ogr:id>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>7</ogr:ArabischeZ>
      <ogr:Zusammense>73/I-7-NO-Erweiterung</ogr:Zusammense>
      <ogr:Car>73.I.7.NO-Erweiterung</ogr:Car>
      <ogr:xmin>2959.902639999999792</ogr:xmin>
      <ogr:ymin>3917.804630000000088</ogr:ymin>
      <ogr:Zusatz1>NO-Erweiterung</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.59">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2967.28624511925,3914.26221764921 2963.23970743888,3917.8046343919 2963.2318916142,3917.81223013402 2968.18049825777,3923.60000685073 2972.97606124062,3920.21484146065 2967.28624511925,3914.26221764921</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>62</ogr:id>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>8</ogr:ArabischeZ>
      <ogr:Zusammense>73/I-8-NO-Erweiterung</ogr:Zusammense>
      <ogr:Car>73.I.8.NO-Erweiterung</ogr:Car>
      <ogr:xmin>2963.231890000000021</ogr:xmin>
      <ogr:ymin>3914.262209999999868</ogr:ymin>
      <ogr:Zusatz1>NO-Erweiterung</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.60">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2972.00843242197,3909.73943066769 2967.28624511925,3914.26221764921 2972.97606124062,3920.21484146065 2977.99846794872,3916.66185665368 2972.00843242197,3909.73943066769</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>63</ogr:id>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>9</ogr:ArabischeZ>
      <ogr:Zusammense>73/I-9-NO-Erweiterung</ogr:Zusammense>
      <ogr:Car>73.I.9.NO-Erweiterung</ogr:Car>
      <ogr:xmin>2967.286239999999907</ogr:xmin>
      <ogr:ymin>3909.739430000000084</ogr:ymin>
      <ogr:Zusatz1>NO-Erweiterung</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.61">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2976.88041780766,3904.29221198068 2972.00843242197,3909.73943066769 2971.95168766844,3909.79377930236 2977.99846794872,3916.66185665368 2982.40585155751,3911.71430715877 2976.88041780766,3904.29221198068</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>64</ogr:id>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>10</ogr:ArabischeZ>
      <ogr:Zusammense>73/I-10-NO-Erweiterung</ogr:Zusammense>
      <ogr:Car>73.I.10.NO-Erweiterung</ogr:Car>
      <ogr:xmin>2971.951680000000124</ogr:xmin>
      <ogr:ymin>3904.292210000000068</ogr:ymin>
      <ogr:Zusatz1>NO-Erweiterung</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.62">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2933.64063724006,3916.96446571381 2928.83222103838,3922.35928458058 2934.57530514646,3927.23669964668 2939.19893859908,3922.15676311155 2933.64063724006,3916.96446571381</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>65</ogr:id>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>III</ogr:RomischeZa>
      <ogr:ArabischeZ>1</ogr:ArabischeZ>
      <ogr:Zusammense>73/III-1</ogr:Zusammense>
      <ogr:Car>73.III.1</ogr:Car>
      <ogr:xmin>2928.832219999999779</ogr:xmin>
      <ogr:ymin>3916.964460000000145</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.63">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2876.28821392042,3904.75604578632 2868.68031741988,3911.61255373055 2875.35823149355,3917.64294513004 2882.00096311403,3909.80452181788 2876.28821392042,3904.75604578632</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>66</ogr:id>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>II</ogr:RomischeZa>
      <ogr:ArabischeZ>6</ogr:ArabischeZ>
      <ogr:Zusammense>73/II-6-NW-Erweiterung</ogr:Zusammense>
      <ogr:Car>73.II.6.NW-Erweiterung</ogr:Car>
      <ogr:xmin>2868.680310000000190</ogr:xmin>
      <ogr:ymin>3904.756040000000212</ogr:ymin>
      <ogr:Zusatz1>NW-Erweiterung</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.64">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2853.12072212504,3889.09270364328 2850.16761570123,3892.60861153178 2857.38153813767,3905.15130703923 2862.03180500958,3900.54981020912 2861.05582926509,3898.91611168029 2868.09982811668,3896.60347350311 2861.75776557225,3890.43847911391 2858.00466339983,3891.22358254092 2853.12072212504,3889.09270364328</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>67</ogr:id>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>II</ogr:RomischeZa>
      <ogr:ArabischeZ>8</ogr:ArabischeZ>
      <ogr:Zusammense>73/II-8-Norderweiterung</ogr:Zusammense>
      <ogr:Car>73.II.8.Nord-Erweiterung</ogr:Car>
      <ogr:xmin>2850.167609999999968</ogr:xmin>
      <ogr:ymin>3889.092700000000150</ogr:ymin>
      <ogr:Zusatz1>Nord-Erweiterung</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.65">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2915.2146212164,3936.84043951321 2924.98895774623,3946.23195496736 2929.93139945125,3942.5070039596 2920.32952456417,3931.0612630034 2915.2146212164,3936.84043951321</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>68</ogr:id>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>II</ogr:RomischeZa>
      <ogr:ArabischeZ>1</ogr:ArabischeZ>
      <ogr:Zusammense>73/II-1</ogr:Zusammense>
      <ogr:Car>73.II.1</ogr:Car>
      <ogr:xmin>2915.214620000000195</ogr:xmin>
      <ogr:ymin>3931.061259999999947</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.66">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2923.66307653056,3884.75953161903 2916.60450032698,3895.2597870385 2921.14908382953,3899.31095937693 2928.23437519254,3889.079199162 2923.66307653056,3884.75953161903</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>69</ogr:id>
      <ogr:Jahr>75</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense>75/I-Westerweiterung2</ogr:Zusammense>
      <ogr:Car>75.I.Westerweiterung 2</ogr:Car>
      <ogr:xmin>2916.604499999999916</ogr:xmin>
      <ogr:ymin>3884.759529999999813</ogr:ymin>
      <ogr:Zusatz1>Westerweiterung 2</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.67">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2898.56590044521,3895.45621999421 2907.26866616732,3901.57939086435 2910.30158327032,3897.54065816376 2902.1481605336,3891.68128168894 2898.56590044521,3895.45621999421</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>70</ogr:id>
      <ogr:Jahr>75</ogr:Jahr>
      <ogr:RomischeZa>XII</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense>75/XII</ogr:Zusammense>
      <ogr:Car>75.XII</ogr:Car>
      <ogr:xmin>2898.565900000000056</ogr:xmin>
      <ogr:ymin>3891.681279999999788</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.68">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2893.11404552974,3901.24681522243 2901.33848886351,3908.75467979956 2906.27456029632,3902.37706134757 2897.95292948388,3896.4904373422 2893.11404552974,3901.24681522243</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>71</ogr:id>
      <ogr:Jahr>75</ogr:Jahr>
      <ogr:RomischeZa>XI</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense>75/XI</ogr:Zusammense>
      <ogr:Car>75.XI</ogr:Car>
      <ogr:xmin>2893.114039999999932</ogr:xmin>
      <ogr:ymin>3896.490429999999833</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.69">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2934.11698113921,3894.7194189043 2930.06995371898,3890.07541417142 2922.93728686798,3898.76832463314 2927.41613236806,3901.76900229499 2934.11698113921,3894.7194189043</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>72</ogr:id>
      <ogr:Jahr>75</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense>75/I</ogr:Zusammense>
      <ogr:Car>75.I</ogr:Car>
      <ogr:xmin>2922.937280000000101</ogr:xmin>
      <ogr:ymin>3890.075409999999920</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.70">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2899.95605909884,3924.56280321032 2894.17688258902,3936.58614744339 2896.84292287644,3938.22524632794 2901.1283401849,3932.11727701336 2903.53394921995,3933.9193800476 2905.65891645656,3929.55874630317 2899.95605909884,3924.56280321032</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>73</ogr:id>
      <ogr:Jahr>75</ogr:Jahr>
      <ogr:RomischeZa>VI</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense>75/VI</ogr:Zusammense>
      <ogr:Car>75.VI</ogr:Car>
      <ogr:xmin>2894.176879999999983</ogr:xmin>
      <ogr:ymin>3924.562800000000152</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.71">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2866.60954636402,3912.87159555827 2873.68405553983,3919.61396815306 2882.9838798085,3909.51701608993 2876.37436184613,3903.83748055442 2866.60954636402,3912.87159555827</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>74</ogr:id>
      <ogr:Jahr>75</ogr:Jahr>
      <ogr:RomischeZa>IX</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense>75/IX</ogr:Zusammense>
      <ogr:Car>75.IX</ogr:Car>
      <ogr:xmin>2866.609539999999924</ogr:xmin>
      <ogr:ymin>3903.837480000000141</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.72">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2928.88515530603,3901.31324253864 2922.50813295037,3908.95238390219 2931.47582063802,3916.72437989815 2937.58713372886,3909.68308438044 2928.88515530603,3901.31324253864</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>75</ogr:id>
      <ogr:Jahr>75</ogr:Jahr>
      <ogr:RomischeZa>III</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense>75/III</ogr:Zusammense>
      <ogr:Car>75.III</ogr:Car>
      <ogr:xmin>2922.508130000000165</ogr:xmin>
      <ogr:ymin>3901.313239999999951</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.73">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2930.84476113407,3900.15076450505 2938.61675713004,3908.12204244963 2943.13381463196,3903.33927568288 2935.18550873284,3895.82117955252 2930.84476113407,3900.15076450505</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>76</ogr:id>
      <ogr:Jahr>75</ogr:Jahr>
      <ogr:RomischeZa>II</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense>75/II</ogr:Zusammense>
      <ogr:Car>75.II</ogr:Car>
      <ogr:xmin>2930.844759999999951</ogr:xmin>
      <ogr:ymin>3895.821179999999913</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.74">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2887.97423707559,3931.38384333724 2893.77831869179,3935.45688306791 2899.74464162748,3921.01552235228 2900.29513408203,3921.34381825479 2891.63576578458,3916.17965095898 2881.13729612839,3928.74867304674 2884.6234616276,3929.84678890837 2888.16682415164,3922.41863053525 2891.19459176197,3923.27021042473 2887.97423707559,3931.38384333724</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>77</ogr:id>
      <ogr:Jahr>75</ogr:Jahr>
      <ogr:RomischeZa>VII</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense>75/VII</ogr:Zusammense>
      <ogr:Car>75.VII</ogr:Car>
      <ogr:xmin>2881.137290000000121</ogr:xmin>
      <ogr:ymin>3916.179650000000038</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.75">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2892.31691773528,3914.93084236062 2901.15175079052,3920.97572813526 2906.46593608691,3914.73156041201 2897.40720685355,3908.12078633814 2892.31691773528,3914.93084236062</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>78</ogr:id>
      <ogr:Jahr>75</ogr:Jahr>
      <ogr:RomischeZa>VIII</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense>75/VIII</ogr:Zusammense>
      <ogr:Car>75.VIII</ogr:Car>
      <ogr:xmin>2892.316910000000007</ogr:xmin>
      <ogr:ymin>3908.120780000000195</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.76">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2885.2700344601,3909.37519894084 2891.14080176926,3914.83026026774 2897.24880989339,3907.84505233569 2892.0495065276,3902.48944492339 2885.2700344601,3909.37519894084</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>79</ogr:id>
      <ogr:Jahr>75</ogr:Jahr>
      <ogr:RomischeZa>II</ogr:RomischeZa>
      <ogr:ArabischeZ>5</ogr:ArabischeZ>
      <ogr:Zusammense>75/II-5</ogr:Zusammense>
      <ogr:Car>75.II.5</ogr:Car>
      <ogr:xmin>2870.175040000000081</ogr:xmin>
      <ogr:ymin>3916.255310000000009</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.77">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2869.13378437981,3896.99546698532 2859.90038742734,3903.77105323822 2866.60954636402,3912.87159555827 2876.37436184613,3903.83748055442 2869.13378437981,3896.99546698532</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>80</ogr:id>
      <ogr:Jahr>75</ogr:Jahr>
      <ogr:RomischeZa>X</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense>75/X</ogr:Zusammense>
      <ogr:Car>75.X</ogr:Car>
      <ogr:xmin>2859.900380000000041</ogr:xmin>
      <ogr:ymin>3896.995460000000094</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.78">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2922.00992807883,3884.83926811984 2915.08810267207,3884.85496887937 2911.35324807909,3895.79521401068 2914.95893566587,3896.4067005557 2922.00992807883,3884.83926811984</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>81</ogr:id>
      <ogr:Jahr>75</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense>75/I-Westerweiterung3</ogr:Zusammense>
      <ogr:Car>75.I.Westerweiterung 3</ogr:Car>
      <ogr:xmin>2911.353239999999914</ogr:xmin>
      <ogr:ymin>3884.839260000000195</ogr:ymin>
      <ogr:Zusatz1>Westerweiterung 3</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.79">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2893.31332747836,3883.24501253093 2901.41790229248,3890.16790337601 2909.1878511423,3881.67233498776 2903.87527075492,3876.33657164563 2893.31332747836,3883.24501253093</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>82</ogr:id>
      <ogr:Jahr>75</ogr:Jahr>
      <ogr:RomischeZa>XIV</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense>75/XIV</ogr:Zusammense>
      <ogr:Car>75.XIV</ogr:Car>
      <ogr:xmin>2893.313320000000203</ogr:xmin>
      <ogr:ymin>3876.336569999999938</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.80">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2902.94528832805,3890.95058121069 2909.58801994853,3895.60049334502 2913.65491542129,3886.60448820629 2910.2314073083,3883.35803100974 2902.94528832805,3890.95058121069</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>83</ogr:id>
      <ogr:Jahr>75</ogr:Jahr>
      <ogr:RomischeZa>XIII</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense>75/XIII</ogr:Zusammense>
      <ogr:Car>75.XIII</ogr:Car>
      <ogr:xmin>2902.945279999999912</ogr:xmin>
      <ogr:ymin>3883.358029999999872</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.81">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2976.61785700417,3925.0612933473 2968.49460080136,3929.73209299552 2971.2457236894,3935.62170552378 2979.17551848383,3931.62522942502 2976.61785700417,3925.0612933473</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>84</ogr:id>
      <ogr:Jahr>76</ogr:Jahr>
      <ogr:RomischeZa>II</ogr:RomischeZa>
      <ogr:ArabischeZ>6</ogr:ArabischeZ>
      <ogr:Zusammense>76/II-6</ogr:Zusammense>
      <ogr:Car>76.II.6</ogr:Car>
      <ogr:xmin>2968.494599999999991</ogr:xmin>
      <ogr:ymin>3925.061290000000099</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.82">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2931.17194348316,3950.4795260888 2938.78282542054,3946.78274048083 2938.73867869326,3945.21528716527 2929.35014651946,3947.44701364288 2931.17194348316,3950.4795260888</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>85</ogr:id>
      <ogr:Jahr>76</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>1</ogr:ArabischeZ>
      <ogr:Zusammense>76/I-1</ogr:Zusammense>
      <ogr:Car>76.I.1</ogr:Car>
      <ogr:xmin>2929.350140000000010</ogr:xmin>
      <ogr:ymin>3945.215279999999893</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.83">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2906.69843169362,3934.39404600863 2913.47401794651,3926.65526367077 2908.19304630823,3923.66603444156 2903.31063856718,3931.2055348308 2906.69843169362,3934.39404600863</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>87</ogr:id>
      <ogr:Jahr>75</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>1</ogr:ArabischeZ>
      <ogr:Zusammense>75/I-1</ogr:Zusammense>
      <ogr:Car>75.I.1</ogr:Car>
      <ogr:xmin>2903.310629999999946</ogr:xmin>
      <ogr:ymin>3923.666029999999864</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.84">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2912.21189893862,3939.54216301451 2917.69511297758,3934.03780596487 2911.9794033319,3929.64449289999 2907.32949119757,3934.79260990586 2912.21189893862,3939.54216301451</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>88</ogr:id>
      <ogr:Jahr>75</ogr:Jahr>
      <ogr:RomischeZa>II</ogr:RomischeZa>
      <ogr:ArabischeZ>2</ogr:ArabischeZ>
      <ogr:Zusammense>75/II-2</ogr:Zusammense>
      <ogr:Car>75.II.2</ogr:Car>
      <ogr:xmin>2907.329490000000078</ogr:xmin>
      <ogr:ymin>3929.644490000000133</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.85">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2913.80615452754,3918.28542182897 2922.76841720468,3926.92688232204 2929.35014651946,3917.62114866692 2920.44888614802,3911.64269020848 2913.80615452754,3918.28542182897</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>89</ogr:id>
      <ogr:Jahr>75</ogr:Jahr>
      <ogr:RomischeZa>IV</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense>75/IV</ogr:Zusammense>
      <ogr:Car>75.IV</ogr:Car>
      <ogr:xmin>2913.806149999999889</ogr:xmin>
      <ogr:ymin>3911.642690000000130</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.86">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2912.76146308475,3917.6463828228 2923.43811537723,3904.53496737457 2914.02300523118,3897.61564481748 2903.90901056998,3910.40188055748 2912.76146308475,3917.6463828228</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>90</ogr:id>
      <ogr:Jahr>75</ogr:Jahr>
      <ogr:RomischeZa>V</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense>75/V</ogr:Zusammense>
      <ogr:Car>75.V</ogr:Car>
      <ogr:xmin>2903.909009999999853</ogr:xmin>
      <ogr:ymin>3897.615639999999985</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.87">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2976.62192155573,3963.89225426211 2982.25059520094,3961.28293535373 2976.38791573453,3950.44551114893 2970.7098904135,3952.93174708831 2976.62192155573,3963.89225426211</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>91</ogr:id>
      <ogr:Jahr>76</ogr:Jahr>
      <ogr:RomischeZa>IV</ogr:RomischeZa>
      <ogr:ArabischeZ>2</ogr:ArabischeZ>
      <ogr:Zusammense>76/IV-2</ogr:Zusammense>
      <ogr:Car>76.IV.2</ogr:Car>
      <ogr:xmin>2970.709890000000087</ogr:xmin>
      <ogr:ymin>3950.445510000000013</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.88">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2960.60901550408,3946.5097768875 2964.73828796046,3955.2965365728 2969.24554069247,3952.9266100914 2964.46277392573,3941.63396633659 2960.60901550408,3946.5097768875</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>92</ogr:id>
      <ogr:Jahr>76</ogr:Jahr>
      <ogr:RomischeZa>III</ogr:RomischeZa>
      <ogr:ArabischeZ>1</ogr:ArabischeZ>
      <ogr:Zusammense>76/III-1</ogr:Zusammense>
      <ogr:Car>76.III.1</ogr:Car>
      <ogr:xmin>2960.609010000000126</ogr:xmin>
      <ogr:ymin>3941.633960000000116</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.89">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2974.78454390916,3947.25326068054 2969.72528902591,3938.33364636283 2966.20561534882,3942.890514713 2970.45696358593,3950.79536534137 2974.78454390916,3947.25326068054</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>93</ogr:id>
      <ogr:Jahr>76</ogr:Jahr>
      <ogr:RomischeZa>III</ogr:RomischeZa>
      <ogr:ArabischeZ>2</ogr:ArabischeZ>
      <ogr:Zusammense>76/III-2</ogr:Zusammense>
      <ogr:Car>76.III.2</ogr:Car>
      <ogr:xmin>2966.205609999999979</ogr:xmin>
      <ogr:ymin>3938.333639999999832</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.90">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2960.46287326261,3932.28404203045 2957.24472637799,3935.57007423898 2964.19826551412,3941.75808141246 2966.84623424459,3938.54096672951 2960.46287326261,3932.28404203045</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>94</ogr:id>
      <ogr:Jahr>76</ogr:Jahr>
      <ogr:RomischeZa>II</ogr:RomischeZa>
      <ogr:ArabischeZ>4</ogr:ArabischeZ>
      <ogr:Zusammense>76/II-4</ogr:Zusammense>
      <ogr:Car>76.II.4</ogr:Car>
      <ogr:xmin>2957.244720000000143</ogr:xmin>
      <ogr:ymin>3932.284040000000005</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.91">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2965.11142262302,3928.81491532281 2961.05935633453,3932.8005542951 2967.34262177379,3939.0834459293 2970.85413159677,3935.35364336222 2965.11142262302,3928.81491532281</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>95</ogr:id>
      <ogr:Jahr>76</ogr:Jahr>
      <ogr:RomischeZa>II</ogr:RomischeZa>
      <ogr:ArabischeZ>5</ogr:ArabischeZ>
      <ogr:Zusammense>76/II-5</ogr:Zusammense>
      <ogr:Car>76.II.5</ogr:Car>
      <ogr:xmin>2961.059349999999995</ogr:xmin>
      <ogr:ymin>3928.814910000000054</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.92">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2967.92383036781,3929.49303818666 2975.85561586591,3924.5208553146 2971.90533243964,3917.23941606564 2963.97504852246,3922.76580824328 2967.92383036781,3929.49303818666</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>96</ogr:id>
      <ogr:Jahr>76</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>6</ogr:ArabischeZ>
      <ogr:Zusammense>76/I-6</ogr:Zusammense>
      <ogr:Car>76.I.6</ogr:Car>
      <ogr:xmin>2963.975039999999808</ogr:xmin>
      <ogr:ymin>3917.239410000000134</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.93">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2953.87723050325,3938.95972147287 2961.25066260199,3945.40317114474 2963.47218903034,3942.25140189135 2956.46789583524,3936.16977419227 2953.87723050325,3938.95972147287</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>97</ogr:id>
      <ogr:Jahr>76</ogr:Jahr>
      <ogr:RomischeZa>II</ogr:RomischeZa>
      <ogr:ArabischeZ>3</ogr:ArabischeZ>
      <ogr:Zusammense>76/II-3</ogr:Zusammense>
      <ogr:Car>76.II.3</ogr:Car>
      <ogr:xmin>2953.877230000000054</ogr:xmin>
      <ogr:ymin>3936.169769999999971</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.94">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2967.12161448523,3958.18540259753 2968.1844515445,3962.43675083464 2972.10366320059,3959.38109428922 2970.8415441927,3955.59473726554 2967.12161448523,3958.18540259753</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>98</ogr:id>
      <ogr:Jahr>76</ogr:Jahr>
      <ogr:RomischeZa>IV</ogr:RomischeZa>
      <ogr:ArabischeZ>1</ogr:ArabischeZ>
      <ogr:Zusammense>76/IV-1</ogr:Zusammense>
      <ogr:Car>76.IV.1</ogr:Car>
      <ogr:xmin>2967.121610000000146</ogr:xmin>
      <ogr:ymin>3955.594729999999799</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.95">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2943.79426173058,3940.48689595047 2948.16161735055,3943.70922817115 2948.34615669746,3943.60778870684 2951.09411514833,3940.46424662631 2947.19559588941,3936.68352999394 2943.79426173058,3940.48689595047</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>99</ogr:id>
      <ogr:Jahr>76</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>2</ogr:ArabischeZ>
      <ogr:Zusammense>76/I-2</ogr:Zusammense>
      <ogr:Car>76.I.2</ogr:Car>
      <ogr:xmin>2943.794260000000122</ogr:xmin>
      <ogr:ymin>3936.683529999999791</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.96">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2956.66673746277,3926.41698061529 2961.03409308274,3929.63931283597 2961.21863242965,3929.53787337166 2963.96659088052,3926.39433129112 2960.06807162159,3922.61361465875 2956.66673746277,3926.41698061529</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>100</ogr:id>
      <ogr:Jahr>76</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>5</ogr:ArabischeZ>
      <ogr:Zusammense>76/I-5</ogr:Zusammense>
      <ogr:Car>76.I.5</ogr:Car>
      <ogr:xmin>2956.666729999999916</ogr:xmin>
      <ogr:ymin>3922.613609999999881</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.97">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2952.10149897635,3931.20673902727 2956.46885459632,3934.42907124794 2956.65339394324,3934.32763178364 2959.4013523941,3931.1840897031 2955.50283313518,3927.40337307073 2952.10149897635,3931.20673902727</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>101</ogr:id>
      <ogr:Jahr>76</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>4</ogr:ArabischeZ>
      <ogr:Zusammense>76/I-4</ogr:Zusammense>
      <ogr:Car>76.I.4</ogr:Car>
      <ogr:xmin>2952.101490000000013</ogr:xmin>
      <ogr:ymin>3927.403369999999995</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.98">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2947.46142051475,3935.6971375385 2951.82877613472,3938.91946975917 2952.01331548163,3938.81803029486 2954.7612739325,3935.67448821433 2950.86275467357,3931.89377158196 2947.46142051475,3935.6971375385</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>102</ogr:id>
      <ogr:Jahr>76</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>3</ogr:ArabischeZ>
      <ogr:Zusammense>76/I-3</ogr:Zusammense>
      <ogr:Car>76.I.3</ogr:Car>
      <ogr:xmin>2947.461420000000089</ogr:xmin>
      <ogr:ymin>3931.893770000000131</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.99">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2976.5915020024,3946.53838772376 2981.2881830604,3944.30319678578 2976.81602192796,3933.5162320412 2972.40046339192,3936.28531112312 2976.5915020024,3946.53838772376</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>103</ogr:id>
      <ogr:Jahr>76</ogr:Jahr>
      <ogr:RomischeZa>III</ogr:RomischeZa>
      <ogr:ArabischeZ>3</ogr:ArabischeZ>
      <ogr:Zusammense>76/III-3</ogr:Zusammense>
      <ogr:Car>76.III.3</ogr:Car>
      <ogr:xmin>2972.400459999999839</ogr:xmin>
      <ogr:ymin>3933.516230000000178</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.100">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3000.31769644417,3928.40607728556 2994.85437825551,3931.39967629305 2998.29701711412,3937.46171428321 3003.46097540203,3935.89007480427 3000.31769644417,3928.40607728556</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>105</ogr:id>
      <ogr:Jahr>77</ogr:Jahr>
      <ogr:RomischeZa>V</ogr:RomischeZa>
      <ogr:ArabischeZ>2</ogr:ArabischeZ>
      <ogr:Zusammense>77/V-2</ogr:Zusammense>
      <ogr:Car>77.V.2</ogr:Car>
      <ogr:xmin>2994.854370000000017</ogr:xmin>
      <ogr:ymin>3928.406070000000000</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.101">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3006.15521450877,3925.78667815401 3000.31769644417,3928.40607728556 3003.46097540203,3935.89007480427 3009.14881351626,3933.4203556231 3006.15521450877,3925.78667815401</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>106</ogr:id>
      <ogr:Jahr>77</ogr:Jahr>
      <ogr:RomischeZa>V</ogr:RomischeZa>
      <ogr:ArabischeZ>3</ogr:ArabischeZ>
      <ogr:Zusammense>77/V-3</ogr:Zusammense>
      <ogr:Car>77.V.3</ogr:Car>
      <ogr:xmin>3000.317689999999857</ogr:xmin>
      <ogr:ymin>3925.786669999999958</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.102">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2988.04201820604,3890.44582632099 2992.45757674208,3899.80082321939 2997.99573490593,3896.8072242119 2992.68209666764,3888.12578709019 2988.04201820604,3890.44582632099</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>108</ogr:id>
      <ogr:Jahr>77</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>3</ogr:ArabischeZ>
      <ogr:Zusammense>77/I-3</ogr:Zusammense>
      <ogr:Car>77.I.3</ogr:Car>
      <ogr:xmin>2988.042010000000118</ogr:xmin>
      <ogr:ymin>3888.125779999999850</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.103">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3009.14881351626,3933.4203556231 3016.18377118385,3929.82803681412 3013.93625849602,3921.35023910433 3006.41714509975,3925.84030824325 3009.14881351626,3933.4203556231</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>109</ogr:id>
      <ogr:Jahr>77</ogr:Jahr>
      <ogr:RomischeZa>V</ogr:RomischeZa>
      <ogr:ArabischeZ>4</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>77.V.4</ogr:Car>
      <ogr:xmin>3006.417140000000018</ogr:xmin>
      <ogr:ymin>3921.350230000000010</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.104">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3002.63773567497,3915.53360155337 2996.44931439394,3919.15453345494 3000.31769644417,3928.40607728556 3006.15521450877,3925.78667815401 3002.63773567497,3915.53360155337</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>110</ogr:id>
      <ogr:Jahr>77</ogr:Jahr>
      <ogr:RomischeZa>IV</ogr:RomischeZa>
      <ogr:ArabischeZ>3</ogr:ArabischeZ>
      <ogr:Zusammense>77/IV-3</ogr:Zusammense>
      <ogr:Car>77.IV.3</ogr:Car>
      <ogr:xmin>2996.449309999999969</ogr:xmin>
      <ogr:ymin>3915.533600000000206</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.105">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2990.36397974428,3921.7453194939 2994.85437825551,3931.39967629305 3000.31769644417,3928.40607728556 2996.27806438266,3919.22744187558 2990.36397974428,3921.7453194939</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>111</ogr:id>
      <ogr:Jahr>77</ogr:Jahr>
      <ogr:RomischeZa>IV</ogr:RomischeZa>
      <ogr:ArabischeZ>2</ogr:ArabischeZ>
      <ogr:Zusammense>77/IV-2</ogr:Zusammense>
      <ogr:Car>77.IV.2</ogr:Car>
      <ogr:xmin>2990.363980000000083</ogr:xmin>
      <ogr:ymin>3919.227440000000115</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.106">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2983.70322195263,3924.81375847658 2988.26846043904,3934.46811527572 2994.85437825551,3931.39967629305 2990.36397974428,3921.7453194939 2983.70322195263,3924.81375847658</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>112</ogr:id>
      <ogr:Jahr>77</ogr:Jahr>
      <ogr:RomischeZa>IV</ogr:RomischeZa>
      <ogr:ArabischeZ>1</ogr:ArabischeZ>
      <ogr:Zusammense>77/IV-1</ogr:Zusammense>
      <ogr:Car>77.IV.1</ogr:Car>
      <ogr:xmin>2983.703219999999874</ogr:xmin>
      <ogr:ymin>3921.745309999999790</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.107">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2995.67569567513,3907.50934066366 3002.33645346678,3916.49013768612 3007.05137190357,3913.49653867863 3001.8088313742,3904.65250538568 2995.67569567513,3907.50934066366</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>113</ogr:id>
      <ogr:Jahr>77</ogr:Jahr>
      <ogr:RomischeZa>III</ogr:RomischeZa>
      <ogr:ArabischeZ>3</ogr:ArabischeZ>
      <ogr:Zusammense>77/III-3</ogr:Zusammense>
      <ogr:Car>77.III.3</ogr:Car>
      <ogr:xmin>2995.675690000000031</ogr:xmin>
      <ogr:ymin>3904.652500000000146</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.108">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2990.43689741203,3910.42809969596 2994.77761597288,3919.70825661917 3002.33645346678,3916.49013768612 2995.67569567513,3907.50934066366 2990.43689741203,3910.42809969596</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>114</ogr:id>
      <ogr:Jahr>77</ogr:Jahr>
      <ogr:RomischeZa>III</ogr:RomischeZa>
      <ogr:ArabischeZ>2</ogr:ArabischeZ>
      <ogr:Zusammense>77/III-2</ogr:Zusammense>
      <ogr:Car>77.III.2</ogr:Car>
      <ogr:xmin>2990.436889999999948</ogr:xmin>
      <ogr:ymin>3907.509340000000066</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.109">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2991.78401696539,3901.07310279757 2995.67569567513,3907.50934066366 3001.8088313742,3904.65250538568 2998.5075997693,3897.40163079681 2991.78401696539,3901.07310279757</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>116</ogr:id>
      <ogr:Jahr>77</ogr:Jahr>
      <ogr:RomischeZa>II</ogr:RomischeZa>
      <ogr:ArabischeZ>3</ogr:ArabischeZ>
      <ogr:Zusammense>77/II-3</ogr:Zusammense>
      <ogr:Car>77.II.3</ogr:Car>
      <ogr:xmin>2991.784009999999853</ogr:xmin>
      <ogr:ymin>3897.401629999999841</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.110">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2986.17101882636,3904.14154178024 2990.43689741203,3910.42809969596 2995.67569567513,3907.50934066366 2991.78401696539,3901.07310279757 2986.17101882636,3904.14154178024</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>117</ogr:id>
      <ogr:Jahr>77</ogr:Jahr>
      <ogr:RomischeZa>II</ogr:RomischeZa>
      <ogr:ArabischeZ>2</ogr:ArabischeZ>
      <ogr:Zusammense>77/II-2</ogr:Zusammense>
      <ogr:Car>77.II.2</ogr:Car>
      <ogr:xmin>2986.171010000000024</ogr:xmin>
      <ogr:ymin>3901.073100000000068</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.111">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2980.63286066251,3907.20998076291 2984.44969939705,3914.09525848013 2990.43689741203,3910.42809969596 2986.17101882636,3904.14154178024 2980.63286066251,3907.20998076291</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>118</ogr:id>
      <ogr:Jahr>77</ogr:Jahr>
      <ogr:RomischeZa>II</ogr:RomischeZa>
      <ogr:ArabischeZ>1</ogr:ArabischeZ>
      <ogr:Zusammense>77/II-1</ogr:Zusammense>
      <ogr:Car>77.II.1</ogr:Car>
      <ogr:xmin>2980.632860000000164</ogr:xmin>
      <ogr:ymin>3904.141540000000077</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.112">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2981.8303002655,3894.26266505554 2986.83173308855,3903.78035131691 2992.15821684133,3900.92342284719 2986.98353168737,3891.05460105883 2981.8303002655,3894.26266505554</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>119</ogr:id>
      <ogr:Jahr>77</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>2</ogr:ArabischeZ>
      <ogr:Zusammense>77/I-2</ogr:Zusammense>
      <ogr:Car>77.I.2</ogr:Car>
      <ogr:xmin>2981.830300000000079</ogr:xmin>
      <ogr:ymin>3891.054599999999937</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.113">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2975.45267027919,3898.19441872455 2980.63286066251,3907.20998076291 2986.17101882636,3904.14154178024 2980.94816392768,3894.58847542193 2975.45267027919,3898.19441872455</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>120</ogr:id>
      <ogr:Jahr>77</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>1</ogr:ArabischeZ>
      <ogr:Zusammense>77/I-1</ogr:Zusammense>
      <ogr:Car>77.I.1</ogr:Car>
      <ogr:xmin>2975.452670000000126</ogr:xmin>
      <ogr:ymin>3894.588470000000143</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.114">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3009.05656710083,3900.8847125152 3001.8088313742,3904.65250538568 3005.77909232539,3912.29909907564 3012.7392100178,3909.67969994409 3009.05656710083,3900.8847125152</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>121</ogr:id>
      <ogr:Jahr>77</ogr:Jahr>
      <ogr:RomischeZa>III</ogr:RomischeZa>
      <ogr:ArabischeZ>4</ogr:ArabischeZ>
      <ogr:Zusammense>77/III-4</ogr:Zusammense>
      <ogr:Car>77.III.4</ogr:Car>
      <ogr:xmin>3001.808829999999944</ogr:xmin>
      <ogr:ymin>3900.884709999999814</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.115">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2984.44969939705,3914.09525848013 2988.11685818122,3922.77669560184 2994.77761597288,3919.70825661917 2990.43689741203,3910.42809969596 2984.44969939705,3914.09525848013</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>122</ogr:id>
      <ogr:Jahr>77</ogr:Jahr>
      <ogr:RomischeZa>III</ogr:RomischeZa>
      <ogr:ArabischeZ>1</ogr:ArabischeZ>
      <ogr:Zusammense>77/III-1</ogr:Zusammense>
      <ogr:Car>77.III.1</ogr:Car>
      <ogr:xmin>2984.449689999999919</ogr:xmin>
      <ogr:ymin>3910.428100000000086</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.116">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3006.41714509975,3925.84030824325 3013.93625849602,3921.35023910433 3011.48144282958,3911.03074991065 3002.21097006523,3914.81365876233 3006.41714509975,3925.84030824325</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>123</ogr:id>
      <ogr:Jahr>77</ogr:Jahr>
      <ogr:RomischeZa>IV</ogr:RomischeZa>
      <ogr:ArabischeZ>4</ogr:ArabischeZ>
      <ogr:Zusammense>77/IV-4</ogr:Zusammense>
      <ogr:Car>77.IV.4</ogr:Car>
      <ogr:xmin>3002.210970000000088</ogr:xmin>
      <ogr:ymin>3911.030749999999898</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.117">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2927.62091098183,3929.96877365683 2928.73224122065,3933.51976431741 2932.03663931492,3932.74437432334 2930.73294809277,3929.46411899019 2927.62091098183,3929.96877365683</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>124</ogr:id>
      <ogr:Jahr>76</ogr:Jahr>
      <ogr:RomischeZa>H13</ogr:RomischeZa>
      <ogr:ArabischeZ>R3</ogr:ArabischeZ>
      <ogr:Zusammense>76/H13-R3</ogr:Zusammense>
      <ogr:Car>76.H13.R3</ogr:Car>
      <ogr:xmin>2927.620910000000094</ogr:xmin>
      <ogr:ymin>3929.464109999999891</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.118">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2903.31063856718,3931.2055348308 2906.69843169362,3934.39404600863 2913.47401794651,3926.65526367077 2908.19304630823,3923.66603444156 2903.31063856718,3931.2055348308</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>126</ogr:id>
      <ogr:Jahr>76</ogr:Jahr>
      <ogr:RomischeZa>H19</ogr:RomischeZa>
      <ogr:ArabischeZ>R1</ogr:ArabischeZ>
      <ogr:Zusammense>76/H19-R1</ogr:Zusammense>
      <ogr:Car>76.H19.R1</ogr:Car>
      <ogr:xmin>2903.310629999999946</ogr:xmin>
      <ogr:ymin>3923.666029999999864</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.119">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2995.05772593453,3951.06020867624 2991.70244596214,3952.63916395737 2993.39329768423,3956.41051241233 2996.72299083585,3954.76093965832 2995.05772593453,3951.06020867624</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>127</ogr:id>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>Schnitt.C</ogr:RomischeZa>
      <ogr:ArabischeZ>0-5m</ogr:ArabischeZ>
      <ogr:Zusammense>1956 / Schnitt C</ogr:Zusammense>
      <ogr:Car>56.Schnitt.C.0-5m</ogr:Car>
      <ogr:xmin>2991.702440000000024</ogr:xmin>
      <ogr:ymin>3951.060199999999895</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.120">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3012.42530540852,4003.96045448236 3015.37223379946,4005.9041732083 3021.12309808214,3996.52582578358 3017.4502385417,3995.75819925375 3012.42530540852,4003.96045448236</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>131</ogr:id>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>Schnitt.B</ogr:RomischeZa>
      <ogr:ArabischeZ>0-10m</ogr:ArabischeZ>
      <ogr:Zusammense>56 - Schnitt B</ogr:Zusammense>
      <ogr:Car>56.Schnitt.B.0-10m</ogr:Car>
      <ogr:xmin>3012.425299999999879</ogr:xmin>
      <ogr:ymin>3995.758190000000013</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.121">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3027.70038157382,4000.63124105203 3058.37535896616,3998.6486172409 3088.22609949321,3995.70809653227 3085.10736540829,3935.96206213411 3058.21942226192,3942.33319033615 3053.95463319797,3959.72276280739 3048.42631736826,3985.11497790184 3045.77473657563,3984.69190292588 3047.9482976586,3974.76414093396 3027.1211881009,3976.12690181341 3027.1211881009,3976.12690181341 3027.47761485347,3996.22045998908 3027.70038157382,4000.63124105203</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>133</ogr:id>
      <ogr:Jahr>38</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense>38</ogr:Zusammense>
      <ogr:Car>38.K.20</ogr:Car>
      <ogr:xmin>3027.121180000000095</ogr:xmin>
      <ogr:ymin>3935.962059999999838</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.122">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2987.91402574137,3998.20793409518 2988.13765016001,4003.54696709021 2993.1875205992,4003.25143836032 2992.92649652701,3997.93576373578 2987.91402574137,3998.20793409518</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>136</ogr:id>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>21</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>56.J.21.h.10.d</ogr:Car>
      <ogr:xmin>2987.914020000000164</ogr:xmin>
      <ogr:ymin>3997.935759999999846</ogr:ymin>
      <ogr:Zusatz1>h</ogr:Zusatz1>
      <ogr:Zusatz2>10</ogr:Zusatz2>
      <ogr:Zusatz3>d</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.123">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2979.02538547596,4003.99993308985 2983.34233416806,4003.80806870353 2983.0802074875,3998.47040386462 2978.76181092431,3998.78115696718 2979.02538547596,4003.99993308985</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>137</ogr:id>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>21</ogr:ArabischeZ>
      <ogr:Zusammense>J21g10</ogr:Zusammense>
      <ogr:Car>56.J.21.g.10.d</ogr:Car>
      <ogr:xmin>2978.761809999999969</ogr:xmin>
      <ogr:ymin>3998.470400000000154</ogr:ymin>
      <ogr:Zusatz1>g</ogr:Zusatz1>
      <ogr:Zusatz2>10</ogr:Zusatz2>
      <ogr:Zusatz3>d</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.124">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3051.61470193552,3959.0590010768 3047.9482976586,3974.76414093396 3045.77473657563,3984.69190292588 3048.39603487533,3985.18964211641 3053.99265190242,3959.54857901116 3051.61470193552,3959.0590010768</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>138</ogr:id>
      <ogr:Jahr>37</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense>37</ogr:Zusammense>
      <ogr:Car>37.K.20.Schnitt1</ogr:Car>
      <ogr:xmin>3045.774730000000091</ogr:xmin>
      <ogr:ymin>3959.059000000000196</ogr:ymin>
      <ogr:Zusatz1>Schnitt1</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.125">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2940.95164202021,4013.34092688958 2941.26929723812,4011.3158748754 2937.41772772096,4011.19675416868 2937.21919320976,4013.30121998734 2940.95164202021,4013.34092688958</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>38</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>21</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>38.J.21.Grab am Tor</ogr:Car>
      <ogr:xmin>2937.219189999999799</ogr:xmin>
      <ogr:ymin>4011.196750000000065</ogr:ymin>
      <ogr:Zusatz1>Grab am Tor</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.126">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3059.19879688745,3969.70304132911 3061.06502129268,3967.79711002164 3060.23117634566,3967.08238578134 3058.04729672253,3969.06773089328 3059.19879688745,3969.70304132911</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>38</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>38.K.20.R34</ogr:Car>
      <ogr:xmin>3058.047289999999975</ogr:xmin>
      <ogr:ymin>3967.082379999999830</ogr:ymin>
      <ogr:Zusatz1>R34</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.127">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3063.96371303265,3976.31501681062 3065.67102195238,3980.62243944478 3069.80053978522,3983.32250879702 3069.80053978522,3979.86800830225 3063.96371303265,3976.31501681062</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>38</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>38.K.20.R33</ogr:Car>
      <ogr:xmin>3063.963709999999992</ogr:xmin>
      <ogr:ymin>3976.315009999999802</ogr:ymin>
      <ogr:Zusatz1>R33</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.128">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3053.48100296506,3994.36102761942 3055.90312400163,3995.59194158882 3057.37227938447,3992.21685489852 3055.14869285909,3991.26388924479 3053.48100296506,3994.36102761942</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>38</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>38.K.20.R3</ogr:Car>
      <ogr:xmin>3053.481000000000222</ogr:xmin>
      <ogr:ymin>3991.263879999999972</ogr:ymin>
      <ogr:Zusatz1>R3</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.129">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3044.46753615685,3984.35488825523 3046.65141577998,3981.21804297837 3045.02343278819,3979.86800830225 3042.60131175162,3982.92543977464 3044.46753615685,3984.35488825523</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>38</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>38.K.20.R20</ogr:Car>
      <ogr:xmin>3042.601310000000012</ogr:xmin>
      <ogr:ymin>3979.867999999999938</ogr:ymin>
      <ogr:Zusatz1>R20</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.130">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3047.44555382476,3981.77393960971 3045.18226039715,3984.8313710821 3046.96907099789,3986.18140575822 3048.7161746964,3982.72690526344 3047.44555382476,3981.77393960971</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>38</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>38.K.20.R19</ogr:Car>
      <ogr:xmin>3045.182260000000042</ogr:xmin>
      <ogr:ymin>3981.773940000000039</ogr:ymin>
      <ogr:Zusatz1>R19</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.131">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3041.52922539117,3988.12704396792 3043.63369120983,3985.22844010449 3041.72775990237,3983.95781923285 3039.54388027923,3986.65788858509 3041.52922539117,3988.12704396792</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>38</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>38.K.20.R18</ogr:Car>
      <ogr:xmin>3039.543880000000172</ogr:xmin>
      <ogr:ymin>3983.957809999999881</ogr:ymin>
      <ogr:Zusatz1>R18</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.132">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3044.78519137476,3990.1520959821 3046.65141577998,3987.05495760747 3044.42782925461,3985.98287124703 3042.28365653371,3988.60352679479 3044.78519137476,3990.1520959821</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>38</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>38.K.20.R17</ogr:Car>
      <ogr:xmin>3042.283649999999852</ogr:xmin>
      <ogr:ymin>3985.982869999999821</ogr:ymin>
      <ogr:Zusatz1>R17</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.133">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3044.26900164565,3991.18447544031 3040.93362185759,3989.00059581718 3037.41870053167,3992.84760101902 3041.29098397774,3995.90959680673 3044.26900164565,3991.18447544031</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>38</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>38.K.20.R16</ogr:Car>
      <ogr:xmin>3037.418700000000172</ogr:xmin>
      <ogr:ymin>3989.000590000000102</ogr:ymin>
      <ogr:Zusatz1>R16</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.134">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3051.3765371464,3994.28161381494 3045.57932941953,3991.62125136494 3042.16453582699,3996.50520034032 3046.41317436655,3999.20526969256 3049.07353681655,3999.2449765948 3051.3765371464,3994.28161381494</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>38</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>38.K.20.R15</ogr:Car>
      <ogr:xmin>3042.164530000000013</ogr:xmin>
      <ogr:ymin>3991.621250000000146</ogr:ymin>
      <ogr:Zusatz1>R15</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.135">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3052.21038209342,3988.52411299031 3050.06620937252,3992.69333772539 3051.77360616879,3993.40806196569 3053.91777888969,3989.35795793733 3052.21038209342,3988.52411299031</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>38</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>38.K.20.R13</ogr:Car>
      <ogr:xmin>3050.066200000000208</ogr:xmin>
      <ogr:ymin>3988.524109999999837</ogr:ymin>
      <ogr:Zusatz1>R13</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.136">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3033.46872423669,3987.84909565225 3036.24820739341,3984.67254347314 3030.68924107997,3979.43123237762 3028.06858553221,3983.20338809031 3033.46872423669,3987.84909565225</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>38</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>38.K.20.Keramikzimmer</ogr:Car>
      <ogr:xmin>3028.068580000000111</ogr:xmin>
      <ogr:ymin>3979.431230000000141</ogr:ymin>
      <ogr:Zusatz1>Keramikzimmer</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.137">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3078.01986854866,3973.87226606418 3084.41267980911,3978.39885291941 3087.03333535687,3975.69878356717 3081.83173116359,3970.10011035149 3078.01986854866,3973.87226606418</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>38</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>38.K.20.R40</ogr:Car>
      <ogr:xmin>3078.019859999999881</ogr:xmin>
      <ogr:ymin>3970.100109999999859</ogr:ymin>
      <ogr:Zusatz1>R40</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.138">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3068.33138440239,3976.1355594918 3075.31979919642,3981.33716368508 3080.64052409642,3976.09585258956 3072.14324701731,3969.86186893806 3068.33138440239,3976.1355594918</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>38</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>38.K.20.R39</ogr:Car>
      <ogr:xmin>3068.331380000000081</ogr:xmin>
      <ogr:ymin>3969.861859999999979</ogr:ymin>
      <ogr:Zusatz1>R39</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.139">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3062.05769384865,3966.56619605224 3067.0210566285,3960.25279859626 3066.02838407253,3959.41895364925 3061.18414199939,3965.53381659403 3062.05769384865,3966.56619605224</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>38</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>38.K.20.R37</ogr:Car>
      <ogr:xmin>3061.184139999999843</ogr:xmin>
      <ogr:ymin>3959.418950000000223</ogr:ymin>
      <ogr:Zusatz1>R37</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.140">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3060.39000395462,3970.97366220075 3062.85183189343,3972.87959350821 3067.93431538,3966.72502366119 3065.27395292999,3965.01762686492 3060.39000395462,3970.97366220075</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>38</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>38.K.20.R35</ogr:Car>
      <ogr:xmin>3060.389999999999873</ogr:xmin>
      <ogr:ymin>3965.017620000000079</ogr:ymin>
      <ogr:Zusatz1>R35</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.141">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3062.69300428447,3984.59312966867 3066.42545309492,3986.30052646494 3067.97402228223,3983.71957781941 3064.47981488522,3981.53569819628 3062.69300428447,3984.59312966867</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>38</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>38.K.20.R8</ogr:Car>
      <ogr:xmin>3062.693000000000211</ogr:xmin>
      <ogr:ymin>3981.535690000000159</ogr:ymin>
      <ogr:Zusatz1>R8</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.142">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3061.77974553298,3984.19606064628 3063.88421135163,3980.78126705374 3061.81945243522,3979.35181857314 3059.79440042104,3983.32250879702 3061.77974553298,3984.19606064628</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>38</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>38.K.20.R9</ogr:Car>
      <ogr:xmin>3059.794400000000223</ogr:xmin>
      <ogr:ymin>3979.351810000000114</ogr:ymin>
      <ogr:Zusatz1>R9</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.143">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3056.73696894864,3986.30052646494 3059.47674520312,3987.57114733658 3060.74736607477,3984.79166417986 3058.44436574492,3983.67987091717 3056.73696894864,3986.30052646494</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>38</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>38.K.20.R7</ogr:Car>
      <ogr:xmin>3056.736960000000181</ogr:xmin>
      <ogr:ymin>3983.679869999999937</ogr:ymin>
      <ogr:Zusatz1>R7</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.144">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2957.08055605324,3997.51086008285 2940.35755136474,4006.60359647241 2942.48850704541,4010.30265809092 2941.3113349021,4014.36103590966 2952.69315178135,4015.69633286198 2958.0343395906,4003.42431801452 2957.08055605324,3997.51086008285</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>38</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>21</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>38.J.21.Tor</ogr:Car>
      <ogr:xmin>2940.357550000000174</ogr:xmin>
      <ogr:ymin>3997.510859999999866</ogr:ymin>
      <ogr:Zusatz1>Tor</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.145">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3022.41472836286,3986.25621434192 3032.23869879774,3985.71573700408 3032.07973487484,3975.82818100005 3022.18596692782,3976.32734224735 3022.41472836286,3986.25621434192</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>53</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>53.K.20.b.2</ogr:Car>
      <ogr:xmin>3022.185959999999795</ogr:xmin>
      <ogr:ymin>3975.828179999999975</ogr:ymin>
      <ogr:Zusatz1>b</ogr:Zusatz1>
      <ogr:Zusatz2>2</ogr:Zusatz2>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.146">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3032.68379778184,3995.69867136186 3032.42945550521,3985.65215143492 3022.42618538884,3986.32375584739 3022.63727785491,3996.20735591512 3032.68379778184,3995.69867136186</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>53</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>53.K.20.b.1</ogr:Car>
      <ogr:xmin>3022.426179999999931</ogr:xmin>
      <ogr:ymin>3985.652149999999892</ogr:ymin>
      <ogr:Zusatz1>b</ogr:Zusatz1>
      <ogr:Zusatz2>1</ogr:Zusatz2>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.147">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3022.76444899323,3996.20735591512 3022.31079356317,3986.26573764789 3012.59075792798,3986.70131332603 3012.7179290663,3996.71604046838 3022.76444899323,3996.20735591512</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>53</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>53.K.20.a.2</ogr:Car>
      <ogr:xmin>3012.590749999999844</ogr:xmin>
      <ogr:ymin>3986.265730000000076</ogr:ymin>
      <ogr:Zusatz1>a</ogr:Zusatz1>
      <ogr:Zusatz2>2</ogr:Zusatz2>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.148">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3012.49537957425,3986.76489889519 3022.31935000912,3986.22442155734 3022.16038608623,3976.33686555331 3012.17745172846,3976.94092846031 3012.49537957425,3986.76489889519</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>53</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>53.K.20.a.1</ogr:Car>
      <ogr:xmin>3012.177450000000135</ogr:xmin>
      <ogr:ymin>3976.336859999999888</ogr:ymin>
      <ogr:Zusatz1>a</ogr:Zusatz1>
      <ogr:Zusatz2>1</ogr:Zusatz2>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.149">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3008.00780349478,3997.15608021455 3017.58222510687,3996.6842476838 3017.2325044765,3976.68658618368 3007.53570517994,3977.16347795236 3008.00780349478,3997.15608021455</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>53.J.20.k.1</ogr:Car>
      <ogr:xmin>3007.535699999999906</ogr:xmin>
      <ogr:ymin>3976.686580000000049</ogr:ymin>
      <ogr:Zusatz1>k</ogr:Zusatz1>
      <ogr:Zusatz2>1</ogr:Zusatz2>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.150">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3012.36820843593,3986.92386281808 3012.1138661593,3977.00451402947 3002.57603078563,3977.41782022899 3002.73499470853,3987.52792572508 3012.36820843593,3986.92386281808</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>53</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>53.J.20.k.2</ogr:Car>
      <ogr:xmin>3002.576030000000173</ogr:xmin>
      <ogr:ymin>3977.004510000000209</ogr:ymin>
      <ogr:Zusatz1>k</ogr:Zusatz1>
      <ogr:Zusatz2>2</ogr:Zusatz2>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.151">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3007.64000047573,3997.18437275448 3007.85890327088,3992.20406705046 3002.95054371127,3992.49341177361 3002.99854450992,3997.43171454181 3007.64000047573,3997.18437275448</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>56.J.20.k.1.a</ogr:Car>
      <ogr:xmin>3002.950539999999819</ogr:xmin>
      <ogr:ymin>3992.204060000000027</ogr:ymin>
      <ogr:Zusatz1>k</ogr:Zusatz1>
      <ogr:Zusatz2>1</ogr:Zusatz2>
      <ogr:Zusatz3>a</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.152">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2820.40337514857,4005.1252319895 2839.20880722698,4001.61212929353 2838.66832988914,3998.17850855901 2819.76751945699,4001.42137258606 2820.40337514857,4005.1252319895</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>55</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>55.J.20</ogr:Car>
      <ogr:xmin>2819.767510000000129</ogr:xmin>
      <ogr:ymin>3998.178499999999985</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.153">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2973.9388869654,3999.01713535302 2973.76656556129,3993.92859234083 2969.13630137773,3994.01277896235 2969.38886124229,3999.23234949654 2973.9388869654,3999.01713535302</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>56.J.20.f.1.b</ogr:Car>
      <ogr:xmin>2969.136300000000119</ogr:xmin>
      <ogr:ymin>3993.928589999999986</ogr:ymin>
      <ogr:Zusatz1>f</ogr:Zusatz1>
      <ogr:Zusatz2>1</ogr:Zusatz2>
      <ogr:Zusatz3>b</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.154">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2978.53714078072,3998.7552919746 2978.17233208747,3993.70409468344 2973.73850335412,3993.92859234083 2973.9834103666,3998.99820011342 2978.53714078072,3998.7552919746</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>56.J.20.g.1.a</ogr:Car>
      <ogr:xmin>2973.738499999999931</ogr:xmin>
      <ogr:ymin>3993.704090000000178</ogr:ymin>
      <ogr:Zusatz1>g</ogr:Zusatz1>
      <ogr:Zusatz2>1</ogr:Zusatz2>
      <ogr:Zusatz3>a</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.155">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2983.11128054993,3998.50273211004 2982.74624152393,3993.53802476794 2978.17233208747,3993.78828130496 2978.53714078072,3998.81141638895 2983.11128054993,3998.50273211004</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>56.J.20.g.1.b</ogr:Car>
      <ogr:xmin>2978.172329999999874</ogr:xmin>
      <ogr:ymin>3993.538019999999960</ogr:ymin>
      <ogr:Zusatz1>g</ogr:Zusatz1>
      <ogr:Zusatz2>1</ogr:Zusatz2>
      <ogr:Zusatz3>b</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.156">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2987.96604239088,3998.22211003831 2987.71348252632,3993.25509936867 2982.85872068537,3993.47959702606 2983.16740496428,3998.50273211004 2987.96604239088,3998.22211003831</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>56.J.20.h.1.a</ogr:Car>
      <ogr:xmin>2982.858720000000176</ogr:xmin>
      <ogr:ymin>3993.255090000000109</ogr:ymin>
      <ogr:Zusatz1>h</ogr:Zusatz1>
      <ogr:Zusatz2>1</ogr:Zusatz2>
      <ogr:Zusatz3>a</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.157">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2992.93305306051,3997.91342575941 2992.87692864617,3992.83416626108 2987.71348252632,3993.17091274715 2987.96604239088,3998.30629665983 2992.93305306051,3997.91342575941</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>56.J.20.h.1.b</ogr:Car>
      <ogr:xmin>2987.713479999999890</ogr:xmin>
      <ogr:ymin>3992.834159999999883</ogr:ymin>
      <ogr:Zusatz1>h</ogr:Zusatz1>
      <ogr:Zusatz2>1</ogr:Zusatz2>
      <ogr:Zusatz3>b</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.158">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2998.18068580188,3997.94148796658 2998.12456138753,3992.55354418935 2992.79274202465,3993.03060171129 2993.04530188921,3998.16598562396 2998.18068580188,3997.94148796658</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>56.J.20.I.1.a</ogr:Car>
      <ogr:xmin>2992.792739999999867</ogr:xmin>
      <ogr:ymin>3992.553539999999884</ogr:ymin>
      <ogr:Zusatz1>I</ogr:Zusatz1>
      <ogr:Zusatz2>1</ogr:Zusatz2>
      <ogr:Zusatz3>a</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.159">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3003.00738543565,3997.35218161594 3002.97932322848,3992.30098432479 2998.15262359471,3992.60966860369 2998.23681021623,3997.88536355223 3003.00738543565,3997.35218161594</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>56.J.20.I.1.b</ogr:Car>
      <ogr:xmin>2998.152619999999843</ogr:xmin>
      <ogr:ymin>3992.300979999999981</ogr:ymin>
      <ogr:Zusatz1>I</ogr:Zusatz1>
      <ogr:Zusatz2>1</ogr:Zusatz2>
      <ogr:Zusatz3>b</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.160">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2988.13765016001,4003.54696709021 2987.91402574137,3998.20793409518 2983.0802074875,3998.47040386462 2983.34329051922,4003.82754280643 2988.13765016001,4003.54696709021</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>136</ogr:id>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>21</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>56.J.21.h.10.c</ogr:Car>
      <ogr:xmin>2983.080199999999877</ogr:xmin>
      <ogr:ymin>3998.207930000000033</ogr:ymin>
      <ogr:Zusatz1>h</ogr:Zusatz1>
      <ogr:Zusatz2>10</ogr:Zusatz2>
      <ogr:Zusatz3>c</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.161">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2988.33239471885,4008.19649343245 2988.13765016001,4003.54696709021 2983.34329051922,4003.82754280643 2983.56916029431,4008.42691115037 2988.33239471885,4008.19649343245</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>136</ogr:id>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>21</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>56.J.21.h.10.a</ogr:Car>
      <ogr:xmin>2983.343289999999797</ogr:xmin>
      <ogr:ymin>4003.546960000000126</ogr:ymin>
      <ogr:Zusatz1>h</ogr:Zusatz1>
      <ogr:Zusatz2>10</ogr:Zusatz2>
      <ogr:Zusatz3>a</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.162">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2988.13765016001,4003.54696709021 2988.33239471885,4008.19649343245 2993.41826459003,4007.95046847862 2993.1875205992,4003.25143836032 2988.13765016001,4003.54696709021</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>136</ogr:id>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>21</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>56.J.21.h.10.b</ogr:Car>
      <ogr:xmin>2988.137650000000122</ogr:xmin>
      <ogr:ymin>4003.251429999999800</ogr:ymin>
      <ogr:Zusatz1>h</ogr:Zusatz1>
      <ogr:Zusatz2>10</ogr:Zusatz2>
      <ogr:Zusatz3>b</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.163">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2979.02538547596,4003.99993308985 2974.34838972246,4004.20779956778 2974.57297987991,4008.86209430039 2979.25949934053,4008.63538760849 2979.02538547596,4003.99993308985</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>137</ogr:id>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>21</ogr:ArabischeZ>
      <ogr:Zusammense>J21g10</ogr:Zusammense>
      <ogr:Car>56.J.21.g.10.a</ogr:Car>
      <ogr:xmin>2974.348390000000109</ogr:xmin>
      <ogr:ymin>4003.999929999999949</ogr:ymin>
      <ogr:Zusatz1>g</ogr:Zusatz1>
      <ogr:Zusatz2>10</ogr:Zusatz2>
      <ogr:Zusatz3>a</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.164">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2983.34233416806,4003.80806870353 2979.02538547596,4003.99993308985 2979.25949934053,4008.63538760849 2983.56916029431,4008.42691115037 2983.34233416806,4003.80806870353</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>137</ogr:id>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>21</ogr:ArabischeZ>
      <ogr:Zusammense>J21g10</ogr:Zusammense>
      <ogr:Car>56.J.21.g.10.b</ogr:Car>
      <ogr:xmin>2979.025380000000041</ogr:xmin>
      <ogr:ymin>4003.808059999999841</ogr:ymin>
      <ogr:Zusatz1>g</ogr:Zusatz1>
      <ogr:Zusatz2>10</ogr:Zusatz2>
      <ogr:Zusatz3>b</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.165">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2974.34838972246,4004.20779956778 2979.02538547596,4003.99993308985 2978.76181092431,3998.78115696718 2974.10270865735,3999.11642735364 2974.34838972246,4004.20779956778</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>137</ogr:id>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>21</ogr:ArabischeZ>
      <ogr:Zusammense>J21g10</ogr:Zusammense>
      <ogr:Car>56.J.21.g.10.c</ogr:Car>
      <ogr:xmin>2974.102699999999913</ogr:xmin>
      <ogr:ymin>3998.781149999999798</ogr:ymin>
      <ogr:Zusatz1>g</ogr:Zusatz1>
      <ogr:Zusatz2>10</ogr:Zusatz2>
      <ogr:Zusatz3>c</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.166">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2965.54666471953,4087.15830728217 2962.60469281909,4085.28320431266 2954.63222077046,4098.29676067502 2958.26157246881,4099.0386304458 2965.54666471953,4087.15830728217</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>131</ogr:id>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>Schnitt.B</ogr:RomischeZa>
      <ogr:ArabischeZ>100-110m</ogr:ArabischeZ>
      <ogr:Zusammense>56 - Schnitt B</ogr:Zusammense>
      <ogr:Car>56.Schnitt.B.100-110m</ogr:Car>
      <ogr:xmin>2954.632219999999961</ogr:xmin>
      <ogr:ymin>4085.283199999999852</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.167">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2962.60469281909,4085.28320431266 2965.54666471953,4087.15830728217 2971.38849575147,4077.63161384602 2968.34207876624,4075.91800429183 2962.60469281909,4085.28320431266</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>131</ogr:id>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>Schnitt.B</ogr:RomischeZa>
      <ogr:ArabischeZ>90-100m</ogr:ArabischeZ>
      <ogr:Zusammense>56 - Schnitt B</ogr:Zusammense>
      <ogr:Car>56.Schnitt.B.90-100m</ogr:Car>
      <ogr:xmin>2962.604690000000119</ogr:xmin>
      <ogr:ymin>4075.918000000000120</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.168">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2968.34207876624,4075.91800429183 2971.38849575147,4077.63161384602 2976.84494468606,4068.73339067497 2973.80926973073,4066.99384659944 2968.34207876624,4075.91800429183</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>131</ogr:id>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>Schnitt.B</ogr:RomischeZa>
      <ogr:ArabischeZ>80-90m</ogr:ArabischeZ>
      <ogr:Zusammense>56 - Schnitt B</ogr:Zusammense>
      <ogr:Car>56.Schnitt.B.80-90m</ogr:Car>
      <ogr:xmin>2968.342070000000149</ogr:xmin>
      <ogr:ymin>4066.993840000000091</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.169">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2973.80926973073,4066.99384659944 2976.84494468606,4068.73339067497 2982.2818404311,4059.86705429444 2979.33742715502,4057.97017266466 2973.80926973073,4066.99384659944</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>131</ogr:id>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>Schnitt.B</ogr:RomischeZa>
      <ogr:ArabischeZ>70-80m</ogr:ArabischeZ>
      <ogr:Zusammense>56 - Schnitt B</ogr:Zusammense>
      <ogr:Car>56.Schnitt.B.70-80m</ogr:Car>
      <ogr:xmin>2973.809270000000197</ogr:xmin>
      <ogr:ymin>4057.970170000000053</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.170">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2979.33742715502,4057.97017266466 2982.2818404311,4059.86705429444 2987.92914349312,4050.65759159025 2984.98456918878,4048.75227880509 2979.33742715502,4057.97017266466</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>131</ogr:id>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>Schnitt.B</ogr:RomischeZa>
      <ogr:ArabischeZ>60-70m</ogr:ArabischeZ>
      <ogr:Zusammense>56 - Schnitt B</ogr:Zusammense>
      <ogr:Car>56.Schnitt.B.60-70m</ogr:Car>
      <ogr:xmin>2979.337419999999838</ogr:xmin>
      <ogr:ymin>4048.752269999999953</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.171">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2984.98456918878,4048.75227880509 2987.92914349312,4050.65759159025 2993.47624831262,4041.61152935656 2990.50333718479,4039.74393134037 2984.98456918878,4048.75227880509</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>131</ogr:id>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>Schnitt.B</ogr:RomischeZa>
      <ogr:ArabischeZ>50-60m</ogr:ArabischeZ>
      <ogr:Zusammense>56 - Schnitt B</ogr:Zusammense>
      <ogr:Car>56.Schnitt.B.50-60m</ogr:Car>
      <ogr:xmin>2984.984559999999874</ogr:xmin>
      <ogr:ymin>4039.743930000000091</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.172">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2990.50333718479,4039.74393134037 2993.47624831262,4041.61152935656 2998.9746164859,4032.64494547225 2996.02425542172,4030.73207401305 2990.50333718479,4039.74393134037</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>131</ogr:id>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>Schnitt.B</ogr:RomischeZa>
      <ogr:ArabischeZ>40-50m</ogr:ArabischeZ>
      <ogr:Zusammense>56 - Schnitt B</ogr:Zusammense>
      <ogr:Car>56.Schnitt.B.40-50m</ogr:Car>
      <ogr:xmin>2990.503330000000005</ogr:xmin>
      <ogr:ymin>4030.732070000000022</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.173">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3007.09729178684,4019.39871547168 3004.16966039261,4017.43623728433 3001.4692406886,4021.84416293221 3004.45139336032,4023.7135720697 3007.09729178684,4019.39871547168</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>131</ogr:id>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>Schnitt.B</ogr:RomischeZa>
      <ogr:ArabischeZ>25-30m</ogr:ArabischeZ>
      <ogr:Zusammense>56 - Schnitt B</ogr:Zusammense>
      <ogr:Car>56.Schnitt.B.25-30m</ogr:Car>
      <ogr:xmin>3001.469239999999900</ogr:xmin>
      <ogr:ymin>4017.436229999999796</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.174">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2996.02425542172,4030.73207401305 2998.9746164859,4032.64494547225 3004.45139336032,4023.7135720697 3001.4692406886,4021.84416293221 2996.02425542172,4030.73207401305</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>131</ogr:id>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>Schnitt.B</ogr:RomischeZa>
      <ogr:ArabischeZ>30-40m</ogr:ArabischeZ>
      <ogr:Zusammense>56 - Schnitt B</ogr:Zusammense>
      <ogr:Car>56.Schnitt.B.30-40m</ogr:Car>
      <ogr:xmin>2996.024249999999938</ogr:xmin>
      <ogr:ymin>4021.844160000000102</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.175">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3004.16966039261,4017.43623728433 3007.09729178684,4019.39871547168 3009.89949709313,4014.82895801855 3006.90544688508,4012.97058202734 3004.16966039261,4017.43623728433</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>131</ogr:id>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>Schnitt.B</ogr:RomischeZa>
      <ogr:ArabischeZ>20-25m</ogr:ArabischeZ>
      <ogr:Zusammense>56 - Schnitt B</ogr:Zusammense>
      <ogr:Car>56.Schnitt.B.20-25m</ogr:Car>
      <ogr:xmin>3004.169660000000022</ogr:xmin>
      <ogr:ymin>4012.970580000000155</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.176">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3006.96402818213,4013.00812626621 3009.95807839019,4014.86650225742 3015.43081509651,4005.94171744717 3012.48388670557,4003.99799872123 3006.96402818213,4013.00812626621</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>131</ogr:id>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>Schnitt.B</ogr:RomischeZa>
      <ogr:ArabischeZ>10-20m</ogr:ArabischeZ>
      <ogr:Zusammense>56 - Schnitt B</ogr:Zusammense>
      <ogr:Car>56.Schnitt.B.10-20m</ogr:Car>
      <ogr:xmin>3006.964019999999891</ogr:xmin>
      <ogr:ymin>4003.997989999999845</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.177">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2860.18249789505,3908.50377871461 2856.33471246499,3900.8082078545 2858.93762613827,3899.67650625742 2856.32189878512,3892.87222738152 2853.91880980227,3893.11423103821 2852.26058671552,3892.26386079657 2849.31816256312,3888.35949028666 2846.31915333087,3876.81613399648 2836.64310467586,3885.9829169328 2839.30260342899,3895.26287002882 2829.0041188956,3901.37405865303 2831.2109370099,3907.42866219739 2833.0006042147,3906.79940855165 2834.32311640186,3909.80523555125 2837.66163611323,3908.67353395418 2838.96309294987,3909.40913999228 2842.3581977411,3908.05109807579 2845.58354729277,3914.2754568597 2847.11134444882,3913.82277622087 2848.69572668473,3914.84130765824 2851.12888511844,3913.03058510292 2853.95813911113,3915.52032861649 2854.9766705485,3914.84130765824 2852.60009719464,3911.78571334614 2860.18249789505,3908.50377871461</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>Stadttor I/19</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>56.Stadttor I/19</ogr:Car>
      <ogr:xmin>2829.004109999999855</ogr:xmin>
      <ogr:ymin>3876.816130000000157</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.178">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3015.67278139098,3937.51265111027 3015.76730805112,3944.24294931234 3020.62949182055,3940.72898169136 3020.62949182055,3937.1924142005 3015.67278139098,3937.51265111027</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.K.20.a.6.d</ogr:Car>
      <ogr:xmin>3015.672779999999875</ogr:xmin>
      <ogr:ymin>3937.192410000000109</ogr:ymin>
      <ogr:Zusatz1>a</ogr:Zusatz1>
      <ogr:Zusatz2>6</ogr:Zusatz2>
      <ogr:Zusatz3>d</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.179">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3015.74151594923,3942.40655165791 3010.79565614669,3942.70744046719 3011.03832078533,3947.66065397346 3015.76730805112,3944.24294931234 3015.74151594923,3942.40655165791</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.K.20.a.6.a</ogr:Car>
      <ogr:xmin>3010.795650000000023</ogr:xmin>
      <ogr:ymin>3942.406550000000152</ogr:ymin>
      <ogr:Zusatz1>a</ogr:Zusatz1>
      <ogr:Zusatz2>6</ogr:Zusatz2>
      <ogr:Zusatz3>a</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.180">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3015.63087593198,3932.50438254938 3020.20543144268,3932.28882757767 3019.89388578245,3927.37490284586 3015.42572376657,3927.59192785806 3015.63087593198,3932.50438254938</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.K.20.a.7.d</ogr:Car>
      <ogr:xmin>3015.425720000000183</ogr:xmin>
      <ogr:ymin>3927.374899999999798</ogr:ymin>
      <ogr:Zusatz1>a</ogr:Zusatz1>
      <ogr:Zusatz2>7</ogr:Zusatz2>
      <ogr:Zusatz3>d</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.181">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3015.62961176682,3932.47411158422 3010.27219160006,3932.76803488514 3010.55734760657,3937.75826499904 3015.83775721659,3937.45824172574 3015.62961176682,3932.47411158422</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.K.20.a.7.a</ogr:Car>
      <ogr:xmin>3010.272190000000137</ogr:xmin>
      <ogr:ymin>3932.474110000000110</ogr:ymin>
      <ogr:Zusatz1>a</ogr:Zusatz1>
      <ogr:Zusatz2>7</ogr:Zusatz2>
      <ogr:Zusatz3>a</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.182">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3025.58446809079,3931.86624508542 3020.21962878158,3932.05687389336 3020.51632166084,3937.24899928035 3025.79673127086,3936.94897600706 3025.58446809079,3931.86624508542</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.K.20.b.7.a</ogr:Car>
      <ogr:xmin>3020.219619999999850</ogr:xmin>
      <ogr:ymin>3931.866239999999834</ogr:ymin>
      <ogr:Zusatz1>b</ogr:Zusatz1>
      <ogr:Zusatz2>7</ogr:Zusatz2>
      <ogr:Zusatz3>a</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.183">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3030.15959436246,3931.70367714683 3025.58446809079,3931.86624508542 3025.79673127086,3936.94897600706 3030.47529571511,3936.68314848181 3030.15959436246,3931.70367714683</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.K.20.b.7.b</ogr:Car>
      <ogr:xmin>3025.584460000000036</ogr:xmin>
      <ogr:ymin>3931.703669999999875</ogr:ymin>
      <ogr:Zusatz1>b</ogr:Zusatz1>
      <ogr:Zusatz2>7</ogr:Zusatz2>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.184">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3025.58446809079,3931.86624508542 3030.15959436246,3931.70367714683 3029.85285983672,3926.86563712718 3025.38469782085,3927.08266213938 3025.58446809079,3931.86624508542</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.K.20.b.7.d</ogr:Car>
      <ogr:xmin>3025.384689999999864</ogr:xmin>
      <ogr:ymin>3926.865629999999783</ogr:ymin>
      <ogr:Zusatz1>b</ogr:Zusatz1>
      <ogr:Zusatz2>7</ogr:Zusatz2>
      <ogr:Zusatz3>d</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.185">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3020.21962878158,3932.05687389336 3025.58446809079,3931.86624508542 3025.38469782085,3927.08266213938 3019.9504708623,3927.34661030593 3020.21962878158,3932.05687389336</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.K.20.b.7.c</ogr:Car>
      <ogr:xmin>3019.950470000000223</ogr:xmin>
      <ogr:ymin>3927.082660000000033</ogr:ymin>
      <ogr:Zusatz1>b</ogr:Zusatz1>
      <ogr:Zusatz2>7</ogr:Zusatz2>
      <ogr:Zusatz3>c</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.186">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2989.46391648684,3947.64625146968 2992.86715180533,3946.19207896185 2990.84110614505,3941.68958212777 2987.48390365244,3943.22994562438 2989.46391648684,3947.64625146968</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>127</ogr:id>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>Schnitt.C</ogr:RomischeZa>
      <ogr:ArabischeZ>10-15m</ogr:ArabischeZ>
      <ogr:Zusammense>1956 / Schnitt C</ogr:Zusammense>
      <ogr:Car>56.Schnitt.C.10-15m</ogr:Car>
      <ogr:xmin>2987.483900000000176</ogr:xmin>
      <ogr:ymin>3941.689580000000205</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.187">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2987.48390365244,3943.22994562438 2990.84110614505,3941.68958212777 2988.66434809237,3936.85215598634 2985.40767444063,3938.59903462976 2987.48390365244,3943.22994562438</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>127</ogr:id>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>Schnitt.C</ogr:RomischeZa>
      <ogr:ArabischeZ>15-20m</ogr:ArabischeZ>
      <ogr:Zusammense>1956 / Schnitt C</ogr:Zusammense>
      <ogr:Car>56.Schnitt.C.15-20m</ogr:Car>
      <ogr:xmin>2985.407670000000053</ogr:xmin>
      <ogr:ymin>3936.852150000000165</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.188">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2985.40767444063,3938.59903462976 2988.66434809237,3936.85215598634 2986.48740895152,3932.0143274113 2983.20088697323,3933.67692088267 2985.40767444063,3938.59903462976</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>127</ogr:id>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>Schnitt.C</ogr:RomischeZa>
      <ogr:ArabischeZ>20-25m</ogr:ArabischeZ>
      <ogr:Zusammense>1956 / Schnitt C</ogr:Zusammense>
      <ogr:Car>56.Schnitt.C.20-25m</ogr:Car>
      <ogr:xmin>2983.200879999999870</ogr:xmin>
      <ogr:ymin>3932.014320000000225</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.189">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2983.20088697323,3933.67692088267 2986.48740895152,3932.0143274113 2984.33810531734,3927.23791343502 2981.0736079698,3928.93214623564 2983.20088697323,3933.67692088267</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>127</ogr:id>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>Schnitt.C</ogr:RomischeZa>
      <ogr:ArabischeZ>25-30m</ogr:ArabischeZ>
      <ogr:Zusammense>1956 / Schnitt C</ogr:Zusammense>
      <ogr:Car>56.Schnitt.C.25-30m</ogr:Car>
      <ogr:xmin>2981.073600000000170</ogr:xmin>
      <ogr:ymin>3927.237909999999829</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.190">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2981.0736079698,3928.93214623564 2984.33810531734,3927.23791343502 2982.22762103318,3922.54776799845 2978.89524590123,3924.07343372151 2981.0736079698,3928.93214623564</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>127</ogr:id>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>Schnitt.C</ogr:RomischeZa>
      <ogr:ArabischeZ>30-35m</ogr:ArabischeZ>
      <ogr:Zusammense>1956 / Schnitt C</ogr:Zusammense>
      <ogr:Car>56.Schnitt.C.30-35m</ogr:Car>
      <ogr:xmin>2978.895239999999831</ogr:xmin>
      <ogr:ymin>3922.547759999999926</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.191">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2978.89524590123,3924.07343372151 2982.22762103318,3922.54776799845 2980.12352160876,3917.87181168476 2976.76185094298,3919.31501778562 2978.89524590123,3924.07343372151</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>127</ogr:id>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>Schnitt.C</ogr:RomischeZa>
      <ogr:ArabischeZ>35-40m</ogr:ArabischeZ>
      <ogr:Zusammense>1956 / Schnitt C</ogr:Zusammense>
      <ogr:Car>56.Schnitt.C.35-40m</ogr:Car>
      <ogr:xmin>2976.761849999999868</ogr:xmin>
      <ogr:ymin>3917.871810000000096</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.192">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2992.86715180533,3946.19207896185 2989.46391648684,3947.64625146968 2991.70244596214,3952.63916395737 2995.05772593453,3951.06020867624 2992.86715180533,3946.19207896185</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>127</ogr:id>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>Schnitt.C</ogr:RomischeZa>
      <ogr:ArabischeZ>5-10m</ogr:ArabischeZ>
      <ogr:Zusammense>1956 / Schnitt C</ogr:Zusammense>
      <ogr:Car>56.Schnitt.C.5-10m</ogr:Car>
      <ogr:xmin>2989.463909999999942</ogr:xmin>
      <ogr:ymin>3946.192070000000058</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.193">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2930.96246431575,3887.7657394936 2941.74151769229,3879.6672993631 2926.56969666644,3861.39295876242 2906.6112594643,3874.69858356385 2914.89222190602,3883.86378545303 2924.1237634749,3883.53987562118 2929.37823828765,3887.74534709968 2930.96246431575,3887.7657394936</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>19</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>56.J.19</ogr:Car>
      <ogr:xmin>2906.611249999999927</ogr:xmin>
      <ogr:ymin>3861.392949999999928</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.194">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3012.65434349714,3996.90679717585 3012.52717235882,3986.82848446434 3002.89395863142,3987.40075458676 3003.11171466963,3997.34683692202 3012.65434349714,3996.90679717585</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>53</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>53.J.20.k</ogr:Car>
      <ogr:xmin>3002.893950000000132</ogr:xmin>
      <ogr:ymin>3986.828480000000127</ogr:ymin>
      <ogr:Zusatz1>k</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.195">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3012.36820843593,3986.92386281808 3012.1138661593,3977.00451402947 3002.57603078563,3977.41782022899 3002.73499470853,3987.52792572508 3012.36820843593,3986.92386281808</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>56</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>56.J.20.k.2</ogr:Car>
      <ogr:xmin>3002.576030000000173</ogr:xmin>
      <ogr:ymin>3977.004510000000209</ogr:ymin>
      <ogr:Zusatz1>k</ogr:Zusatz1>
      <ogr:Zusatz2>2</ogr:Zusatz2>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.196">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2982.82835515066,3993.43836746312 2982.60336844508,3988.56428923249 2977.8894066882,3988.72391665804 2978.25421538145,3993.74705174203 2982.82835515066,3993.43836746312</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.g.1.d</ogr:Car>
      <ogr:xmin>2977.889400000000023</ogr:xmin>
      <ogr:ymin>3988.564280000000053</ogr:ymin>
      <ogr:Zusatz1>g</ogr:Zusatz1>
      <ogr:Zusatz2>1</ogr:Zusatz2>
      <ogr:Zusatz3>d</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.197">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2982.51713721147,3988.34571027628 2982.29215050589,3983.47163204565 2977.74794398856,3983.65955201113 2977.8864123624,3988.5695169354 2982.51713721147,3988.34571027628</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.g.2.b</ogr:Car>
      <ogr:xmin>2977.747940000000199</ogr:xmin>
      <ogr:ymin>3983.471630000000005</ogr:ymin>
      <ogr:Zusatz1>g</ogr:Zusatz1>
      <ogr:Zusatz2>2</ogr:Zusatz2>
      <ogr:Zusatz3>b</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.198">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2987.86442725765,3998.21980671077 2987.63944055208,3993.34572848014 2983.09523403475,3993.53364844562 2983.23370240859,3998.4436133699 2987.86442725765,3998.21980671077</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.h.1.b</ogr:Car>
      <ogr:xmin>2983.095229999999901</ogr:xmin>
      <ogr:ymin>3993.345719999999801</ogr:ymin>
      <ogr:Zusatz1>h</ogr:Zusatz1>
      <ogr:Zusatz2>1</ogr:Zusatz2>
      <ogr:Zusatz3>b</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.199">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2987.69467201809,3993.12714952393 2987.46968531251,3988.2530712933 2982.92547879519,3988.44099125878 2983.06394716902,3993.35095618305 2987.69467201809,3993.12714952393</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.h.1.c</ogr:Car>
      <ogr:xmin>2982.925470000000132</ogr:xmin>
      <ogr:ymin>3988.253070000000207</ogr:ymin>
      <ogr:Zusatz1>h</ogr:Zusatz1>
      <ogr:Zusatz2>1</ogr:Zusatz2>
      <ogr:Zusatz3>c</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.200">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2992.58928142545,3992.87251666459 2992.36429471987,3987.99843843396 2987.82008820254,3988.18635839943 2987.95855657638,3993.09632332371 2992.58928142545,3992.87251666459</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.h.1.d</ogr:Car>
      <ogr:xmin>2987.820079999999962</ogr:xmin>
      <ogr:ymin>3987.998430000000099</ogr:ymin>
      <ogr:Zusatz1>h</ogr:Zusatz1>
      <ogr:Zusatz2>1</ogr:Zusatz2>
      <ogr:Zusatz3>d</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.201">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3002.03898976103,3982.26281419199 3001.81400305546,3977.38873596136 2997.26979653813,3977.57665592684 2997.40826491197,3982.48662085112 3002.03898976103,3982.26281419199</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.I.2</ogr:Car>
      <ogr:xmin>2997.269789999999830</ogr:xmin>
      <ogr:ymin>3977.388730000000123</ogr:ymin>
      <ogr:Zusatz1>I</ogr:Zusatz1>
      <ogr:Zusatz2>2</ogr:Zusatz2>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.202">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2992.2214784064,3987.66668931803 2991.99649170082,3982.7926110874 2987.45228518349,3982.98053105288 2987.59075355733,3987.89049597716 2992.2214784064,3987.66668931803</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.h.2.b</ogr:Car>
      <ogr:xmin>2987.452279999999973</ogr:xmin>
      <ogr:ymin>3982.792609999999968</ogr:ymin>
      <ogr:Zusatz1>h</ogr:Zusatz1>
      <ogr:Zusatz2>2</ogr:Zusatz2>
      <ogr:Zusatz3>b</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.203">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2997.31413559324,3987.61010423818 2997.08914888766,3982.73602600755 2992.54494237034,3982.92394597303 2992.68341074417,3987.8339108973 2997.31413559324,3987.61010423818</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.I.2.a</ogr:Car>
      <ogr:xmin>2992.544940000000224</ogr:xmin>
      <ogr:ymin>3982.736019999999826</ogr:ymin>
      <ogr:Zusatz1>I</ogr:Zusatz1>
      <ogr:Zusatz2>2</ogr:Zusatz2>
      <ogr:Zusatz3>a</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.204">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2984.80184634941,3929.23072175337 2985.02547076805,3934.5697547484 2990.07534120724,3934.27422601851 2989.81431713505,3928.95855139397 2984.80184634941,3929.23072175337</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.h.7.d</ogr:Car>
      <ogr:xmin>2984.801840000000084</ogr:xmin>
      <ogr:ymin>3928.958549999999832</ogr:ymin>
      <ogr:Zusatz1>h</ogr:Zusatz1>
      <ogr:Zusatz2>7</ogr:Zusatz2>
      <ogr:Zusatz3>d</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.205">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2985.02547076805,3934.5697547484 2985.22021532689,3939.21928109064 2990.30608519807,3938.97325613681 2990.07534120724,3934.27422601851 2985.02547076805,3934.5697547484</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.h.7.b</ogr:Car>
      <ogr:xmin>2985.025470000000041</ogr:xmin>
      <ogr:ymin>3934.274219999999787</ogr:ymin>
      <ogr:Zusatz1>h</ogr:Zusatz1>
      <ogr:Zusatz2>7</ogr:Zusatz2>
      <ogr:Zusatz3>b</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.206">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2985.22021532689,3939.21928109064 2985.02547076805,3934.5697547484 2980.23111112726,3934.85033046462 2980.45698090235,3939.44969880856 2985.22021532689,3939.21928109064</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.h.7.a</ogr:Car>
      <ogr:xmin>2980.231110000000172</ogr:xmin>
      <ogr:ymin>3934.569750000000113</ogr:ymin>
      <ogr:Zusatz1>h</ogr:Zusatz1>
      <ogr:Zusatz2>7</ogr:Zusatz2>
      <ogr:Zusatz3>a</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.207">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3010.27219160006,3932.76803488514 3015.62961176682,3932.47411158422 3015.42572376657,3927.59192785806 3009.99149680803,3927.85587602462 3010.27219160006,3932.76803488514</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.K.20.a.7.c</ogr:Car>
      <ogr:xmin>3009.991489999999885</ogr:xmin>
      <ogr:ymin>3927.591919999999845</ogr:ymin>
      <ogr:Zusatz1>a</ogr:Zusatz1>
      <ogr:Zusatz2>7</ogr:Zusatz2>
      <ogr:Zusatz3>c</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.208">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2997.42730575295,3992.47642110561 2997.20231904737,3987.60234287498 2992.65811253005,3987.79026284046 2992.79658090388,3992.70022776473 2997.42730575295,3992.47642110561</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.I.1.c</ogr:Car>
      <ogr:xmin>2992.658109999999851</ogr:xmin>
      <ogr:ymin>3987.602339999999913</ogr:ymin>
      <ogr:Zusatz1>I</ogr:Zusatz1>
      <ogr:Zusatz2>1</ogr:Zusatz2>
      <ogr:Zusatz3>c</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.209">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2998.18068580188,3997.94148796658 2998.12456138753,3992.55354418935 2992.79274202465,3993.03060171129 2993.04530188921,3998.16598562396 2998.18068580188,3997.94148796658</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.I.1.a</ogr:Car>
      <ogr:xmin>2992.792739999999867</ogr:xmin>
      <ogr:ymin>3992.553539999999884</ogr:ymin>
      <ogr:Zusatz1>I</ogr:Zusatz1>
      <ogr:Zusatz2>1</ogr:Zusatz2>
      <ogr:Zusatz3>a</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.210">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2995.72582319595,3943.78710852817 3001.02746223525,3943.5882970642 3000.83886014167,3938.40899341736 2995.57030476094,3938.73275939047 2995.72582319595,3943.78710852817</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.I.6.d</ogr:Car>
      <ogr:xmin>2995.570299999999861</ogr:xmin>
      <ogr:ymin>3938.408989999999903</ogr:ymin>
      <ogr:Zusatz1>I</ogr:Zusatz1>
      <ogr:Zusatz2>6</ogr:Zusatz2>
      <ogr:Zusatz3>d</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.211">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2990.94645027653,3943.96633501265 2995.72582319595,3943.78710852817 2995.57030476094,3938.73275939047 2990.71013084784,3939.03142929575 2990.94645027653,3943.96633501265</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.I.6.c</ogr:Car>
      <ogr:xmin>2990.710129999999936</ogr:xmin>
      <ogr:ymin>3938.732750000000124</ogr:ymin>
      <ogr:Zusatz1>I</ogr:Zusatz1>
      <ogr:Zusatz2>6</ogr:Zusatz2>
      <ogr:Zusatz3>c</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.212">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3001.02746223525,3943.5882970642 2995.72582319595,3943.78710852817 2995.88038446805,3948.81034987142 3001.20666316072,3948.50943017126 3001.02746223525,3943.5882970642</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.I.6.b</ogr:Car>
      <ogr:xmin>2995.725820000000112</ogr:xmin>
      <ogr:ymin>3943.588290000000143</ogr:ymin>
      <ogr:Zusatz1>I</ogr:Zusatz1>
      <ogr:Zusatz2>6</ogr:Zusatz2>
      <ogr:Zusatz3>b</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.213">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2995.72582319595,3943.78710852817 2990.94645027653,3943.96633501265 2991.19110402659,3949.0752809698 2995.88038446805,3948.81034987142 2995.72582319595,3943.78710852817</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.I.6.a</ogr:Car>
      <ogr:xmin>2990.946449999999913</ogr:xmin>
      <ogr:ymin>3943.787100000000009</ogr:ymin>
      <ogr:Zusatz1>I</ogr:Zusatz1>
      <ogr:Zusatz2>6</ogr:Zusatz2>
      <ogr:Zusatz3>a</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.214">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2991.33256672623,3958.55328184532 3001.97056173875,3958.3269415259 3001.57446617977,3948.93381827017 2990.71013084784,3949.35820636907 2991.33256672623,3958.55328184532</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.I.5</ogr:Car>
      <ogr:xmin>2990.710129999999936</ogr:xmin>
      <ogr:ymin>3948.933809999999994</ogr:ymin>
      <ogr:Zusatz1>I</ogr:Zusatz1>
      <ogr:Zusatz2>5</ogr:Zusatz2>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.215">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2995.44289779668,3933.60179415449 2990.66352487726,3933.78102063896 2990.90817862733,3938.88996659611 2995.59745906878,3938.62503549773 2995.44289779668,3933.60179415449</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.I.7.a</ogr:Car>
      <ogr:xmin>2990.663520000000062</ogr:xmin>
      <ogr:ymin>3933.601790000000165</ogr:ymin>
      <ogr:Zusatz1>I</ogr:Zusatz1>
      <ogr:Zusatz2>7</ogr:Zusatz2>
      <ogr:Zusatz3>a</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.216">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3000.74453683598,3933.40298269051 2995.44289779668,3933.60179415449 2995.59745906878,3938.62503549773 3000.92373776145,3938.32411579758 3000.74453683598,3933.40298269051</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.I.7.b</ogr:Car>
      <ogr:xmin>2995.442889999999807</ogr:xmin>
      <ogr:ymin>3933.402979999999843</ogr:ymin>
      <ogr:Zusatz1>I</ogr:Zusatz1>
      <ogr:Zusatz2>7</ogr:Zusatz2>
      <ogr:Zusatz3>b</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.217">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2990.66352487726,3933.78102063896 2995.44289779668,3933.60179415449 2995.28737936167,3928.54744501679 2990.42720544857,3928.84611492206 2990.66352487726,3933.78102063896</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.I.7.c</ogr:Car>
      <ogr:xmin>2990.427200000000084</ogr:xmin>
      <ogr:ymin>3928.547439999999824</ogr:ymin>
      <ogr:Zusatz1>I</ogr:Zusatz1>
      <ogr:Zusatz2>7</ogr:Zusatz2>
      <ogr:Zusatz3>c</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.218">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2995.44289779668,3933.60179415449 3000.74453683598,3933.40298269051 3000.5559347424,3928.22367904367 2995.28737936167,3928.54744501679 2995.44289779668,3933.60179415449</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.I.7.d</ogr:Car>
      <ogr:xmin>2995.287370000000010</ogr:xmin>
      <ogr:ymin>3928.223669999999856</ogr:ymin>
      <ogr:Zusatz1>I</ogr:Zusatz1>
      <ogr:Zusatz2>7</ogr:Zusatz2>
      <ogr:Zusatz3>d</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.219">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2990.79500846762,3939.11630691553 3000.98032284131,3938.32411579758 3000.85136625953,3928.79351683124 2990.39891290864,3929.1007477814 2990.79500846762,3939.11630691553</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.I.7</ogr:Car>
      <ogr:xmin>2990.398909999999887</ogr:xmin>
      <ogr:ymin>3928.793509999999969</ogr:ymin>
      <ogr:Zusatz1>I</ogr:Zusatz1>
      <ogr:Zusatz2>7</ogr:Zusatz2>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.220">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2995.10338731756,3923.86916041963 3000.40502635686,3923.67034895565 3000.21642426328,3918.49104530881 2994.94786888255,3918.81481128193 2995.10338731756,3923.86916041963</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.I.8.d</ogr:Car>
      <ogr:xmin>2994.947860000000219</ogr:xmin>
      <ogr:ymin>3918.491039999999884</ogr:ymin>
      <ogr:Zusatz1>I</ogr:Zusatz1>
      <ogr:Zusatz2>8</ogr:Zusatz2>
      <ogr:Zusatz3>d</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.221">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3007.6117079358,3992.23317826727 3007.83061073096,3987.25287256325 3002.92225117135,3987.5422172864 3002.97025197,3992.4805200546 3007.6117079358,3992.23317826727</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.k.1.c</ogr:Car>
      <ogr:xmin>3002.922250000000076</ogr:xmin>
      <ogr:ymin>3987.252869999999803</ogr:ymin>
      <ogr:Zusatz1>k</ogr:Zusatz1>
      <ogr:Zusatz2>1</ogr:Zusatz2>
      <ogr:Zusatz3>c</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.222">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3012.64778004279,3991.92196032807 3012.86668283795,3986.94165462405 3007.95832327834,3987.2309993472 3008.00632407699,3992.1693021154 3012.64778004279,3991.92196032807</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.k.1.d</ogr:Car>
      <ogr:xmin>3007.958320000000185</ogr:xmin>
      <ogr:ymin>3986.941650000000209</ogr:ymin>
      <ogr:Zusatz1>k</ogr:Zusatz1>
      <ogr:Zusatz2>1</ogr:Zusatz2>
      <ogr:Zusatz3>d</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.223">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3001.69761544876,3953.33116572846 3006.47698836817,3953.15193924398 3006.32146993317,3948.09759010628 3001.46129602006,3948.39626001156 3001.69761544876,3953.33116572846</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.k.5.b</ogr:Car>
      <ogr:xmin>3001.461290000000190</ogr:xmin>
      <ogr:ymin>3948.097589999999855</ogr:ymin>
      <ogr:Zusatz1>k</ogr:Zusatz1>
      <ogr:Zusatz2>5</ogr:Zusatz2>
      <ogr:Zusatz3>b</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.224">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3006.08295913743,3952.93507016948 3010.86233205684,3952.755843685 3010.70681362184,3947.70149454731 3005.84663970873,3948.00016445258 3006.08295913743,3952.93507016948</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.k.5</ogr:Car>
      <ogr:xmin>3005.846640000000207</ogr:xmin>
      <ogr:ymin>3947.701489999999922</ogr:ymin>
      <ogr:Zusatz1>k</ogr:Zusatz1>
      <ogr:Zusatz2>5</ogr:Zusatz2>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.225">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3005.68479725022,3942.96662487029 3000.90542433081,3943.14585135477 3001.15007808087,3948.25479731192 3005.83935852232,3947.98986621354 3005.68479725022,3942.96662487029</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.k.6.a</ogr:Car>
      <ogr:xmin>3000.905420000000049</ogr:xmin>
      <ogr:ymin>3942.966620000000148</ogr:ymin>
      <ogr:Zusatz1>k</ogr:Zusatz1>
      <ogr:Zusatz2>6</ogr:Zusatz2>
      <ogr:Zusatz3>a</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.226">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3010.98643628953,3942.76781340632 3005.68479725022,3942.96662487029 3005.83935852232,3947.98986621354 3011.16563721499,3947.68894651338 3010.98643628953,3942.76781340632</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.k.6.b</ogr:Car>
      <ogr:xmin>3005.684789999999794</ogr:xmin>
      <ogr:ymin>3942.767809999999827</ogr:ymin>
      <ogr:Zusatz1>k</ogr:Zusatz1>
      <ogr:Zusatz2>6</ogr:Zusatz2>
      <ogr:Zusatz3>b</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.227">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3000.90542433081,3943.14585135477 3005.68479725022,3942.96662487029 3005.52927881521,3937.91227573259 3000.66910490211,3938.21094563787 3000.90542433081,3943.14585135477</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.k.6.c</ogr:Car>
      <ogr:xmin>3000.669100000000071</ogr:xmin>
      <ogr:ymin>3937.912269999999808</ogr:ymin>
      <ogr:Zusatz1>k</ogr:Zusatz1>
      <ogr:Zusatz2>6</ogr:Zusatz2>
      <ogr:Zusatz3>c</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.228">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3005.68479725022,3942.96662487029 3010.98643628953,3942.76781340632 3010.79783419594,3937.58850975948 3005.52927881521,3937.91227573259 3005.68479725022,3942.96662487029</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.k.6.d</ogr:Car>
      <ogr:xmin>3005.529269999999997</ogr:xmin>
      <ogr:ymin>3937.588510000000042</ogr:ymin>
      <ogr:Zusatz1>k</ogr:Zusatz1>
      <ogr:Zusatz2>6</ogr:Zusatz2>
      <ogr:Zusatz3>d</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.229">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3010.5337556507,3932.75225427219 3005.23211661139,3932.95106573617 3005.38667788349,3937.97430707941 3010.71295657616,3937.67338737926 3010.5337556507,3932.75225427219</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.k.7.b</ogr:Car>
      <ogr:xmin>3005.232109999999921</ogr:xmin>
      <ogr:ymin>3932.752250000000004</ogr:ymin>
      <ogr:Zusatz1>k</ogr:Zusatz1>
      <ogr:Zusatz2>7</ogr:Zusatz2>
      <ogr:Zusatz3>b</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.230">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3005.23211661139,3932.95106573617 3000.45274369198,3933.13029222065 3000.69739744204,3938.23923817779 3005.38667788349,3937.97430707941 3005.23211661139,3932.95106573617</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.k.7.a</ogr:Car>
      <ogr:xmin>3000.452740000000176</ogr:xmin>
      <ogr:ymin>3932.951059999999870</ogr:ymin>
      <ogr:Zusatz1>k</ogr:Zusatz1>
      <ogr:Zusatz2>7</ogr:Zusatz2>
      <ogr:Zusatz3>a</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.231">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3005.23211661139,3932.95106573617 3010.5337556507,3932.75225427219 3010.34515355711,3927.57295062535 3005.07659817638,3927.89671659847 3005.23211661139,3932.95106573617</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.k.7.d</ogr:Car>
      <ogr:xmin>3005.076590000000124</ogr:xmin>
      <ogr:ymin>3927.572950000000219</ogr:ymin>
      <ogr:Zusatz1>k</ogr:Zusatz1>
      <ogr:Zusatz2>7</ogr:Zusatz2>
      <ogr:Zusatz3>d</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.232">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3000.45274369198,3933.13029222065 3005.23211661139,3932.95106573617 3005.07659817638,3927.89671659847 3000.21642426328,3928.19538650374 3000.45274369198,3933.13029222065</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.k.7.c</ogr:Car>
      <ogr:xmin>3000.216420000000198</ogr:xmin>
      <ogr:ymin>3927.896709999999985</ogr:ymin>
      <ogr:Zusatz1>k</ogr:Zusatz1>
      <ogr:Zusatz2>7</ogr:Zusatz2>
      <ogr:Zusatz3>c</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.233">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3020.20543144268,3932.28882757767 3015.63087593198,3932.50438254938 3015.83775721659,3937.45824172574 3020.51632166084,3937.1924142005 3020.20543144268,3932.28882757767</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.K.20.a.7.b</ogr:Car>
      <ogr:xmin>3015.630869999999959</ogr:xmin>
      <ogr:ymin>3932.288820000000214</ogr:ymin>
      <ogr:Zusatz1>a</ogr:Zusatz1>
      <ogr:Zusatz2>7</ogr:Zusatz2>
      <ogr:Zusatz3>b</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.234">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3029.40396098092,3906.45954423105 3018.9747570248,3907.0745611379 3019.44120514362,3917.18958847217 3029.64451670092,3916.43688456004 3029.40396098092,3906.45954423105</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.K.20.b.9</ogr:Car>
      <ogr:xmin>3018.974749999999858</ogr:xmin>
      <ogr:ymin>3906.459539999999834</ogr:ymin>
      <ogr:Zusatz1>b</ogr:Zusatz1>
      <ogr:Zusatz2>9</ogr:Zusatz2>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.235">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3010.79565614669,3942.70744046719 3015.74151594923,3942.40655165791 3015.67278139098,3937.51265111027 3010.55734760657,3937.84314261882 3010.79565614669,3942.70744046719</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.K.20.a.6.c</ogr:Car>
      <ogr:xmin>3010.557339999999840</ogr:xmin>
      <ogr:ymin>3937.512650000000122</ogr:ymin>
      <ogr:Zusatz1>a</ogr:Zusatz1>
      <ogr:Zusatz2>6</ogr:Zusatz2>
      <ogr:Zusatz3>c</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.236">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3019.75275080385,3922.27326844355 3015.17819529315,3922.48882341525 3015.38507657776,3927.44268259161 3020.06364102201,3927.17685506637 3019.75275080385,3922.27326844355</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.K.20.a.8.b</ogr:Car>
      <ogr:xmin>3015.178190000000086</ogr:xmin>
      <ogr:ymin>3922.273259999999937</ogr:ymin>
      <ogr:Zusatz1>a</ogr:Zusatz1>
      <ogr:Zusatz2>8</ogr:Zusatz2>
      <ogr:Zusatz3>b</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.237">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3015.17819529315,3922.48882341525 3019.75275080385,3922.27326844355 3019.44120514362,3917.35934371173 3014.97304312774,3917.57636872394 3015.17819529315,3922.48882341525</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.K.20.a.8.d</ogr:Car>
      <ogr:xmin>3014.973039999999855</ogr:xmin>
      <ogr:ymin>3917.359339999999975</ogr:ymin>
      <ogr:Zusatz1>a</ogr:Zusatz1>
      <ogr:Zusatz2>8</ogr:Zusatz2>
      <ogr:Zusatz3>d</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.238">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3020.84206465998,3942.01584794763 3026.20690396918,3941.82521913969 3026.00713369924,3937.04163619365 3020.5729067407,3937.30558436021 3020.84206465998,3942.01584794763</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.K.20.b.6.c</ogr:Car>
      <ogr:xmin>3020.572900000000118</ogr:xmin>
      <ogr:ymin>3937.041630000000168</ogr:ymin>
      <ogr:Zusatz1>b</ogr:Zusatz1>
      <ogr:Zusatz2>6</ogr:Zusatz2>
      <ogr:Zusatz3>c</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.239">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2990.37558715574,3982.8267632604 2990.20315951493,3977.94005581045 2982.44220942486,3981.61403305821 2982.65555144,3988.26493912703 2987.28192852584,3985.6942343381 2987.16665827787,3982.81850349892 2990.37558715574,3982.8267632604</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>58</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>58.J.20.h.2</ogr:Car>
      <ogr:xmin>2982.442199999999957</ogr:xmin>
      <ogr:ymin>3977.940050000000156</ogr:ymin>
      <ogr:Zusatz1>h</ogr:Zusatz1>
      <ogr:Zusatz2>2</ogr:Zusatz2>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.240">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2995.72582319595,3943.78710852817 3001.02746223525,3943.5882970642 3000.83886014167,3938.40899341736 2995.57030476094,3938.73275939047 2995.72582319595,3943.78710852817</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>58</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>58.J.20.I.6.d</ogr:Car>
      <ogr:xmin>2995.570299999999861</ogr:xmin>
      <ogr:ymin>3938.408989999999903</ogr:ymin>
      <ogr:Zusatz1>I</ogr:Zusatz1>
      <ogr:Zusatz2>6</ogr:Zusatz2>
      <ogr:Zusatz3>d</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.241">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2980.92091203312,3948.90552573024 2989.80476957017,3948.90552573024 2985.84381398041,3939.22947707524 2980.52481647415,3939.68215771407 2980.92091203312,3948.90552573024</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>58</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>58.J.20.h.6</ogr:Car>
      <ogr:xmin>2980.524809999999889</ogr:xmin>
      <ogr:ymin>3939.229470000000219</ogr:ymin>
      <ogr:Zusatz1>h</ogr:Zusatz1>
      <ogr:Zusatz2>6</ogr:Zusatz2>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.242">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2986.12673937968,3956.37475627095 2990.93647116725,3956.31817119109 2990.82330100754,3952.52697084088 2986.12673937968,3952.64014100059 2986.12673937968,3956.37475627095</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>58</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>58.J.20.h.5.b</ogr:Car>
      <ogr:xmin>2986.126729999999952</ogr:xmin>
      <ogr:ymin>3952.526969999999892</ogr:ymin>
      <ogr:Zusatz1>h</ogr:Zusatz1>
      <ogr:Zusatz2>5</ogr:Zusatz2>
      <ogr:Zusatz3>b</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.243">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2981.14725235254,3956.54451151051 2986.07161727217,3956.45960866706 2986.12673937968,3952.64014100059 2990.78186125173,3952.58281712444 2990.65354576798,3949.01869588995 2980.86432695327,3949.35820636907 2981.14725235254,3956.54451151051</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>58</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>58.J.20.h.5</ogr:Car>
      <ogr:xmin>2980.864320000000134</ogr:xmin>
      <ogr:ymin>3949.018689999999879</ogr:ymin>
      <ogr:Zusatz1>h</ogr:Zusatz1>
      <ogr:Zusatz2>5</ogr:Zusatz2>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.244">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2981.38635251923,3962.72590951168 2985.84381398041,3962.4293598153 2985.84381398041,3956.54451151051 2981.09066727269,3956.65768167021 2981.38635251923,3962.72590951168</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>58</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>58.J.20.h.4.c</ogr:Car>
      <ogr:xmin>2981.090659999999843</ogr:xmin>
      <ogr:ymin>3956.544510000000173</ogr:ymin>
      <ogr:Zusatz1>h</ogr:Zusatz1>
      <ogr:Zusatz2>4</ogr:Zusatz2>
      <ogr:Zusatz3>c</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.245">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3001.02746223525,3943.5882970642 2995.72582319595,3943.78710852817 2995.88038446805,3948.81034987142 3001.20666316072,3948.50943017126 3001.02746223525,3943.5882970642</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>58</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>58.J.20.I.6.b</ogr:Car>
      <ogr:xmin>2995.725820000000112</ogr:xmin>
      <ogr:ymin>3943.588290000000143</ogr:ymin>
      <ogr:Zusatz1>I</ogr:Zusatz1>
      <ogr:Zusatz2>6</ogr:Zusatz2>
      <ogr:Zusatz3>b</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.246">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3007.6117079358,3992.23317826727 3007.83061073096,3987.25287256325 3002.92225117135,3987.5422172864 3002.97025197,3992.4805200546 3007.6117079358,3992.23317826727</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>58</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>58.J.20.k.1.c</ogr:Car>
      <ogr:xmin>3002.922250000000076</ogr:xmin>
      <ogr:ymin>3987.252869999999803</ogr:ymin>
      <ogr:Zusatz1>k</ogr:Zusatz1>
      <ogr:Zusatz2>1</ogr:Zusatz2>
      <ogr:Zusatz3>c</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.247">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2830.91386534064,3907.88134283622 2829.10314278532,3901.09113325376 2826.16071863292,3902.67551548967 2828.19778150766,3908.78670411388 2830.91386534064,3907.88134283622</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>70</ogr:Jahr>
      <ogr:RomischeZa>Stadttor I/19</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>70.Stadttor I/19</ogr:Car>
      <ogr:xmin>2826.160710000000108</ogr:xmin>
      <ogr:ymin>3901.091129999999794</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.248">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2945.32889680507,3904.37306788528 2943.40807717466,3907.07191033915 2946.55626722686,3909.66293054666 2948.38901095908,3906.51411263973 2945.32889680507,3904.37306788528</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>70</ogr:Jahr>
      <ogr:RomischeZa>II</ogr:RomischeZa>
      <ogr:ArabischeZ>3</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>70.II.3.Erweiterung</ogr:Car>
      <ogr:xmin>2943.408069999999952</ogr:xmin>
      <ogr:ymin>3904.373059999999896</ogr:ymin>
      <ogr:Zusatz1>Erweiterung</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.249">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2834.42214029158,3914.55838225897 2843.66122890652,3911.13316457995 2841.96366574531,3908.37532911622 2833.6726814746,3911.01230565718 2832.61141773626,3907.76817267651 2829.66899358386,3909.35255491242 2831.7060564586,3915.46374353663 2834.42214029158,3914.55838225897</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>72</ogr:Jahr>
      <ogr:RomischeZa>Stadttor I/19</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>72.Stadttor I/19</ogr:Car>
      <ogr:xmin>2829.668990000000122</ogr:xmin>
      <ogr:ymin>3907.768169999999827</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.250">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2943.40807717466,3907.07191033915 2953.47714830402,3916.31251973444 2969.54731098251,3899.67650625742 2957.55405346341,3889.36215372392 2943.40807717466,3907.07191033915</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>71</ogr:Jahr>
      <ogr:RomischeZa>XII</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>71.XII</ogr:Car>
      <ogr:xmin>2943.408069999999952</ogr:xmin>
      <ogr:ymin>3889.362149999999929</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.251">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2957.77761437291,3868.89422281694 2962.41759092092,3874.3263904829 2965.36001507332,3871.72347680963 2960.6068683656,3866.06496882425 2957.77761437291,3868.89422281694</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>71</ogr:Jahr>
      <ogr:RomischeZa>XI</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>71.XI</ogr:Car>
      <ogr:xmin>2957.777610000000095</ogr:xmin>
      <ogr:ymin>3866.064960000000156</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.252">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2921.33682294705,3930.79830017702 2925.86362933536,3935.89095736386 2930.72994620278,3930.45878969789 2924.85399944369,3926.20841083723 2921.33682294705,3930.79830017702</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>II</ogr:RomischeZa>
      <ogr:ArabischeZ>1</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>73.II.1.SO-Erweiterung</ogr:Car>
      <ogr:xmin>2921.336819999999989</ogr:xmin>
      <ogr:ymin>3926.208410000000185</ogr:ymin>
      <ogr:Zusatz1>SO-Erweiterung</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.253">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2907.32949119757,3934.79260990586 2912.21189893862,3939.54216301451 2917.73696761212,3933.99051566351 2911.9794033319,3929.64449289999 2907.32949119757,3934.79260990586</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>75</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>2</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>75.I.2</ogr:Car>
      <ogr:xmin>2907.329490000000078</ogr:xmin>
      <ogr:ymin>3929.644490000000133</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.254">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2962.60488185592,4003.83734442256 2967.39707794806,4000.11502299795 2960.83320868502,3986.87411431215 2954.83519022051,3990.15604894368 2962.60488185592,4003.83734442256</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>75</ogr:Jahr>
      <ogr:RomischeZa>Stadttor J/21</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>75.Stadttor J/21</ogr:Car>
      <ogr:xmin>2954.835189999999784</ogr:xmin>
      <ogr:ymin>3986.874110000000201</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.255">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2842.36960695055,3898.04921240465 2852.53529575241,3900.57546940862 2859.99859638551,3895.26287002882 2855.35861983749,3886.77510805075 2840.98600955462,3889.94387252256 2842.36960695055,3898.04921240465</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>75</ogr:Jahr>
      <ogr:RomischeZa>Stadttor I/19</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>75.Stadttor I/19</ogr:Car>
      <ogr:xmin>2840.986010000000078</ogr:xmin>
      <ogr:ymin>3886.775099999999838</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.256">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2853.77423760159,3912.46473430438 2864.2990624544,3924.00809059456 2869.11302699205,3918.1012978429 2860.45127702434,3909.29596983257 2853.77423760159,3912.46473430438</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>75</ogr:Jahr>
      <ogr:RomischeZa>Stadtmauer</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>75.Stadtmauer</ogr:Car>
      <ogr:xmin>2853.774229999999989</ogr:xmin>
      <ogr:ymin>3909.295970000000125</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.257">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2901.47545991836,3932.26951225322 2897.17499384947,3938.26753071772 2918.11147339538,3954.56403371562 2922.8646201031,3948.90552573024 2901.47545991836,3932.26951225322</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>76</ogr:Jahr>
      <ogr:RomischeZa>Stadtmauer</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>76.Stadtmauer</ogr:Car>
      <ogr:xmin>2897.174989999999980</ogr:xmin>
      <ogr:ymin>3932.269510000000082</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.258">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2949.28985239484,3944.71822982106 2946.46059840215,3946.8684628555 2955.17470069963,3953.09282163942 2956.30640229671,3949.13186604966 2949.28985239484,3944.71822982106</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>76</ogr:Jahr>
      <ogr:RomischeZa>II</ogr:RomischeZa>
      <ogr:ArabischeZ>1</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>76.II.1</ogr:Car>
      <ogr:xmin>2946.460590000000138</ogr:xmin>
      <ogr:ymin>3944.718229999999949</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.259">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2916.69684639904,3942.68116694632 2922.80803502325,3947.66065397346 2923.93973662033,3946.18944189726 2918.62073911407,3940.75727423129 2916.69684639904,3942.68116694632</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>76</ogr:Jahr>
      <ogr:RomischeZa>I</ogr:RomischeZa>
      <ogr:ArabischeZ>0</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>76.I.0</ogr:Car>
      <ogr:xmin>2916.696840000000066</ogr:xmin>
      <ogr:ymin>3940.757270000000062</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.260">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2935.82260338963,4020.71199206474 2939.33087834056,4020.42906666547 2940.91526057647,4013.86519740243 2937.29381546583,4014.09153772184 2935.82260338963,4020.71199206474</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>78</ogr:Jahr>
      <ogr:RomischeZa>Stadttor J/21</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>78.Stadttor J/21</ogr:Car>
      <ogr:xmin>2935.822599999999966</ogr:xmin>
      <ogr:ymin>4013.865189999999984</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.261">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2965.42602816799,3878.32577127022 2968.54657102468,3873.83999091373 2970.20435941729,3863.89326055804 2969.42422370312,3862.43050609397 2968.90368835489,3858.10668144208 2961.33031566859,3865.35601502211 2967.27885048915,3871.2070328784 2962.98810406121,3875.49777930634 2965.42602816799,3878.32577127022</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>19</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>73.J.19.VorTempelterasse</ogr:Car>
      <ogr:xmin>2961.330309999999827</ogr:xmin>
      <ogr:ymin>3858.106679999999869</ogr:ymin>
      <ogr:Zusatz1>VorTempelterasse</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.262">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2938.80389692189,3885.34699269776 2948.45131365597,3897.87514259819 2952.37990678663,3894.21820883055 2943.88934579768,3881.27216826859 2938.80389692189,3885.34699269776</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>73</ogr:Jahr>
      <ogr:RomischeZa>Nordwestlich</ogr:RomischeZa>
      <ogr:ArabischeZ>Hause</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>73.Nordwestlich.Hause.5</ogr:Car>
      <ogr:xmin>2938.803890000000138</ogr:xmin>
      <ogr:ymin>3881.272159999999985</ogr:ymin>
      <ogr:Zusatz1>5</ogr:Zusatz1>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.263">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2913.35196924704,3939.95649268977 2915.44858397888,3942.0531074216 2917.74023263926,3940.05400965404 2915.30230853247,3937.8111194758 2913.35196924704,3939.95649268977</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>75</ogr:Jahr>
      <ogr:RomischeZa>Nordwestlich</ogr:RomischeZa>
      <ogr:ArabischeZ>H13</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>75.Nordwestlich.H13</ogr:Car>
      <ogr:xmin>2913.351959999999963</ogr:xmin>
      <ogr:ymin>3937.811110000000099</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.264">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2932.1239848693,3890.75918421482 2944.40177136473,3903.11801040912 2947.23911433137,3897.78040564237 2938.17003665413,3886.17588689407 2932.1239848693,3890.75918421482</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>75</ogr:Jahr>
      <ogr:RomischeZa>G14</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>75.G14</ogr:Car>
      <ogr:xmin>2932.123979999999847</ogr:xmin>
      <ogr:ymin>3886.175879999999779</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.265">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2874.84850857792,3920.13964882855 2880.23000124892,3926.31907631974 2890.48424112539,3915.82104403259 2883.56053666212,3909.87250921203 2874.84850857792,3920.13964882855</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>75</ogr:Jahr>
      <ogr:RomischeZa>H18</ogr:RomischeZa>
      <ogr:ArabischeZ>R5</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>75.H18.R5</ogr:Car>
      <ogr:xmin>2877.221930000000157</ogr:xmin>
      <ogr:ymin>3909.872499999999945</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.266">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2947.84859535806,3932.60615150781 2950.82286276834,3931.92353275791 2947.50728598311,3925.58493008027 2944.92308642992,3926.51134124084 2947.84859535806,3932.60615150781</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>75</ogr:Jahr>
      <ogr:RomischeZa>H13</ogr:RomischeZa>
      <ogr:ArabischeZ>R11</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>75.H13.R11</ogr:Car>
      <ogr:xmin>2944.923080000000027</ogr:xmin>
      <ogr:ymin>3925.584929999999986</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.267">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2942.94836790342,3933.80073432013 2946.16642772438,3933.04497784703 2943.41157348371,3926.73075441046 2940.33978910916,3927.53526936569 2942.94836790342,3933.80073432013</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>75</ogr:Jahr>
      <ogr:RomischeZa>H13</ogr:RomischeZa>
      <ogr:ArabischeZ>R9</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>75.H13.R9</ogr:Car>
      <ogr:xmin>2940.339779999999791</ogr:xmin>
      <ogr:ymin>3926.730750000000171</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.268">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2931.97770942289,3928.93707572709 2935.0007353153,3935.47071233328 2937.29238397568,3934.93436902979 2934.85445986889,3928.10818153079 2931.97770942289,3928.93707572709</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>75</ogr:Jahr>
      <ogr:RomischeZa>H13</ogr:RomischeZa>
      <ogr:ArabischeZ>R5</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>75.H13.R5</ogr:Car>
      <ogr:xmin>2931.977699999999913</ogr:xmin>
      <ogr:ymin>3928.108180000000175</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.269">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2961.05669117282,3934.04979960354 2962.37845208502,3933.44136997728 2964.9590329136,3939.5256662398 2967.05706610758,3938.14096433178 2962.71413739605,3928.40609031175 2958.95865797884,3930.23137919051 2961.05669117282,3934.04979960354</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>76</ogr:Jahr>
      <ogr:RomischeZa>H13</ogr:RomischeZa>
      <ogr:ArabischeZ>R14</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>76.H13.R14</ogr:Car>
      <ogr:xmin>2958.958650000000034</ogr:xmin>
      <ogr:ymin>3928.406089999999949</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.270">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2925.59993019468,3934.34352425069 2928.43227500655,3933.58823230086 2926.90071077495,3929.95863487529 2924.1313069589,3930.67196616124 2925.59993019468,3934.34352425069</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>76</ogr:Jahr>
      <ogr:RomischeZa>H13</ogr:RomischeZa>
      <ogr:ArabischeZ>R4</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>76.H13.R4</ogr:Car>
      <ogr:xmin>2924.131300000000010</ogr:xmin>
      <ogr:ymin>3929.958630000000085</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.271">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3001.75853513588,3904.29968891301 3009.10172188395,3900.60020417101 3000.68912012617,3883.61200813659 2992.27542509912,3888.10287265554 3001.75853513588,3904.29968891301</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>77</ogr:Jahr>
      <ogr:RomischeZa>H42</ogr:RomischeZa>
      <ogr:ArabischeZ xsi:nil="true"/>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>77.H42</ogr:Car>
      <ogr:xmin>2992.275419999999940</ogr:xmin>
      <ogr:ymin>3883.612000000000080</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.272">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3007.73758992744,3987.0300559462 3007.64302422538,3982.095708769 3002.71244785195,3982.31811463339 3002.73946831866,3987.28788789949 3007.73758992744,3987.0300559462</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.k.2.a</ogr:Car>
      <ogr:xmin>3002.712439999999788</ogr:xmin>
      <ogr:ymin>3982.095699999999852</ogr:ymin>
      <ogr:Zusatz1>k</ogr:Zusatz1>
      <ogr:Zusatz2>2</ogr:Zusatz2>
      <ogr:Zusatz3>a</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.273">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3012.3770257593,3986.80265886558 3012.28893126817,3981.9028879186 3007.6639439255,3982.11650892797 3007.71180698839,3987.02563375025 3012.3770257593,3986.80265886558</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>J</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.J.20.k.2.b</ogr:Car>
      <ogr:xmin>3007.663939999999911</ogr:xmin>
      <ogr:ymin>3981.902880000000096</ogr:ymin>
      <ogr:Zusatz1>k</ogr:Zusatz1>
      <ogr:Zusatz2>2</ogr:Zusatz2>
      <ogr:Zusatz3>b</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.274">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>2936.27867747055,3928.16080344532 2938.65131307283,3934.65247938763 2941.5723716914,3934.05319508832 2939.13444758461,3927.54171256841 2936.27867747055,3928.16080344532</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>75</ogr:Jahr>
      <ogr:RomischeZa>H13</ogr:RomischeZa>
      <ogr:ArabischeZ>R7</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>75.H13.R7</ogr:Car>
      <ogr:xmin>2936.278670000000147</ogr:xmin>
      <ogr:ymin>3927.541709999999966</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.275">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3030.22112144949,3931.6984024299 3034.79624772116,3931.53583449131 3034.48951319542,3926.69779447166 3030.02135117954,3926.91481948386 3030.22112144949,3931.6984024299</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id xsi:nil="true"/>
      <ogr:Jahr>57</ogr:Jahr>
      <ogr:RomischeZa>K</ogr:RomischeZa>
      <ogr:ArabischeZ>20</ogr:ArabischeZ>
      <ogr:Zusammense xsi:nil="true"/>
      <ogr:Car>57.K.20.c.7.c</ogr:Car>
      <ogr:xmin>3030.021349999999984</ogr:xmin>
      <ogr:ymin>3926.697790000000168</ogr:ymin>
      <ogr:Zusatz1>c</ogr:Zusatz1>
      <ogr:Zusatz2>7</ogr:Zusatz2>
      <ogr:Zusatz3>c</ogr:Zusatz3>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:WTer_Grabungskanten fid="WTer_Grabungskanten.276">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>3005.59542412553,3893.20624664425 2998.45197826383,3897.18350721511 3001.83478992071,3904.20069094688 3009.15810028701,3900.71909296665 3005.59542412553,3893.20624664425</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:id>121</ogr:id>
      <ogr:Jahr>77</ogr:Jahr>
      <ogr:RomischeZa>II</ogr:RomischeZa>
      <ogr:ArabischeZ>4</ogr:ArabischeZ>
      <ogr:Zusammense>77/III-4</ogr:Zusammense>
      <ogr:Car>77.II.4</ogr:Car>
      <ogr:xmin>2998.451970000000074</ogr:xmin>
      <ogr:ymin>3893.206239999999980</ogr:ymin>
      <ogr:Zusatz1 xsi:nil="true"/>
      <ogr:Zusatz2 xsi:nil="true"/>
      <ogr:Zusatz3 xsi:nil="true"/>
    </ogr:WTer_Grabungskanten>
  </gml:featureMember>
</ogr:FeatureCollection>
