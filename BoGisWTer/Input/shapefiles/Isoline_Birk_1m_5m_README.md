 # Description of the data from the shapefile  
 
  This is a shapefile of isoline, documenting the topography 
 There is no height associated with the lines 
 
 
_U_s_a_g_e:

     data("objet")
     
_F_o_r_m_a_t:

     The format is: int [1:14864] 92445 92446 92447 92448 92449 92450
     92451 92452 92453 92454 ...

_E_x_a_m_p_l_e_s:

     data(objet)
     ## maybe str(objet) ; plot(objet) ...
     
