 # Description of the data from the shapefile  
 
  This is a shapefile of polygons, documenting the shape of different zone 
 
 
_U_s_a_g_e:

     data("objet")
     
_F_o_r_m_a_t:

     A data frame with 32 observations on the following 10 variables.

     ‘FID’ a factor with levels ‘’

     ‘ID’ a numeric vector

     ‘ART’ a factor with levels ‘zone’

     ‘TYPE’ a factor with levels ‘flach’ ‘hang’ ‘steil’

     ‘NAME’ a factor with levels ‘Buyukkale’ ‘Buyukkaya-OP-MP’
          ‘Buyukkaya-Silo-11’ ‘Buyukkaya-Silo-8’ ‘Buyukkaya-Silo-9’
          ‘Buyukkaya-UP’ ‘Haus am Hang - Altbau’ ‘Haus am Hang -
          Kesikkaya’ ‘Haus am Hang - Pithoshaus’ ‘Kesikkaya-Haus am
          Hang’ ‘Kesikkaya-Sudwest’ ‘Komplex-1’ ‘Komplex-2’ ‘Komplex-3’
          ‘Nordstadt’ ‘Nordwesthang’ ‘Nordwesthang-MP’
          ‘Nordwesthang-Silo’ ‘Nordwestterrasse’ ‘Quellgrotte’
          ‘Tempel-1’ ‘Tempel-1-Magazine’ ‘Unterstadt’ ‘Unterstadt-Hang’
          ‘Unterstadt-Terrasse’ ‘UST-Hang’ ‘UST-Terrasse’

     ‘PERIODE’ a factor with levels ‘LBA’ ‘MBA’

     ‘PHASE’ a factor with levels ‘BKA-10’ ‘MTER-LBA-1’ ‘MTER-LBA-1a’
          ‘MTER-LBA-2b’ ‘NWH-6’

     ‘LAGE’ a factor with levels ‘BK’ ‘BKA’ ‘HAH’ ‘KNW’ ‘KSW’ ‘MTER’
          ‘NST’ ‘NWH’ ‘NWTE’ ‘UST’

     ‘STAND’ a factor with levels ‘’

     ‘KENNUNG’ a factor with levels ‘’

_E_x_a_m_p_l_e_s:

     data(objet)
     ## maybe str(objet) ; plot(objet) ...
     
