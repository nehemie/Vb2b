 # Description of the data from the shapefile  
 
  This is a shapefile of lines, go to the category Art below to see 
 which kinds are documented in this file 
 
 
_U_s_a_g_e:

     data("objet")
     
_F_o_r_m_a_t:

     A data frame with 607 observations on the following 9 variables.

     ‘Id’ a factor with levels ‘0’ ‘1’ ‘10’ ‘104880’ ‘106344’ ‘106369’
          ‘106458’ ‘106464’ ‘106486’ ‘106487’ ‘106488’ ‘106498’
          ‘106583’ ‘106585’ ‘106588’ ‘106589’ ‘106590’ ‘106591’
          ‘106593’ ‘106594’ ‘106595’ ‘106596’ ‘106597’ ‘106598’
          ‘106599’ ‘106600’ ‘106601’ ‘106602’ ‘106603’ ‘106605’
          ‘106606’ ‘11’ ‘12’ ‘13’ ‘14’ ‘15’ ‘16’ ‘17’ ‘18’ ‘19’ ‘2’
          ‘20’ ‘21’ ‘22’ ‘23’ ‘24’ ‘25’ ‘26’ ‘27’ ‘28’ ‘29’ ‘3’ ‘30’
          ‘31’ ‘32’ ‘33’ ‘38’ ‘4’ ‘41’ ‘44’ ‘45’ ‘46’ ‘47’ ‘48’ ‘49’
          ‘5’ ‘50’ ‘51’ ‘6’ ‘698’ ‘7’ ‘8’ ‘81253’ ‘81311’ ‘81324’ ‘9’
          ‘99874’

     ‘Kennung’ a factor with levels ‘BK-80’ ‘BK-Mauer’ ‘Kanal-Geb13’
          ‘Kanal-Geb25’ ‘Kanal-Geb28-Geb27’ ‘KNW’ ‘KNW-1Sondage’
          ‘KNW-Poternenmauer’ ‘KSW’ ‘NWH-HaH’ ‘NWH-Pithosgebaeude’
          ‘NWter’ ‘Poternenmauer-BKA’ ‘Schnittkante’ ‘Schutzzone’
          ‘Strasse’ ‘Tempel-u-Komplexe’ ‘Tempelstrasse’
          ‘Ust-Grabungsareal’ ‘Westterrassestrasse’
          ‘Westterrassestrasse-alt’

     ‘Art’ a factor with levels ‘Bach’ ‘Schnittkante’ ‘Schutzzone’
          ‘Stadtmauer’ ‘Verbindungsweg’ ‘Wasserleitung’

     ‘Type’ a factor with levels ‘Keramik’ ‘large’ ‘normal’
          ‘Schutzzone’ ‘small’ ‘Stein’ ‘tiny’

     ‘Name’ a factor with levels ‘1938-1957’ ‘1955’ ‘1960-1963’ ‘1964’
          ‘1967-1969’ ‘1970-1978’ ‘2009’ ‘2009-2010’ ‘2009-2013’
          ‘2010-2014’ ‘Abschnittsmauer’ ‘Aussenwaende’
          ‘ByzantynischeWasserLeitung’ ‘GrobRekonstruiert’
          ‘Innenwaende’ ‘Kanal’ ‘Kanal-1’ ‘Kanal-10’ ‘Kanal-11’
          ‘Kanal-14’ ‘Kanal-17’ ‘Kanal-2’ ‘Kanal-3’ ‘Kanal-4’ ‘Kanal-5’
          ‘Kanal-6’ ‘Kanal-7’ ‘Kanal-BK’ ‘Kanal-Feldaufnahme70’
          ‘Kanal-Gebaude-1’ ‘Kanal-Gebaude-15’ ‘Kanal-Gebaude-28’
          ‘Kanal-Gebaude-35’ ‘Kanal-KomplexI’ ‘Kanal-TempelI-ost’
          ‘Kanal-Tempelstrasse’ ‘Kanal15’ ‘NordlicheBefestigung’
          ‘Poternenmauer’ ‘Schutzzone’ ‘Strasse’

     ‘Stand’ a factor with levels ‘grob vermutet’ ‘nachgewiesen’
          ‘vermutet’

     ‘Periode’ a factor with levels ‘BYZ’ ‘LBA’ ‘MBA’

     ‘Phase’ a factor with levels ‘AM-1’ ‘BK-IVd’ ‘LBA’ ‘MTER-LBA-1’
          ‘NOV-MBA-1’ ‘NWH-8a’ ‘NWTER-LBA’ ‘OS-?’ ‘PM-1’ ‘PM-2’
          ‘WTER-BYZ’ ‘WTER-LBA-2’ ‘WTER-LBA-3’ ‘WTER-MBA-1’

     ‘Lage’ a factor with levels ‘BK’ ‘BKA’ ‘BO’ ‘KNW’ ‘MTer’ ‘MTER’
          ‘NoV’ ‘NWH’ ‘NWter’ ‘OS’ ‘US’ ‘WTer’

_E_x_a_m_p_l_e_s:

     data(objet)
     ## maybe str(objet) ; plot(objet) ...
     
