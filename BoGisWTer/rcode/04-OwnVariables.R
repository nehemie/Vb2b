#projection
projection <- c("+proj=utm +zone=36 +datum=WGS84 +units=m +no_defs +ellps=WGS84 +towgs84=0,0,0")

# Define palette
palette(RColorBrewer:::brewer.pal(4,"Set1"))

## Defin colors
colbkg   <- "grey70" # Background
collines <- "grey70"  # Lines
coliso <- "grey70" # contour line / isolines
white_t  <- "#FFFFFF00" # transparent white
pal_trans <- RColorBrewer:::brewer.pal(6,"YlOrRd") # a transparent palette
  # Add White (#FFFFFF) at the beginning for 'transparency'
  pal_trans = append(pal_trans, "#FFFFFF", after = 0)
# Complete transparency for first colors and then
pal_trans[1:2] = paste(pal_trans[1:2],"10",sep = "")
pal_trans[3:4] = paste(pal_trans[3:4],"60",sep = "")
pal_trans[5:6] = paste(pal_trans[5:6],"90",sep = "")

# Colour for Strasse (str)
# n means nachgewiesen
# v means vermutet
strn1 <- "#cc4c02" # Strasse1
strv1 <- "#cc4c024d" # 4D means 30% transparency
strn2 <- "#fe9929"
strv2 <- "#fe99294d"
strn3 <- "#fed98e"
strv3 <- "#fed98e4d"


## xlim ylim
xlimi = c(2850,3100)
ylimi = c(3820,4095)

## Phase
periode_karum <- "Karum"

phase17 <- c("WTER-17Jh_b","WTER-17Jh_a", "KNW-Hittite-4", "NWH-7",
             "BKA-12", "BK-IVc2-IVc5")

phase16  <- c("WTER-16Jh-15Jh_a","WTER-16Jh-15Jh_b","WTER-16Jh-15Jh_b",
              "WTER-16Jh-15Jh_c", "NOV-16Jh-15Jh_a", "NOV-16Jh-15Jh_b", "NOV-16Jh-15Jh_c",
              "NWH-6", "KNW-Hittite-3", "KNW-Hittite-2a","KNW-Hittite-2b",
              "KSW-Hittite", "BKA-10", "BKA-11", "BK-IVc1")

phase15 <-  c("WTER-16Jh-15Jh_a","WTER-16Jh-15Jh_b","WTER-16Jh-15Jh_b", "WTER-16Jh-15Jh_c",
                    "NOV-16Jh-15Jh_a", "NOV-16Jh-15Jh_b", "NOV-16Jh-15Jh_c",
                    "KNW-Hittite-2a","KNW-Hittite-2b",
                    "KSW-Hittite",
                    "BK-IVa","BK-IVb1","BK-IVb2","BK-IVb3")

phase14 <-  c("WTER-14Jh-13Jh_a","WTER-14Jh-13Jh_b",  "NOV-14Jh-13Jh", "NOV-14Jh-13Jh_a", "NWH-5",
              "KNW-Hittite-1", "BKA-9", "BK-III")

phase14ust <-  c(phase14, # WTER+KNW
                 "AM-1", # Abschnittsmauer
                 "BK-IVc1", "BK-IVb2", #Buyukkale
                 "MTER-Hittite-2b", "MTER-Hittite-1", "MTER-Hittite-2a", "MTER-Hittite-1a",#MTER
                 "KNW-Hittite-3",
                 "PM-1", "PM-2") # Poternenmauer
