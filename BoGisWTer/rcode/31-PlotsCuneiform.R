# Plots-Repartion Tablettes cunéiformes Density ----
### xlim ylim
xlimi <- c(2835, 3100)
ylimi <- c(3820, 4095)
pnts <- cbind(x = c(2840, 2850, 2850, 2840),
              y = c(4000, 4085, 4000, 4085))
title_coordinate <- c(2975, 4100)

## Select the data
subspdf <- spdf[spdf$Objet == "Tablette cunéiforme", ]
subspdf@data <- droplevels(subspdf@data)

DensPA   <- density(unmark(sSp[sSp$marks$Objet %in% c("Tablette cunéiforme") &
                           sSp$marks$Datation %in% c("PA")]), adjust = 0.1)
DensHAHM <- density(unmark(sSp[sSp$marks$Objet %in% c("Tablette cunéiforme") &
                           sSp$marks$Datation %in% c("HA", "HM")]), adjust = 0.1)
DensHR   <- density(unmark(sSp[sSp$marks$Objet %in% c("Tablette cunéiforme") &
                           sSp$marks$Datation %in% c("HR")]), adjust = 0.1)
DensInc  <- density(unmark(sSp[sSp$marks$Objet %in% c("Tablette cunéiforme") &
                           sSp$marks$Datation %in% c("inconnue")]), adjust = 0.1)


# Export file
export_file("GIS-Tablette-Dens")
layout_4GIS_1Histo()
# 1st Plot
## Polygons
plot_background()
plot_gebaude_mba()

plot(DensPA,
     col = pal_trans,
     xlim = xlimi,
     ylim = ylimi,
     add = TRUE)

legend.gradient(pnts = pnts,
                cols = pal_trans,
                cex = 0.6,
                limits = c("", round(summary(DensPA)$max, digits = 2)),
                title = "")#"Estimation par noyau")

## Points
plot(subspdf[subspdf$Datation == "PA", ],
     add = TRUE,
     pch = c(21),
     cex = cex_points,
     lwd = 0.2,
      bg = "black",
     col = "black"
    )

text(3050, 4035,
     label = "Gebäude 72",
     cex = 0.7)

segments(2994, 3998, 3050, 4030)



text(title_coordinate[1], title_coordinate[2],
     label = "Paléo-assyrien", cex = 0.7)
axis(2, labels = FALSE)
axis(3, labels = FALSE)
box()

# 2nd Plot
## Polygons
plot_background()
plot_gebaude_wter_16()

## Points
plot(subspdf[subspdf$Datation %in% c("HA", "HM"), ],
     add = TRUE,
     pch = c(21),
     cex = cex_points,
     lwd = 0.2,
      bg = "black",
     col = "black"
    )

text(title_coordinate[1], title_coordinate[2],
     label = "Hittite ancien et moyen", cex = 0.7)
axis(3, labels = TRUE, cex.axis = 0.7, mgp = c(1, 0.3, 0))
axis(4, labels = TRUE, cex.axis = 0.7, mgp = c(1, 0.3, 0))
box()

# 3rd Plot
## Polygons
plot_background()
plot_gebaude_wter_14()

plot(DensHR,
     col = pal_trans,
     xlim = xlimi,
     ylim = ylimi,
     add = T)

legend.gradient(pnts = pnts,
                cols = pal_trans,
                cex = 0.6,
                limits = c("", round(summary(DensHR)$max, digits = 2)),
                title = "")#"Estimation par noyau")

## Points
plot(subspdf[subspdf$Datation == "HR", ],
     add = TRUE,
     pch = c(21),
     cex = cex_points,
     lwd = 0.2,
     bg = "black", #"black",
     col = "black"
)

text(title_coordinate[1], title_coordinate[2],
     label = "Hittite récent", cex = 0.7)
axis(1, labels = TRUE, cex.axis = 0.7, mgp = c(1, 0.3, 0))
axis(2, labels = TRUE, cex.axis = 0.7, mgp = c(1, 0.3, 0))
box()

# 4th Plot
## Polygons
plot_background()
plot_gebaude_wter_16()
plot(DensInc,
     col = pal_trans,
     xlim = xlimi,
     ylim = ylimi,
     add = T)

legend.gradient(pnts = pnts,
                cols = pal_trans,
                cex = 0.6,
                limits = c("", round(summary(DensInc)$max, digits = 2)),
                title = "")#"Estimation par noyau")



## Points
plot(subspdf[subspdf$Datation == "inconnue", ],
     add = TRUE,
     pch = c(22),
     cex = cex_points,
     lwd = 0.2,
     bg = "black",
     col = "black"
)

text(title_coordinate[1], title_coordinate[2],
     label = "Datation inconnue", cex = 0.7)
axis(1, labels = FALSE)
axis(4, labels = FALSE)
box()


## Scale arrow
    scarrow(xsca = 3035,
            ysca = 4080,
           ytext = 4087,
         cextext = 0.6,
            size = 50,
          xarrow = 3057,
          yarrow = 4085,
           scale = 10,
         arrowtype = 2)


#### Boxplot
## Margin
par(mar = c(1.2, 12, 2, 12) + 0.1)
## Boxplot
subspdf$Datation <- factor(subspdf$Datation, levels = c("PA", "HA", "HM",
                                                        "HR", "inconnue"))

bp <- barplot(rev(table(subspdf$Datation)),
              horiz = T,
              las = 1,
              #col= rev(c("#E41A1C", "#377EB8", "#377EB8", "#4DAF4A", "#984EA3")),
              cex.names = 0.7,
              cex.axis = 0.7,
              mgp = c(1, 0.3, 0),
              names.arg = rev(c("Paléo-assyrien (PA)", "Hittite ancien (HA)",
                                "Hittite moyen (HM)", "Hittite récent (HR)",
                                "Inconnue")
                              ),
              xlim = c(0, 150)
              )
#title(main="Tuyaux d'orgue des datation des \n tablette cunéiformes", cex.main = 1, font.main= 2)

text(x = rev(table(subspdf$Datation) + 9),
     labels = rev(as.character(table(subspdf$Datation))),
     y = bp,
     xpd = TRUE,
     cex = 0.6)

dev.off()
