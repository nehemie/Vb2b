# Load dwellings data ----
path_wter_dwelling = "../../BoGisWTer/Output/"
path_other_dwelling = "../../data/"

kultepe_houses <- read.delim(paste(path_other_dwelling,
                                   "Hertel2015a",
                                   ".tsv",
                                   sep = ""))

bo_karum_hauses <- read.delim(paste(path_wter_dwelling,
                                   "karum_dwelling",
                                   ".tsv",
                                   sep = ""))
rownames(bo_karum_hauses) <- bo_karum_hauses$X
bo_karum_hauses <- subset(bo_karum_hauses, select = -X )

bo_hittite_houses <- read.delim(paste(path_wter_dwelling,
                                   "hittite_dwelling",
                                   ".tsv",
                                   sep = ""))


# Make shapefile ----
weg_karum_nov <- subset(vector_poly_nonarchi,
       Art == "Weg" & Periode == "Karum" & Lage == "NOV")

mauer_karum_nov <- subset(vector_poly_archi,
                   Periode == "Karum" & Lage == "NOV")

schnitt_karum_nov <- subset(vector_poly_nonarchi,
       Art == "Schnittkante" & Periode == "Karum" & Lage == "NOV")

surface_karum_nov <- subset(
       vector_poly_surface, Name %in%
       c(karum_gebaude_number, karum_gebaude_unvollstandig, "Mauerreste") &
       Kennung == "total"
       )
crs(surface_karum_nov) <- ""



# functions ----
botleft_corner_axes <- function(){
  axis(1, labels = TRUE, cex.axis = 0.7, mgp = c(1, 0.3, 0))
  axis(2, labels = TRUE, cex.axis = 0.7, mgp = c(1, 0.3, 0))
  box()
}

output_path = "../../BoGisWTer/Output/"

export_file <- function(file_name, export_height = 6, export_width = 8){
  pdf(file = paste(output_path, file_name,".pdf", sep = ""),
      width = export_width,
      height = export_height,
      pointsize = 12)
  }


# Plot Dwelling Karum NOV ----
export_file("Dwelling-Karum-WTer", export_height = 3.5)
par(mar = c(1.5,1.2,0.2,0.2))
xlimi = c(2965,3065)
ylimi = c(3970,4010)

plot(schnitt_karum_nov,
     xlim = xlimi,
     ylim = ylimi,
     border = "black",
     lty = "longdash"
     )

plot(surface_karum_nov,
     xlim = xlimi,
     ylim = ylimi,
     col = "grey80",
     border = NA,
     add = TRUE
     )

plot(mauer_karum_nov,
     xlim = xlimi,
     ylim = ylimi,
      col = "black",
   border = "black",
      add = TRUE
     )


plot(weg_karum_nov,
     xlim = xlimi,
     ylim = ylimi,
     col = strn2,
     border = strn2,
     add = TRUE
     )

plot(schnitt_karum_nov,
     xlim = xlimi,
     ylim = ylimi,
     border = "black",
     lty = "longdash",
     add = TRUE)
## Scale arrow
    scarrow(xsca = 3050,
            ysca = 4005,
           ytext = 4006.5,
         cextext = 0.6,
            size = 10,
          xarrow = 3054,
          yarrow = 4006.5,
           scale = 4
         )

  botleft_corner_axes()
# Export End
dev.off()


# mba area NOV table ----
karum_area_nov <- sapply(c(schnitt_karum_nov,
                     weg_karum_nov,
                     surface_karum_nov,
                     mauer_karum_nov),gArea)
karum_area_nov[5] <- karum_area_nov[1] # Surface totale
karum_area_nov[1] <- (karum_area_nov[1] - sum(karum_area_nov[2:3])) # undefined space
karum_area_nov[4] <- (karum_area_nov[3] - karum_area_nov[4]) # Surface minus wall
karum_area_nov <- array(karum_area_nov,c(1,5))
karum_area_nov <- as.data.frame(karum_area_nov)
colnames(karum_area_nov) <- c("Indéfini","Rues","Emprise au sol", "Surface hors œuvre", "Surface totale")
karum_area_nov <- karum_area_nov[,c(3,4,2,1,5)]
karum_area_nov <- rbind(round(karum_area_nov,1), round((karum_area_nov/ karum_area_nov[,5])*100,1))

rownames(karum_area_nov) <- c("mètre carré","pourcentage")
write.table(karum_area_nov,
            file = "../Output/karum_area_nov.tsv",
            col.names = NA,
            sep = "\t",
            fileEncoding = "UTF-8")


# Calculate Karum Zone Surface ----
zone_mba <- subset(zone@data, select = NAME, PERIODE == 'Karum')
zone_mba <- as.vector(t(zone_mba))
karum_area_bo <- list(NULL)
for (i in zone_mba) {
  karum_area_bo[i] <- round(gArea(subset(zone, NAME == i & PERIODE == 'Karum')),1)
}
karum_area_bo <- as.data.frame(karum_area_bo[2:9])
m2hak2 <- function(x) c("mètre carré" = round(x,0),
                        "hectare" = round(x/10000,2),
                        "kilomètre carré" = round(x/1000000,3)
)

karum_area_bo <- apply(karum_area_bo,2,m2hak2)

colnames(karum_area_bo) <- zone_mba
rownames(karum_area_bo) <- c("mètre carré", "hectare", "kilomètre carré")
karum_area_bo <- karum_area_bo[,sort(colnames(karum_area_bo))]
write.table(karum_area_bo,
            file = "../Output/karum_area_bo.tsv",
            col.names = NA,
            sep = "\t",
            fileEncoding = "UTF-8")

# karum_area_bo/10000 #ha
# karum_area_bo <- round(karum_area_bo/1000000,3) #km2
karum_area_bo * 0.66 # Areas with houses 66 percents are houses
bo_num_houses <- karum_area_bo * 0.66 / 175 # A House = 175 m2
bo_num_houses <- karum_area_bo *  c(0.66,0.50,0.66,0.5,0.5,0.66,0.2,0.2) / 175
sum(bo_num_houses) * 8


# Plot Karum Zone BO ----
export_file("Dwelling-Karum-Zone-Bo", export_height = 7, export_width = 6)
par(mar = c(1.5,1.2,0.2,0.2))
## xlim ylim
xlimi <- c(2800,3670)
ylimi <- c(3300,4500)

plot_iso1m()
plot_bach()
#plot_zone_mba <- function(){
palette(paste(gray(c(1,4,8)/8),"85",sep = ""))
  plot(subset(zone, PERIODE == 'Karum'),
       xlim = xlimi,
       ylim = ylimi,
       col = as.factor(zone@data[zone@data$PERIODE %in% c('Karum'), "TYPE"]),
       lwd = 0.7,
       border = "#20202075",
       add = TRUE)
#}
#plot_zone_mba()
colbkg = "black"
collines = "black"
plot_gebaude_mba()

plot(schnittkante[schnittkante$Name != "1967-1969" &
                      schnittkante$Kennung != "KNW-Poternenmauer",],
     xlim = xlimi,
     ylim = ylimi,
     col = "black",
     lty = "longdash",
     add = T)

## Legend
scarrow(xsca = 3400,
        ysca = 4430,
       ytext = 4470,
     cextext = 1,
        size = 200,
      xarrow = 3490,
      yarrow = 4455,
       scale = 50)

text(x = c(3425), y = c(3420), cex = 0.7,
      paste(c("Büyükkale"),
            subset(karum_area_bo, select = Buyukkale)[1],
            sep = "\n")
     )

text(x = c(3100), y = c(3480), cex = 0.7,
      paste(c("Nordwesthang MP"),
            subset(karum_area_bo, select = 'Nordwesthang-MP')[1],
            sep = "\n")
     )

text(x = c(3220), y = c(3650), cex = 0.7,
      paste(c("Nordwesthang"),
            subset(karum_area_bo, select = Nordwesthang)[1],
            sep = "\n")
     )

text(x = c(3040), y = c(3680), cex = 0.7,
     paste(c("Kesikkaya \n Haus am Hang"),
           subset(karum_area_bo, select = 'Haus am Hang - Kesikkaya')[1],
           sep = "\n")
     )

text(x = c(3040), y = c(3810), cex = 0.7,
      paste(c("Unterstadt"),
            subset(karum_area_bo, select = 'UST-Terrasse')[1],
            sep = "\n")
     )

text(x = c(3220), y = c(3910), cex = 0.7,
      paste(c("Unterstadt Hang"),
            subset(karum_area_bo, select = 'UST-Hang')[1],
            sep = "\n")
     )

text(x = c(3600), y = c(4170), cex = 0.7,
      paste(c("Büyükkaya OP & MP"),
            subset(karum_area_bo, select = 'Buyukkaya-OP-MP')[1],
            sep = "\n")
     )

text(x = c(3650), y = c(4270), cex = 0.7,
      paste(c("Büyükkaya UP"),
            subset(karum_area_bo, select = 'Buyukkaya-UP')[1],
            sep = "\n")
     )


botleft_corner_axes()
dev.off()


# Box plot Karum Bo vs KUl ----

ks <- kultepe_houses$Surface
bs <- bo_karum_hauses$Emprise
data_surface_mba <- data.frame(surface = c(ks, bs),
                               name = c(rep("Kültepe", length(ks)),
                           rep("Bogazköy", length(bs)) ))

export_file("Dwelling-Houses-Karum-Bo-Kul", export_height = 3.5)
par(mar = c(2.5,5.5,0.5,0.5))
boxplot(surface ~ name, data = data_surface_mba,
        varwidth = TRUE, horizontal = TRUE,
        names = c("Bogazköy","Kültepe"), las = 1, ylim = c(0,300),
        ylab = NA)
stripchart(surface ~ name, data = data_surface_mba, vertical = FALSE,
           method = "jitter", pch = 20, col = 'blue', add = TRUE)
dev.off()


# Bo_Hittite allometric ----
export_file("Dwelling-Bo-Hittite-Emrpise_Surface", export_height = 6)
par(mar = c(2.7,2.9,0.5,0.5))
X <- log10(bo_hittite_houses$Emprise)
Y <- log10(bo_hittite_houses$Surface)
plot(Y ~ X,
     pch = 21, bg = "black", cex = 0.7, axes = FALSE)
reg1 <- lm(Y ~ X)
abline(reg1)
title(xlab = expression(paste(log[10], " Emprise en ", m^2)),
      ylab = expression(paste(log[10], " Surface en ", m^2)),
      line = 1.7)
text(X, Y, labels = bo_hittite_houses$X, pos = 3, cex = 0.7)
text(1.6131, 2.4, bquote(r^2 == .(round(summary(reg1)$r.squared,3))))
#

## Karum
#points(log(bo_karum_hauses$Emprise), log(bo_karum_hauses$Surface),
#       pch = 21, cex = 0.7)
#reg2 <- lm(log(bo_karum_hauses$Surface) ~ log(bo_karum_hauses$Emprise))
#abline(reg2, lty = "dashed")
#text(log(bo_karum_hauses$Emprise),
#     log(bo_karum_hauses$surface),
#     labels = rownames(bo_karum_hauses), pos = 3, cex = 0.7)

  axis(1, labels = TRUE, cex.axis = 0.7, mgp = c(0.2, 0.5, 0))
  axis(2, labels = TRUE, cex.axis = 0.7, mgp = c(0.2, 0.6, 0), las = 2)
  box()
 
dev.off()

cbind(bo_hittite_houses[order(-reg1$residuals), ],
      10 ^ reg1$fitted.value[order(-reg1$residuals)])
bo_hittite_houses[order(-reg1$residuals),"X"][1:7] # Top 7 +
bo_hittite_houses[order(-reg1$residuals),"X"][nrow(bo_hittite_houses) :
                                         (nrow(bo_hittite_houses) - 7)] # Top 5 -


# Make polygon for AREA WTER Hittite ----
poly_area_hittite_total <-
  as(limits_exca_poly[limits_exca_poly@data$Kennung %in%
                        c("Schnittkante--1970-1978",
                          "Schnittkante--1978",
                          "Schnittkante--1938-1957") &
                        limits_exca_poly@data$Periode == "Hittite"
                                            ,], "SpatialPolygons")
area_hittite_total <- gArea(poly_area_hittite_total)

# Abschniitsmauer
poly_area_hittite_Abschnittsmauer <- gIntersection(limits_exca_poly,
            archi_poly[archi_poly@data$Name %in% "Abschnittsmauer",]
)

# remove hole for area
NZ <- poly_area_hittite_Abschnittsmauer
NZp <- slot(NZ, "polygons")
holes <- lapply(NZp, function(x) sapply(slot(x, "Polygons"), slot, "hole"))
res <- lapply(1:length(NZp), function(i) slot(NZp[[i]], "Polygons")[!holes[[i]]])
IDs <- 1:length(res)
poly_area_hittite_Abschnittsmauer <-
  SpatialPolygons(lapply(1:length(res),
                         function(i) Polygons(res[[i]],
                                              ID = IDs[i])),
                  proj4string = CRS(proj4string(NZ)))
area_hittite_Abschnittsmauer <- gArea(poly_area_hittite_Abschnittsmauer)

# Weg
poly_area_hittite_weg <- gIntersection(limits_exca_poly,
                                   wege_poly[wege_poly$Periode ==
                                             "Hittite",])
area_hittite_weg <- gArea(poly_area_hittite_weg)

# Archi 1
poly_area_hittite_surface <- gIntersection(limits_exca_poly, vector_poly_emprise)
area_hittite_surface <- gArea(poly_area_hittite_surface)

# Archi 2
area_hittite_gebaude <- hittite_qdm_buildings_data[hittite_qdm_buildings_data$Name %in%
                                                   gsub("Gebaude-","",unique(gebaude_wter_16@data$Name)),]
area_hittite_gebaude <- sum(area_hittite_gebaude$Emprise)

# Archi plot
gebaude_surface <- archi_poly[archi_poly$Phase %in%
                    c("WTER-16Jh-15Jh_a","WTER-16Jh-15Jh_ab","WTER-16Jh-15Jh_b",
                      "WTER-16Jh-15Jh_c", "NOV-16Jh-15Jh_a",
                      "NOV-16Jh-15Jh_b", "NOV-16Jh-15Jh_c", "NWH-6",
                      "KNW-Hittite-3",
                      "KNW-Hittite-2a","KNW-Hittite-2b", "BK-IVc1"),]

# Indefini
areda_hittite_undef = area_hittite_total -
  (area_hittite_surface + area_hittite_weg + area_hittite_Abschnittsmauer)



# Plot Dwelling AREA WTER Hittite ----
export_file("Dwelling-WTer-Hittite", export_height = 6)
par(mar = c(1.5,1.2,0.2,0.6))
xlimi = c(2825,3090)
ylimi = c(3828,4025)

plot(poly_area_hittite_total,
     xlim = xlimi, ylim = ylimi,
     border = "black", lty = "longdash", cex = 1.2)
#plot(Iso1m, add = TRUE)

plot(poly_area_hittite_surface, add = TRUE, col = 'grey80')

plot(poly_area_hittite_weg, col = strn2, add = TRUE)
plot(poly_area_hittite_Abschnittsmauer, col = 'brown', add = TRUE)
plot(gebaude_surface, col = 'black', add = TRUE)

## Scale arrow
    scarrow(xsca = 3050,
            ysca = 4005,
           ytext = 4008,
         cextext = 0.6,
            size = 20,
          xarrow = 3058,
          yarrow = 4008.5,
           scale = 9
         )

  botleft_corner_axes()
# Export End
dev.off()


# hittite area WTERtable ----

hittite_area_wter <- data.frame(area_hittite_surface, # guessed
                   #area_hittite_gebaude, # addition from phase 2
                   area_hittite_weg,
                   area_hittite_Abschnittsmauer,
                   areda_hittite_undef,
                   area_hittite_total)


colnames(hittite_area_wter) <- c("Emprise des bâtiments", "des rues",
                            "des fortifications", "indéfini",
                            "Surface totale")
hittite_area_wter <- rbind(round(hittite_area_wter,1),
                           round((hittite_area_wter/ hittite_area_wter[,5])*100,1))

rownames(hittite_area_wter) <- c("mètre carré","pourcentage")
write.table(hittite_area_wter,
            file = "../Output/hittite_area_wter.tsv",
            col.names = NA,
            sep = "\t",
            fileEncoding = "UTF-8")


# Calculate Hittite Zone Surface ----
zone_hittite <- subset(zone@data, select = NAME, PERIODE == 'Hittite')
zone_hittite <- as.vector(t(zone_hittite))
hittite_area_bo <- list(NULL)
for (i in zone_hittite) {
  hittite_area_bo[i] <- round(gArea(subset(zone, NAME == i & PERIODE ==
                                       'Hittite')),1)
}
hittite_area_bo <- as.data.frame(hittite_area_bo[2:length(hittite_area_bo)])
hittite_area_bo <- apply(hittite_area_bo,2,m2hak2)
area_teich <- round(gArea(teich_poly),0)
hittite_area_bo <- cbind(hittite_area_bo,
                         data.frame("Teich.NWTER" = m2hak2(area_teich)))
colnames(hittite_area_bo) <- gsub("\\.", "-", colnames(hittite_area_bo))
hittite_area_bo <- hittite_area_bo[,sort(colnames(hittite_area_bo))]

write.table(hittite_area_bo,
            file = "../Output/hittite_area_bo.tsv",
            col.names = NA,
            sep = "\t",
            fileEncoding = "UTF-8")
m2hak2(round(gArea(subset(zone, NAME == "Unterstadt")),1))


# Plot Hittite Zone BO ----
export_file("Dwelling-Hittite-Zone-Bo", export_height = 7, export_width = 6)
par(mar = c(1.5,1.2,0.2,0.2))
## xlim ylim
xlimi <- c(2750,3670)
ylimi <- c(3300,4500)

plot_iso1m()
plot_bach()
plot_teich()

palette(paste(gray(c(1,4,8)/8),"85",sep = ""))
  plot(subset(zone, PERIODE %in% c('Hittite')),
       xlim = xlimi,
       ylim = ylimi,
       col = as.factor(zone@data[zone@data$PERIODE %in% c('Hittite'), "TYPE"]),
       lwd = 0.7,
       border = "#20202075",
       add = TRUE)

colbkg = "black"
collines = "black"

plot(archi_poly[archi_poly$Name %in%
                  c("Abschnittsmauer","Tempel-1", "Komplex-1",
                    "Komplex-2","Komplex-3", "Quellgrotte",
                    "Poternenmauer"),],
     xlim = xlimi,
     ylim = ylimi,
     col = "black",
     border = "black",
     add = TRUE
     )

plot(archi_poly_16e,
     xlim = xlimi,
     ylim = ylimi,
      col = "black",
      add = TRUE
     )

plot(vector_lines[vector_lines$Name %in% c("Poternenmauer","Abschnittsmauer"),],
      xlim = xlimi,
      ylim = ylimi,
      col = "black",
      lty = "dotted",
      add = TRUE
       )

plot(schnittkante[schnittkante$Periode == "Hittite",],
     xlim = xlimi,
     ylim = ylimi,
     col = "grey40",
     lty = "longdash",
     lwd = 0.5,
     add = TRUE
     )

## Legend
scarrow(xsca = 3400,
        ysca = 4430,
       ytext = 4470,
     cextext = 1,
        size = 200,
      xarrow = 3490,
      yarrow = 4455,
       scale = 50)

text(x = c(3425), y = c(3420), cex = 0.7,
      paste(c("Büyükkale"), hittite_area_bo$Buyukkale[1], sep = "\n"))

text(x = c(3120), y = c(3510), cex = 0.7,
      paste(c("Nordwesthang MP"), hittite_area_bo$'Nordwesthang-MP'[1], sep = "\n"))

text(x = c(3220), y = c(3630), cex = 0.7,
      paste(c("Nordwesthang"), hittite_area_bo$Nordwesthang[1], sep = "\n"))

text(x = c(3040), y = c(3680), cex = 0.7,
     paste(c("Kesikkaya \n Haus am Hang"), hittite_area_bo$'Kesikkaya-Haus-am-Hang'[1],
           sep = "\n") )

text(x = c(3010), y = c(4050), cex = 0.7,
      paste(c("Unterstadt"), hittite_area_bo$'Unterstadt-Terrasse'[1], sep = "\n"))

text(x = c(3215), y = c(3910), cex = 0.7,
      paste(c("Unterstadt Hang"), hittite_area_bo$'Unterstadt-Hang'[1], sep = "\n"))

text(x = c(3630), y = c(4150), cex = 0.7,
      paste(c("Büyükkaya \n OP & MP"), hittite_area_bo$'Buyukkaya-OP-MP'[1], sep = "\n"))

text(x = c(3685), y = c(4270), cex = 0.7,
      paste(c("Büyükkaya \n  UP"), hittite_area_bo$'Buyukkaya-UP'[1], sep = "\n"))

text(x = c(2870), y = c(4230), cex = 0.7,
      paste(c("Teich \n NWTER"), hittite_area_bo$'Teich-NWTER'[1], sep = "\n"))

text(x = c(3120), y = c(4440), cex = 0.7,
      paste(c("Nordstadt"), hittite_area_bo$'Nordstadt'[1], sep = "\n"))

text(x = c(2805), y = c(4020), cex = 0.7,
      paste(c("Nordwestterrasse"), hittite_area_bo$'Nordwestterrasse'[1], sep = "\n"))

botleft_corner_axes()
dev.off()
