La chronologie, l'ordonnance des faits dans le temps, est fondamentale
pour l'étude archéologique. Seul l'établissement de séquences
chronologiques permet d'ordonner le matériel archéologique et fournit
le cadre à toute recherche.  Chaque séquence (d'un même site ou de
différents sites) peut être relative ou absolue et leur mise en
relation permet de comparer les différents corpus. La chronologie
n'est pas une fin en soi, mais c'est uniquement sur la base d'une
bonne chronologie que les questions portant sur l'évolution d'un
corpus ont une chance de trouver une réponse solide. L'emploi de dates
calendaires, en général présentées grâce un intervalle de précision,
une «~fourchette de datation~», et l'établissement d'un cadre absolu
permettent de déterminer le rythme des changements culturels et la durée
des activités étudiées. Il est important de savoir non seulement à
quel moment une activité a eu lieu, mais aussi de savoir combien de
temps elle a duré. La temporalité des objets étudiés tout comme leur
individualité sont ainsi retrouvées, ce qui minimise le danger de
créer un passé éloigné, atemporel et simplificateur.

La datation absolue des vestiges selon des critères
\emph{archéologiques}, même élémentaires, a longtemps été un des
points faibles des fouilles de \bo{}. L'emploi systématique de
datations radiocarbones a été tardif et ce n'est qu'avec la reprise
des fouilles par \Seeher{} que les méthodes physico-chimiques
serviront de fondement aux datations des vestiges.  Jusqu'en 1994, le
cadre chronologique reposait sur les découvertes épigraphiques et sur
l'interprétation géopolitique des textes, postulant que les événements
les plus marquants relatés dans les textes devaient être visibles dans
les vestiges archéologiques\footnote{Pour une critique de cette
approche, voir \cite{Muller-Karpe2003, Schoop2003, Seeher2006b,
Seeher2008a}.} Les travaux des deux dernières décennies ont
indubitablement mis en évidence les problèmes posés par cette méthode
de datation, en particulier pour la la ville haute de \bo.  \Neve{}
avait considéré que, d'après les témoignages épigraphiques,
l'extension de la ville avait eu lieu au \xiiieme{} siècle et, donc,
que les trois phases architecturales de la ville haute (\oberstadt{}
1, 2, et 3) se répartissaient au long de ce
siècle\footnote{\cite{Seeher2006b} pour un historique de cette
datation.}. Néanmoins, les datations par radiocarbone ont montré que
la ville haute a été occupée dès le \xvieme{} siècle, que le
raisonnement employé pour dater les monuments était inconséquent,
l'argumentation, circulaire et, donc, que toute la chronologie
(absolue) était à revoir. Il va sans dire qu'un tel changement dans la
séquence chronologique a des répercussions importantes sur
l'interprétation du site : la construction de nombreux monuments n'a
pas eu lieu au \xiveme{}-\xiiieme{} siècle, mais bien avant. Cet état
rend la chronologie employée jusqu'à une date récente douteuse et de
nombreuses recherches sont encore à mener pour fournir un nouveau
cadre général pour les différents ensembles de la ville\footnote{La
séquence de la ville haute ne sera pas discutée. Voir
\cite[13--18]{Mielke2006a} pour une première proposition.}.

Pour dissiper le doute qui a été jeté sur la chronologie et établir
des points de repère stables, il convient de faire le point sur les
arguments proposés en reprenant les séquences établies
(\cref{31-Stratigraphie-Epigraphique}) avant de dégager des bases
solides (\cref{32-Nouvelle-Chrono}) afin d'établir un cadre
chronologique clair et d'en souligner ses limites
(\cref{34-Stratie-Chrono}).  Ce chapitre ouvre donc sur l'état actuel
de la recherche et expose la séquence stratigraphique de \bk{}
(\cref{311-Sequence-Buyukkale}), qui consitue la colonne vertébrale
de la chronologie, à laquelle ont été agrégées les séquences des
chantiers de \nwh{} (\cref{312-Sequence-NWH}) et de \nordV{}
(\cref{313-Sequence-NoV}). Cette sous-partie considère ensuite
l'architecture monumentale de la ville basse
(\cref{314-Archi-Munumental}) et enfin la \WTer{}
(\cref{315-Sequence-WTer}). Tour à tour sont repris les arguments qui
ont été proposés pour dater la ville basse et les principaux ensembles
l'environnant.  Dans un deuxième temps, j'expose la méthode que j'ai
développée pour obtenir un cadre chronologique minimal pour l'étude de
la \WTer. Celle-ci combine l'étude de la céramique
(\cref{321-Analyse-Ceramique}), la création d'un cadre chronologique à
l'aide d'une modélisation des datations radiocarbones
(\cref{322-Sequence-C14}) tout en intégrant les derniers résultats de
la recherche (\cref{323-Decouvertes-Recentes}). Ces trois thèmes sont
abordés successivement avant d'être réunis dans une dernière partie
qui fait le bilan (\cref{34-Stratie-Chrono}).
