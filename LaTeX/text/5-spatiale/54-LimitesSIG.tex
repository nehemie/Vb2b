L'analyse spatiale des objets repose sur l'étude de l'emplacement de
leur découverte. Lors des fouilles de la \WTer{}, chaque objet
découvert a été enregistré selon son carroyage\footnote{De nos jours,
une station totale permet aisément de relever les objets en 3
coordonnées absolues.}. Ainsi, chaque objet ne possède pas de
coordonnées absolues, mais est défini à l'intérieur d'un polygone.
Toutefois, une des caractéristiques des SIG est de ne pas pouvoir
attribuer de coordonnées imprécises à un objet par exemple, quelque
part dans le carré. Ainsi, un point doit, par définition, être
déterminé par un jeu de deux coordonnées précises\footnote{La
troisième dimension n'a pas pu être prise en compte dans cette étude.
Elle est par ailleurs encore difficile à analyser dans les SIG.}.


\subsection{Définir le carroyage}\label{Definir-carroyage}

Tout cela pourrait être assez anecdotique, s'il existait une carte
recensant tous les carreaux de fouille. Grâce au référencement du
carroyage dans un SIG, il serait assez facile d'attribuer des
coordonnées. Néanmoins, la procédure se complique, puisque cette carte
n'existe pas et qu'il faut donc l'élaborer a posteriori.

Le carroyage des années 1930 est inconnu, s'il a jamais existé pendant
les fouilles. Une carte du carroyage a été réalisée pour les fouilles
des années 1950, alors orientées vers le nord, puis en
1970\footnote{\cite[25 fig.  12]{Neve1975a}.}, en
1971\footnote{\cite[34 fig. 20]{Neve1975b}.}, sans numérotation en
1973\footnote{\cite[111]{Neve1975}.}, mais n'a pas été réalisée en
1975, 1976, 1977 et 1978. Ce n'est que par la lecture des carnets de
fouilles et grâce aux relevés à la main qu'il est possible de
reconstituer le carroyage tel que \Neve{} l'a «~conçu~» pour les
années 1975--1978
(\crefrange{ExcavationTrenches1}{ExcavationTrenches6}). La
délimitation de ces tranchées est problématique puisqu'elles ne sont
pas orientées vers le nord (\cref{222-Methode-Neve},
\cpageref{222-Methode-Neve}). Puisque la largeur des tranchées ou des
carreaux n'a pas toujours été strictement définie, le carroyage
proposé dans ce manuscrit reste approximatif. Néanmoins, la position
relative est en général assurée, seule la taille des carrés pourrait
varier de quelques mètres\footnote{C'est avant tout les
limites des carreaux de 1976 qui ont été les plus difficiles à
placer.}. Dans certains cas, les objets ont été enregistrés selon les
limites du sondage, en particulier pour la fouille en profondeur du
\gebZR. Si le matériel a été défini selon d'autres références, de
nouveaux polygones ont été créés comme pour \citfr{1976.H13.R5} à
l'origine défini comme pièce n°5 (\textit{Raum 5}) du \gebQE{}
(\textit{Hause 13})\footnote{À ce propos, voir les \gebQE, \gebZR,
\gebIR{} et \gebOR{}, puisqu'ils ont été renommés.}, mais aussi pour
les objets des fouilles des années 1930. Enfin, l'orientation exacte
de la tranchée B de 1956 est incertaine. Celle-ci est longue de 110
mètres et l'extrémité nord définie sur la carte pourrait bien dévier
de quelques mètres de l'emplacement original -- même si cela ne
changera rien à l'interprétation\footnote{De futures recherches
pourront corriger l'orientation, si des données exactes sont
relevées.}.


 \opt{optbw}{
   \begin{figure}[p!]
   \centering
     \includegraphics[width=160mm]{Fig_62}
     \caption{Dénomination des tranchées du \nordV}
     \label{ExcavationTrenches1}
   \end{figure}
 }

 \opt{optbw}{
   \begin{figure}[p!]
   \centering
     \includegraphics[width=160mm]{Fig_63}
     \caption{Dénomination des tranchées au nord-est de la \WTer}
     \label{ExcavationTrenches2}
   \end{figure}
 }

 \opt{optbw}{
   \begin{figure}[p!]
   \centering
     \includegraphics[width=160mm]{Fig_64}
     \caption{Dénomination des tranchées au nord-ouest de la \WTer}
     \label{ExcavationTrenches3}
   \end{figure}
 }

 \opt{optbw}{
   \begin{figure}[p!]
   \centering
     \includegraphics[width=160mm]{Fig_65}
     \caption{Dénomination des tranchées au sud-est de la \WTer}
     \label{ExcavationTrenches4}
   \end{figure}
 }


 \opt{optbw}{
   \begin{figure}[p!]
   \centering
     \includegraphics[width=160mm]{Fig_66}
     \caption{Dénomination des tranchées au sud de la \WTer}
     \label{ExcavationTrenches5}
   \end{figure}
 }

 \opt{optbw}{
   \begin{figure}[p!]
   \centering
     \includegraphics[width=160mm]{Fig_67}
     \caption{Dénomination des tranchées au sud-ouest de la \WTer}
     \label{ExcavationTrenches6}
   \end{figure}
 }


\FloatBarrier



\subsection{Attribution des coordonnées}

Pour attribuer un jeu de coordonnées à chaque point
(\cref{Fig-542-Attribution-Coordonnees-SIG}), j'ai tout d'abord créé les
polygones dans un SIG (a), ensuite j'ai composé un algorithme qui fonctionne de
la manière suivante. Il génère aléatoirement quatre cents points à l'intérieur
d'un carreau (b), sélectionne un nombre de points correspondant au nombre
d'objets à associer à ce carreau (c) et attribue ensuite les données du registre
à chaque point (d). Cette méthode suit les principes énoncés pour créer une
carte de distribution de points pour des informations collectées à partir d'une
prospection\footnote{\cite[272\psq]{Conolly2006a}.}.



\setlength{\fboxsep}{10pt}
\opt{optbw}{
  \begin{figure}[h!]
    \centering %
        \begin{subfigure}[b]{0.47\textwidth}
          \centering %
          \fbox{\includegraphics[width=0.70\textwidth]{Fig_68a}}
          \caption{Référencement du carreau dans le SIG}
        \end{subfigure}%
           ~
        \begin{subfigure}[b]{0.47\textwidth}
          \centering %
          \fbox{\includegraphics[width=0.70\textwidth]{Fig_68b}}
          \caption{Génération aléatoire de points dans le carreau}
          \end{subfigure}

          \begin{subfigure}[t]{0.47\textwidth}
          \centering %
          \fbox{\includegraphics[width=0.70\textwidth]{Fig_68c}}
          \caption{Sélection du nombre de points dans le carreau en fonction du
          nombre d'objets associés au carreau}
        \end{subfigure}%
       ~
        \begin{subfigure}[t]{0.47\textwidth}
          \centering %
          \fbox{\includegraphics[width=0.70\textwidth]{Fig_68d}}
          \caption{Association des données du registre des petits objets aux
          points sélectionnés}
        \end{subfigure}
       \caption{Méthode pour attribuer des coordonnées aux petits objets}
       \label{Fig-542-Attribution-Coordonnees-SIG}
    \end{figure}
  }


\subsection{Critique de la méthode}

Cette méthode ne peut pas être utilisée sans critique préalable à
cause de sa mise en œuvre. Elle repose sur plusieurs assomptions à
expliciter. Tout d'abord, si la qualité et la quantité des données
étaient très hétérogènes, il serait difficile, voire impossible, de
les comparer entre elles\footnote{Voir les remarques dans
  \cite{Terrenato2004}.}. Néanmoins, un diagramme de la surface
fouillée par rapport au nombre d'objets enregistrés indique une
corrélation plutôt linéaire entre ces deux variables
(\cref{Excavation-Objet_per_square_perYear}). En effet, d'une manière
générale, \num{0,25} objet par mètre carré a été enregistré. On
remarque toutefois que les années 1970 se situent au-dessus de cette
tendance, alors que les années 1930 et 1950 se situent en dessous.
Plus d'objets ont été enregistrés dans les années 1970, ce qui
témoigne d'une recherche légèrement plus soigneuse.  Seule l'année
1938 s'écarte fortement de cette tendance avec une moyenne de
\num{0,08} objet par mètre carré. De plus, par l'absence de
coordonnées associées aux objets pendant cette année de fouilles, les
objets sont répartis de manière aléatoire à l'intérieur du périmètre
de fouilles de 1938. Une grande prudence s'impose quant à
l'interprétation des données de ces années et il faudra faire
attention à ne pas interpréter abusivement un vide dans l'est des
fouilles du \NoV.

La répartition totale des objets retrouvés
(\cref{GeneralObjectDistribution}) ment en évidence des espaces vides -- telle
une carte de répartition comportant des régions qui n'auraient pas été
prospectées. Ceci concerne en particulier le \gebU{} fouillé en 1956
et les \gebEQ{} et \gebWI{} fouillés en 1975. Pour ces zones, il n'y a
simplement que très peu de données à disposition permettant de leur
attribuer du matériel. Dans toutes les cartes de répartition, ces
zones seront donc toujours assez vides, un reflet de la documentation
des fouilles archéologiques plus que de l'état au moment de la
fouille.

L'analyse spatiale se base avant tout sur l'analyse visuelle de la
répartition des points grâce à la création d'une série de cartes. À
l'aide d'un SIG, il est aisé de créer une série de cartes selon une
même thématique en ne faisant varier qu'une seule variable, comme par
exemple la chronologie. Uniquement deux séries d'objets se prêtent à
ce genre de répartition : les tablettes cunéiformes et les sceaux et
leurs empreintes
(\crefrange{GIS-Repartition-tablette-carte}{GIS-Repartition-sceau-carte}).
Seules ces deux catégories se laissent plus facilement dater grâce à
des sources externes. Pour les objets dont la classification
chronologique ne pouvait pas se faire grâce à une typo-chronologie,
comme par exemple pour les aiguilles, ce sont les classes d'objets qui
ont été cartographiées. Est-ce que les artefacts liés à la métallurgie
ou au travail du textile se retrouvent dans des lieux différents de la
ville ? Qu'en est-il des éléments de parure ou bien des figurines
zoomorphes et anthropomorphes ? Est-il possible de mettre en avant une
ségrégation de certains objets ? Pour aider à la lecture et à
l'analyse, une analyse statistique de la densité des points a été
menée grâce à une estimation par noyau et dont l'échelle a été placée
sur la gauche\footnote{\cite{Bevan2013b} pour une introduction. Tous
les details plus techniques sont à disposition avec le code source qui
permet de produire les cartes de répartition.}.

\vspace{1cm}
 \opt{optbw}{
   \begin{figure}[h!]
   \centering
     \includegraphics[width=160mm]{Fig_69}

     \caption{Diagramme de la surface fouillée par rapports au nombre d'objets
     retrouvés. En trait plein, la droite de régression orthogonale selon la
     méthode des moindres carrés. La valeur-$p$ du test de Fischer est
     inférieure à \num{4,291e-05} et les résidus satisfont les conditions
     requises. La droite de régression linéaire explique ici environ
     \SI{76.5}{\percent} de la variance ($r^2$)}

     \label{Excavation-Objet_per_square_perYear}
   \end{figure}
 }

