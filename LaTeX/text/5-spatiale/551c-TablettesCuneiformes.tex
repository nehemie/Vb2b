Ma première hypothèse de travail était de représenter la répartition
des tablettes en essayant de faire correspondre au mieux la date
d'écriture − le ductus − avec les vestiges architecturaux, afin de
voir s'il y avait une adéquation et si l'organisation architecturale
pourrait expliquer la répartition des fragments de tablettes
(\cref{GIS-Repartition-tablette-carte}).

60 fragments de tablettes en paléo-assyrien ont été découverts à la
\WTer{} et au \nordV{}. Même si des fragments ont été retrouvés ici ou
là, les fouilles du \nordV{} montrent que seule une archive a été
retrouvée. En effet, pour cette période, la carte de répartition
illustre un regroupement de tablettes autour du \gebUW{}. Étant donné
que certains de ces textes proviennent de l'archive d'un certain
\daya{}, on pourrait, à titre d'essai, identifier cette maison comme
l'une de ses dépendances\footnote{\cite{Dercksen2001}.}. Les tablettes
n'ont par ailleurs pas été retrouvées directement sur le sol, mais
dans les décombres de l'incendie de la maison, ce qui laisse supposer
que le stockage de ces documents avait lieu sur une étagère ou à
l'étage. Les autres fragments de tablettes, dont certains sont
toujours inédits, sont répartis dans la ville basse. Ils indiquent
qu'il devait y avoir un ou plusieurs autres lieux de conservation de
tablette cunéiformes à cette période, comme en témoigne également la
découverte en 2009 d'un fragment à \knw{}\footnote{\cite{Wilhelm2010}.}.

Les recherches des 10 dernières années ont réussi à montrer les
grandes lignes de l'organisation du matériel épigraphique de la
période hittite à \hattusa\footnote{\cite{Hout2005,Hout2008,Hout2008a}
  et en particulier \cite{Hout2008a}.}. La plupart des reconstructions
s'accordent à définir trois «~archives~», qui concentrent l'essentiel
des tablettes : à \buyukkale, à \hah{} et au \tempelI{}. Celle du
\tempelI{} se distingue des autres par une très faible proportion de
textes en script ancien (\ca{} \SI{1}{\percent}) et moyen (\ca{}
\SI{8}{\percent}) et une très grande proportion de textes de la
période récente (\ca{}
\SI{91}{\percent})\footnote{\cite[12]{Klinger2006textfunde};
  \cite[216]{Hout2008a}.}.  Par rapport à la répartition des textes de
la ville basse, les similitudes sont évidentes et on remarque que de
nombreux fragments proviennent de contextes qui jouxtent le \templeI.
On peut donc suppposer que des textes provennant du \templeI{} se
soient répandus dans la ville basse lors de la destruction du temple
ou bien proviennent des remblais de la fouille du temple dans les
années 1930~\dne{} %. - inclus dans J.-C.
%
Néanmoins, d'autres fragments sont repartis dans toute la zone de la
\westterrasse{} et méritent d'être étudier plus en détail\footnote{À
  cet endroit, je tiens à renouveler mes remerciements envers les deux
  personnes qui ont accepté de rédiger un rapport sur ma thèse dans le
  cadre de ma candidature à la bourse de voyage de l'Institut
  archéologique allemand. L'un des rapports, dubitatif face à mes
  conclusions sur la répartition des tablettes cunéiformes, m'a
  encouragé à en reprendre l'étude et à l'approfondir.}.

Par contre, si l'on postule que les fragments proviennent d'une
«~archive~», ceci remet largement en question la mise en correspondance
de la répartition des fragments en fonction de leur date de rédaction
et des vestiges architecturaux
(\cref{GIS-Repartition-tablette-carte}). En effet, à la différence des
autres vestiges, les tablettes cunéiformes ont pu être conservées dans
une archive, ce qui implique que la date de rédaction n'est pas
forcément indicative de la date d'abandon. Ainsi, une séparation
\textit{chronologique} des textes de la période hittite pour l'analyse
de leur répartition spatiale n'est peut-être pas pertinente à
l'échelle de la \WTer{} et il est peut-être plus pertinent de
reprendre le problème sous un autre angle.


\subsubsection{Une question de taille ?}



Quelles sont les informations qu'il est possible de gagner de la
répartition des tablettes et que nous
apprennent-elles sur l'origine des objets « hors contexte ». Afin de
mieux comprendre la répartition des fragments et de faire la
différence entre ce qui relèverait d'une découverte fortuite, on peut
supposer que les fragments les plus volumineux sont moins susceptibles
d'avoir été déplacés sur de longues distances et donc indiqueraient
des lieux probables d'origine de tablettes. Ainsi, il devrait être
possible de confirmer ou d'infirmer s'il faut considérer le \templeI{}
comme le principal lieu de dépôt des tablettes.

J'ai donc entrepris une pondération volumétrique de
chaque fragment\footnote{Cette estimation a été obtenue par deux
sources différentes. D'une part en utilisant les données renseignées
dans le registre des objets inventoriés qui fait état de la longueur et
de la largeur. Lorsque ces données étaient indisponibles, j'ai mesuré les
fragments à l'aide des photographies des tablettes. Dans ce cas, j'ai
pris deux mesures pour chaque fragment en me basant sur les lignes de
signes cunéiformes pour mesurer la largeur maximale et une mesure en
angle droit pour mesurer la longueur maximale. J'imagine qu'une
méthode similaire a été adoptée pour les dimensions consignées dans
le registre des objets. Il en découle que la surface calculée, qui est
une multiplication de la longueur par la largeur, est généralement
surévaluée, car les fragments sont plus souvent
triangulaires que rectangulaires et parce que c'est une valeur
maximale et non moyenne qui a été mesurée. Le poids, sans doute la
mesure la plus facile à prendre, serait sans doute un autre bon
indicateur de la taille, mais il n'est jamais renseigné. Néanmoins,
l'approximation proposée, faute de mieux, ne devrait pas changer ici
l'interprétation des résultats. Les scanographies en trois
dimensions de fragments de tablettes, qui sont en train de se
développer, permettront sans aucun doute d'avoir une valeur beaucoup
plus exact et, qui plus est, en centimètres
cubes.}(\cref{Pl_43}).
Le nuage de points de la surface estimée exprime clairement qu'il
n'existe que trois fragments qui fassent (au maximum) plus de
\SI{100}{\square\centi\meter} et uniquement 18 fragments qui ont une
taille supérieure à \SI{50}{\square\centi\meter}. La grande majorité
des fragments est donc de petite taille (la médiane se situe à moins de
\SI{15}{\square\centi\meter}), ce qui s'accorde assez bien avec l'idée
de découvertes fortuites plutôt qu'\insitu{}\footnote{Étant donné
que la taille des fragments n'a pas été reportée dans
la \textit{Konkordanz} (\cite{KosakKhK}), je n'ai pas pu entreprendre
de comparaison avec l'ensemble des tablettes.}.  Cependant,
cette remarque n'est pas valable pour les plus grands fragments, dont
il faut interpréter la répartition spatiale
(\cref{Lab_GIS-Cuneiform-bubbleplot-size}). Celle-ci indique, par la
petitesse des points, que les fragments retrouvés aux abords du
\templeI{} ne sont pas de gros fragments. Ceux-ci peuvent donc très
bien provenir du temple et de ses magasins, tout comme les fragments \kfjn{70}{10} (\ca{}
\SI{80}{\square\centi\meter}) et \kfjn{57}{617} (\ca{}
\SI{75}{\square\centi\meter}). Les deux graphiques
(\crefrange{Pl_43}{Lab_GIS-Cuneiform-bubbleplot-size})
indiquent toutefois que les deux plus gros fragments ont été retrouvés
l'un à la suite de l'autre en 1957, assez loin du temple. De plus, il
y a une concentration de fragments à cet endroit. Cette disposition
indique qu'il y a de fortes chances qu'elles proviennent d'un lieu
proche de leur découverte, en particulier du \gebTT{}.


% On note que les fragments découverts à l'ouest de cette
% concentration et au nord du \templeI{} en 1938 sont difficile à
% interpréter. En effet, les fouilles de 1938 ne permettent pas de
% savoir à quel endroit du chantier de 1938 ils proviennent. Il est
% donc impossible de distinguer si les fragments proviennent plutôt de
% l'ouest (et pourrait provenir de la concentration aux abords du
% \gebTT{}) ou plutôt du sud de la zone fouillée (ce qui indiquerait
% plutôt une orgine du \templeI{} et des magasins). Faute de mieux,
% les fragments sont situés au hasard dans la zone de fouille de 1938
% et il est difficile d'interpréter leur répartition spatiale.

L'emplacement de la découverte des trois autres grands fragments (\kfjn{73}{382},
\kfjn{75}{146d} et \kfjn{75}{146b}) indiquent qu'ils pourraient
avoir une autre source. Ils témoignent en outre d'un autre phénomène.
Ces trois fragments sont jointifs et appartiennent à la même tablette.


\subsubsection{Répartition des fragments jointifs}

Les études des textes ont permis de proposer des joints entre les
fragments et la liste des fragments jointifs sont récapitulés dans la
liste ci-dessous\footnote{Données d'après \cite{KosakKhK}.}. Une
différence est faite entre les joints directs, marqués par le signe
plus et les joints indirects, c'est-à-dire où l'on suppose que les
fragments proviennent d'une même tablette.  Bien entendu, cette liste
prend tout son sens, si l'on représente la répartition spatiale de
tous les fragments jointifs en indiquant les joints par une ligne
(\cref{Lab_GIS-Cuneiform-joints}).


\begin{enumerate}[noitemsep]
  \item \label{joints-13} \kfjn{38}{11} (+) \kfjn{38}{36}
  \item \label{joints-14} \kfjn{38}{132a} (+) \kfjn{38}{132b}
  \item \label{joints-16} \kfjn{38}{134} + \kfjn{38}{167}
  \item \label{joints-15} \kfjn{56}{49} (+) \kfjn{56}{158}
  \item \label{joints-1} \kfjn{57}{65} + \kfjn{57}{97} (+) \kfjn{57}{102}
  \item \label{joints-2} \kfjn{57}{99} (+) \kfjn{57}{101} (+) \kfjn{62}{1}  + \kfjn{77}{165}
  \item \label{joints-3} \kfjn{57}{103} + \kfjn{57}{130} + \kfjn{57}{240}
  \item \label{joints-4} \kfjn{57}{231} (+) \kfjn{57}{232} + \kfjn{57}{233}
  \item \label{joints-5} \kfjn{57}{617} (+) \kfjn{60}{159}
  \item \label{joints-11} \kfjn{61}{479} + \kfold{Bo 3006} + \kfjn{69}{926} (+) \kfjn{77}{105}
  \item \label{joints-6} \kfjn{73}{323} + \kfjn{73}{382} (+) \kfjn{73}{382a} +  \kfjn{75}{146} + \kfjn{75}{146a} + \kfjn{75}{146b} + \kfjn{75}{146d}
  \item \label{joints-7} \kfjn{75}{95} (+) \kfjn{76}{150}
  \item \label{joints-8} \kfjn{75}{368} (+) \kfjn{75}{378}
  \item \label{joints-10} \kfjn{77}{1} (+) \kfold{AO 9411.46} (+) \kfold{Bo 1286} (+) \kfold{Bo 6435}
  \item \label{joints-9} \kfjn{77}{292} + \kfold{Bo 1587} + \kfold{Bo 5436}
\end{enumerate}

Tout d'abord, une remarque générale sur les joints proposés s'impose.
Dans deux cas (liste n°\ref{joints-2} : \kfjn{62}{1} et liste
n°\ref{joints-5} : \kfjn{60}{159}), une suggestion de joints a été
faite entre des fragments trouvés à la \wter{} et à \hah{}. Dans ces
deux cas, il ne s'agit pas de joints directs, mais d'une suggestion
d'un fragment qui pourrait appartenir à la même tablette
%: \kfjn{57}{617} (+) \kfjn{60}{159} et \kfjn{57}{101} (+)
%\kfjn{62}{1}.
.  Il me semble que ces propositions sont peu probables, puisque cela
impliquerait que les fragments d'une même tablette aient été retrouvés
à plus de \SI{300}{\metre} l'un de l'autre. Ceci remettrait en
question la séparation des archives entre \hah{} et \templeI{} et
demanderait, à minima, une explication archéologique. Étant donné que
ces fragments ne sont pas physiquement jointifs, mais qu'il s'agit de
propositions d'appartenance à la même tablette, il semble plus
raisonnable d'envisager deux tablettes différentes. Je ne considère
donc pas ce joint comme valide.  De même, les joints avec des fragments découverts lors
des premières fouilles du \templeI{}
% commme \kfold{AO 9411.46} (+) \kfold{Bo 1286} (+) \kfold{Bo 6435};
% \kfjn{77}{105} + \kfold{Bo 3006} + \kfjn{69}{926} + \kfjn{61}{479};
% \kfjn{77}{292} + \kfold{Bo 1587} + \kfold{Bo 5436}),
(liste n°\ref{joints-11} : \kfold{Bo 1587} et \kfold{Bo 5436}) ne sont
pas assurés, même si, dans ce cas, l'emplacement, directement à l'ouest
de la terrasse du \templeI{}, ne pose pas de problème archéologique.


De façon intéressante, les emplacements des fragments qui sont
jointifs recoupent certains paramètres de la répartition par taille
(\cref{Lab_GIS-Cuneiform-bubbleplot-size,Lab_GIS-Cuneiform-joints}). Dans le
nord, on retrouve toute une série de fragments découverts en 1957. Ils
correspondent exactement à l'emplacement de la découverte de deux plus
grands fragments et renforcent l'idée que des tablettes avaient été
conservées à cet endroit, et il est tout à fait légitime de suggérer
que le \gebTT{} pourrait en être le lieu d'origine. Celui-ci est l'un
des rares bâtiments datés à cette période et il jouxte l'ensemble de
tablettes. Ceci permet d'identifier un bâtiment où était conservées
des tablettes, mais également d'inférer sur la taphonomie. En effet,
tant la répartition des points que la taille et les joints tendent à
indiquer que les fragments n'ont pas forcément été déplacés sur de
grandes distances et étayent la thèse que les objets sont retrouvés à
une certaine proximité de leur lieu d'abandon.

Dans le sud-ouest de la \wter{}, six fragments sont jointifs, qui
appartiennent, là encore, aux fragments les plus grands. Néanmoins, la
répartition est beaucoup plus large. Ceci s'explique d'une part par
l'incertitude dans les lieux de découvertes des fragments, du fait que le
carroyage de 1975 que j'ai reconstruit est, par manque de
documentation, assez approximatif. Il est aussi remarquable qu'il y a
peu d'autres fragments à proximité qui laisseraient supposer un lot de
tablettes. Ces six fragments appartiennent à une tablette de la fête
du KI.LAM (CTH 627). On pourrait supposer qu'elle ait été conservée à
proximité, même si aucune architecture voisine n'a pu être
retrouvée datant des \xiveme--\xiiieme{} siècles. Ou bien faut-il
envisager une origine légèrement en amont, là où il n'y a pas eu de
fouilles, ce qui pourrait également expliquer la provenance des
fragments alentours ? Comment faudrait-il alors qualifier le lieu ?
Peut-on envisager un lieu dans la capitale qui n'aurait conservé que
quelques tablettes ?  Peut-on concevoir que ce genre de tablette ait
été conservé dans un bâtiment « privé » ou bien doit-on y voir un
bâtiment officiel ? Il faudrait peut-être y ajouter par ailleurs le
joint n°\ref{joints-8} : \kfjn{75}{368} (+) \kfjn{75}{378} et les
quelques tablettes environnantes.

Le joint entre liste n°\ref{joints-2} : \kfjn{77}{165} + \kfjn{57}{99}
et, éventuellement,  (+) \kfjn{57}{101}, est de loin le plus
problématique, puisqu'il réunit deux fragments qui ont été retrouvés à
plus de \SI{70}{\meter} l'un de l'autre, voire à \SI{300}{\meter}, si
l'on considère le fragment \kfjn{62}{1}. Il s'agit du texte CTH 258,
c'est-à-dire des instructions du roi \tuthalija{}. En premier lieu, je me suis demandé
s'il avait été possible que le fragment \kfjn{57}{99} ait été mal
enregistré, car en 1957 des fouilles avait été aussi menées à quelques
mètres au nord du lieu de découverte la découverte de \kfjn{77}{165}
(\cref{GeneralObjectDistribution}).  Néanmoins, le journal de fouille
mentionne : «~\textit{Im Nordareal fand sich im Abbruchschutt der
Schichte 1 unmittelbar neben dem S. 24 erwähnten dreikolumnigen
Festritual ein Tontafelstück: Instruktion Tuthalija's IV, Duplikat zu
KUB XIII 9.  Muss schon im defektem Zustand in alter Zeit an Ort und
Stelle gekommen sein}~»\footnote{Journal des fouilles de
1957, 27}. Si le passage se réfère à \kfjn{57}{103}, alors il
semble bien provenir de la partie nord\footnote{
La publication d'\Otten{} n'apporte pas beaucoup plus d'informations à
ce sujet, puisqu'il mentionne : «~\textit{Beide Stücke [\kfjn{57}{99} und
\kfjn{77}{165}] stammen aus dem Gebiet nordwestlich ausserhalb der
Magazine von Tempel I, etwa auf der Schnittline der Planquadrate
J/20-K/20 des Unterstatdtareals}~» (\cite{Otten1979original}).
La ligne qu'il indique fait exactement \SI{100}{\meter} de long du
nord au sud et recoupe les deux positions évoquées. Elle est
quasiment identique à la ligne entre ces deux joints sur la
\cref{Lab_GIS-Cuneiform-joints}.}.

Enfin, il reste un autre joint à expliquer (liste n°\ref{joints-7} :
\kfjn{75}{95} (+) \kfjn{76}{150}), ainsi que la présence de nombreux
fragments dans la partie nord de la \westt{}. Il est difficile de
préciser leur origine, puisque la taille pourrait en faire des
découvertes « hors contexte », mais l'étude de la répartition des
documents sigillaires (\cref{5-spatial-repartition-seals}) tend
plutôt à suggérer la présence d'un bâtiment officiel dans les
environs.

\subsubsection{Question de genre ?}


La dernière répartition spatiale qu'il est possible de faire est celle
des genres selon la classification du Catalogue des Textes Hittites
(CTH) de \Laroche{}\footnote{\cite{Laroche1971catalogue}.}, révisée et
augmentée par \Kosak{} et \Muller\footnote{\cite{KosakCTH}.}
(\cref{Pl_46}). Cette catégorisation révèle que les types
qui dominent sont les textes de fête et les instructions de rituels,
mais que les proportions de ces catégories correspondent à peu près
aux pourcentages que l'on connaît pour l'ensemble  des tablettes
retrouvées dans la ville\footnote{\cite[69, table
1]{Miller2017tablet}.}.  De même, on remarque que les textes de
présages sont peu nombreux et que les textes mythiques le sont
nettement plus. À ce propos, il est tout à fait remarquable que la
plupart des fragments de textes mythiques aient été trouvés aux abords
du \gebTT{}. Sinon, il ne me semble pas qu'il y a de répartition
significative.


\subsubsection{Bilan de la répartition des tablettes cunéiformes}

À partir de la répartition des tablettes cunéiformes, on peut conclure
qu'il existait «~trois sources~» principales de tablettes : l'archive
de \daya{} \alaperiodeCAC{}, déposée dans le \gebUW{};
celle localisée dans le \templeI{}, dont des fragments ont été
retrouvés à proximité lors des fouilles en contrebas du temple;
enfin, un groupe de textes était conservé probablement dans le
\gebTT{}.  On notera par ailleurs que l'ensemble se caractérise par la
présence de textes mythologiques. En outre, il est possible
d'identifier une tablette du KI.LAM qui a été retrouvée à proximité de la
porte de la rue du temple (\tortstr), mais la répartition spatiale ne
permet pas d'apporter des conclusions définitives sur son origine.

La plus grande partie des fragments en dehors de ces lieux sont des
découvertes hors contextes, ce qui rend l'attribution topographique
assez difficile. Un regroupement de fragments semble exister au sud
de la route moderne, tout au nord de la limite des fouilles de la
\westt{}. Existait-il un bâtiment sous la route moderne où
était conservé des tablettes cunéiformes ?

Enfin, du point de vue de la taphonomie, l'étude des tablettes montre
que l'on peut établir d'une manière globale une adéquation entre
lieux d'abandon et de découverte. Néanmoins, il est difficile
d'attribuer les objets à des vestiges architecturaux.
