\subsection[Objets cultuels]{Objets cultuels (\cref{GIS-Repartition-cultuel,GIS-Repartition-cultuel2})}

Cette catégorie regroupe les objets qui n'ont été utilisé ni dans la
production de bien matériel ni comme armement. Ils devaient ainsi
posséder une valeur symbolique forte.




\subsubsection{Figurines zoomorphes}


Les figurines en terre cuite n'ont jamais été traitées de manière
globale et nos connaissances sont limitées sur cette catégorie
d'artefacts. Ces figurines peuvent être reparties entre figurines
anthropomorphes et figurines zoomorphes. \Schachner{} a esquissé
les caractéristiques de l'ensemble de la production \citfr{artistique}
de figurines\footnote{\cite[154--159]{Schachner2012e}.}, mais un
travail de fond reste à entreprendre. L'un des premiers problèmes
concerne la distinction entre figurine et rhyton, puisque, si seule la
tête ou le buste est conservé, il est impossible de faire la
différence entre une figurine, un rhyton ou un protomé.  Il est
cependant probable que ces objets soient intervenus de toute façon
dans la même sphère cultuelle et n'aient pas été perçus comme
si différents.

Les figurines zoomorphes sont très bien représentées à la \WTer{} et
sont réparties de manière assez homogène. Ceci indique plutôt qu'il ne
s'agit pas d'une possession extraordinaire. Ces objets ont dû jouer
un rôle important dans le culte domestique. Parmi toutes les figurines
zoomorphes, équidés, oiseaux (notamment canards), lions, antilopes
sont les espèces les plus fréquemment représentées après les taureaux.
Les représentations de taureaux sont les plus nombreuses et les plus
soignées.

\subsubsection{Figurines de taureau}


Le groupe des terres cuites en forme de taureau est à diviser en deux
groupes.

Il y a les taureaux à l'échelle un cinquième (plus proche de la statue
que de la statuette), qui sont les plus impressionnants. Le fameux
exemple du couple de taureaux découverts soigneusement déposés à
\buyukkale{} illustre la qualité et la taille de cette production
particulière\footnote{\cite[151 fig.  156]{Bittel1976}.}. Alors que la
plupart des figurines en terre cuite ne présentent pas d'engobe ou de
traitements élaborés, les taureaux sont engobés et polis en rouge,
rehaussés de blanc et de noir, ce qui leur donne un aspect luisant
très distinctif. Ces figurines sont également incisées
autour des yeux comme sur le front et les jambes sont
caractéristiques avec leurs ergots et leurs sabots bien dessinés. Ces
statues de taureaux en terre cuite sont interprétés comme des
représentations du couple divin \hurri{} et
\serri{}\footnote{\cite[14--18]{Bittel1937}.}. Ces objets pouvaient
sans doute être polysémiques et jouer tant un rôle dans le culte
officiel qu'avoir une fonction apotropaïque. Ils semblent même avoir
joué un rôle dans le culte domestique.

Un deuxième groupe inclut les figurines de taureaux en terre cuite de
petite taille et sans engobe. Ce deuxième groupe est plus rare.  On
remarquera, néanmoins, que le nombre d'objets répertoriés ne
représente pas un nombre minimum d'individus, puisque c'est l'un ou
l'autre élément qui a pu été enregistré (sabot, corne ou tête).

Le nombre de fragments retrouvés est assez important. Tout comme pour
les figurines zoomorphes, il n'y a pas de concentrations évidentes et
les fragments sont dispersés de façon homogène. Ceci plaide en faveur
du rôle de telles figurines dans le culte domestique.

\subsubsection{Figurines anthropomorphes}


Les figurines anthropomorphes, en revanche, sont très rares et il n'existe pas
de groupements particuliers. Elles sont le plus souvent d'une facture
simple, plus simple que celle des statues de taureaux (voir
\cref{Lab_Bo70-145i-Tete-figurine}, \cpageref{Lab_Bo70-145i-Tete-figurine}). Ces
figurines sont la plupart du temps de simples modelages avec adjonction
d'éléments rapportés pour le décor, mais sans autre traitement de surface (engobe,
polissage ou peinture).




\subsubsection{Les fragments de céramique à décor en relief}


Les fragments de céramique à décor en relief sont largement répartis dans toute
la \WTer{}, sans que l'on puisse distinguer de groupements particuliers.
Nettement moins abondante que les figurines zoomorphes, la céramique à décor en
relief devait appartenir à des vases rares et prisés.






\subsubsection{Bras de libation}


Les \citfr{bras de libation}, appelés également \citfr{objets en ‘forme
d'avant-bras’} appartiennent à la catégorie d'objets dont les interprétations
ont été les plus élaborées\footnote{Par exemple \cite[25\psq]{Bittel1937} ;
\cite{Bittel1957d,Alp1967,Kepinski1977,Courtois1979,Mielke2007,Muhlenbruch2011a}.}.
Les multiples dénominations reflètent les débats sur les fonctions de l'objet :
interprétés tout d'abord comme encensoirs -- cette interprétation est
actuellement largement rejetée --, puis comme bras de libation, ces objets
restent assez énigmatiques. \Mielke{} y voit des objets en forme d'avant-bras
destinée à l'onction\footnote{\cite{Mielke2007}.}. À l'instar des
«~\emph{spindle bottles}~», il s'agit d'importations bien reconnaissables par
l'argile utilisée, fine et rouge, couramment appelée \gls{rlw}\footnote{Il
existe de très nombreuses études de provenance
(par exemple \cite{Eriksson1993,Knapett2007,Schubert2007}), qui assignent l'origine de
l'argile \gls{rlw} soit à Chypre, soit aux rivages de Cilicie.}.

La répartition de ces objets se concentre directement à l'ouest du
\tempelI. De même qu'une partie des tablettes cunéiformes ou des
documents sigillaires, ces objets pourraient provenir de
l'inventaire du temple qui se serait effondré, à moins que leur répartition
suggère une connexion avec le \gebRE. Du point de vue uniquement de la
répartition des objets, il est difficile de faire la
différence. Il est incontestable que ces objets ne se retrouvent pas
sur la totalité du site, mais en certaines concentrations. La
découverte d'un ensemble très important de «~bras de libation~»
déposés dans un bassin de la ville haute a été interprété comme
inventaire d'un temple\footnote{\cite[61--66]{Seeher2002} ;
\cite[153\psq]{Schoop2009a}.}.  Si l'on suit l'interprétation d'un
objet dédié à la manipulation de liquide, alors l'association avec le
\gebRE{}, un bassin, pourrait être validée.

On notera que le nombre d'objets répertoriés, 91, ne représente pas un
nombre minimum d'individus, puisque c'est l'une ou l'autre extrémité du
bras qui a été enregistrée dans le registre des petits objets.


\subsubsection{Tessons poinçonnés}

Les tessons poinçonnés ont été estampillés avant la cuisson du vase,
c'est-à-dire au moment de la production. Ceux-ci sont tout aussi
fréquents que les bras à libation ou la céramique à décors en relief
et ne présentent pas de répartition particulière. La partie sud de la
ville basse est toujours moins bien représentée (à cause de la nature
des données), quoique l'absence soit légèrement accentuée dans ce cas.
Les tessons poinçonnés sont à considérer plutôt comme des objets
typiques des périodes anciennes.



\subsection[Métallurgie et textile]{Métallurgie et textile (\cref{GIS-Repartition-prodobjet})}



Parmi les objets découverts dans la ville basse, ceux destinés à la
transformation et à la production d'autres objets sont bien documentés.

\subsubsection{Aiguilles}

En tête du classement, les aiguilles sont des outils éminemment
abondants. Une aiguille est une simple tige de métal dont une
extrémité se termine en pointe effilée et l'autre par un chas. Elles
étaient utilisées pour les travaux de couture, de broderie, de
tapisserie, de maroquinerie et autres dérivés du travail du cuir. De
manière remarquable, même s'il s'agit d'un des objets les plus
abondants (et donc représentatifs), il est presque systématiquement
passé sous silence dans la littérature primaire et secondaire et est
traité avec les épingles\footnote{\cite[79--86 dont un petit
paragraphe p. 80 sur les aiguilles]{Boehmer1972} ; voir aussi
\cite[11--14]{Boehmer1979}.}. Dans le travail de \Boehmer{}, qui vise
à établir une typo-chronologie, les aiguilles jouent un moindre rôle,
puisqu'elles n'évoluent presque pas. La quantité d'aiguilles
retrouvées n'est pas corrélée avec d'autres objets liés à la
production de textiles. Seules les épingles sont aussi abondantes. Au
vu de la quantité d'aiguilles et d'épingles qui ont été retrouvées, on
peut conclure que ces objets sont parmi ceux qui étaient les plus
courants. Ceux-ci devaient même avoir une valeur moindre au vu de la
fréquence de leur abandon. Ces objets auraient très bien pu être
recyclés, mais ils ont été perdus et oubliés.

\subsubsection{Fusaïoles}

Les fusaïoles sont des marqueurs de la production de textile. Une
fusaïole est un petit objet conique ou discoïde en terre cuite, percé
d'un trou central destiné à recevoir l'extrémité du fuseau auquel il
sert de contrepoids. Alors que les fuseaux, sans doute en bois, ont
disparu, les fusaïoles témoignent de l'activité de filage, de la
transformation de fibre en fil. Cette répartition des fusaïoles est
très homogène et il s'agit d'un objet assez fréquent, sans être
abondant. Cela témoigne de la production de fil pour le textile ;
parmi ces outils pour la production et le travail du textile, on
remarque l'absence de poids pour métier à tisser. Dans l'ensemble,
même si les traces sont tenues et peu spectaculaires, leur abondance
montre que les activités liées au textile avaient une place très
importante pour ce quartier.

\subsubsection{Burins et poinçons}

Les burins et les poinçons se distinguent respectivement par la forme
de leur extrémité, plate ou effilée. Ils ont pu servir à travailler
des matériaux très différents comme le textile, le métal, le bois, le
cuir. Ces objets sont très fréquents et se retrouvent partout dans la
ville basse.

\subsubsection{Moules et lingots}

En ce qui concerne la métallurgie et le travail du métal, les
attestations d'objets sont beaucoup moins nombreuses. Les exemples de
moules et de lingots ne permettent pas d’identifier de lieux de
production. Il est probable que ces objets étaient aussi conservés en
dehors du lieu de production, en particulier pour les lingots, qui ont
une valeur en tant que matériaux bruts. Les pierres à aiguiser sont
classées parmi les outils du métallurgiste dans le sens où elles font
partie de la chaîne opératoire du travail du métal. Ces outils, non
liés à la production première du métal, sont également répartis dans
toute la ville basse. Si on les ignore, la quantité d'attestations de
matériel indiquant le travail du métal est très faible.

La mise en perspective des outils de métallurgie et de textile laisse
clairement transparaître l'absence d'outils «~lourds~» pour la
transformation de matériaux. Les objets retrouvés sont de petites
tailles et en métal, comme les aiguilles, les poinçons et les burins.
Ils s'adaptent très bien à des petits travaux domestiques de retouche
ou de décoration.








\subsection[Les catégories armes, outils, parures]{Les catégories armes, outils,
parures (\cref{GIS-Repartition-arme-outil})}


Armes et « outils » sont répartis de manière assez homogène dans la
\WTer{}.  Parmi les armes, ce sont les pointes de flèche qui sont les
plus abondantes sans que des dépôts puissent être distingués, de type
carquois par exemple. J'ai déjà remis en cause l'interprétation du
\gebEU{} et je ne pense pas que ses flèches indiquent un épisode
guerrier, mais pourraient tout aussi bien appartenir à l'inventaire de
la maison. La répartition générale indique bien qu'elles sont assez
courantes.

Les objets, majoritairement en bronze, dont l'utilisation n'est pas
identifiable grâce aux photographies, mais dont la forme laisse
supposer un emploi «~technique~», sont classés dans la catégorie
«~outils~». Leur répartition n'indique pas de groupement particulier.

Les différents types d'épingles forment la majeure partie de la
catégorie parure, qui est l'ensemble fonctionnel le plus important
numériquement. La plupart des catégories typologiques sont connues à
toutes les périodes et, en étudiant leur répartition, il n'est pas
possible de retrouver une différence simplement spatiale. Les épingles
sont largement diffusées dans toute la \WTer.




\subsection{Subsistance}

Pour aborder le problème de la subsistance, je n'ai pas cartographié les
vases, car la sélection sur le terrain est très aléatoire et seuls
les vases qui sont suffisamment bien conservés ont été directement
enregistrés. En revanche, la répartition des meules aurait pu fournir
une porte d'entrée, si celles-ci avaient été soigneusement
documentées. Toutefois, la répartition est biaisée par les années de
collecte des meules, car elles n'ont été documentées que dans les
zones fouillées en 1976 et 1977 (voir \cref{MeulesCourante}).



\section{Bilan}\label{56-Bilan}


Le travail réalisé sur la chronologie, la stratigraphie, la nature de
l'occupation tant à travers les vestiges immobiliers que mobiliers prouve
que la documentation rend la reconstruction de la vie quotidienne, à
l'échelle de la personne, très difficile. À partir de mon étude,
certains points, notamment sur l'utilisation des bâtiments et les
différentes fonctions des pièces, devront faire l'objet de nouvelles
recherches. De même, la restitution du volume de l'architecture, la
question de l'étage (et du toit) seront à reprendre complètement. De
nombreuses questions restent ouvertes sur la vie quotidienne :
quelles étaient les activités journalières qui rythmaient la vie et
jouaient un rôle primordial dans la socialisation ? Par exemple, où,
comment et en quelle compagnie étaient pris les repas ? À quel rythme
? Comment se passaient leur préparation et leur service ? Quels
étaient les décors intérieurs et extérieurs des bâtiments ? Néanmoins,
à partir de la répartition globale des objets de la \WTer{}, quelques
remarques générales peuvent être formulées.

Il n'est pas possible de dater précisément les artefacts à l'aide de
la seule typologie et donc de différencier les répartitions par
période. Dans les cartes de répartition présentées, hormis la
répartition des tablettes cunéiformes et des documents
sigillaires, les objets qui appartiennent clairement
\alaperiodeCAC{} (comme les poids en forme de croissant de lune) ou
aux périodes plus récentes (par exemple, les fibules) ont été
éliminés. Globalement, l'exploration archéologique \delaperiodeCAC{} a
été assez réduite et ce matériel « contamine » assez peu l'image
obtenue pour la répartition des objets. De même, cette zone de \bo{} a
uniquement été utilisée comme une nécropole par la suite, ce qui a
permis de bien mettre à l'écart le matériel plus récent (urnes,
fibules ou monnaies). Mon sentiment est que les cartes de répartition
donnent une image assez fiable du matériel de la période hittite et,
en particulier, de la période ancienne. En effet, parmi les artefacts
retrouvés dans une proportion standard, nombreux sont ceux qui sont
plutôt typiques des périodes anciennes (céramique à relief, céramique
estampillée), alors que les exceptions notables, comme les « bras à
libation » (qui datent plutôt des \xveme--\xiveme{} siècles), ont une
répartition géographique qui s'accorde très bien avec les vestiges
plus récents (aux environs du \gebRE{} ou du \tempelI).

Une question qui reste ouverte concerne la différence entre ce qui a
été retrouvé lors des fouilles par rapport à ce qui a été utilisé dans
le passé. Il va sans dire qu'une réponse exacte ne peut pas être
formulée. Dans tous les cas de figure, il est frappant que la plupart
des objets qui ont été retrouvés sont de petite taille (aiguilles,
fuseaux, poinçons, pointes de flèche, cachets) et que ce sont plutôt
les objets massifs (moules, meules dormantes) qui sont manquants. Une
poignée d'objets en or (le plus souvent en feuille d'or) ont même été
retrouvés (voir \cref{Fig_74},
\cpageref{Fig_74})\footnote{À ce propos,
\cite[40\psq]{Boehmer1979}.}.  Le raisonnement le plus répandu suppose
que lors de l'abandon ou la destruction d'un site, ce sont les petits
objets de haute valeur qui sont emportés, alors que les objets les plus
larges sont laissés sur place. Ainsi, si l'on postulait une production
intensive à la \WTer, il est assez étonnant qu'aucun déchet n'ait été
retrouvé. Il n'existe pas une seule mention de scorie pour la \WTer{}.
Cet indice confirme l'idée que l'artisanat qui avait lieu dans ce
secteur, n'a pas
pu être de grande ampleur, mais restreint à de petites retouches,
travaux de finition, quelques refontes d'objets. Parmi toutes les
activités possibles, c'est le travail du textile qui ressort le mieux.

%\subsection{La population de la \WTer{} et sa «~disparition~»}

Le chapitre précédent souligne les caractéristiques principales des
bâtiments et ce chapitre étudie les objets pour en induire les
activités auxquelles ils étaient liés. Dès lors, il est possible d'interroger les identités des
habitants et des habitantes de ces bâtiments et de commencer une
réflexion, qui sera poursuivie au chapitre suivant, sur les raisons de
l'abandon de la \WTer{}.

\subsubsection{\LaperiodeCAC{}}

À cette période, les tablettes cunéiformes et les documents
sigillaires prouvent que des activités d'administration se
déroulaient à l'échelon de l'individu à la \WTer. Les maisons du \NoV{}
ressemblent à ce que l'on connaît dans la ville basse de \kultepe{}.
Les vestiges de la \WTer{} (\gebOW{} à \gebOZ{}), même si les plans ne
sont que partiels, ne laissent pas transparaître de différences
évidentes. Ces bâtiments combinent toujours des vases de stockage avec
des fours et des foyers. Ils étaient le lieu d'activités assez
diverses, de commerce, mais aussi de production d'objets.

\subsubsection{\Laph{}}\label{56-Abandon}

Pour la phase la mieux attestée, la différence la plus notable se
situe au niveau des outils d'administration (tablettes cunéiformes,
scellés), puisque des concentrations apparaissent. Ce changement est
particulièrement marqué à partir du \xvieme{}--\xveme{} siècle. Les
armes et les outils pour le travail du métal sont présents, mais ne
représentent pas les catégories principales. Répartis de façon
homogène dans la ville, on retrouve des objets liés aux cérémonies,
comme les figurines zoomorphes ou la céramique à relief, que ce soit
pour le culte domestique ou la boisson, à supposer que ces
catégories étaient différenciées à l'\ageBronze{}. Enfin, les deux
catégories qui sont les mieux représentées sont intimement liées,
puisqu'il s'agit des éléments de parure et des outils de travail du
textile. Ils témoignent d'une production et de réparations à petite
échelle.

Ce portrait général correspond plutôt à celui d'habitations
d'individus dont l'activité principale n'était ni dédiée à la
production de denrées ni à celle d'outils. La position de ce quartier,
directement à côté du \tempelI{} et protégé par l'\abschnittsmauer{},
indique qu'il s'agit d'un emplacement privilégié. De surcroit, la
taille des bâtiments indique qu'ils sont assez spacieux et leur
construction souvent soignée étaye cette hypothèse.  Il semble donc
approprié de voir dans la plus part des bâtiments des habitations pour
des fonctionnaires ayant un rôle dans la gestion de la ville, du
temple et même peut-être dans la gestion du royaume.

Une telle interprétation rend la question de l'«~abandon~» de cette
zone d'occupation problématique. Comment serait-il possible que
l'administration du royaume disparaisse de la capitale deux siècles
avant la fin du royaume ? Une telle reconstruction est-elle réellement
envisageable ?  Les textes nous indiquent que les fonctions de la capitale ont été
déplacées à \tarhuntassa{} au tout début du \xiiieme{} siècle (\ca{}
1295−1274)
\footnote{\cite{Dogan-Alparslan2011b,Hout2012c}.}. Par
conséquent, on peut en déduire que les fonctionnaires se soient
déplacé\es{} en même temps.  Ceci expliquerait assez bien la situation
du \xiiieme{} siècle et de plus en plus d'indices concordent pour
montrer que «~la~» capitale \hattusa{} devait déjà avoir
perdu de son importance au \xiiieme{}, un changement visible dans toute la
ville (voir \infra{} \cref{Chapter6-BR}). Mais peut-être faut-il
envisager que le déplacement de la capitale vers \tarhuntassa{} ne fût
pas si radical. Une mise en perspective de cet évènement avec le règne
de \tuthalijaIII{} pourrait expliquer l'abandon de la \WTer{} dès le
\xiveme{} siècle. En effet, pendant son règne, \tuthalijaIII{} a été
confronté à des attaques armées venant du nord, attribuées aux
\kaska{}, et la capitale avait dû être déplacée à \samuha{}
(\kayalipinar) et c'est pendant le même règne que la ville de
\sapinuwa{} (\ortakoy) est considérée comme une autre capitale
possible -- une assertion
controversée\footnote{\cite{Dogan-Alparslan2011b}, et \cite{Hout2012c}
avec références.}. Les textes évoquent la destruction du pays de
\hatti{} pendant le règne de \tuthalijaIII{} et on peut supposer que le
centre du pouvoir avait été déplacé, sans doute,
temporairement\footnote{L'interprétation du passage n'est pas
évidente, mais plusieurs scientifiques considèrent que \hattusa{} a
été détruite, par exemple \cite[586]{Beal2011a}, et \cite[note de bas
de page 18, avec autres références]{Dogan-Alparslan2011b}.
\cite[132]{Klengel1999a} est plus prudent dans l'interprétation de
l'étendue de la destruction. Voir également \cite{Klinger2006textfunde}}. Là encore, si la capitale a été
déplacée, il faut considérer que l'administration ait suivi ce
déplacement. Il s'agit là, à mon avis, d'une explication plausible
pour comprendre l'absence de constructions au \xiveme{} siècle. On
pourrait même aller plus loin en suggérant que cette administration
avait eu un avantage à avoir un centre de commandement plus au
sud-est, à \samuha, notamment parce que \suppiluliumaI{}, successeur
et soutien de \tuthalijaIII{}, a attaqué le Mittani et a étendu
l'emprise du \royaumehittite{} en Syrie du nord, une expansion que les
historiens considèrent comme le signe du passage du Royaume hittite à
l'Empire hittite.


Pour conclure, si on interprète ce quartier comme le lieu de
résidence principale de l'administration \delaph{}, alors son
abandon au cours du \xiveme{} siècle marque le premier signe dans le
changement de statut de l'Empire et le déplacement de ses intérêts vers
le sud-est.

