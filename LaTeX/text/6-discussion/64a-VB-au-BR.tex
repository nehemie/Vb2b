\section{La ville basse \alaph{}}

Avant de proposer une synthèse sur l'espace urbain à \bo{}, je
récapitulerai les attestations d'occupation pour la ville basse pour
brosser une série de panoramas documentant au mieux la constante
restructuration du paysage urbain.



\subsection{Reconfiguration de la ville basse}

\paragraph[Le \xviieme{} siècle]{Le \xviieme{} siècle
  (\cref{PlanHittite-UST-17})} ― D'après les résultats des fouilles
menées sous l'égide de \Seeher{} à \bka{}, un large bâtiment à
caractère officiel (appelé \chantier{Monumentalgebäude}) a été bâti à
la fin du \xviieme{} siècle (Phase \emph{BKA 12}), au point le plus
élevé, avec une vue imprenable\footnote{\cite{Seeherinpressa}.}. Il
présente une continuité dans sa fonction avec la période précédente,
même si une (courte) discontinuité et un abandon sont attestés. En
revanche, il n'y a pas d'activités attestées pour le \MP{}
\chantier{(MP)} et l'\UP{} \chantier{(UP)}. À \buyukkale{} des
constructions sont attestées (phase \emph{BK-IVc}), notamment des
bâtiments que \Neve{} interprète comme des ateliers où l'on
travaillait le métal\footnote{\cite[34--46]{Neve1982}.}, mais pas de
constructions monumentales, que \Neve{} présume avoir été localisées
au nord-est du \buyukkale. Il suppose par ailleurs que la poterne a
été construite dès la fin de cette période. Pour la \WTer{}, un
bâtiment monumental est également attesté (\gebIR{}), qui devait
servir au stockage de denrées alimentaires, les autres vestiges sont à
considérer comme des maisons. À \knw{}, quelques murs datent de cette
période, assez imposants, sans qu'un ensemble cohérent puisse être
dégagé.

\paragraph[Le \xvieme{} siècle]{Le \xvieme{} siècle
  (\cref{PlanHittite-UST-16})} ― Tous les témoignages assurent que le
\poternenmauer{} était déjà cons\-truit à cette époque, du moins à
\buyukkaya, \buyukkale{} et \nwh{}, l'érection de la deuxième
fortification de \bka{} et de l'\abschnittsmauer ayant rapidement
suivi. \Seeher{} suppose que le deuxième mur de \buyukkaya{} est
contemporain d'une occupation à \chantier{Nordstadt}, zone qui sera
abandonnée aux périodes suivantes, lorsque le nord du \poternenmauer{}
ne sera plus fonctionnel\footnote{\cite{Seeherinpressa}, qui évoque un
  ramassage de surface dans la zone \nordstadt{} pour justifier cette
  interprétation.}. Des constructions à \buyukkaya-\UP{} sont
attestées, tout comme à \MP{} les silos 8, 9, 11. Le lien
chronologique avec le silo de \nwh{}-\MP{} est inconnu. Ceux de
\buyukkaya{} pourraient avoir été construits après la destruction
partielle de celui-ci. En effet, le silo de \buyukkaya{} est
uniquement attesté pour le \xvieme{} siècle, au cours duquel une
partie a été carbonisée lors d'un incendie avec son
contenu\footnote{\cite{Seeher2006b}.}.  Les vestiges de cette période
à \buyukkale{} (phase \emph{BK-IVb}) sont encore assez maigres. Ce
n'est qu'à la période suivante que les premiers bâtiments monumentaux
sont attestés.  \Schachner{} considère que le bâtiment monumental
\kso{} avait un caractère religieux et daterait entre le \xviieme et
le \xvieme{} siècle\footnote{\cite{Schachner2015b}.}.  Puisqu'il n'y a
pas d'information sur la durée de l'occupation, je l'ai également
inclus à la période du \xveme{} siècle, mais peut-être a-t-il été
utilisé jusqu'au début du \xiieme{} siècle.  Au nord-ouest de
\kesikkaya{}, le \gebOP{} a été érigé\footnote{\cite{Strupler2013b}.}.
À \nwh{}, des constructions monumentales ont été mises au jour, sous
la \hah{} (Phase \emph{NWH-6}), mais n'ont été fouillées que
partiellement\footnote{\cite[23\psq]{Schirmer1969}.}. Un bâtiment
(\chantier{Pithoshaus}) était dédié au stockage tandis que le plan
d'un second (\chantier{Altbau}) ne correspond pas à ce qui est connu
pour les bâtiments à caractère religieux, de sorte qu'il est considéré
comme un bâtiment administratif, à l'image de la \hah{} qui sera
construite par-dessus au \xiveme{} siècle. Mon étude attribue
l'extension maximale de la \WTer{} et l'érection du \tempelI{} à cette
période.  Enfin, un bassin de rétention au nord de la \NWTer{} a dû
être établi.  Celui-ci est attesté tant par la géologie, grâce à un
carottage\footnote{\cite{Seeher2010a}.}, que confirmé par les
prospections par résistivité
électrique\footnote{\cite[104--107]{Schachner2012b}.} et par ondes
sismiques\footnote{\cite[98--100]{Schachner2015a}.}. On ne possède
aucun indice sur sa datation, mais il semble logique de le faire
apparaître en même temps que le \poternenmauer{}, dont la construction
a pu être concomitante du creusement de celui-ci pour récupérer des
matériaux. Sa taille est estimée d'après les prospections.


Le nombre de constructions attribuées à cette phase est
particulièrement impressionnant et invite à être prudent sur leur
contemporanéité. Cette activité particulièrement riche n'est sans
doute pas qu'une simple illusion, mais un phénomène d'ampleur plus
général propre au \xvieme{} siècle, visible dans la boucle du
\kizilirmak, où de nombreuses innovations et villes
apparaissent\footnote{\cite{Schachner2009b}.}. Toutes ces
constructions témoignent d'une «~fièvre de la construction~», née
d'une probable émulation.  D'un autre côté, il est difficile de faire
des différences  plus fines et, sans aucun doute, des constructions
sont à dater du \xviieme{} siècle, ou du moins les premières phases de
travaux. D'autres ont pu être finies, ou bien même construites
au début de la phase suivante. L'un des meilleurs exemples à avancer,
c'est le \tempelI{}.  Il est difficile d'envisager que le temple et
les magasins environnants soient apparus au même moment.


\paragraph[Le \xveme{} siècle]{Le \xveme{} siècle (\cref{PlanHittite-UST-15})}
― L'image de la ville basse au \xveme{} siècle est particulièrement difficile à
rendre. À \buyukkaya, il n'y a tout simplement pas d'activités
attestées\footnote{\cite{Seeherinpressa}.}. À \hah{}, le bâtiment pourrait avoir
disparu, puisqu'il n'y a pas de traces directes. Ce n'est qu'à \buyukkale{} que
les activités sont bien attestées (phase \emph{BK-IVa}).

\paragraph[Les \xiveme-\xiiieme{} siècles]{Les \xiveme-\xiiieme{}
  siècles (\cref{PlanHittite-UST-14-13})} ― À l'inverse du \xveme{}
siècle, de nombreux changements sont visibles pour les deux derniers
siècles \delaph{}. À \buyukkaya{}, la \chantier{Nordtor} du mur ouest
(\buyukkayawestmauer) est  bloquée et \Seeher{} suppose que le nord du
\poternenmauer{} n'était plus en fonction.  C'est la première fois que
l'\UP{} est pleinement utilisé, avec des bâtiments à l'ouest
(\chantier{Bau I}), des fours de potier à l'est et deux  batteries de
silos (\UP{} 1--6 et \MP{} 7--10). Les recherches de \Seeher{}
montrent que \buyukkaya{} est utilisé jusqu'au milieu du \xiiieme{}
siècle, mais peu à peu abandonné\footnote{\cite{Seeherinpressa}.}.
Pour la \WTer{} tout comme \kesikkaya{}, les attestations d'activités
sont moindres. En revanche, c'est de cette période que la \hah{} et le
palais monumental de \buyukkale{} sont à dater.

Cette reconstitution est en contradiction avec celles du \xxeme{} siècle \dne{}
qui présentaient justement ces deux siècles comme l'époque des
principales constructions monumentales avant qu'elles ne disparaissent dans un incendie
général lors du saccage de la ville. \Seeher{}, entre autres, a prouvé que
ce n'était pas une reconstitution plausible et que les vestiges archéologiques
témoignent d'un abandon progressif de la ville.

\paragraph{\xiieme{} siècle} ― \Seeher{} a démontré dès 2001 que la
ville n'avait pas été détruite par un événement unique, mais qu'elle
avait été peu à peu abandonnée\footnote{\cite{Seeher2001b}.}, et les
fouilles récentes ont apportées de nombreux nouveaux indices qui
étayent cette hypothèse. Dans la ville haute, de nombreux temples
n'étaient plus en fonction et un petit habitat s'était établi.  Les
temples et le palais avaient déjà été vidés de leurs biens précieux,
avant leur abandon. Si des archives administratives ont été retrouvées
à \buyukkaya{} ou \nisantepe{}, c'est parce qu'elles avaient peu
d'intérêt et n'ont pas été récupérées. Les fouilles dans la zone à
l'ouest de \sarikale{} montrent également un abandon progressif.  Le
quartier de la \WTer{} avait déjà été abandonné à cette période et les
dernières découvertes de \buyukkaya{} confirment cette interprétation.
\Seeher{} indique que certains fragments de tablettes cunéiformes
retrouvés à \buyukkaya{} (dans les décombres de la dernière phase
\delaph{}) sont jointifs avec ceux trouvés dans la ville basse et il
suppose que de la terre a été transportée de la ville basse vers
\buyukkaya{} à la fin \delaph{}, c'est-à-dire avant la fin de la
capitale hittite\footnote{\cite{Seeherinpressa}.}.  Cet argument vient
à l'appui de ma reconstitution de l'évolution de la \WTer{} et il est
nécessaire de regarder quelles causes ont pu être à l'origine d'un tel
développement.


Ces témoignages contrastent avec les activités des deux derniers rois,
\tuthalijaIV{} et \suppiluliumaII, qui sont les noms qui ont été
associés avec des réalisations prestigieuses et monumentales (stèles,
monuments de \yazilikaya{}, \kammerW{} et \nisantas). On possède, de
plus, de larges corpus de textes et de scellés avec leur nom, qui
attestent que la ville avait continué (ou repris) ses activités de
résidence cultuelle et de principal centre de gouvernement, du moins
\emph{pro forma}\footnote{Expression de \Seeher.}, alors que la ville
n'était plus à l'apogée de son rayonnement, mais en déclin. Sa survie
a dû en grande partie tenir à sa valeur symbolique et historique.
\hattusa{} a servi de lieu de légitimation du pouvoir royal et de son
idéologie, et, dans aucune autre ville, on ne retrouve une telle
concentration de monuments\footnote{\cite[117]{Schachner2011c}.}.  Les
raisons de l'abandon sont inconnues et à défaut de preuves, on reste
condamné à dresser une liste d'éventualités qui ont pu être
concomitantes : guerres inter-dynastiques, offensives ennemies, en
particulier menées par les \kaska{} provenant du Nord, mauvaises
récoltes à cause de la dégradation du climat et de la perte de
rendement des sols, interruption des routes de commerce et du système
d'impôts, révoltes des états vassaux, etc.  L'effondrement et
l'abandon de la ville prouvent en tout cas qu'elle n'était pas viable
sans l'existence d'une idéologie forte et d'un réseau pour
l'approvisionner.


