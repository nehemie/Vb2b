\section{La ville basse à la \pcactitre}


À l'image du \BA{}, les vestiges \delaperiodeCAC{} sont tous datés de la fin de la
période, et la résolution chronologique ne permet pas de restituer une vision
dynamique de l'occupation. La structure de l'établissement \delaperiodeCAC{} reprend les
grandes lignes de l'organisation de la phase précédente. \buyukkale{} sert de
résidence à l'élite, mais cette fois l'occupation inclut non seulement \nwh{} et
\buyukkaya{}, mais aussi la \WTer. La maison attestée à
\buyukkale{} (\gebIP{}, \ca{}  \SI{400}{\square\meter}) est considérée comme
une résidence de l'élite, notamment en raison de l'inventaire riche qui y a été
découvert. Néanmoins, la différence entre les habitations en dehors de
\buyukkale{} semble moins marquée que pendant le \BA{} : des bâtiments assez vastes
sont connus au \NoV{} (par exemple \gebUW) et les fouilles de \buyukkaya{} ont
aussi révélé un complexe de plusieurs pièces\footnote{\cite{SchoopBKinpress}.}. 

Le \pitgeb{}, à l'est des fouilles de \nwh{}, est un bâtiment qui se démarque
des vestiges d'habitation. D'après la reconstruction que j'ai
proposée\footnote{\cite{Strupler2013d}.}, il s'agit d'un grenier où le stockage
était organisé en d'immenses jarres (entre \SIrange[range-phrase = ~et~
]{1000}{1500}{\liter}) et dont la capacité totale, lorsque l'on additionne les
jarres, devait avoisiner les \SI{100000}{\liter}. J'ai ainsi proposé d'y
reconnaître un grenier royal, en accord avec les textes qui mentionnent un
fonctionnaire chargé de l'administration de greniers, le
\emph{rabi hurš\={a}tim} (attesté à \kultepe).

Il n'y a pas d'attestation de fortifications pour \laperiodeCAC{}
hormis quelques traces à \buyukkale{}. Néanmoins, il semble assez
probable que le site ait été fortifié à l'image des sites
contemporains de \buklukale{}, \konyakarahoyuk{} ou \kultepe{}. Je
propose de reconnaître le tracé de l'\abschnittsmauer{} comme un
candidat possible pour un tracé plus ancien qui pourrait marquer les
limites de l'occupation. Les fouilles de \Schachner{} dans la partie
nord de la ville basse n'ont pas encore atteint de tels niveaux pour
voir si l'occupation continue jusqu'au nord de la \nordT{} et la
documentation des fouilles de la longue tranchée de 1956 est trop
superficielle pour tirer toute conclusion définitive sur l'extension
du site. 

Le traitement par \Barjamovic{} de \hattus, le nom de la ville
\delaperiodeCAC{} de \bo{}, est un bon point de départ pour la
replacer dans son contexte
historique\footnote{\cite[292--297]{Barjamovic2011a}.}. D'après les
ressources épigraphiques, \hattus{} possédait un comptoir de marchands
assyriens, un roi (la reine est passée sous silence), et dominait une
région. Alors que l'écrasante majorité des textes date de la période
\emph{\kultepe{} Ib}, un texte retrouvé dans les décombres (plus
récents) de la \hah{} date de la période \emph{\kultepe{} II}. Cette
découverte, mise en perspective avec les sondages de \nwh{} et de
\buyukkale{}, qui ont révélé également des vestiges antérieurs
(notamment du \BA), a porté plusieurs scientifiques à considérer que
l'occupation ancienne était limitée à \buyukkale{} et \nwh{} et
s'était soit déplacée, soit étendue par la suite. Néanmoins, les
découvertes de \buyukkaya{} montrent qu'une phase plus ancienne avait
existé à cet endroit\footnote{\cite{SchoopBKinpress}.} et mon analyse
des dates radiocarbones de \knw{} a montré qu'une phase architecturale
peut avoir une durée d'occupation assez
longue\footnote{\cite{Strupler2013a}.}, surtout si l'on postule que
des changements de l'emprise au sol étaient de potentiels générateurs
de conflits (\supra{} \cref{planification-WTER-BM}). On pourrait
supposer que, dès la phase ancienne \delaperiodeCAC{}, une partie de
la ville basse était occupée. Ceci reste néanmoins à prouver. Les
vestiges matériels ne permettent de mener une analyse poussée que pour
la phase la plus récente.


%\subsection{La taille de \bogazkoy}

Les études paléodémographiques en sont encore à leurs débuts et on ne peut que
regretter l'absence de discussion à ce sujet, spécialement en Anatolie. Même si,
nous le verrons, chaque modèle repose obligatoirement sur de  nombreuses
prémisses, cette absence de discussion montre que l'histoire des
«~grands personnages~» occulte
l'étude de la population qu'il est difficile de
différencier pour les archéologues. C'est un phénomène problématique, puisqu'il s'agit
d'un cercle vicieux. Donner une idée de la
taille d'un site oblige à prendre en considération tout ce que l'on ne sait pas
et donc à extrapoler\footnote{Soulignons, s'il était nécessaire, qu'une telle
  évaluation ne sera jamais juste, mais plus ou moins fausse. Il existe un
  débat sur les limites d'une telle évaluation, qui transparait très bien dans
  les remarques de \Yon{} (\citeyear{Yon1992a}) adressées à \Garr{}
  (\citeyear{Garr1987a}), qui a «~osé~» proposer une estimation de la population
  à \ugarit{}. Je l'ai choisi comme exemple, puisque j'y reviendrai dans la
  partie sur \laph{}. \Yon{} critique, entre autres, le fait que \Garr{} ait utilisé
  dans son évaluation uniquement les plans (préliminaires), sans se préoccuper
  du volume des maisons  et insiste sur le fait que de toute façon, il était
  impossible de connaître la segmentation du site, car moins du quart
  a été fouillé. \Yon{} préfère l'estimation fondée sur les textes, proposée par
  \Liverani{} (\citeyear{Liverani1979a}). De mon point de vue, l'utilisation des
  textes n'est pas non plus sans problème, car il est difficile d'estimer ce
  qui est passé sous silence. Surtout, il convient de se demander quelle
  représentativité accorder aux vestiges archéologiques, si un site dont
  «~seulement~» un quart a été fouillé ne convient pas pour proposer une
estimation. Pour ces raisons, dans mon raisonnement, j'essaye simplement de
montrer quelles estimations ne peuvent pas être acceptées et quelle fourchette
semble acceptable.}.  Alors que certaines régions ou périodes ont reçu nettement
plus d'attention pour évaluer le nombre d'\habitantEs{} d'un site, les essais
sont rares pour l'Anatolie centrale. 

Dans une série d'articles dérivant de sa thèse de doctorat,
\Palmisano{} a créé une simulation d'interactions spatiales pour
comprendre  la hiérarchisation des sites \delaperiodeCAC{}  et
concevoir comment les principaux sites sont apparus et se sont
développés\footnote{\cite{Palmisano2015a,Palmisano2015b}.}. Sans
mettre en doute la méthode ou l'algorithme employé, les résultats de
ces simulations donnent une prépondérance à \bogazkoy{} par rapport
aux autres sites d'Anatolie centrale, ce qui est à mon avis fondé sur
un faux postulat\footnote{En l'absence des données et de la
simulation, on est bien en mal de vérifier ou de refaire la simulation
(\cite{Palmisano2015a,Palmisano2015b}).}. Dans sa simulation, la
taille du site joue un rôle central, puisqu'il s'agit d'un paramètre
directement incorporé dans le modèle, mais en plus, c'est à partir de
la taille du site que la population est
déterminée\footnote{\cite{Palmisano2015a}.}. D'après les données
inclues dans le modèle, \bogazkoy{} mesurerait \SI{65}{\hectare} et
serait plus vaste que \kultepe{} (\SI{50}{\hectare}) ou
\acemhoyuk{}\footnote{\cite[134]{Palmisano2015b}.}. La valeur de
\SI{65}{\hectare} pour le site de \bo{} est tout à fait exagérée et il
est adéquat de rappeler ici les mises en garde de \Bittel{} contre
l'utilisation de la superficie maximale de l'occupation à \bogazkoy{}
: «~\emph{Cette comparaison pourrait amener à des conclusions erronées
sur la densité des habitats et édifices et par suite sur l'importance
de la population de Hattusa ; de telles estimations ont été
effectivement suggérées tout récemment \emph{[référence à
\cite{Mora1977a}]}. Contrairement à Assour et à beaucoup d'autres
grandes villes du Proche-Orient ancien, comme, par exemple, Ugarit,
Mari, Karkemish, bâties sur un terrain plat ou presque plat, Hattusa
s'est développée sur un site extrêmement accidenté ; il y a des pentes
raides, des collines, des rochers escarpés et des ravins profonds qui
entrecoupent l'intérieur de la ville.  À côté, on y trouve également
des parties moins compartimentées convenant mieux à la construction de
cellules d'habitat ou d'édifices
officiels.}~»\footnote{\cite[485\psq]{Bittel1983b}.}


 D'un point de vue général, on regroupe les méthodes d'estimation de
la population à partir des vestiges archéologiques à trois cas
principaux\footnote{Rapidement synthétisé dans
\cite[126--128]{Chamberlain2006a}.} :
\begin{enumerate*}[label={\alph*)},font={\color{red!50!black}\bfseries}] \item
      l'étude de la taille des maisons couplée à la segmentation de
      l'occupation, \item une équation à partir de la taille des sites et, \item
    l'analyse du territoire d'approvisionnement. \end{enumerate*}


La dernière méthode ne peut pas être directement appliquée dans notre cas,
puisqu'on ne pourrait évaluer qu'un maximum possible pour un site autosuffisant
et indépendant, ce qui n'est pas le cas \alaperiodeCAC{} ou \alaph{}. Néanmoins, le
raisonnement inverse est à mon avis indispensable pour tester la
validité d'une
estimation. Quelle est l'empreinte écologique d'une ville de \num{20000} personnes
sur l'environnement. Quelle surface de champs cultivés est nécessaire ? En
ce qui concerne la deuxième méthode, même si, d'une façon générale, la taille
d'un site semble un bon indicateur pour déduire la population, elle ne peut en aucun
cas être employée telle quelle à \bogazkoy{}, puisque la topographie est très
inégale et accidentée et qu'elle ne peut pas être retranscrite par une simple
équation (d'où l'erreur de \Palmisano). En revanche, le travail sur l'occupation
(\supra{} \cref{Chapter4-Occupation}) permet de donner une bonne estimation de
la taille des maisons et de la densité de leur implantation. 

Dans la suite de ce développement, je propose une estimation qui
considère non seulement la taille du site, mais prend également en compte la
topographie et les résultats des fouilles. Néanmoins, avant de présenter le cas
de \bogazkoy, je fais une synthèse rapide des derniers résultats des les
recherches sur l'occupation de \kultepe{}, qui permettront de mieux mettre en
relief les estimations pour \bogazkoy{}.

\subsubsection{Excursus : la taille de \kultepe}


D'après les recherches de \Barjamovic{}, le site de \kultepe{} mesurerait entre
\SIrange[range-phrase = ~et~ ]{170}{230}{\hectare}, à raison d'une
population supposée de \num{150} individus par hectare, soit une population totale
comprise entre \numrange[range-phrase = ~et~]{25000}{35000}
\habitantEs{}\footnote{\cite{Barjamovic2014a}.}. Par ailleurs, il présume que la
ville était largement autosuffisante et que les villages répartis dans le
territoire de la cité-État -- il y a au moins \num{29} villages d'après les
textes -- ne participaient que peu ou pas du tout à la subsistance de la ville
et il penche plutôt pour une population de \num{25000} personnes. Pour mieux
concrétiser cette évaluation, il propose une consommation annuelle de \num{300}
litres de grains par personne d'après les résultats des recherches de
\Dercksen\footnote{Des textes indiquent une quantité d'au moins \SI{20}{\liter}
par mois par personne, \cite[93]{Dercksen2008a}.}. On peut remarquer que cette
moyenne correspond assez bien à la valeur moyenne de \SI{0.5}{\kilo\gram} par
jour par personne obtenue indépendamment par
\Seeher\footnote{\cite[294\psq]{Seeher2000}, d'après \cite{Ellison1981a}. Il est
étonnant de remarquer que \Barjamovic{} ne fait pas référence au travail de
\Seeher{}, dont l'étude de cas, les silos de \bogazkoy{} \alaph{}, est pourtant
assez proche dans le temps et l'espace.  Néanmoins, les valeurs obtenues
indépendamment et qui correspondent assez bien entre elles, montrent que les
modèles ont une certaine stabilité.}. Si l'on utilise la valeur de rendement de
\SI{40000}{\kgpersqkm},  on obtient une surface de
\SI{115}{\square\kilo\meter} pour produire les \SI{4575000}{\kilo\gram} par an
nécessaires pour \num{25000} personnes\footnote{
  (\num{25000}$\times$\SI{0.5}{\kilo\gram}$\times$\SI{365}{jours})/\SI{40000}{\kgpersqkm}.
  La valeur de rendement provient de \cite{Seeher2000}, mais correspond assez
  bien aux valeurs inédites mises en avant par Shy et rapportées par
  \cite[61]{Barjamovic2014a}. Shy a reconstruit la production de deux villages
  de \kultepe{}, qui produisaient respectivement \SI{36000}{\liter} sur
  \SI{50}{\hectare}, soit \SI{43200}{\kgpersqkm} et
  \SI{96000}{\liter} sur \SI{140}{\hectare}, soit
  \SI{41100}{\kgpersqkm} (selon l'équivalence de
  \SI{1}{\liter} de grains correspondant à \SI{0.6}{\kilo\gram} de grains).
  D'après les données de la division statistique de l'\emph{Organisation des
  Nations Unies pour l'Alimentation et l'Agriculture}, en Turquie en 1961 (la
  plus ancienne date disponible), le rendement de blé et de seigle était
  d'environ \SI{90000}{\kgpersqkm} et le rendement d'orge
  et d'avoine était de \SI{100000}{\kgpersqkm}.
  D'après la même source, le rendement de la production de blé en France en 2014
  était de \SI{750000}{\kgpersqkm}.}.  Cela correspond
théoriquement à la surface d'un anneau de rayon de \SI{6.5}{\kilo\meter}, une
surface généralement associée comme la zone d'exploitation d'un
site\footnote{\cite{WilkinsonTJ1994a}.} et dont le cercle central d'un rayon de
\SI{2}{\kilo\meter} équivaudrait à l'emplacement du site. Évidemment, ce modèle
ne donne qu'une idée vague de la surface nécessaire puisque personne ne se
nourrit exclusivement de grains (fruits, légumes, légumineuses, viandes, huiles
et autres  sont absents du modèle) et, de toute façon, il me semble difficile
d'accepter que la ville fût autosuffisante et ne reposât pas sur un système de
villages qui versaient une rétribution à \kultepe{} ou vendaient la
production\footnote{Je n'aborde pas ici la question de la production d'excédent
et le rapport entre la production des villages et de la ville.}.  Il s'agit d'un
simple calcul qui montre que le chiffre de \num{25000} paraît plausible.

