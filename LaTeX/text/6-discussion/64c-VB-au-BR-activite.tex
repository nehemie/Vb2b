\subsection{Les activités d'une capitale hittite}

Une ville prend une place particulière dans un réseau et, pour la qualifier
pleinement, il est nécessaire de déceler les activités qui n'y
prenaient pas place, puis de comparer avec les
autres villes du réseau pour comprendre comment une ville est viable. Chaque
ville est différente, non pas par le type d'activité qu'elle exerce,
puisqu'à
peu près partout les mêmes activités prennent place, mais c'est l'intensité avec
laquelle elles exercent une activité qui les différencie\footnote{Pour quelques
  notions sur la ville et ses activités au Proche-Orient, voir entre autres
  \cite{Dittmann2002a} ; \cite{Huot1990} ; \cite[52--57]{Novak1999} et plus
  particulièrement pour le contexte hittite voir \cite{Mielke2011a,
Mielke2013}.}. Une ville portuaire (comme \ugarit) ou le nœud d'un réseau
d'échange comme \kultepe{} \alaperiodeCAC{} ont des activités de commerce très
développées. L'une des particularités de \bogazkoy{}, ce n'est pas simplement sa
topographie. La taille de la ville et l'ampleur des monuments construits sont
spectaculaires et témoignent de l'idéologie royale et des ressources nécessaires
pour la maintenir\footnote{\cite{Schachner2011}.}. Aucune autre ville ne
présente un tel nombre de bâtiments monumentaux à la construction très soignée
et dispendieuse.  Les remparts illustrent bien comment un monument conjugue
utilité (dans ce cas, défense) et symboles (pouvoir, domination), mais les
remparts et leurs portes servent aussi de  point de contrôle et fragmentent
l'espace. À \hattusa{}, leur déploiement témoigne de la largesse dans leur mise
en œuvre avec \emph{plusieurs} remparts et non une approche «~minimaliste~» avec
un rempart autour de la ville, comme c'est le cas pour les autres villes
hittites de la
région \gls{acs}.


Parmi les activités qui sont bien attestées, on retrouve
l'administration et le gouvernement (palais et bâtiments officiels,
textes et scellés), la culture et les loisirs (temples, processions,
fêtes). Particulièrement bien marquées, mais ce n'est pas exclusif à
\bo{}, il y a les activités de stockage (silos, bassins).  Les
activités de résidence (palais et habitation) étaient plutôt dédiées
aux personnes liées à l'administration et aux temples. Les activités
liées à l'agriculture ou à l'artisanat sont infiniment moins visibles,
même si elles ne sont pas absentes, notamment lorsqu'elles témoignent
de la production d'objets à petite échelle.


À la fin des années 1970, un débat a été lancé par \Blanton{},
lorsqu'il a qualifié Monte Albán, l'ancienne capitale zapotèque
(Mexique, 300 \avne -- 700 \dne) de «~capitale déracinée~»
(\emph{disembedded capital})\footnote{\cite{Blanton1978a} ;
  \cite[62--66]{Blanton1999a}; voir également la critique comparative
  de \cite{Willey1979a}.}.  C'est un site qui a été fondé pour être
une capitale et dont la taille et les structures sont sans précédent.
La population n'était pas autosuffisante et sa viabilité reposait sur
un réseau d'échanges. Une des autres similitudes avec \bogazkoy{},
c'est que la ville possède les structures publiques et religieuses les
plus importantes.  Si l'on revient à \bogazkoy{}, \Beckman{}
considère que les textes transmettent l'impression que la ville de
\hattusa{} n'était pas une place de commerce majeure, mais que la
richesse accumulée et exhibée était assurée par le prélèvement
d'impôts, de tributs, et par les pillages lors des
conquêtes\footnote{\cite{Beckman1995a, Beckman1999}.}. En revanche, la
non-viabilité de Monte Albán n'a rien à voir avec celle de \hattusa{}.
Alors que le site mésoaméricain est presque cent fois supérieur aux
autres sites, le quartier d'habitation de \ha{} est très petit en
comparaison des monuments qui ont été créés.  Pour caractériser ce rapport, il est
primordial d'avoir une idée de la taille de la ville. Ce que je
propose, c'est de voir \hattusa{} comme une capitale déracinée, parce
que son rôle économique est minime et sa subsistance en tant que
capitale est uniquement possible grâce à sa place parmi les villes
hittites.


\subsubsection{Taille maximale de la ville basse}

À la différence \delaperiodeCAC{}, il est possible d'attribuer des fonctions, assez
stables au cours des siècles, aux différentes zones du site. Les données que je
présente ici,  ne sont que des indicateurs de grandeur et je les ai
établies en m'appuyant plutôt sur le \xvieme{} siècle. On peut considérer
\buyukkale{} comme le lieu de résidence de la famille royale\footnote{Même si le
  complexe palatial n'est attesté que pour le \xiiieme{} siècle, on peut
  supposer que l'élite dirigeante y a résidé tout au
  long \delaph{}.}, soit
  environ \SI{6}{\percent} de l'espace utile de la ville basse (les espaces
  trop escarpés et les abords du \bkderesi{} ne sont pas pris en considération).
  Le \tempelI{}  et les magasins qui l'entourent  occupent environ
  \SI{4}{\percent}.  Les différents bâtiments monumentaux (\kso{}, les complexes
  autour du temple, les prédécesseurs de \hah{}) occupent  \SI{2}{\percent} et
  le silo de \nwh-\MP{} couplé à ceux de \buyukkaya{} occupent{}
  \SI{1.5}{\percent}.  Ces quelques chiffres soulignent à quel point l'espace
  urbain est encore très peu connu et les recherches antérieures n'ont jamais
  tenté de formaliser une segmentation. Même si l'on additionne toute la place
  disponible (\nwh, \kesikkaya-\hah, \unterstadt, \NWTer{}, \nordstadt), ce qui
  représenterait presque \SI{80}{\percent} de la ville basse, la surface ne
  serait «~que~» de \SI{30}{\hectare}. De plus, on peut noter que \buyukkaya{}
  et \nwh-\MP{} semblent n'avoir jamais été
  occupés\footnote{\Cref{occupation-nwhmp-seeher,occupation-nwhmp-schachner}.}.
  Le principal problème vient de l'évaluation de la ville haute, puisque, même
  si il est  désormais attesté qu'elle est occupée dès le \xvieme{} siècle, on
  ignore l'extension de son occupation et le rôle de ses différentes
  parties. En tout cas,  la majeure partie  était dédiée aux bâtiments religieux
  et administratifs\footnote{Les prospections par résistivité au-delà des murs
  de la ville tendent à montrer que les environs directs n'étaient pas
densément peuplés (voir \cite[156--158]{Schachner2013} ;
\cite[28--30]{Schachner2014a}).}. 


Les estimations sur la ville sont assez rares. \Mora{} a estimé la population de
la ville basse entre \num{9000} et \num{11000}
habitants\footnote{\cite{Mora1977a}.} -- un chiffre improbable.
\Bittel{} proposa d'estimer sa
population totale (ville basse et ville haute) entre \num{9000} et
\num{15000}\footnote{\cite[\XX{} 85]{Bittel1983}.} et
\Schachner{} entre  \num{10000} et
\num{12000}\footnote{\cite[241]{Schachner2011}.}. \AlaperiodeCAC{}, j'ai évalué une
population maximale de \num{5000} personnes pour \SI{25}{\hectare}, mais je
considère qu'elle était plutôt de \num{2000}-\num{4000}. Pour \kusakli{}
(\SI{18}{\hectare}), \MullerKarpe{} a évoqué une population de \num{5000}, mais
\Mielke{} considère ce chiffre trop
élevé\footnote{\cite{Muller-Karpe2002}, \cite{Mielke2011a}.}. Il n'existe pas
d'autres \emph{comparanda} directs, mais pour \ugarit, qui mesurait au
moins \SI{25}{\hectare}, la population a été estimée entre \num{6000} et
\num{8000} habitants, et les plans à disposition montrent que la densité de
l'occupation à \ugarit{} était bien plus élevée qu'à
\bogazkoy\footnote{\cite{Liverani1979a, Yon1992a}.}.  L'estimation de \Casana{}
pour \alalakh{} est de \num{350}--\num{400} maisonnées pour \SI{20}{\hectare},
qu'il met en équation avec une population comprise entre \num{2100}--\num{4000},
une estimation qui  serait trop élevée, car elle ne prend pas  en considération
le complexe palatial et les divers monuments\footnote{\cite{Casana2009a}.}.


La population totale de \hattusa{} est difficile à préciser, car cela
nécessiterait une analyse plus détaillée de la segmentation de la
ville haute, mais il semble raisonnable de ne pas dépasser les
\num{10000} habitants. Par ailleurs, il est particulièrement difficile
de savoir comment intégrer les esclaves, captur\Ees{} lors des
campagnes armées qui sont relat\Ees{} dans les textes dans cette
estimation.


\subsubsection{La place de \bogazkoy{} dans le \royaumehittite{}}


Il est nécessaire de penser à la place de \bogazkoy{} au sein du
territoire hittite\footnote{La prospection aux alentours de
  \bogazkoy{} n'est connue que par des rapports préliminaires qui ne
  permettent pas de poser des questions sur la micro-région. Voir
  \cite{Czichon1999a, Czichon2001a}, avec bibliographie antérieure.},
car le terme même de capitale n'est pas sans poser de problème ni être
sujet à débat. Du point de vue philologique, hameau, village, ville ou
capitale sont exprimés avec le déterminatif sumérien
\textsuperscript{\textsc{URU}} et c'est le traducteur qui est
confronté à la classification.  Les Hittites avaient un réseau
complexe de villes et le couple royal et sa suite passaient une bonne partie de son
temps à voyager et à résider plus ou moins temporairement dans
différentes villes. \hattusa{} était la capitale politique et
religieuse du royaume, hormis lors de l'épisode de \tarhuntassa{} ou
lorsqu'elle a été déplacée au loin de menaces ennemies trop
pressantes\footnote{\cite{Dogan-Alparslan2011b}.}. Le quartier
résidentiel, au moins celui mis au jour par les fouilles, n'a jamais
été réoccupé à la suite de ces troubles politiques (voir \supra{}
\cref{56-Abandon}). Quoi qu'il en soit, la ville
n'a pu fonctionner que dans un réseau.

Il reste encore tout un travail à faire pour passer de l'étude de  l'espace
urbain à celui des réseaux urbains et comprendre les relations entre les
villes, leur interaction, compétition, solidarité, concurrence ou répartition
des activités\footnote{Pour des efforts dans cette direction, voir
\cite{Okse2001a,Okse2005a,Okse2006a,Okse2014b}.}.  \Mielke{} a classé les villes
en capitale (\hattusa), résidence royale (\sapinuwa{} : \ortakoy), ville
frontière (\tapikka{} : \masathoyuk) et capitale de province (\sarissa{} :
\kusakli)\footnote{\cite{Mielke2011a, Mielke2013}.}. \Glatz{} avait initié une
classification similaire\footnote{\cite{Glatz2005a}.} et  \Omura{} a proposé de
voir \kamankalehoyuk{} comme un grenier centralisateur et
redistributeur\footnote{\cite[1106]{Omura2011a}.}, une proposition bienvenue qui
complexifie un tout petit peu ce réseau.  Les attestations montrent que la ville
de \bogazkoy{} était une ville «~démesurée~» au vu du grand nombre de
ses bâtiments monumentaux et astreinte à survivre uniquement au sein d'un
réseau qui assurait sa viabilité.


