Selon \Huot{}, qui a défendu l'approche sociologique, la ville possède
de multiples facettes politiques et idéologiques, elle est tissée dans
un réseau de relations vers l'extérieur, elle se caractérise par sa
diversité sociale et économique, ses diverses fonctions et une
spécialisation dans de multiples activités\footnote{\cite[11.
24--26]{Huot1990}.}. La ville est un centre économique régional, en
particulier un centre de production d'artisanat spécialisé, un centre
religieux, un centre politique et un centre administratif.  Les
villages des alentours, appelé l'arrière-pays, et la ville sont
interdépendants, puisque le ravitaillement d'une ville dépend de
l'arrière-pays (une large partie de la population d'une ville ne
produit pas de ressources alimentaires) et les villages et hameaux des
alentours sont dépendants de la ville pour l'acquisition de nombreux
produits à valeur ajoutée et l'écoulement de leur production. Les
matériaux bruts peuvent provenir de l'arrière-pays ou bien de régions
plus éloignées grâce à des échanges à longue distance. La ville est
donc au centre d'un vaste réseau d'échanges diversifiés.

Un marqueur physique de la ville préindustrielle, mais qui n'est pas
systématique, c'est le rempart qui l'encercle et la protège, et qui
devait également servir à abriter la population de l'arrière-pays en
cas de guerre ou de siège. La diversité des fonctions d'une ville se
retrouve dans sa topographie qui le plus souvent peut être distinguée
en différentes grandes zones fonctionnelles, comme les quartiers
d'habitation, les temples et les palais (palais royal, du gouverneur,
de justice, etc.). Toutes les parties sont reliées grâce au réseau
viaire, qui permet de circuler d'une zone à l'autre et à l'intérieur
de celles-ci et qui est organisé en différents niveaux de
hiérarchisation (rues principales, secondaires, etc.). L'apparition
des villes est le plus souvent considérée comme l'un des changements
des plus importants dans les modes de vie, à l'image de la
sédentarisation et de l'agriculture, et dont l'influence jusqu'à la
période contemporaine ne peut pas être mise en doute.

\subsection{Les premières villes en Mésopotamie et en
Anatolie}\label{111-PremieresVilles}

L'émergence des villes au Proche-Orient au \ivemeMil{} millénaire anime
un débat ardent et constamment nourri de nouvelles thèses : quand,
comment et pourquoi les premières villes apparaissent-elles ? En 1950,
\Childe{} a publié un article intitulé «~la révolution urbaine~»
(\emph{The Urban Revolution}) qui tente de faire une synthèse sur les
foyers d'apparition du phénomène urbain qu'il accompagna d'une liste
de dix critères pour distinguer les villes des
villages\footnote{\cite{Childe1950a}.}, très souvent débattus. Sans
revenir sur le processus d'urbanisation en Mésopotamie\footnote{À ce
sujet, consulter \cite{Butterlin2003a}; \cite{Pollock1999a}.}, il
est important de retenir que l'urbanisation marque un changement
fondamental dans les modes de vie. En effet, cette genèse s'accompagne
d'une diversification des réalisations architecturales, d'une
accentuation de la stratification sociale et d'une spécialisation
accrue des activités. Il est désormais bien admis que ces
transformations sont le résultat d'un long processus et non d'une
révolution, dont «~l'étape~» précédente avait été la néolithisation.

L'émergence des centres urbains en Anatolie est loin d'être uniforme
et le Sud-Est, où l'urbanisme est plus prononcé, fait figure
d'exception\footnote{\cite{Bachhuber2016a,Cevik2007a,Huot1990}.}. Il
ne s'agit pas d'une invention indépendante, puisqu'il est assez clair
que l'apparition de l'urbanisme va de pair avec l'intensification des
échanges avec les régions voisines, en particulier avec la Mésopotamie
(voir \cref{I-BA}). C'est à la fin du \iiiemeMil{} millénaire que le
nombre et la taille des agglomérations augmentent assez rapidement et
que la distinction entre ville et autres formes d'agglomération
s'accentue. C'est dans ce contexte que l'on assiste à la naissance de
la cité-État en Anatolie.

\subsection{Le concept de cité-État pour l'Anatolie du début du
  \iiemeMil{}
millénaire} \label{112-Concept-CiteEtat}

Le terme de cité-État, tout comme celui de ville, est polysémique. Il
fait référence à la ville, à son statut politique et à son territoire.
Ce terme est fréquemment utilisé mais avec une définition assez
imprécise. Cette flexibilité a permis d'utiliser le terme, souvent
avec de bon résultats, comme dénominateur commun pour des approches
comparatistes\footnote{\cite{Glassner2004a, Hansen2000a,
Hansen2002a}.}. Ce terme est systématiquement employé pour décrire
le paysage politique de l'Anatolie centrale \delaperiodeCAC{}. D'après
ce que l'on sait des textes et des vestiges matériels de cette
période, on peut définir une cité-État comme une ville insérée dans un
réseau de villes qui partagent un bagage linguistique, culturel,
historique et religieux commun, mais dans lequel l'autorité de chaque
(cité-)État (législatif, judiciaire, politique, économique) est
indépendante et spécifique à chacune. La diplomatie, la guerre et le
commerce ajustaient les relations entre les cités-États. On
imagine\footnote{Rares sont les travaux qui ont analysé le territoire
et le fonctionnement d'une cité-État concrètement. Voir
\cite{Barjamovic2014a} pour une première tentative.} que le
territoire gouverné par la cité-État entoure la ville principale et
comprenait en général plusieurs villages. Même si la cité-État n'est
pas autarcique, mais active dans le commerce avec d'autres
cités-États, elle semble autosuffisante pour tous ses besoins de base.
C'est sans doute la proximité culturelle et les fortes relations
d'interdépendance entre les cités-États qui doit faire prévaloir
l'usage de ce terme plutôt que celui de «~micro-État~», qui suggère
plus d'indépendance, puisqu'il est rare de parler d'une cité-État
isolément et beaucoup plus courrant de le faire d'une constellation de
cités-États. Le réseau de cités-États semble indispensable pour la
survie de celles-ci et l'effritement du réseau annonce la disparition
des cités-États.


\subsection{Le concept de capitale du royaume} \label{113-ConceptCapitale}

La notion de capitale est indissociable de celle de royaume, puisque
c'est la ville la plus éminente et, souvent, elle symbolise le royaume
et son organisation. En tant que figure symbolique forte de l'État, la
capitale regroupe et exhibe, le plus souvent sans partage avec
d'autres villes, toutes les fonctions de commandement (politiques,
économiques, militaires, religieuses).  Placée, par définition, au
centre du réseau de commandement, c'est autour d'elle que gravitent
les autres villes du royaume. La capitale est un emblème du pouvoir et
ceux qui la détiennent n'ont pas hésité à utiliser leur capital pour
rendre ce pouvoir visible, voire omniprésent. La capitale abrite donc
des lieux symboliques qui servent de référents pour l'identification à
l'idéologie du pouvoir. Compte tenu de leur prestige, de l'influence
qu'elles exercent et des nombreux exemples contemporains et
historiques à disposition, les capitales font également l'objet
d'études comparatistes\footnote{\cite{Charle2002a, Clark1996a}.}.
Celles-ci se concentrent en général sur les bâtiments monumentaux et
de pouvoir pour voir comment le pouvoir se met en scène, dans une
approche de géographie du politique\footnote{Par exemple
\cite{Fauve2009a}, avec références.}.

L'inconvénient dans cette approche des villes capitales est qu'il est
encore rare de s'intéresser en détail à l'envers de leur décor
monumental. La ville s'observe à différentes échelles qui
s'entremêlent : l'échelle de la personne, celle plus large du groupe
et enfin l'échelle régionale.  L'architecture des bâtiments,
l'urbanisme et les activités reflètent non seulement des contraintes
écologiques, technologiques et chronologiques, mais possèdent
également une dimension sociale. Parmi toutes ces facettes, dans
l'étude d'une capitale, les lieux de pouvoir, de décision ou
«~enchantés~» suscitent en général une attraction dominante. Lors de
la description d'une ville comme Paris, il est d'usage de décrire des
lieux comme la tour Eiffel, la Sainte-Chapelle ou Disneyland plutôt
que Neuilly-sur-Seine ou Saint-Denis.  L'archéologie suit cette
tendance avec un intérêt porté, en général, sur les ensembles
prestigieux et monumentaux, délaissant souvent les quartiers
résidentiels. Cependant, nettement plus sensibles aux évolutions que
les bâtiments religieux ou les complexes monumentaux, les habitations,
lieux de résidence de la population d'une ville, ont un potentiel
équivalent et surtout alternatif pour l'étude d'une société. Les
habitantes et les habitants jouent un rôle actif dans la formation de
la société -- quelle que soit leur position sociale. L'analyse d'un
quartier d'habitation ouvre une voie indépendante pour révéler les
structures sociales d'une ville. \Huot{} a par ailleurs indiqué
comment une telle étude est possible : \emph{Si [l]a morphologie
physique [d'une ville] renvoie obligatoirement à sa morphologie
sociale, l'analyse socio-économique d'une ville passe
obligatoirement par sa description
formelle}\footnote{\cite[11]{Huot1990}.}.

\subsection{Potentiel de l'étude d'une cité-État devenue capitale de royaume}
\label{114-Potentiel}

Dans la présente étude, je concentre mon attention sur la capitale
hittite, \hattusa{}, pour interroger le phénomène urbain et souligner
le rôle des quartiers domestiques. N'ayant pas été
déposé\es{}\footnote{La rédaction épicène, aussi dit langage non
sexiste et que j'ai essayé d'employer dans ce document, vise à
promouvoir la mixité et l'égalité des genres. Les points pour les
formes contractées sont destinés à éviter les discriminations.} dans
le berceau des civilisations, les Hittites ne sont étudié\es{} que
marginalement, tant par les archéologues du Proche-Orient que dans des
études comparatives\footnote{Par exemple, le sujet est absent chez
\cite{Trigger2003a} et \cite{Huot1990} reste une exception. Il est
évident que ce n'est pas le cas de la langue hittite, qui est la
plus ancienne langue «~indo-européenne~» attestée et elle fait, à ce
titre, partie intégrante d'études comparatistes.}. Situé au cœur de
la boucle du \kizilirmak, au nord de l'Anatolie centrale, le site de
\bogazkoy{} est une ville tout au long du \iiemeMil{} millénaire et est l'objet de recherches archéologiques depuis plus
d'un siècle\footnote{ \bogazkoy{} peut se traduire, à la suite de
\Texier{}, par «~le village du défilé~» (\cite[210]{Texier1839}).
Depuis 1935, l'appellation officielle du lieu est Boğazkale
(\cite[235 note 1]{Alaura2006}), un nom qui ne s'est jamais frayé de
chemin dans la littérature scientifique. Le terme de \bogazkoy{}
dénote le site archéologie en tant qu'espace alors que \hattusa{}
fait référence spécifiquement à la situation politique de la ville
dans la deuxième moitié du \iiemeMil{} millénaire.}. Tout d'abord
connue sous le nom de \hattus{}, c'est une cité-État qui accueille un
comptoir commercial pendant la période \descac{} (\ca{} 1950--1750).
L'emplacement sera choisi par la suite pour établir la capitale
hittite, \hattusa{} (\ca{} 1650--1180). Même si un siècle de
recherches a permis de lever une partie du voile mystérieux qui
recouvrait les ruines, seule une fraction du site est connue.
%
Parmi les recherches effectuées, les bâtiments officiels -- temples ou
palais -- ont longtemps été au centre des préoccupations. Ils ont fait
l'objet de multiples publications et leur importance a largement été
perçue dans la littérature scientifique. Néanmoins, un important
matériel sur les vestiges de l'architecture domestique a été accumulé
au fil des campagnes de fouilles. Il est resté peu étudié et souvent
relégué au second plan\footnote{Dans sa synthèse sur \bogazkoy{}
(\ca{} 200 pages de texte), \Bittel{} consacre une seule page au
quartier d'habitation de la période hittite (\cite[60]{Bittel1983}).
Pour un aperçu critique sur les villes hittites, voir
\cite{Mielke2011a,Mielke2013}.}.  Les résultats de plusieurs années
de fouilles dans la ville basse sont toujours, en grande partie,
inédits. En focalisant ce travail sur l'architecture domestique de la
ville basse, il est possible d'éclairer une nouvelle facette de la vie
citadine à \hattusa{} et de l'organisation des sociétés du \iiemeMil{}
millénaire. Néanmoins, avant de présenter un portrait de la ville de
\bogazkoy{} (\cref{sec-13-Compendium-Bogazkoy}), afin de mieux saisir
ses développements, la prochaine section (\ref{120-EtatDeLaRecherche})
rappelle le contexte général de l'Anatolie centrale.
