Le paysage de cette région, déboisée à l'heure actuelle, a largement
été modifié par l'activité humaine au cours des millénaires, notamment
à cause de l'agriculture et de l'élevage
extensifs\footnote{\cite{Asouti2014a, Marsh2010a, Marsh2014a,
    Wright2015a}.}. Les séquences de pollens indiquent assez
clairement l'implantation de forêt de chênes et de pins dans cette
région dès le début de l'Holocène, dont l'extension maximale se situe
vers 3000 \avne{} avant de régresser
brutalement\footnote{\cite{Roberts2011b, Woldring1991a}.}. Cette
déforestation a souvent été accompagnée d'une forte érosion, diminuant
progressivement la fertilité des sols.

Pour se donner une idée du climat, on peut commencer par regarder les
données actuelles de température et de pluviométrie des préfectures
des provinces de la région \gls{acs} : \kirikkale{}, \kirsehir{},
\yozgat{}, \sivas{}, \tokat{}, \amasya{}, \corum{}\footnote{Par
  rapport à \bo{},
%%
\kirikkale{} se situe à \SI{120}{\kilo\metre} à l'ouest ; \kirsehir{}
à \SI{130}{\kilo\metre} au sud-ouest ; \yozgat{}    à
\SI{40}{\kilo\metre} au sud-est ; \sivas{}     à \SI{250}{\kilo\metre}
à l'est ; \tokat{}     à \SI{200}{\kilo\metre} à l'est nord-est ;
\amasya{}    à \SI{180}{\kilo\metre} au nord-est ; \corum{}     à
\SI{130}{\kilo\metre}  au nord-est.}.
%
%
Cette région connaît de nos jours un climat continental avec un hiver
très froid et un été chaud et sec. La moyenne annuelle des
températures varie entre \SI{-0.3}{\celsius} (température moyenne du
mois de janvier à \corum{}) et \SI{21.3}{\celsius} (température
moyenne du mois de juillet à \corum). Les précipitations sont
reparties de l'automne au printemps pour des moyennes annuelles par
mètre carré de \SI{575}{\milli\metre} à \yozgat,
\SI{433}{\milli\metre} à \corum{} et \SI{379}{\milli\metre} à
\kirikkale{} (\cref{weather}). Cette moyenne des précipitations est
largement au-dessus du minimum nécessaire pour pratiquer une
agriculture sèche et il est possible d'induire que des changements
climatiques eurent lieu au fil des millénaires, à partir des séquences
de pollens et de sédimentation, en particulier grâce aux données du
lac Tezer (région de \sivas{}). La tendance générale montre que le
climat, plus humide au début de l'Holocène, tend à devenir plus sec et
cette séquence est marquée par d'importants épisodes de
sécheresse\footnote{\cite{Kuzucuoglu2011a, Roberts2011a}.}.


\subsection{Chalcolithique (\ca{} 6000--3000)} \label{122-chalco}

Alors que des traces sporadiques d'occupations paléolithiques et
mésolithiques de cette région sont attestées dans cette cette
région\footnote{\cite[31--46]{During2011a}.}, aucun site n'est pour
l'instant connu entre \num{10000} et \num{6000}. Cette séquence
contraste avec ce que l'on connaît pour la région directement au sud,
où de larges sites néolithiques se sont développés tels que
\asiklihoyuk, \musular{} ou
\tepecikciftlik\footnote{\cite{Ozdogan2012a}.}. On peut supposer que
l'absence totale de site est peut-être le reflet du manque de
recherches plutôt que l'attestation d'une réelle absence, surtout si
les sites étaient petits et occupés brièvement, ne formant ainsi pas
de tells, à la différence des régions plus au
sud\footnote{\cite[229]{During2011a}, suppose que la végétation, la
  taille et la nature des sites ont pu être des facteurs importants
  qui ont empêché de retrouver des sites néolithiques lors des
  prospections, qui, pour la plupart, ont été extensives. Voir
  également \cite[447]{Baird2012a}.}.


La période du Chalcolithique (\si{6000}--\si{3000}) est d'avantage connue, même
si elle est longtemps restée ignorée par la recherche (\cref{Map-Chalco-Ba}). La
chronologie est problématique, car les assemblages céramiques sont très
régionalisés (en particulier au Chalcolithique ancien et récent), ce qui rend
difficile l'établissement de séquences fiables basées sur une étude
stratigraphique comparée\footnote{\cite{,Schoop2005, Schoop2011b}; voir
cependant les travaux récents de \cite{Godoninpressa}.}. La période ancienne
reste marginalement explorée et c'est à \bogazkoy, en particulier à
\buyukkaya{}, que les restes les plus anciens pour cette région ont été
retrouvés. L'occupation date d'environ 5600\footnote{\cite{SchoopBKinpress}.}.
Le Chalcolithique moyen commence à être appréhendé à travers des sites comme
\guvercinkayasi{}\footnote{\cite{Gulcur1997a, Gulcur2009a}.} ou \cadirhoyuk{},
mais c'est surtout la période récente qui est mieux documentée grâce aux sites
-- du nord au sud -- de \dundartepe{}, \alacahoyuk{}, \yarikkaya{},
\camlibeltarlasi{}, \cadirhoyuk{} et
\alisarhoyuk{}\footnote{\cite[229--240]{During2011a}.}. Le site de
\camlibeltarlasi{}, qui jouxte directement le site de \bogazkoy{}
(\SI{2}{\kilo\metre} à l'ouest) et dont l'exploration est récente, donne une
bonne idée des activités d'un village du Chalcolithique récent (\ca{}
\si{3590}--\si{3470})\footnote{\cite{Schoop2008b, Schoop2009b, Schoop2010b,
Schoop2011a}.}. Le site d'environ \SI{50}{\square\meter} de surface est composé d'une
architecture unicellulaire simple et ses quatre phases d'occupation (\emph{ÇBT
I--IV}) ont révélé des activités de métallurgie, dont, entre autres, la
réduction de minerai de cuivre et la production de cuivre à
l'arsenic\footnote{\cite{Rehren2010a}.}. Ces activités étaient organisées à
l'échelle de la communauté.

Le Chalcolithique est la période durant laquelle la métallurgie prend son essor,
reflétant non seulement l'appropriation de nouvelles technologies, mais aussi
l'acquisition de nouvelles ressources et une spécialisation (au moins partielle)
de la production\footnote{\cite[539--544]{Lehner2014a}.}. Même si la région
n'est pas intégrée dans un réseau d'échanges aussi vaste et intense que celui
d'\arslantepe{} plus à l'est\footnote{Le site d'\arslantepe{}
(\cite{Frangipane2011a}) est particulièrement connu pour avoir livré, au
Chalcolithique récent, du matériel céramique similaire à celui du site
mésopotamien d'Uruk. La nature de ces relations avec l'élite locale est très
débattue (par exemple : \cite{Algaze2005a, Frangipane2001a, Rothman2011a}).}, la
multiplication des contacts et des échanges entre une multitude de communautés
régionales est évidente. Les disparités entre les sites au sein de la région
restent importantes (différences dans la taille des sites, l'agencement du bâti
et de l'espace) et leurs trajectoires sont tout aussi diverses : certains sites
sont abandonnés (\camlibeltarlasi), d'autres se développent en de larges
communautés à la période suivante (\alacahoyuk{} ou \alisarhoyuk{}).


\subsection{\BA{} (\ca{} 3000--2000)} \label{I-BA}

La phase ancienne du \BA{} est presque inconnue dans la région \gls{acs}
(\cref{Map-Chalco-Ba}) et commence à être révélée grâce aux découvertes de
\cadirhoyuk\footnote{\cite[121--124]{Steadman2013a}. Pour un aperçu général de la
période voir \cite{Bachhuber2016a} et sur l'architecture domestique voir
\cite{Perello2011a}.}. En revanche, à partir de la deuxième moitié du
\iiiemeMil{} millénaire, des témoignages plus nombreux illustrent des interactions intenses
avec les régions alentours.  Les premiers vases tournés apparaissent à cette
période (un millénaire après le début de l'utilisation du tour de potier en
Mésopotamie), en particulier des petits bols à base pointue. L'introduction de
cette technologie coïncide avec l'apparition des vases dits \emph{depas
amphikypellon} dans les régions directement voisines. Les bols tournés de la
région \gls{acs} correspondent bien aux \emph{depas}, étant tous les deux des
vases à boire à la base pointue qu'il est impossible de (re)poser tant que le
contenant n'est pas vide. De nombreux indices invitent à y voir un set dédié à
la consommation de boissons alcoolisées, en particulier de vin, lors de fêtes
aristocratiques\footnote{\cite[18]{During2011a}; \cite{Schoop2011b}. Voir
\cite{McGovern1996a}, sur l'apparition de la viticulture à cette période.}.
Néanmoins, il est important de souligner que même dans les phases tardives du
\BA{}, l'essentiel de la poterie est montée et la production de la céramique
tournée n'est pas associée avec une production de masse ; cette céramique
tournée était d'ailleurs «~d'assez mauvaise
qualité~»\footnote{\cite[148--150]{Schoop2009a}.}.


L'une des innovations majeures de cette période est l'introduction de la
métallurgie du bronze (alliage de cuivre et d'étain), qui est intimement liée à
la réorganisation de la production et de la consommation des métaux. Cette
métallurgie se spécialise avec l'apparition de sites dédiés à l'extraction et à
la réduction du minerai de cuivre, comme en témoignent les découvertes à
\derekutugun, à l'ouest de \corum\footnote{\cite{Yalcin2011a,Yalcin2013a}.}, ou
celles d'étain au pied du massif de l'Erciyes\footnote{\cite{Yener2015a}.}. Ces
sites s'inscrivent dans le modèle développé par \Yener{}, qui souligne la
réorganisation de la production en Anatolie à cette
période\footnote{\cite{Yener2000a}.}, en rupture avec la période précédente.
L'apparition de sites «~spécialisés~» ne peut avoir lieu que grâce au
développement de sites «~primaires~» qui centralisent la production de surplus
et qui organisent les échanges avec d'autres régions. Même si cette
réorganisation hiérarchise les sites à cette période, il ne semble pas opportun
de parler d'urbanisme pour cette région\footnote{\cite{Cevik2007a}.}
et, à
l'intérieur même de la région \gls{acs}, les disparités semblent très grandes.
Les sites de la région sont mal connus, puisque, pour la plupart, ils
ont été réoccupés lors
\delaperiodeCAC{}, comme \bogazkoy, \alisar, \acemhoyuk, \eskiyapar{} ou
\karahoyuk{}.

L'apparition de nécropoles \extramuros{} met la disparité entre les communautés
particulièrement bien en valeur. Les 14 tombes d'\alacahoyuk{} sont les plus
fameuses et témoignent de l'ensevelissement d'objets extraordinaires dans des
tombes lors de cérémonies dispendieuses\footnote{\cite{Bachhuber2011a} avec
références antérieures.}. Des sépultures similaires, mais moins ostentatoires,
ont également été retrouvées à \mahmatlar\footnote{\cite{Kosay1950a}.},
\kalinkaya{}\footnote{\cite{Zimmermann2007a}.} et
\horoztepe\footnote{\cite{Ozguc1958a,Ozguc1964a}.}. D'autres nécropoles
\extramuros{} sont attestées à
\resuloglu\footnote{\cite{Yildirim2006a,Yildirim2012a}.} ou
\salurnorth\footnote{\cite{Matthews2004a}.}.


On peut donc souligner l'intensification des échanges, la
diversification des formes d'occupation de l'environnement, la
réorganisation de la production spécialisée, ainsi que la naissance
d'une élite. A. \SherrattA{} et S.  \SherrattS{} ont explicité, dans
un modèle sur l'économie de l'\ageBronze{}, comment l'augmentation des
rivalités entre les élites locales − dont les intérêts dans
l'acquisition d'\emph{exotica} (en particulier métal et textile) − a
catalysé les échanges à longue distance, avant d'initier des réseaux
d'échanges plus soutenus et
diversifiés\footnote{\cite{Sherratt1991a}.}.

Pendant le \BA{}, les disparités entre les différentes régions de l'Anatolie sont
importantes. Ainsi, alors que les régions au sud de la chaîne de montagnes du
Taurus sont, dès le \BA{}, en contact étroit avec la Mésopotamie et que les
premières villes apparaissent (ex. \titrishoyuk\footnote{\cite{ Algaze2001b,
Matney2000a, Nishimura2014a}.}), seul le site de \kultepe{}, tout juste au sud
de notre région, indique une trajectoire similaire. Les récentes découvertes de
scellements à \kultepe{} datant de la fin du \BA{} prouvent l'intensité des
contacts et la mise en place d'un système régulier
d'échanges\footnote{\cite{Ezer2014a, Kulakoglu2013a}.}. Les fouilles,
concentrées sur des bâtiments imposants dans la partie sommitale du tell, ne
permettent pas de répondre sans équivoque à la question du caractère
urbain du site.
En revanche, à la toute fin du \BA{}, ce processus est fortement visible dans le
répertoire céramique de la région \gls{acs} avec la période dite transitoire (ou
intermédiaire) comme à \bogazkoy\footnote{\cite[148--150]{Schoop2009a}.},
\masathoyuk\footnote{\cite{Emre1989a}.},
\alisar\footnote{\cite[14--21]{Orthmann1963a}.},
\mercimektepe{}\footnote{\cite{Ozcan1993a,Zoroglu1977a}.} ou
\acemhoyuk\footnote{\cite{Emre1966b}.}. Leur céramique peinte montre sans aucun
doute l'adoption d'un nouveau répertoire qui annonce directement \laperiodeCAC{}.


\subsection{\Pcactitre{} (\ca{} 2000--1750)} \label{124-BM}

Pendant \laperiodeCAC{} (CAC), une multitude d'états indépendants apparaissent,
étroitement connectés grâce à un système de comptoirs, mis en place
avec les activités commerciales assyriennes (\cref{Map-BM-BR})\footnote{Pour un aperçu général
: \cite{Garelli1963a, Veenhof2008a}.}. À partir du \iiemeMil{} millénaire, des
familles marchandes originaires de la ville d'\assur{} développent un commerce à longue
distance et s'installent dans une quarantaine de localités en Anatolie centrale,
dont la plaque tournante du réseau se situe à \kultepe{} (son nom antique est
alors \kanes{}, transcrit également par Kaniš). Les archives des
familles marchandes
(assyriennes et anatoliennes) documentent le commerce régulier entre \kultepe{} et
\assur{}, mais aussi, entre les autres comptoirs d'Anatolie. Deux systèmes
d'échanges sont alors parallèlement établis. Le premier, à longue distance,
assure un flot régulier d'étain et d'étoffes vers l'Anatolie et, en retour, d'or
et d'argent vers la Mésopotamie. Le deuxième est dédié au commerce en Anatolie
de produits locaux, comme le cuivre et la laine. Les marchandises étaient
transportées à dos d'âne et de mule, rassemblés en caravanes qui voyageaient
entre \assur{} et \kanes{} en quelque six semaines\footnote{Voir \cite{Stratford2014a} pour
un exemple détaillé.}. Il est intéressant de relever que le métal d'argent
servait de valeur standard pour les transactions et la différence entre les prix
à l'achat et à la revente rendait le commerce très lucratif. Les plusieurs
milliers de documents épigraphiques découverts à \kultepe{}, couramment appelés
«~tablettes cappadociennes~», outils administratifs pour la gestion du commerce,
livrent également nombre de renseignements sur l'organisation de l'Anatolie
centrale\footnote{\cite{Barjamovic2011a}; \cite[205--230]{Garelli1963a};
\cite[147--182]{Veenhof2008a}.}, et font de \kultepe{} un site clef pour
comprendre la période.


Les fouilles, continues depuis 60 ans à \kultepe{}, ont clairement
démontré que l'organisation spatiale, sociale, économique et
technologique du site est le reflet manifeste d'une ville
développée\footnote{\cite{Barjamovic2014a, Hertel2014a,
    Kulakoglu2011a, OzgucT2003a}.}. De même, d'autres centres urbains
sont dès lors bien visibles en Anatolie centrale, tant à travers les
sources archéologiques qu'épigraphiques. Non seulement des palais sont
attestés pour une quinzaine de localités dans les tablettes
cappadociennes\footnote{\cite[154\psq]{Veenhof2008a}.}, mais aussi des
princes et des princesses\footnote{\cite[12\psq]{Michel2015b}.}. Les
documents indiquent que l'Anatolie était alors organisée en royaumes,
qui comprenaient une ville principale -- divisée en une citadelle et
une ville basse -- et entre 10 et 20 villages alentours qui alimentaient
en céréales les \habitantEs{} de la ville
principale\footnote{\cite{Dercksen2008b}.}. Une partie importante des
terres appartenait à l'élite urbaine ou au palais (qui pouvait
posséder des fermes ou même des
villages)\footnote{\cite{Dercksen2004b}.}, le reste étant détenu par
une population libre qui fournissait les produits agricoles sur les
marchés. Le palais, ou plus exactement, les palais successifs de
\kultepe{} ont été endommagés par les premières fouilles du tell et ne
sont donc connus que partiellement\footnote{\cite{Michel2015b,
    OzgucT1999a}.}. Le palais est avant tout la résidence du prince et
constitue un centre de pouvoir où de nombreuses décisions sont prises.
En outre, à \kultepe{}, deux temples sont attestés par les
textes et par les découvertes archéologiques sur la
citadelle\footnote{\cite{OzgucT1993a, OzgucT1999a}.}. Au pied du tell
s'étend une ville basse\footnote{\Michel{} a insisté à de nombreuses
  reprises sur l'importance de la distinction entre ville basse et
  \karum{}, le terme assyrien connu à travers les textes, qui a
  longtemps été employé comme synonyme (\cite{Michel2013c,
    Michel2014b}).}. La stratigraphie de la ville basse est d'une
importance capitale pour l'Anatolie centrale, car c'est elle qui sert
de référence pour les sites de la région \gls{acs}
(\cref{table-kultepe}).
