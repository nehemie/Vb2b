Très peu d'informations sont connues sur les niveaux les plus anciens de
la ville basse en dehors d'un aperçu de la
céramique\footnote{\cite{Emre1989a}.}. La ville basse est composée,
pour ses deux niveaux \delaperiodeCAC{}, de maisons individuelles,
reliées par des rues qui recouvrent un réseau de canalisations. La
taille et la densité de l'occupation de la ville basse paraissent
avoir été très importantes, puisqu'un rempart d'un rayon d'environ
\SI{1}{\kilo\meter} l'entoure\footnote{\cite{Barjamovic2014a,
Fukuda2004a}.}. Les maisons ont des tailles assez variables (de 40 à
\SI{200}{\square\meter}) et des contours
irréguliers\footnote{\cite{Hertel2014a} ;
\cite[165--173]{Schachner1999a}. }.  Les archives découvertes à
l'intérieur de certaines maisons montrent qu'Assyriens et autochtones
vivaient ensemble\footnote{\cite{Michel2011b, Michel2014b}.}. Par
ailleurs, il a souvent été souligné que, sans ces archives, il serait
impossible de faire la différence entre ces deux ethnies sur la base
unique des maisons et de leur mobilier.

Des activités métallurgiques assez intenses se déroulaient dans la
ville basse, avec des ateliers situés dans des maisons. Ceux-ci sont
bien identifiés par les moules, les tuyères et les
creusets\footnote{\cite[70--73]{Lehner2015a} ;
\cite{Muller-Karpe1994a} ; \cite[39--43]{OzgucT1986a}.}. En
revanche, même si les textes sont riches en informations sur la
production locale de textiles (laine et lin) -- ils pouvaient être
commercés par les marchands assyriens -- les traces (étudiées) sont
encore tenues\footnote{\cite{Michel2010b}.}.


\subsubsection{Les principaux sites de la région}

De nombreux sites contemporains des niveaux de la ville basse de
\kultepe{} ont été fouillés dans la région \gls{acs}
(\cref{Map-BM-BR}), mais à la différence des autres périodes, aucune
synthèse récente ne s'engage dans une critique du résultat des
fouilles. On peut néanmoins noter l'intégration des résultats, et
notamment des prospections, dans les analyses de géographie
historique\footnote{\cite{Barjamovic2011a}.} et dans les simulations
de systèmes multi-agents\footnote{\cite{Palmisano2015a,
Palmisano2015b}.}.


Aussi vais-je présenter, du nord au sud, les principaux résultats des
fouilles archéologiques. \ikiztepe{}, à l'embouchure du \kizilirmak{},
est un site fouillé et occupé \alaperiodeCAC{} (phase
\emph{transitional period})\footnote{\cite{Alkim1988a,
Muller-KarpeV2001a}.}, tout comme \tekkekoy{}, \dundartepe{} et
\kaledorugu\footnote{\cite[213--215]{DonmezS2002a} ;
\cite{Kokten1945a}. }. Les recherches de \Donmez{} et \Yurtsever{}
donnent un bon aperçu de l'occupation de cette région grâce à diverses
prospections et interventions\footnote{Récapitulatif chez
\cite{DonmezS2002a, DonmezS2008a, DonmezS2008c,
YurtseverBeyazit2014a}. Il n'est pas encore certain qu'il faille
ajouter le site de \oymaagac{} à cette liste.}. Pour ces sites, les
restes architecturaux sont très maigres.

À \alacahoyuk{}, la période \emph{IV} a été attribuée \alaph{} dans
les rapports de fouilles\footnote{\cite{Kosay1938a, Kosay1951a,
Kosay1966a, Kosay1973a}.}, mais \TOzguc{} l'attribue
\alaperiodeCAC{}\footnote{\cite[473\psq]{OzgucT1993b}.}. De nombreux
indices invitent à suivre l'interprétation de \TOzguc{}, puisque du
matériel de cette période apparaît en divers endroits dans les
rapports de \Cinaroglu{}. L'absence de plans ou de discussion sur
l'étendue du site empêche d'avoir une vue globale de l'occupation.

À \bogazkoy{}, 73 tablettes datant de la phase \emph{\kultepe{} Ib}
ont été découvertes dans les quartiers d'habitation de la ville
basse\footnote{\cite{Dercksen2001, Wilhelm2010}.}. Au total, une
quinzaine de maisons ont été fouillées, la plupart à la \NTer{},
auxequelles il faut ajouter une maison bien conservée à \knw{} et une
autre sur la citadelle de \buyukkale{}. De plus, un important grenier
a été dégagé à \nwh{}\footnote{Récapitulatif dans
\cite{Strupler2013a}.}. Les fouilles de \buyukkaya{} ont mis en
évidence des activités domestiques pour cette
période\footnote{\cite{SchoopBKinpress}.}. Les différentes découvertes
indiquent une structuration du site très similaire à celle de
\kultepe{}, avec une citadelle et un rempart surplombant une ville
basse où se trouvait, entre autres, les habitations des marchands,
organisées selon un système de rues couvrant les évacuations d'eau.
Les techniques de construction sont également très similaires. Ainsi,
\bogazkoy{} donne une bonne idée d'une ville \delaperiodeCAC{} au cœur
de la région \gls{acs}.

\masathoyuk{} a livré de la céramique, bien conservée, mais il
n'existe pas de plans pour cette période (\emph{Hittite Layer V}).
Seule l'indication de \TOzguc{}, «~\emph{the hittite layer V extended
in all directions of the lower city. \masathoyuk{} was clearly a
large city}~»\footnote{\cite[85--87, 103--110, fig.
36--82]{OzgucT1982a}.}, donne une idée de l'extension et de
l'intensité de l'occupation. À \kayapinar{}, d'anciennes fouilles ont
révélé un matériel archéologique de cette
période\footnote{\cite{Temizer1954a}.}.

À \alisar{}, la dissertation de \Gorny{} fait la synthèse de cette
période à partir de fouilles
anciennes\footnote{\cite[160--230]{Gorny1990a}.}. La dernière phase
\delaperiodeCAC{}, contemporaine de \emph{\kultepe{} Ib}\footnote{Voir
\cite{Dercksen2001}.}, est la mieux attestée. La citadelle et la
ville basse sont ceintes d'un rempart, une rue concentrique est
attestée le long du rempart et une rue radiale qui se dirige vers la
citadelle a été retrouvée entre un groupe de bâtiments. Les plans des
constructions sont tous incomplets, ce qui rend une analyse
fonctionnelle impossible, car le nombre d'objets associés aux
bâtiments est trop faible\footnote{\cite[183\psq]{Gorny1990a}.}.

Un des sites en cours de fouilles, \kayalipinar{}, l'ancienne \samuha,
pourrait bien apporter de riches données pour cette région. Le niveau
\emph{5}, caractérisé par une architecture de murs relativement fins
en briques crues, a déjà révélé des scellements et une tablette
paléo-assyrienne\footnote{\cite{Muller-Karpe2006a, Muller-Karpe2009a,
Muller-Karpe2009c}.}. Jusqu'à présent, uniquement deux maisons ont
été dégagées, mais des informations détaillées ne sont pas encore
disponibles.

À \kamankalehoyuk{}, le niveau \emph{IIIc} daté \delaperiodeCAC{} a
été détruit dans un incendie. Là encore, les murs sont assez fins et
en briques crues, sans fournir de plans cohérents. Plusieurs
inhumations \intramuros{} ont été
découvertes\footnote{\cite[1106--1108]{Omura2011a}.}. Deux tablettes
cappadociennes ont également été
retrouvées\footnote{\cite{Yoshida2002a}.}. Les fouilles voisines de
\yassihoyuk{}\footnote{\cite{OmuraM2011a, OmuraM2012a}.} et de
\buklukale{}\footnote{\cite{Matsumura2013a, Matsumura2013b,
Matsumura2014a}.} commencent également à mettre au jour des
niveaux \delaperiodeCAC{} et vont sûrement apporter prochainement de
nouveaux résultats.
%% Topakli, Hacibektas, Hashöyük

Le site d'\acemhoyuk{}, au sud du \kizilirmak{}, est fouillé de longue
date ; c'est l'un des grands sites \delaperiodeCAC{}\footnote{Les
niveaux sont identiques pour la ville basse et la citadelle :
%12 à 7 seraient du \BA{} tandis que les niveaux %
les niveaux \emph{6} et \emph{5} correspondraient aux niveaux
\emph{IV} et \emph{III} de la ville basse de \kultepe{}, le niveau
\emph{4} à \kultepe{} \emph{II}, le niveau \emph{3} au niveau
\emph{Ib} de \kultepe{} et enfin les niveaux \emph{2} et \emph{1} au
niveau \emph{Ia} de \kultepe{} (d'après \cite{Oztan2012a}). --
\cite{Kuniholm1989a} ; \cite{Newton2004a} pour les datations
dendrochronologiques et \cite{Arbuckle2013a} pour les premières
datations radiocarbones.}. Les fouilles se sont jusqu'à présent
largement concentrées sur les bâtiments monumentaux et leur voisinage.
Deux « palais » sont attribués à la phase \emph{III}, \emph{Sarıkaya}
au sud et \emph{Hatipler} au nord\footnote{\cite{OzgucN1966a,
OzgucN1977a, OzgucN1995a, Oztan2012b}.}. Un autre bâtiment
imposant, \emph{Hizmet Binası}, est en cours de fouille entre les deux
autres. \emph{Hizmet Binası} (\cad{} bâtiment de service) a été
dénommé ainsi, car de nombreux fours y ont été
découverts\footnote{\cite{Oztan2007a}.}.  Le niveau \emph{III} a
disparu dans un incendie généralisé et les découvertes des 10
dernières années montrent que les phases \emph{II} et \emph{I} n'ont
pas été occupées par des bâtiments monumentaux, mais plutôt par des
maisons à deux ou trois pièces, des silos et de nombreuses
fosses\footnote{\cite{Oztan2008a, Oztan2009a, Oztan2010a, Oztan2011a,
Oztan2012a, Oztan2013a, Oztan2014a}.} ; au moins un atelier de
métallurgiste est attesté\footnote{\cite[238\psq]{Oztan2010a}.}.
L'absence de plan d'ensemble du site pose problème pour comprendre les
changements dans l'urbanisme. Des fouilles de sauvetage au sud et au
sud-ouest du tell, lors de la construction de canalisations dans le
village adjacent de Yeşilova, montrent qu'une ville basse s'étendait
au pied du tell\footnote{\cite{Oztan2014a}.}. \Arbuckle{}, qui étudie,
entre autres, la faune d'\acemhoyuk{}, apporte un nouveau regard sur
les données, en s'intéressant aux interactions entre production,
consommation et urbanisation\footnote{\cite{Arbuckle2012a,
Arbuckle2013a, Arbuckle2014b}.}. Il démontre que les éleveurs de
troupeaux (principalement de moutons et de chèvres) visent
l'extraction de produits secondaires (laine, lait) plutôt que la
viande, et ceci, indépendamment des changements dans l'occupation du
tell, considérée comme centralisée administrativement (\emph{niveau
III}), puis plus lâche (\emph{niveau II}). En revanche, les
activités de boucherie sont différentes entre les phases : elles sont
centralisées puis locales. Il propose donc de repenser le rapport
entre les villes et la campagne. L'étendue du contrôle des sites
urbains sur l'arrière-pays serait faible et seules la production et la
distribution à l'intérieur des sites évolueraient en fonction de
l'urbanisation.

\konyakarahoyuk{} n'appartient pas à cette région et, hormis la
publication des sceaux\footnote{\cite{Alp1968a}.}, les rapports
irréguliers -- le plus souvent de deux pages -- ne permettent en
aucune façon de prendre conscience de l'ampleur des
fouilles\footnote{\cite{%Alp1956b, Alp1961a, Alp1964a, Alp1967a,
%Alp1974a, Alp1975a, Alp1980c, Alp1981a, Alp1981b, Alp1982a,
Alp1988a,Alp1992a}, pour les derniers rapports.}.

Enfin, des nécropoles, qui existent en parallèle des inhumations
\intramuros, apportent quelques données intéressantes sur les
pratiques funéraires\footnote{\cite{Emre1991a}.}, comme à
Büğet/Ferzant\footnote{\cite[87\psq]{OzgucT1978a} ;
\cite{OzgucT1986b}.} et
Kazankaya\footnote{\cite[69--87]{OzgucT1978a}.}.

\subsubsection{Linéament historique}

Les découvertes et les travaux de ces dix dernières années sur la
reconstruction de la liste des éponymes d’\assur{} (\kultepe{} Eponym
List = KEL) ont permis de mieux saisir la chronologie des milliers de
tablettes cappadociennes et de dresser un portrait dynamique de cette
période.  \Barjamovic, \Hertel{} et \Larsen{} ont proposé une synthèse
dont je reprends quelques points principaux
ici\footnote{\cite{Barjamovic2012a}.}. Selon cette reconstruction,
l'Anatolie était fragmentée en de nombreuses cités-États, souvent
rivales, formant des alliances dans la première moitié du \xixeme{}
siècle. Durant cette période, le système commercial est principalement
fondé sur la visite de marchands assyriens qui s'installent pour
quelques années avant de retourner à \assur. Ce système se transforme
peu à peu avec l'installation permanente des marchands et la
multiplication des mariages mixtes, alors que les activités
commerciales entrent dans une phase de récession qui perdure jusqu'à
la fin de la période \emph{\kultepe{} II} (\ca{} 1840) lorsque ville
basse et tell sont détruits par un incendie. Une réorganisation du
commerce pendant la phase \emph{\kultepe{} Ib} est identifiable, mais
la nature des changements reste débattue. Il est néanmoins bien
perceptible que certaines cités-États agrandissent leur territoire.
Par exemple, on sait qu'\amkuwa{} domine \lakimissa{}, puis tombe
ensuite sous le joug de \kultepe{}, ou encore que \kussara{} contrôle
\luhusaddiya{}. Vers 1750 \pithana{} (roi de \kussara) s'empare de
\kanes{} et son fils héritier, \anitta, continue l'extension militaire
et détruit, entres autres, \hattus{} (\bogazkoy) vers 1730. Ces deux
rois ont donc dû régner sur un territoire assez vaste en Cappadoce,
mais cet espace est rapidement remis en question par des révoltes
internes. Enfin \kultepe{} est repris par le roi Zuzu, avant d'être
abandonné assez rapidement.


Tous ces évènements sont peu visibles dans les sources archéologiques.
Hormis à \kultepe{} ou à \acemhoyuk{}, les sites ne sont connus que
par une phase contemporaine de la phase \emph{\kultepe{}
Ib}\footnote{D'après les textes découverts (\cite{Dercksen2001}) et
les datation radiocarbones (\cite{Strupler2013a}).}, ce qui empêche
d'avoir une vue diachronique pour la région étudiée. Il est néanmoins
possible de souligner quelques différences majeures avec le \BA{}. Du
point de vue de la céramique, le répertoire des formes correspond à
une suite directe de la phase terminale (\emph{transitional period})
du \BA. Cependant, l'introduction du tour de potier rapide, désormais
employé pour l'ensemble du répertoire, indique que le mode de
production a été modifié. Même si aucune preuve directe (comme des
ateliers de potier) ne peuvent conforter cette hypothèse, il est
tentant d'y voir une spécialisation de la production\footnote{Des
scientifiques parlent de style céramique proto-hittite pour
souligner la continuité de tradition avec la période suivante (par
ex.  \emph{frühhetitisch} \cite{Muller-Karpe2009a}), mais l'usage
d'un terme ethnique ne semble pas judicieux au regard du nombre
d'ethnies différentes en présence (hittites, louvites, hattis, pour
ne citer qu'elles) d'autant plus qu'il indique une domination a
priori.}. En outre, la profusion des rhytons et autres formes
plastiques, non seulement étaye cette hypothèse, mais montrent qu'une
partie de la production céramique avait alors atteint un rôle
important dans la manifestation de la richesse et du
pouvoir\footnote{\cite{Schoop2011d}.}. Une remarque similaire peut
être formulée à propos de la naissance d'un style anatolien pour les
cachets circulaires, style qui sera lui aussi transmis à la période
suivante\footnote{\cite{OzgucN1968a, OzgucN1980a, OzgucN1993a,
Teissier1994a}.}.  La structuration des sites indique une division
claire entre citadelle et ville basse, accentuée par la présence de
bâtiments monumentaux sur la citadelle (palais, temple) par rapport
aux habitats domestiques. Ces derniers se diversifient et sont
parfois étagés, et on note la présence d'ateliers. Les plans des
bâtiments monumentaux ne sont pas similaires entre eux, mais ceux que
l'on connaît (\kultepe{}, \acemhoyuk{}, \bogazkoy{}) montrent que les
palais sont l'expression de solutions locales qui ne reflètent pas une
idéologie unifiée, à la différence de la période
hittite\footnote{\cite{Bachhuber2012a}.}.  Enfin, rues, canalisations
et remparts rythment l'espace urbain et donnent une image imposante de
la ville.




\subsection{\Ph{} (\ca{} 1750--1200)} \label{125-BR}

La naissance du \royaumehittite{} fait directement suite
\alaperiodeCAC{}. Elle est voilée d'un certain flou dans les sources
épigraphiques, mythes étiologiques se mêlant aux chroniques royales.
La fin tumultueuse de la période précédente est scellée pour de
nombreux sites par un incendie généralisé, reflet d'une période de
guerres endémiques. L'interruption des relations commerciales
régulières avec l'Assyrie, dans lesquelles l'élite trouvait sans aucun
doute une source de légitimation à sa domination, a laissé la place à
un vide où les faits de guerre ont eu un rôle unificateur.

La dénomination de l'histoire hittite et son découpage en phases sont
en eux-même un débat ouvert et l'archéologie hittite a tendance à
répercuter ces problèmes\footnote{\cite{Schoop2008a}.}. Non seulement
différentes terminologies sont en concurrence, avec une division
binaire entre \emph{Ancien Royaume} et \emph{Nouveau Royaume} (appelé
aussi \emph{Empire}) et une division ternaire où une période a été
intercalée, donnant la suite \emph{Ancien Royaume}, \emph{Moyen
Royaume} et \emph{Empire}, mais la définition de ces périodes est
loin de faire l'unanimité dans les reconstructions
historiques\footnote{Récapitulatif chez \cite{Dincol2006a}. Par
exemple voir \cite{Klengel2011a} et \cite{Beal2011a} pour deux
synthèses aux périodisations différentes.}, ce qui s'ajoute, il va
sans dire, au problème irrésolu de la datation absolue des sources
historiques antérieures au \ierMil{} millénaire au
Proche-Orient\footnote{\cite{Pruzsinszky2009a} et pour le cas hittite
\cite{Wilhelm2004a}.}.

L'élévation de \hattusa{} (\bogazkoy{}) au rang de capitale hittite
est datée traditionnellement de la deuxième moitié du \xviieme{}
siècle. La naissance du \royaumehittite{} s'inscrit pleinement dans
les luttes territoriales de la phase précédente avec les exemples de
\pithana{}, d'\anitta{} ou d'\anumhirbe{}, qui ont établi des
royaumes, même si aucun n'a duré très
longtemps\footnote{\cite{Klinger2014a}.}.  Pour la région concernée,
certains sites sont rapidement abandonnés\footnote{Par exemple
\kultepe{} (\cite{Kulakoglu2014a}).}, mais de nouvelles fondations,
créées \emph{de novo}, font également leur apparition\footnote{Pour
quelques synthèses récentes, voir \cite{Genz2011, Glatz2011b,
Mielke2011b, Seeher2011a}.}.

\subsubsection{Les données archéologiques}

Le nombre de sites fouillés pour cette période est étonnamment
restreint. Les principaux résultats sont présentés ici, du nord au sud
(\cref{Map-BM-BR}).

Le tell le plus septentrional, \oymaagac{}, fait l'objet de recherches
depuis une dizaine d'années. Sur sa partie sommitale, un temple de
\SI{40}{m} de côté a commencé à être dégagé, livrant tablettes,
empreintes et scellements dans le remblai d'abandon. Ceint d'une
fortification, le site semble posséder une «~grotte~» similaire à
celle de \bo{} (voir \infra). De nombreux indices contribuent à y
reconnaître la ville hittite de \nerik\footnote{\cite{Czichon2009a,
Czichon2011a, Czichon2013a}.}.

À \SI{35}{\kilo\meter} au sud, les vestiges \delaph{} d'\oluzhoyuk{}
(\emph{levels 7--8}) n'ont été exposés que sur une petite surface,
parmi lesquels seuls quelques tronçons de murs ont été mis au
jour\footnote{\cite[102 fig.  12]{DonmezS2011a}~;
\cite{YurtseverBeyazit2014a}. Pour un aperçu global du site, voir
\cite{DonmezS2014c}.}. Les sites voisins de \dogantepe{} et
d'\ayvalipinar{} sont sans aucun doute contemporains, mais ils sont
uniquement connus par quelques sondages et
prospections\footnote{\cite{DonmezS2010a, Dogan-Alparslan2011a,
YurtseverBeyazit2014a}.}.

À l'ouest, le site d'\inandik{} est composé d'un unique bâtiment
monumental, organisé autour de deux cours, qui devait être le siège
d'une autorité régionale. Dans cette construction, un extraordinaire
vase à quatre frises en relief a été dégagé. L'ensemble, certainement
à rapprocher d'une résidence princière, date de la deuxième moitié du
\xvieme{} siècle\footnote{\cite{Mielke2006b}.}.

\hdd{} est un site sans doute contemporain d'\inandik{}, où plusieurs
vases à decors en relief ont été découverts dans le bâtiment \emph{I}
qui surplombe quatre autres bâtiments (\emph{II}, \emph{III},
\emph{IV} et \emph{VI}) repartis autour d'une rue. Ces dernières
constructions, bien régulières, ont des plans caractéristiques d'un
habitat\footnote{\cite{Sipahi2012b, Yildirim2009b, Yildirim2013a}.}.

À \boyali{}, les fouilles menées entre 2004 et 2008 ont révélé une
phase d'occupation possédant une série de bâtiments agglutinés les uns
aux autres où 44 pièces ont été
identifiées\footnote{\cite{Sipahi2012f}.}. Parmi les découvertes les
plus intéressantes, on relève des poids, des fusaïoles, une meule
dormante et courante ainsi qu'un pot qui contenait des graines
carbonisées de blé, d'orge, de millet et de
gesse\footnote{\cite{Sipahi2013a}.}. En outre, une cruche a livré un
mélange de propolis et de graines de cumin, une mixture à considérer
comme un remède thérapeutique\footnote{\cite{Salih2009a}.}. Le site
date du \xvieme{} ou du \xveme{} siècle et a été détruit dans un
incendie général.  Le site tout proche de \fatmaoren{}, ponctuellement
sondé, est très érodé et seulement deux longues pièces rectangulaires
de \SI{12}{\meter} de longueur ont été
fouillées\footnote{\cite{Sipahi2012c}.}.

Au sud-est de \corum, \ortakoy{} est le site qui a livré la plus
grande collection de tablettes cunéiformes en dehors de \bogazkoy{} et
qui a été identifié comme l'ancienne
\sapinuwa\footnote{\cite{Suel2005b}.}. Malgré de nombreux rapports
publiés depuis 25 ans, il n'y a pas d'étude archéologique détaillée.
D'après les informations disponibles provenant de l'étude des
tablettes cunéiformes publiées, on peut conclure que le site est
occupé aux \xveme{}--\xiveme{} siècles, puis détruit et
abandonné\footnote{\cite{Suel2013a}.}. Dans la partie sud du site, un
palais, un bâtiment de stockage et d'autres bâtiments monumentaux
s'opposent à l'architecture plus légère retrouvée au nord du site.

À l'ouest, \alacahoyuk{} est ceint d'une fortification dont la porte
sud-est est décorée de bas-reliefs représentant une
procession\footnote{Cet ensemble a été repris dernièrement par
\cite{Taracha2011a,Schachner2012e}.}. Cette porte monumentale donne
sur un complexe palatial\footnote{\cite[121--128]{Kosay1966a},
\cite{Mielke2011b}.} qui jouxte trois
silos\footnote{\cite{Cinaroglu2013a}}. Ce site est le plus souvent
identifié comme la ville hittite d'\arinna{} ou bien celle de
\zippalanda{}.

Les fouilles reprises en 2010 sur le site voisin d'\eskiyapar{} ont
confirmé une occupation à la période hittite grâce à un fragment de
tablette cunéiforme et à quelques tronçons de murs
dégagés\footnote{\cite{Sipahi2012h,Sipahi2014c,Sipahi2015a}.}.

À \masathoyuk{}, au \xveme{} siècle (\emph{Layer III}), un palais
occupe le sommet du tell, qui regroupe de multiples activités
(archivage de documents, stockage de denrées,
paraphernalia)\footnote{\cite[51--65]{OzgucT1978a} ;
\cite[73--83]{OzgucT1982a}.}. En outre, un bâtiment a été dégagé en
contrebas\footnote{\cite[85--89]{OzgucT1982a}.}. À la suite d'une
destruction générale dans une conflagration, un nouveau palais a été
reconstruit à la phase suivante (\emph{Layer II, ca} \xiveme{}
siècle), tout comme des bâtiments dans la ville basse. Enfin, une
dernière phase (\xiiieme{} siècle, \emph{Layer I}) ne présente plus de
constructions monumentales\footnote{\cite[65\psq]{OzgucT1978a} ;
\cite[73--83]{OzgucT1982a}.}. Les archives permettent d'associer ce
site à la ville hittite de \tapikka{}, qui est décrite comme une ville
située à la frontière avec les \kaska{}\footnote{Le terme \kaska{} (ou
\gasga) est un nom générique pour les ennemi\es{} des chaînes
pontiques \auxquelles{} on associe un mode de vie semi-nomade :
\cite{DonmezS2008a, Glatz2005a, Klinger2005a, Matthews2009c,
Singer2007a, Yakar2008a}.}.


L'exploration de \kayalipinar{} a débuté en 2005 et deux bâtiments
monumentaux de la période hittite, dénommés A et B, sont interprétés
par les fouilleurs comme des composants d'un complexe palatial. Le
bâtiment A est orné d'une frise d'orthostates décorés en bas-relief,
dont l'un présente une figure de déesse particulièrement bien
conservée. Le second bâtiment, B, est rectangulaire et divisé en une
vingtaine de pièces. Lors des fouilles, plusieurs tablettes et sceaux
y ont été découverts\footnote{\cite{Muller-Karpe2006a,
Muller-Karpe2009a, Muller-Karpe2013c, Rieken2009a}.}.

À \SI{15}{\kilo\meter} au sud, \kusakli{} est un site très bien connu
non seulement par des fouilles, mais aussi par des prospections
géophysiques intensives qui ont révélé la topographie générale de la
ville\footnote{\cite{Muller-Karpe2002a,Muller-Karpe2009b,Muller-Karpe2013d,
Muller-Karpe2013b}, avec références bibliographiques
antérieures.}. Il s'agit d'une fondation nouvelle de la fin du
\xvieme{} siècle où ont été fouillés deux temples (bâtiment C, temple
1), un large silo, un barrage, deux portes du rempart à caissons qui
ceint la ville et un sanctuaire extra-urbain. Les découvertes
épigraphiques et sigillaire identifient le site comme la ville de
\sarissa{}, un centre d'une province hittite.

À l'ouest, \buklukale{} contrôle l'un des points de passage du
\kizilirmak{} où les recherches viennent de
commencer\footnote{\cite{Matsumura2013b, Matsumura2013a,
Matsumura2014a}.}. Le site est divisé en une citadelle séparée par
un mur cyclopéen de la ville basse qui est, elle, cernée par une
enceinte à caissons. Une tablette cunéiforme hittite a été découverte
à la surface tout comme quelques impressions de cachets
circulaires\footnote{\cite{Weeden2013a}.}.

À \kamankalehoyuk{}, le dégagement de tronçons de murs épais laisse
supposer la présence de bâtiments monumentaux, mais pour le moment,
seuls des silos ont été soigneusement fouillés. Le plus imposant
mesure \SI{15}{\meter} de diamètre et son mur périphérique est
conservé sur \SI{5}{\meter} de hauteur. L'un des silos a fourni des
graines de blé\footnote{\cite[1102--1106]{Fairbairn2005,
Omura2011a}.}, alors que plusieurs remblais de remplissage de
silos contiennent des empreintes de cachets du \xiveme{} siècle
fournissant ainsi un
\taq{}\footnote{\cite{Yoshida1999a,Yoshida2006a}.}. Il n'existe pas de
vestiges plus récents \delaph{}.



\paragraph{}


Du point de vue architectural, les Hittites se démarquent des périodes
précédentes par une série d'innovations qui transparaissent dans la
création de constructions massives, en particulier à \bogazkoy. Le
\tempelI{} de \bogazkoy{} est construit en blocs taillés, dont le plus
gros ferait au moins \SI{40}{\tonne}. De nombreuses techniques et
méthodes ont été développées pour travailler les pierres, comme le
percement de mortaises, le sciage de blocs, l'utilisation de crampons
en métal ou la construction de voûtes en encorbellement\footnote{Le
détail de ces techniques a été traité par \Seeher{} :
\cite{Seeher2005b, Seeher2007c, Seeher2008b, Seeher2009b}. }. Ces
techniques ont été employées dans la région \gls{acs}, comme à
\alaca{}, à \masathoyuk, à \kusakli{}, à \kayalipinar{} ou à \ortakoy,
mais les blocs taillés ou percés sont rares et ne sont représentés que
par quelques exemples, hormis à \eflatunpinar, réalisé entièrement
avec des pierres de taille, et à \bogazkoy{}.

En revanche, les habitations sont construites selon les mêmes
principes qu'aux périodes précédentes. Les soubassements sont en
pierre, il s'agit de moellons à peine dégrossis formant quelques
assises et supportant une superstructure en briques crues. Néanmoins,
les murs possèdent désormais un double parement, et sont, de fait,
plus larges que ceux de la période précédente. Les pierres sont liées
avec un mortier. L'élévation des murs est recouverte d'un enduit, qui
protège les briques, mais qui avait également une fonction esthétique.
La reconstruction d'un tronçon du mur de fortification de \bo{} permet
d'imaginer l'effet d'une telle mise en
œuvre\footnote{\cite{Seeher2007a}.}. Les toits sont plats et la
majorité des sols est en terre battue.

Pendant les premiers siècles du \royaumehittite, la céramique poursuit
la tradition précédente, mais perd peu à peu de son prestige et le
répertoire des formes tend à se simplifier. Les vases de prestige ou
décorés sont peu à peu abandonnés et une production de masse de
céramiques utilitaires s'impose dans les centres
urbains\footnote{\cite{Schoop2011d}.}.



\subsubsection{Organisation du Royaume hittite}

À la différence de la période des \cac{}, quand le commerce (et sa
taxation) était le levier principal de l'économie, le Royaume hittite
s'est fondé sur le prélèvement de taxes et l'accumulation de tributs
de guerre. Les paysans payaient une taxe et étaient astreints à des
travaux ou à des services. Les terrains pouvaient appartenir à un
palais, à un temple, à une ville\footnote{\cite{Klengel2006a}.} ou à
l'élite à laquelle était octroyée une parcelle de terre en
contrepartie de services rendus\footnote{Quelques informations sur
l'organisation de l'économie rurale ont été récemment mises à
disposition avec la publication des \emph{Landschenkungsurkunden}
par \cite{Ruster2012a} ; voir également \cite{Wilhelm2009a}.}. Les
parcelles étaient ensuite organisées en maisonnées, regroupant une
famille élargie et des esclaves\footnote{\cite{Wilhelm2009a}.}.

La focalisation sur l'agriculture comme levier de finance est bien
mise en évidence par les silos découverts\footnote{\cite{Hoffner1974,
Hoffner2001a, Klengel2006a} et entre autres,
\cite{Dorfler2000,Dorfler2011, Fairbairn2005a, Pasternak1998a,
Pasternak2012} pour des résultats paléobotaniques.}. Le plus
massif, celui de \bo{}, dont la destruction date du tout début du
\xvieme{} siècle, avait une capacité comprise entre
\SIrange[range-phrase = ~et~ ]{4}{6}{\tonne} de
grains\footnote{\cite{Seeher2000, Seeher2006b}.}.  Des silos
postérieurs et plus modestes sont également attestés à
\bo{}\footnote{\cite{Seeher2000, Seeherinpressa}.}, mais ont aussi été
retrouvés à \kamankalehoyuk, à \alacahoyuk, ou à
\kusakli{}\footnote{\cite{Mielke2001a}.} et étaient soigneusement
administrés par des fonctionnaires\footnote{\cite{Singer1984a}.}. Les
temples, quant à eux, revêtaient un rôle économique considérable. Le
\tempelI{} de \bo{}, où une centaine de jarres de stockage (d'une
capacité entre \SIrange[range-phrase = ~et~ ]{900}{1750}{\liter}) a
été retrouvée \footnote{\cite{Neve1969c}.} -- même si ses dimensions
sont hors norme par rapport aux autres temples -- matérialise ce rôle,
qui est également visible dans les textes\footnote{\cite{Klengel1975a,
Gilan2007a}.}. Cette organisation du stockage souligne
l'importance du contrôle des produits agricoles dans la gestion du
royaume\footnote{\cite{Seeher2015a}.}. Bien entendu,
l'élevage\footnote{\cite{Beckman1988a, Klengel2007a}.} et les
métaux\footnote{\cite{Alparslan2011a, Lehner2015a, Siegelova2011a}.}
avaient un rôle important dans l'économie, pour fournir laine, outils,
armes, objets de prestiges. Le métal d'argent reste l'étalon de
référence dans l'établissement des prix\footnote{\cite{Hout2005a}.}.
L'organisation de l'économie a eu des répercussions sur
l'environnement, puisque l'étendue des forêts a diminué à cause de
l'exploitation du bois, du défrichement pour les cultures et de
l'élevage\footnote{\cite{Wright2015a}.}. Ces données contrastent avec
la faible intensité d'attestations directes d'échanges au-delà de
l'Anatolie\footnote{\cite{Genz2006a, Genz2011c, Kozal2006a}.}. Par
ailleurs, les textes évoquent très rarement le monde administratif ou
économique. Tout porte à penser que l'essentiel des échanges à longue
distance se déroulait dans le cadre de tributs et de relations
diplomatiques. Lorsque des marchands sont évoqués, ils semblent avoir
plus ou moins eu le rôle d'émissaires, échangeant des cadeaux
diplomatiques et collectant des tributs\footnote{\cite{Giorgieri2012a,
Hoffner2001a, Klengel1979a, Kozal2007a}.}.


D'une manière générale, les études sur les taxations montrent que le
royaume était divisé en trois entités : la commune, la région et la
capitale\footnote{\cite{Siegelova2001a}.}. Cette division devait
certainement servir à l'administration du royaume. Les taxes sont
collectées localement par les chefs-lieux de région et une partie
devait transiter vers \bo{}. Cette organisation est attestée par les
sources des \xiveme{} et \xiiieme{} siècles, mais on considère
généralement qu'elle est en place dès le \xvieme{} siècle, à défaut de
modèle alternatif\footnote{\cite{Hout2009a, Hout2009b, Hout2012a}.}.
Le matériel issu des prospections archéologiques dans la région semble
correspondre à ce modèle\footnote{\cite{Glatz2009, Okse2001a,
Okse2006a, Okse2014b}.} et les textes et les sceaux livrent une
mine d'informations sur les fonctionnaires et leurs
titres\footnote{\cite{Herbordt2005}.}.


\subsubsection{Linéaments historiques}

La phase de création et de consolidation du \royaumehittite{} est mal
connue à partir des textes et l'écriture n'a dû être utilisée que
sporadiquement\footnote{\cite{Hout2009a, Hout2012a}.}. La classe
dirigeante a néanmoins rapidement réussi à s'établir et à créer de
nouvelles villes, comme l'illustre \kusakli / \sarissa{}. Les
marqueurs du \royaumehittite{} sont en place pour cette région dès le
\xvieme{} siècle\footnote{\cite{Schachner2009b}.}.

Si l'on se concentre sur la seule région du cadre de l'étude, qui est
le centre du \royaumehittite, il apparaît qu'elle est restée stable.
En effet, les principales extensions territoriales ont eu lieu en
dehors de nos limites géographiques\footnote{\cite{Klengel1999a,
Beal2011a}.}. Les rois et les reines hittites ont avant tout été
préoccupé.e.s par les territoires à l'est, au sud-est et à l'ouest,
tout en combattant régulièrement contre les \kaska{} au nord. Entre le
\xveme{} siècle et la disparition du Royaume hittite, les \kaska{}
sont régulièrement mentionnés dans les textes comme ennemi\es, et sont
tenu\es{} pour responsables de la destruction de la ville de \nerik{}
(\oymaagac{}) et de \tapikka{} (\masathoyuk).

D'une manière générale, trois événements se dégagent parmi les faits
connus par les textes, qui ont dû largement influencer l'occupation de
la région et en particulier la ville de \hattusa{} :

\begin{itemize}[noitemsep,nolistsep,topsep=0pt, partopsep=0pt]

  \item Au \xiveme{} siècle, le roi \tuthalijaIII{} et sa cour sont
  contraints à quitter \hattusa{}, la capitale, pour leur sauvegarde
  alors qu'elle est ravagée\footnote{\cite[586]{Beal2011a}.}.

  \item La première moitié du \xiiieme{} siècle, le moment pendant
  lequel la capitale \hattusa, sur décision de \muwatalliII{}, est
  déplacée vers \tarhuntassa{}, puis retransférée par son fils
  \mursiliIII{} à \hattusa\footnote{Voir \cite{Dogan-Alparslan2011a}
  et \cite{Hout2012c} avec références bibliographiques
  antérieures.}.

  \item La fin \delaph{}, qui est marquée par l'abandon presque total
  des sites et la disparition d'une autorité
  centrale\footnote{\cite{Seeher2001b}.}.

\end{itemize}


\subsection{Bilan : épigraphie et archéologie}
\label{Epilegomenes-epigraphie-archeologie}

Cette description de la région et de son évolution vont permettre de
mieux saisir dans la prochaine section le développement des recherches
concernant la capitale hittite \hattusa{} / \bogazkoy. Notons
néanmoins, comme on peut le voir dans la section sur la \pcac{} et
celle de la \ph{} (\cref{124-BM,125-BR}), que l'épigraphie joue un
rôle prépondérant dans l'analyse et la mise en perspective des
vestiges archéologiques, ce qui n'est pas sans poser problème. Alors
que le \cref{Chap3-Strati-Chrono} reviendra sur les problèmes
spécifiques de la chronologie à \bogazkoy, la
\cref{sec-13-Compendium-Bogazkoy} présente la topographie et une
chronologie des découvertes de la ville basse afin de donner une idée
des données à disposition et d'annoncer les problématiques qui seront
traités (\cref{Sec-14-problematiques}).
