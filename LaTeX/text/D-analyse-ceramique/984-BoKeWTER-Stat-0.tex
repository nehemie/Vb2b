Dans cet appendice sont regroupées les analyses statistiques et typologiques de
la céramique. Dans la mesure du possible, la méthode de \Schoop{} a été
employée (\gebQO, \gebWO, \gebRE, \gebTQ{} et \gebIR) sinon seule une étude plus
classique de la typologie a été menée (\gebOW, \gebOR{} et \gebOZ). Enfin, les
dessins des profils complets de vase retrouvés sur les sols terminent cette
section. Pour chaque ensemble, une sélection des formes typologiques les plus
représentatives sont présentées à côté des résultats de l'analyse quantitative.


\section*{Remarques préliminaires}

\subsection*{Méthode de \Schoop{}}

Dans la présentation préliminaire de ses résultats, \Schoop{} a mis en
exergue des critères pour dater un assemblage céramique à l'aide d'une étude
quantitative\footnote{\cite{Schoop2003,Schoop2006a,Schoop2009a}.}. À partir
d'assemblages de céramiques bien datés, il a créé une série de graphiques où les
assemblages sont ordonnés chronologiquement. Comme repère, il a dénommé ces
assemblages à l'aide d'une lettre alphabétique accompagnée d'un nombre renvoyant à
un siècle. Ainsi, les ensembles A18, B17, C16, D15a, E15b, F15c, G14a, H14b,
J13a, K13b couvrent successivement la période entre le \xviiieme{} et le
\xiiieme{} siècle.

Ses recherches se basent sur l'étude quantitative et qualitative de gros
ensembles céramiques. L'ensemble de la méthode repose sur le principe de la
distribution normale dans le temps. Lorsque un nouvel élément (type, décor,
méthode de fabrication, etc.) apparaît dans l'assemblage archéologique, sa
présence est, au départ, restreinte, s’accroît et atteint un apogée puis tend à
disparaître. Parmi les critères qui permettent de dater un ensemble céramique,
on retrouve la fréquence de certains types de grands bols (SHAMRD, SUKER), la
fréquence relative de deux types de bols (SUKALE-V1 par rapport à SUKALE-V3) ,
de deux types de marmite (TEKU par rapport à TED) ou encore le rapport de
trois types de bols (SHAMS, SHAMR, SUKALE-V1/V3) \footnote{Ces cinq figures ont
été regroupées sur la première page d'analyse de chaque assemblage.}. Les
critères morphologiques, mesurés sur certains types de tessons, permettent
d'affiner ces observations, notamment en prenant en considération la dimension
de la lèvre (hauteur, largeur et leur rapport) des marmites (TED) ou des
plateaux (TELD) \footnote{Ces six figures ont été regroupées sur la deuxième
page d'analyse de chaque assemblage.}.

Mon travail a visé à utiliser la méthode de \Schoop{} afin d'insérer des
ensembles non datés dans son système et d'obtenir ainsi une datation
(\cref{UlfIntegration}). Ses recherches mettent en relation la chronologie
absolue avec les assemblages céramiques. Grâce à ce travail, il a été possible
d'obtenir des datations absolues pour des ensembles sans datation radiocarbone.
En classant un ensemble selon les critères de \Schoop, il est alors possible de
déduire la datation de l'ensemble.

Néanmoins, il est encore difficile de cerner la précision de la
datation. Un assemblage céramique ne représente jamais un instant \textit{t},
mais possède toujours une profondeur temporelle, tout comme sa datation au
radiocarbone. Même selon les prémisses de Pompéi, la différence de durée de vie
des vases céramiques -- un problème souvent passé sous silence -- implique que
l'on attribue une fourchette chronologique à un ensemble. Le travail de
\Schoop{} n'est qu'une première étape et doit encore être ajusté, complété et
amélioré par l'addition de nouveaux ensembles datés par radiocarbone. Ces
ensembles permettront d'obtenir une « courbe de calibration » et juger ainsi de
la précision de la fourchette chronologique. Dans cette étude, par mesure de
précaution, seuls des ordres de grandeur sont employés, de l'ordre du
demi-siècle ou du siècle.


\opt{optbw}{
 \begin{figure}[h]
  \centering
   \begin{minipage}[]{0.97\linewidth}
   \centering
     \includegraphics[width=150mm]{Fig_85}
     \caption{Représentation schématique de la méthode employée pour dater les
     assemblages céramiques. Les cercles représentent les assemblages de
   \Schoop, datés par radiocarbone. À partir de l'évolution des assemblages, il
 est possible d'en déduire une datation pour un nouvel ensemble.}
     \label{UlfIntegration}
   \end{minipage}
 \end{figure}
 }


Les graphiques proposés par \Schoop{} forment un référentiel préliminaire et
permettent de placer un nouvel assemblage à la position qui correspond le mieux
à l'ensemble des critères pour en déduire une datation : si les valeurs de
l'étude quantitative d'un contexte se situent entre celles des assemblages B17
et C16 alors ce contexte date du \xviieme{}-\xvieme{} siècle
(\cref{UlfIntegration}). Pour que l'analyse ait un sens, une fois le meilleur
placement déterminé, il est important de toujours garder le même arrangement des
contextes et de regarder pour quelles raisons les données coïncident ou
divergent. Pour comparer mon travail aux résultats de \Schoop, j'ai recréé les
mêmes graphiques où j'ai inséré un contexte de mon étude céramique, à la place
la plus appropriée
(\crefrange{BoKeWTerGeb19-Stat1}{Fig_174})\footnote{Je remercie
\Schoop{} d'avoir mis à ma disposition le résumé de ses données de son article
\cite{Schoop2006a}.}.

Les différences doivent être revues au cas par cas, mais quelques considérations
d'ensemble sur leur origine peuvent être formulées d'emblée. Les graphiques de
\Schoop{} n'illustrent que des moyennes de valeurs et n'indiquent pas la
dispersion de l'échantillon. Une représentation des données par des diagrammes
en boîte à moustaches ou bien, les mêmes représentations accompagnées de l'écart
type permettraient de mieux cerner la tendance centrale d'une série et à partir
de quel moment une valeur s'écarte de cette tendance. Seule l'utilisation
d'outils statistiques complémentaires permet d'assurer que la différence entre
les moyennes est significative. Ni la moyenne ni la médiane ne sont des méthodes
robustes pour évaluer une population\footnote{Voir \cite{Strupler2013b} où
j'ai employé la même technique et souligné le problème de cette présentation. Je
n'ai pas pu employer d'autres méthodes en l'absence des données primaires de
\Schoop.}.

Par ailleurs, lors de la comparaison des résultats, il faut veiller à prendre
garde à l'échelle employée pour chaque graphique. Par exemple, la variation,
toutes périodes confondues, des diamètres des marmites de type TED se situe
entre 20 et 28 cm alors que celle des plats est distribuée entre 30 et 80 cm.
Dans le premier cas, une moyenne se retrouvera vite à l'écart des autres données
sur le graphique même si la différence n'est que de quelques centimètres. Dans
le deuxième cas, la variation est bien moins directement visible sur le
graphique (par ex. \cref{Fig_118}), mais une différence de
plusieurs centimètres est concrètement plus significative.



Un deuxième point général à considérer avant l'étude au cas par cas concerne la
taille des ensembles. En dessous de 150 individus, les contextes n'ont pas été
étudiés statistiquement puisque les analyses (préliminaires) mises en œuvre par
\Schoop{} sont basées uniquement sur les moyennes de quelques types. L'analyse
du \gebTQ{} n'est donc pas fiable puisque pour une partie des analyses, seul
un individu est disponible (\cref{Fig_138}).

Cette dernière remarque introduit un point à souligner dans l'étude préliminaire
de \Schoop{} : la majorité des analyses reposent sur des catégories de
céramiques faites en pâte commune alors que celles-ci sont assez mal
représentées dans l'ensemble de la \WTer{} (\SI{8.18}{\percent} du corpus). Lorsque
l'ensemble des données de \Schoop{} sera disponible, la prise en compte d'autres
critères affinera la datation. En outre, grâce à des méthodes plus robustes de
statistique exploratoire multidimensionnelle d'autres corrélations et critères
pourront être déterminés.

L'analyse des ensembles révèle néanmoins que la méthode de \Schoop{} apporte
une solide base pour la datation et pour continuer à mieux cerner l'évolution de
la céramique. Lorsqu'une datation radiocarbone est disponible, elle concorde
avec la datation obtenue pour l'assemblage céramique. Soulignons également que
sa méthode à l'avantage d'être suffisamment simple pour être réemployée et
transmise afin que les différentes bases de données soient, à terme, réunies.

\subsection*{Typo-chronologie}

Les ensembles étudiés par \Schoop{} qui se répartissent entre le \xviiieme{}et
le \xiiieme{} siècle lui ont permis de souligner à quel point certains types
perdurent sur plusieurs siècles et qu'il est difficile d'utiliser la typologie
pour dater un ensemble céramique. Néanmoins, dans son article de 2009, il
indique quelques points de repère et je m'en suis servi pour apporter des
comparaisons\footnote{\cite{Schoop2009a}.}. Lorsque des ensembles ne sont
connus que par quelques vases complets, ou bien, là où la méthode de \Schoop{}
ne peut pas être employée, alors la typo-chronologie reste la seule méthode pour
dater l'ensemble. Dans chaque cas, cela permet de mettre en vis-à-vis les vases
et la stratigraphie.
