
L'image de la \WTer{} à la période suivante est tout à fait
différente. La quantité des témoignages est sans comparaison. Des
vestiges sont retrouvés presque systématiquement, attestant le plus
souvent une présence d'habitations, comme au \NoV{} et à la \WTer{}
(\cref{Pl_31}). Si au sud-ouest de la \WTer{} les traces
d'occupation sont absentes, cela est sans doute dû à l'absence de
fouilles en profondeur plutôt qu'à l'absence d'occupation.
Malheureusement, la documentation ne permet pas d'être catégorique sur
ce point, puisque « l'absence de résultats » (la non-découverte de
vestiges) n'a pas été rapportée. La zone indiquée par les traitillés
correspond à la limite de la zone fouillée en surface et non à celle
étudiée en profondeur\footnote{Deux ou trois sondages ponctuels, voire
  carottages, pourraient aider à résoudre ce problème.}. L'état de
conservation du \NoV{} contraste fortement avec celui de la \WTer{},
où il est à peine possible de reconstituer un bâtiment dans son
intégralité. Comme cela a été souligné dans la partie sur la
chronologie et la stratigraphie (\cref{34-Stratie-Chrono}), les
vestiges architecturaux ne peuvent pas être divisés en deux phases
pour \laperiodeCAC{}. Les \gebZU{} à \gebUO{} et les \gebOW{} à
\gebOZ{} sont à attribuer à cette période et leur disposition
indiquent qu'ils étaient contemporains les uns des autres.

Seuls les \gebUP, \gebUW, \gebUE, \gebUT, \gebUU{} et \gebUI{}, tous dans le
\NoV{}, sont suffisamment complets pour permettre de dresser un portrait général de
l'architecture et ils ont déjà été commentés à de multiples
reprises\footnote{\cite[114--118]{Schachner1999a} avec bibliographie
antérieure.}. Les plans des édifices sont très similaires entre eux. La plupart
des pièces ont une surface avoisinant les \SI{15}{\square\meter} et chaque
bâtiment possède une (voire deux dans le cas du \gebUT{}) pièce(s) nettement
plus grande(s), mesurant entre \SIrange[range-phrase = ~et~
]{40}{50}{\square\metre} (\cref{SquareMeter-MBA-Rooms}). Le diagramme met en
évidence ces caractéristiques, avec pour chaque édifice une ou deux pièces dont
la surface est largement supérieure. Ces pièces plus grandes sont généralement
placées au centre de la construction et sont les marqueurs typiques du plan dit
à pièce centrale, dans laquelle une pièce commande l'accès à l'ensemble des autres
pièces\footnote{Voir, par exemple, les remarques similaires sur le plan à cour
centrale de \abusalabikh{} (\cite[60]{Postgate1994b}).}. Les pièces centrales
devaient sans doute servir de pièces à activités multiples et tout porte à croire
que celles-ci étaient couvertes et qu'il ne s'agissait pas de
cours\footnote{Voir \cite{Schachner1999a} pour une discussion à ce sujet.}.

À la différence des habitations \delaph{}, un foyer a presque toujours été
retrouvé associé à la pièce centrale et des fours ou foyers secondaires dans les
pièces adjacentes. Des assemblages importants de céramique ont à plusieurs
reprises été retrouvés ensembles, déposés dans des pièces spécifiques,
considérées comme des pièces de stockage, mais qu'il est impossible de
reconstruire précisément\footnote{Voir \cite{Strupler2013d}.}.

\subsubsection[La taille des maisons]{La taille des maisons\footnotemark}

\footnotetext{Pour éviter toute confusion, je rappelle quelques termes,
\begin{enumerate*}[label={\alph*)},font={\color{red!50!black}\bfseries}] \item
la \emph{surface hors œuvre} comprend toute la surface à l'intérieur des
murs (y compris cage d'escalier), évaluée pour le rez-de-chaussée ; \item
\emph{l'emprise au sol} correspond à la surface totale occupée par le
bâtiment, y compris celles des murs ; \item la \emph{maisonnée} fait référence
aux personnes qui habitent dans une maison. Celle-ci n'est pas forcément
équivalente à la famille, puisque une famille peut résider dans différentes
maisons ou bien plusieurs familles dans une seule maison.\end{enumerate*}}

Seul le \NoV{} a livré un ensemble cohérent de maisons qui peut servir de base
pour analyser la taille des maisons et l'utilisation de l'espace dans un
quartier (\cref{Dwelling-MBA-WTer}). L'emprise au sol des \gebUP, \gebUW,
\gebUE, \gebUT, \gebUU{} et \gebUI{} est comprise entre \SI{63}{\square\meter}
et \SI{284}{\square\meter}, la médiane se situait à \SI{177}{\square\meter}
(\cref{Dwelling-Houses-MBA-Bo-Kul}). Pour ces bâtiments, la somme totale de
l'espace hors œuvre est de \SI{717.3}{\square\meter} et correspond à
% environ %\SI{66}{\percent} par rapport à l'
une emprise totale de \SI{1064}{\square\meter}.
%Cette valeur est tout à fait comparable à la valeurs mentionnés précédemment
%pour \kultepe{} (\SI{65}{\percent}).
En prenant en considération les bâtiments qui ne sont pas complets, je restitue
une surface hors œuvre de \SI{961.7}{\square\meter} et une emprise totale de
\SI{1366}{\square\meter} (\cref{Table62-Seg-NOV-MBA}, dont les surfaces
ont été calculées comme indiqué sur le plan \cref{Dwelling-MBA-WTer}). Les rues
occuperaient \SI{17}{\percent} de la surface, légèrement plus que la surface
indéfinie. J'ai opté pour un espace indéfini pour rendre compte de l'incertitude
(et éviter d'inclure trop de catégories), mais aussi pour considérer des espaces
qui pourraient appartenir à une ou plusieurs catégories simultanément ou
alternativement.
%Si l'on se fonde sur ces nombres, on peut poser \SI{65}{\percent} comme valeur
%\emph{maximale} du rapport dans l'occupation entre la surface totale et
%l'espace occupé par des maisons. Cette valeur maximale, déduite de l'une des
%zones les plus plates et les plus denses, ne peut donc qu'être revue à la
%baisse pour d'autres zones d'occupation, soit parque la topographie se prête
%moins à une occupation dense ou parce que d'autres fonctions sont à supposer
%(temples, palais, greniers, entre autres).

%\paragraph{Excursus à Kültepe}

Récemment, \Hertel{} a composé une synthèse sur la taille et le nombre de maisons
et de maisonnées de la ville de \kultepe{}, et son travail est une ressource
extraordinaire pour \kultepe\footnote{\cite{Hertel2014a}. Je
  formulerais toutefois une critique mineure :
 l'étude ne semble pas utiliser de \gls{sig}, ce qui
aurait permis non seulement de vérifier la méthode, mais aussi d'affiner
l'évaluation. Par exemple, seule l'emprise au sol est reportée, mais pas la
surface dans œuvre.}.  C'est la première recension des maisons du niveau
\emph{\kultepe{} II} pour lesquelles il évalue l'emprise au sol et la taille des
maisonnées. Après un travail d'assemblage des plans publiés, il recense
\num{112} maisons, dont 47 sont suffisamment bien connues pour en estimer
l'emprise au sol (la surface hors œuvre n'a pas été systématiquement reportée).
De plus, il évalue que \SI{28}{\percent} de la ville basse étaient occupés par
des espaces communs (rues, places) et donc \SI{72}{\percent} par des maisons.
Dans une étude détaillée de la maison 71, il divise la phase finale en trois
pièces, une entrée avec escalier de \SI{12}{\square\meter}, une pièce de
\SI{22}{\square\meter} et une pièce de stockage de \SI{18}{\square\meter}. Au
total, cela équivaut à une surface dans œuvre de \SI{52}{\square\meter}, pour
une emprise au sol de \SI{80}{\square\meter}. %, soit un rapport de la surface
% dans œuvre à l'emprise au sol de \SI{65}{\percent}.
Pour la maison 48 (3 pièces, emprise au sol de \SI{47}{\square\meter}), il
estime d'après les textes qu'il y avait 5 occupants permanents et donc
probablement un étage pour arriver à une moyenne d'espace habitable par personne
%(surface dans œuvre à laquelle on soustrait une entrée cage / cage d'escalier)
entre \SIrange[range-phrase = ~et~ ]{8}{10}{\square\meter}.
