L'eau est un élément ambivalent, elle est essentielle à
la vie, mais elle est également une force destructrice.
Son aspect bienfaiteur est en archéologie de loin mieux
documenté. L'approvisionnement et la gestion des eaux
douces tant pour la population que pour l'agriculture
font partie intégrante de l'étude économique d'un site
ou d'une population nomade. Des études détaillées ont
illustré la diversité et l'ingéniosité des
installations hydrauliques des Hittites, en se
focalisant sur les barrages, les réservoirs ou bien
encore sur les installations cultuelles comme le bassin
cultuel
d'\eflatunpinar\footnote{\cite{Bachmann2004,Celik2008,Huser2007,Strobel2013}
  avec bibliographies plus complète. Soulignons que des
  ouvrages hydrauliques monumentaux similaires ne sont
  pas connus pour \laperiodeCAC{} ; il est néanmoins
  difficile d'exclure leur existence et leur absence ne
  pourrait être qu'un reflet de l'état de la
  recherche.}. Dans \bo{} \intramuros, on dénombre une
série de réservoirs repartis dans la ville haute, au
pied de \bk{} et dans la ville
basse\footnote{\cite[142--144]{Schachner2008a} ;
  \cite[59--71]{Seeher2002} ;
  \cite{Seeher2006e,Seeher2010a}.}. Ces installations
exploitent subtilement la géomorphologie du terrain et
la fluctuation du niveau de la nappe phréatique. Ils
dénotent la connaissance et la maîtrise approfondies de
ce domanine par les
Hittites\footnote{\cite{Schachner2012d,Wittenberg2012}.}.
En outre, de nombreuses sources naturelles sont
présentent en divers points du site.

La gestion des eaux usées n'est pas souvent prise en
considération dans la recherche archéologique. Même si
les canalisations pour l'évacuation des eaux sont
régulièrement mises au jour lors des fouilles, rares
sont les tentatives d'exploration globale pour
comprendre le réseau d'évacuation. Dès lors, il faut
souligner la qualité du travail de \Huser{} sur les
ouvrages hydrauliques de \kusakli, qui fait de surcroît
une synthèse sur les données éparses concernant
l'ensemble de \laph{}\footnote{\cite{Huser2007}.}.
Selon les connaissances actuelles, un système complexe
(intra-site) de gestion des eaux apparaît en Anatolie
de concert avec l'apparition du phénomène
urbain\footnote{\cite[34--39]{Hemker1993}.}. Il semble
que les conditions climatiques aient rendu un système
de drainage des eaux de pluie indispensable pour
répondre à la densité de la population et des
constructions. La forte déclivité du terrain à
\bogazkoy{} rend vital un système de gestion des eaux
pour éviter les glissements de terrain. Cette déclivité
a bien entendu joué un rôle déterminant dans le tracé
des canalisations dont l'écoulement est toujours
gravitaire\footnote{Dans les données des archives, il
  n'existe pas de description et les canalisations ne
  sont connues que par les relevés et quelques
  photographies.}.


\subsection{Ouvrages hydrauliques de la \pcactitre{}}

\AlaperiodeCAC{}, trois conduites sont attestées,
toutes en pierre, mais elles témoignent de trois mises
en œuvre différentes.

Premièrement, une conduite associée à la série de
bâtiments du \NoV{} se trouve partiellement sous le
\gebUT{}, puis est divisée en deux branches dont l'une
continue sous la rue entre les \gebUE, \gebUR{} et
\gebUZ{} (\cref{Kanal-MBA-NoV})\footnote{Elle fut
  fouillée en 1953, il n'y a pas de relevé particulier
  ni de photographie.}.  Cette canalisation, courant
sous les rues, devait appartenir à un réseau complexe
qui permettait à chaque maison d'en tirer profit en
assurant l'évacuation des eaux de ruissellement des
rues et des zones surplombant la ville
basse\footnote{Il n'existe pas de mention de la
  présence ou de l'absence de canalisation à l'ouest du
  \NoV{}, de sorte qu'on ne sait, si une canalisation
  (souterraine) n'a tout simplement pas été repérée.}.
Ce type de canalisation est également connu à
\kultepe\footnote{Celui-ci n'est presque pas documenté
  : \cite[97]{Ozguc1959}. Voir notamment la
  photographie dans \cite[102\psq]{OzgucT2003a}.}.

 \opt{optbw}{
   \begin{figure}[h!]
   \centering
     \includegraphics[width=0.8\linewidth]{Fig_49}
     \caption{Conduites du \NoV{} \alaperiodeCAC{}}
     \label{Kanal-MBA-NoV}
   \end{figure}
 }



Deuxièmement, à \nwh{}, une canalisation est placée
parallèlement et à côté de la rue du \gebIE{} et se
distingue de la première canalisation par ses
dimensions imposantes. Elle est large d'environ
\SI{60}{\centi\metre} et elle possède des fondations de
plus de \SI{150}{\centi\metre} de
profondeur\footnote{\cite[30]{Schirmer1969}.}. Un pont,
restitué grâce aux vestiges de bois carbonisés,
permettait de passer d'une rive à l'autre.  Néanmoins,
l'environnement proche de ce canal est trop peu connu
pour comprendre l'insertion de cet ouvrage dans le
système de gestion des eaux.

Enfin, une troisième conduite est documentée pour le
\gebIP{} de \bk{} et dirigeait les eaux de la cour
directement sur les flancs de \bk{}\footnote{\cite[26,
  \Beilage{} 18]{Neve1982}.}.

Ces canalisations, qui sont souterraines et se situent
sous les rues ou les murs, montrent que leur
établissement précède la construction des bâtiments et
prouve que le système a été conçu avant leur érection.
L'occupation \delaperiodeCAC{} fournit des données trop
disparates pour reconstituer un système. Il est
cependant indubitable qu'il est, dans son organisation,
très différent de celui \delaph{}.






\subsection{Ouvrages hydrauliques \delaph{}}

Pour \laph{} à \bo, il existe deux types de conduites,
l'un en pierre, l'autre en terre cuite
(\cref{Planning-Hittite-Kanalen})\footnote{Les
  conduites en terre cuite retrouvées à \kusakli{} ont
  fait l'objet d'un développement long dans
  \cite[157--214]{Huser2007}.}.




Les conduites voûtées en pierre sont des installations
soignées, d'assez grande taille (environ \SI{1}{\metre}
de hauteur et \SI{1}{\metre} à la base) et qui ont été
aménagées presque systématiquement sous les principales
voies d’accès.  La technique de construction est
similaire à celle des poternes, à une échelle bien
inférieure, avec une voûte en encorbellement
(\cref{CoupeKanalStrass}).

Les conduites en terre cuite sont constituées d'une
succession de tuyaux coniques d'environ
\SI{70}{\centi\metre} de long et \SI{15}{\centi\metre}
de diamètre s’emboîtant les uns dans les autres
(\cref{Lab_Bo73-Tonrohre}). Chaque tuyau possède une
ouverture à son extrémité la plus grande, bloquée par
une pierre, pour la mise en place et la maintenance des
conduites\footnote{\cite[204--207]{Huser2007}.}.




\subsubsection{Canalisations en pierre}
\label{canalisations-en-pierre} \vspace{-0.3cm} Cinq
canalisations en pierre forment le squelette du système
de drainage de la ville basse. Elles sont situées sous
les principaux axes de circulation : \tempelstr,
\wterstr, \strR, \strT{} et \strZ{}. Les canalisations
\tempelstr{} et \wterstr{} passent sous les portes de
la ville, qui sont des points bas\footnote{Hemker a
  déjà souligné à cet égard la complémentarité entre
  portes de ville et canalisations :
  \cite[33\psq]{Hemker1993}. Ce type de canalisation
  est connu également sous la porte nord de \bka{}
  (voir \cite[60 fig. 4. 7]{Seeher1995}).}. Ces deux
canalisations, placées sous les principales voies
d'accès, sont de taille plus importante et servent de
collecteurs principaux.


\paragraph{\tempelstr{}}\label{canal-tempelstr}

La portion la mieux connue de la canalisation
\tempelstr{} se situe sous la porte. Elle est
documentée par une coupe transversale et longitudinale
(\infra{} \cref{431-Tor-Temepelstr} ;
\cref{TorTempelstr,TorTstr-Coupe},
\cpageref{TorTstr-Coupe}). La canalisation mesure un
mètre de profondeur\footnote{\cite[119]{Naumann1984}.}.
Elle est également connue entre les \gebQ{}, \gebW{} et
\gebE{} (\cref{geb-1}) et ont peu supposer qu'elle
était connectée à la canalisation entre le \tempelI{}
et le \komplexI. Là, la canalisation n'a pas été mise
au jour en raison du pavage des dalles massives de la
rue. À l'est, cette canalisation devait recueillir
l'effluent du canal interne du
\komplexI{}\footnote{Deux phases sont attestées pour ce
  tronçon, (\cite[11\psq]{Neve1970}). Une pierre de
  couverture avec une ouverture circulaire, sans doute
  similaire à celle du \NoV, a par ailleurs été
  découverte.} et être raccordée à la canalisation qui
se situe l'est du
\tempelI\footnote{\cite[8]{Neve1970}.}.  Seule la
portion entre le \gebQ{} et la \tortstr{} est
suffisamment connue pour estimer la pente de la
canalisation à \SI{13,5}{\percent}\footnote{Entre les
  cotes d'altitude \num{1000.44} et \num{984.07} pour
  une distance de \SI{120}{\metre}.}.



\paragraph{\wterstr{}}\label{canal-wterstr}


La canalisation \wterstr{} file sous la \torwterstr{}
et a été très bien dégagée dans le \NoV. Ici, deux
phases sont attestées, avec un changement d'orientation
de la canalisation et une restructuration complète du
système. Le remaniement a réorienté l'évacuation de
l'eau en passant d'une orientation vers le nord (phase
ancienne) à une orientation vers l'ouest, en direction
de la porte (phase plus récente). Cette réorientation a
dû avoir lieu lorsque la porte a été créée ou
transformée\footnote{\infra{} \cref{432-Tor-WTerstr}.},
à un moment où la circulation a été repensée. Du point
de vue de la construction, la canalisation la plus
ancienne est légèrement plus large et elle est
construite avec des pierres de couverture un peu plus
grandes. La canalisation plus récente mesure entre
\SI{1,1}{\metre} et \SI{1,2}{\metre} à la base.

\opt{optbw_photo}{
\begin{figure}[h!]
 \begin{minipage}[c]{0.47\linewidth}
    \centering
    \includegraphics[width=0.9\linewidth]{Fig_50}
    \caption{Canalisation récente sur les vestiges \delaperiodeCAC{}.
      Vue de l'ouest.}
    \label{Photo-CanalNoV-Jung}
 \end{minipage} \qquad
 \begin{minipage}[c]{0.47\linewidth}
    \centering
    \includegraphics[width=0.9\linewidth]{Fig_51}
    \caption{Canalisation ancienne sur les vestiges \delaperiodeCAC{}.
      Vue du nord.}
    \label{Photo-CanalNoV-Alt}
  \end{minipage}
   \begin{minipage}[]{\linewidth}
   \centering
   \fbox{\includegraphics[width=159mm]{Fig_52}}
   \caption{Relevé des canalisations de la \wterstr{} (Neve, 1958). Le carroyage de \SI{10}{\meter} de côté est
       orienté vers le nord. \WTPe}
     \label{Plan-Kanal-WterStr}
   \end{minipage}
 \end{figure}
 }

\FloatBarrier




Aucun raccord entre la canalisation et les bâtiments
environnants n'est attesté.  Seule une pierre de
couverture avec une ouverture centrale circulaire
témoigne d'une communication directe avec la surface.
La pente, d'après la portion révélée à \NoV{}, est de
\SI{1,68}{\percent}\footnote{Entre les cotes d'altitude
  \num{985.25} et \num{984.93} pour une distance de
  \SI{19}{\metre}. Les cotes d'altitude ne renseignent
  que les pierres sommitales, ce qui relativise la
  précision du calcul.}.

Ces deux grands axes, \wterstr{} et \tortstr{}, presque
parallèles, ont servi de points de repère pour
l'érection de canaux transversaux : \strR{}
(\cref{Plan-Kanal-Strasse4}), \strT{}
(\cref{Plan-Kanal-Strasse5}) et \strZ{}
(\cref{Plan-Kanal-Strasse6}). Ces canalisations sont de
taille moindre et appartiennent au réseau secondaire.
Elles sont raccordées soit à la \tempelstr, soit à la
\wterstr. À la différence de ces dernières, ces
canalisations de second rang montrent de nombreux
coudes s'adaptant à l'irrégularité des rues
(\cref{Releve-CanalCoude-str6,Foto-CanalCoude-str6}).
Les raccords entre les canalisations sont le plus
souvent à angle droit. On peut en conclure que la
pression de l'eau à l'intérieur des canaux ne devait
pas être élevée, car la paroi opposée à l'intersection
est soumise à la décharge du canal. De même, la
construction du \gebQI{} sur le canal de la \strT{}
prouve que ceux-ci n'étaient pas considérés comme
\citfr{menaçants}.

Du point de vue de la planification du quartier, la
hiérarchisation permet de souligner la répartition
spatiale de ces canalisations. Elles divisent l'espace
entre \tempelI{} et \abschnittsmauer{} en quatre
surfaces plus ou moins égales, de sorte que
l'éloignement maximum n'excède pas les \SI{15}{\metre}.


\opt{optbw}{
  \begin{figure}[h]
    \begin{minipage}[t]{0.48\linewidth}
    \centering
    \fbox{\includegraphics[width=0.9\textwidth]{Fig_53}}
      \caption{Relevé de la canalisation \strZ{} (fouilles de 1973)}
      \label{Releve-CanalCoude-str6}
    \end{minipage} \hfill
    \begin{minipage}[t]{0.48\textwidth}
      \centering
      \includegraphics[width=\textwidth]{Fig_54}
      \caption{Photo de la canalisation \strZ{} (vue de l'ouest, 1973)}
      \label{Foto-CanalCoude-str6}
    \end{minipage}
  \end{figure}
}

\afterpage{
  \newgeometry{top=2cm,bottom=2.4cm,left=2.2cm,right=2.2cm}
  \begin{landscape}


 \end{landscape}
}
\subsubsection{Un lieu d'aisances ?} \label{452-Toilette}

Alors que le canal continue sous le \gebQI{}, le
\strT{} se termine en cul-de-sac. Là, une construction
rectangulaire prend singulièrement place, exactement au
bord du canal (\cref{Plan-Kanal-Strasse5}). Aucun autre
indice n'existe sur cette structure, mais on pourrait y
voir un lieu d'aisances, qui tirerait profit de la
proximité du canal et de l'isolement. On peut même
imaginer que les bâtiments environnants n'aient pas eu
de regard direct sur l'installation (en raison des
odeurs). De plus, le coude de la \strT{} rend la
construction bien isolée.

À ma connaissance, c'est la première proposition pour
reconnaître une structure d'aisances dans la région de
la boucle du \kizilirmak. Cet état surprend, puisque la
gestion des déchets, notamment des excréments humains,
est nécessaire pour contenir la diffusion de
parasites\footnote{Voir \cite{Mitchell2015a}, et
  notamment les contributions sur le Proche-Orient
  (\cite{McMahon2015a}) et la Crête minoenne
  (\cite{Antoniou2015a})}. Si c'est la seule structure
connue, il est nécessaire d'envisager que la plupart
des excréments humains étaient régulièrement éliminés à
l'extérieur de la ville.

\subsubsection{Conduites en terre cuite}

Somme toute, les conduites en terre cuite sont assez
rares et seuls trois exemples sont bien connus
(\cref{TonRohre-Strasse6--Geb28,Plan-Kanal-Strasse5,Plan-Kanal-Strasse4}).
Deux conduites relient directement une pièce précise
d'un bâtiment à un canal et ont une pente de
\SI{5,2}{\percent} et de \SI{4,8}{\percent}
(\cref{Plan-Kanal-Strasse5,Plan-Kanal-Strasse4}). On
peut supposer que les espaces reliés étaient des cours,
justifiant une évacuation d'eau et, par corollaire, que
les autres espaces étaient couverts. En revanche, la
conduite de la \cref{TonRohre-Strasse6--Geb28} joue le
rôle de collecteur d'eaux de pluie de la \strZ{} sur
une vingtaine de mètres et elle a une inclinaison de
\SI{6,3}{\percent}\footnote{\SI{0,9}{\meter} de
  dénivelé pour une distance de \SI{14}{\meter}.}.

\opt{optbw}{
 \begin{figure}[h]
   \centering
   \begin{minipage}[]{0.83\linewidth}
   \centering
   \fbox{\includegraphics[width=105mm]{Fig_55}}
     \caption{Canalisation en terre cuite direction \tempelstr. 1/200\e}
     \label{TonRohre-Strasse6--Geb28}
   \end{minipage}
 \end{figure}
 }
