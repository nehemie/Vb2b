Dans cette section sont présentés les principaux bâtiments et
structures de la \WTer{}. Pour éviter toute confusion avec les données
de \Neve, tout en permettant de les comparer facilement, j'ai renommé
les bâtiments en \textsc{Gebäude}, en m'appuyant sur sa
numérotation. Ainsi, le \gebQ{} correspond à la \textsc{Haus 1} de
\Neve{} et dans de nombreux cas, les références sont rétrocompatibles.
Néanmoins, un nouveau système de numérotation s'imposait, puisque
\Neve{} a simplement appelé «~phase ancienne~» une construction qui se
trouvait sous une autre, même si elles n'avaient aucun rapport entre
elles. J'ai donc procédé dans certains cas à une reclassification des
édifices pour dissiper toute confusion possible. Enfin, une nouvelle
dénomination a permis de mettre en place une numérotation continue,
qui s'étend désormais de 1 à 96 et intègre l'ensemble des fouilles.

Les édifices ont été délimités selon la méthode de lecture de plan
explicité dans la \cref{542-LecturePlans}. Pour chaque construction,
un plan à l'échelle \WTPe{} accompagne la description
(\cref{Annexe-Plans-Couleur}), et si du matériel est associé à
l'édifice, une liste fait l'inventaire des objets, et seulement ceux
qui sont inédits sont illustrés. Les plans se composent d'une
superposition des relevés et de ma propre lecture du plan. Les plans
sur lesquels j'ai réalisé la délimitation des bâtiments n'ont pas été
modifiés. Il s'agit des relevés originaux au crayon ou les plans mis
au propre à l'encre. Ils fournissent de nombreux renseignements
importants (cotes d'altitude, indications sur la situation lors de la
découverte d'objets \insitu, délimitation des rochers, limites des
fouilles, etc.). Ainsi, apparaissent également des mentions sur la
fonction des espaces, comme par exemple «~cour~»\footnote{Voir la
  mention \chantier{Hof} des \gebTO, \gebZP{}. Ces plans ont été
  réalisés par \Naumann{} dans les années 1930.}. Ces indications sur
\emph{la fonction} n'indiquent en rien \emph{mon interprétation}. Sur
ce point particulier, notons qu'il est admis que la cour n'était pas
un élément fréquent de l'architecture anatolienne pour des raisons
climatiques\footnote{La cour est un élément exceptionnel et, de ce
  fait, nécessite des aménagements particuliers. Sans indication
  d'aménagement particulier, il n'y a aucune raison de considérer
  qu'une pièce centrale ou plus large soit une cour, comme ce serait
  le cas en Mésopotamie. Voir les réflexions à ce sujet dans
  \cite[80--82]{Schachner1999a}, qui sont applicables à \alaperiodeCAC{}
  comme \alaph{}.}.

Tout comme pour les numéros des édifices, j'ai essayé de suivre la
numérotation de \Neve{} pour les pièces (voir
\cref{Annexe-Plans-Schema} pour le numéro de celles-ci). Lorsque
c'était possible, les surfaces des pièces et des bâtiments ont été
listées dans un tableau (\cref{Annexe-surface} ; toutes les valeurs
sont en mètres carrés). La surface totale équivaut à l'emprise du
bâtiment au sol et n'est donc pas égale à la somme des surfaces des
pièces, qui, elles, sont exclusives des murs. Il est donc possible
de déduire l'emprise au sol des murs en déduisant la somme de la
surface des pièces de la surface totale.

La présentation de chaque bâtiment se fonde essentiellement sur la
lecture des plans et il est indispensable d'attirer l'attention sur
deux carences. La première concerne la circulation dans les maisons.
Puisque les vestiges sont souvent assez mal conservés et en l'absence
d'ouvertures visibles, il est difficile de déterminer le réseau de
circulation.  Pour être complet, il faudrait donc évoquer à chaque
fois tous les cas possibles afin de sélectionner les plus probables.
Cette démarche semblait longue et peu fructueuse et n'a pas été
suivie. Les exemples proches dans le temps et l'espace ne sont pas
nombreux, ce qui rend les comparaisons difficiles. Il ne me semble pas
approprié de faire des comparaisons trop serrées sur l'architecture
vernaculaire avec des villes éloignées (par exemple \ugarit, \emar{}
ou \tellbazi), même si elles ont été un temps sous la domination
hittite. Il est important de noter que rien n'indique non plus que
l'entrée des pièces devait être de plain-pied (voir
\cref{463--Diff-Simi-bat}). \Neve{} a souvent proposé une entrée dans
de nombreuses restitutions, mais elles sont rarement
justifiées\footnote{Par exemple \cite{Neve1978a}.}. On doit donc lier
ce problème de la circulation interne à l'utilisation du ou des
niveaux supérieurs (étage ou toit plat). La documentation de la
\WTer{} n'apporte pas de nouvelles indications et toutes les
possibilités sont ouvertes : absence d'étage, étage partiel ou étage
similaire à celui du rez-de-chaussée. À la vue des reconstitutions
proposées par les architectes pour le
\templeI{}\footnote{\cite{Neve1969c}.} ou le palais de
\buyukkale{}\footnote{\cite{Neve1982}.}, de la taille des pièces et des
murs, il semble que toutes les pièces auraient pu, techniquement,
supporter un niveau supérieur. Il n'y a pas de traces incontestables d'escalier
univoques dans les bâtiments, puisqu'il n'y a pas de marches, mais rien
n'empêche d'utiliser une échelle de meunier.

Dans les synthèses proposées ici, je ne prétends donc pas épuiser le sujet,
mais, plus modestement, proposer une première lecture simple, mais solide,
des espaces, qui pourra servir à de futurs travaux\footnote{Ainsi une
collaboration entre architectes et archéologues pourrait apporter de nombreuses
informations sur l'utilisation de l'habitat, tout comme de plausibles
reconstitutions, à l'image de ce qui s'est fait à \ugarit.}.

 \opt{optbw_photo}{
  \begin{figure}[h!]
    \vspace{-0.5cm}
  \centering
   \includegraphics[width=80mm]{Fig_16}
   \caption{Vue sur les \gebQO{} et \gebWP{}, sans ouverture.}
   \label{Img4-2-SansOuverture}
  \end{figure}
 }
