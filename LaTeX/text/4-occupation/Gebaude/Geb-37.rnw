\subsection{\gebEU{}}

Fouillé en 1957, ce bâtiment se trouve à environ \SI{1}{\meter}  en
dessous du bâtiment attenant à l'est. Pour ces raisons, celui-ci avait
été attribué à la phase «~2~», sans tenir compte de la topographie
propre du site (voir \cref{313-Sequence-NoV},
\cpageref{313-Sequence-NoV}). Le nord du bâtiment, qui se situe sous
la route moderne, n'a pas été fouillé.  \vspace{1cm}

\begin{longtable}{rcl}
 \toprule
   Nature        & Matériaux & Morphologie\\
 \midrule
 Fondation     & Pierre    & \SIrange[range-phrase = --]{1}{1,2}{\meter} d'épaisseur, jusqu'à \SI{1}{\meter} de profondeur \\
   Soubassement  & Pierre    & \SIrange[range-phrase = --]{1}{1,2}{\meter} d'épaisseur, \SI{0,4}{\meter} de hauteur \\
   Élévation     & Brique    & 2 briques d'épaisseurs\\
   Brique        & Argile    & 43*48*\SI{10}{\centi\meter} \\
 \bottomrule
 \caption{Données métriques sur l'architecture du bâtiment}
 \label{GebEU-Tab-Dim}
\end{longtable}

\opt{optbw_photo}{
  \begin{figure}[h]
    \begin{minipage}[t]{0.49\linewidth}
    \centering
      \includegraphics[width=\textwidth]{Fig_34}
      \caption{Vue de l'ouest sur les restes de l'élévation en briques crues}
      \label{Fig_34}
    \end{minipage} \hfill
    \begin{minipage}[t]{0.48\textwidth}
      \centering
      \includegraphics[width=\textwidth]{Fig_35}
      \caption{Jarre de stockage de la pièce 4}
      \label{Fig_35}
    \end{minipage}
  \end{figure}
}

Les données sur l'architecture de cette construction  sont
exemplaires, puisque
c'est l'un des rares bâtiments où des briques de terre crue, cuites lors de
l'incendie ont été retrouvées
(\cref{GebEU-Tab-Dim}, \cref{Fig_34}.).  Dans la pièce nord-ouest,
la partie inférieure d'une jarre de stockage et un vase ont été
découverts,
contenant l'une et l'autre des grains carbonisés dont des échantillons ont été
prélevés et analysés\footnote{\cite{Hopf1992}, \citfr{Sample A} et
\citfr{Sample B}. Ces échantillons n'ont pu être retrouvés en 2013. Je
remercie vivement R.~\Pasternak{} d'avoir fait des recherches pour tenter de les
retrouver.}. D'après ces recherches, la grosse jarre de stockage contenait de
l'orge et l'autre vase contenait du blé. Une photo prise lors des fouilles
(\cref{Fig_35}) montre bien qu'il s'agit d'une énorme jarre de
stockage, d'un volume d'au moins \SI{100}{\liter}. Parmi les artefacts qui ont
été attribués à ce bâtiment, on retrouve toute une série d'objets
\citfr{prestigieux} dont \Bittel{} avait déjà fait
l'inventaire\footnote{\cite[25--32]{Bittel1958}.}. De façon tout à fait
exceptionnelle, trois cachets ont été découverts dans les décombres. Ils sont
inscrits de motifs simples (croix, points). Six pointes de flèche en bronze
complètent l'inventaire du bâtiment. Bittel avait suggéré que le bâtiment ait été
détruit par un incendie provoqué lors d'une attaque, à cause du nombre important
de pointes de flèche retrouvées\footnote{\cite[26]{Bittel1958}.}. Néanmoins,
il est maintenant possible d'affirmer que c'est l'un des rares édifices qui a
été détruit lors d'un incendie, ce qui rend l'hypothèse de l'attaque peu
probable. Il semble en revanche tout à fait possible qu'il s'agisse d'un
bâtiment où logeaient des gardes, puisqu'il se situe juste au pied de la
terrasse du temple et devait donner directement sur la \wterstr.


\FloatBarrier

\opt{optR_surface}{
    \begin{minipage}[c]{0.28\linewidth}
<<'Geb37-Phase1-Tab',echo=FALSE, results='asis'>>=
## Liste
GebPhaseTabV(37,1,"Phase 1","Lab-Surface-Geb37Phase1")
@
    \end{minipage}
} \opt{opt_geb_plan}{
  \begin{figure}[h!]
    \begin{minipage}[c]{0.7\linewidth}
      \centering
      \fbox{\includegraphics[width=75mm]{Pl_98}}
      \caption{\gebEU{}. \WTOe}
      \label{geb-37}
    \end{minipage}
  \end{figure}
}



\opt{optR_list}{
<<'Geb37-Obj',echo=FALSE, results='asis'>>=
## Liste
ObjeTab(c(rep("57",16)),
        c("214","257","258","303","332":"335",
          "342","343","361","367","370","371","450","537"),
        "Objets associés au \\gebEU", "Lab-TabObjGeb37"
        )
@
}

