\subsection{\gebZR}

Ce bâtiment, considéré par \Neve{} comme une partie du \gebQE{} a été
renommé \gebZR{} (voir la description du \gebQE{}). Le \gebZR{}
recouvre le \gebIR{} et ces deux ensembles architecturaux ont le même
plan. Ce plan est tout à fait unique pour la ville basse et se
caractérise par une série de petites pièces rectangulaires, de taille
presque identique, organisées de part et d'autre d'un mur parallèle au
murs extérieurs. Ce type de plan rappelle la batterie de silos de
\nwhmp{}\footnote{\cite{Seeher2000,Seeher2006b}.}. \Seeher{} avait par
ailleurs considéré ce bâtiment comme une éventuelle batterie de silos,
sans pouvoir donner de conclusions définitives au vu des données dont
il disposait\footnote{\cite[289]{Seeher2000}.}. \Neve, quant à lui, a
proposé de reconnaître les fondations d'un bâtiment soutenant une
vaste pièce à colonnades\footnote{\cite[59]{Neve1978}.}. À partir des
données que j'ai pu rassemblées, il apparaît que le pavage est
absent\footnote{Il n'est pas possible que celui-ci n'ait pas été
  atteint, comme lors des fouilles à \nwh{} (voir
  \cite[52]{Seeher2006b}), puisque dans 4 pièces, les niveaux
  \delaperiodeCAC{} ont été atteints et, si un pavage existait à la
  base des murs, il aurait forcément été rencontré.}. Or, grâce aux
recherches de \Seeher{} sur les silos, il a été démontré que le pavage
est un des éléments essentiels aux silos, puisqu'il permet
l'évacuation de l'eau\footnote{\cite[81--82]{Seeher2006b}, pour une
  liste d'exemples.}.  De plus, aucune couche de matériaux organiques
n'a pu être observée. Il semble donc que ce bâtiment n'est pas à
rapprocher d'une batterie de silos, mais plutôt de magasins, comme
ceux du \tempelI. Par rapport aux murs, le niveau de la rue et sa
canalisation sont assez élevés, ce qui a fait supposer à \Neve{} que le
bâtiment se trouvait en contrebas de la rue. Il est cependant assez
évident que ce sont les fondations qui ont été mises au jour et on
peut imaginer un bâtiment semi-enterré, tout à fait adapté pour le
stockage.  S'il y avait un étage, il est difficile de concevoir
comment celui-ci était organisé.


\opt{opt_geb_plan}{
 \begin{figure}[h]
   \centering
   \begin{minipage}[]{\linewidth}
   \centering
   \fbox{\includegraphics[width=159mm]{Pl_121}}
     \caption{\gebZR{} et \gebIR{}. \WTPe}
     \label{geb-64--geb84}
   \end{minipage}
 \end{figure}
 }


\opt{optR_surface}{
    \begin{minipage}[b]{0.28\linewidth}
<<'Geb64-Phase1-Tab',echo=FALSE, results='asis'>>=
## Liste
GebPhaseTabV(64,1,"Phase 1","Lab-Surface-Geb64Phase1")
@
    \end{minipage}
  }\opt{optbw}{
  \begin{figure}[h]
    \begin{minipage}[b]{0.60\linewidth}
      \centering
      \fbox{\includegraphics[width=0.8\textwidth]{Fig_43}}
      \caption{\gebZR{}, Section sud de la pièce 7. Dans la légende, substituer
      \gebZR{} à «~Haus 13~», \gebIR{} à «~ALTH~», et \gebOR{} à «~ÄLTESTE MAUER~»}
      \label{geb-64-R7}
    \end{minipage}
  \end{figure}
}

