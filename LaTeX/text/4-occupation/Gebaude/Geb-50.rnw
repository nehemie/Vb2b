\subsection{\gebTP}

À l'ouest du \gebRI{} se distingue un ensemble de murs, le \gebTP, qui
se caractérise par le redoublement d'un mur et l'emploi de pierres de
plus gros calibre. La conception de ce bâtiment est néanmoins
similaire, puisque les murs au sud s'orientent aussi parallèlement au
mur extérieur du \gebRW.

\opt{opt_geb_plan}{
 \begin{figure}[h]
   \centering
   \fbox{\includegraphics[width=85mm]{Pl_108}}
     \caption{\gebTP{}. \WTPe}
     \label{geb-50}
 \end{figure}
 }

