\subsection{\gebTU{}}

Seul le sud de ce bâtiment est connu, puisqu'il se situe à la limite
des fouilles de 1938 (à l'est) et de 1956 et notamment au départ de la
longue tranchée B de 1956. Séparé du groupe d'édifices
\gebTI{}--\gebZQ{} par une rue large et rectiligne, il se situe au
bord de l'\abschlussmauer{}. Le plan est trop lacunaire pour proposer
une reconstruction. On notera, d'une part, la présence de plusieurs
portes donnant sur la rue formant une circulation non linéaire à
l'intérieur du bâtiment et, d'autre part, l'absence de cotes
d'altitude pour connaître la hauteur des seuils.

\opt{opt_geb_plan}{
 \begin{figure}[h!]
   \centering
   \begin{minipage}[]{0.8\linewidth}
   \centering
   \fbox{\includegraphics[width=110mm]{Pl_114}}
     \caption{\gebTU{}. \WTPe}
     \label{geb-57}
   \end{minipage}
 \end{figure}
 }

