\subsection{\gebTW{} et \gebTE}

Ces bâtiments, très mal documentés, appartiennent à la phase plus
ancienne.  Ils sont orientés comme le \gebRI{} et il est difficile
d'en faire l'analyse, puisque les plans sont incomplets. J'ai séparé
ces vestiges en deux bâtiments, tout comme \Neve{}, car il y a un
redoublement des murs au nord et il n'y a pas de continuité entre les
murs au sud ; notons néanmoins que cette interprétation est fragile.

Trois ensembles céramiques ont initialement été étudiés. Suite à la
relecture des carnets de fouilles, il s'est avéré que la stratigraphie
est trop incertaine et ils ont donc été laissés de côté, car ils
ne peuvent pas être considérés comme fermés (\cref{Lab-KeraGeb52}).


\opt{opt_geb_plan}{
 \begin{figure}[h]
   \centering
   \begin{minipage}[]{0.70\linewidth}
   \centering
   \fbox{\includegraphics[width=85mm]{Pl_111}}
     \caption{\gebTW{} et \gebTE. \WTPe}
     \label{geb-52-53}
   \end{minipage}
 \end{figure}
 }

\opt{optR_list}{
<<'Geb52-Kera-Liste',echo=FALSE, results='asis'>>=
KeraKontext(c("Bo77-IV4-12","Bo77-V4-4","Bo77-V4-5","Bo78-Geb52"),
            "Ensembles céramiques «~du \\gebTE{}~», enregistrés dans la base de données",
            "Lab-KeraGeb52"
            )
@
}
