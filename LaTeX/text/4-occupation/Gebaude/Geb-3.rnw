\subsection{\gebE}

Ce bâtiment s'organise autour d'une grande pièce principale à l'ouest, où
devaient prendre place les activités les plus
importantes\footnote{Bibliographie: \cite[26]{Neve1975a} ;
\cite[33--35]{Neve1975b}.}. Les autres pièces, plus petites, parfois à peine de
plus de \SI{1}{\square\meter}, ne peuvent avoir qu'une fonction de stockage.
Cette distribution des pièces, grande pièce d'un côté, petites pièces de
l'autre, est très similaire au \gebR{}, dont la pièce principale est à l'est.

\opt{optR_gen_plan}{
  \begin{figure}[h]
    \begin{minipage}[c]{0.33\linewidth}
<<'Geb3-Phase1-Tab',echo=FALSE, results='asis'>>=
## Liste
GebPhaseTabV(3,1,"Phase 1","Lab-Surface-Geb3Phase1")
@
   \end{minipage} \hfill
    \begin{minipage}[c]{0.6\linewidth}
      \centering
      \fbox{\includegraphics[width=85mm]{Pl_74}}
      \caption{\gebE{}. \WTOe}
      \label{Pl_74}
    \end{minipage}
  \end{figure}
}

Même si, d'après le plan, une jarre de stockage a été découverte
\insitu{} dans la pièce 6, aucune information à son sujet n'a été
retrouvée dans les archives.  En revanche, un assemblage d'objets en
bronze \insitu{} a été retrouvé sous le sol et publié par \Boehmer{}
(\cref{Lab-TabGeb3}). Ces objets ressemblent à des épingles,  mais ils
sont de section carrée et ne possèdent pas de chas. Le fait que ces
artefacts lot
aient été retrouvés en lot est tout à fait remarquable. Dans les cas
les plus fréquents, les épingles ou les autres objets de métal sont
retrouvés épars, ici ou là. Deux autres assemblages
similaires, avec le même type d'objets, déposés ensemble, ont été
découverts dans la ville haute ainsi qu'à \yk{}. Il est tentant d'y
voir les témoins d'un rite (de fondation ?  de purification
?)\footnote{Je remercie Suzanne \Herbordt{} pour les indications sur
la ville haute ; pour \yk{} voir \cite[64\psq]{Bittel1975b}.}.

      \setlength{\fboxsep}{0mm}


\opt{optR_list}{
<<'Geb3-Obj',echo=FALSE, results='asis'>>=
## Liste
ObjeTab(c("71","71"),c("393","394"), "Objets in situ", "Lab-TabGeb3" )
@
}
