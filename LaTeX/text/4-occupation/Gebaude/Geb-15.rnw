\subsection{\gebQT}

Édifice bien conservé, mais qui n'est quasiment pas évoqué ni dans le
rapport préliminaire des fouilles, ni dans le journal de 1975.  Le
\gebQT{} possède une forme rectangulaire qui s'articule bien avec les
bâtiments avoisinants et la \strT{}. La jonction avec le \gebU{}, qui
le surplombe, se révèle énigmatique (un mur est comme absent), mais
soulignons que le \gebU{} avait déjà été dégagé 20 ans auparavant, en
1956, par \Naumann.  Même si \Neve{} mentionne la découverte de
plusieurs vases dans le journal de fouille\footnote{Voir, par
exemple, les entrées du 01.08, du 06.08 ou du 05.09.} et que ceux-ci
sont bien visibles sur le plan, presque aucune information n'existe à
leur sujet, hormis pour le vase découvert au centre de l'édifice
(\cref{Lab_Bo75-588}, \cpageref{Lab_Bo75-588}).  Parmi les détails
intéressants, on remarque que les relevés indiquent une crapaudine
(\emph{Türangel}) sur un mur, à environ \SI{40}{\centi\meter} de
hauteur, ce qui pourrait indiquer la hauteur du seuil de la porte.  Du
point de vue de la construction, l'emploi de moellons de petit calibre
frappe immédiatement à l'attention et l'organisation est assez
similaire à celle du \gebQQ{}.



\opt{optR_surface}{
    \begin{minipage}[c]{0.28\linewidth}
<<'Geb15-Phase1-Tab',echo=FALSE, results='asis'>>=
## Liste
GebPhaseTabV(15,1,"Phase 1","Lab-Surface-Geb15Phase1")
@
    \end{minipage}%
  }%
  %
  \opt{opt_geb_plan}{
    \begin{figure}[h]
    \begin{minipage}[c]{0.7\linewidth}

      \centering
      \fbox{\includegraphics[width=90mm]{Pl_86}}
      \caption{\gebQT{}. \WTOe}
      \label{geb-15}
    \end{minipage}
    \end{figure}
}



\opt{optR_list}{
 <<'Geb15-Obj',echo=FALSE, results='asis'>>=
## Liste
ObjeTab(rep("75",4),c("322","323","347","588"), "Objets in situ", "Lab-TabGeb15" )
@
}
