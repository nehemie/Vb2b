\subsection{\gebUP}
 \vspace{-0.3cm}

Ce bâtiment \delaperiodeCAC{} a été mis au jour en
1956\footnote{\cite{Bittel1957a} ; \cite[22]{Strupler2013d}.} et un
ensemble de vases a été découvert dans la pièce oblongue (\emph{Gefäßlager}),
tandis qu'un foyer circulaire se trouvait dans la plus grande pièce. Aucune
trace des vases du \gebUP{} n'a été retrouvée, que ce soit à \bo{} ou dans les
archives\footnote{Neuf vases sont inventoriés :
\kfd{Bo56}{512}, \kfd{Bo56}{520}, \kfd{Bo56}{521}, \kfd{Bo56}{522},
\kfd{Bo56}{523}, \kfd{Bo56}{524}, \kfd{Bo56}{525}, \kfd{Bo56}{526},
\kfd{Bo56}{532}.}.



\opt{optR_surface}{
    \begin{minipage}[b]{0.24\linewidth}
<<'Geb70-Phase1-Tab',echo=FALSE, results='asis'>>=
## Liste
GebPhaseTabV(70,1,"\\textsc{Geb 70}","Lab-Surface-Geb70Phase1")
@
    \end{minipage}
  }  \opt{opt_geb_plan}{
\begin{figure}[h]
    \begin{minipage}[t]{0.49\linewidth}
      \centering
      \fbox{\includegraphics[width=75mm]{Pl_127}}
      \caption{\gebUP{}. \WTPe}
      \label{geb-70}
    \end{minipage}
  \end{figure}
  }  \opt{opt_geb_plan}{
\begin{figure}[h]
    \begin{minipage}[t]{0.24\linewidth}
      \centering
      \fbox{\includegraphics[width=35mm]{Pl_128}}
      \caption{\gebUQ{}. \WTPe}
      \label{geb-71}
    \end{minipage}
  \end{figure}
}
