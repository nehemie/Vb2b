\subsection{\gebZU, \gebZI{} et \gebZO}

Ces bâtiments \delaperiodeCAC{} ont été mis au jour en
1956--1957\footnote{\cite{Bittel1957a, Neve1958}.}. Seuls quelques tronçons de
murs, ici et là, sont connus et je les ai regroupés en trois bâtiments pour
donner une idée de l'occupation. Les rapports ou les journaux sont quasi-muets
sur ces fouilles.

\opt{opt_geb_plan}{
  \begin{figure}[hb]
    \centering
    \begin{minipage}[b]{0.32\textwidth}
      \fbox{\includegraphics[width=51mm]{Pl_124}}
      \caption{\gebZU{}. \WTPe}
      \label{Img-Geb-67}
    \end{minipage}
    \begin{minipage}[b]{0.32\textwidth}
    \fbox{\includegraphics[width=51mm]{Pl_125}}
      \caption{\gebZI{}. \WTPe}
      \label{Img-Geb-68}
    \end{minipage}
    \begin{minipage}[b]{0.32\textwidth}
      \fbox{\includegraphics[width=51mm]{Pl_126}}
      \caption{\gebZO{}. \WTPe}
      \label{Img-Geb-69}
    \end{minipage}
  \end{figure}
}

