\subsection{\gebOW}

Sous le \gebQQ{}, un inventaire de vases \insitu{} \delaperiodeCAC{} a
été mis au jour et étudié en 2012 (\cref{Stat-BoKeWTer-Geb92},
\cpageref{Stat-BoKeWTer-Geb92}). Malheureusement, seul un pan de mur,
que j'ai nommé \gebOW, a pu être associé à cet inventaire. Cet édifice
est contemporain du \gebOE{}.


\opt{opt_geb_plan}{
 \begin{figure}[h]
   \centering
   \begin{minipage}[]{0.83\linewidth}
   \centering
   \fbox{\includegraphics[width=92mm]{Pl_141}}
     \caption{\gebOW{}. \WTPe}
     \label{Img-Geb-92}
   \end{minipage}
 \end{figure}
 }


\opt{optR_list}{
<<'Geb92-Karum-Obj-Liste',echo=FALSE, results='asis'>>=
## Liste
ObjeTab(rep("73",6),
        c("484":"488", "491"),
        "Vases associés au \\gebOW{} (\\cref{Stat-BoKeWTer-Geb92}, \\cpageref{Stat-BoKeWTer-Geb92})",
        "Lab-TabGe92"
        )
@
}
