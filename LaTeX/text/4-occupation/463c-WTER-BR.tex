\subsubsection{Différences et similitudes entre les bâtiments}
\label{463--Diff-Simi-bat}

La circulation à l'intérieur d'un bâtiment est un des points cruciaux pour
comprendre l'agencement des pièces et induire leur utilisation. Lorsque les
conditions le permettent, c'est l'étude de la circulation et des installations
qui conduisent à définir différentes maisons. De façon inattendue, il n'existe
pas de traces d'entrée pour les bâtiments, ni de seuil indiquant le passage d'une
pièce à l'autre. Même dans le cas où le départ des murs est bien conservé, les
seuils sont le plus souvent absents. Dans de nombreux cas, seules les
fondations sont conservées, ce qui élimine de facto la possibilité de découvrir
des seuils. Des exemples de seuils indiqués sur un plan (\gebRP{}, \cref{geb-40b})
indiqueraient qu'ils pouvaient se situer à quelques \SIrange[range-phrase =
--]{50}{80}{\centi\meter} au-dessus du sol d'occupation, tant pour l'entrée que
pour les passages internes entre les pièces. La description de l'architecture a
cependant montré qu'une telle élévation n'est qu'exceptionnellement conservée.
Il n'est donc pas possible de déduire la circulation de la lecture du plan. Les
découvertes faites dans les autres chantiers de \bo{} (\bk{},
\oberstadt\footnote{\cite[pl. 42]{Neve1999}.}) et dans les sites à l'intérieur
de la boucle de \kizilirmak{} (\kusakli{}\footnote{\cite[pl. 2.
6]{Mielke2006}.}, \alaca{}\footnote{\cite[pl. 86. 93. 98]{Kosay1966a}; \cite[pl.
86--92]{Kosay1973a}.}, \alisar{}\footnote{En particulier, la \textit{mansion},
\cite[pl. 17]{Osten1937b}.}, \hdd{}\footnote{\cite[183]{Sipahi2004a} ;
\cite[275]{Sipahi2005b}.}, \masathoyuk\footnote{\cite[pl. 5. 9. 10.
12]{OzgucT1982a}.}) indiquent une tendance tout à fait similaire pour
l'architecture vernaculaire, où les seuils sont absents (ou ne sont pas
reconnus). Les portes et les passages indiqués sur les plans sont le plus
souvent restitués, mais il n'y a pas d'attestation formelle. Ne pas avoir
d'ouverture de plain-pied a plusieurs avantages, par exemple pour empêcher des
intrus d'entrer lorsque la porte (?) est ouverte, notamment les animaux
domestiques (cochons, chiens, moutons), ou bien empêcher des enfants en bas âge
de sortir. Évidemment, des arguments plus poussés pourraient être formulés
pour reconstituer la circulation, en essayant, entre autres, de définir l'accès
au bâtiment, puisqu'il devait forcément se trouver du côté de la rue. Il
apparaît cependant acquis qu'il n'existait pas de solution unique. Un des
rares contre-exemples avec une ouverture de plain-pied provient du bâtiment F
de \bk{}\footnote{\cite[58 pl. 28a]{Neve1982}.}. Là, deux ouvertures
pour passer entre deux pièces sont très bien conservées. Les murs en moellons
sont conservés sur une hauteur d'environ \SI{1}{\meter} et les deux passages
étaient de plain-pied et d'une largeur de \SI{90}{\centi\meter}. Une réflexion
globale sur ce point de l'architecture reste à mener pour l'architecture
hittite. Elle devra prendre en considération la troisième dimension et passer de
la restitution du plan à celle du volume.




\paragraph{}

\opt{optbw}{
  \begin{figure}[h!]
    \centering
    \includegraphics[width=1\linewidth]{Fig_57}

    \caption{Histogramme de la taille des pièces des maisons de la \WTer{} \alaph{}
    dont le plan peut être reconstitué. Classes de \SI{5}{\square\metre}
    dont le nombre total a été défini selon la règle de Scott. La moyenne est
    de \SI{12.8}{\square\metre} ; la médiane de \SI{9.8}{\square\metre}.}

    \label{SquareMeter-Hittite-Rooms}
  \end{figure}
}

Si l'on regarde la taille des pièces (hors œuvre), on note que ce sont celles
qui mesurent entre \SIrange[range-phrase ={} et {}]{5}{10}{\square\meter} qui
sont de loin les plus courantes, viennent ensuite celles qui ont une surface
entre \SIrange[range-phrase = {} et {}]{0}{5}{\square\meter} ou
\SIrange[range-phrase ={} et {}]{10}{15}{\square\meter}
(\cref{SquareMeter-Hittite-Rooms}). Les pièces mesurant plus de
\SI{20}{\square\metre} sont rares. Il existe sans doute des contraintes
techniques à ne pas créer des espaces trop larges, mais cette disposition
marque une volonté d'avoir des petites pièces à disposition. \AlaperiodeCAC{}, ce
sont les pièces qui ont une surface entre \SIrange[range-phrase ={} et
{}]{10}{15}{\square\meter} qui sont les plus fréquentes (médiane de
\SI{14.4}{\square\meter}). Si l'on s'intéresse à l'ensemble des bâtiments,
l'emprise au sol est un indicateur permettant de les comparer et de les classer
aisément (\cref{SquareMeter-Hittite-Building})\footnote{Seuls sont considérés les
bâtiments dont les plans sont complets. Ils sont étudiés ici selon l'attestation
de leur phase la plus récente. Ainsi, si une phase 3 est connue, c'est celle-ci
qui a prévalu sur l'étude de la phase 2, qui elle-même a prévalu sur la
phase 1.}. Les valeurs s'échelonnent entre
\SI{26.30}{\square\metre} (\gebRR{}) et \SI{537.50}{\square\metre} (\gebTI). Les
trois premiers quarts de cet échantillon s'échelonnent régulièrement, mais
différents groupes se dégagent.




\opt{optbw}{
 \begin{figure}[p!]
   \centering
   \includegraphics[width=1\linewidth]{Fig_58}

   \caption{Diagramme de l'emprise au sol (en $m^2$) des maisons \alaph{} de la
   \WTer{} dont le plan peut être reconstitué, accompagné d'une boîte à
   moustaches. Les bâtiments ont été ordonnés selon leur emprise et trois
   groupes sont à distinguer. Le premier avec les cinq bâtiments les plus petits
   et le troisième avec les bâtiments de plus de \SI{300}{\square\meter},
   correspondant à \SI{25}{\percent} de l'échantillon.}

   \label{SquareMeter-Hittite-Building}
   \vspace{0.5cm}
   \includegraphics[width=1\linewidth]{Fig_59}

   \caption{Diagramme de dispersion du logarithme décimal des couples
   \textit{Emprise} et \textit{Surface} des maisons au plan complet \delaph{}. En
   trait plein, la droite de régression orthogonale selon la méthode des
   moindres carrés.}

   \label{LogSurfaceDwelling--Hittite-Building}
 \end{figure}
}





Tout d'abord, les surfaces les plus modestes regroupent les plans de bâtiments
composés d'une ou deux pièces (\gebRR, \gebZZ, \gebRE{} et \gebWP), comme le
\gebWP{} mesurant \SI{40}{\square\meter}. Ne serait-ce que d'après la taille disponible, une
fonction résidentielle n'est pas envisageable pour ces bâtiments. Par exemple,
le \gebRE{} pourrait être interprété comme un bassin, sans doute associé à des
rituels, comme le \gebRR{}. Le \gebWP{} comporte lui aussi une pièce pavée, mais
aucun élément n'aide à son interprétation. S'agit-il également d'un bâtiment
dédié à la manipulation de l'eau, aux ablutions ?
%
Les bâtiments suivants dans le diagramme sont composés de trois ou quatre pièces
et commencent à avoir la forme de ce que l'on peut considérer comme une unité
résidentielle (\gebTZ, \gebO, \gebEW, \gebZQ, \gebRO{} et \gebTT{}). À compter des
\gebZP{} et \gebRQ{}, une pièce centrale plus grande se dégage et les bâtiments
occupent une surface de \SI{100}{\square\metre}. À partir des \gebEZ{} et
\gebRT{}, d'environ \SI{150}{\square\metre}, une deuxième pièce adjacente de
grande taille fait son apparition et les plans se complexifient jusqu'au \gebW{}
avec un nombre toujours plus important de pièces. Tous ces bâtiments semblent
avoir eu une fonction résidentielle et peuvent donc être considérés comme des
maisons.

Le dernier quart de ce jeu de données, au-dessus du troisième quartile de
\SI{213.4}{\square\meter} (\cref{SquareMeter-Hittite-Building}), se situe à une
rupture dans l'échelonnement de la taille des bâtiments. Les \gebQI{} et
\gebQZ{} mesurent 300 \mc{} et se distinguent clairement des autres bâtiments
tout comme ceux encore plus vastes. Les \gebQE, \gebRW{}, \gebZR{}, \gebU{} et
\gebTI{} ont tous des plans assez uniques qu'il est
difficile de résumer. De par leur taille et leur position, ils avaient une
visibilité tout à fait particulière et avaient des fonctions, au moins
partiellement, administratives, de production ou de stockage.

\paragraph{}

Pour analyser plus en détail ces bâtiments, on peut étudier la relation entre
emprise au sol et surface hors œuvre. Il va de soi que la surface hors œuvre
d'un bâtiment est corrélée avec l'emprise au sol. Plus l'emprise au sol est
importante, plus la surface hors œuvre est grande. Mais encore ? Que peut-on
dire de cette relation ? Quels sont les bâtiments dont le rapport est
«~optimal~», c'est-à-dire dont la surface hors œuvre est importante par rapport
à l'emprise au sol, ou en d'autres termes, dont la surface occupée par les murs
est moindre ? Quels sont ceux dont la relation est «~suboptimale~», c'est-à-dire
que la surface hors œuvre est moindre par rapport à l'emprise au sol, ou en
d'autres termes, ceux dont la surface occupée par les murs est très importante ?
Une étude de la relation de la surface avec l'emprise met en valeur les
bâtiments dont les plans sont massifs par rapport aux bâtiments aux plans plus
aérés (\cref{LogSurfaceDwelling--Hittite-Building}).

Les distributions des variables \textit{Emprise} et \textit{Surface},
asymétriques, ne peuvent être considérées comme normales, mais elles
sont liées par une relation de type allométrique (surface = k $\times$
emprise\textsuperscript{a}). La transformation logarithmique
(décimale) que je propose assure que les conditions de normalité
exigées par les statistiques paramétriques soient respectées. La
corrélation linéaire entre les nouvelles variables est assurée (la
valeur-$p$ du test de Fischer est inférieure à \num{2,2e-16} et les
résidus satisfont les conditions requises). L'emprise et la surface
des bâtiments n'augmentent pas à un rythme constant, mais
l'augmentation de la surface est, en proportion, plus rapide que celle
de l'emprise. La droite de régression explique ici environ
\SI{96.4}{\percent} de la variance ($r^2$).  De cette analyse, on peut
en déduire la relation entre emprise et surface en mesurant la
distance orthogonale de chaque point par rapport à la droite de
régression linéaire. Les bâtiments qui sont les plus éloignés
au-dessus de la barre (\gebZW, \gebQQ, \gebRZ{}, \gebEW{} et \gebQO{})
sont ceux qui ont une surface plus importante par rapport à l'emprise
au sol. Au contraire ceux qui sont les plus éloignés au-dessous de la
barre (\gebQR, \gebU, \gebWP{}, \gebQ{} et \gebQE{}) sont ceux qui ont
une surface moindre par rapport à l'emprise au sol.

Tout d'abord, la division en trois groupes principaux (selon la taille) se
retrouve bien visible sur ce graphique. À ma surprise, le \gebZR{}, dont le
plan a été interprété comme celui d'un édifice dédié au stockage, n'est pas
massif. En revanche, la particularité de celui qui s'éloigne le plus de la
tendance, le \gebQR{}, est renforcée. Ce bâtiment qui se situe en plein milieu
de la \WTer{} est très massif et on pourrait supposer que ce bâtiment était plus
conçu ainsi pour supporter une élévation plus importante afin de capter plus de
lumière. Parmi les bâtiments massifs, on retrouve ceux qui sont construits avec
de gros moellons (\gebQ{}, \gebU{} et \gebQE{}). On peut conclure qu'ils devaient
être particulièrement impressionnants. Parmi les bâtiments les plus légers, il
est intéressant de noter que ceux-ci n'appartiennent pas au groupe des bâtiments
à la plus grande emprise au sol, mais aux maisons avec une ou deux pièces
principales particulièrement larges.

Néanmoins, il est particulièrement évident que la relation entre emprise au sol
et surface hors œuvre est très régulière, ce qui plaide pour une homogénéité dans
la construction et témoigne de la régularité dans l'élaboration des plans et
leur mise en œuvre. Il s'agit là d'un indice puissant du professionnalisme
impliqué dans la construction des bâtiments.

\subsubsection{Élévation}
\label{463-elevation}

Les découvertes de la \WTer{} n'apportent pas d'éléments concrets sur la
présence d'un étage aux bâtiments : il n'y a pas d'escalier ni d'autres indices
qui laissent supposer d'une telle présence\footnote{\Neve{} avait déjà formulé
une remarque similaire, \cite[49]{Neve1978a}.}. Il est néanmoins plus que
probable que, dans certains cas, des bâtiments étaient semi-enterrés et d'autres
avaient un étage, notamment lorsque le plan est massif. Cette question est
intimement liée à celle, tout aussi élusive, de la circulation. Élévation et
circulation sont d'une importance capitale pour savoir comment les pièces
étaient éclairées, quelle était
la luminosité de chaque pièce et, par conséquent, mieux comprendre leur
utilisation. En outre, l'élévation est une donnée essentielle pour prendre en
considération la surface totale \emph{habitable} et non pas simplement
l'emprise au sol.

Néanmoins, le faisceau d'indices est encore trop faible pour pouvoir
justifier la présence ou l'absence d'un étage. Les méthodes de
fouilles utilisées ainsi que la conservation des vestiges ne
permettent pas de répondre.  Techniquement,  toutes les pièces s'y
prêtent, d'après les dimensions et les matériaux à disposition.
Certains bâtiments, notamment ceux qui sont très massifs (\gebQR,
\gebU, \gebWP{} et \gebQ{}) avaient peut-être un étage avec moins de
pièces, mais plus larges, pour réaliser d'autres activités. Les
bâtiments les moins massifs, au contraire, avaient peut-être moins de
chance de supporter un étage complet, mais uniquement quelques pièces
(\gebZW, \gebQQ, \gebRZ{}, \gebEW{} et \gebQO{}). Pour chaque
bâtiment, une infinité de reconstructions sont possibles, auxquelles
on peut ajouter le problème de la circulation interne. Pour chaque
plan, il faut toujours garder à l'esprit que ni la circulation ni
l'élévation ne sont restituées. En l'état des données publiées à
l'intérieur de la boucle du \kizilirmak, seule une étude comparative
pourraient éventuellement apporter quelques réponses.

Enfin l'absence de crapaudines et d'ouvertures imposent de se demander si
l'accès aux pièces ne se faisait le plus souvent pour le haut. Comme
on l'a souligné
précédemment, l'absence d'ouverture devrait inviter à considérer cette option
comme une éventualité et, plutôt que de supposer que les entrées des bâtiments se
trouvaient le plus souvent de plain-pied, il faudrait tout d'abord le démontrer.
Les temples, en revanche, et en particulier le \templeI, ont souvent un seuil
soigné, qui est resté bien visible. Peut-être que cette particularité est due à
la fonction de ces bâtiments, où le stockage à grande échelle et le transport de
denrées volumineuses avaient un rôle très important. Aussi l'accès de
plain-pied était-il préféré. Notons néanmoins que l'accès par le haut ne semble
pas être utilisé en Anatolie centrale pendant l'âge du Bronze, ce qui oblige a
rester tout de même très prudent au sujet de cette hypothèse.

\subsubsection{Installations}

Tout comme les seuils de portes, rares sont les installations qui ont été
découvertes en association avec l'architecture. Parmi les installations les plus
fréquentes, on retrouve les foyers et les fours. Alors que ces structures sont
systématiquement découvertes dans les maisons \delaperiodeCAC{}, seuls quatre exemples sont
à mentionner pour \laph{} de la \WTer{}.

Le \gebRT{} possède un four en forme de fer à cheval. D'après les
parallèles de \bk\footnote{\cite[58--60]{Neve1982}.} et de
\knw{}\footnote{\cite[39]{Schachner2010b}.}, ce four devait avoir une
ouverture sur le dessus et une autre sur l'un des ses flancs. Tout
comme pour l'exemplaire de \bk{}, le four du \gebRT{} est placé à
proximité des murs ce qui montre que l'intensité du feu ne devait pas
être trop intense.

Les foyers sont légèrement plus fréquents, puisqu'ils sont attestés pour les
\gebRT, \gebRZ{} et \gebTO. Il s'agit systématiquement de foyers construits, le
plus souvent avec une sole composée de fragments de céramique, soit de forme
circulaire, soit rectangulaire, et qui est recouverte d'un enduit de terre. En
général, l'épaisseur de la sole n'est que de quelques centimètres, mais témoigne
de nombreuses réfections, puisqu'elle augmente avec le renouvellement de la
surface.

Ces deux types d'installations ont été utilisés pour produire une chaleur
modérée, utilisable pour chauffer une pièce, préparer de la nourriture
ou confectioner des objets dans
le cadre de petits travaux artisanaux.



\subsubsection{Vases de stockage et cuves}

Les vases de stockage et les cuves sont le plus souvent enterrés, soit
seulement de quelques centimètres, soit plus profondément et sont
alors retrouvés
semi-enterrés. Il s'agit donc d'installations que l'on peut considérer comme
immobiles. Leur étude souffre du manque de données, puisque ces éléments n'ont
pas été traités comme des objets mobiles : ils ne possèdent pas de numéro
d'inventaire et n'ont pas été restaurés. Il est le plus souvent impossible de
glaner plus d'informations que la simple mention «~pithos~» que l'on trouve sur
des plans et l'on ignore leur capacité.

Des vases de stockage sont attestés pour les \gebR{}, \gebWO{}, \gebEZ{},
\gebEU{}, \gebTQ{} et \gebTO{}. En général, plusieurs exemplaires (3) sont
disposés côte à côte. D'après la taille des vases et leur position dans un
recoin d'une petite pièce, on peut affirmer qu'il s'agit de vases de stockage.
Il n'y a pas de mention des contenus, sauf dans le cas du \gebEZ{}.
Ici
l'analyse des prélèvements a permis d'identifier des graines
d'orge\footnote{\cite[échantillon A]{Hopf1992}.}. Du même bâtiment, un
échantillon provenant d'un vase de plus petite taille contenait lui du
blé\footnote{\cite[échantillon B]{Hopf1992}.}.

Les cuves sont nettement plus rares et attestées pour trois bâtiments: \gebWO,
\gebEZ{} et \gebTR. Ces contenants ont dû servir pour manipuler des liquides (pour
faire des ablutions, des teintures ou diverses macérations).

\subsubsection{Discussion}



Ce bilan est maigre par rapport à ce qu'il était possible d'attendre
d'une fouille aussi ambitieuse. Il est mieux perceptible, si
on le met en perspective avec les résultats des fouilles de la ville ouest
de \tellbazi{}\footnote{\cite{Otto2006}.}. Assez éloigné
géographiquement, \tellbazi{} se situe en Syrie, au bord de
l'Euphrate, à \SI{60}{\kilo\meter} de la frontière turque. Le site a
été occupé de 1250 à 1180 \avne{} et se trouvait dans la zone
d'influence du vice-roi hittite de \karkamis{}. Là, l'occupation a été
brève et s'est terminée par une destruction rapide, provoquant un
enfouissement d'une grande quantité de matériel. Les vestiges
mobiliers et immobiliers ont permis d'étudier les activités exercées à
l'échelle de chaque pièce de maison.  C'est l'un des \emph{comparanda}
les plus riches à disposition, avec \ugarit{}, puisque les
connaissances sur les quartiers sont très réduites en Anatolie.
Les différences géographiques et culturelles rendent certes
l'établissement d'analogies problématique, mais on peut
souligner quelques points à prendre en considération dans l'évaluation
des vestiges de \bo. \Otto{} a montré à plusieurs reprises que
l'emprise au sol des habitations seule ne peut pas servir de critère pour
déterminer la richesse d'une habitation, puisqu'elle ne correspond pas
systématiquement à la richesse de
l'inventaire\footnote{\cite{Otto2006,Otto2012a,Otto2014a}.}. Pour la
\WTer{}, cette remarque s'applique en particulier au \gebQO{}, un
bâtiment à trois pièces mais à l'inventaire très riche. La répartition
des emprises au sol des maisons à \tellbazi{} ressemble étonnamment, en certains
points, aux résultats obtenus à \bo{}. À \tellbazi{}, lors
de la dernière phase, l'emprise au sol des maisons s'échelonne entre
\SI{40}{\square\meter} et \SI{240}{\square\meter}
(\cref{Img-Telbazi-EmpriseSol}).  Évidemment, l'environnement
géographique, les techniques de construction, l'organisation politique
à \tellbazi{} étaient complètement différents.  Cette comparaison
permet d'insister sur la conclusion émise pour \bo{} concernant le
dernier quart des bâtiments de plus de \SI{300}{\square\meter}, qui
sont à part. Dans son argumentation, \Otto{} interprète la moindre
différence entre les bâtiments comme un indice d'une société
\emph{well organized, but which had no strong horizontal
stratification}\footnote{\cite[46]{Otto2014a}.}. Pour la \WTer{}, il
est possible de suivre une ligne d'argumentation similaire et de
voir les différences entre la plupart des bâtiments comme le résultat
d'agrandissements ou de réductions de l'espace. Néanmoins, j'y
reconnaitrais une plus forte ségrégation et stratification
horizontale, qui ressemble plus à celle d'\ugarit{}. En effet, dans
cette ville, à la structure urbaine certes complètement différente,
l'emprise au sol des maisons de la \emph{Ville Sud} mesure entre une
\SI{38}{\square\meter} et \SI{290}{\square\meter} et celle de la
maison de \rapanu{}, scribe du roi, mesure quelque \num{300} mètres
carrés\footnote{\cite{Callot1994a} ; \cite[42--44]{Otto2014a} ;
\cite{Yon1987a,Yon1992a}.}. Ceci laisse supposer avec une assez grande
certitude que la taille de la maison reflète d'une certaine façon la
position sociale, d'autant plus lorsque l'emprise au sol peut-être
jusqu'à six fois supérieure.


\opt{optbw}{
 \begin{figure}[h!]
   \centering
   \begin{minipage}[]{1\linewidth}
   \centering
     \includegraphics[width=160mm]{Fig_60}

     \caption{Diagramme de l'emprise au sol  (en m²) des maisons de la \emph{Weststadt}
     de \tellbazi{} (phase 2), accompagné d'une boîte à moustaches. D'après les
     données de \cite{Otto2006} (voir également \cite[48 fig.
     6]{Otto2014a}) }

     \label{Img-Telbazi-EmpriseSol}
   \end{minipage}
 \end{figure}
 }


\Neve{} a jugé les vestiges de la \WTer{} comme un \emph{Nebeneinander und
Durcheinander verschiedenzeitlicher Mauern}\footnote{\cite[49]{Neve1978a}.}. Ces
conditions ont «~naturellement~» influencé l'importance donnée à la publication
des résultats. Parmi les autres fouilles menées sous son égide, auxquelles
il a consacré une monographie\footnote{\cite{Neve1982,Neve1999,Neve2001a} et une
monographie posthume à paraître.}, ce sont celles dont l'architecture
a été la
moins étudiée. La publication de résultats «~négatifs~», c'est-à-dire dont
l'interprétation est difficile et semble peu prometteuse voire contradictoire,
est souvent laissée de côté, créant des vides et des incompréhensions de la part
des autres scientifiques. \Mielke{} jugea : «~\emph{One of the main problems of
Hittite archaeology is that hardly any domestic quarters have been investigated
to date. Only in the Lower City of \bo{} has a quarter with densely built houses
and narrow lanes been excavated ; unfortunately, a detailed publication is still
unavailable}~»\footnote{\cite[173]{Mielke2011a}.}.  Quels que soient les résultats
des recherches, ils doivent être publiés.

À la vue des données de la \WTer{}, l'accent des recherches ne peut pas être
placé sur la reconstruction fonctionnelle de chaque maison, car l'inventaire est
bien trop lacunaire. En revanche, les modifications incessantes
auxquelles elles ont été soumises indiquent comment
l'espace était vécu.


\subsubsection{Quartiers et districts}


La reconstruction proposée est, de prime abord, étonnante, puisque jusqu'à
présent, la plupart des bâtiments étaient datés du \xiveme--\xiiieme{} siècle.
La remise en question de la chronologie n'est pas incompatible avec les grandes
lignes esquissées par \Neve{} et l'évolution des monuments (détaillée dans le
\cref{Chapter6} sur la ville basse). Surtout, comme il le souligne, les plans,
la taille et l'agencement des maisons sont le résultat de processus continus.

Tout d'abord, la nouvelle périodisation rejette l'idée de l'évolution entre le
plan à pièce centrale (\textit{Hofhaus}), le plan tripartite
(\textit{Hallenhaus}) et le plan en enfilade (\textit{Zweiraumhaus}). Surtout,
il n'existe aucune attestation pour parler d'une «~métamorphose~» de la population
de \textit{Ackerbürger} à citadin. Cette interprétation repose sur un préjugé de
l'évolution de la capitale, qui évoluerait d'une bourgade à une métropole. S'il
fallait choisir entre les deux, je pencherais plutôt pour considérer ces maisons
comme des témoignages d'une population urbaine, dont les activités (et ce dès \laperiodeCAC{})
n'était pas tournée vers l'agriculture. Le \cref{5-Chapter-GIS} va
permettre de cerner de plus près ce problème en explorant les activités
attestées à l'échelle de la \WTer{}.

L'évolution du quartier souligne que le système de circulation et le système
d'évacuation des eaux étaient étroitement associés et avaient été conçus dès le
début de l'implantation \delaph{}. Ces aménagements collectifs ont très peu
évolué, mais sont marqués par des réfections locales. Le changement le plus
profond s'est produit lors de la réorientation du canal \wterstr{}. Cette
transformation montre que ces installations devaient être gérées par
l'administration de la ville qui empêchaint ainsi tout enclavement.  Il faut
néanmoins nuancer ce propos, puisque, même si le tracé des rues n'est pas remis en
cause, l'espace qui leur est fourni est, lui, plus sensiblement sollicité. Pour
en témoigner, l'évolution des phases du \gebU{} illustre comment le plan a été
agrandi, restreignant la circulation dans la petite ruelle directement au sud.
L'emplacement du \gebQ{} montre  comment celui-ci empiète sur l'espace dédié à
la \tempelstr, le mettant particulièrement en valeur par la même occasion.

Les maisons, quant à elles, témoignent de réfections continues. Dans la
description, les plans des bâtiments ont été divisés en de multiples sous-phases
soulignant les changements les plus importants, ceux qui bouleversèrent
manifestement l'organisation de l'espace. De multiples détails montrent que des
changements mineurs ont également été menés pendant chaque phase. Là, les
bâtiments ont été agrandis, réduits ou modifiés au gré des besoins. De plus, il
n'y a pas de continuité entre les différentes phases de la ville,  à la
différence des espaces collectifs, restés très stables. Les
espaces privés ont été assez librement modifiés.

Si l'on reprend la définition de quartier, il semble tout à fait approprié de
l'appliquer à l'ensemble de la \WTer{} et d'en délimiter un deuxième pour le
\NoV{}. Associée à la voirie, cette organisation permettait de contrôler
l'accès aux rues tout en empêchant l'appropriation d'une zone par enclavement.
Les districts (ou îlots) sont à définir entre les rues et leur organisation
interne semble avoir été libre, évoluant selon une médiation entre les
\habitantEs{}, ce qui correspond bien à l'image de construction intensive.

L'idée que la \WTer{} n'a pas fait l'objet d'une planification est une idée à
rejeter. La répartition des rues et du système d'évacuation des eaux montre que
des districts principaux ont été définis et maintenus pour éviter tout
enclavement.  La qualité du système d'évacuation d'eau et son importance pour
l'ensemble de la \WTer{} montrent que ces aménagements collectifs n'ont pas pu
évoluer au simple gré des \habitantEs. De plus, ils se trouvent sous les
rues et ont dû être définis avant les districts.  Le pouvoir central, à travers
l'administration de la ville, a sans doute utilisé ces aménagements collectifs
pour rappeler son pouvoir d'autorité et affirmer sa supériorité. La \WTer{} et
le \NoV{} ont été divisés en quartiers et districts et seule l'organisation
interne des districts semble ne pas avoir été contrôlée.

