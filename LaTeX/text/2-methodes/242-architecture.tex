La création de cartes n'est pas un acte anodin, comme le prouvent les
cartes politiques où le tracer de frontières est souvent, en l'absence
de barrières géographiques évidentes, la seule « preuve » de la
démarcation\footnote{On peut citer l'exemple récent du rattachement de
la Crimée à la Russie par le service \emph{Google Maps}.}. Le choix
de figer sur une carte telle ou telle interprétation de l'histoire
ou bien le choix d'utiliser des couleurs chaudes ou froides pour
visualiser un concept, influent, le plus souvent de manière
inconsciente, sur le public. Lorsqu'une personne est exposée à une
carte toute faite, elle ne peut pas la comparer avec d'autres
exemples et devient plus aisément manipulable\footnote{Pour une
introduction générale, voir \cite{Dodge2011} et en particulier
\cite{Kitchin2011} sur l'influence des cartes.}. Si l'on regarde la
définition du mot «~carte~», elle est plus ou moins systématiquement
associée à une «~représentation conventionnelle de données concrètes
ou abstraites~». Une carte est donc toujours subjective, puisqu'elle
représente un niveau d'abstraction et donc d'interprétation.

Les cartes présentées dans ce travail sont le plus souvent un
assemblage de plans. Canoniquement, un plan est une
\citfr{représentation graphique d'un bâtiment selon une section
horizontale}\footnote{\cite[19]{Perouse1988}.}.  Dans le cas de
figure de l'archéologie, il s'agit tout d'abord d'un relevé de terrain
qu'il faut transformer en plan.

\subsubsection{Délimiter un bâtiment} \label{542-LecturePlans}


C'est un truisme que de rappeler qu'aucune analyse architecturale ne
peut être fondée si l'on ne limite pas \citsg{correctement} les
édifices que l'on décrit et interprète. Néanmoins, à ma connaissance,
il n'existe pas de réflexion sur les méthodes à employer pour définir
les limites d'un bâtiment. Dès que c'est possible, il faut se fonder
sur la circulation, les biens immobiliers et mobiliers. Lorsque
l'édifice est bien conservé cela pose moins de problèmes, mais, au
contraire, que faire lorsque le cas n'est pas évident, lorsque les
murs et les fondations ne sont que partiellement conservés ? Doit-on
en abandonner l'interprétation ? Les murs et fondations découverts
dans la ville basse de \bo{} sont enchevêtrés, abruptement
interrompus; ils témoignent de remaniements fréquents et l'attribution
à une construction voire à une période n'est pas sans poser
problème. Afin de contenir autant que possible la part de l'arbitraire
dans l'établissement des limites des bâtiments, j'ai mis en place un
protocole qui a été appliqué aux relevés pierre à pierre.

\subsubsection{Protocole}


\begin{enumerate}%[nolistsep]


  \item En tout premier lieu, on délimite des constructions. Une construction
    est définie comme un ensemble d'un seul tenant\footnote{Aurenche définit la
    construction ainsi : \citfr{La construction peut aller du simple mur au
    “groupe de murs” ou à “l'ensemble de murs” à quoi se réduisent souvent les
    restes architecturaux sur une fouille. Le mot ne préjuge pas la fonction.}
    \cite[61 \sv{} construction]{Aurenche1977}.}. Sur les plans, on peut les
    identifier grâce aux critères suivants :

    \begin{itemize}[noitemsep,nolistsep,topsep=0pt, partopsep=0pt]
      \item deux murs accolés appartiennent à deux
      constructions. La réciproque est fausse, c'est le cas du mur mitoyen;
      \item le même mur d'une construction, en retour d'équerre ou non,
    possède la même largeur;
      \item au sein d'une construction, les matériaux et leur mise en œuvre sont
        similaires;
      \item l'orientation des murs, s'ils sont par exemple parallèles ou
      perpendiculaires, permet de différencier des constructions.
  \end{itemize}

  \item Un bâtiment ou édifice (\all{Gebäude}) est formé par une ou plusieurs
    constructions organisées en un ensemble cohérent, unitaire et avec
    des pièces communicantes\footnote{\cite[34 \sv{} bâtiment et 78 \sv{}
    édifice]{Aurenche1977}; \cite[40 \sv{} bâtiment]{Perouse1988}.}:

    \begin{itemize}[noitemsep,nolistsep,topsep=0pt, partopsep=0pt]
      \item un bâtiment doit être accessible (accès à une rue);
      \item si le bâtiment est constitué de plusieurs constructions, celles-ci
      peuvent être ordonnées selon une chronologie relative (observation des
        joints);
      \item si une typologie existe, elle peut aider à définir un bâtiment.
		\end{itemize}

 \end{enumerate}







\subsubsection{Exemples}\label{ExempleLecturePlan}


Pour illustrer le protocole, ses avantages et ses limites, deux
exemples de son application seront explicités ici. Le \gebU{}\footnote{Afin
de créer une numérotation continue et éviter les confusions -- si
l'interprétation du bâtiment venait à changer, en passant, par
exemple de «~maison~» à «~entrepôt~» -- les bâtiments ont
été nommés \chantier{Gebäude}. Dans la mesure du possible, la
numérotation existante a été conservée. Dans ce cas, le \gebU{}
équivaut à la \chantier{Haus 7} de \Neve. Voir
\cref{420-Bat-BM-Br}.} fut rapidement dégagé en 1956 et on ne
possède que peu d'informations sur celui-ci (fig.
\ref{MethGeb7})\footnote{\cite[22]{Bittel1957c}.}. Le relevé pierre à
pierre de \Naumann{}, complété par \Neve{} (fig.  \ref{Geb7Mono}),
permet de diviser ce bâtiment en trois constructions (fig.
\ref{Geb7Poly}). Si l'on étudie les joints entre les différents murs,
on peut individualiser une première construction, sur laquelle prend
appui une deuxième construction, puis l'ensemble a été clos en
redoublant le mur nord-ouest pour y ajouter une troisième construction. \Neve{}
avait lui aussi proposé de diviser ce bâtiment en trois ensembles,
mais de manière légèrement différente (fig. \ref{Geb7Neve}) -- en
raison de la typologie ? D'après la photographie (fig.
\ref{Geb7Photo}), seule une ou deux assises de pierre sont conservées,
très près de la surface. On ne connaît pas de seuil, de sorte qu'il
n'est pas possible de reconstituer la circulation à l'intérieur du
bâtiment. Le protocole permet de mettre en valeur les trois
constructions et l'évolution du bâti.




Le deuxième exemple s'appuie sur un ensemble plus vaste de
constructions situées entre deux voies d'accès et dégagées en 1938
dans le \nordV{} (fig.
\ref{MethodoGeb58})\footnote{\cite[6--13]{Bittel1939a};
  \cite[99--103]{Bittel1952}.}. \Naumann{} identifie dans cet ensemble
sept maisons\footnote{\cite[8]{Bittel1939a}.}, mais la lecture du plan
montre qu'une telle interprétation n'est pas évidente. Le \gebTI{}
permet de saisir une évolution importante dans le groupe d'édifices
(fig. \ref{Geb58b}). De la construction 1, la plus ancienne du
\gebTI{}, ne reste plus qu'un mur et son retour. Celle-ci contraste
avec la construction 2, plus récente, qui est nettement mieux
conservée et qui témoigne d'un changement d'orientation, qui sera
suivi par la construction 3. Alors que \Naumann{} et \Bittel{}
divisaient ce bâtiment en trois maisons, avec à chaque fois 3--4
pièces, il me semble que les constructions indiquent plutôt
l'évolution d'un même édifice. Là encore, aucun seuil n'est conservé,
ne permettant pas de reconstruire la circulation du bâtiment. Il n'est
donc pas possible de vérifier s'il les pièces sont communicantes.
Néanmoins, si on laisse de côté l'interprétation fonctionnelle de ce
bâtiment, on remarque que la dernière construction du \gebTI{} a été
planifiée sans égard ni pour la première construction ni pour le
\gebTO{} (fig. \ref{Geb58c}). La pièce sud du \gebTO{} a été modifiée
et semble s'être accommodée d'un mur mitoyen, si elle n'a pas été
abandonnée comme pièce.  Les autres bâtiments n'indiquent pas de
mutations et sont restés relativement stables.  Ainsi, c'est une tout
autre image du quartier qui transparaît par rapport à celle plus
statique proposée par \Neve{} (fig.  \ref{Geb58d})\footnote{\cite[8
  fig. 3]{Neve1958}.}. Le \gebTI{} prend successivement de l'ampleur
et se réoriente en fonction de la rue (?) à l'ouest.  Là se
manifestent, dans la trame urbaine, des relations sociales et une
démonstration de domination (politique ou économique).



\subsubsection{Le choix des couleurs} \label{543-Couleurs}

Pour représenter les différentes phases d'un bâtiment, trois couleurs
ont été sélectionnées à l'aide du service \emph{Color
Brewer}\footnote{\cite{Brewer2014, Harrower2003}.}. Le set
\emph{Accent} a été choisi, car il représente de manière optimale des
variables qualitatives qui se laissent facilement identifier et dont
la classification devrait encore être lisible malgré une photocopie en
noir et blanc\footnote{Les trois couleurs retenues (accompagnées de
leur triplet hexadécimal RGB entre parenthèses) sont, cyan (\#8dd3c7),
jaune pâle (\#ffffb3) et ardoise (\#bebada).}. En outre, si une partie
d'un mur n'est pas attestée, mais restituée, alors elle n'est pas
représentée avec un aplat en couleur, mais par des hachures croisées.
Si la partie manquante était trop importante, plutôt que d'inventer un
plan complet, seul un petit tronçon est restitué à la suite de la
partie attestée pour indiquer dans quelle direction le mur devait se
poursuivre.
