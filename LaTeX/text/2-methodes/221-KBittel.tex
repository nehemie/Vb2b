Il n'existe pas de prise de position de \Bittel{} sur sa technique ou
son mode de documentation.  J'ai donc essayé, à l'aide de ses écrits
autobiographiques et de notices biographiques, de retrouver les idées
directrices du projet scientifique de \Bittel{}\footnote{Pour des
notices biographiques, on peut se référer à \cite{Boehmer1993}.
Parmi les fouilles auxquelles \Bittel{} a participé avant de
travailler à \bo{} (liste dans \cite[10\psq]{Bittel2007}) et qu'il
considère comme décisives dans sa formation de terrain, il mentionne
les fouilles en Égypte sous la direction de \Noldeke{} et les
fouilles d'\alisar{} sous la direction de \vonderOsten{} (\cite[62.
66]{Bittel2007}).}.  En arrivant à \bo{} en 1931, \Bittel{} désirait
établir une chronologie pour comprendre et dater la culture hittite:
«~\emph{Was noch fehlte, war eine genaue Feststellung der zeitlichen
Abfolge der verschiedenen Gruppen von
Keramik}~»\footnote{\cite[2]{Bittel1932}.  Il continue ainsi
«~\emph{Denn die Beobachtung einer möglichst großen Anzahl von
Funden in ungestörter Lagerung und in Verbindung mit Bauten gerade
in der urkundlichen gesicherten Hauptstadt des Hethiterreichs ist
ein dringendes Erfordernis der Altertumsforschung, weil man
erwarten kann, von diesem Kernpunkt der hethitischen Kultur aus
für die Ordnung der gesamten anatolischen Altertümer eine feste
Grundlage zu gewinnen}~».}.  Ce sera tout d'abord \bk{} qui sera
exploré, \Bittel{} étant persuadé que c'était le lieu le plus propice
pour obtenir une séquence
stratigraphique\footnote{\cite[6--8]{Bittel1935} ;
\cite[202--204]{Bittel1998}.  \Bittel{} n'emploie jamais le terme de
stratigraphie, mais parle d'une suite de niveaux et de phases.}.
Dans sa démarche scientifique, \Bittel{} s'est fixé deux exigences :
donner la possibilité de contrôler les résultats grâce à
l'établissement de plans et de coupes
stratigraphiques\footnote{\cite[8]{Bittel1935}.} et accorder une
attention particulière aux contextes des découvertes en discernant
celles associées à une architecture de celles provenant de
déblais\footnote{\cite[6]{Bittel1935} : «~\citde{Bei der Beobachtung
und Wertung der Kleinfunde hat man also stets zu unterscheiden, ob
sie aus Schutt- oder aus richtigen Wohnschichten stammen. Die
bloße Angabe der Tiefe, in der ein Fund gemacht wurde, ist deshalb
meistens wertlos}~».}.  Pour assurer l'enregistrement des
fouilles, un carroyage fut établi pour tout le site de \bo{} en
1931--1933. Il se compose de carreaux de \SI{100}{\meter} codés par
une lettre majuscule (longitude) et un nombre (latitude)\footnote{Un
plan où apparaît le carroyage de \SI{100}{\meter} fut
publié pour la première fois en 1935 dans \cite[Pl. 1]{Bittel1935}. Ce
carroyage n'a pu être intégré en 1994--1997 lors de l'établissement
d'un nouveau plan topographique de la ville, en raison des
divergences entre les relevés topographiques (\cite[339\psq{} et
note 36]{Seeher1999b}\nocite{Seeher1999}).}. Ce carroyage était
subdivisé selon le même principe en carreaux de \SI{10}{\meter},
chacun affecté d'une lettre minuscule et d'un nombre\footnote{Le
premier carroyage de \bo{} a été établi pour \bk{} avec un maillage
de \SI{10}{\meter} par l'architecte \SteinC{} en 1933
(\cite[3]{Bittel1933}).}.

L'exploration archéologique reposait sur le concept de
\emph{Bauschicht} (phase architecturale) et de \emph{Bauphase} (état
architectural)\footnote{ L'utilisation du terme \emph{Baustadium} à la
place de \emph{Bauphase} serait aujourd'hui plus conforme à la
tradition allemande : \cite[18--23]{Echt1984};
\cite[3--11]{Eichmann1989}. Les termes allemands ont été adaptés en
français de manière à suivre les définitions de \Thalmann{}, mais
les dénotations, appliquées de manière rétroactive, ne sauraient être
aussi strictes que celles qu'il propose
(\cite[10--14]{Thalmann2006}).}.  Les fouilleurs cherchaient à
attribuer les artefacts à des niveaux d'architecture, à les
subdiviser le cas échéant en phases et à y associer les artefacts
mobiles.  Seul \Neve{} a explicité cette démarche -- mais sa position
devait être partagée par \Bittel{}\footnote{Une définition d'une phase
architecturale selon Bittel peut se lire en filigrane dans la
description des résultats de la campagne de fouilles de 1956 dans le
\nordV{}. Les phases architecturales se différencient par
l'altitude, les changements d'orientation et de fonction dans la
succession de constructions : «~\citde{[D]rei weitere [...] Besiedlungsschichten
(4, 2, 1), alle nicht nur ihrer Tiefenlage nach klar voneinander
geschieden, sondern ebenso durch die wechselnde Orientierung ihrer
Bauten und durch deren veränderte Bestimmung}~»
(\cite[8]{Bittel1957}).}. D'après Neve, une phase architecturale en
tant que témoin d'une période historique est caractérisée par une
unité de construction et elle est délimitée dans le temps ; une phase
architecturale (\textit{Bauschicht}) peut se décliner en états
(\textit{Bauphasen}) qui représentent des modifications mineures au
sein de la phase
architecturale\footnote{\label{Anm.Neve.phases}\cite[4\psq]{Neve1982}.
Pour une réflexion sur le concept de phase architecturale voir
\cite[3--11]{Eichmann1989}.}.

Sur le plan pratique\footnote{Il n'existe que de très rares
indications sur le déroulement technique des fouilles dans les
mémoires de \Bittel. Pour les campagnes de 1931, 1932 et 1933, voir
respectivement \cite[208--212. 282--288. 382--411]{Bittel1998}. },
un passage provenant du rapport préliminaire de la campagne de 1936
explicite la mise en œuvre de la  méthode, lorsque \Bittel{} entreprit
de fouiller la phase «~sous-jacente~» du bâtiment A de \bk{} :
«~\citde{Dieser Aufgabe hat sich mit 5 bis 8 Arbeitern -- eine größere
Zahl verbot sich aus Gründen der Beobachtungen -- W. Dehn gewidmet.
Es versteht sich, daß wir auch hier wie stets jeden, auch den
kleinsten Fund berücksichtigt haben, so daß wir am Ende der Kampagne
aus diesen Schichten, die in 20-cm-Abstichen abgetragen wurden,
u.~a. viele Tausende von Gefäßscherben zur Verfügung hatten, deren
genauere Sichtung und Zusammensetzung freilich nur zum
allergeringsten Teil während der Kampagne selbst vorgenommen werden
konnte}~»\footnote{\label{Bittel-Schicht}\cite[4]{Bittel1937a}.}.
Grâce à la méthode mise en place par Bittel et qui fut adoptée par les
archéologues et architectes travaillant à \bo{}, les fouilles furent
toujours documentées par des plans et des coupes  stratigraphiques.

À chaque objet archéologique remarquable -- c'est-à-dire tout ce qui
ne correspond pas à des fragments de céramiques non-jointifs -- était
attribué un numéro se composant de l'année de découverte et d'un
numéro de série, comme par exemple «~Bo-a-119~» pour le
119\textsuperscript{e} objet inventorié en 1931\footnote{\cite[226 :
\catalognum{} 2365]{Boehmer1972}. L'année de la campagne a tout
d'abord été codifiée sous la forme de lettre, puis des deux derniers
chiffres de l'année. Pour une correspondance entre lettre et
campagne archéologique, voir \cref{Tab.LettreAnnee} p.
\pageref{Tab.LettreAnnee}. Originellement, le numéro de série est
écrit sous la forme 119/a. J'ai jugé préférable de remplacer la
barre oblique (\textit{slash}) par un trait d'union, d'inverser
l'ordre entre lettre et numéro de série, puis d'ajouter «~Bo~»
pour unifier les numéros avec ceux des fouilles plus récentes . On
passe ainsi de {119/a} à Bo-a-119, puis \kfd{Bo31}{119}.}.  Chaque
objet inventorié était localisé grâce au carroyage puis, le cas
échéant, on lui attribuait une phase architecturale\footnote{Par
exemple «~Bo-a-119, BK, w/8, Schicht III~», pour le
119\textsuperscript{e} objet inventorié en 1931, découvert dans la
«~couche III~» du carreau w/8 de \bk{}. Cf. \cite[226 :
\catalognum{} 2365]{Boehmer1972}.  Pour la correspondance entre le
chantier (\bk) et le sigle (BK) voir \cref{Tab.LettreAnnee}, p.
\pageref{Tab.LettreAnnee}.}.

En comparaison avec d'autres fouilles contemporaines, il semble que la
méthode de \Bittel{} soit tout à fait exemplaire : dès le départ, il a
mis en place un système permettant d'enregistrer efficacement du
matériel et il a pris soin de relever des coupes stratigraphiques qui
permettent de contrôler les résultats.  Les coupes montrent que les
couches stratigraphiques ont été distinguées selon la nature du
sédiment\footnote{Il faut souligner la qualité des coupes
stratigraphiques publiées pour la première fois en 1935 (\Bittel{}
est alors âgé de 28 ans) à une échelle de 1/100 (\cite[pl.
1--3]{Bittel1935}). On peut les comparer à celles de la fouilles
d'\alisar{} (1930--1932), qui ne représentent que l'architecture (en
niveaux horizontaux) et n'incluent pas les différents sédiments
(\cite[pl. 19--23]{Osten1937b}). Comment se positionne la méthode
mise en place par \Bittel{} dans l'histoire de l'archéologie ?  À
défaut de disposer d'un ouvrage sur l'évolution des méthodes
archéologiques, en particulier, ici, de «~l'école allemande~»,
rappelons que les grandes publications sur les méthodes
archéologiques sont plus tardives (par ex.  \cite{Wheeler1954a}).
Pour un aperçu des concepts stratigraphiques dans la première moitié
du \xxeme{} siècle au Levant, voir \cite[24--39]{Echt1984}.}. De
plus, il a toujours pris soin de séparer ce qui provenait, selon lui,
des déblais et ce qui correspondait à du matériel associé à des restes
architecturaux\footnote{\cite[4]{Bittel1937a}. }.  Cependant, il est
difficile de juger comment le concept de découvertes «~\insitu{}~»
a été appliqué : rien n'indique comment le matériel était attribué à
une phase architecturale. De nombreuses études ont montré que
l'association de matériel à une architecture ne peut se faire sans
réflexion sur la formation de la
stratigraphie\footnote{\cite{Schiffer1996, LaMotta1999} ou, plus
spécifique à \bo{}, \cite[216--218]{Schoop2006a}.} et la lecture des
rapports donne l'impression que la stratigraphie était connue à
l'avance : sous une phase architecturale commence la phase plus
ancienne\footnote{\supra{} \label{InSituBittel} note
\ref{Bittel-Schicht}, p. \pageref{Bittel-Schicht}; cf.
\cite[6]{Warburton2003}.}. Mais est-ce une hypothèse
systématiquement valable pour la totalité des objets découverts dans
la \citfr{phase} ? Si les artefacts ne proviennent pas d'un niveau de
sol, ce qui est impossible à vérifier, les datations doivent  être
considérées avec la plus grande précaution.
