Le numérique transforme les pratiques scientifiques et éditoriales
dans les sciences humaines, jusqu'à présent principalement liées à
l'écrit et au livre.  La «~dé-livrance~»\footnote{Sur ce concept, voir
  l'article introductoire de \cite{Vinck2014} à un dossier sur le
  rapport entre la transmission du savoir et son support, en
  particulier le livre.} et l'abandon de la pagination remettent en
question les voies de transmission du savoir scientifique et offrent
de nouvelles possibilités de mises en forme, de mixages et
d'expérimentations de divers processus récursifs\footnote{Je renvoie à
  la définition de \emph{public récursif} donnée par \Kelty{}
  (\citeyear[4, 28--30]{Kelty2008a}) ; voir également
  \cite{Kelty2005a,Kelty2005b}.}. Un débat très actif, mené tout aussi
bien par des scientifiques que des libraires, existe sur les avantages
et désavantages que ces nouvelles technologies fournissent aux
scientifiques\footnote{Pour une introduction, voir \cite{Sonke2014a}.}.
Ne pas viser uniquement à la rédaction de livres pour bibliothèques
permet de tirer profit des possibilités d'édition du \emph{Web 2.0}.
Utiliser Internet comme support passe au-delà de la publication de
CD-ROM pour rendre les documents et les données dynamiques. Par
exemple, dans leur étude sur la diffusion de la domestication, Kansa
\textit{et alii} ont montré comment l'adoption de documents et de
données dynamiques ont servi à faire un pas décisif dans la
compréhension de ce phénomène, mais aussi dans la discussion entres
scientifiques pour comparer leurs données à l'échelle de plusieurs
milliers de kilomètres\footnote{\cite{Kansa2014,Arbuckle2014c}.}.
L'emploi de nouveaux supports de communication ne signifie nullement
l'éradication de ceux en place, mais la coexistence de différents
modèles.

Un document dynamique, à la différence d'un texte statique, intègre des outils
étendus de collaboration et de réutilisations du texte et des
données\footnote{\cite{Heller2014a}.}. En s'inspirant des méthodes de
coopération développées pour et par les développeurs de programmes informatiques
(\cref{DynamicPublicationFormats}), quatre techniques appliquées aux travaux
scientifiques se démarquent\footnote{Il s'agit de termes traduits de l'anglais
pour lesquels l'usage n'est pas encore strictement établi.} :

\opt{optbw}{
 \begin{figure}[h!]
 \centering
 \fbox{\includegraphics[width=130mm]{Fig_2}}
  \caption{Modèle de document dynamique, dessiné d'après \cite[202, fig. 6]{Heller2014a}}
    \label{DynamicPublicationFormats}
  \end{figure}
}

\begin{description}
  \item[- la transclusion :] inclusion sur référence d'un autre document avec
                          mise à jour automatique ;
  \item[- le branchage (\emph{forking}) :] version clonée sur une version
                         initiale et qui constitue un rameau indépendant,
                         c'est-à-dire qu'elle peut évoluer indépendament ;
  \item[- les suggestions d'amélioration (\emph{pull requests})] : suggestion
                         d'une modification d'une branche par un collaborateur ;
  \item[- le fusionnement (\emph{merge}):] fusionnement de deux branches.
\end{description}


Derrière ces anglicismes se cachent des principes de collaboration qui sont déjà
utilisés par tous les scientifiques à l'étape de pré-publication. Lorsqu'une
première version d'un travail est finie, elle est soumise à des collègues ou
à des comités de lecture pour corrections et avis (branchages). L'auteur reçoit des
suggestions d'amélioration (\emph{pull request}), qu'il peut accepter
(fusionner) ou refuser au cas par cas. Ces outils, même s'ils sont couramment
employés à l'étape de pré-publication et dans un cadre privé, peuvent devenir
publiques et employés aussi pour la période
post-publication\footnote{\cite{Kansa2014}.}. Au-delà de la mise en place de
systèmes de gestion de versions, les documents dynamiques ont avant tout
l'avantage de pouvoir intégrer des données pour qu'elles soient liées au texte
et faciliter une évolution constante du texte.
