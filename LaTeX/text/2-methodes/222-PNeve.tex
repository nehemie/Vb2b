Suite à la reprise des fouilles en 1954, \Bittel{} travailla avec une
équipe d'architectes et d'archéologues qui avaient pour mission de
prendre en charge le chantier de fouilles, puis la publication du
matériel\footnote{Pour l'exemple, on peut citer le travail à \nwhmp{}
où \Schirmer{} dirigea le chantier puis publia les fouilles et
\Orthmann{} la céramique, cf. \cite{Schirmer1969}.}. À partir de
1963, \Neve{}\footnote{Pour un aperçu de son travail,
\cite{Seeher1995, Seeher2014a}.} dirigea seul les fouilles de la
ville basse et adopta sa propre méthode.  Tout comme les choix
de \Bittel{}, ceux de \Neve{} sont décisifs dans la formation de la
documentation à notre disposition pour les années 1970--1977.

\label{Neve-Carroyage}\Neve{} appliqua le principe de phases et
d'états architecturaux instauré par
\Bittel\footnote{\cite[4\psq]{Neve1982}. -- Cf. note
\ref{Anm.Neve.phases}, p. \pageref{Anm.Neve.phases}.}. Cependant, au
lieu de se conformer à un carroyage de \SI{10}{\meter}, il n'a
conservé que le carroyage de \SI{100}{\meter} et a choisi de mettre en
place des tranchées parallèles, numérotées par des chiffres romains,
subdivisées en carré par des chiffres arabes\footnote{Pour un des
premiers exemples de sa méthode, voir \cite[plan 8]{Bittel1969}.}.
Il n'existe pas de principe sur la longueur ou l'orientation d'une
tranchée -- si ce n'est l'aspect pragmatique d'une orientation
basée sur les constructions visibles. Pour les fouilles à l'ouest du
\tempelI{}, les tranchées sont orientées NNO--SSE ; une orientation
quasiment perpendiculaire à la terrasse du \tempelI{} et à
l'\abschnittsmauerfr{}. Ce système a été logiquement utilisé dans
l'enregistrement du matériel et, par conséquent, dans les
publications. Dans celles-ci, on retrouve des indications d'origine
telles que par exemple \emph{\kfd{Bo76}{20}, Ust, J/20,
I/2}\footnote{\cite[35 \catalognum{} 43]{Boehmer1983}.}, qui étaient
condamner à rester inexploitables faute de plan de repérage\footnote{Le dernier
plan où les tranchées sont répertoriées date de 1971, cf. \cite[25
fig. 12]{Neve1975a}; \cite[34 fig. 20]{Neve1975b}. Par la suite, les
plans ne récapitulent que l'extension de la fouille.}. Où se trouve
le carré 2 de la I\textsuperscript{[ère]} tranchée de 1976 de la ville
basse (Ust, J/20), à l'intérieur duquel l'objet \kfd{Bo76}{20} a été
découvert ? L'utilisation d'une méthode qui n'est pas systématique ne
permet pas a priori de retrouver l'emplacement des fouilles. Ainsi,
seule l'étude de la documentation permet de reconstituer -- à peu près
-- leur position (\infra{}
\cref{54-Limites-SIG})\footnote{L'emplacement des différentes
tranchées a été retrouvé grâce aux relevés de terrain, aux
photographies et aux carnets de fouilles. Voir \infra{}, en particulier,
note \ref{534-Anm-LegendeFoto}, p. \pageref{534-Anm-LegendeFoto}.}.

\label{Neve Profil}À la différence de \Bittel{}, \Neve{} a appliqué un
concept de couche stratigraphique qu'il n'a jamais
explicité\footnote{Pour une définition, \cite[12]{Thalmann2006}.}.
\Neve{} utilise dans ses notes un vocabulaire redondant pour les
qualifier et leur a attribué des numéros, qui ont été également donnés
au matériel céramique\footnote{Les termes les plus récurrents sont :
  \emph{Füllschicht}, \emph{Çorakhaltige Erde}, \emph{Brandschutt},
  \emph{Steingeröll} ou \emph{Fußboden}.}. Cependant, cette méthode
n'a pas été appliquée de manière systématique pour toutes les couches
découvertes lors de la fouille : seule une partie de celles-ci ont été
enregistrées\footnote{Il n'existe aucune information sur les
 couches qui n'ont pas de numéro.}, parmi celles qui
semblaient les plus intéressantes. Cette inconsistance nuit à
l'analyse de la ville basse et des couches stratigraphiques, car il
n'existe que de très rares relevés de ces dernières\footnote{Par comparaison, seuls 7 profils ont été
  publiés pour les fouilles 1978--1987 de la ville haute, cf. \cite[49
  fig.  26 \Beilage{} 26. 41b. 46. 51. 52a. 52b]{Neve1999}.}. Dans les
archives qui documentent la ville basse, on retrouve ici ou là des
croquis de profils, mais ils n'ont rien de comparable à un relevé
stratigraphique. Même les coupes qui semblaient documenter de vrais
relevés ne sont que des mises au propre de croquis réalisés à la hâte
(fig. \ref{CoupeStratNeve}).

\opt{optbw}{
  \begin{figure}
    \centering %
      \fbox{
        \begin{subfigure}[b]{0.52\textwidth}
          \centering %
          \includegraphics[width=\textwidth]{Fig_5a}
          \caption{Coupe publiée dans \cite[64 fig. 2]{Neve1984}}
          \end{subfigure}%
           ~
          \begin{subfigure}[b]{0.42\textwidth}
          \centering %
          \includegraphics[width=\textwidth]{Fig_5b}
          \caption{Relevé de terrain sur feuille vierge}
          \end{subfigure}
         }
        \caption{Coupe stratigraphique du \gebTQ.} \label{CoupeStratNeve}
    \end{figure}
  }


Ces déficiences entravent l'analyse des fouilles de la ville basse
menées par \Neve{} dans les années 1970--1977. Néanmoins, il faut
considérer que l'introduction de couches stratigraphiques et leur
classification permettent de mieux définir le matériel en association
avec l'architecture, à la différence de la méthode de \Bittel{}.
Hélas, on ne peut que se fier aux affirmations de \Neve{} et il
n'existe aucun moyen de les contrôler. S'il est de peu d'intérêt de
mettre en doute les couches qualifiées de perturbées, il en va
autrement du concept de sol (\textit{Fußboden}), qui possède un emploi
très large au Proche-Orient\footnote{\cite[13]{Warburton2003}.}. C'est
uniquement le matériel qui repose sur un niveau de sol qui doit servir
à dater la construction associée, formant avec lui un ensemble plus ou
moins contemporain. Le qualificatif de sol, selon la définition de
\Neve, semble avoir été utilisé pour les couches qu'il considérait
comme fermées. Elles semblent, avec toute la précaution nécessaire,
exploitables pour associer du matériel à l'architecture\footnote{Sur
le concept de couches fermées et le problème du matériel résiduel,
voir \cite[92]{Thalmann2006}.}.

De cette analyse des méthodes de fouilles à \bogazkoy, on peut tirer
les conclusions suivantes. Même si les techniques de \Bittel{} et de
\Neve{} sur le terrain étaient tout à fait en accord avec leur époque,
l'absence de l'enregistrement systématique des strates ne permet pas de
reprendre toute la stratigraphie et il est tout simplement impossible
de construire un diagramme de
Harris\footnote{\cite{Harris1979principles}.}. Seuls les objets qui
sont attribués aux sols peuvent servir à la datation et l'absence de
relevés stratigraphiques impose de recomposer une chronologie fondée
sur des phases architecturales.
