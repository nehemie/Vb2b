\opt{optbw}{
 \begin{figure}[h!]
   \centering
   \begin{minipage}[]{\linewidth}
   \centering
   \fbox{\includegraphics[width=150mm]{Fig_3}}
     \caption{Research Pipeline, d'après une idée de R. Peng}
     \label{ResearchPipeline}
   \end{minipage}
 \end{figure}
}

Il est inhabituel de publier l'ensemble des données d'une fouille
archéologique. Le plus souvent, seuls quelques éléments font partie
intégrante de la publication : un choix de certaines photographies,
des plans, des listes d'objets et leurs dessins, listes, tableaux,
quelques graphiques, du texte et une bibliographie. Cette tradition
n'a quasi pas évolué en un siècle et il est temps de la remettre en
question, car il n'y pas de raison non plus de ne pas étendre les
données à « publier » à tout ce qui est disponible et possible.  Il
existe en général une grande différence entre les informations que les
auteur.e.s d'une étude ont à disposition et celles qui sont mises à
disposition.  Seul l'établissement d'un protocole explicite et connu
de tous, y compris comment la documentation a été créée, permet
d'assurer la scientificité d'une fouille. Ce ne sont pas les qualités
intrinsèques des scientifiques qui rendent les connaissances robustes,
mais l'exercice du jugement des pairs. «~\emph{We often forget that
scientific knowledge is reliable not because scientists are more
clever, objective or honest than other people, but because their
claims are exposed to criticism and
replication.}~»\footnote{\cite[149]{Fanelli2013a}.} Il est du devoir
de l'archéologue d'organiser ses archives d'une telle manière qu'elles
puissent être consultées en cas de besoin\footnote{Ma position diffère
totalement de celle de \Demoule{}, qui me parait injustifiable :
\emph{De fait, à l'état brut, cette documentation [issue des
fouilles] est souvent inutilisable pour tout autre que le
fouilleur} (\cite[244]{Demoule2005a}).}.


\AuteurEs{} et publics partent de deux positions opposées
(\cref{ResearchPipeline}).  L'instigateur ou l'instigatrice dispose
des données et à l'aide de ses idées mène une analyse et produit un
texte les articulant. Le texte aide l'audience à remonter le fil des
recherches et à mieux comprendre la pensée de l'auteur\E, tout comme
les références bibliographiques renvoient aux ouvrages consultés. Les
figures, les tableaux et les textes sont les portes d'entrées de
l'audience pour comprendre rapidement comment se sont déroulées les
fouilles. Le texte et sa hiérarchisation permettent d'accéder aux
informations essentielles et aux résultats de la fouille. Néanmoins,
revenir aux observations est souvent impossible.

Depuis quelques années, de nombreuses voix critiquent l'absence de
publication des données et l'application de méthodes empêchant la
science d'être
«~reproductible~»\footnote{\cite{Ioannidis2014a,Peng2011a}.}. Ce débat
est très intense dans les sciences
exactes\footnote{\cite{EditorialNature2013}.} et pourrait sembler
étranger à l'archéologie, où, dans tous les cas de figure, la fouille
ne peut pas être reproduite. Néanmoins, l'archive archéologique permet
de recommencer une analyse, ce qui rend la recherche tout aussi
reproductible et permet d'aboutir à des résultats alternatifs.

Les technologies actuelles ont le potentiel, depuis déjà plusieurs
années, d'assurer la mise à disposition de l'ensemble des données.
Seule cette mise à disposition permet aux autres scientifiques
d'évaluer les conclusions
d'un travail.  Si toutes les données sont à disposition alors le
travail peut-être être digéré, réemployé et remixé. En
revanche, ne pas fournir toute la documentation ni les données à
disposition sous une forme \textit{directement réutilisable} par
d'autres personnes, tant juridiquement que matériellement, pose des
problèmes scientifiques et éthiques ; dans quelques années, un rapport
incomplet pourrait bien être considéré comme une fraude
scientifique\footnote{\cite{Fanelli2013a}.}. En archéologie, chaque
archéologue connaît la frustration face à des affirmations qui ne
peuvent pas être réinterprétées ou difficilement vérifiées.
