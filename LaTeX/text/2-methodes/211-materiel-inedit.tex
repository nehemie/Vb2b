L'archéologie est une science jeune dont les méthodes et les objectifs
ne cessent d'évoluer. Ces dernières décennies, l'emprunt de méthodes
issues des sciences physiques et chimiques et adaptées à l'archéologie
ont défini de nouveaux champs d'étude, élargissant l'éventail
des analyses à toutes les échelles, des paysages aux
molécules\footnote{Pour une courte rétrospective sur ces nouvelles
méthodes, voir par exemple \cite[17--19]{Kristiansen2014}.}. De
nouveaux instruments de mesure étendent considérablement le domaine de
l'observation, permettant, par exemple, d'enregistrer aisément les
fouilles en trois dimensions grâce aux stations totales, d'explorer de
nouveaux champs de recherche grâce aux images
satellites\footnote{\cite{Barge2011,Comer2013}.}, aux
scanners-imprimantes tridimensionnels ou à la réalité
augmentée\footnote{Voir les actes du congrès annuels \textit{Computer
Applications and Quantitative Methods in Archaeology} pour avoir un
aperçu des derniers développement et dont les derniers publiés sont
\cite{Earl2013a, Earl2013b}.}. Le volume de données collecté par les
projets archéologiques est gigantesque et croît chaque année.
Conjointement à cette évolution technique, la révolution numérique et
le passage au \emph{Web 2.0} bouleversent les modes de travail des
archéologues\footnote{Voir \cite[1--6]{Kansa2011b}, sur le terme \emph{Web
2.0} et son rapport à l'archéologie.}. Théoriquement, il n'existe plus
de limites à l'enregistrement des données et les nouveaux outils tout
comme les techniques de pointe à disposition promettent de simplifier
l'enregistrement, la gestion, la redistribution, la consultation et
l'archivage des données.

Concrètement, il est légitime de se demander comment ce nouveau
paradigme conditionne la mise à disposition des données. Depuis déjà
plus de deux décennies, des archives numériques dédiées à
l'archéologie existent pour sauvegarder et mettre à disposition les
données archéologiques\footnote{L'\textit{Archaeology Data Service}
(\href{http://archaeologydataservice.ac.uk/}{<http://archaeologydataservice.ac.uk>}),
établi en 1996, est le plus ancien, mais on retrouve également depuis
5--6 ans \textit{Open Context}
(\href{http://opencontext.org/}{<http://opencontext.org>}),
\textit{The Digital Archaeological Record}
(\href{http://www.tdar.org}{<http://www.tdar.org>}) et bientôt
\textit{IANUS}
(\href{http://www.ianus-fdz.de/}{<http://www.ianus-fdz.de>}).
\Consultes{}.}.  Ces dépôts profitent de l'avantage fondamental des
données numériques par rapport aux publications uniquement papier. La
réplication de données est illimitée, dans n'importe quel volume, sans
perte de qualité et à coût presque nul, devenant des bien
non-rivaux\footnote{Un bien non-rival est un bien dont l'usage ne
diminue pas la disponibilité pour autrui. Voir
\cite[19--23]{Lessig2001a}.}. Pour comparaison, un livre d'archéologie
est imprimé dans le meilleur des cas à un millier d'exemplaires (le
plus souvent, quelques centaines), est limité dans la taille, le
nombre, les couleurs des photographies ou l'échelle des
plans\footnote{\cite{Demoule2005a}.}.

Dans le domaine anatolien de l'\ageBronze{}, rares sont les
expériences qui s'éloignent des voies traditionnelles de publication
ou même mettent les jeux de données à disposition en ligne. Au mieux,
les données numériques sont distribuées grâce à un CD-ROM accompagnant
le livre\footnote{Voir \cite{Frangipane2007} pour consulter un des
  rares exemples.}, même si cette pratique révèle plus de l'exception
que de la règle. Souvent, les jeux de données ne sont pas mis à
disposition dans un format qui permet de les réutiliser. Pour \bo{},
jusqu'à présent, peu de données uniquement numériques ont été mises à
disposition, en dehors du site web, qui, dans son état actuel, est
plutôt un guide
dématérialisé\footnote{\href{http://www.hattuscha.de}{<http://www.hattuscha.de>}
  \consulte{}.}. À l'échelle de l'archéologie du Proche-Orient,
l'article d'Anastasio et de Saliola donne un bon aperçu des ressources
à disposition sur Internet\footnote{\cite{Anastasio2014}.}, même si
le domaine anatolien est presque systématiquement omis de la synthèse.
Giusfredi mentionne quelques références pour
l'hittitologie\footnote{\cite{Giusfredi2014}.}, mais sans signaler des
projets innovant comme le \emph{Türkiye Arkeolojik Yerleşmeleri
  Projesi}\footnote{
  \href{http://www.tayproject.org}{<http://www.tayproject.org>} \url{}
  \consulte{}.} ou des sites de fouilles archéologiques dont toutes
les données sont systématiquement enregistrées grâce à des bases de
données mises à disposition en ligne
%: \catalhoyuk{}\footnote{\url{http://www.catalhoyuk.com}.} et
comme
\oymaagac{}\footnote{\href{http://www.nerik.de}{<http://www.nerik.de>}
  \consulte{}.}.

Cette esquisse révèle que les projets qui s'écartent de l'unique objectif de la
publication monographique sont de rares exceptions, alors que c'est le seul
moyen de rendre l'archéologie numérique et de dépasser le stade actuel où, dans
le meilleur des cas, il est possible d'afficher un livre sur un écran
grâce au format « pdf ».
