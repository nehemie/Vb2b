\chapter{Stratigraphie und Chronologie}

Die absolute Datierung der Befunde und Artefakte nach archäologischen
Kriterien war lange eine der Schwachstellen der Ausgrabungen in
Boğazköy. Erst seit 1994, als Jürgen Seeher die Grabungsleitung
übernahm, wurden Radiokarbon-Datierungen systematisch angewendet. Bis
dahin  stützte sich die Datierung hauptsächliche auf die Textfunde, um
absolute Daten für die verschiedenen Schichten zu gewinnen − unter der
Prämisse, dass die ›bedeutendsten‹ Ereignisse in den erhaltenen Texten
nachvollziehbar sein müssten. Wie verschiedene Forscher gezeigt haben
(u.~a. Klinger, Müller-Karpe, Schoop, Seeher), hat diese Methode, die
ich als ›epigraphische Stratigraphie‹ bezeichne, zu erheblichen
Fehleinschätzungen geführt.

Die zwei letzten Dekaden haben vor allem am Beispiel der Oberstadt
gezeigt, wie groß das Problem ist. Neve dachte, dass die Bebauung
dieses Stadtteils ausschließlich in das \xiiijh{} zu datieren sei.
Neuere Forschungen zeigen jedoch, dass diese bereits im \xvijh{}
entstanden ist, und dass die Chronologie ihrer Entwicklung komplett
revidiert werden muss.  Dies betrifft die Befunde ebenso wie die
Funde. Da die Keramikformen der \heteng{} (d.~h.{} zwischen dem
15. und frühen \xiiijh{}) typologisch kaum Veränderungen aufweist, wurde
diese Fehlbeurteilung nicht bemerkt. Daher sind auch die Vorschläge
absoluter Datierungen der Befunde in der \unterstadt{} von Boğazköy
unsicher, zumal es schwer ist, zwischen sicheren und unsicheren
Kontexten zu unterscheiden. Dieses Problem ist für die \karumzeit{}
weniger gravierend, da die meisten Funde aus Brandschuttschichten
stammen.  Prinzipiell hätten andere Ausgrabungen in der Region dieses
Problem aufzeigen können. Der Status des Fundortes als frühere
›Hauptstadt‹ und die überwältigenden Mengen an Fundmaterial haben
jedoch dazu geführt, dass die Datierungsvorschläge kaum angezweifelt
wurden. Erst durch die Ausgrabungen in Kuşaklı und durch die dortige
konsequente Anwendung der Dendrochronologie wurde das Problem erkannt.
Nachdem die Datierungen in Boğazköy angezweifelt wurden, brauchte es –
zumindest für die bis 1993 ausgegrabenen Areale − neue Studien, um ein
belastbares Gerüst zu schaffen.

Um eine Chronologie für die \unterstadt{} zu erstellen, habe ich
zunächst die ursprüngliche Argumentation zusammengefasst und
diskutiert (\cref{III-1-EpigraphischeStratigraphie}). Nach dieser
Dekonstruktion werden die neu gewonnenen Daten herangezogen, um ein
neues chronologisches Gerüst zu erstellen
(\cref{III-2-NeuesFundament}), welches im letzten Teil  (\cref{III-3-Ergebnisse})
nochmal kritisch überprüft wird.

\section{Die Probleme der ›epigraphischen Stratigraphie‹}\label{III-1-EpigraphischeStratigraphie}

\subsection{Der Maßstab für Boğazköy: die Sequenz von \buyukkale}

Die stratigraphische Sequenz von \buyukkale{} ist die erste, die in
Boğazköy ausgegraben und mit historischen Ereignissen korreliert
wurde. Dies wurde schließlich 1982 durch Neve publiziert
(\cref{Tab-StraVilleBasseNeve}). Die kulturelle Abfolge beginnt in der
Frühbronzezeit und reicht bis zu Einzelfunden der byzantinischen
Epoche, wobei die römische Bauschicht nicht in das Nummerierungssytem
einbezogen wurde: die Periode I umfasst die jüngere Eisenzeit.
Das Ende der durch Brand zerstörten Phase IVd wird mit der
Eroberung und Zerstörung der Stadt durch \DEanitta{} in Verbindung
gebracht, die im gleichnamigen Text beschrieben ist. Die Phase
IVc beginnt nach einem anhand desselben Textes rekonstruierten
Hiatus; der Übergang zur Phase IVb wäre demnach in die Mitte
des \xvjhs{} zu datieren. Diese Datierung wurde damit begründet, dass
im dieser Zeit ein erheblicher Umbau der Büyükkale vorgenommen wurde,
der nur von einem ›mächtigen‹ König wie \DEtelepinu{} durchgeführt
worden sein konnte. Die Umsiedlung der Hauptstadt nach
\DEtarhuntassa{} markiert das Ende der Phase IVa und die Phasen
IIIc, IIIb, IIIa wurden den letzten drei Großkönigen,
\DEhattusiliIII, \DEtuthalijaIV{} und \DEsuppiluliumaII{} zugewiesen.

Diese archäologische Stratigraphie und die mit ihr verknüpften
historischen Ereignisse wurden spätestens seit den 1960er Jahren als
Maßstab und Ausgangspunkt für die Rekonstruktion der Stadtgeschichte
verwendet, an die die Sequenzen anderer, topographisch nicht
verbundener Grabungsbereiche angeschlossen wurden.

\subsection{Die Sequenz des \nwhs}

Die Schichtenabfolge auf dem \nwh{} ist sowohl für die frühen Perioden
von der \karumzeit{} bis in die hethitische Epoche wichtig als auch
für die Eisenzeit und die hellenistische Periode von besonderer
Bedeutung. Insbesondere die Publikation der Keramik der \fbz{} und
\karumzeit{} stellt bis heute eine wichtige Referenz dar.

\subsection{Die Sequenz des \nordvs}

Während der Ausgrabungen in den 1950er Jahren wurden fünf Bauschichten
unterschieden, die durch eine Profilzeichnung dokumentiert wurden
(\cref{BogazkoyIII-UST-Strat}). Im Archiv der Boğazköy-Grabung
befinden sich zwei weitere Zeichnungen aus den Kampagnen 1956 und
1958, die erheblich genauer sind, jedoch bisher nicht veröffentlicht
wurden (\cref{Bo56-Schnitt--k1a-f1b-Sud,Bo58-SchnittC,Bo58-SchnittC-Detail}).

Hinsichtlich der Chronologie ist die Bauschicht 5 von großem
Interesse. Diese befindet sich unter der Bauschicht 4 der
\karumzeit{} und datiert mithin in die ausgehende Frühbronzezeit. Sie
umfasst die doppelte Brennkammer eines Keramikbrennofens (s.
\cref{IV-I-BefundeFBZ}). 1956--1957 wurde die Bauschicht 4 in
zwei Phasen − 4a und 4b − unterteilt, die jedoch auf den
Plänen nicht sichtbar sind und daher infrage gestellt werden müssen.

Bezüglich des sich im \nordV{} befindenden Profils
(\cref{Bo56-Schnitt--k1a-f1b-Sud}) ist bemerkenswert, dass hier
Strukturen bis zur Oberfläche vorhanden sind. Im Gegensatz dazu sind
im Profil der \WTer{} kaum Strukturen in dem oberen Teil sichtbar.
Dieser Unterschied muss bei der Interpretation des Gesamtareals
berücksichtigt werden.

\subsection{Die monumentalen Bauten in der \unterstadt{}}

Anhand der Chronologie von \buyukkale{} versuchte man die monumentalen
Bauten in der \unterstadt{} in die Geschichte der Stadt einzubinden.
Einige dieser Bauwerke waren immer an der Oberfläche sichtbar und
wurden schon vor dem Ersten Weltkrieg freigelegt. Da es keine
Stratigraphie gibt, konnten  nur vage Datierungen vorgeschlagen
werden, die sich später durch ihre stetige Verwendung verfestigten.
Die Datierung des \tempelI{} wurde nicht älter als das \xivjh{}
geschätzt, die \poternenmauer{} wurde zwischen das 17. und
\xvjh{} und die \abschnittsmauer{} in das \xiiijh{} datiert. 1967−1969
hat Neve die Magazine um den \tempelI{} sowie die \komplexeIIII{} und
die \quellgrotte{} freigelegt (\cref{BogazkoyV-PlanKomplexII}).

\subsection{Die Sequenz der \westt}

Zwischen 1970 und 1973, nach den ersten Ausgrabungen auf der \westt{},
hat Neve zunächst 8 Bauschichten unterschieden, die mit drei Schichten
für die \hetp{} und einer für die \karumzeit{} der Sequenz des
\nordV{}s ähneln (\cref{Tab-StraUST-Neve}). Diese Einteilung wurde
aber nicht durchgehend verwendet und Neve hat letztendlich die
Benennung aus dem \nordV{} übernommen.


\subsection{Zwischenbilanz}

Dieser Überblick zeigt, dass es nicht möglich ist, die postulierten
absoluten Datierungen als gesichert anzunehmen, da sie mehrheitlich
auf Aussagen ohne nachprüfbare Belege beruhen.

\section{Ein neues Fundament für die Datierung}\label{III-2-NeuesFundament}

Im ersten Teil wurde gezeigt, dass die absoluten Datierungen der Funde
und Befunde problematisch sind. Allerdings berührt dieses Problem die
relative Chronologie innerhalb des jeweiligen Grabungsareals kaum. Die
Synchronisation zwischen den verschiedenen Sequenzen ist jedoch
schwieriger, da die Synchronismen nicht auf archäologischem Material
basieren. Um dieses Problem zu lösen, habe ich zwei Methoden
angewendet: in einem ersten Schritt habe ich eine Auswertung von neu
aufgenommenen Keramik-Ensembles der Grabung auf der \westt{} nach der
von Schoop entwickelten Methode vorgenommen; in einem zweiten Schritt
wurden diese Ergebnisse mit den Ergebnissen verschiedener
Radiokarbon-Datierungen verknüpft. Das übergeordnete Ziel dieser
Vorgehensweise ist aber nicht, eine sehr feine Chronologie für jedes
einzelne Gebäude zu erstellen, vielmehr geht es darum eine mögliche
Zeitspanne für die vier Bauschichten der \westt{} zu rekonstruieren
(siehe dazu \cref{IV-Besiedlung}).


\subsection{Die Ergebnisse der Keramik-Analyse}

Im Laufe der Ausgrabungen auf der \westterrasse{} wurde nur
vergleichsweise wenig Material auf sicher datierbaren Fußböden dokumentiert. Von
diesem wurden bisher lediglich zwei Ensembles analysiert und
publiziert − eines aus dem \gebR{} (Raum 4) und ein weiteres aus dem
Gebäude \gebTQ{}. Um zusätzliche Datierungsmöglichkeiten für die
\westterrasse{} einzubeziehen, galt es, neue gut stratifizierte
Keramik-Ensembles zu untersuchen. Diese Analyse und die Typologie der
Keramik werden im Anhang behandelt (s. Anhang \ref{Ana-Kera}).  Sie
beruht auf diagnostischen Scherben von 1884 und ermöglicht einen \DEtaq{}
für einzelne Gebäude. Demnach datiert der Zeitpunkt der Aufgabe der \gebOW,
\gebOR, \gebOT{} und \gebOZ{} in die \karumzeit. \gebTQ{}, \gebIR{}
und \gebIT{} gehören dem Ende des \xviijhs{} an; \gebWO, \gebQO,
\gebR{} sind in das 16. oder \xvjh{} zu datieren, während
\gebRE{} im \xivjh{} aufgelassen wurde. Über das \gebI{} ist nur sehr
wenig bekannt, möglicherweise ist es ins \xiiijh{} zu datieren.


\subsection{Radiokarbon-Datierungen}

Zur Kalibrierung der Radiokarbondaten wurden das Programm \OxCal{}
sowie die Kalibrationskurve \IntCalQE{} verwendet. Die kalibrierten
Daten sind mit \calBC{} angegegben. Diese wurden in einem weiteren
Schritt in Baye'sche Interferenzmodelle integriert, deren Ergebnissen
kursiv (d.~h. \scalBC{}) gekennzeichnet sind. Im folgenden Abschnitt
wurden die englischen Termini übernommen, die für das Erstellen der
Modelle im Programm \OxCal{} verwendet wurden.

\subsubsection{Daten der \fbz}

1964 wurden vier Proben in der frühbronzezeitlichen Schicht von
\buyukkale{} genommen und datiert. Diese ermöglichten es, eine grobe
Zeitspanne für die Dauer der Besiedlung in der \fbz{} zu ermitteln
(\cref{Radiocarbon--EBA--1964}). Die Proben stammen von verbrannten
Holzbauteilen des ›Gebäudes 1‹ und wurden in einer \emph{phase}
zusammengefasst, da es sich nicht um Datierungen handelt, die
aufeinander folgen (\cref{Plot-Radiocarbon--EBA--1964}). Diese Phase
wurde dann mit zwei \emph{boundaries} in einer Sequenz eingegrenzt.
Da es sich um ein einfaches Modell handelt, ist die allgemeine
Übereinstimmung des Models höher (\aoverall{}: \SI{112,8}{\percent})
als die \SI{60}{\percent} Schwellenwerte.  Dieses Modell datiert den
Anfang der Phase zwischen 2372 und 1902 \scalBC{} (bei einer
Wahrscheinlichkeit von \SI{95,4}{\percent}) und das Ende zwischen 2109
und  1549 \scalBC{} (bei einer Wahrscheinlichkeit von
\SI{95,4}{\percent}).

\subsubsection{Die \karumzeit}

Wie für die \fbz{} wurden 1964 Radiokarbonproben aus Kontexten der
\karumzeit{} auf \buyukkale{} entnommen, die jedoch bisher
unveröffentlicht geblieben sind (\cref{Radiocarbon--MBA--1964}). Diese
stammen von Holzbalken des \gebsIP{}, sodass sich ein terminus ante
quem für dieses ergibt.  Seit 2009 wurden weitere Datierungen von
Kontexten der \unterstadt{} durchgeführt, die von verkohlten Samen
stammen und wesentlich zu einer genaueren Datierung der \karumzeit{}
beitragen (\cref{Radiocarbon--MBA--2013}).

Es gibt keine direkte Korrelation zwischen diesen beiden Arealen. Um
dennoch ein Baye'sches Interferenzmodell erstellen zu können, wurden
die Daten der verschiedenen Areale in ein aus zwei \emph{phases}
bestehendes \emph{overlapping model} integriert
(\cref{Plot-Radiocarbon--MBA}). Anhand dieses Modells kann man
schließen, dass die \karumzeit{} in Boğazköy zwischen 2206 und 1940
\scalBC{} begonnen hat (bei einer Wahrscheinlichkeit von
\SI{95,4}{\percent}) und zwischen 1867 und 1588 \scalBC{} endete (bei
einer Wahrscheinlichkeit von \SI{95,4}{\percent}). Mithin dauerte die
Besiedlung auf der \buyukkale{} zwischen 0 und 262 Jahre, während für
die Nutzung im Bereich \knw{} eine Dauer zwischen 83 und 383 Jahren
angenommen werden kann.

\subsubsection{Die \hetp}

Die Radiokarbondatierungen dieser Periode werden seit 1994 immer
zahlreicher und stammen meistens von verkohlten Samen aus gesicherten
Kontexten, wie z.~B. vom \nwh{} (\cref{Radiocarbon--Hittite--NWH-Silo})
und aus dem Bereich \knw{} (\cref{Radiocarbon--Hittite--2013}).

Während der Bearbeitung der Keramik aus den Ausgrabungen auf der
\westt{} wurden 2012 im Depot des Grabungshauses Tierknochen der
Kampagnen 1975--1977 entdeckt, die mit den Nummern der assoziierten
Keramik beschriftet waren. Sie können deshalb stratigraphisch
eingeordnet werden. Fünf Knochen wurden datiert, jedoch habe ich mich
entschieden nur drei der Daten als sicher einzustufen.

Zwischen den verschiedenen Arealen (\nwh{}, \knw{} und \westt{}) gibt
es keine direkte stratigraphische Verbindung, so dass im Programm
\OxCal{} erneut drei \emph{phases} benutzt wurden
(\cref{Plot-Radiocarbon--Hittite--2013-WTer}). Mit Blick auf die
Areale von \knw{} und \westt{} können verbindende stratigraphische
Informationen ebenso wie die Keramikanalyse verwendet werden. Das so
gewonnene Modell zeigt, dass das \gebIT{} älter sein muss als das
\gebWO{}.

Die allgemeine Übereinstimmung des Modells liegt deutlich über dem
Schwellenwerte von \SI{60}{\percent} (\aoverall{}:
\SI{122,4}{\percent}).  Dieses Modell datiert den Anfang der
hethitischen Bauschichten in diesem Stadtbereich zwischen 1815 und
1556 \scalBC{} (bei einer Wahrscheinlichkeit von \SI{95,4}{\percent})
und deren Ende zwischen 1609 und 1422 \scalBC{} (bei einer
Wahrscheinlichkeit von \SI{95,4}{\percent}).


\subsubsection{Ein umfassendes  Modell}

Bisher wurden die drei Abschnitte der Bronzezeit separat behandelt, um
die Ausgangslage zu erläutern. Nun müssen diese zu einer \sequence{}
verbunden werden, die die gesamte Bronzezeit überspannt. Dabei
wurden zwei Modelle erstellt, um zwei unterschiedliche Szenarien zu
testen.

Das erste Modell trennt die einzelnen Phasen mit  \boundaries{}
(\cref{Plot-Radiocarbon--model-base1}). Die Übereinstimmung
(\aoverall{}: \SI{106,5}{\percent}) des Modells liegt weit über dem
Schwellenwert von \SI{60}{\percent}. Aus den \boundaries{}
(\cref{model-base-BoundaryDebutBA,model-base-BoundaryTransitionBA-BM,model-base-BoundaryTransitionBM-BR,model-base-BoundaryFinBR});
kann man schließen, dass der Anfang der \fbz{} zwischen 2170 und 1964
\scalBC{} gelegen haben muss (bei einer Wahrscheinlichkeit von
\SI{95,4}{\percent}), der Übergang von der \fbz{} zur \karumzeit{}
zwischen 2105 und 1944 \scalBC{} (bei einer Wahrscheinlichkeit von
\SI{95,4}{\percent}), der Übergang von der \karumzeit{} und zur
\hetenp{} zwischen 1847 und 1617 \scalBC{} (bei einer
Wahrscheinlichkeit von \SI{95,4}{\percent}) und das Ende der
hethitischen Besiedlung in dem fraglichen Bereich der südlichen
\unterstadt{} zwischen 1608 und 1443 \scalBC{} (bei einer
Wahrscheinlichkeit von \SI{95,4}{\percent}) gelegen haben muss.

Im zweiten Modell wurde die Gleichzeitigkeit der beiden Areale der
\karumzeit{} als zusätzliche Komponente hinzugefügt, was die Angaben
des Anitta Textes widerspiegelt. Dort ist erwähnt, dass die gesamte
Stadt in einem Ereignis zerstört wurde. Die Übereinstimmung mit dem
Modell (\cref{Plot-Radiocarbon--model-base2}) zeigt, dass dieses
ebenfalls gültig ist (\aoverall{} von 107,5). Dazu ist zu bemerken,
dass sich die Datierungen an sich kaum ändern.  Dies erklärt sich
dadurch, dass es noch zu wenig zusätzliche Informationen zu den
Modellen gibt, um ihre Genauigkeit zu erhöhen.

Bezüglich des Endes der Sequenz muss die komplette Abwesenheit von
Datierungen aus dem 14. und \xiiijh{} hervorgehoben werden.  Daraus
kann man möglicherweise schließen, dass die Siedlung in dieser Zeit
kleiner war und deswegen schlechter erhalten ist. Dieses Ergebnis steht in
jedem Fall in einem deutlichen Gegensatz zu den Vorschlägen von Bittel
und Neve, aber es deckt sich in gewisser Weise mit den Ergebnissen,
die in den letzten zwanzig Jahren gewonnen wurden.

\subsubsection{Weitere neue Erkenntnisse}

Seit der Etablierung der relativen und historischen Chronologie in der
\unterstadt{} sind an verschiedenen Fundorten neue Erkenntnisse
gewonnen worden, die es zu berücksichtigen gilt. Angelehnt an die
Datierung des Tempels von \kusakli{} in das \xvijh{} wurde auch für
den \tempelI{} aufgrund typologischer Ähnlichkeiten eine Datierung in
das \xvijh{} vorgeschlagen. Die Datierung der verschiedenen Wehrmauern
in Boğazköy, deren Verlauf durch die massiven Fundamente gesichert
ist, ist schwierig. Es gibt kaum direkte Hinweise, da meistens nur die
Oberkanten der Fundamente und der Sockel freigelegt wurden, ohne dass
datierendes Material \DEinsitu{} gefunden wurde. Allerdings haben die
Ausgrabungen auf \buyukkaya{}, auf dem \nwh{} und in \kusakli{} dazu
beigetragen, die Chronologie der Wehrmauer durch zwei verschiedene
Arten von Türmen zu rekonstruieren (\cref{PlanTower}). Der ältere
Typus wurde vor die durchgehende Außenmauer der \poternenmauer{}
gesetzt und scheint schon im Verlauf des Baus dieser
Befestigungsanlage von dem jüngeren Typ ersetzt worden zu sein. Dies
spricht dafür, dass es sich bei der \poternenmauer{} um die älteste
Verteidigungsanlage handelt und die Datierung der Zerstörung des Silos
auf dem \nwh{} im \xvijh{} als \DEtaq{} gelten kann.

Eine Datierung der \abschnittsmauer{} in der \unterstadt{} ist
schwierig. Ausgehend von den Ausgrabungen auf \buyukkaya{} hat Seeher
eine Datierung in das \xvijh{} vorgeschlagen. Dies würde bedeuten,
dass die \abschnittsmauer{} relativ schnell nach dem Bau der
\poternenmauer{} errichtet wurde. Wenn dies zutrifft, muss sich das
Gesicht der Stadt zu dieser Zeit komplett geändert haben.  Unklar
bleibt, ob es Vorgänger zu diesen Mauern gegeben hat. M.~E.{} ist es
wahrscheinlich, dass eine Wehrmauer in der \karumzeit{} existierte,
wie sie bei anderen Städten dieser Zeit belegt ist. Es wäre z.~B.
möglich, dass ein Vorgänger der \abschnittsmauer{} existierte, um die
Stadt im Norden zu verteidigen.

\section{Ergebnisse}\label{III-3-Ergebnisse}

\subsection{Die \fbz}

Belege für die \fbz{} sind selten in der \unterstadt{}, da bisher nur
die doppelte Kammer eines Keramikbrennofens bekannt ist, obwohl ab und
zu Keramikscherben der \fbz{} entdeckt wurden (aus Aufschüttungen?).
Die Keramik dieser Brennkammer lässt sich gut mit der Keramik von
\buyukkaya{} vergleichen und die Radiokarbondatierungen von
\buyukkaya{} liefern die besten Anhaltspunkte für eine allgemeine
absolute Datierung der \unterstadt{}.

\subsection{Die \karumzeit}

Die Abfolge von Schichten auf \buyukkaya{} spricht möglicherweise für
eine kontinuierliche Besiedlung von der \fbz{} bis in die \karumzeit{}
an dieser Stelle. In der \unterstadt{} sind die Befunde der
\karumzeit{} gut erhalten, wenn sie nicht in der \hetenp{} zerstört
wurden. In fast allen Grabungsarealen sind Spuren eines heftigen
Brands in der \karumzeit{} zu finden.  Die Keramik-Sequenzen der
verschiedenen Areale sind einander sehr ähnlich und es ist bisher
nicht möglich gewesen, verschiedene Phasen zu identifizieren. Deshalb
sollten alle Ensembles als zeitgleich betrachtet werden, was für eine
großflächige Zerstörung der Stadt spräche. Lediglich \buyukkaya{}
scheint nicht zerstört worden zu sein.


\subsection{Die \hetp}

Die Synchronisierung der Chronologie der verschiedenen Areale ist nur
bedingt möglich.  In der Phase \buyukkale IVb wurde im
\chantier{Gebäude F} die am besten erhaltene Gefäßgruppe gefunden, die
bisher leider nicht bearbeitet wurde. Dennoch deuten die wenigen
bekannten Indizien auf eine Datierung in das \xvijh{} hin. Das einzige
bekannte Gefäß aus diesem Kontext erlaubt eine Verbindung zum Inventar
von \gebR{} oder auch von \gebWO{} in der \unterstadt{}; der Ofen des
Raumes 5 von \chantier{Gebäude F} ähnelt einem Ofen im Areal \knw{},
der nach Radiokarbondatierungen in das \xvijh{} datiert; schließlich
datiert auch ein Siegel in diese Periode (\cref{Bo64-786}), welches in
einem Raum dieses Gebäudes gefunden wurde.  Dementsprechend datiere
ich die Phase \buyukkale{} IVb in das \xvijh{} Aus Mangel an
gesicherten Daten gehe ich davon aus, dass die Phase IVc
allgemein ins \xviijh{} und IVa ins \xvjh{} datiert. Die
Bauwerke der \unterstadt{} sind folglich früher zu datieren als bisher
angenommen.

Diese Elemente bilden die Grundlage für die Rekonstruktion eines neuen
chronologischen Gerüsts (\cref{Tab-Strati-Finale}), das in der Zukunft
weiter verfeinert und korrigiert werden muss.
