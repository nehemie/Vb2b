\chapter{Räumliche Analyse der Unterstadt}

Oft wird behauptet, dass nur erhaltene Befunde mit \DEinsitu{} Funden
eine funktionale Analyse einer Siedlung erlauben. Im ersten Teil des
Abschnittes erkläre ich (\cref{V-1-Fundlage}), warum diese nicht
zwingend ist. In einem zweiten Schritt wird die spezifische Situation
in Boğazköy betrachtet (\cref{V-2-Aktivitaten}). Dabei liegt ein
besonderer Schwerpunkt auf der Verteilung der Kleinfunde, die als
Indikator von Aktivitäten interpretiert werden können
(\cref{V-3-Implementierung}). Eine räumliche Analyse der
identifizierten Aktivitäten bildet den Abschluss
(\cref{V-4-Verbreitungskarten}).

\section{Zur Verbindung zwischen der Fundlage und Nutzung von Objekten}
\label{V-1-Fundlage}

Kleinfunde geben viele Hinweise auf Aktivitäten, da ihre Funktionen
und vor allem ihre Verteilung von den jeweiligen Aktivitäten − z.~B.
Produktion, Konsum, Aufbewahrung oder Entsorgung − der sie nutzenden
Individuen abhängig ist. Um Rückschlüsse über die Aktivitäten ziehen
zu können, ist es notwendig, die Beziehung zwischen den Objekten und
dem Ort ihrer Auffindung möglichst objektiv darzustellen und die
Prozesse der Entstehung des jeweiligen archäologischen Kontextes,
d.~h.\ der Taphonomie, zu rekonstruieren und bei der Interpretation zu
berücksichtigen.

\subsection{Grundlage der Taphonomie}

Schiffer hatte 1976 vorgeschlagen, dass archäologische
Fundverteilungen ein verzerrtes Bild von einem vergangenen
Verhaltensmuster widerspiegeln − eine Position, die Binford 5 Jahre
später als ›Pompeji Prämisse‹ bezeichnet. Binford war der Meinung, dass
die Fundverteilung kein verzerrtes Bild widerspiegelt, sondern das
Ergebnis von kulturellen Handlungen ist. Mithin liefern die Funde, die
nicht aus ihrer ›primären Lage‹ stammen, trotzdem viele Informationen
über kulturelle Entscheidungen wie z.~B.{} über den Umgang mit Abfall.
Vor diesem Hintergrund können die Funde aus Boğazköy zwar nicht als
Indikatoren lokalisierbarer Funktionen in bestimmten Gebäuden
interpretiert werden, aber sie können dennoch als Quelle für die
Rekonstruktion eines allgemeinen Überblicks über die Aktivitäten
genutzt werden, die in der Stadt verortet waren.

\subsection{Zur Anwendung auf die Befunde in Boğazköy}

Um qualitativ aussagefähige Rückschlüsse auf bestimmte Aktivitäten
ziehen zu können, wurden die Objekte ihrer Taphonomie entsprechend
geordnet. Da die Informationen über viele Funde und ihrer Zuordnung zu
bestimmten Strata häufig minimal sind, wurden die Objekte in drei
Klassen eingeordnet:

\begin{description}[noitemsep,nolistsep,font=~$\bullet$~\normalfont\textbf]
 \item [\DEinsitu{}:] Objekte, die auf Fußböden gefunden wurden.
 \item [geschlossener Kontext:] Objekte, die zusammengefasst wurden.
 \item [offener Kontext:] Objekte, die ohne klaren Zusammenhang –
   z.~B.{} in Raumfüllungen – gefunden wurden. Auch wenn es keine
  Informationen über ein Objekt gab, wurde es in diese Klasse
  eingeordnet.
\end{description}

\section{Bestimmung von Aktivitäten}
\label{V-2-Aktivitaten}

In diesem Abschnitt erfolgt eine funktionale Bewertung der Objekte,
um diese bestimmten Aktivitätsmustern zuzuordnen.

\subsection{Definition der Aktivitätskategorien}

Bei der Zuordnung der Objekte zu verschiedenen Aktivitätsmustern
stellen sich verschiedene Fragen.  Zunächst macht ein Blick auf die
ursprüngliche Kategorisierung der Objekte zwei Bemerkungen notwendig
(\cref{HistObjet}): die ursprüngliche Klassifizierung ist ein
Konstrukt, das die Methode der Fundaufnahme und den damaligen
Forschungsstand widerspiegelt. Dies wird insbesondere  in dem Diagramm
sichtbar, bei dem die Anzahl der zahlreichen Nadel-Typen mit dem
einzigen ›Gefäß‹ Typ verglichen wird. Daher ist es notwendig, diese
Kategorien zunächst infrage zu stellen, wenn man darauf aufbauend
abstrakte Aktivitäten definieren möchte. Darüber hinaus sind unzählige
Kategorisierungen möglich. Ein Spinnwirtel  kann beispielsweise als
Nachweis verschiedener Aktivitäten dienen − z.~B.  als Hinweis auf die
Produktion von Fäden, Textilien oder anderer Objekte. Daher ist eine
moderne Kategorisierung immer subjektiv, heuristisch und
meinungsbildend.

Um meine Kategorisierung nach Möglichkeit zu objektivieren, habe ich
zunächst zwei Ansätze verglichen
(\cref{AktivitatenVerhoeven,AktivitatenPfalzner}). Verhoeven,
der größtenteils die Klassifizierung von Voigt übernommen hat,
unterteilte Fundstücke in vier Hauptkategorien: Lebensunterhalt,
Objektproduktion, Administration und soziale Aktivitäten. Stattdessen
hat Pfälzner in seiner Untersuchung von Hausinventaren die Objekte in
12 Kategorien gegliedert. Im Rahmen dieser Arbeit habe ich die
ursprüngliche Deutung der Objekte der \unterstadt{} von Boehmer
übernommen und in einem zweiten Schritt aber, die Objekte bestimmten
Aktivitäten zugeordnet, indem ich die Gliederungen von Verhoeven und
Pfälzner benutzt habe
(\cref{TabTypesObjets,TabFunObjets,TabCatObjets}).

\subsection{Visualisierung der Klassifikation}

Um die Kategorisierung der Kleinfunde visuell darzustellen und ihre
Diskussion zu vereinfachen, wurden Sankey-Diagramme verwendet
(\cref{Lab_SankeyPrObjet_p,Lab_SankeySocial_p,Lab_SankeySubsi_p}). Bei diesem
Diagrammtyp ist die Breite jedes Pfeils proportional zur Objektanzahl
der jeweiligen Kategorie angegeben. Dies ermöglicht leichter zu
erkennen, welche Kategorien mehr oder weniger Gewicht für die
Interpretation haben. Es sind z.~B. die Keilschrifttexte, die den
Hauptanteil der Kategorie ›Administration‹ ausmachen. Für ›gemischte‹
Kategorien wie ›Metall/Textilien‹ oder ›Schmuck/Textilien‹ hilft diese
Form der Darstellung dabei, sich vorzustellen, was sich ändert,
wenn man Kategorien zusammen auswerten würde.  Mittels
Sankey-Diagrammen ist es zudem einfacher, neue Gruppierungen zu
schaffen, wie z.~B. eine Zusammenführung von kleineren Metallobjekten
wie Meißel und Nadeln, um so die Kategorie ›kleine Handwerkzeuge‹
hinzuzufügen. Im Fall der \westt{} wird deutlich, wie geläufig diese
Art von Werkzeugen ist; die Diagramme zeigen, dass Handwerk eine
alltägliche Aktivität in den Häusern gewesen sein muss.

\section{Die Anwendung einer räumlichen Analyse}
\label{V-3-Implementierung}

Eine räumliche Analyse basiert zuallererst auf den Fundorten jedes
einzelnen Objekts. Im Verlauf der Grabungen auf der \westt{} wurden
die Objekte mit Planquadratangaben und nicht mit genauen Koordinaten
aufgenommen. Da GIS nur genaue Koordinaten verarbeiten kann, ist es
unvermeidlich, den Fundstücke genauere Koordinaten zu zuweisen, um die
Objekte mittels eines GIS zusammenführen zu können.

\subsection{Planquadrate georeferenzieren}

Eine entsprechende Zuweisung wäre einfach gewesen, wenn ein Plan aller
Planquadrate vorhanden wäre, der einfach zu georeferenzieren ist.
Leider sind die Planquadrate der Jahre 1975--1978 nirgendwo
dokumentiert. Deswegen habe ich einen Plan erarbeitet, in dem alle
relevanten Planquadrate dargestellt sind
(\crefrange{ExcavationTrenches1}{ExcavationTrenches6}). Wenn bezüglich
eines Fundes genauere Angaben zum Fundort gemacht wurden als das
Planquadrat, wie z.~B. ein konkreter Raum, wurde das Objekt anhand
dieser Angaben kartiert, um die Genauigkeit der Zuweisung zu erhöhen.
Das Grundschema der gewonnenen Kartierung ist mithin relativ sicher,
jedoch musste die Größe einiger Planquadrate geschätzt werden.

\subsection{Koordinaten vergeben}

Dieser Plan wurde anschließend genutzt, um jedem Objekt Koordinaten
zuzuweisen. Innerhalb eines jeden Planquadrats wurden zufällige Punkte
erstellt und den Objekten zugewiesen
(\cref{Fig-542-Attribution-Coordonnees-SIG}).

\subsection{Kritik der Methodik}

Diese Vorgehensweise beruht auf verschiedenen Annahmen, die ich
explizit nennen möchte. Zunächst müssen die Daten homogen sein, um sie
vergleichen zu können.  Ein Diagramm der Anzahl der gefundenen Objekte
in Relation zu der ausgegrabenen Fläche zeigt, dass es einen linearen
Zusammenhang zwischen diesen beiden Variablen gibt
(\cref{Excavation-Objet_per_square_perYear}). Lediglich im Jahre 1938
ist die Ratio deutlich kleiner. Daher müssen die in diesem Jahr
ausgegrabenen Areale immer mit besonderer Vorsicht in den Vergleich
einbezogen und interpretiert werden. Daneben gibt es einige ›leere
Areale‹ (\gebU, \gebEQ, \gebWI), zu denen es kaum Informationen gibt
(\cref{GeneralObjectDistribution}).

Die Objekte wurden nach Aktivitäten kartiert. Leider war eine
chronologische Gliederung nur im Fall der Tontafeln und Siegel
möglich, weil diese gut datierbar sind. Für alle anderen Kategorien
sind alle Perioden zusammen dargestellt. Da die meisten Objekte aus
der Zeit des 17. bis  \xvjhs{} keine deutlich erkennbaren typologischen
Unterschiede aufweisen, eignet sich dieser Zeitraum am besten für die
Darstellung. Um mögliche Cluster besser sichtbar zu machen, wurde eine
Kerndichteschätzung angewendet, deren Skala am Rand der Karte
dargestellt ist.

\section{Verbreitungskarten}
\label{V-4-Verbreitungskarten}

\subsection{Keilschrifttexte der \westt}

Die Untersuchung der räumlichen Verteilung von Keilschriftfragmenten
gibt Hinweise auf die Art ihrer Aufbewahrung und auf die Taphonomie.

Die hethitischen Texte sind in der \textit{Konkordanz der hethitischen
  Keilschrifttafeln}
entsprechend ihres Duktus in drei Stufen grob −
und manchmal umstritten − chronologisch  eingeordnet: althethitisch,
mittelhetitisch und junghethitisch (\cref{TableTabletteProp}). Ein
Arbeitsansatz war, die Verteilung der Tafeln zu untersuchen, indem
die Datierung der Keilschrifttafeln -- anhand der Datierung des Duktus --
so genau wie möglich mit den
architektonischen Überresten abgeglichen wird, um zu sehen, ob die
architektonische Struktur die Verteilung der Keilschriftfragmente
erklären könnte (\cref{GIS-Repartition-tablette-carte}).

Ein Viertel der Texte gehört der \karumzeit{} an und stammt aus dem
Umfeld des \gebesUW{}. Da die meisten Texte aus dem Archiv des
\DEdaya{} stammen, ist zu vermuten, dass dies sein Haus gewesen ist.
Aus dem Großteil der Auswertungen der Keilschriftbefunde der Stadt
\hattusa{} geht hervor, dass die meisten Keilschrifttexte aus drei
›Archiven‹ der hethitischen Zeit stammen: auf \buyukkale, \hah{} und
\tempelI. Der Anteil der Fragmente pro Duktus aus \tempelsI{}  ist
ähnlich wie der aus der Unterstadt (\cref{TableTabletteProp}). Es ist
daher davon auszugehen, dass die Texte aus dem \tempelI{} stammen und
in der Unterstadt bei der Zerstörung des Tempels oder während der
Tempelausgrabung der 1930er Jahre dorthin verlagert wurden.

Wenn die Fragmente jedoch aus einem ›Archiv‹ stammen, ist es
problematisch die Übereinstimmung von Datierung der Fragmente und den
architektonischen Überresten zu interpretieren. Die Datierung des
Textes ist nicht unbedingt ein Hinweis auf den Anfang oder auf das Ende der
Nutzung der Architektur und dies könnte irreführend sein. Wie könnte
man anders vorgehen?

\subsubsection{Eine Frage der Größe?}

Um herauszufinden, ob es sich bei den Fragmenten nur um Streufunde
handelt oder ob sie für Hinweise auf ihren ursprünglichen
Aufbewahrungsort herangezogen werden können, wurde die Größe der
Fragmente ermittelt. Eine Darstellung der Fläche jedes Fragments drückt
deutlich aus, dass nur wenige Fragmente groß sind, und dass die
überwiegende Mehrheit klein ist, was eher auf zufällige Entdeckungen
zurückzuführen ist (\cref{Pl_43}). Die räumliche Verteilung der Größe
deutet darauf hin, dass die kleinen Fragmente, die um den \tempelI{}
herum gefunden wurden, ursprünglich von dort stammen müssen
(\cref{Lab_GIS-Cuneiform-bubbleplot-size}). Darüber hinaus gibt es eine
Konzentration von Fragmenten im Bereich von \gebTT{}. In diesem Gebäude,
einem der wenigen spät gebauten Gebäuden, kann ein zusätzlicher
Aufbewahrungsort vermutet werden, welchem drei zu derselben
Keilschrifttafel gehörenden Fragmente (Bo73-382, Bo75-146d und
Bo75-146b) zugeschrieben werden können.

\subsubsection{Verteilung der verbundenen Fragmente}

Die 1957 im Norden der \westt{} gefundenen und zusammenpassenden
Fragmente verstärken die Idee (\cref{Lab_GIS-Cuneiform-joints}), dass im
\gebTT{} Keilschrifttafeln aufbewahrt wurden. Die Verteilungen deuten
darauf hin, dass die Fragmente nicht über weite Strecken verlegt
wurden und stützen damit die These, dass sich die Objekte in einer
gewissen Nähe zu ihrem ursprünglichen letzten Aufbewahrungsort
befunden haben. Aus dem südwestlichen Teil der \westt{} kommen sechs
Fragmente, die zu den größten Fragmenten und zu einer Tafel des
KI.LAM-Festivals (CTH 627) gehören. Ist es denkbar, dass diese Art von
Keilschrifttexten einzeln in einem ›privaten‹ Gebäude aufbewahrt wurde
oder sollte es als offizielles Gebäude betrachtet werden? Schließlich
scheint es schwierig, die Präsenz vieler Fragmente im nördlichen Teil
der West-Terrasse zu erklären. Jedoch deutet die Untersuchung der
Verteilung von Siegeln und Siegelabdrücken tendenziell auf ein offiziellen
Gebäude in der Nähe hin (siehe unten).


\subsubsection{Genre-Verteilung}

Die räumliche Verteilung der Gattungen nach der Klassifizierung von
Laroches \emph{Catalogue des Textes Hittites (CTH)} zeigt, dass die
meisten Fragmente mythischer Texte in der Nähe des \gebsTT{} gefunden
wurden.

\subsubsection{Kurzfassung der Verteilung von Keilschrifttafeln}

Zusammenfassend lässt sich sagen, dass es drei ›Hauptquellen‹ von
Keilschrifttexten in der Unterstadt gab: das Archiv von \DEdaya{} in der
\karumzeit; das ›Archiv‹ im \tempelI{} und eine Gruppe im \gebTT{}.
Schließlich zeigt die Studie der Keilschrifttafeln aus der Sicht der
Taphonomie, dass es möglich ist, eine Verbindung zwischen
Aufbewahrungsort und Fundort herzustellen, auch wenn es schwierig
bleibt, die Objekte den architektonischen Überresten zuzuordnen.


\subsection{Siegel und -abdrücke}

Abdrücke von Siegeln sowie die Stempel- und Rollsiegel an sich können
wie die Keilschrifttexte in vier chronologische Gruppen klassifiziert
werden. Etwa die Hälfte der Funde datiert in die \karumzeit{}, während
ein Viertel der \althetenp{} und ein weiteres Viertel der
\junghetenp{} zugewiesen werden kann (\cref{TableSeals} et
\cref{TableSealsProp}). Interessanterweise ist der Charakter der
erhaltenen Funde zwischen den einzelnen Perioden sehr unterschiedlich:
in der \karumzeit{} wurden vor allem Siegel gefunden, wohingegen in
den beiden hethitischen Perioden meistens Abdrücke dominieren.
Dies weist, wie im Falle der Verteilung der Keilschrifttexte, darauf
hin, dass die administrativen Aktivitäten in der \hetenp{} deutlich
stärker kontrolliert wurden. Die Verteilung zeigt zudem, dass in den Schichten der
\karumzeit{} die Objekte überall verbreitet sind, wohingegen sie sich
in den Grabungsschichten der \althetenp{} um das \gebIT{} konzentrieren
(\cref{GIS-Repartition-sceau-carte}). Dort, in der Nähe der Straße der
jüngeren \hetenp{}, kann vielleicht ein administratives Gebäude
vermutet werden, das den Eingang oder den Warenfluss in der Stadt
kontrollierte.

\subsection{Zeremonielle Objekte}

In dieser Kategorie wurden Fundstücke zusammengefasst,
die weder als Waffe eingesetzt noch für die Produktion
materieller Gegenstände benutzt wurden
(\cref{GIS-Repartition-cultuel,GIS-Repartition-cultuel2}).
Diese Objekte hatten vermutlich einen hohen
symbolischen Wert.

\begin{description}

\item[Tierstatuetten] Diese sehr einfach hergestellten Objekte sind
  überall vertreten und sehr weit verbreitetet.

\item[Stierstatuetten] Innerhalb der Tierstatuetten bilden die
  Stierstatuetten eine besondere Gruppe, die in zwei Untergruppen
  unterteilt werden kann. Einerseits gibt es Stierstatuetten, die
  ungefähr ein fünftel so groß sind wie echte Stiere sowie sehr fein
  ausgearbeitet, bemalt und poliert sind. Daneben bilden kleine und
  deutlich einfacher gestaltete Statuetten eine zweite Gruppe. Es
  konnte aber kein eindeutiger Unterschied in der Verteilung beider
  Gruppen festgestellt werden.

\item[Anthropomorphe Statuetten] Es handelt sich um kleine Statuetten
  einfacher Machart ohne Überzug. Diese sind relativ selten und ohne
  erkennbare Häufungen gleichmäßig verteilt.

\item[Reliefkeramik] Fragmente entsprechender Gefäße sind auf der
  \westt{} weit verbreitet. Reliefkeramik ist erkennbar seltener als
  Stierstatuetten und repräsentierte wahrscheinlich kulturell
  hochwertige Gefäße.

\item[Libationsarme] Diese Gruppe ist die der hethitischen Kleinfunde,
  die am intensivsten erforscht wurde. Libationsarme wurden in der
  Regel aus Varianten der sogenannten ›\emph{red lustrous
  wheel-made-Ware}‹ gefertigt und werden oft mit ›\emph{spindle
  bottles}‹ zusammengefasst. Die Verteilung der Libationsarme
  konzentriert sich auf den Westteil des Tempels. Es ist sehr wahrscheinlich,
  dass die Libationsarme zum \gebRE{} gehören, das vermutlich als
  Becken genutzt wurde.

\item[Gestempelte Scherben] Diese besonderen Scherben wurden vor dem
  Brennen gestempelt, als Teil des Herstellungsprozesses, ähnlich wie
  bei der Reliefkeramik. Sie weisen keine besondere Verteilung auf.

\end{description}

Zusammenfassend lässt sich feststellen, dass zeremonielle Objekte
überall gefunden wurde. Nur die Libationsarme sind einem Gebäude
zuzuordnen. Generell können die zeremoniellen Objekte eine Vielfalt
möglicher Funktionen haben, die von religiösen Ritualen bis zu
aristokratischen Mahlzeiten reichen.


\subsection{Metalle und Textilien}

Unter den Funden von der \westt{} sind Objekte für die Herstellung und
Verarbeitung von Metall- und Textilprodukten sehr gut dokumentiert
(\cref{GIS-Repartition-prodobjet}).


\begin{description}

 \item[Ösennadeln] Die Ösennadeln sind sehr häufige Funde und weit
   verbreitet. Sie können sowohl für die Herstellung oder Bearbeitung
   von Textilien als auch für Lederarbeiten eingesetzt werden.
   Meistens wurden diese Objekte in der Literatur nicht im Detail
   behandelt, weil sie sich über lange Zeiträume typologisch kaum
   verändert haben und daher keine chronologische Relevanz haben. Aus
   der Anzahl der gefundenen Ösennadeln kann man schließen, dass diese
   Objekte zu den meist benutzten Werkzeugen gehören und einen
   typischen Fund in der Stadt darstellen.

\item[Spinnwirtel] Spinnwirtel belegen die Produktion von Garnen und
  Fäden für Textilien, aber auch die Herstellung von Draht. Die
  relativ hohe Häufigkeit dieser Objektgruppe zeigt, dass das Spinnen
  eine verbreitete Aktivität war. Ihr Auftreten deckt sich mit der
  Verbreitung von Ösennadel. Textilarbeit spielte eine wichtige Rolle
  in der \westt{}. Umso verwunderlicher ist die Abwesenheit von
  Gewichten für Webstühle.

\item[Meißel und Pfriem] Diese zwei Objektgruppen, die sich durch die
  Form ihrer Enden unterscheiden (flach oder spitz), konnten für die
  Bearbeitung bzw.\ Überarbeitung von verschiedenen Materialien (u.~a.
  Metall, Holz, Leder oder Textilien) verwendet werden. Diese Objekte
  sind sehr häufig und überall verbreitet.

\item[Gussformen und Barren] Es gibt nur wenige Hinweise auf die
  Verarbeitung von Metallen. Man muss allerdings berücksichtigen, dass
  solche wertvollen Objekte seltener weggeworfen wurden und somit
  potentiell seltener in der materiellen Kultur sichtbar sind.

\end{description}

Anhand des Fundmaterials wird deutlich, dass größere Objekte für die
Metallverarbeitung selten sind, und dass generell eher kleinere
Objekte für die Bearbeitung von Metall- und Textilprodukten genutzt
wurden. Diese kleineren Objekte sind Hinweise auf die Metall- und
Textilproduktion im Bereich der \westt{}.

\subsection{Die Kategorien Waffe, Werkzeug und Zierrat}

Waffen und Werkzeuge sind relativ gleichmäßig verteilt, wobei die
Pfeilspitzen am häufigsten auftreten
(\cref{GIS-Repartition-arme-outil}). In der Kategorie Werkzeug habe
ich Artefakte zusammengestellt, denen keine eindeutige Funktion
zugewiesen werden konnte, aber die generell ›technisch‹ genutzt
wurden. Unter der Kategorie Zierrat sind hauptsächlich Nadeln
vertreten, die überall verteilt waren und wahrscheinlich sehr
häufig genutzt wurden.

\subsection{Subsistenz}

Aktivitäten der Subsistenzwirtschaft nachzugehen ist schwierig. Weder
die Gefäße noch die Mahl- und Reibsteine wurden konsequent
dokumentiert (S. Anhang \ref{MeulesCourante}).

\section{Zusammenfassung}

Die Abschnitte über die Chronologie, die Stratigraphie und die
funktionalen Eigenschaften der Siedlung zeigen unter Berücksichtigung
der Verteilung der beweglichen und unbeweglichen Fundgruppen, dass
eine Rekonstruktion des täglichen Lebens schwierig ist. Zwar können
einige Aspekte in weiteren Studien vertieft werden, wie z.~B.  die
Frage, ob die Gebäude in der Regel ein zweites Stockwerk hatten, oder
die Problematik der Fenster und Türen; dennoch bleiben viele Fragen
wahrscheinlich offen: Welche Hauptaktivitäten gehörten zum
alltäglichen Leben und welche Rolle spielten diese im sozialen
Zusammenleben?

Die Verteilungskarten vermitteln einen guten Eindruck von der auf der
\westt{}  angesiedelten Aktivitäten. Die Funde belegen vor allem
Tätigkeiten, die in den Häusern durchgeführt werden konnten.
Textilverarbeitung ist besonders sichtbar, wohingegen Spuren der
Produktion von Metall eher selten sind.

\subsubsection{Die \karumzeit}

Durch die Keilschrifttexte und Siegel sind administrative Tätigkeiten
im Bereich der \WTer{} gut belegt. In jedem Haus können verschiedene
Aktivitäten nachgewiesen werden, die sowohl Handel als auch die
Produktion von Gütern umfassen.

\subsubsection{Die \hetp}

Das fast vollständige Fehlen von Keilschrifttexten und Siegeln bzw.
Siegelabdrücken in den Wohnbereichen der \WTer{} ist in der \hetenp{}
ein markanter Unterschied zu der vorhergehenden Periode. Werkzeuge, um
Metalle zu verarbeiten, sowie Waffen sind zwar vorhanden, scheinen
aber eine geringere Rolle im Vergleich zu den anderen Aktivitäten
gespielt zu haben, die durch Zierrat und Werkzeuge für die
Textilverarbeitung belegt sind.  Im Allgemeinen scheinen sich die
handwerklichen Aktivitäten auf die Ausbesserung, Veredlung beschränkt
zu haben.

Dieses Ergebnis könnte bei aller Vorsicht der schmalen Befundlage
darauf hinweisen, dass es sich eher um ein Wohnviertel für Bedienstete
handelte und nicht primär für handwerkliche Produktion oder
kommerzielle Zwecke genutzt wurde. Die Nähe zum Tempel, die Qualität
und die Größe der Häuser verstärken diesen Eindruck. Diese
Interpretation ist jedoch problematisch, wenn man die Aufgabe des
Stadtviertels im \xivjh{} berücksichtigt, wodurch diejenigen
Bewohner\_innen weggezogen sind, die für die Verwaltung der Stadt
bzw.{} des Staates sowie für die Durchführung von kultischen
Handlungen verantwortlich waren.

Schriftliche Überlieferungen belegen, dass am Anfang des \xiiijhs{}
(\DEca{} 1295−1274) die Hauptstadt nach \tarhuntassa{} verlegt wurde.  Es
ist zu vermuten, dass die Verwaltung zumindest teilweise ebenfalls
umzog. Dies könnte erklären, warum in der \unterstadt{} wenige Spuren
aus dem \xiiijh{} zu finden sind. Darüber hinaus gibt es bereits aus
dem \xivjh{} verschiedene Hinweise auf politische und militärische
Probleme.  Darunter fallen beispielsweise der Angriff der \kaskaer{}
im Norden, eine kurzzeitige Verlagerung der Hauptstadt nach \samuha{}
(\kayalipinar) sowie generelle Unruhen im ›Land von Hatti‹. Diese
Entwicklungen könnten erklären, warum es kaum archäologischen
Überreste aus dem \xivjh{} gibt. Die Aufgabe verschiedener Tempel im
\xiiijh{} in der \oberstadt{} spricht auch für eine solche
Entwicklung.
