\chapter{Quellen und Methoden}

Die Natur und Art der zur Verfügung stehenden Dokumentation bedingt
die Methode, die man anwenden kann, und so auch die erzielbaren
Ergebnisse einer Untersuchung. Ich möchte daher den Charakter der
verfügbaren Dokumentation und die Methoden beschreiben, die in den
folgenden Kapiteln angewendet werden.  Im ersten Teil wird auf die
Probleme in der Weitergabe der archäologischen Dokumentation
eingegangen sowie auf die Frage, wie die Digitalisierung viele dieser
Probleme beheben kann (\cref{II-1-Repro}).  Der zweite Teil
beschreibt die archäologischen Methoden der
\unterstadt{} (\cref{II-2-Methoden-Feldforschung}), um das Potenzial und die
Grenzen der Interpretation darzustellen
(\cref{II3-doku}). Schließlich erkläre ich, welche
Methoden ich entwickelt habe, um die vorhandene Dokumentation für
meine Interpretation zu nutzen.

\section{Archäologie und Reproduzierbarkeit: eine neue Methodik}
\label{II-1-Repro}

Die Archäologie ist eine Wissenschaft, die auf der Analyse empirischer
Daten aufbaut. Ausgrabungen sind durch die Singularität jeder
Beobachtung gekennzeichnet. Generell sind die Kontexte, in denen die
einzelnen Objekte gefunden wurden, viel wichtiger als diese, so dass
man sich immer darüber im Klaren sein muss, dass bei der Ausführung
einer Ausgrabung das Ziel gleichzeitig erfasst und zerstört wird. 
Daher ist die Dokumentation der Ausgrabung der Ersatz für das
Zerstörte. Eine Ausgrabung kann nur einmal durchgeführt werden, aber
die Interpretationsmöglichkeiten der Dokumentation sind vielfach. Nur wenn diese
Dokumentation zugänglich ist, können Interpretationen überprüft werden.
Arbeiten mit unveröffentlichtem Material wie im Rahmen dieser Arbeit,
zwingt dazu, dieses Material zugänglich zu machen. Aber was bedeutet
›zugänglich machen‹? Wie haben Archäolog\_innen diese Aufgabe bisher
bewerkstelligt und wie haben sich die Forschungsmethoden entwickelt?

\subsection{Dokumentation und Veröffentlichung archäologischer Daten}

Archäologie ist eine junge Wissenschaft, deren Ziele und Methoden sich
im Laufe der Zeit stark geändert haben. Während der letzten Jahrzehnte
ist eine Fülle neuer Herangehensweisen entwickelt worden; dabei
ermöglichen insbesondere naturwissenschaftliche Analysen neue
Perspektiven. Neue Messinstrumente erlauben z.~B. dreidimensional
zu messen oder neue Blickwinkel durch Satellitenbilder und ›\emph{augmented
reality}‹ zu schaffen. Kurzum, jedes Jahr wächst das Volumen von
produzierten und gesammelten Daten. Welchen Einfluss haben diese
Entwicklungen auf die Zugänglichkeit der Daten? Theoretisch gibt es
mit der digitalen Entwicklung fast keine Obergrenze dafür, wie viele
Daten gesammelt und gespeichert werden können. Allerdings hat sich
bisher in der Realität wenig geändert. Für die anatolische Bronzezeit
gibt es fast keine Publikation, die mit \emph{digitalen Daten}
begleitet wird. Projekte, die nicht prinzipiell auf die
Publikation von gedruckten Monographien zielen, sind immer noch
selten. In vielen
Fällen liegt lediglich eine digitale Version des analog gedruckten
Buches als PDF-Datei vor.

\subsection{Die Entwicklung hin zu einer digitalen Archäologie}

Digitale Werkzeuge und Methoden hinterfragen die Möglichkeiten der
Wissensvermittlung, die sich bisher in den Geisteswissenschaften auf
gedrucktem Papier beschränkten. Das Loslösen von der Anfertigung statischer
Seiten erlaubt es, die neuen Möglichkeiten des Internets und der
interaktiven Darstellung zu nutzen. Diese neuen
Herangehensweisen zielen nicht darauf ab, gedruckte Werke zu ersetzen. Vielmehr
sind sie eine wesentliche Bereicherung der Wissensvermittlung und
der Darstellung bzw. Bereitstellung von Rohdaten. Darüber hinaus
vereinfacht eine interaktive Publikation, bei der Daten und Texte
verwoben sind, die Aktualisierung und die Umsetzung in ein analoges
Buch (\cref{DynamicPublicationFormats}).

\subsection{Reproduzierbarkeit in der Archäologie}

Es ist unüblich, die gesamten Daten einer Ausgrabung zu
publizieren. In der Regel wird nur ein Bruchteil davon
veröffentlicht: d.~h. eine Auswahl von Fotografien, Plänen,
Objektlisten, Zeichnungen, beschreibenden und interpretierenden Texten
und ein Literaturverzeichnis. Diese
Tradition hat sich seit über einem Jahrhundert kaum verändert. Die
beschriebenen Möglichkeiten der Digitalisierung erlauben es nun
erstmals, alle Daten zugänglich zu machen (\cref{ResearchPipeline}).
Denn im Allgemeinen gibt es einen großen Unterschied zwischen den
Informationen, die ein\_e Leser\_in zur Verfügung hat, und denjenigen,
die insgesamt erhoben wurden. Nur wenn alle Daten zur Verfügung
stehen,  ist es möglich, die Ergebnisse unabhängig zu prüfen.

In den ›Naturwissenschaften‹ wird  seit etwa zehn Jahren eine
intensive Debatte über die Reproduzierbarkeit der Wissenschaft
geführt. Diese kritisiert, die Veröffentlichung von Ergebnissen ohne die Rohdaten oder in
einer Form, die es nicht erlaubt, diese Daten einfach (sowohl
juristisch als auch praktisch) weiter zu benutzen. Nur wenn die
Daten elektronisch wieder verwendet werden können, können die
Ergebnisse unabhängig überprüft oder mit anderen Daten
zusammengeführt und in übergreifenden Studien weiter analysiert
werden.

\subsection{Ein Goldstandard: literarische digitale Archäologie}

Um die Rohdaten einer Ausgrabung wieder benutzen zu können, ist eine
Kombination von Daten und Text nötigt. Um die Analyse transparent und
nachprüfbar zu machen, müssen auch die Methoden zugänglich sein, mit
denen die Daten analysiert wurden (\cref{RmdWorkflow}). Durch dieses
Vorgehen liegt der Wert einer Studie nicht mehr nur in den
Ergebnissen, sondern auch in der Methode, wie man zu den jeweiligen
Resultaten gekommen ist: darüber hinaus können die Rohdaten weiter
verwendet werden. In den 80er Jahren wurde das sogenannte
\emph{Literate programming} entwickelt, um dieses Problem zu lösen.
Gemeint ist das Schreiben von Computerprogrammen, die nicht nur ein
Ergebnis erzielen, sondern auch für Leser\_innen lesbar und
verständlich sind, so dass transparent und nachvollziehbar ist, wie
(und warum) das Programm in einer bestimmten Weise funktioniert.



\subsection{Die Methodik der vorliegenden Arbeit}

