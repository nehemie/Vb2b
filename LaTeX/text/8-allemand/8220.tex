Im Hinblick auf den Wissenstransfer ist es nicht unbedingt
notwendig, dass jede Person den Programmcode sieht, insbesondere in
einem gedruckten Buch. Um dieses Problem zu umgehen, wurden
Markierungen im Rohtext eingesetzt und dann benutzt, um nur die
Ergebnisse in Form vom Text, Zahl oder Grafik anzuzeigen. Der Code,
die Daten und der Text stehen online zur Verfügung,
ebenso wie alle Dokumente, die ich im Archiv gesichtet habe (dazu
\cref{II3-doku}).

\section{Die Methode der archäologischen Feldforschungen in Boğazköy}
\label{II-2-Methoden-Feldforschung}


Die während einer Ausgrabung entstandene Dokumentation ersetzt die
durch die Ausgrabung verursachte ›Zerstörung‹ der Befunde, die
unwiderruflich ist. Diese Substitution ist einmalig und subjektiv,
denn die im Feld getroffenen Entscheidungen bestimmen, was und wie
dokumentiert wurde.  Die Konzepte und Methoden einer
Ausgrabung spielen deswegen eine wichtige Rolle für den Prozess, auf
dem die späteren Interpretationen basieren. Die Kenntnis der
angewandten Methode ermöglicht es, Verzerrungen aufzuzeigen und so
Fehler zu vermeiden.  Durch Bittel wurden die Ausgrabungen mit einer
Methodik begonnen, die teilweise bis heute wirkt und thematisiert
werden muss. Darauf aufbauend beschreibe ich die Herangehensweise von
Neve, durch die der größte Teil des in dieser Arbeit verwendeten
Materials erarbeitet wurde.


\subsection{Kurt Bittel, Pionier der anatolischen Archäologie}

Von Beginn seiner Arbeit 1931 in Boğazköy an zielte Bittel darauf ab,
eine Chronologie der Keramik aufzubauen. Er legte großen Wert
auf eine stratigraphische Ausgrabung, deren Schwerpunkt auf Büyükkale
lag.  Es wurden Schichten, die mit Architekturresten assoziiert wurden,
von denen getrennt, die nur aus Schutt bestanden. Die Schichten wurden
in ›Bauschichten‹ eingeteilt, die anhand kleinerer Änderungen in
›Bauphasen‹ untergliedert wurden. Technisch gesehen sind die
Ausgrabungen von Bittel durch Pläne und Schnitte dokumentiert worden.
Jeder Kleinfund wurde mit einer Nummer versehen, im jeweiligen
Planquadrat lokalisiert und einer ›Bauschicht‹ stratigraphisch
zugeordnet. Im Vergleich zu anderen Ausgrabungen dieser Zeit war
Bittels Vorgehensweise beispielhaft. Leider ist es aufgrund der
verfügbaren Unterlagen nicht möglich nachzuverfolgen, auf welcher
Grundlage Artefakte einer ›Bauphase‹ zugewiesen wurden. Es muss betont
werden, dass auch Objekte, die nicht auf Fußböden lagen, einzelnen
›Bauphasen‹ zugewiesen wurden. Viele archäologische Arbeiten belegen
heute,
dass es sehr schwierig ist, ohne ein genaues Verständnis der
Stratigraphie, Objekte einer ›Bauphase‹ zuzuweisen. Wenn die Objekte
nicht von einem Fußboden stammen, muss aus heutiger Sicht die
angegebene Schichtzuweisung deshalb als fraglich angesehen werden.

\subsection{Peter Neve, ein wagemutiger Architekt}

1963 übernahm Neve die örtliche Leitung der Ausgrabung und
arbeiteten im Wesentlichen mit den gleichen methodischen Prinzipien
wie Bittel. Der größte
Unterschied zu Bittels System ist die Abweichung von
dem bis dahin etablierten Planquadratsystem. In den früheren Grabungen
wurde ein nach Norden ausgerichtetes in
\SI{10} x \SI{10}{\meter} Quadranten gegliedertes System
benutzt. Dagegen verwendete Neve in der \unterstadt{} nach Nordwesten
ausgerichtete Suchschnitte mit einer eigenen Nummerierung. Diese
Suchschnitte sind nicht gleich groß gewesen und anstatt einer
durchgehenden Nummerierung hat er jedes Jahr mit einem ›Schnitt I‹ in der
\unterstadt{} begonnen. Da es keine Übersicht über die Suchschnitte gibt,
habe ich versucht, dieses System zu rekonstruieren, um die
Kleinfunde einordnen zu können (s. \cref{V-3-Implementierung}).

Zur Beschreibung der Stratigraphie hat Neve nicht nur ›Bauschichten‹
und ›Bauphasen‹ unterschieden, sondern auch stratigraphische
Schichten. Leider sind diese nicht durchgehend dokumentiert und es
gibt fast keine Dokumentation zu den Profilen (\cref{CoupeStratNeve}).
Daher ist es unmöglich nachträglich eine Harris-Matrix zu erstellen.
Die Schichtenangaben enthalten jedoch Informationen über die Fundlage
und diese konnte ich nutzen, um zwischen gesicherten Kontexten (z.~B.
Fußböden) und unsicheren Kontexten (z.~B. Schutt oder Gruben) zu
unterscheiden. Da nur wenige Informationen zur Verfügung stehen und
angesichts der beschriebenden Vorgehensweise während der Grabung,
wurden nur die Objekte, die auf Fußböden lagen, als stratigraphisch
sicher betrachtet. Es ist unmöglich, genauere Fundzusammenhänge für
die übrigen Objekte zu erstellen.

\section{Die zur Verfügung stehende Dokumentation}\label{II3-doku}

Die in den Archiven überlieferte Dokumentation der Grabungen in der
\unterstadt{} ist das Ergebnis der Umsetzung der beschriebenen Methoden
von Bittel und Neve. Im folgenden Abschnitt möchte ich die verfügbaren
Dokumente kurz darstellen, um die Grenzen und Möglichkeiten der
Analysen nachvollziehbar zu machen.

\subsection{Die Tagebücher}

Die Tagebücher geben eine allgemeine Beschreibung des Verlaufs der
Ausgrabungen. Bittel (während der Jahre 1931--1958) und Neve
(1970--1978) haben eine tägliche Zusammenfassung niedergeschrieben.
Einige Skizzen, die Details zeigen, die nirgendwo anders dokumentiert
sind, begleiten die textlichen Beschreibungen
(\cref{Journeaux2fouilles}). Die Tagebücher sind die einzigen
Dokumente, die eine Rekonstruktion erlauben, wo bzw.{} in welcher
Tiefe die Ausgrabungen beendet wurden. Die Tagebücher sind mithin eine
unerlässliche Quelle, um zu verstehen, wo Objekte \DEinsitu{} gefunden
wurden. Die Dokumentation enthält auch Informationen über Funde und Befunde des ersten Jts.{} v.{} Chr.{} und der
ersten Jahrhunderte n. Chr., die in dieser Arbeit nicht weiter
berücksichtigt werden konnten.


\subsection{Die Kleinfundzettel}

Jeder Kleinfund wurde mit einem vorformatierten Zettel dokumentiert
(\cref{FicheInventaire}). Das Format der Kleinfundzettel wurde von 1931 bis
1993 nahezu unverändert beibehalten. Das einheitliche Format der Formulare
ermöglicht, dass vergleichbare Informationen für alle Objekte
festgehalten wurden. Für einige Objekte sind diese Zettel die einzige
Informationsquelle, da sie sonst nirgendwo erwähnt werden. Die
Hauptinformationen jedes Fundzettels (außer der Beschreibung) habe ich
in eine Datenbank \BoFuWTer{} aufgenommen (s. Anhang \ref{981-BDD-KF}).


\subsection{Fotografien}

Die Fotografien dokumentieren Ansichten der Befunde und der Verbindung
zwischen einzelnen Funden und Befunden. Die Fotografien geben zudem
auch einen guten Eindruck des Verlaufs der Grabungen und nicht nur
deren Endzustand, der auf den Plänen dokumentiert wurde
(\cref{PlancheContact}). Die Bilder wurden durchnummeriert und
gelistet, aber häufig fehlen Beschreibungen. Fototafeln wurden damals
nicht benutzt, so dass es schwierig ist, einige Bilder einzuordnen.

\subsection{Die Bauaufnahmen}

Bauaufnahmen im Maßstab 1~:~100 bilden die Hauptinformationsquelle über
die Architektur. Fast alle Steinpläne, die vor 1956 von Naumann
gezeichnet wurden, sind publiziert. Die bisher unveröffentlichten
steingerechten Pläne der \unterstadt{} von Neve enthalten wesentliche
Hinweise über den Zustand der Grabung, da Neve sehr viele Annotationen
gemacht hat.  Diese Bauaufnahmen bilden allerdings jeweils nur den letzten
Zustand der Grabung ab. Da jüngere Mauern der bronzezeitlichen
Schichten fast nie abgetragen wurden, wurden regelmäßig alle Phasen auf
ein Blatt gezeichnet. Es gibt zwar viele Höhenangaben auf den Plänen,
dabei handelt es sich aber vor allem um die Niveaus der Oberkanten der
Mauern; nur gelegentlich wurden die Höhen der Unterkanten eingetragen. Es ist deshalb manchmal schwer zu
unterscheiden, ob sich die Messung auf die Unterkante einer Mauer oder den
tiefsten Punkt der Grabung bezieht. Neve hatte getuschte Pläne der
\unterstadt{} für eine Publikation vorbereitet. Diese sind in drei
Bauschichten untergliedert.  Diese Pläne und die Bauaufnahmen bilden die
Grundlage des IV.{} Kapitels. Sie wurden jedoch neu bewertet, so dass
die Interpretationen nicht unbedingt der Bewertung von Neve folgen.

\subsection{Die gezeichneten Profile}

Für die Ausgrabungen vor 1956 in der \unterstadt{} ist nur ein
einziges Profil dokumentiert worden, das bisher nicht veröffentlicht wurde.
Während der Arbeiten in den 1970er-Jahren wurden nur sehr wenige
Profile aufgenommen, die alle im III.{} Kapitel über die Chronologie
berücksichtigt werden.

\subsection{Die Vorberichte}

Die ›wichtigsten‹ Informationen sind in den Vorberichten
veröffentlicht worden, die immer zeitnah nach den Kampagnen erschienen sind. Diese geben
einen guten Eindruck von den Ausgrabungen und sind reich bebildert.
Alle Vorberichte haben eine ähnliche Struktur; der erste Teil
behandelt die Befunde, ein zweiter die Funde und ein dritter die
Textfunde, falls vorhanden.


\subsection{Die Monographien}

Einzelne Fundgruppen aus der \unterstadt{} wurden in monographischen
›Endpublikationen‹ berücksichtigt, die jeweils einzelne Objektgruppen
separat behandeln. Die Keramik der Grabungskampagne 1937--1957 wurde
von Fischer 1963 vorgelegt. Diese Studie basiert weitgehend auf
vollständigen Gefäßen oder gut erhaltenen Gefäßprofilen. Sie stellt einen
guten Formenkatalog dar. Dieses gilt genauso für das
1972 veröffentlichte Buch von Boehmer über die Kleinfunde, das auch
die Kampagnen  1937--1957 berücksichtigt. Von den Ausgrabungen
1970--1977 wurden die Kleinfunde, abegesehen von denen aus gebranntem
Ton, ebenfalls von Boehmer im Jahre 1979 veröffentlicht. 1983 wurden
die Reliefkeramik von Boehmer und 1987 die Siegel von Boehmer und
Güterbock vorgelegt.  Eine archäozoologische Studie über die
Tierknochen der Kampagnen 1975−1976 publizierten von den Driesch und
Boessneck 1981.  Darüber hinaus erschienen einige punktuelle Arbeiten
zu besonderen Funden und Befunden in der Boğazköy-Ḫattuša Reihe.

Diese Publikationen behandeln die verschiedenen Objekt- und Materialgruppen
jeweils für sich und versuchen, diese chronologisch, historisch und
kulturgeschichtlich einzuordnen. Die Publikationen geben bezüglich der
Fundkontexte nur die Angaben wieder, die bei der Kleinfundregistratur
dokumentiert wurden.  Aus heutiger Sicht muss man feststellen, dass
eine Trennung zwischen gesicherten und unsicheren Fundzusammenhängen
unmöglich ist, wenn genauere Angaben wie z.~B. ›Schicht 3‹ fehlen.
Diese Umstände sind  zu beachten, wenn man die in den Publikationen
genannten Datierungen und Schichtzuordnungen nutzt. Wie belastbar
sind diese? Wenn man nur die Objekte berücksichtigt, deren
chronologische Einordnung als sicher angenommen werden kann (z.~B.
Funde auf Fußböden), reduziert sich die Fundmenge deutlich, deshalb
muss diesen Objekten ein höheres Gewicht für Datierungsvorschläge
beigemessen werden (s. Anhang \ref{981-BDD-KF}).

\subsection{Der Zugang zum Material}

\subsubsection{Das Archiv}

Alle Dokumente über die Ausgrabungen der \unterstadt{} werden in den
Archiven des DAI in Berlin und Istanbul aufbewahrt.

\subsubsection{Archäologisches Material}

% Das Jahr 2012 war ein Wendepunkt in der Organisation der dieses
% Buches zugrundeliegenden Dissertation. Von einer Seite, da keine
% Ausgrabung stattfinden konnte, hatte der Autor die Gelegenheit ein
% Team zu bilden, um die Keramik von den Ausgrabungen der siebziger
% aufzunehmen.  Anderseits ist es eindeutig geworden, dass diese
% Untersuchung so konzipieren sein sollte, als es keine Gelegenheit
% mehr geben würde, neues Material aufzunehmen, da der Werdegang
% dieser Arbeit nicht auf unvorhersehbarer Genehmigungerteilung
% basieren konnte.
Der Autor hat von 2009 bis 2013 an den Ausgrabungen des DAI in
Boğazköy teilgenommen. 2012 hatte er die Gelegenheit ein Team
zu bilden, um die Keramik der Ausgrabungen der 1970er-Jahre vor dem
Hintergrund einer kritischen Bewertung der Befunde aufzunehmen.


\subsection{Verwendung der Dokumentation}

Die vorliegende Arbeit beruht auf den Publikationen und berücksichtigt
alle weiteren vorhandenen Dokumente, um alle Materialien in ihrem
Zusammenhang zu sehen und insgesamt neu zu bewerten.


\section{Daten sichtbar machen: von der Dokumentation zur Interpretation}

\subsection{Die Werkzeuge: Datenbanken und GIS}

Die Heterogenität der Dokumentation und die unterschiedliche Qualität,
Quantität und Natur jedes einzelnen Dokuments ermöglichen es,
verschiedene Aspekte zu beleuchten und verschiedene Untersuchungen
durchzuführen. Um die Wiederverwendung dieses Materials zu
erleichtern, habe ich die Kleinfundzettel in einer Datenbank gelistet
(\BoFuWTer, \cref{981-BDD-KF}); eine weitere Datenbank wurde für die
Keramik erstellt (\BoKeWTer, \cref{982-BDD-Kera}) und in einer dritten
Datenbank wurden die Keramikfundeinheiten in Kontexten
zusammengefasst (\BoKeWTerKontexte, \cref{983-BDD-Kontexte}).

Ein wichtiger Teil der Arbeit wurde mit einem GIS (Geografisches
Information System) ausgeführt, um die Pläne und Karten zu erstellen.
In der Regel wird in archäologischen Publikationen nicht weiter
thematisiert, wie die Pläne und Karten herstellt worden sind. Dennoch
ist der Akt der Planerstellung nicht unproblematisch, weshalb die
angewandte Methodik im Folgenden dargestellt wird.

\subsection{Die Architektur: von der Aufnahme zum Plan}\label{interpretation-architektur}

Die Erstellung von Karten und Plänen stellt bereits eine
Interpretation und eine Analyse der Architektur dar. Soweit möglich,
muss man die Zirkulation innerhalb eines Gebäudes beachten, um zu
verstehen, welche Räume miteinander in Verbindung standen und eine
Einheit bildeten. In einem zweiten Schritt müssen die in situ Funde
berücksichtigt und mit den gewonnenen Plänen verknüpft
werden, um Rückschlüsse ziehen zu können. Aber wie kann ein Gebäude
abgegrenzt werden, wenn nur die Fundamente und keine Begehungsfläche
erhalten sind? − Eine sehr häufig in der \unterstadt{} dokumentierte
Situation. Um die Beschreibung der Gebäude homogen vorzunehmen, bin
ich folgendermaßen vorgegangen:

\begin{enumerate}%[nolistsep]


  \item Zuerst wird eine Baueinheit bestimmt. Diese besteht aus allen
    zusammengehörenden Mauern, die anhand folgender Kriterien
    identifiziert werden:

    \begin{itemize}[noitemsep,nolistsep,topsep=0pt, partopsep=0pt]
      \item Nebeneinander liegende Mauern gehören zu verschiedenen Baueinheiten;
      \item Ineinander einbindende Mauern einer Baueinheit haben die gleiche Breite;
      \item Innerhalb einer Baueinheit ist die Bauweise ähnlich;
      \item Die Orientierung hilft, verschiedene Baueinheiten zu unterscheiden
  \end{itemize}

  \item Ein Gebäude besteht aus verschiedenen Baueinheiten, die
    miteinander kommunizieren:

    \begin{itemize}[noitemsep,nolistsep,topsep=0pt, partopsep=0pt]
      \item Ein Gebäude muss von einer Straße zugänglich sein;
      \item Wenn ein Gebäude aus verschiedenen Einheiten besteht, können diese in
        einer relativen Chronologie zueinander eingeordnet werden;
      \item Die Gebäudetypologie kann helfen, ein Gebäude ein- oder abzugrenzen.
		\end{itemize}

 \end{enumerate}

 \subsubsection{Einige Beispiele}

 Das \gebU{} wird als Beispiel genommen, um die Umsetzung dieser
 Vorgehensweise zu illustrieren. Das Bauwerk wurde 1956 freigelegt und es
 stehen kaum  Informationen zur Verfügung. Der Steinplan von Naumann
 wurde von Neve vervollständigt und zeigt drei Baueinheiten
 (\cref{MethGeb7}). Die zweite Einheit wurde an die Erste angebaut und
 diese beiden wurden im Norden durch eine Dritte ergänzt. Neve hatte
 dagegen eine andere Unterteilung vorgeschlagen (\cref{Geb7Neve}), die
 jedoch nicht begründet wurde. Den Fotos und Plänen nach zu urteilen, wurde kein
 Zugang und keine Tür nachgewiesen, so dass die Zirkulation innerhalb
 des Gebäudes nicht
 rekonstruiert werden kann; eine sichere Abgrenzung des Bauwerks ist
 deshalb unmöglich. Das Protokoll ermöglicht dennoch die
 Rekonstruktion der Entwicklung.

 Das \gebTI{} im \nordv{} dient als zweites Beispiel. Es wurde 1938
 freigelegt und Naumann hat insgesamt sieben verschiedene Gebäude
 identifiziert. Eine neue Betrachtung des \gebesTI{} gemäß des
 beschriebenen Vorgehens erlaubt allerdings auch eine andere
 Interpretation (\cref{Geb58d}): meiner Meinung nach, wurde die Mauer
 zwischen den Baueinheiten 1 und 2 im Laufe der Nutzungszeit
 neu orientiert. Naumann und Neve identifizieren in diesen zwei
 Baueinheiten drei Gebäude, wohingegen ich vorschlage, in den
 Bauresten ein sich entwickelndes Gebäude zu sehen. Durch die
 Anwendung des Protokolls wird zudem deutlicher, wie das
 \gebTP{} wegen dieser Änderungen reduziert werden musste.

 Dieses Protokoll zur Identifizierung der einzelnen Baueinheiten trägt
 nicht zur Erhellung funktionaler Aspekte bei, aber es
 vermittelt erstmals einen differenzierten Eindruck der
 chronologischen Entwicklung und deren Dynamik in der \unterstadt{}.

 \subsubsection{Kolorierung}

 Um die verschiedenen Einheiten darzustellen, wurden Farben vom Set
 \emph{Accent} des Service \emph{Color Brewer} ausgewählt. Schraffuren
 stellen Mauern dar, die nur vermutet werden können. Wenn die Lücken
 zu groß sind, wurde nur der Anfang der Mauer schraffiert, um die
 Ausrichtung des Verlaufes anzudeuten.

\subsection{Ein Ausblick}

Dieses Kapitel hat die Methoden und die Quellen vorgestellt, die für
die Analysen und Interpretationen in den folgenden Kapiteln verwendet
werden. Das nächste Kapitel zeigt, wie ich diese benutze, um
eine Chronologie zu rekonstruieren; das IV.{} Kapitel fasst die Befunde
zusammen, um einen Überblick über die Besiedlung zu schaffen.
