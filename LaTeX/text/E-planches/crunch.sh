#!/bin/bash

SOURCE="../../../../Vb2b-Rmd-Supplement/Plan"
DEST="../../planches/"

MATCH=(NEVE-WTer-TuschPlan-Schicht-3-b NEVE-WTer-TuschPlan-Schicht-3-a
  NEVE-WTer-TuschPlan-Schicht-2b NEVE-WTer-TuschPlan-Schicht-2a
  NEVE-WTer-TuschPlan-Karum-b NEVE-WTer-TuschPlan-Karum-a
  NEVE-WTer-Schichten-b NEVE-WTer-Schichten-a)
MATCH=("${MATCH[@]/#/$SOURCE/}")

for f in \
  ${MATCH[@]}\
  ; do
  convert -resize 1000x  "${f}.tif" "${f}.jpg"
  mv "${f}.jpg" $DEST
done

