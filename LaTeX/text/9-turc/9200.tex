\chapter{Kaynaklar ve Yöntemler}

Mevcut arkeolojik belgelerin niteliği ve türü,
uygulanan yönteme ve araştırmanın hedeflenen
sonuçlarına yön verir. Bu nedenle eldeki belge ve bir
sonraki bölümlerde kullanılan yöntemin özelliklerinin
açıklaması şöyledir: Birinci alt bölümde arkeolojik
belgenin sunulması ile ilgili problemler ve bu
belgelerin dijital ortama aktarılmasıyla nasıl
çözebileceği sorusu ele alınmaktadır
(\cref{ii-1-Repro}). İkinci alt bölümde
\unterstadt{}'de uygulanan arkeolojik yöntemler
açıklanmaktadır (\cref{ii-2-Metod}) ve bu bölüme bağlı
olarak değerlendirmenin potansiyeli ve sınırları
verilmiştir (\cref{ii-3-doku}). Sonuç olarak ise,
mevcut belgeleri değerlendirmek için geliştirdiğim
yöntemler açıklanmaktadır.

\section{Arkeoloji ve Reprodüksiyon: Yeni bir Yöntem}
\label{ii-1-Repro}

Arkeoloji, ampirik verilerin analizi üzerine kurulmuş
bir bilimdir. Kazılar her bir gözlemin özgünlüğü ile
karakterize edilir. Küçük buluntuların ele geçirildiği
konteksler, sözkonusu gözlemlerden genellikle daha
önemlidir. Öyle ki, kazı faaliyetinin bir yandan hedefe
ulaştırdığı fakat diğer yandan bir tahribat olduğu
unutulmamalıdır. Bu nedenle bir kazıda arkeolojik
belgeleme, tahrip edilen şeyin yerini alır. Bir kazı
bir kereliktir fakat belgeleme birçok defa
değerlendirilebilir. Fakat bu değerlendirmelerin
doğruluk derecesini kontrol etmek, belgelemenin
yalnızca erişime açık olmasıyla mümkündür. Bu
çalışmadaki gibi, yayımlanmamış malzeme ile çalışmak,
malzemeyi erişime açmaya zorlamaktadır. Fakat erişime
açmak ne anlama gelir? Arkeologlar malzemeyi bugüne dek
nasıl erişime açtılar ve araştırma yöntemleri bugüne
dek nasıl gelişti?

\subsection{Yayımlanmamış Malzemenin Kaydedilmesi}

Arkeoloji, amaç ve yöntemleri zaman içinde oldukça
değişmiş yeni bir bilimdir. Geçtiğimiz on yıllar
boyunca çok sayıda yeni yaklaşımlar geliştirilmiştir ve
özellikle fen bilim dallarının analizleri yeni bakış
açılarına olanak sağlamaktadır. Yeni ölçüm araçları, üç
boyutlu ölçüm ya da satelit resimleriyle yeni görüş ve
›\emph{augmented reality}‹  edinmeye izin vermektedir.
Kısacası, elde edilen verilerin hacmi her yıl
büyümektedir. Bu gelişimlerin veri erişimine etkileri
nelerdir? Dijital gelişimlerde, toplanan ve kaydedilen
veri sayısında – teorik olarak – hemen hemen bir üst
sınır yoktur ve bu durum şimdiye kadar pek
değişmemiştir. Anadolu'nun Tunç Çağı ile ilgili,
dijital verilerin eşlik ettiği yayınların sayısı yok
denecek kadar azdır. Esas olarak basılmış monografi
nitelikli yayın amacı taşımayan projeler hâlâ çok
azdır. Analog basımlı kitabın çoğunlukla sadece Pdf
formatında dijital kopyası bulunmaktadır.

\subsection{Dijital Arkeolojiye Doğru Gelişim}

Dijital araç ve yöntemler, sosyal bilimler alanında
bugüne dek kağıda basılan bilgileri aktarma olanakları
üzerinde durmaktadır. Durağan sayfalar yapmayı bırakmak
internetin yeni imkanlarını ve interaktif sunumu
kullanmanın yolunu açmaktadır. Bu yeni yaklaşımların
amacı basılmış eserlerin yerini almak değildir; daha
çok bilgi aktarımı ve ham verilerin hazırlanması ya da
sunumunda önemli bir zenginliktir. Ayrıca verilerle
metinlerin iç içe geçtiği bir interaktif yayın,
güncellemeyi ve analog kitaba aktarmayı
kolaylaştırmaktadır (\cref{DynamicPublicationFormats}).

\subsection{Arkeolojide Reprodüksiyon}

Arkeolojik kazının tüm verilerini yayımlamak olağan bir
durum değildir ve genel olarak bu verilerin sadece
küçük bir bölümü yayımlanmaktadır. Bunlar seçilmiş
fotoğraf, plan, buluntu listeleri ve çizimler, açıklama
ve yoruma dayalı metin ve kaynakçadır. Bu gelenek yüz
yılı aşkın bir süredir neredeyse hiç değişmemiştir.
Dijital ortamın sunduğu olanaklar, ilk olarak tüm
verilere erişimi mümkün kılmaktadır
(\cref{ResearchPipeline}). Zira okuyucunun sahip olduğu
veri ile tümü bir araya toplanmış veri arasında büyük
fark bulunmaktadır. Ancak tüm verilerin erişime açık
olması durumunda, sonuçlar bağımsız biçimde kontrol
edilebilmektedir.

Fen bilimlerinde son on yıldır bilimde reprodüksiyon
(tekrarlama) tartışması sürmektedir. Bu tartışmada, sonuçları ham
veriler olmaksızın ya da izinsiz kullanılması (hukuki
ve uygulamada) eleştirilmektedir.  Yalnızca veriler
elektronik ortamda kullanılabildiğinde, sonuçlar
bağımsız olarak denetlenebilir, diğer verilerle bir
araya getirilebilir ve kapsamlı çalışmalarda yeniden
analiz edilebilir.

\subsection{Altın Standard: Yazınsal Dijital Arkeoloji}

Bir kazının ham verilerini yeniden kullanabilmek için
veri ve metin kombinasyonu gereklidir. Veri analizini
şeffaf ve kontrol edilebilir kılmak için, analiz
yöntemi de erişilebilir olmalıdır (\cref{RmdWorkflow}).
Bu şekilde, bir çalışmanın niteliği sadece sonuçlarında
değil aynı zamanda bu sonuçlara ulaşmada kullanılan
yöntemlere de dayanır. Ayrıca ham veriler yeniden
kullanılabilir. Bu sorunu çözmek amacıyla 1980'li
yıllarda \emph{Literate programming} adlı bir program
geliştirilmiştir. Bu, sadece sonuç hedefli değildir
aynı zamanda neden ve nasıl çalıştığı konusunda okuyucu
için okunabilir, anlaşılır ve şeffaf bir program
yazılımıdır.



\subsection{Çalışmada Uygulanan Yöntem }

