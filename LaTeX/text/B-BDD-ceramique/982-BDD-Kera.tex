L'objectif de l'analyse céramique a été, dans le cadre de ce travail,
volontairement limité à la définition d'un canevas chronologique. Il est évident
que le corpus céramique se prête à de nombreuses autres analyses, orientées vers
des questions d'avantage socio-économiques. Le corpus présenté ici est donc
partiellement exploité, mais sa mise à disposition doit permettre à chacun, y
compris son auteur, de réutiliser les données pour poursuivre l'analyse.

Cette annexe explicite les méthodes de travail, sa réalisation, et le résultat,
c'est-à-dire la base de données. L'\cref{Ana-Kera} présente l'étude
chronologique de la céramique.


\section{Méthode}

Pour dater la céramique, j'ai exploité les résultats des recherches sur
l'évolution de la céramique à \bo{} entreprises par
\Schoop\footnote{\cite{Schoop2003,Schoop2006a,Schoop2009a}.}. Sans sa
présence à mes côtés en 2012, ce travail n'aurait pas pu être réalisé sous cette
forme.  Dès 2012, \Schoop{} m'a transmis les données résumées des résultats de
ses recherches, ce qui m'a donné l'occasion de les intégrer à mes données.
D'un autre côté, l'absence d'une publication définitive et de la mise à
disposition des données primaires ne m'a pas permis de sortir du cadre qu'il a
délimité dans ses articles pour étudier la chronologie à \bogazkoy.




\subsection{Choix des échantillons}

La méthode développée par \Schoop{} nécessite d'étudier de larges ensembles
céramiques afin d'avoir un nombre d'individus suffisamment important pour éviter
d'obtenir un résultat biaisé lors d'une analyse quantitative.
L'analyse se fonde
sur des ensembles non-fonctionnels et, plus le corpus est large, plus les biais
issus de l'échantillonnage sont réduits.  De plus, il faut veiller à ce que le
matériel soit contemporain et provienne d'un ensemble homogène.


Ces conditions réduisent les possibilités d'étude du matériel. Le matériel qui a
pu être consulté se trouvait soit dans le dépôt de la mission archéologique à
\bo{}, soit au musée de \bo\footnote{Lors des campagnes d'étude céramique, du
matériel qui ne provient pas de la \WTer{} a aussi été étudié (par exemple
\cite{Strupler2013b}), mais il n'est pas traité dans ce travail, \infra{}
\cref{b12-collecte-donnees}.}. En ce qui concerne la \WTer{}, il n'existe pas
d'ensemble conservé antérieur aux années 1970 qui soit suffisamment grand pour
se prêter à une analyse quantitative. C'est uniquement à partir des années 1970
que des collections de matériel céramique assez étoffées ont été prélevées et
inventoriées. Pour ces contextes, seuls les tessons diagnostiques ont été
conservés et il n'existe aucune documentation sur les fragments
nondiagnostiques. Le nombre de contextes et la quantité totale de
céramique mise au jour sont impossibles à reconstruire.

Pour la \WTer{}, il a été difficile de définir des ensembles suffisamment grands
et dont la stratigraphie était assurée. Étant donné qu'il n'y a  presque pas de
relevé stratigraphique, seuls certains contextes ont été sélectionnés, pour
lesquels l'architecture est bien conservée et dont les installations tout comme
la documentation archéologique assurent que les sols d'occupation ont été
repérés\footnote{Sur les méthodes de fouille voir \supra{}
\cref{222-Methode-Neve} et la liste des contextes présentée \infra{}
en \cref{983-BDD-Kontexte}.}. Tous les ensembles analysés se trouvaient directement
sur des sols d'occupation\footnote{Même si j'ai tenté de n'étudier que des
ensembles provenant de sols, lors de l'étude post-fouille, certaines
incohérences m'ont poussé à ne pas analyser des ensembles. Le nombre de
contextes analysés diffère donc du nombre de contextes enregistrés dans la base
de données.}.


\subsection{Collecte des données}
\label{b12-collecte-donnees}

L'étude de la céramique a débuté à petite échelle en 2010 et a été poursuivie en
équipe en 2012 et 2013\footnote{Voir également \cref{238-Acces-Materiel}.}. Pour
faciliter l'intégration des jeux de données, notamment avec le travail de
\Schoop{} et les données de la mission archéologique de \bo, la base de données
a été conçue en allemand\footnote{Pour les termes, je me suis largement inspiré
de l'article de \cite{Schneider1989}.}. La base de données a été initialement
réalisée avec FileMaker Pro 11, ce qui s'est avéré être un très mauvais choix,
puisqu'il s'agit d'un logiciel privateur au format très contraignant.
L'essentiel du travail a ensuite été géré avec le logiciel libre \emph{R}.

La base de données a été conçue avec des variables indépendantes et j'ai
cherché à ne pas avoir trop de catégories. La typologie provient du travail de
\Schoop{} (\cref{BDD-Cera-Typo}). Le matériel a directement été enregistré dans
la base de données et chaque individu diagnostique a été renseigné dans la base
de données grâce à un identifiant unique (\textbf{IDKera}), qui a également été
inscrit à l'encre sur le tesson original.  L'expression \emph{individu} fait
référence au terme statistique et désigne un fragment ou plusieurs fragments
jointifs. Une étape de remontage de la céramique a précédé systématiquement
l'attribution des identifiants.

En 2010--2011, j'ai travaillé seul sur la céramique \delaperiodeCAC{} mise au jour en
2010--2011 à \knw{}. La céramique des années 1970 a été étudiée en 2012 et
enregistrée dans la base de données \BoKeWTer{} (1884 tessons diagnostiques),
fruit d'un travail collectif d'\Egbers{}, \Gocmez{}, \Strupler{} et \Wittmann{}.
Cette même année, avec la même équipe, un ensemble provenant d'un sondage de la
ville haute à \tvs{} (\BoKeSondageTvS{}, 409 tessons diagnostiques) et un corpus
issu des sondages menés par \Dittmann{} et \Huh{} dans la \sudareal{} en
2010--2011 (\BoKeSuda{}, 467 tessons diagnostiques) ont été analysés et intégrés
dans la même base de données\footnote{Ces deux derniers jeux de données  sont à
paraître en collaboration avec \Schachner.}. La céramique du sondage sous le
\gebOQ{}, qui représente une séquence unique allant de 1800 à 1500 \avne{}, a été
étudiée en 2013 par \Beckmann{} et \Strupler{} et agrégée aux données collectées
par \Strupler{} en 2010--2011. Cette base de données, \BoKeKNW{} (1318+2215
tessons diagnostiques), n'est pas étudiée ici et doit faire l'objet d'une étude
ultérieure.

Une fois le travail de terrain terminé, toutes les données brutes ont été
exportées dans un fichier texte sous forme tabulaire \BoKeStruplerUnclean. Pour
exclure et corriger les résultats incohérents, les fautes de frappe, les
combinaisons impossibles, les clics maladroits et les réponses erronées, j'ai
écrit le script \BoKeStruplerCleaning{}. Ce premier traitement de données permet
de passer des données brutes aux données primaires analysables et a été appliqué
aux différents jeux de données avant de les isoler (\cref{BDD-Kera-Databases}).


\opt{optbw}{
 \begin{figure}[h!]
   \centering
   \begin{minipage}[]{0.95\linewidth}
   \centering
     \includegraphics[width=130mm]{Fig_79}
     \caption{Schéma de la création des différents jeux de données. Toutes les
          données sur la céramique ont d'abord été centralisées dans le fichier
          \BoKeStruplerUnclean{}, qui ont ensuite été préparées et
          homogénéisées avec le script \BoKeStruplerCleaning. Enfin, les données ont été
          séparées en fonction de leur publication : \BoKeWTer{}, analysées dans
          ce manuscrit, et, à paraître, les jeux de données  \BoKeSondageTvS{},
          \BoKeSuda{}, \BoKeKNW{}. Les icônes du schéma sont  de Everaldo Coelho
        (licence  LGPL)  }
     \label{BDD-Kera-Databases}
   \end{minipage}
 \end{figure}
 }


En ce qui concerne la céramique de la \WTer{}, 1884 individus des fouilles des
années 1970 ont été enregistrés dans la base de données, dont environ 1136 ont
été dessinés. De nombreux tessons ont été dessinés pour relever des données
morphologiques du vase, qui sont difficilement accessibles
directement, comme,
par exemple, l'angle de l'inclinaison de l'ouverture. Toutes les formes dont la
classification pouvait poser problème ont également été dessinées. J'ai assisté
à tout le processus de collection des données et j'ai formé tous les
\etudiantEs{} avec qui j'ai collaboré. Par conséquent, je considère que les
données sont homogènes.

\section{Description de la base de données}
