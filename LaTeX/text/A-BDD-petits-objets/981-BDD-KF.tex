Lors des fouilles de \bogazkoy, un registre des objets inventoriés a été créé
sous la forme de fiches individuelles (\cref{FicheInventaire},
\cpageref{FicheInventaire} et voir la  \cref{232-Registre-objets}).
Chaque fiche du registre renseigne sept variables : le numéro d'inventaire
(\textbf{Inv. Nr.}), la date (\textbf{Datum}), les numéros des photographies
correspondantes (\textbf{Photo Nr.}), le lieu de découverte (\textbf{Fundort}),
une description de l'objet (\textbf{Gegenstand}), les données métriques
(\textbf{Masse}) et la nature des matériaux (\textbf{Material}), selon un
principe qui n'a presque pas évolué jusqu'en 1991. Toutes les fiches
correspondantes aux petits objets découverts à la \WTer{} et au \NoV{} ont été
renseignées dans la base de données \BoFuWTer{}.

\section{Mise en œuvre}
\label{981-BDD-KF-MisEnOeuvre}

Les informations ont été extraites une à une des fiches, mais seule une partie
des informations a été enregistrée. Étant donné que le cœur de cette
recherche n'est pas une publication des petits objets, mais l'étude du quartier,
seules les informations essentielles, en raison de contraintes temporelles, ont
pu être considérées. Par exemple, la description n'a pas été recopiée. Dans le
cadre de futures recherches, de nombreuses informations pourront compléter et
corriger cette liste, selon la méthode \textit{Publish and Push}\footnote{\label{PublishAndPush}\cite{Kansa2014} et voir
\cref{212-archeo-numerique}.}.

L'uniformité des fiches a largement facilité la numérisation et
l'intégration dans une base de
données. Néanmoins, les informations n'ont pas été numérisées selon les sept
variables des fiches, puisque, pour optimiser la base de données,
ces informations ont été « découpées ». Les données ont été enregistrées en
allemand, pour rester le plus près possible de la documentation primaire et
simplifier le réemploi des données au sein de la mission germanophone.
\Cref{ListeAttributeObjet} indique les traductions employées pour les objets.
Les variables de la base de données \BoFuWTer{} sont les suivantes :

\begin{description}

    \item[Année :] Année de la fouille. Seuls les deux derniers chiffres d'une
    année sont renseignés («~71~» pour 1971)

  \item[Numéro :] Numéro unique (par année) attribué à chaque objet (tel que 316)

    \item[Num.A :] Lettre employée pour subdiviser les \textbf{Numéros}
      identiques. Les lettres ont été par exemple employées dans le cas des
      tombes, pour lesquelles tous les objets ont reçu le même numéro (par exemple 71-316a, 71-316b,
      71-316c)

    \item[Matériau :] Sur le terrain, l'identification du matériau de chaque objet est
      souvent problématique, en particulier en ce qui concerne les
      métaux et les pierres. J'ai repris les désignations telles quelles, comme
      or, plomb ou fer.
      % et en remplaçant simplement \textit{bronze} par \textit{alliage cuivreux}.
      La nature des pierres n'a pas été précisée.

    \item[Objet :] voir \infra{}

    \item[Stratigraphie :] Distinction du contexte de l'objet : \insitu, fermé ou
      ouvert.

    \item[Strate :] Précision, si elle existe, sur la \textbf{stratigraphie} (numéro de la tombe,
      trouvaille de surface)

    \item[Année.Tranchée :] Année de la tranchée de fouille (comme 1954)

    \item[Tranchée :] Dénomination de la tranchée souvent en chiffres
      romains. Les
      dénominations ayant été réemployées au fil des campagnes, elles sont à
      combiner avec la variable \textbf{Année.Tranchée} pour être
      uniques et différencier la tranchée \citfr{I} de 1975 et celle
      de 1976.

    \item[Carreau :] Subdivision de la tranchée en chiffres romains ou bien
      intervalle de distances (entre 0 et 5 m)

    \item[TC.1 :] Premier degré de précision de la variable \textbf{Carreau}

    \item[TC.2 :] Deuxième degré de précision de la variable \textbf{Carreau}

    \item[TC.3 :] Troisième degré de précision de la variable \textbf{Carreau}

    \item[Bx.Px :] Référence au bâtiment ou à la pièce où un objet a été
    découvert. Par exemple «~B13.P3~» renvoie à la Pièce 3 du Bâtiment 13

  \item[Rq :] Remarques


    \item[Datation :] Variable renseignée lorsque le matériel ne datait pas, à
      l'évidence, \delaph{} (par exemple, pour les monnaies ou fibules).

    \item[Publié :] Indication précisant si l'objet est publié ou inédit

    \item[Publication :] Référence à la publication (comme Boehmer 1979)

    \item[Catalogue :] Numéro du catalogue de la \textbf{Publication} (par exemple
      : 3701)

    \item[Cat.A :] Subdivision du numéro de \textbf{Catalogue} (tel que : a)

    \item[Musée :] Si précisé, le lieu où est conservé l'objet

    \item[ID :] Identifiant unique pour chaque objet, composé de
      \textit{Bo\textbf{Année}-\textbf{Numéro} \textbf{Num.A}} (comme : Bo38-219a)

    \item[Réf. :] Identifiant unique pour la publication, composé de
      \textit{\textbf{Publication}, \textbf{Catalogue} \textbf{Cat.A}}
       (par exemple : Boehmer 1979, 3701a)

     \item[Car :] Identifiant unique pour chaque carreau du système de carroyage
       (voir \supra{} \cref{Definir-carroyage}),
      composé de  \\
      \textit{\textbf{Année.Tranchée} . \textbf{Tranchée} . \textbf{Carreau} .
      \textbf{TC.1} . \textbf{TC.2} . \textbf{TC.3}} (tel que : 73.I.9.NO-Erweiterung)


\end{description}

\paragraph{Objets}

Les objets ont tout d'abord été décrits en allemand et une correspondance a été
mis en place (\cref{ListeAttributeObjet}).  Jusqu'à présent, l'interprétation des
objets de \bo{} repose sur le principe \textit{la forme définit la fonction}.
Cette approche est à juste titre critiquée, car, pour induire la fonction d'un
objet ou, plus exactement, ses usages, la forme doit être considérée en même
temps que les contextes de découverte, les traces d'usage, et elle devrait se
fonder sur une analogie explicitée\footnote{\cite[85--108]{Bernbeck1997} ; \cite[67--105]{Verhoeven1999},
\cite{Verhoeven2005} ; \cite{Wylie1985}.}. Un tel travail n'a pas pu être entrepris et je me suis
contenté de suivre la formule ancienne ne pouvant pas étudier individuellement tous
les objets\footnote{Voir \cref{532-Activites}.}. Les objets ont été classés en
101 catégories (\cref{ListeAttributeObjet}).

Les dénominations des classes qui suivent celles des
catalogues publiés\footnote{Chronologiquement : \cite{Fischer1963,Seidl1972,Boehmer1979,Boehmer1983,Boehmer1987}.}
ne sont pas explicitées. Dans certains cas, une remarque précise la dénomination
et je commente succinctement les dénominations les plus énigmatiques. Les \emph{tessons
gravés}, classés comme outil d'administration, regroupent uniquement les tessons
pour lesquels on suppose un sens qui indique la destination du vase\footnote{Il
  s'agit des tessons regroupés par \Glatz{} (\citeyear[7 fig. 2]{Glatz2012a}).}.
  Les \emph{tessons décorés} regroupent les tessons poinçonnés de manière
  décorative, d'où le classement dans les décors.
%(\XX{} Problème dans le classement par exemple Bo1973-265p : quel est le sens ?
  %).
La dénomination \emph{Épingle-x} renvoie aux épingles dont la tête
est absente et qui ne peuvent donc pas être classées selon la
typologie reposant
sur la morphologie de la tête. En ce qui concerne les objets en terre cuite,
je les ai classé en terres cuites \emph{anthropomorphes},
\emph{zoomorphes} et spécifiquement \emph{tauromorphes}.





% Rcode to make the table
% write.table(levels(KfL$Objet), "clipboard", row.names=FALSE, eol="\n", quote=FALSE )
% write.table(table(KfL$Objet), "clipboard", row.names=FALSE, sep=";", eol="\n", quote=FALSE )
% write.table(prop.table(table(KfL$Objet))*100, "clipboard", row.names=FALSE, sep=";", eol="\n", quote=FALSE )


\begin{landscape}
\footnotesize
\begin{longtable}{rrllp{12cm}}
 \toprule
\multicolumn{1}{l}{Nombre} & \multicolumn{1}{l}{Pourcentage} & français & allemand & Remarque \\
  \midrule
\endhead
 \bottomrule
 \multicolumn{2}{r@{}}{\footnotesize Suite à la page suivante} \\
\endfoot
\endlastfoot
3 & 0,08 & Éclat & Abschlag & Éclat de silex ou d'obsidienne \\
3b & 0,76 & Pendentif & Anhänger &  \\
7 & 0,18 & Astragale & Astragal & Osselet en os ou autre matériau \\
2 & 0,05 & Attache & Attache & Fixation \\
14 & 0,35 & Lingot & Barren & Masse de métal \\
29 & 0,73 & Outils en os & Bearbeiteter Knochen &  \\
12 & 0,3 & Hache & Beil &  \\
2 & 0,05 & Représentation & Bild & Élément de décoration en bas-relief \\
51 & 1,29 & Tôle & Blech & Mince plaque de métal pour recouvrir une surface\\
5 & 0,13 & Carotte & Bohrkern & Déchet du perçage de pierre \\
2 & 0,05 & Herminette & Dechsel &  \\
12 & 0,3 & Couvercle & Deckel &  \\
1 & 0,03 & Diadème & Diadem &  \\
14 & 0,35 & Poignard & Dolch &  \\
29 & 0,73 & Fil de métal & Draht &  \\
26 & 0,66 & Tesson gravé & eingeritzte Scherbe &  Motif sémiotique, selon la définition de \cite{Glatz2012a}\\
2 & 0,05 & Chenet & Feuerbock &  \\
53 & 1,34 & Fibule & Fibel &  \\
27 & 0,68 & Figurine & Figurine & Représentation anthropomorphe en terre cuite \\
416 & 10,51 & Vase & Gefäß &  \\
1 & 0,03 & Vase à décor appliqué & Gefäß mit Relief &  \\
96 & 2,43 & Tesson poinçonné & gestempelte Scherbe &  \\
28 & 0,71 & Poids & Gewicht & Peson et autre \\
12 & 0,3 & Poids (croissant) & Gewicht – Halbmondförmig & Poids en forme de croissant de lune, spécifique \delaperiodeCAC{} \\
1 & 0,03 & Cloche & Glocke &  \\
24 & 0,61 & Manche & Griff &  \\
15 & 0,38 & Boucle de ceinture & Gürtel &  \\
8 & 0,2 & Moule & Gussform &  \\
11 & 0,28 & Loupe (métal) & Gusskuchen &  \\
9 & 0,23 & Crochet & Haken &  \\
20 & 0,51 & Marteau & Hammer &  \\
2 & 0,05 & Massue & Keule &  \\
8 & 0,2 & Lame & Klinge &  \\
3 & 0,08 & Pommeau & Knauf &  \\
1 & 0,03 & Bouton & Knopf &  \\
1 & 0,03 & Boule & Kugel &  \\
11 & 0,28 & Pointe de lance & Lanzenspitze &  \\
9 & 0,23 & Couteau à cuir & Ledermesser & Interprétation d'après \cite{Herbordt2015a}\\
92 & 2,32 & Bras à libation & Libationsarm &  \\
8 & 0,2 & Meule dormante & Mahlstein &  \\
95 & 2,4 & Burin & Meißel &  \\
43 & 1,09 & Couteau & Messer &  \\
36 & 0,91 & Monnaie & Münze &  \\
37 & 0,94 & Épingle - Divers & Nadel – Divers &  \\
91 & 2,3 & Épingle - Tête conique & Nadel – Kegelkopf &  \\
38 & 0,96 & Épingle - Tête à ailettes & Nadel – Lamellenkopf &  \\
37 & 0,94 & Épingle - Tête lenticulaire & Nadel – Linsenkopf &  \\
249 & 6,29 & Aiguille & Nadel – Ösen &  \\
74 & 1,87 & Épingle  (champ.) & Nadel – Pilzkopf &  \\
57 & 1,44 & Épingle - Tête facettée & Nadel – Pyramidenkopf &  \\
23 & 0,58 & Épingle - Tête enroulée & Nadel – Rollen &  \\
109 & 2,75 & Épingle - Tête discoïdale & Nadel – Scheibenkopf &  \\
19 & 0,48 & Épingle - Tête carrée & Nadel – Würfelkopf &  \\
  70 & 1,77 & Épingle - x & Nadel – x & \footnotesize{Épingle à la
    tête manquante dont seule la tige est conservée, inclassable selon la typologie} \\
19 & 0,48 & Clou & Nagel &  \\
95 & 2,4 & Objet & Objekt & Inclassable selon les informations disponibles \\
5 & 0,13 & Anneau d'oreille & Ohrpflock & Pourrait aussi être un élément de parure (\cite[Cat. 2367]{Boehmer1972}) \\
2 & 0,05 & Écaille d'armure & Panzerplättchen &  \\
61 & 1,54 & Perle & Perle &  \\
86 & 2,17 & Pointe de flèche & Pfeilspitze &  \\
215 & 5,43 & Poinçon & Pfriem &  \\
2 & 0,05 & Pincette & Pinzette &  \\
2 & 0,05 & Décoration en ronde-bosse & Plastik &  \\
33 & 0,83 & Plaquette & Plättchen &  \\
2 & 0,05 & Plaquette à filature & Quadrat (Seile) &  \\
7 & 0,18 & Rouelle (TC) & Rädchen & Petite roue en terre cuite \\
49 & 1,24 & Meule courante & Reibstein &  \\
91 & 2,3 & Céramique à relief & Reliefkeramik &  \\
122 & 3,08 & Anneau & Ring &  \\
7 & 0,18 & Sceau-cylindre & Rollsiegel &  \\
28 & 0,71 & Rhyton & Rhyton & Classe probablement sous-estimée, puisque dans certains cas, si seule la tête de l'animal est conservée, il n'est pas possible de faire la différence avec une figurine zoomorphe \\
30 & 0,76 & Pierre à aiguiser & Schleifstein &  \\
2 & 0,05 & Balle de fronde & Schleuderkugel &  \\
1 & 0,03 & Creuset & Schmelztiegel &  \\
3 & 0,08 & Vase en forme de pied & Schuhgefäß &  \\
1 & 0,03 & Épée & Schwert &  \\
3 & 0,08 & Faucille & Sichel &  \\
4 & 0,1 & Passoire & Sieb &  \\
141 & 3,56 & Sceau & Siegel &  \\
87 & 2,2 & Scellement & Siegelabdruck & Support scellé sans distinction de forme (\textit{bullae} ou autres) \\
6 & 0,15 & Pion & Spielstein &  \\
82 & 2,07 & Fusaïole & Spinnwirtel &  \\
1 & 0,03 & Pointe & Spitze &  \\
26 & 0,66 & Barrette & Stab & Petite barre (de métal) de section circulaire \\
2 & 0,05 & Stèle & Stele &  \\
2 & 0,05 & Corne de taureau (TC) & Stierhorn & Corne de figurine de taureau en terre cuite, soigneusement travaillée (engobe blanc) \\
125 & 3,16 & Figurine de taureau (TC) & Stierterrakotta & Terre cuite
de grande taille en forme de taureau, soigneusement travaillée à
engobe rouge et blanc, et polies \\
6 & 0,15 & Stylet & Stylus &  \\
7 & 0,18 & Terre cuite & Terrakotta & Fragment de terre cuite zoomorphe, anthropomorphe ou architecturale \\
9 & 0,23 & Anse à protomés & Terrakotta – Henkel &  \\
236 & 5,96 & Figurine zoomorphe & Tierstatuette & Parmi les figurines zoomorphes ont été classées toutes les représentations animales ; certaines appartiennent sans doute à des rhyta (dans certains cas, si seule la tête est présente, il n'est pas forcément possible de faire la différence.) \\
9 & 0,23 & Instrument de toilette & Toilettengerät &  \\
1 & 0,03 & Bloc & Tonklotz &  \\
229 & 5,79 & Tablette cunéiforme & TT (Tontafel) &  \\
2 & 0,05 & Crapaudine & Türangelstein &  \\
6 & 0,15 & Vase à créneaux & Turmvase &  \\
1 & 0,03 & Urne & Urne &  \\
24 & 0,61 & Tige & Vierkant & Tige de section quadrangulaire, sans pointe, à la différence du poinçon \\
1 & 0,03 & Plateau de balance & Waagschale &  \\
4 & 0,1 & Navette de tissage & Weberschiffchen &  \\
5 & 0,13 & Outil & Werkzeug & Outil à la fonction indéfinissable sans étude approfondie \\
\bottomrule
\caption{Dénombrement et dénominations des petits objets de la
\WTer{} enregistrés dans la base de données \BoFuWTer. On notera la
présence d'objets distinctifs de l'âge du Fer (en particulier les
urnes et les épingles), qui n'ont pas été pris en compte dans
l'analyse}
\label{ListeAttributeObjet}
\end{longtable}
\end{landscape}
\normalsize



\opt{optbw_photo}{
 \begin{figure}[h]
   \centering
   \begin{minipage}[]{0.50\linewidth}
   \centering
     \includegraphics[width=60mm]{Fig_70}
     \caption{Exemple d'un petit objet inédit, tête d'une figurine
     (\kfd{Bo70}{145})}
     \label{Lab_Bo70-145i-Tete-figurine}
   \end{minipage}
 \end{figure}
 }





\section{Description élémentaire} \label{9812-BDD-KF-DescElementaire}


Dans cette partie, je conduis des études statistiques univariées et bivariées pour décrire le jeu
de données, souligner son potentiel, ses limites et les problèmes
qu'il pose. Le premier
graphique (\cref{Lab_Small_finds_by_year-1}) représente le nombre des petits objets
inventoriés de la \WTer{}, classés par année. On peut aisément distinguer
les années durant lesquelles se sont déroulées des fouilles (1938, 1953, 1956--1957,
1970--1971, 1973, 1975--1977) des années pendant lesquelles seuls quelques objets ont été
inventoriés lors de l'étude du matériel (1955, 1972) ou bien les années où seuls
quelques sondages ponctuels ont été menés (1958, 1974, 1978). D'une façon générale, les fouilles des
années 1970 sont assez homogènes et le nombre d'objets découverts, en moyenne
535 objets par campagne, laisse supposer que les conditions de travail (durée
des fouilles, taille du personnel) et les méthodes ont
été très similaires d'une campagne à l'autre.

La répartition des 24 catégories d'objets les plus fréquents par année
(\cref{Lab_HistFig_73,Lab_HistFig_72}) met en valeur des tendances
s'écartant d'une répartition aléatoire. Si la répartition était aléatoire, alors
le nombre d'objets d'une catégorie dépendrait du nombre total d'objets
inventoriés et la silhouette du diagramme en bâtons de chaque catégorie devrait
être similaire à celle du diagramme du nombre d'objets inventoriés
(\cref{Lab_Small_finds_by_year-1}). Clairement,  certaines catégories s'éloignent d'une
telle distribution. Si l'on prend l'exemple des scellements
(\cref{Lab_HistFig_73}), le pic de l'année 1957 indique la mise au jour
d'un ensemble «~anormalement~» riche. Plus nette encore sur le
diagramme suivant,
la quantité de fragments de bras à libation en 1977 (et en 1956) révèle
clairement que des ensembles fonctionnels ont été découverts. Un écart de la
tendance générale peut signaler d'autres corrélations et n'est pas
\emph{forcément} lié aux découvertes. Dans le cas des tessons poinçonnés
(\cref{Lab_HistFig_72}), un nombre très important d'objets sont datés de
1971, mais cette anomalie a été causée par la préparation de la publication de
\Seidl{} sur cette catégorie d'objet\footnote{\cite{Seidl1972}.} plutôt que par
la mise au jour de contextes particuliers pendant la fouille de cette année.
Semblablement, les meules courantes ont été presque exclusivement découvertes en
1976 et 1977 (\cref{Lab_HistFig_73}), ce qui semble être plutôt la
conséquence d'un changement dans l'enregistrement des objets. À mon avis, avant
1976, les meules courantes n'ont simplement pas été
inventoriées\label{MeulesCourante}.

Le diagramme en bâtons des matériaux (\cref{Fig_74}) indique la
 répartition générale des matériaux utilisés. À eux seuls, les objets en terre
 cuite et les alliages cuivreux totalisent plus de \SI{70}{\percent} des petits
 objets. La répartition selon la classification stratigraphique
 (\cref{Fig_75}) révèle que la plupart des objets proviennent de
 contextes incertains, que j'ai qualifiés de remblai. En ce qui concerne la
 publication des petits objets, le diagramme (\cref{Fig_76}) dévoile
 l'état d'avancement des différentes publications de fouille. Le corpus le
 plus important -- les objets en métaux, pierres et os -- a été publié par
 \Boehmer, en deux volumes (\cref{Fig_77}). Le premier, concernant
 les fouilles antérieures à 1958, regroupe 432 objets de la ville basse
 \footnote{\cite{Boehmer1972}.}, celui des années 1970 comprend 1743 objets
 \footnote{\cite{Boehmer1979}.}. Les catégories spécifiques d'objets, comme les
 textes,
 sont publiés dans la série \textit{Keilschrifttexte aus Boghazköi}, les
 sceaux et les vases à reliefs dans deux
 volumes\footnote{\cite{Boehmer1987,
 Boehmer1983}.}. Fischer a publié des objets en céramique de la ville
basse datant des
 fouilles des années 1950 et Seidl quelques tessons poinçonnés et l'inventaire
 d'une unique pièce de la ville
 basse\footnote{\cite{Fischer1963,Seidl1972,Seidl1975}.}.  Néanmoins, un grand
 nombre d'objets sont toujours inédits, dont la majorité de ceux qui sont en
 terre cuite, notamment les vases, les figurines, les fusaïoles et autres
 (\cref{Fig_78}, \cref{Lab_Bo70-145i-Tete-figurine}).

\opt{optbw}{
 \begin{figure}[h!]
   \centering
     \includegraphics[width=160mm]{Fig_71}
     \caption{Diagramme en bâtons du nombre de petits objets inventoriés de la ville
     basse, répartis par année. En 1937, 1955, 1958, 1972, 1974, 1978, seuls des travaux
   d'études ou des sondages limités ont eu lieu.}
     \label{Lab_Small_finds_by_year-1}
 \end{figure}
 }

\opt{optbw}{
 \begin{figure}[h!]
   \centering
   \begin{minipage}[]{\linewidth}
   \centering
     \includegraphics[width=160mm]{Fig_72}
     \caption{Diagramme en bâtons des objets les plus courants, de la 2\eme{}
     catégorie (aiguille) à la 12\eme{} catégorie (céramique à relief)}
     \label{Lab_HistFig_72}
   \end{minipage}
   \begin{minipage}[]{\linewidth}
   \centering
     \includegraphics[width=160mm]{Fig_73}
     \caption{Diagramme en bâtons des objets les plus courants, de la 13\eme{}
     catégorie (bras à libation) à la 24\eme{} catégorie (meule courante)}
     \label{Lab_HistFig_73}
   \end{minipage}
 \end{figure}
 }

\FloatBarrier


  \begin{figure}[h]
      \centering
        \includegraphics[width=160mm]{Fig_74}
        \caption{Diagramme en bâtons des matériaux des petits objets}
        \label{Fig_74}
%  \end{figure}

% \begin{figure}[h]
%      \centering
        \includegraphics[width=160mm]{Fig_75}
        \caption{Diagramme en bâtons de la stratigraphie des petits objets}
        \label{Fig_75}
%  \end{figure}

%  \begin{figure}[h]
%      \centering
        \includegraphics[width=160mm]{Fig_76}
        \caption{Diagramme en bâtons du nombre d'objets par publication et
             de ceux restés inédits}
        \label{Fig_76}
  \end{figure}



  \begin{figure}[h]
      \centering
        \includegraphics[width=160mm]{Fig_77}
        \caption{Répartition des petits objets publiés dans
             \cite{Boehmer1972} et \cite{Boehmer1979} et de ceux restés inédits}
        \label{Fig_77}
  \end{figure}

  \begin{figure}[h]
      \centering
        \includegraphics[width=160mm]{Fig_78}
        \caption{Diagramme en bâtons des objets inédits, classés par matériaux}
        \label{Fig_78}
  \end{figure}

