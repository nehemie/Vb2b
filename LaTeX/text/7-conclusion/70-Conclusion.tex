L'étude de la ville basse de \bogazkoy{} est importante, car elle
permet de voir la restructuration du paysage urbain et la
réorganisation des activités lors du passage de la cité-État
\delaperiodeCAC{} à la capitale du Royaume hittite. À travers l'examen
des archives des fouilles (1938--1978), ce travail apporte un
éclairage multiscalaire sur la relation entre le quartier d'habitation
et la ville. Les archives de la ville basse ont été exploitées près de
\num{40} ans après la fin de l'exploration archéologique. De fait, ce
travail est un premier essai pour montrer le potentiel de cette
archive et fournir un cadre à l'interprétation.



\paragraph{} L'établissement d'un nouveau cadre chronologique remet en question
les paradigmes de l'interprétation du siècle précédent.  Jusqu'à présent, la
chronologie de la ville basse se fondait sur différentes fouilles
stratigraphiques dont la synchronisation n'avait été que superficiellement
démontrée par les recherches antérieures. En reprenant tour à tour les chantiers
et leurs stratigraphies respectives (\buyukkale, \nwh, \NoV{} et \unterstadt{}),
ce travail souligne les incohérences du cadre antérieur. Les travaux inédits sur la
céramique de la \WTer{} (présentés en détail en annexe), couplés à l'analyse de
dates radiocarbones et aux résultats des recherches récentes, établissent un
nouveau  cadre chronologique pour l'évolution de la ville basse. L'analyse
démontre que la phase la mieux attestée de la \WTer{} ne date pas des
\xiveme{}--\xiiieme{} siècles, comme on l'admettait jusqu'à présent, mais des
\xvieme{}--\xveme{} siècles et que seuls quelques vestiges sporadiques sont
attestés pour la \WTer{}  aux \xiveme{} et \xiiieme{} siècles. L'analyse par
inférence bayésienne date l'occupation de la \WTer{} du \BA{} entre 2178 et 1967
\emph{cal. bc} (intervalle de confiance à \SI{94,3}{\percent}), la transition
du \BA{} \alaperiodeCAC{} entre 2062 et 1944 \emph{cal. bc} (intervalle de confiance à
\SI{91,3}{\percent}), la transition \delaperiodeCAC{} \alaph{} entre 1813 et 1617 \emph{cal.
bc} (intervalle de confiance à \SI{94,6}{\percent}) et la fin de l’occupation de
la \WTer{}  entre 1607 et 1438 \emph{cal. bc} (intervalle de confiance à
\SI{95,4}{\percent}). Ce cadre permet de mieux saisir l'évolution de la ville et
la structuration de l'occupation, et oblige à réviser le statut de la ville au
\xiveme{} siècle.


\paragraph{}
L'étude de l'occupation de la \WTer{} a démontré qu'il n'y a pas de
redondance évidente entre les plans de bâtiments, chacun étant plus ou moins
unique. L'idée de \Neve{}, qui postula
que les changements structuraux des plans des maisons seraient la conséquence
des changements structuraux de la société, ne peut pas être retenue. Une disparition
du plan à pièce centrale, \textit{Hofhaus}, remplacé par le plan tripartite,
\textit{Hallenhaus},
n'est pas attestée. \AlaperiodeCAC{} et \alaph{}, la \WTer{} est occupée principalement
par des maisons dont les types de plan sont tous contemporains. \AlaperiodeCAC{}, les maisons
attestées sont relativement spacieuses, plus vastes que les maisons de \kultepe{} ou
que celles \delaph{}.

\Alaph{}, alors que la plupart des bâtiments sont considérés comme des maisons,
certains bâtiments étaient visiblement dédiés au stockage (\textsc{Gebäude 64},
\textsc{Gebäude 84}), d'autres,  soignés et spacieux, avaient une fonction
résidentielle mais aussi administrative (\gebQ, \gebWI) et certains bâtiments
restent incompris (\gebQR{}). L'analyse des fortifications et des portes
d'accès, des voies de circulation et du système d'évacuation a démontré comment
les aménagements collectifs étaient planifiés, \alaperiodeCAC{} comme \alaph{}.  Les
concepts de quartier (conception privée de l'espace) et de district (conception
publique de l'espace), ont mis en valeur la médiation nécessaire pour leur
cohabitation et illustrent bien la gestion des aménagements collectifs et de
l'habitat privé.

Dans la littérature secondaire, l'habitat de la \WTer{} a été considéré
comme l'aboutissement d'une longue évolution qui ne serait pas le résultat d'une
planification centrale. Si l'habitat témoigne certes d'une
«~(dés)organisation par agglutination~», elle est toute relative, car bien
délimitée. La \WTer{} a été planifiée de manière à éviter tout enclavement
et à assurer un accès aux voies de circulation et au système d'évacuation des eaux.

\paragraph{}

L'analyse spatiale de la totalité des petits objets de la \WTer{} a identifié les activités
principales et les a pondérées. L'examen des contextes montre que seule une
étude à l'échelle du quartier permet d'établir une relation entre objets et
activités. Alors que les outils d'administration sont explicitement accaparés
par le pouvoir central \alaph{}, les petits objets dédiés aux travaux
d'artisanat sont largement répartis sur tout le secteur. En revanche, les objets
liés aux activités de subsistance sont étrangement absents du registre des
petits objets et donc des cartes de répartition. Les attestations de production
d'objets sont également assez rares, ce qui permet de conclure que la population
de la \WTer{} était principalement constituée de fonctionnaires, dont le rôle
était d'administrer la ville. Étant donné la dépendance de la capitale
dans le réseau de villes et de villages, sa gestion avait des répercussion sur tout le royaume.


\paragraph{}

L'étude de la restructuration du paysage urbain de la ville basse
montre comment la ville a été modifiée lorsqu'elle devenait la
capitale du Royaume hittite. La taille du site \delaperiodeCAC{} peut
être estimée à \SI{25}{\hectare} et différents modèles d'évaluation du
peuplement déterminent la taille de la population à environ
\num{2000}--\num{3000} personnes. La transition \delaperiodeCAC{}
\alaph{} indique que la réoccupation a dû intervenir très rapidement
après le «~saccage~» de la ville à la fin \delaperiodeCAC{}.
L'évolution de l'organisation de la capitale hittite souligne de
manière manifeste comment les bâtiments monumentaux et administratifs
viennent à occuper de plus en plus d'espace. Je  propose une
estimation théorique de la population maximale à moins de \num{10000}
habitants et supporte l'hypothèse  que la ville ne devait pas être
autosuffisante. Ce développement montre que la reconfiguration de
l'espace urbain témoigne non seulement d'une volonté de mettre en
scène l'idéologie royale, mais témoigne aussi de la réorganisation du réseau
des villes et de leur système d'approvisionnement, permettant à la capitale de
dédier des ressources principalement aux piliers du pouvoir royal
(administration et culte). C'est l'une des ruptures principales avec
la période \delaperiodeCAC{}, lorsque la ville était organisée en
cité-État. En tant que capitale, la ville a capté une attention sans
précédent pour soutenir l'idéologie du royaume. Cet «~équilibre~» est
remis en cause dès le \xiveme{} siècle, sans doute avec l'abandon de la
\WTer{} et les projets monumentaux des derniers rois hittites, qui sont les
témoins d'une tentative d'agir contre l'effritement du pouvoir.


\paragraph{}
À l'issue de ce travail sur le paysage urbain de \bogazkoy, deux
axes en particulier semblent s’ouvrir à la recherche.

Le premier serait de mettre en avant les activités que l'on connaît
dans les autres villes et de les pondérer pour caractériser les réseaux d'échange et
leur restructuration entre \laperiodeCAC{} et \laph{}. Par exemple, on
pourrait imaginer
que \kamankalehoyuk{} jouait le rôle d'un grenier centralisateur et
redistributeur ou bien que l'une ou l'autre ville avait des activités plus
spécifiquement dédiées à la métallurgie. Une telle approche assurerait un
premier pas, fondé, vers la modélisation de réseaux. De nombreux résultats de
mon analyse pourraient aider à établir de tels modèles grâce aux estimations
plausibles fournies.

Le deuxième axe, complémentaire du premier, devra mettre l'accent sur
l'environnement. Une réflexion sur les relations des villes avec
leur environnement reste pour l'essentiel à faire. Pour l'instant,
faute de données et de modèles à disposition, la recherche est
condamnée à des affirmations telles que : «~\emph{Es ist
  wahrscheinlich, daß der größere Teil der hethitischen Bevölkerung
  Kleinasiens auf dem Lande lebte und sich Landwirtschaft und
  Viehhaltung oder auch Jagd und Fischfang -- Bereichen ohne größere
  ökonomische Bedeutung --
  widmete}~»\footnote{\cite[350]{Imparati1999a}.}. Tant que
l'archéologie en Anatolie centrale ne se sera pas consacrée à de tels
questionnements, notamment en recourant à des prospections intensives,
l'essentiel de la population restera occultée par les monuments des
classes dominantes, si bien que les reconstructions de son histoire
resteront fondamentalement biaisées.

\pagebreak

\paragraph{}

 La structure et la nature de ce travail sont organisées pour faciliter sa
 reprise, mais aussi l'élaboration de nouveaux travaux.

 Les choix techniques à la base de ce travail ont été explicités pour
 montrer à quel point il est primordial en archéologie de s'investir
 pour assurer la comparabilité des résultats.  Grâce à la mise en
 place d'une chaîne opératoire transparente et reproductible, ce
 travail démontre comment il est possible de tirer profit des
 logiciels libres pour créer une archéologie numérique littéraire.
 Elle a pour but, en mettant à disposition les données et le code
 informatique, de transformer une étude en un terreau fertile pour
 d'autres travaux et de futures publications,
 notamment pour les objets en terre cuite qui sont toujours inédits.

 Il va de soi qu'une seule personne ne peut pas mener à bien une étude aussi
 vaste sans occulter l'une ou l'autre partie. Je ne doute pas un instant non
 plus que des erreurs, plus ou moins compromettantes, se sont glissées dans mon
 raisonnement et que des raccourcis grossiers ont été empruntés.  J'espère que
 cette mise à disposition facilitera aussi la vérification, la critique et
 l'approfondissement de l'esquisse présentée dans ce manuscrit.
