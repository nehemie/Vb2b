#! /bin/bash

cat << README

This a script to move the R-figures in the folder images for LaTeX

README

list_images="rename_figs_list.csv"
images_folder="../images/"

cat   $list_images | \
      while IFS=, read old new; do
       echo "I am now moving this"
       echo $old "->" $new
       echo ""

        ### Move file
       find ../../** \
         ! -path "./.git*" \
         ! -path "./temporary*" \
         ! -path "./workInProgress*" \
         ! -path "./lc.sh" \
         ! -path "./makepng.sh" \
         ! -path "./list_image_names.csv" \
         ! -path "./LaTeX/cache/*" \
         ! -name "*.odg" \
         ! -name "*.sh" \
         ! -name "*.html" \
         ! -name "*.R" \
         ! -name "*.rnw" \
         ! -name "*.svg" \
         ! -name "*p.png" \
         -a -type f -name "${old}\.*" | \
         xargs -I{} mv {} ${images_folder} && \
          rename "s,$old\.,$new\.," ${images_folder}*

         ### cp file
       find ../../** \
         -name "*p.png" \
         -a -type f -name "${old}\.*" | \
         xargs -I{} cp {} ${images_folder} && \
          rename "s,$old\.,$new\.," ${images_folder}*
      done
