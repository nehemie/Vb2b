# KnitrOption, include=FALSE, cache=FALSE ----
library(knitr)
options(replace.assign = TRUE, width = 50, OutDec = ",")

opts_chunk$set(fig.path = 'Rfig/Rfig-', 
               cache.path = 'cache/', 
               fig.align = 'center', 
               fig.width = 5, 
               fig.height = 5, 
               fig.show = 'hold', 
               cache = TRUE, 
               par = TRUE, 
               warning = FALSE)

knit_hooks$set(par = function(before, options, envir) {
  if (before && options$fig.show != 'none') 
    par(mar = c(4,4,.1,.1),
        cex.lab = 0.95,
        cex.axis = 0.9,
        mgp = c(2,.7,0),
        tcl = -.3)
}, crop = hook_pdfcrop)
