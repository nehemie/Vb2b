# README BoFuWTer

The file `BoFuWTer.tsv` is the raw data I collected about the small
finds from the West-Terrace. It is a data frame with 4070 observations
on the following 28 variables.

 - **jahr**: Year of the excavation. Only the last two numbers of a
   year are given: "71" means "1971" (categorical)

 - **kfnr**: Unique number (per year) assigned to each object, such as
   316 (numeric)

 - **kfnr_roemisch**: Letter used to subdivide identical numbers
   (**kfnr**). For example, letters were used in the case of graves,
where all objects were given the same number, e.g. 71-316a, 71-316b,
71-316c (character)

 - **material**: Identification of the material of each object as they
   stand, like gold, lead or iron (it is often problematic, especially
for metals and stones).  The nature of the stones has not been
specified. (character)

 - **typ**: Type of object that follow the published catalogues
   (character)

 - **fundort**: place of discovers (categorical)

 - **stratigraphie**: object context: in situ, closed or open
   (categorical)

 - **strat**: Precision, if it exists, on the stratigraphy (character)

 - **schnitt_roemisch**:  The name of the trench, often in Roman
   numerals. The denominations having been re-used over the years,
they have to be combined with the variable **jahr_des_schnittes** to
be unique and to differentiate the trench "I" of 1975 and "I" of 1976
(character)

 - **jahr_des_schnittes**: Year of excavation trench (numeric)

 - **schnitt_arabisch**: Subdivision of the trench into Roman numerals
   or distance interval (character)

 - **schnitt_zusatz_1**: Precision of the **schnitt** (character)

 - **schnitt_zusatz_2**: Precision of the **schnitt** (character)

 - **schnitt_zusatz_3**: Precision of the **schnitt**  (character)

 - **gx_rx**: Reference to the building and room where an object was
   found. For example "B13.R3" means Building 13, Room 3 (character)

 - **datierung**: Dating. Used for texts ans seals and when the
   material obviously did not date from the Hittite period, like coins
(categorical)

 - **laenge_mm**: Length of the object in mm (numeric)

 - **breite_mm**: Width of the object in mm (numeric)

 - **dicke_mm**: Thickness of the object in mm (numeric)

 - **masse_g**: Weight of the object in g (numeric)

 - **publikation**: Reference to publication (character)

 - **publikation_nr**:  Publication Catalogue Number (character)

 - **publikation_nr_roemisch**:  Publication Catalogue Letter (character)

 - **tafel**: Publication Plate (for image) (character)

 - **CTH**: Only for cuneiform text. Reference to the Catalogue des Textes Hittites (character)

 - **museum**: Museum where the object is kept (character)

 - **bemerkungen**: Remarks (character)
