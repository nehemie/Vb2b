# GEBCO's gridded bathymetric data sets

## Terms of use

Data within GEBCO's gridded data sets are subject to copyright and database
right restrictions.

Providing the source material is properly credited, the reproduction of the
gridded bathymetry data sets in derivative form for scientific research,
environmental conservation, education or other non-commercial purposes is
authorised without prior permission.

Users are encouraged to advise third parties to download the GEBCO gridded data
sets from our web site rather than providing the grids to third parties
themselves. This will allow GEBCO to keep statistics on how widely the GEBCO
gridded data sets are used.

Users who intend to use GEBCO's gridded data for commercial purposes are kindly
asked to seek our prior permission.

    In the first instance, please contact BODC (enquiries@bodc.ac.uk) and
include a clear statement of the purpose for which the material will be used and
the manner in which it will be produced.

Users are asked to report any problems encountered with the data as this
feedback may result in the further improvement of GEBCO's data sets.

## Attribution

Any material reproduced from GEBCO's gridded data sets should be accompanied by
appropriate attribution to the source material. Users are advised the consult
the accompanying data set documentation before using the data sets.

For the GEBCO bathymetry data, this should be of the form:

    Reference for the GEBCO_2014 Grid: 'The GEBCO_2014 Grid, www.gebco.net'
Reference for the GEBCO One Minute Grid: 'The GEBCO One Minute Grid, version
2.0' For non-GEBCO material, such as the land data from the SRTM30 data set, the
acknowledgement should include reference to the original source material.

## Disclaimer

GEBCO'S GLOBAL GRIDS SHALL NOT TO BE USED FOR NAVIGATION OR FOR ANY OTHER
PURPOSE INVOLVING SAFETY AT SEA.

Data used in the creation of GEBCO's gridded data sets has been obtained from
sources believed to be reliable. While every effort has been made to ensure
reliability within the limits of present knowledge, the accuracy and
completeness of GEBCO's gridded data sets cannot be guaranteed. No
responsibility can be accepted by those involved in their compilation or
publication for any consequential loss or damage arising from their use.

User should be aware that GEBCO's grids are deep ocean products (based on
trackline data from many different sources of varying quality and coverage) and
do not include detailed bathymetry for shallow shelf waters. Even to the present
day, most of the world's oceans have not been fully surveyed and, for the most
part, bathymetric mapping is an interpretation based on random tracklines of
data from many different sources. The quality and coverage of the data from
these sources is highly variable. Although GEBCO's grids are presented at 30
arc-second and one arc-minute intervals of latitude and longitude, this does not
imply that knowledge is available on sea floor depth at this resolution - the
depth in most 30 arc-second squares of the world's oceans has yet to measured.
Users are advised to consult the accompanying data set documentation before
using the data sets. 
