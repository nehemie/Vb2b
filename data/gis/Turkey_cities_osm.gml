<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation=""
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>26.4148869</gml:X><gml:Y>36.2176872</gml:Y></gml:coord>
      <gml:coord><gml:X>44.0467885</gml:X><gml:Y>42.0266698</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                                                            
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.0">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>32.853758,39.9220732</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>25869410</ogr:osm_id>
      <ogr:name>Ankara</ogr:name>
      <ogr:type>city</ogr:type>
      <ogr:population>4338620</ogr:population>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.1">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>26.5587225,41.6759327</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>25869574</ogr:osm_id>
      <ogr:name>Edirne</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.2">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>26.4148869,40.1500808</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>25869575</ogr:osm_id>
      <ogr:name>Çanakkale</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.3">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>27.2235523,41.7370223</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>25869577</ogr:osm_id>
      <ogr:name>Kırklareli</ogr:name>
      <ogr:type>city</ogr:type>
      <ogr:population>70000</ogr:population>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.4">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>27.8855587,39.6470406</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>25869582</ogr:osm_id>
      <ogr:name>Balıkesir</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.5">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>27.8338384,37.8459132</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>25869583</ogr:osm_id>
      <ogr:name>Aydın</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.6">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>28.3644557,37.2148161</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>25869694</ogr:osm_id>
      <ogr:name>Muğla</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.7">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>30.5346872,38.7579642</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>25869720</ogr:osm_id>
      <ogr:name>Afyonkarahisar</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.8">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>29.9752911,40.1435101</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>25869729</ogr:osm_id>
      <ogr:name>Bilecik</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.9">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>30.2887286,37.7248394</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>25869752</ogr:osm_id>
      <ogr:name>Burdur</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.10">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>29.0863125,37.7715165</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>25869767</ogr:osm_id>
      <ogr:name>Denizli</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.11">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>31.1569975,40.8422778</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>25869768</ogr:osm_id>
      <ogr:name>Düzce</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.12">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>30.5216465,39.7688564</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>25869769</ogr:osm_id>
      <ogr:name>Eskişehir</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.13">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>29.9683195,40.7684074</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>25869794</ogr:osm_id>
      <ogr:name>Kocaeli</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.14">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>32.4844015,37.8719963</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>25869814</ogr:osm_id>
      <ogr:name>Konya</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.15">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>29.9854528,39.4195048</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>25869815</ogr:osm_id>
      <ogr:name>Kütahya</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.16">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>27.4255716,38.6155029</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>25869819</ogr:osm_id>
      <ogr:name>Manisa</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.17">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>29.4033271,38.6810732</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>25869820</ogr:osm_id>
      <ogr:name>Uşak</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.18">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>29.2729091,40.6556669</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>25869857</ogr:osm_id>
      <ogr:name>Yalova</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.19">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>35.3263252,37.0033484</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26480399</ogr:osm_id>
      <ogr:name>Adana</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.20">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>43.73766,37.574898</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26480484</ogr:osm_id>
      <ogr:name>Hakkâri</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.21">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>43.3742174,38.5083848</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26480571</ogr:osm_id>
      <ogr:name>Van</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.22">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>44.0467885,39.9215462</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26480597</ogr:osm_id>
      <ogr:name>Iğdır</ogr:name>
      <ogr:type>city</ogr:type>
      <ogr:population>75927</ogr:population>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.23">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>36.1653617,36.2176872</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26480836</ogr:osm_id>
      <ogr:name>Antakya</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.24">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>36.9034134,37.587084</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26480875</ogr:osm_id>
      <ogr:name>Kahramanmaraş</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.25">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>34.630542,36.81275</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26481523</ogr:osm_id>
      <ogr:name>Mersin</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.26">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>36.255941,37.073671</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26481742</ogr:osm_id>
      <ogr:name>Osmaniye</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.27">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>34.026907,38.3705416</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26482228</ogr:osm_id>
      <ogr:name>Aksaray</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.28">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>33.6162774,40.6003519</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26482245</ogr:osm_id>
      <ogr:name>Çankırı</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.29">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>33.2224781,37.1792447</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26482281</ogr:osm_id>
      <ogr:name>Karaman</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.30">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>35.4836107,38.7253231</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26482287</ogr:osm_id>
      <ogr:name>Kayseri</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.31">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>33.5276222,39.8485708</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26482294</ogr:osm_id>
      <ogr:name>Kırıkkale</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.32">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>34.1606334,39.1460997</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26482519</ogr:osm_id>
      <ogr:name>Kırşehir</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.33">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>34.7136022,38.6223688</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26482523</ogr:osm_id>
      <ogr:name>Nevşehir</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.34">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>34.6775534,37.9712079</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26482716</ogr:osm_id>
      <ogr:name>Niğde</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.35">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>37.013697,39.749781</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26482940</ogr:osm_id>
      <ogr:name>Sivas</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.36">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>34.8094917,39.8205571</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26482958</ogr:osm_id>
      <ogr:name>Yozgat</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.37">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>35.8338214,40.6502007</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26482974</ogr:osm_id>
      <ogr:name>Amasya</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.38">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>41.8278892,41.1831962</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26482975</ogr:osm_id>
      <ogr:name>Artvin</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.39">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>32.3384354,41.6338394</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26483113</ogr:osm_id>
      <ogr:name>Bartın</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.40">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>40.224099,40.25569</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26483234</ogr:osm_id>
      <ogr:name>Bayburt</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.41">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>34.9602453,40.5491496</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26483306</ogr:osm_id>
      <ogr:name>Çorum</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.42">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>38.3879289,40.9148702</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26483307</ogr:osm_id>
      <ogr:name>Giresun</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.43">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>39.4757339,40.4617844</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26483350</ogr:osm_id>
      <ogr:name>Gümüşhane</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.44">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>32.6218583,41.1974459</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26485694</ogr:osm_id>
      <ogr:name>Karabük</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.45">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>33.7770087,41.3765359</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26485876</ogr:osm_id>
      <ogr:name>Kastamonu</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.46">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>37.8839101,40.9805304</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26486071</ogr:osm_id>
      <ogr:name>Ordu</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.47">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>40.519612,41.022809</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26486097</ogr:osm_id>
      <ogr:name>Rize</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.48">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>36.3258984,41.2893535</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26486105</ogr:osm_id>
      <ogr:name>Samsun</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.49">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>35.1506765,42.0266698</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26486164</ogr:osm_id>
      <ogr:name>Sinop</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.50">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>36.55403,40.32648</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26486186</ogr:osm_id>
      <ogr:name>Tokat</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.51">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>31.7884596,41.4524668</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26486267</ogr:osm_id>
      <ogr:name>Zonguldak</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.52">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>43.0500388,39.7201318</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26486334</ogr:osm_id>
      <ogr:name>Ağrı</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.53">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>42.7035585,41.1102966</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26486338</ogr:osm_id>
      <ogr:name>Ardahan</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.54">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>40.4965998,38.8851831</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26486414</ogr:osm_id>
      <ogr:name>Bingöl</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.55">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>42.1081317,38.4002185</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26486498</ogr:osm_id>
      <ogr:name>Bitlis</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.56">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>39.221849,38.676303</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26486523</ogr:osm_id>
      <ogr:name>Elazığ</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.57">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>41.4967451,38.7403703</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26486736</ogr:osm_id>
      <ogr:name>Muş</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.58">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>39.4941023,39.7496052</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26486753</ogr:osm_id>
      <ogr:name>Erzincan</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.59">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>41.2727638,39.9063215</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26486754</ogr:osm_id>
      <ogr:name>Erzurum</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.60">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>43.0961734,40.605158</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26486777</ogr:osm_id>
      <ogr:name>Kars</ogr:name>
      <ogr:type>city</ogr:type>
      <ogr:population>75291</ogr:population>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.61">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>38.309269,38.350261</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26486997</ogr:osm_id>
      <ogr:name>Malatya</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.62">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>39.548197,39.1080631</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26487261</ogr:osm_id>
      <ogr:name>Tunceli</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.63">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>38.2764355,37.7640008</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26487468</ogr:osm_id>
      <ogr:name>Adıyaman</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.64">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>41.1279423,37.8838505</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26487469</ogr:osm_id>
      <ogr:name>Batman</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.65">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>40.2226158,37.9166031</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26487666</ogr:osm_id>
      <ogr:name>Diyarbakır</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.66">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>37.3793085,37.0611756</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26487670</ogr:osm_id>
      <ogr:name>Gaziantep</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.67">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>37.11688,36.718045</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26487717</ogr:osm_id>
      <ogr:name>Kilis</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.68">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>40.7251509,37.326634</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26487735</ogr:osm_id>
      <ogr:name>Mardin</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.69">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>38.7909159,37.160388</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26487743</ogr:osm_id>
      <ogr:name>Şanlıurfa</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.70">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>41.93984,37.931282</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26487823</ogr:osm_id>
      <ogr:name>Siirt</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.71">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>42.4605665,37.519248</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>26487882</ogr:osm_id>
      <ogr:name>Şırnak</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.72">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>30.5556933,37.77035</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>212488422</ogr:osm_id>
      <ogr:name>Isparta</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.73">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>28.9670144,38.3998673</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>212577787</ogr:osm_id>
      <ogr:name>Eşme</ogr:name>
      <ogr:type>city</ogr:type>
      <ogr:population>14700</ogr:population>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.74">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>27.144474,38.4153421</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>428039507</ogr:osm_id>
      <ogr:name>İzmir</ogr:name>
      <ogr:type>city</ogr:type>
      <ogr:population>2783866</ogr:population>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.75">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>30.6937117,36.9008783</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>428039517</ogr:osm_id>
      <ogr:name>Antalya</ogr:name>
      <ogr:type>city</ogr:type>
      <ogr:population>1001318</ogr:population>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.76">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>39.7253106,41.0052391</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>512552945</ogr:osm_id>
      <ogr:name>Trabzon</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.77">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>29.0667635,40.1827061</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>1786760799</ogr:osm_id>
      <ogr:name>Bursa</ogr:name>
      <ogr:type>city</ogr:type>
      <ogr:population>2740970</ogr:population>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.78">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>28.9651646,41.0096334</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>1882099475</ogr:osm_id>
      <ogr:name>İstanbul</ogr:name>
      <ogr:type>city</ogr:type>
      <ogr:population>13483052</ogr:population>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.79">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>27.5158544,40.9795272</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>2270281532</ogr:osm_id>
      <ogr:name>Tekirdağ</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.80">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>31.6128194,40.73454</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>3479054194</ogr:osm_id>
      <ogr:name>Bolu</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:Turkey_cities_osm fid="Turkey_cities_osm.81">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>30.4038668,40.7721863</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:osm_id>3984197634</ogr:osm_id>
      <ogr:name>Sakarya</ogr:name>
      <ogr:type>city</ogr:type>
    </ogr:Turkey_cities_osm>
  </gml:featureMember>
</ogr:FeatureCollection>
