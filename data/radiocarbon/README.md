radiocarbons
============

This folds contains the data for the C14 Bayesian model used with the software
OxCal.

Folder
------

 - **table**: data frames with the data for the analysis
 - **model**: model for the OxCal software (see specific **Readme** in that
   folder)
 - **labor-results**: original reports from the laboratories on the C14
   measurements

