# This is a script to Transform the .csv table in latex table
# All the table are done along the same principles
# Library
library(xtable)

# Table EBA
table <- read.delim("data/radiocarbon/table/Radiocarbon--EBA--1964.tsv")

# Rename column
colnames(table) <- gsub("d13c", "$\\\\delta^{13}C$",colnames(table))
table$Datation..BP. <- gsub("±", "$\\\\pm$ ",table$Datation..BP.)
colnames(table) <- gsub("Datation..BP.", "Datation (BP)",colnames(table))

# Latex output
print(xtable(table[,c(3:11)] ,
             align = c(c(rep("c",c(10)))),
             caption = "Datations  de la phase Vc, analyses du laboratoire
             de Heidelberg en 1972, qui a fournit les informations suivantes sur
             le traitement des échantillons : \\emph{Alle Proben wurden mit
             verdünnter Salzsäure vorbehandelt und der gemessene C-14 Gehalt
             aufgrund des massenspektrometrisch ermittelten C15/C12
             Verhältnisses auf Isotopentrennung korrigiert. Der angegebene
             Fehler ist ein reiner Analysenfehler (mittlerer Fehler)",
             label = "Radiocarbon--EBA--1964"),
      include.rownames = FALSE,
      floating = FALSE,
      tabular.environment = "longtable",
      table.placement = NULL,
      booktabs = TRUE,
     sanitize.text.function = identity
)


##########
# Table MBA 1964
table <- read.delim("data/radiocarbon/table/Radiocarbon--MBA--1964.tsv")
colnames(table) <- gsub("d13c", "$\\\\delta^{13}C$",colnames(table))
table$Datation..BP. <- gsub("±", "$\\\\pm$ ",table$Datation..BP.)
colnames(table) <- gsub("Datation..BP.", "Datation (BP)",colnames(table))

print(xtable(table[,c(3:11)] ,
             align = c(c(rep("c",c(10)))),
             caption = "Datations de la phase IVd, analyses du laboratoire
             d'Heidelberg en 1972 (cf. \\cref{Radiocarbon--EBA--1964})",
             label = "Radiocarbon--MBA--1964"),
      include.rownames = FALSE,
      floating = FALSE,
      tabular.environment = "longtable",
      table.placement = NULL,
      booktabs = TRUE,
     sanitize.text.function = identity
)





##########
# Table MBA 2013
table <- read.delim("data/radiocarbon/table/Radiocarbon--MBA--2013.tsv")

colnames(table) <- gsub("d13c", "$\\\\delta^{13}C (AMS)$",colnames(table))
table$Datation..BP. <- gsub("±", "$\\\\pm$ ",table$Datation..BP.)
colnames(table) <- gsub("Datation..BP.", "Datation (BP)",colnames(table))
colnames(table) <- gsub("Numéro.Inventaire", "Inventaire",colnames(table))
table$Remarque <- gsub("insitu", "\\\\insitu{}",table$Remarque)
table$Bibliographie <- gsub("Strupler2013a", "\\\\citealt{Strupler2013a}",
                            table$Bibliographie)

print(xtable(table[,c(3:11)] ,
             align = paste("ccc","p{4cm}","cccccc", sep = ""),
             caption = "Datations de \\knw, analyses du laboratoire CAU Kiel,
             qui fournit les information suivantes sur le prétraitement de
             l'échantillon : \\emph{Die Proben wurden unter dem Mikroskop auf
             Verunreinigungen kontrolliert und eine geeignete Menge Material
             wurde zur Datierung entnommen. Das ausgewählte Material wurde dann
             mit \\SI{1}{\\percent} \\ch{HCl}, \\SI{1}{\\percent} \\ch{NaOH} bei
             \\SI{60}{\\celsius} und wieder \\SI{1}{\\percent} \\ch{HCl}
             extrahiert (Laugenrückstand). Die Verbrennung erfolgte bei
             \\SI{900}{\\celsius} in einer mit \\ch{CuO} und Silberwolle
             gefüllten Quarzampulle. Das entstandene \\ch{CO2} wurde
             anschließend mit \\ch{H2} bei \\SI{600}{\\celsius} über einen
             Eisen-Katalysator zu Graphit reduziert und das
             Eisen-Graphit-Gemisch in einen Probenhalter für die AMS-Messung
             gepreßt.} ; Calcul d'après \\citealt{Stuiver1977a}, avec correction
             du fractionnement isotopique selon les rapports de
             \\ch{^{13}C}/\\ch{^{12}C}.",
             label = "Radiocarbon--MBA--2013"),
      include.rownames = FALSE,
      floating = FALSE,
      tabular.environment = "longtable",
      table.placement = NULL,
      booktabs = TRUE,
     sanitize.text.function = identity
)


##########
# Radiocarbon Silo
table <- read.delim("data/radiocarbon/table/Radiocarbon--LBA--NWH-Silo.tsv")

colnames(table) <- gsub("d13c", "$\\\\delta^{13}C $",colnames(table))
table$Datation..BP. <- gsub("±", "$\\\\pm$ ",table$Datation..BP.)
colnames(table) <- gsub("Datation..BP.", "Datation (BP)",colnames(table))
colnames(table) <- gsub("Numéro.Inventaire", "Inventaire",colnames(table))
table$Bibliographie <- gsub("Schoop & Seeher 2006", "\\\\citealt{Schoop2006b}",
                            table$Bibliographie)

print(xtable(table[,c(3:11)] ,
             align =  paste("ccc","c","cccccc", sep = ""),
             caption = "Datations du silo de \\NWH, analyses du laboratoire de
                       l'Institut für Umweltphysik der Universität Heildelberg",
             label = "Radiocarbon--LBA--NWH-Silo"),
      include.rownames = FALSE,
      floating = FALSE,
      tabular.environment = "longtable",
      table.placement = NULL,
      booktabs = TRUE,
      sanitize.text.function = identity
      )


##########
# Radiocarbon LBA 2013
table <- read.delim("data/radiocarbon/table/Radiocarbon--LBA--2013.tsv")

colnames(table) <- gsub("d13c", "$\\\\delta^{13}C (AMS)$",colnames(table))
table$Datation..BP. <- gsub("±", "$\\\\pm$ ",table$Datation..BP.)
colnames(table) <- gsub("Datation..BP.", "Datation (BP)",colnames(table))
colnames(table) <- gsub("Numéro.Inventaire", "Inventaire",colnames(table))
table$Remarque <- gsub("insitu", "\\\\insitu{}",table$Remarque)
table$Bibliographie <- gsub("Strupler2013a", "\\\\citealt{Strupler2013a}",
                            table$Bibliographie)

print(xtable(table[,c(3:11)] ,
             align =  paste("cc","p{1.8cm}p{5cm}p{1.6cm}","ccccc", sep = ""),
             caption = "Datations de \\knw, analyses des laboratoires de Kiel
             (voir \\cref{Radiocarbon--MBA--2013} sur le traitement des
              échantillons) est  de Zürich (pas de données)",
             label = "Radiocarbon--LBA--2013"),
      include.rownames = FALSE,
      floating = FALSE,
      tabular.environment = "longtable",
      table.placement = NULL,
      booktabs = TRUE,
     sanitize.text.function = identity
)



##########

table <- read.delim("data/radiocarbon/table/Radiocarbon--LBA--WTer.tsv")
colnames(table) <- gsub("δ13.C.relatif.à.VPDB", "$\\\\delta^{13}C$ (VPDB)",
                        colnames(table))
colnames(table) <- gsub("δ15.N.relatif.à.l.air", "$\\\\delta^{15}N$ (air)",
                        colnames(table))
colnames(table) <- gsub("rapport.C.N..moles.", "rapport C/N (moles)",
                        colnames(table))
table$Datation..BP. <- gsub("±", "$\\\\pm$ ",table$Datation..BP.)
colnames(table) <- gsub("Datation..BP.", "Datation (BP)",colnames(table))
colnames(table) <- gsub("Numéro.Inventaire", "Inventaire",colnames(table))


print(xtable(table[,c(3:11)] ,
             align =  paste0("cp{1.8cm}p{1.8cm}p{5cm}p{2cm}p{1.8cm}p{1.5cm}",
                             "p{1.8cm}p{1.8cm}c" ),
         caption = "Datations de \\WTer, analyses du laboratoire d'Edinburgh
                    (pas d'information sur le prétraitement de l'analyse)",
         label = "Radiocarbon--LBA--WTer"),
  include.rownames = FALSE,
  floating = FALSE,
  tabular.environment = "longtable",
  table.placement = NULL,
  booktabs = TRUE,
 sanitize.text.function = identity)
