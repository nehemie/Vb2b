Models for the programm OxCal
-----------------------------

5 models
======

This folders contains following model:

  - model-base: the model
  - model-BA: only the EBA part of the model-base
  - model-BM: only the MBA part ot the model-base
  - model-BR: only the LBA part of the model-base
  - model-CrossRef : model-base with the end of the two MBA cross-reference


Kind of files
=======

  - oxcal: code for the program
  - csv: output of the table
  - svg: diagrams
