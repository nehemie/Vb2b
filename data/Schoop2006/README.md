README
------

## Copyrights:
Copyrights owner of the data is: U.-D. Schoop
License: unknown

## Origin of the data 

This data-sets are the data behind the figures from the articles of  U.-D.
Schoop, ‘Dating the Hittites with Statistics. Ten Pottery Assemblages', in:
Mielke, D. P.; Schoop, U.-D. & Seeher, J. (Eds.) _Strukturierung und Datierung in
der hethitischen Archäologie_, Ege Yayınları, 2006, 215-240. This data weren't
meant to be published, Schoop gives them to me for my thesis. They are here 
only to make my thesis reproducible. 


## Description of the variable
 - Kontext: name of the assemblages
 - req: rim-equivalent of subset
 - reqAll: rim-equivalent of the all the set but the subset
 - sreq: sum of rim-equivalent in the set
 - n:	number of element of subset
 - sn: sum of elements in the set


## Encoding

  - csv
  - separator ;
  - UTF-8
