
                       ██╗   ██╗██████╗ ██████╗ ██████╗
                       ██║   ██║██╔══██╗╚════██╗██╔══██╗
                       ██║   ██║██████╔╝ █████╔╝██████╔╝
                       ╚██╗ ██╔╝██╔══██╗██╔═══╝ ██╔══██╗
                        ╚████╔╝ ██████╔╝███████╗██████╔╝
                         ╚═══╝  ╚═════╝ ╚══════╝╚═════╝
                    _____  _____  _____  _____  _____  _____
                   |_____||_____||_____||_____||_____||_____|


    █╗$$$$$███$$$$$█╗$$$█╗█╗█╗$$$$█╗$$$$████╗$$$$████╗$█████$█████╗█████╗████╗
    █║$$$$█╔═█╗$$$$█║$$$█║█║█║$$$$█║$$$$█╔══╝$$$$█╔═██╗█╔══█╗█╔═══╝█╔═══╝█╔══╝
    █║$$$$████║$$$$█║$$$█║█║█║$$$$█║$$$$███╗$$$$$████╔╝█████║█████╗█████╗███$$
    █║$$$$█╔═█║$$$$╚█╗$█╔╝█║█║$$$$█║$$$$█╔═╝$$$$$█╔═██╗█╔══█║╚═══█║╚═══█║█╔═$$
    █████╗█║$█║$$$$$████╝$█║█████╗█████╗████╗$$$$████╔╝█║$$█║█████║█████║████╗
    ═════╝╚╝$╚╝$$$$$╚═══$$═╝╚════╝═════╝════╝$$$$════╝$═╝$$╚╝╚════╝╚════╝╚═══╝
    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

                                    ▀▀▀                      ▀ ▀
       ████╗$████╗$$$$████╗$$████╗$$███╗$$███╗$█████╗█╗$██╗$█████╗$██╗$$$██╗
       █╔═██╗█╔══╝$$$$█╔═██╗█╔═══█╗█╔══╝$█╔══█╗╚═██╔╝█║██╔╝██═══██╗╚██╗$██╔╝
       █║$██║███╗$$$$$████╔╝█║$$$█║█║$██╗█████║$$██╝$███╔╝$██$$$██║$╚████╔╝$
       █║$██║█╔═╝$$$$$█╔═██╗█║$$$█║█║$$█║█╔══█║$██╔$$█╔██╗$██$$$██║$$╚██╔╝$$
       ████╔╝████╗$$$$████╔╝╚████╔╝╚███╔╝█║$$█║█████╗█║$██╗╚█████╔╝$$$██║$$$
       ════╝$╚═══╝$$$$╚═══╝$$════╝$$═══╝$╚╝$$═╝╚════╝╚╝$╚═╝$╚════╝$$$$╚═╝$$$
       $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

         _____  _____  _____  _____  _____  _____  _____  _____  _____
        |_____||_____||_____||_____||_____||_____||_____||_____||_____|



Ceci est le repertoire de ma thèse, créé avec le logiciel `Git`. Il contient
toutes les informations nécessaires pour reproduire ma thèse, en partant des
données brutes.

## Reproduire cette thèse

Un [docker container](https://www.docker.com/what-container)
virtualise un système d'exploitation afin de fournir une
configuration identique, indépendamment de la machine hôte. L'image
est basée sur [debian testing](https://wiki.debian.org/DebianTesting)
où les programmes nécessaires sont installés (entre autres [GNU
make](https://www.gnu.org/software/make/),
[R](https://www.r-project.org/) (+ bibliothèques),
[LaTeX](https://www.latex-project.org/) (+ bibliothèques) et
[Inkscape](https://inkscape.org/en/). (Lien vers le [dockerfile](https://github.com/nehemie/vb2b/blob/master/Dockerfile)).
Pour produire le fichier PDF de cette thèse, il suffit d'exécuter les
commandes suivantes :

```bash
# You need docker
# Get the docker environment at https://docs.docker.com/get-started/
docker pull nehemie/vb2b:latest

# Go for interactive mode
docker run -it nehemie/vb2b:latest /bin/bash

# Clone only the latest version (the repository is very large...)
git clone --depth=1 "https://gitlab.com/nehemie/Vb2b.git"

# In the root directory (`Vb2b`) run make
cd Vb2b && make
```

How it works?

 1. execute the bash script `./makepng.sh` to create `*.png` from
    `*.svg` vector graphic of the pottery assemblages (with inkscape)
 2. change directory to ./Latex/
 3. `knitr` the 0-Vb2b-R.Rnw (run the `R` code and output `tex` chunk to be
    weave with `.tex` file
 4. compile the master `0-Vb2b-R.tex` file and compile bibliography
    and glossaries
 5. You own it: `0-Vb2b-R.pdf` is done

If you test it and have any problem, contact me! I will be happy to
help you (and to know that someone else tested it)

## Organisation des fichiers

### [BoFuWTer](/BoFuWTer)

**Bo**ğazköy **Fu**nden der **W**est **Ter**rase: base de données et le code `R`
des objets découverts à la WTer.


### [BoGisWTer](/BoGisWTer)

**Bo**ğazköy **GIS** der **W**est **Ter**rase :  Base de données et le code `R`
pour le Système d'information géographique de la WTer.

### [BoKeWTER](/BoKeWTER)

**Bo**ğazköy **Ke**ramik der **W**est   **Ter**rase : Base de données et le code
`R` pour la base de donnée céramique de la WTer


### [data](/data)

Autres données, soit compilées à partir de sources extérieures (livres, sites
internet) soit à partir de données réutilisées (datation C14). Les données sont
accompagnées du code `R` ou `oxcal` pour les analyser.

### [LaTeX](/LaTeX)

Textes de la thèse et code LaTeX pour produire le document .pdf final. Ce
dossier est subdivisé en :

  - **images** : images
  - **latex_preamble** : configuration de latex
  - **r_preamble** : chargement des données avec R pour faire les graphiques et
    exporter des tables en format LaTeX à partir des données brutes
  - **text**: le texte de ma thèse, divisé et numéroté selon les sections
  - **Makefile** Le fichier à compiler avec GNU make
  - **Bibliography-Vb2b.bib** la bibliographie

## License

 - Text: Néhémie Strupler CC BY-SA 4.0
 - Code: Néhémie Strupler GPL-3
 - Data (Image, Shapefile): Néhémie Strupler & Archiv des DAI CC BY-ND-NC 4.0

If you use it, I would appreciate a notice, in order to give you the
best way of citing this work. Thank you!

## Warranty

THE REPOSITORY IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
REPOSITORY OR THE USE OR OTHER DEALINGS IN THE REPOSITORY.
