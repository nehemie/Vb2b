README
------

- BoKeWTer: Database of the ceramic fromt the West-Terrasse (n=1884). Copyrights
  holder: N. Strupler. Data collectors:  Vera Egbers,  Irem Gocmez, Néhémie
Strupler,  Sarah Wittmann. License: CC BY-SA 4.0

- BoKeWTer-Kontexte: Database of the context for the database BoKeWTER.
  Copyrights holder: N. Strupler. Data collectors: P. Neve, N. Strupler. License
CC BY-SA 4.0
