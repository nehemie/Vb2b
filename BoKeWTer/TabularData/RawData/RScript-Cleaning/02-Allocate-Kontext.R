## On the field (Jun-Oct 2012), the ceramic was recorder with the year-identifier
## system. In this section each Kontext are lumped together to form 
## ceramic-assemblages

# Copy the Number before classification
a$Kontext <- as.character(a$KontextNbr)


##################################################
##### Make the assemblages from the context ######






#---------------------------------------------------------------------------------------------------------------------------
#### "Haus 19" ####
## Stratigraphy
## -------------
##     Geb19
##       ^
##     Geb85 
##--------------
##
a[a$Jahr == "Bo75" & a$Kontext == "G7-8","Kontext"] <- "Geb19"  # "H19" # Bo75-G7-8- Haus 19 - Altbau StegII/2.II/3 scherbenschutt über Fussboden H19
a[a$Jahr == "Bo75" & a$Kontext == "G14","Kontext"] <- "Geb19" # "H19" # Bo75-G14 - Haus 19 - Raum2 - Brandmischerde über Fussboden. (hieraus Frgmt des Reliefvase, diese z.T. direkt auf dem Fussboden)
a[a$Jahr == "Bo76" & a$Kontext == "A6",   "Kontext"] <- "Geb19" # "H85" # Bo76-A6  - Haus 19 - Raum1 - Ost. Aus Fallschutt über Fussboden (Wie Reliefgefäss)
a[a$Jahr == "Bo76" & a$Kontext == "A15", "Kontext"] <- "Geb19" # "H85" # Bo76-A15 - Haus 19 - Raum 1 - Altbau Fussboden
a[a$Jahr == "Bo76" & a$Kontext == "A17", "Kontext"] <- "Geb19" # "H85" # Bo76-A17 - Haus 19 - Raum1 - NW - Hirschenvaseniveau
a[a$Jahr == "Bo76" & a$Kontext == "A18", "Kontext"] <- "Geb19" # "H85" # Bo76-A18 - Haus 19 - Raum1 - NW, in Höhe des Fussbodens in Raum 1 Süd. Entspricht A17
a[a$Jahr == "Bo76" & a$Kontext == "A19", "Kontext"] <- "Geb19" # "H85" # Bo76-A19 - Haus 19 - Raum1 - NO in Höhe von A18, Brandmischerde

a[a$Jahr == "Bo76" & a$Kontext == "A20", "Kontext"] <- "Geb19" # "H19" # Bo76-A20 - Haus 19 - Raum1 - nördlich unterhalb Quermauer
a[a$Jahr == "Bo75" & a$Kontext == "P4",   "Kontext"] <- "Geb85" # "H85" # Bo75-P4  - Haus 19 - Unter P3, stark Scherbenhaltig - Raum2
a[a$Jahr == "Bo75" & a$Kontext == "P5",   "Kontext"] <- "Geb85" # "H85" # Bo75-P5  - Haus 19 - Unter P4, Brandschutt unter P4 und unter Steinsetzung, roter Ziegelbrand- Raum 2
a[a$Jahr == "Bo75" & a$Kontext == "P6",   "Kontext"] <- "Geb85" # "H85" # Bo75-P6  - Haus 19 - Tief, unter P5 (cf. schema stratigraphique sur le plan) -Raum2
a[a$Jahr == "Bo76" & a$Kontext == "A21", "Kontext"] <- "Geb19" # "H85" # Bo76-A21 - Haus 19 - Raum1 - unter A20 - Fussbodenbereich
a[a$Jahr == "Bo76" & a$Kontext == "A22", "Kontext"] <- "Geb19" # "H85" # Bo76-A22 - Haus 19 - Raum1 - unter A20
a[a$Jahr == "Bo76" & a$Kontext == "A23", "Kontext"] <- "Geb85" # "H85" # Bo76-A23 - Haus 19 - Raum1 - unter A20





#---------------------------------------------------------------------------------------------------------------------------
### Bo75 - Geb29 (Badewannenhaus)
a[a$Jahr == "Bo75" & a$Kontext == "o7",   "Kontext"] <- "Geb29" # Raum1a Fussboden in situ Gefass
a[a$Jahr == "Bo75" & a$Kontext == "o9",   "Kontext"] <- "Geb29" # Raum1a Fussbiden in situ Gefass
a[a$Jahr == "Bo75" & a$Kontext == "o8",   "Kontext"] <- "Geb29" # Raum1a Fussbodenauffullung
a[a$Jahr == "Bo76" & a$Kontext == "o12", "Jahr"] <- "Bo75"  # Bo76-o12 is wrong it has to be Bo75-012
a[a$Jahr == "Bo75" & a$Kontext == "o12", "Kontext"] <- "Geb29" # Raum2 uber Fussboden und ausgebrochener Mauer in SW-Teil, Schotterschichterde
a[a$Jahr == "Bo75" & a$Kontext == "o15", "Kontext"] <- "Geb29" # neben und unter Fragmenten des Badewanne uber Fussboden
a[a$Jahr == "Bo75" & a$Kontext == "U4",   "Kontext"] <- "Geb29" # Raum3 SUd, Fussboden jungere Phase
a[a$Jahr == "Bo75" & a$Kontext == "U5",   "Kontext"] <- "Geb29" # Haus29, Raum 8 , altere Phase
a[a$Jahr == "Bo75" & a$Kontext == "U6",   "Kontext"] <- "Geb29" # Haus29, Raum3 Sud auf Fussboden jungere phase 
a[a$Jahr == "Bo75" & a$Kontext == "T12",  "Kontext"] <- "Geb29" # Raum7 auf Fussboden




#---------------------------------------------------------------------------------------------------------------------------
### Bo77 Geb43
a[a$Jahr == "Bo77" & a$Kontext == "III3-8",   "Kontext"] <- "Geb43" #
a[a$Jahr == "Bo77" & a$Kontext == "III3-4",   "Kontext"] <- "Geb43" #




#---------------------------------------------------------------------------------------------------------------------------
### Bo77 Geb51
a[a$Jahr == "Bo77" & a$Kontext == "IV2-9", "Kontext"] <- "Geb51" # Bo77 - Haus 51 - Pithoshaus
# Notice founded with the ceramic:
# IV2-9 -> Ab KfNr 150 = "Vom Boden des alth Pithosraumes, dazu 1 Randstück
# IV2-9 -> vON kFNR 100-149 = "Im Pithos gefunden"
a[a$Jahr == "Bo78" & a$Kontext == "R3", "Kontext"] <- "Geb51" # Bo78 - Haus 51 - Pithoshaus




#---------------------------------------------------------------------------------------------------------------------------
#### "Haus 13" ####
## Stratigraphy
## -------------
##     Geb64
##       ^
##     Geb84 
##       ^
##     Geb94
##--------------
## Ev13ka = Geb94
## Ev13a  = Geb84 
## Ev13   = Geb13 (Excavation 1976)

## Bo76
a[a$Jahr == "Bo76" & a$Kontext == "F5", "Kontext"] <- "Geb94" # Bo76 - Raum14a
a[a$Jahr == "Bo76" & a$Kontext == "F6", "Kontext"] <- "Geb94" # Bo76 - Raum14
a[a$Jahr == "Bo76" & a$Kontext == "A7", "Kontext"] <- "Geb94" # Bo76 - Feuerstelle
a[a$Jahr == "Bo76" & a$Kontext == "A4",  "Kontext"] <- "Geb94" ## Haus 13, Raum3

#Bo75 
a[a$Jahr == "Bo75" & a$Kontext == "A2", "Kontext"] <- "Geb84" # Raum7 Altbau13
a[a$Jahr == "Bo75" & a$Kontext == "A3", "Kontext"] <- "Geb84" # Raum7 Altbau13
a[a$Jahr == "Bo75" & a$Kontext == "A4", "Kontext"] <- "Geb84" # Raum 7
a[a$Jahr == "Bo75" & a$Kontext == "A5", "Kontext"] <- "Geb84" # Auffullung Altbau13 Raum 7
a[a$Jahr == "Bo75" & a$Kontext == "A8", "Kontext"] <- "Geb84" # Auffullung Altbau13 Raum 8

# Bo76
a[a$Jahr == "Bo76" & a$Kontext == "A12", "Kontext"] <- "Geb84" ##Haus 13, Raum2 Brandschutt vor hier ?ltester Mauer 
a[a$Jahr == "Bo76" & a$Kontext == "A16", "Kontext"] <- "Geb84" ## Haus 13, Raum4 ?ber Mauer 13a
a[a$Jahr == "Bo76" & a$Kontext == "A25", "Kontext"] <- "Geb84" ### Haus 13, Fussboden

#### Bo76 Phase recente de "Haus 13" = Geb64
a[a$Jahr == "Bo76" & a$Kontext == "B16", "Kontext"] <- "Geb13" ## Haus 13, Ostfl?gel, alterhethi Fussboden
a[a$Jahr == "Bo76" & a$Kontext == "B24", "Kontext"] <- "Geb13" ## Haus 13, ?ber altbau
a[a$Jahr == "Bo76" & a$Kontext == "B25", "Kontext"] <- "Geb13" ## ?ber Pflasterboden des Bassins
a[a$Jahr == "Bo76" & a$Kontext == "B23", "Kontext"] <- "Geb13" ## Oberste Fussboden SO Eckraum





#---------------------------------------------------------------------------------------------------------------------------
#### Geb95 ####
## Stratigraphy
## -------------
##     Geb41
##       ^
##     Geb95 
##--------------
a[a$Jahr == "Bo77" & a$Kontext == "I1-9",    "Kontext"] <- "Geb95" # Bo77-Fussboden USt.4 Hufeisenherd mit Skelett
a[a$Jahr == "Bo77" & a$Kontext == "I1-10",  "Kontext"] <- "Geb95" # Bo77-Fussboden USt.4 Hufeisenherd mit Skelett
a[a$Jahr == "Bo77" & a$Kontext == "I1-13",  "Kontext"] <- "Geb95" # Bo77-Fussboden USt.4 Hufeisenherd mit Skelett
a[a$Jahr == "Bo77" & a$Kontext == "II1-19", "Kontext"] <- "Geb95" # Haus 40, Raum 3 nebem karumzeitlicher Mauer
a[a$Jahr == "Bo78" & a$Kontext == "Ust4", "Kontext"] <- "Geb96" # 


#---------------------------------------------------------------------------------------------------------------------------
## Not enough material to date
###Geb42
a[a$Jahr == "Bo77" & a$Kontext == "IV4-12",   "Kontext"] <- "Geb52" # Bo77, Haus 42-Ost Geb52
### Geb 40

a[a$Jahr == "Bo77" & a$Kontext == "V4-5",   "Kontext"] <- "Geb52" # unter V4-3 -> Geb52?
#a[a$Jahr=="Bo77" & a$Kontext=="V4-3",   "Kontext"] <- "karum" # Fussboden der Karumzeit mit Siegel
a[a$Jahr == "Bo77" & a$Kontext == "V4-4",   "Kontext"] <- "Geb52" # unter V4-4 Mauer der Karumzeit -> Geb52?

#------------------------------------------------------------------------------------------------------------------------------
### Karum
## Bo73 - Geb10, Geb11
a[a$Jahr == "Bo73" & a$Kontext == "40",   "Kontext"] <- "Geb10" # n=40 Haus 10, über altem aschenboden (zu Ofen 1 gehörig) 
a[a$Jahr == "Bo73" & a$Kontext == "49A", "Kontext"] <- "Geb10" # n=15 Haus 10, unmittelbar auf Fussbpdem, der zu altem gebäude mit Ofen Geh?rt
a[a$Jahr == "Bo73" & a$Kontext == "49B", "Kontext"] <- "Geb10" # n= 55 West Haus 10 uber gewachsenem Boden
a[a$Jahr == "Bo73" & a$Kontext == "46",   "Kontext"] <- "Geb11" # n=26 Haus 11, über altem aschenboden (zu Ofen 1 gehörig)
a[a$Jahr == "Bo73" & a$Kontext == "69",   "Kontext"] <- "Geb11" # n = 15 Haus 11 -
a[a$Jahr == "Bo75" & a$Kontext == "T4", "Kontext"] <- "Geb18" # Bo75 - Haus 18 - Auf älterem Fussobden Haus 18, Raum 12
a[a$Jahr == "Bo76" & a$Kontext == "B28", "Kontext"] <- "Geb31" # Bo76 - Haus 31 uber aelterheth. Fussboden





 






###############################
###############################
#                        NOT STUDIED                                #
####            IN THIS THESIS !!!!                       ###																				
###############################
																							





#------------------------------------------------------------------------------------------------------------------------------
## Bo09-Bo10-Bo11 MBA Inventories
#
# These assemblages were described in
#
# - Strupler 2011, Vorläufiger Überblick über die Karum-zeitlichen Keramikinventare aus den Grabungen in der südlichen Unterstadt
#   AA, Die Ausgrabungen in 2010, 2011, AA 2011/1, 51 - 57
# 
# - See AA, Die Ausgrabungen in 2011, 2012, AA 2012/1, 89 - 92
#
#
### South MBA Inventory 
a[a$Jahr == "Bo09" & a$Kontext == "204", "Kontext"] <- "KaInSud" # Bo9 - 204
a[a$Jahr == "Bo10" & a$Kontext == "80",   "Kontext"] <- "KaInSud" # Bo10-80
a[a$Jahr == "Bo10" & a$Kontext == "81",   "Kontext"] <- "KaInSud" # Bo10-81
a[a$Jahr == "Bo10" & a$Kontext == "93",   "Kontext"] <- "KaInSud" # Bo10-93
a[a$Jahr == "Bo10" & a$Kontext == "94",   "Kontext"] <- "KaInSud" # Bo10-94
a[a$Jahr == "Bo10" & a$Kontext == "112", "Kontext"] <- "KaInSudFB" # Bo10-112 Fussboden
a[a$Jahr == "Bo11" & a$Kontext == "57",   "Kontext"] <- "KaInSud" # Bo11-57
a[a$Jahr == "Bo11" & a$Kontext == "70",   "Kontext"] <- "KaInSudFB" # Bo11 Fussboden
a[a$Jahr == "Bo11" & a$Kontext == "71",   "Kontext"] <- "KaInSud2" # Bo11-ZwischenMauerU-offen
a[a$Jahr == "Bo11" & a$Kontext == "58",   "Kontext"] <- "KaInSud2" # Bo11-Uber 71
a[a$Jahr == "Bo11" & a$Kontext == "74",   "Kontext"] <- "KaInSud" # Bo11-Unkalr
### North MBA Inventory 
a[a$Kontext == "177",   "Kontext"] <- "KaInNo" # Bo09, Bo10, Bo11-177











#------------------------------------------------------------------------------------------------------------------------------
## Bo09 Hehtitisches Haus and Bo09-Oven
#
#
# This section was analysed and publish in 
# Strupler, N. Hethitische Keramik aus den Grabungen in der Unterstadt AA, 2013, 2, 147-193
a[a$Jahr == "Bo09" & a$Kontext == "309", "Kontext"] <- "HH" ## Controler l'origine
a[a$Jahr == "Bo09" & a$Kontext == "393", "Kontext"] <- "HH" ## "Raum 6" (SO-Ecke)
a[a$Jahr == "Bo09" & a$Kontext == "395", "Kontext"] <- "HH" ## "Raum 6" (SO-Ecke)
a[a$Jahr == "Bo09" & a$Kontext == "403", "Kontext"] <- "HH" ## "Raum 2" (West-Mitte)
a[a$Jahr == "Bo09" & a$Kontext == "412", "Kontext"] <- "HH" ## "Raum 2" (West-Mitte)
a[a$Jahr == "Bo09" & a$Kontext == "414", "Kontext"] <- "HH" ## "Raum 6" (SO-Ecke)
a[a$Jahr == "Bo09" & a$Kontext == "416", "Kontext"] <- "HH" ## "Raum 6" (SO-Ecke)
#
#
# ### Oven
a[a$Jahr == "Bo10" & a$Kontext == "50",   "Kontext"] <- "Ofen" # Bo10-50 Raumfullung
a[a$Jahr == "Bo10" & a$Kontext == "85",   "Kontext"] <- "Ofen" # Bo10-85 - Ofenfullung
a[a$Jahr == "Bo11" & a$Kontext == "85",   "Kontext"] <- "Ofen" # Bo11-85 - Mit dem Ofen Geborgen

### Sondage im Sudareal
#
# For Preliminary report see:
##
#
a[a$Jahr == "Bo10" & a$Kontext == "930",   "Kontext"] <- "Suda" # Bo10-930
a[a$Jahr == "Bo10" & a$Kontext == "931",   "Kontext"] <- "Suda" # Bo10-931
a[a$Jahr == "Bo10" & a$Kontext == "932",   "Kontext"] <- "Suda" # Bo10-932
a[a$Jahr == "Bo11" & a$Kontext == "924",   "Kontext"] <- "Suda" # Bo11-924
a[a$Jahr == "Bo11" & a$Kontext == "925",   "Kontext"] <- "Suda" # Bo11-925
a[a$Jahr == "Bo11" & a$Kontext == "926",   "Kontext"] <- "Suda" # Bo11-926


### Bo07 -- Sondage Oberstadt
#
# For Preliminary report see:
##
#
a[a$Jahr == "Bo07" & a$Kontext == "1155",   "Kontext"] <- "Soob" # Sondage Oberstadt
a[a$Jahr == "Bo07" & a$Kontext == "1172",   "Kontext"] <- "Soob" # Sondage Oberstadt
a[a$Jahr == "Bo07" & a$Kontext == "1186",   "Kontext"] <- "Soob" # Sondage Oberstadt
a[a$Jahr == "Bo07" & a$Kontext == "1193",   "Kontext"] <- "Soob" # Sondage Oberstadt
a[a$Jahr == "Bo07" & a$Kontext == "1200",   "Kontext"] <- "Soob" # Sondage Oberstadt
a[a$Jahr == "Bo07" & a$Kontext == "1202",   "Kontext"] <- "Soob" # Sondage Oberstadt
a[a$Jahr == "Bo07" & a$Kontext == "1208",   "Kontext"] <- "Soob" # Sondage Oberstadt
a[a$Jahr == "Bo07" & a$Kontext == "1213",   "Kontext"] <- "Soob" # Sondage Oberstadt
a[a$Jahr == "Bo07" & a$Kontext == "1219",   "Kontext"] <- "Soob" # Sondage Oberstadt
a[a$Jahr == "Bo07" & a$Kontext == "1223",   "Kontext"] <- "Soob" # Sondage Oberstadt
a[a$Jahr == "Bo07" & a$Kontext == "1227",   "Kontext"] <- "Soob" # Sondage Oberstadt
a[a$Jahr == "Bo07" & a$Kontext == "1228",   "Kontext"] <- "Soob" # Sondage Oberstadt
a[a$Jahr == "Bo07" & a$Kontext == "1233",   "Kontext"] <- "Soob" # Sondage Oberstadt
a[a$Jahr == "Bo07" & a$Kontext == "1236",   "Kontext"] <- "Soob" # Sondage Oberstadt
a[a$Jahr == "Bo07" & a$Kontext == "1242",   "Kontext"] <- "Soob" # Sondage Oberstadt
a[a$Jahr == "Bo07" & a$Kontext == "1243",   "Kontext"] <- "Soob" # Sondage Oberstadt



a$Kontext <- as.factor(as.character(a$Kontext)) 
