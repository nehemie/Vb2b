write.table(a, file = "../BoKeStrupler2012-cleaned.tab", quote = FALSE, sep = "\t", na = "NA", row.names = FALSE, fileEncoding = "UTF-8")

export<-a

### Add 3 columns to be like FIleMakerPro (container) and sort by colnames
export$LinkZ <- c("")
export$LinkTemiz <- c("")
export$LinkFoto <- c("")
export <- export[,sort(colnames(export))]

write.table(export, file = "../BoKeStrupler2012-cleaned-4FileMaker.tab", 
            quote = FALSE, 
            sep = "\t", 
            na = "NA", 
            row.names = FALSE, 
            fileEncoding = "UTF-8")



### Export for publication


BoKeWTer <- droplevels(a[a$Jahr!="Bo07" & a$Jahr!="Bo09" & a$Jahr!="Bo10" & a$Jahr!="Bo11", ])
BoKeSondageTvS <- droplevels(a[a$Jahr=="Bo07", ])
BoKeKNW  <- droplevels(a[a$Jahr=="Bo09" | a$Jahr=="Bo10" & a$Kontext!="Suda" | a$Jahr=="Bo11"& a$Kontext!="Suda" , ])
BoKeSuda  <- droplevels(a[a$Kontext=="Suda", ])

write.table(BoKeWTer, file = "../../BoKeWTer.tab", 
            quote = FALSE, 
            sep = "\t", 
            na = "NA", 
            row.names = FALSE, 
            fileEncoding = "UTF-8")

write.table(BoKeSondageTvS, file = "../BoKeSondageTvS.tab", 
            quote = FALSE, 
            sep = "\t", 
            na = "NA", 
            row.names = FALSE, 
            fileEncoding = "UTF-8")

write.table(BoKeKNW, file = "../BoKeKNW.tab", 
            quote = FALSE, 
            sep = "\t", 
            na = "NA", 
            row.names = FALSE, 
            fileEncoding = "UTF-8")

write.table(BoKeSuda, file = "../BoKeSuda.tab", 
            quote = FALSE, 
            sep = "\t", 
            na = "NA", 
            row.names = FALSE, 
            fileEncoding = "UTF-8")