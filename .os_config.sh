echo LIST OF INSTALLED PACKAGE \\n -------------------- \\n dpkg-query --list  \\n --------------------- \\n  | tee -a os_config.txt
   dpkg-query --list | tee -a os_config.txt
echo \\n\\n\\n\\n\\n\\n CPU / HARDWARE INFORMATION \\n --------------------- \\n lscpu  \\n --------------------- \\n  | tee -a os_config.txt
   lscpu | tee -a os_config.txt
echo \\n\\n\\n\\n\\n\\n  LINUX DISTRIBUTION AND VERSION \\n --------------------- \\n   cat /proc/version  \\n --------------------- \\n  | tee -a os_config.txt
   cat /proc/version | tee -a os_config.txt
