#! /bin/bash

PATHIMG=$PWD/BoKeWTer/Images/SVG

GEB4=(Bo71-341 Bo71-342 Bo71-350 Bo71-356a Bo71-356b Bo71-489 Bo71-496 Bo71-497
  Bo73-495 Bo73-496 Bo73-497)
GEB4=( "${GEB4[@]/#/$PATHIMG/Small_assemblages/Geb4/}" )

GEB18=(Bo75-589 Bo75-T4-3 Bo75-T4-4 Bo75-T4-6 Bo75-T4-8 Bo75-T4-12 Bo75-T4-18
  Bo75-T4-22)
GEB18=( "${GEB18[@]/#/$PATHIMG/Small_assemblages/Geb18/}" )

GEB19=(Bo75-186 Bo75-333 Bo75-G14-7 Bo75-353 Bo75-G14-40 Bo75-G14-60 Bo75-G14-73
 Bo76-355 Bo76-421 Bo76-A15-6 Bo76-A15-13 Bo76-A19-1 Bo76-A18-1 Bo76-A19-47)
GEB19=("${GEB19[@]/#/$PATHIMG/Geb19/}")

GEB29=(Bo75-590 Bo75-591 Bo75-592 Bo75-601 Bo75-603 Bo75-604 Bo75-o9-4
  Bo75-T12-16 Bo75-T12-30 Bo75-U5-7 Bo75-U6-1)
GEB29=("${GEB29[@]/#/$PATHIMG/Geb29/}")

GEB43=(Bo77-III3-4-4 Bo77-III3-4-18 Bo77-III3-4-23 Bo77-III3-4-28 Bo77-III3-4-29
  Bo77-III3-4-38 Bo77-III3-4-48 Bo77-III3-4-53 Bo77-III3-4-83 Bo77-III3-8-13
  Bo77-III3-8-19 Bo77-III3-8-20 Bo77-III3-8-35 Bo77-III3-8-41 Bo77-III3-8-55)
GEB43=("${GEB43[@]/#/$PATHIMG/Geb43/}")

GEB51=(Bo77-IV2-9-153)
GEB51=("${GEB51[@]/#/$PATHIMG/Geb51/}")

GEB84=( Bo75-A2-21 Bo75-A2-28 Bo75-A2-39 Bo75-A2-9 Bo75-A5-15 Bo75-A5-25
  Bo75-A5-53 Bo75-A5-8 Bo75-A8-23 Bo75-A8-51 Bo76-A12-14 Bo76-A12-15 Bo76-A12-21
  Bo76-A12-3 Bo76-A16-47 Bo76-A16-9 )
GEB84=("${GEB84[@]/#/$PATHIMG/Geb84/}")


GEB85=(Bo75-P6-104 Bo75-P5-119 Bo75-P5-120 Bo75-P5-135 Bo75-P5-137 Bo75-P5-174
  Bo75-P5-176 Bo75-P6-100 Bo75-P6-101 Bo75-P6-108 Bo76-350
  Bo76-352 Bo76-415 Bo76-426 Bo76-A23-16 Bo76-A23-17 )
GEB85=( "${GEB85[@]/#/$PATHIMG/Geb85/}" )

GEB92=(Bo73-485 Bo73-486)
GEB92=("${GEB92[@]/#/$PATHIMG/Geb92/}")

GEB94=(Bo76-432 Bo76-433 Bo76-434 Bo76-A4-11 Bo76-A4-52 Bo76-A4-6
  Bo76-A4-7 Bo76-F5-1 Bo76-F5-9 Bo76-F6)
GEB94=("${GEB94[@]/#/$PATHIMG/Geb94/}")

GEB95=(Bo77-335 Bo77-360 Bo77-I1-10-11 Bo77-I1-10-12 Bo77-I1-10-6 Bo77-I1-10-7
  Bo77-I1-10-8 Bo77-I1-10-9 Bo77-I1-13-1 Bo77-I1-9-2 Bo77-I1-9-5 Bo77-I1-9-7
  Bo77-I1-9-8 Bo77-I1-9-11)
GEB95=("${GEB95[@]/#/$PATHIMG/Geb95/}")

GEB96=(Bo78-46 Bo78-47 Bo78-48 Bo78-49 Bo78-52 Bo78-55 Bo78-Ust4-4 Bo78-Ust4-5
  Bo78-Ust4-6)
GEB96=("${GEB96[@]/#/$PATHIMG/Geb96/}")

SA=(Bo75-588 Bo75-589 Bo76-307 Bo76-402)
SA=( "${SA[@]/#/$PATHIMG/Small_assemblages/}" )

PATHDES=$PWD/BoKeWTer/Description
DES=(B1-Struplers_Databases B2-BoKeWTer-Inclusion_size
  B3-BoKeWTer-Inclusion_shape B4-BoKeWTer-Inclusion_selection
  B5-BoKeWTer-Firing_core  B5-BoKeWTer-Firing_core_c
  B6-BoKeWTer-Colors_chart )
DES=( "${DES[@]/#/$PATHDES/}" )

[ -d $PWD/LaTeX/images] || mkdir $PWD/LaTeX/images

for f in \
  ${GEB4[@]}\
  ${GEB18[@]}\
  ${GEB19[@]}\
  ${GEB29[@]}\
  ${GEB43[@]}\
  ${GEB51[@]}\
  ${GEB84[@]}\
  ${GEB85[@]}\
  ${GEB92[@]}\
  ${GEB94[@]}\
  ${GEB95[@]}\
  ${GEB96[@]}\
  ${SA[@]}\
  ${DES[@]}\
  ; do
  inkscape "${f}.svg" -d 600 -o "${f}.pdf"
  mv "${f}.pdf" $PWD/LaTeX/images
done

PATHc14=$PWD/data/radiocarbon/model
DESTc14=$PWD/LaTeX/images
RES=600
inkscape "$PATHc14/model-BronzeAncien.svg" -d $RES -o "$DESTc14/3-6-model-BronzeAncien.png"
inkscape "$PATHc14/model-BronzeMoyen.svg" -d $RES -o "$DESTc14/3-7-model-BronzeMoyen.png"
inkscape "$PATHc14/model-BronzeRecent.svg" -d $RES -o "$DESTc14/3-8-model-BronzeRecent.png"
inkscape "$PATHc14/model-Schematic-Nehemie.svg" -d $RES -o "$DESTc14/3-9-model-Schematic-Nehemie.png"
inkscape "$PATHc14/model-base-BoundaryDebutBA.svg" -d $RES -o "$DESTc14/3-10-model-base-BoundaryDebutBA.png"
inkscape "$PATHc14/model-base-BoundaryTransitionBA-BM.svg" -d $RES -o "$DESTc14/3-11-model-base-BoundaryTransitionBA-BM.png"
inkscape "$PATHc14/model-base-BoundaryTransitionBM-BR.svg" -d $RES -o "$DESTc14/3-12-model-base-BoundaryTransitionBM-BR.png"
inkscape "$PATHc14/model-base-BoundaryFinBR.svg" -d $RES -o "$DESTc14/3-13-model-base-BoundaryFinBR.png"
inkscape "$PATHc14/model-base.svg" -d $RES -o "$DESTc14/3-14-model-base.png"
inkscape "$PATHc14/model-CrossRef.svg" -d $RES -o "$DESTc14/3-15-model-CrossRef.png"

PATHdata=$PWD/data
DEST_data_img=$PWD/LaTeX/images
RES=600
inkscape "$PATHdata/2-1-docu_dynamique.svg" -d $RES -o "$DEST_data_img/2-1-docu_dynamique.png"
inkscape "$PATHdata/2-2-ResearchPipeline.svg" -d $RES -o "$DEST_data_img/2-2-ResearchPipeline.png"
inkscape "$PATHdata/2-4-WorkflowNSt.svg" -d $RES -o "$DEST_data_img/2-4-WorkflowNSt.png"
