SHELL := /bin/bash
BASE = 0-Vb2b-R
BASE_rnw = $(BASE).Rnw
BASE_pdf = $(BASE).pdf
DEST = STRUPLER_MS
DEST_pdf = STRUPLER_MS.pdf
DEST_zip = STRUPLER_MS.zip

Vb2b: ./LaTeX/0-Vb2b-R.Rnw output_png_from_BoKeWTer_svgs
	cd LaTeX && \
		Rscript -e 'grDevices::pdf.options(useDingbats = FALSE); require(knitr);  knit("$(BASE_rnw)", encoding="UTF-8")' && \
		xelatex $(BASE) && \
		biber $(BASE) && \
		makeglossaries $(BASE) && \
		xelatex $(BASE) && \
		xelatex $(BASE) && \
		mv $(BASE_pdf) $(DEST_pdf)

test: output_png_from_BoKeWTer_svgs
	cd LaTeX && \
		Rscript -e 'grDevices::pdf.options(useDingbats = FALSE); require(knitr);  knit("$(BASE_rnw)", encoding="UTF-8")' && \
		xelatex $(BASE) && \
		biber $(BASE)

output_png_from_BoKeWTer_svgs:
	bash makepng.sh


MS:
	zip -r $(DEST_zip) \
		LaTeX/Bibliography-Vb2b.bib


