#!/bin/bash

for f in $(find -name '*.pdf') ; do
  pdftops ${f} ./"${f%.pdf}".ps
  done

for f in $(find -name '*.ps') ; do
  gs -dPDFA -dBATCH -dNOPAUSE -dNOOUTERSAVE -dUseCIEColor \
    -sProcessColorModel=DeviceCMYK -sDEVICE=pdfwrite \
    -sPDFACompatibilityPolicy=1 \
    -sOutputFile=./"${f%.ps}.pdf" \
     ${f}
  rm ${f}
  done
#
